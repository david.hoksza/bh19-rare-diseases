package lcsb.mapviewer.modelutils.map;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * Class with some util method for {@link BioEntity} objects.
 * 
 * @author Piotr Gawron
 * 
 */
public final class ElementUtils {

  /**
   * This object contains inheritance tree for {@link BioEntity} interface.
   */
  private static ClassTreeNode annotatedObjectTree = null;

  /**
   * Map between class names (used as user input) and implementation of
   * {@link Element} class.
   */
  private static Map<String, Class<? extends Element>> elementClasses = null;

  /**
   * Map between class names (used as user input) and implementation of
   * {@link Reaction} class.
   */
  private static Map<String, Class<? extends Reaction>> reactionClasses = null;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(ElementUtils.class);

  /**
   * @param elementClasses
   *          the elementClasses to set
   * @see #elementClasses
   */
  protected static void setElementClasses(Map<String, Class<? extends Element>> elementClasses) {
    ElementUtils.elementClasses = elementClasses;
  }

  /**
   * @param reactionClasses
   *          the reactionClasses to set
   * @see #reactionClasses
   */
  protected static void setReactionClasses(Map<String, Class<? extends Reaction>> reactionClasses) {
    ElementUtils.reactionClasses = reactionClasses;
  }

  /**
   * This method return tag that identifies {@link BioEntity}. This tag should be
   * used in warning messages.
   *
   * @param element
   *          tag for this element is created
   * @return tag that identifies element
   */
  public String getElementTag(Drawable element) {
    return getElementTag(element, null);
  }

  /**
   * This method return tag that identifies {@link BioEntity}. This tag should be
   * used in warning messages.
   *
   * @param element
   *          tag for this element is created
   * @param annotator
   *          this object identifies class that will produce warning. it can be
   *          null (in such situation it will be skipped in the tag)
   * @return tag that identifies element
   */
  public String getElementTag(Drawable element, Object annotator) {
    String id = null;
    String className = null;
    if (element != null) {
      id = element.getElementId();
      className = element.getClass().getSimpleName();
    }
    if (annotator != null) {
      return "[" + annotator.getClass().getSimpleName() + "]\t[" + className + " " + id
          + "]\t";
    } else {
      return "[" + className + " " + id + "]\t";
    }
  }

  /**
   * Returns {@link #annotatedObjectTree}.
   *
   * @return {@link #annotatedObjectTree}
   */
  public ClassTreeNode getAnnotatedElementClassTree() {
    if (annotatedObjectTree == null) {
      Reflections reflections = new Reflections("lcsb.mapviewer.model.map");
      Set<Class<? extends BioEntity>> classes = reflections.getSubTypesOf(BioEntity.class);

      ClassTreeNode result = new ClassTreeNode(BioEntity.class);
      result.setData(false);

      Map<Class<?>, ClassTreeNode> map = new LinkedHashMap<>();
      map.put(Object.class, result);
      for (Class<? extends BioEntity> class1 : classes) {
        if (map.get(class1) == null) {
          ClassTreeNode node = new ClassTreeNode(class1);
          // set information if the class annotation should be required
          for (RequireAnnotationMap val : RequireAnnotationMap.values()) {
            if (val.getClazz().equals(class1)) {
              node.setData(true);
            }
          }
          map.put(class1, node);
        }
      }
      for (Class<? extends BioEntity> class1 : classes) {
        ClassTreeNode parent = map.get(class1.getSuperclass());
        ClassTreeNode child = map.get(class1);
        parent.getChildren().add(child);
        child.setParent(parent);
      }

      // set information if the class annotation should be required for children
      // classes (if not explicitly set in child then inherit it from parent)
      for (Class<? extends BioEntity> class1 : classes) {
        ClassTreeNode child = map.get(class1);
        Boolean value = (Boolean) child.getData();
        if (value == null) {
          ClassTreeNode parent = map.get(class1.getSuperclass());
          while (parent.getData() == null) {
            parent = parent.getParent();
          }
          child.setData(parent.getData());
        }
      }
      for (ClassTreeNode node : map.values()) {
        Collections.sort(node.getChildren(), new ClassTreeNodeNameComparator());
      }
      annotatedObjectTree = result;
    }
    return annotatedObjectTree;
  }

  /**
   * Returns list of classes that extends {@link Element} class, but don't have
   * children (leaves in the hierarchy tree).
   *
   * @return list of classes that extends {@link Element} class, but don't have
   *         children (leaves in the hierarchy tree)
   */
  public List<Class<? extends Element>> getAvailableElementSubclasses() {
    List<Class<? extends Element>> result = new ArrayList<>();
    if (elementClasses == null) {
      refreshClasses();
    }
    result.addAll(elementClasses.values());
    Collections.sort(result, new ClassNameComparator());
    return result;
  }

  /**
   * Refresh list of known implementation of {@link Element} class.
   */
  protected void refreshClasses() {
    List<Class<? extends Element>> tmp = new ArrayList<>();

    Reflections reflections = new Reflections("lcsb.mapviewer.model.map");
    elementClasses = new LinkedHashMap<>();
    Set<Class<? extends Element>> classes = reflections.getSubTypesOf(Element.class);

    Set<Class<?>> toRemove = new HashSet<Class<?>>();

    for (Class<? extends Element> class1 : classes) {
      toRemove.add(class1.getSuperclass());
      tmp.add(class1);
    }
    for (Class<?> clazz : toRemove) {
      tmp.remove(clazz);
    }
    for (Class<? extends Element> class1 : tmp) {
      elementClasses.put(class1.getSimpleName(), class1);
    }

    List<Class<? extends Reaction>> rTmp = new ArrayList<>();

    reflections = new Reflections("lcsb.mapviewer.model.map.reaction.type");
    reactionClasses = new LinkedHashMap<>();

    Set<Class<? extends Reaction>> rClasses = reflections.getSubTypesOf(Reaction.class);

    toRemove = new HashSet<>();

    for (Class<? extends Reaction> class1 : rClasses) {
      if (!(class1.isAnonymousClass() || class1.isMemberClass())) {
        toRemove.add(class1.getSuperclass());
        rTmp.add(class1);
      }
    }
    for (Class<?> clazz : toRemove) {
      rTmp.remove(clazz);
    }
    for (Class<? extends Reaction> class1 : rTmp) {
      reactionClasses.put(class1.getSimpleName().replaceAll("Reaction", ""), class1);
    }

  }

  /**
   * Returns a {@link Class} that extends {@link BioEntity} for a given name.
   *
   * @param name
   *          name of the class
   * @return {@link Class} that extends {@link BioEntity} for a given name
   */
  public Class<?> getClassByName(String name) {
    if (elementClasses == null) {
      refreshClasses();
    }
    Class<?> result = elementClasses.get(name);
    if (result == null) {
      result = reactionClasses.get(name);
    }
    return result;
  }

  /**
   * Return list of {@link Reaction} classes that are available in the system.
   *
   * @return list of {@link Reaction} classes that are available in the system
   */
  public List<Class<? extends Reaction>> getAvailableReactionSubclasses() {
    List<Class<? extends Reaction>> result = new ArrayList<Class<? extends Reaction>>();
    if (reactionClasses == null) {
      refreshClasses();
    }
    result.addAll(reactionClasses.values());
    Collections.sort(result, new ClassNameComparator());
    return result;
  }

  public String getElementTag(ReactionNode node) {
    String reactionPrefix = "";
    if (node.getReaction() != null) {
      reactionPrefix = getElementTag(node.getReaction()).replaceAll("\t", "");
    }
    return reactionPrefix + "[" + node.getClass().getSimpleName() + "]" + getElementTag(node.getElement());
  }

  public String getElementTag(ModificationResidue mr) {
    return "[" + mr.getClass().getSimpleName() + "," + mr.getIdModificationResidue() + "]"
        + getElementTag(mr.getSpecies());
  }

}
