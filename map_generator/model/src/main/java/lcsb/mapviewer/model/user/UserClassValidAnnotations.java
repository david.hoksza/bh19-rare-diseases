package lcsb.mapviewer.model.user;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import lcsb.mapviewer.model.map.MiriamType;

/**
 * Defines set of valid {@link MiriamType annotations} for a given object type.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class UserClassValidAnnotations implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  /**
   * {@link UserAnnotationSchema} in which this set of valid {@link MiriamType
   * annotations} is used.
   */
  @ManyToOne
  private UserAnnotationSchema annotationSchema;

  /**
   * Name of the class for which this set is defined.
   */
  private String className;

  /**
   * List of annotations that are valid.
   */
  @ElementCollection
  @JoinTable(name = "class_valid_annotation_miriam_type_table", joinColumns = @JoinColumn(name = "class_valid_annotation_id"))
  @Column(name = "miriam_type_name", nullable = false)
  @OrderColumn(name = "idx")
  @Enumerated(EnumType.STRING)
  private List<MiriamType> validMiriamTypes = new ArrayList<>();

  /**
   * Default constructor.
   */
  public UserClassValidAnnotations() {

  }

  /**
   * Default constructor.
   * 
   * @param clazz
   *          {@link #className}
   * @param miriamTypes
   *          {@link #validMiriamTypes}
   */
  public UserClassValidAnnotations(Class<?> clazz, Collection<MiriamType> miriamTypes) {
    setClassName(clazz);
    this.validMiriamTypes.addAll(miriamTypes);
  }

  /**
   * Default constructor.
   * 
   * @param clazz
   *          {@link #className}
   * @param miriamTypes
   *          {@link #validMiriamTypes}
   */
  public UserClassValidAnnotations(Class<?> clazz, MiriamType[] miriamTypes) {
    setClassName(clazz);
    for (MiriamType miriamType : miriamTypes) {
      this.validMiriamTypes.add(miriamType);
    }
  }

  /**
   * @return the validMiriamTypes
   * @see #validMiriamTypes
   */
  public List<MiriamType> getValidMiriamTypes() {
    return validMiriamTypes;
  }

  /**
   * @param validMiriamTypes
   *          the validMiriamTypes to set
   * @see #validMiriamTypes
   */
  public void setValidMiriamTypes(List<MiriamType> validMiriamTypes) {
    this.validMiriamTypes = validMiriamTypes;
  }

  /**
   * @return the className
   * @see #className
   */
  public String getClassName() {
    return className;
  }

  /**
   * @param className
   *          the className to set
   * @see #className
   */
  public void setClassName(String className) {
    this.className = className;
  }

  /**
   * Sets {@link #className} value.
   *
   * @param clazz
   *          new {@link #className} value
   */
  public void setClassName(Class<?> clazz) {
    setClassName(clazz.getCanonicalName());
  }

  /**
   * @return the id
   * @see #id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Adds new valid annotation to {@link #validMiriamTypes}.
   * 
   * @param miriamType
   *          obejct to add
   */
  public void addValidMiriamType(MiriamType miriamType) {
    validMiriamTypes.add(miriamType);
  }

  /**
   * @return the annotationSchema
   * @see #annotationSchema
   */
  public UserAnnotationSchema getAnnotationSchema() {
    return annotationSchema;
  }

  /**
   * @param annotationSchema
   *          the annotationSchema to set
   * @see #annotationSchema
   */
  public void setAnnotationSchema(UserAnnotationSchema annotationSchema) {
    this.annotationSchema = annotationSchema;
  }
}
