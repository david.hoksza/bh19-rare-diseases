package lcsb.mapviewer.model.map.compartment;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;

/**
 * This class defines compartment that covers right part of the model up to some
 * border on the left side.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("RIGHT_SQUARE_COMPARTMENT")
public class RightSquareCompartment extends Compartment {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(RightSquareCompartment.class);

  /**
   * Empty constructor required by hibernate.
   */
  RightSquareCompartment() {
    super();
  }

  /**
   * Constructor that creates a compartment with the new shape but takes the
   * reference data from the compartment given as parameter.
   * 
   * @param original
   *          original compartment where the data was kept
   * @param model
   *          model object to which the compartment will be assigned
   */
  public RightSquareCompartment(Compartment original, Model model) {
    super(original);
    setX(0.0);
    setWidth(model.getWidth() * 2);
    setY(0.0);
    setHeight(model.getHeight() * 2);
  }

  /**
   * Constructor that creates a compartment with the new shape and takes the
   * reference data from the compartment given as parameter.
   *
   * @param original
   *          original compartment where the data was kept
   */
  public RightSquareCompartment(RightSquareCompartment original) {
    super(original);
  }

  /**
   * Default constructor.
   *
   * @param elementId
   *          identifier of the compartment
   */
  public RightSquareCompartment(String elementId) {
    setElementId(elementId);
  }

  /**
   * Sets the CellDesigner point coordinates. In the implementation of
   * BottomSquare it should define left border.
   *
   * @param y
   *          to be ignored
   * @param x
   *          left border
   *
   */
  public void setPoint(String x, String y) {
    // set left border
    setX(x);
    setWidth(getWidth() - getX());
  }

  @Override
  public RightSquareCompartment copy() {
    if (this.getClass() == RightSquareCompartment.class) {
      return new RightSquareCompartment(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
