/**
 * Provides data structure for statistics about the usage of the map.
 */
package lcsb.mapviewer.model.map.statistics;
