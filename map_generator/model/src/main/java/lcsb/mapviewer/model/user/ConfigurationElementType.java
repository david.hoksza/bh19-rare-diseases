package lcsb.mapviewer.model.user;

import lcsb.mapviewer.model.security.PrivilegeType;

/**
 * This enumerate defines all possible configuration parameter that are
 * configurable by the user.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ConfigurationElementType {

  /**
   * Email address used for sending email from the system.
   */
  EMAIL_ADDRESS("E-mail address", "your.account@domain.com", ConfigurationElementEditType.EMAIL, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION),

  /**
   * Login for the email account.
   */
  EMAIL_LOGIN("E-mail server login", "your@login", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION),

  /**
   * Password for the email account.
   */
  EMAIL_PASSWORD("E-mail server password", "email.secret.password", ConfigurationElementEditType.PASSWORD, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION),

  /**
   * Address of the IMAP server.
   */
  EMAIL_IMAP_SERVER("IMAP server", "your.imap.domain.com", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION),

  /**
   * Address of the SMTP server.
   */
  EMAIL_SMTP_SERVER("SMTP server", "your.smtp.domain.com", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION),

  /**
   * Port used for SMTP connection (sending e-mails).
   */
  EMAIL_SMTP_PORT("SMTP port", "25", ConfigurationElementEditType.INTEGER, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION),

  /**
   * MinervaNet endpoint
   */
  MINERVANET_URL("Issue tracker", "https://minerva-net.lcsb.uni.lu/issues",
      ConfigurationElementEditType.STRING, true, ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  /**
   * Default map that should be presented if no map is selected by user side.
   */
  DEFAULT_MAP("Default Project Id", "empty", ConfigurationElementEditType.STRING, false,
      ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  /**
   * Left logo presented in the system.
   */
  LEFT_LOGO_IMG("Left logo icon", "resources/images/udl.png", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * Address connected to the left logo.
   */
  LEFT_LOGO_LINK("Left logo link (after click)", "http://wwwen.uni.lu/", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * Description of the left logo presented in the system.
   */
  LEFT_LOGO_TEXT("Left logo description", "University of Luxembourg", ConfigurationElementEditType.STRING, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * Right logo presented in the system.
   */
  RIGHT_LOGO_IMG("Right logo icon", "resources/images/lcsb.png", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * Address connected to the right logo.
   */
  RIGHT_LOGO_LINK("Right logo link (after click)", "http://wwwen.uni.lu/lcsb/", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * Description of the right logo presented in the system.
   */
  RIGHT_LOGO_TEXT("Right logo description", "LCSB - Luxembourg Centre for Systems Biomedicine",
      ConfigurationElementEditType.STRING, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * Maximum distance (in pixels) that is allowed during finding closest element
   * on the map.
   */
  SEARCH_DISTANCE("Max distance for clicking on element (px)", "10", ConfigurationElementEditType.DOUBLE, false,
      ConfigurationElementTypeGroup.POINT_AND_CLICK),

  /**
   * Email used for requesting an account (in client side).
   */
  REQUEST_ACCOUNT_EMAIL("Email used for requesting an account", "",
      ConfigurationElementEditType.EMAIL, false, ConfigurationElementTypeGroup.EMAIL_NOTIFICATION),

  /**
   * Max number of results in search box.
   */
  SEARCH_RESULT_NUMBER(
      "Max number of results (this value indicates the max number of elements that will be returned from search - not the number of aggregated elements in the search box).",
      "100", ConfigurationElementEditType.INTEGER, false, ConfigurationElementTypeGroup.POINT_AND_CLICK),

  /**
   * Google Analytics tracking ID used for statistics. This tracking ID should
   * look like "UA-000000-01". More information about tracking ID can be found
   * <a href="https://support.google.com/analytics/answer/1032385?hl=en"> here
   * </a>.
   */
  GOOGLE_ANALYTICS_IDENTIFIER("Google Analytics tracking ID used for statistics", "",
      ConfigurationElementEditType.STRING, false, ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  /**
   * Domain allowed to connect via x-frame technology.
   */
  X_FRAME_DOMAIN("Domain allowed to connect via x-frame technology", "", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  /**
   * Domain allowed to connect via x-frame technology.
   */
  CORS_DOMAIN("Disable CORS (when disabled 'ORIGIN' http header is required)", "false",
      ConfigurationElementEditType.BOOLEAN, false, ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  /**
   * Relative directory (in webapps folder) where big files will be stored.
   */
  BIG_FILE_STORAGE_DIR("Path to store big files", "minerva-big/", ConfigurationElementEditType.STRING, false,
      ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  /**
   * File where legend 1/4 is stored.
   */
  LEGEND_FILE_1("Legend 1 image file", "resources/images/legend_a.png", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * File where legend 2/4 is stored.
   */
  LEGEND_FILE_2("Legend 2 image file", "resources/images/legend_b.png", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * File where legend 3/4 is stored.
   */
  LEGEND_FILE_3("Legend 3 image file", "resources/images/legend_c.png", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * File where legend 4/4 is stored.
   */
  LEGEND_FILE_4("Legend 4 image file", "resources/images/legend_d.png", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * File where legend 4/4 is stored.
   */
  USER_MANUAL_FILE("User manual file", "resources/other/user_guide.pdf", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * Color used for negative overlay values.
   */
  MIN_COLOR_VAL("Overlay color for negative values", "FF0000", ConfigurationElementEditType.COLOR, false,
      ConfigurationElementTypeGroup.OVERLAYS),

  /**
   * Color used for positive overlay values.
   */
  MAX_COLOR_VAL("Overlay color for postive values", "0000FF", ConfigurationElementEditType.COLOR, false,
      ConfigurationElementTypeGroup.OVERLAYS),

  /**
   * Color used for undefined overlay values.
   */
  SIMPLE_COLOR_VAL("Overlay color when no values are defined", "00FF00", ConfigurationElementEditType.COLOR, false,
      ConfigurationElementTypeGroup.OVERLAYS),

  /**
   * Color used for 0 overlay value.
   */
  NEUTRAL_COLOR_VAL("Overlay color for value=0", "FFFFFF", ConfigurationElementEditType.COLOR, false,
      ConfigurationElementTypeGroup.OVERLAYS),

  /**
   * Opacity of data overlay objects in the frontend.
   */
  OVERLAY_OPACITY("Opacity used when drawing data overlays (value between 0.0-1.0)", "0.8",
      ConfigurationElementEditType.DOUBLE, false, ConfigurationElementTypeGroup.OVERLAYS),

  /**
   * Default content of the email when requesting for an account in the system.
   */
  REQUEST_ACCOUNT_DEFAULT_CONTENT("Email content used for requesting an account",
      "Dear Disease map team,\nI would like to request an account in the system.\nKind regards",
      ConfigurationElementEditType.TEXT, false, ConfigurationElementTypeGroup.EMAIL_NOTIFICATION),

  DEFAULT_READ_PROJECT("Default user privilege for: " + PrivilegeType.READ_PROJECT.getDescription(), "true",
      ConfigurationElementEditType.BOOLEAN, false, ConfigurationElementTypeGroup.DEFAULT_USER_PRIVILEGES),

  DEFAULT_WRITE_PROJECT("Default user privilege for: " + PrivilegeType.WRITE_PROJECT.getDescription(), "false",
      ConfigurationElementEditType.BOOLEAN, false, ConfigurationElementTypeGroup.DEFAULT_USER_PRIVILEGES),

  DEFAULT_CAN_CREATE_OVERLAYS("Default user privilege for: " + PrivilegeType.CAN_CREATE_OVERLAYS.getDescription(),
      "true",
      ConfigurationElementEditType.BOOLEAN, false, ConfigurationElementTypeGroup.DEFAULT_USER_PRIVILEGES),

  SHOW_REACTION_TYPE("Show reaction type", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  GOOGLE_MAPS_API_KEY("By providing this Google Maps Platform API key I declare that I am aware that "
      + "I am a Customer of the Google Maps Platform and I agree to the terms of the <a href=\"https://cloud.google.com/maps-platform/terms/\"  target='_blank'>license of Google Maps Platform</a>."
      + "In particular, I warrant that neither any of the maps nor publicly available data overlays "
      + "(\"General overlays\") on this MINERVA server contain Protected Health Information (as defined in and subject to HIPAA).",
      "", ConfigurationElementEditType.STRING, false, ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  /**
   * Terms of use.
   */
  TERMS_OF_USE("URL of platform's Terms of Service", "", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  LDAP_ADDRESS("LDAP address", "", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),
  LDAP_PORT("LDAP port", "389", ConfigurationElementEditType.INTEGER, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),
  LDAP_SSL("LDAP uses SSL", "false", ConfigurationElementEditType.BOOLEAN, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),
  LDAP_BIND_DN("LDAP bind DN", "", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),
  LDAP_PASSWORD("LDAP password", "", ConfigurationElementEditType.PASSWORD, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),
  LDAP_BASE_DN("LDAP base DN", "", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),
  LDAP_OBJECT_CLASS("LDAP filter objectClass", "*", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),
  LDAP_FIRST_NAME_ATTRIBUTE("LDAP first name attribute", "givenName", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),
  LDAP_LAST_NAME_ATTRIBUTE("LDAP last name attribute", "sn", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),
  LDAP_EMAIL_ATTRIBUTE("LDAP email attribute", "mail", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),

  LDAP_FILTER("LDAP filter", "(memberof=cn=minerva,cn=groups,cn=accounts,dc=uni,dc=lu)",
      ConfigurationElementEditType.STRING, true, ConfigurationElementTypeGroup.LDAP_CONFIGURATION),

  LDAP_UID("LDAP login (user id)", "uid", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),

  SHOW_REACTION_TITLE("Show reaction title", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_LINKED_SUBMAP("Show linked submap for reaction", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_SYMBOL("Show reaction symbol", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_ABBREVIATION("Show reaction abbreviation", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_FORMULA("Show reaction formula", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_MECHANICAL_CONFIDENCE_SCORE("Show reaction mechanical confidence score", "true",
      ConfigurationElementEditType.BOOLEAN, false, ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_LOWER_BOUND("Show reaction lower bound", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_UPPER_BOUND("Show reaction upper bound", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_GENE_PROTEIN_REACTION("Show reaction gene protein reaction", "true",
      ConfigurationElementEditType.BOOLEAN, false, ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_SUBSYSTEM("Show reaction subsystem", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_SYNONYMS("Show reaction synonyms", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_DESCRIPTION("Show reaction description", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_REACTION_ANNOTATIONS("Show reaction annotations", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_TYPE("Show element type", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_TITLE("Show element title", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_LINKED_SUBMAP("Show linked submap for element", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_GROUP_SIZE("Show element group size", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_COMPARTMENT("Show element compartment", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_FULL_NAME("Show element full name", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_SYMBOL("Show element symbol", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_ABBREVIATION("Show element abbreviation", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_FORMULA("Show element formula", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_FORMER_SYMBOLS("Show element former symbol", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_MODIFICATIONS("Show element modifications", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_CHARGE("Show element charge", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_SYNONYMS("Show element synonyms", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_DESCRIPTION("Show element description", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SHOW_ELEMENT_ANNOTATIONS("Show element annotations", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.SEARCH_VISIBLE_PARAMETERS),

  SESSION_LENGTH("Max session inactivity time (in seconds)", "7200", ConfigurationElementEditType.INTEGER, false,
      ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  MAX_NUMBER_OF_MAP_LEVELS("Max number of map zoom levels", "10", ConfigurationElementEditType.INTEGER, false,
      ConfigurationElementTypeGroup.POINT_AND_CLICK),

  ;

  /**
   * Default value of the configuration parameter (it will be used only when value
   * doesn't exist in the DAO).
   */
  private String defaultValue;

  /**
   * Common name used for visualization (query user).
   */
  private String commonName;

  /**
   * How we want to edit specific parameter.
   */
  private ConfigurationElementEditType editType;

  private boolean serverSide;
  private ConfigurationElementTypeGroup group;

  /**
   * Default constructor.
   *
   * @param commonName
   *          common name used for this parameter
   * @param editType
   *          type defining how we want to edit this configuration parameter
   * @param defaultVal
   *          default value assigned to this parameter
   */
  ConfigurationElementType(String commonName, String defaultVal, ConfigurationElementEditType editType,
      boolean serverSide, ConfigurationElementTypeGroup group) {
    this.defaultValue = defaultVal;
    this.commonName = commonName;
    this.editType = editType;
    this.serverSide = serverSide;
    this.group = group;
  }

  /**
   * @return the defaultValue
   * @see #defaultValue
   */
  public String getDefaultValue() {
    return defaultValue;
  }

  /**
   * @return the commonName
   * @see #commonName
   */
  public String getCommonName() {
    return commonName;
  }

  /**
   * @return the editType
   * @see #editType
   */
  public ConfigurationElementEditType getEditType() {
    return editType;
  }

  /**
   * @return the serverSide
   * @see #serverSide
   */
  public boolean isServerSide() {
    return serverSide;
  }

  public ConfigurationElementTypeGroup getGroup() {
    return group;
  }
}
