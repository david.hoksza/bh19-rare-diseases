package lcsb.mapviewer.model.map.kinetics;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.map.model.ModelData;

/**
 * Representation of a single SBML unit
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@XmlRootElement
public class SbmlUnit implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SbmlUnit.class);
  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String unitId;
  private String name;

  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "unit", orphanRemoval = true)
  private Set<SbmlUnitTypeFactor> unitTypeFactors = new HashSet<>();

  /**
   * Map model object to which unit belongs to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData model;

  /**
   * Constructor required by hibernate.
   */
  SbmlUnit() {
    super();
  }

  public SbmlUnit(String unitId) {
    this.unitId = unitId;
  }

  public SbmlUnit(SbmlUnit sbmlUnit) {
    this(sbmlUnit.getUnitId());
    this.setName(sbmlUnit.getName());
    for (SbmlUnitTypeFactor factor : sbmlUnit.getUnitTypeFactors()) {
      this.addUnitTypeFactor(factor.copy());
    }
  }

  public String getUnitId() {
    return unitId;
  }

  public void setUnitId(String unitId) {
    this.unitId = unitId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void addUnitTypeFactor(SbmlUnitTypeFactor factor) {
    unitTypeFactors.add(factor);
    factor.setUnit(this);
  }

  public Set<SbmlUnitTypeFactor> getUnitTypeFactors() {
    return unitTypeFactors;
  }

  public ModelData getModel() {
    return model;
  }

  public void setModel(ModelData model) {
    this.model = model;
  }

  public SbmlUnit copy() {
    return new SbmlUnit(this);
  }

  public int getId() {
    return id;
  }

}
