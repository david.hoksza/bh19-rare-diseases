package lcsb.mapviewer.model.map;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

/**
 * Representation of a single species or reaction annotation. Miriam format is a
 * standard described <a href="http://www.ebi.ac.uk/miriam/main/">here</a>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@XmlRootElement
public class MiriamData implements Comparable<MiriamData>, Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(MiriamData.class);
  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * What is the connection between element and the annotation.
   */
  private MiriamRelationType relationType = null;

  /**
   * Type of database.
   */
  @Enumerated(EnumType.STRING)
  private MiriamType dataType;

  /**
   * Resource identifier in the database.
   */
  private String resource = "";

  /**
   * The annotator which created the miriam data or which should be associated
   * with it.
   */
  private Class<?> annotator = null;

  /**
   * Default constructor.
   */
  public MiriamData() {
    super();
  }

  /**
   * Constructor that initialize the data by information from param.
   * 
   * @param md
   *          original miriam data object
   */
  public MiriamData(MiriamData md) {
    setRelationType(md.relationType);
    setDataType(md.dataType);
    setResource(md.resource);
    setAnnotator(md.annotator);
  }

  /**
   * Constructor that initialize the data by information from params.
   * 
   * @param relationType
   *          {@link #relationType}
   * @param mt
   *          type of the miriam data (see: {@link MiriamType})
   * @param resource
   *          {@link #resource}
   * @param annotator
   *          {@link #annotator}
   * 
   */
  public MiriamData(MiriamRelationType relationType, MiriamType mt, String resource, Class<?> annotator) {
    if (mt == null) {
      throw new InvalidArgumentException("MiriamType cannot be null");
    }
    if (relationType == null) {
      throw new InvalidArgumentException("MiriamRelationType cannot be null");
    }
    setRelationType(relationType);
    setDataType(mt);
    setResource(resource);
    setAnnotator(annotator);
  }

  /**
   * Constructor that initialize the data by information from params.
   * 
   * @param relationType
   *          {@link #relationType}
   * @param mt
   *          type of the miriam data (see: {@link MiriamType})
   * @param resource
   *          {@link #resource}
   */
  public MiriamData(MiriamRelationType relationType, MiriamType mt, String resource) {
    this(relationType, mt, resource, null);
  }

  /**
   * Constructor that initialize the data by information from params.
   * 
   * @param mt
   *          type of the miriam data (see: {@link MiriamType})
   * @param resource
   *          {@link #resource}
   */
  public MiriamData(MiriamType mt, String resource, Class<?> annotator) {
    this(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, mt, resource, annotator);
  }

  /**
   * Constructor that initialize the data by information from params.
   * 
   * @param mt
   *          type of the miriam data (see: {@link MiriamType})
   * @param resource
   *          {@link #resource}
   */
  public MiriamData(MiriamType mt, String resource) {
    this(mt, resource, null);
  }

  /**
   * @param identifier
   *          string represents identifier.
   * @return {@link #resource}
   */
  public static String getIdFromIdentifier(String identifier) {
    if (identifier != null) {
      int index = identifier.indexOf(":");
      if (index != -1) {
        identifier = identifier.trim().substring(index + 1);
      }
    }
    return identifier;
  }

  /**
   *
   * @return {@link #resource}
   */
  public String getResource() {
    return resource;
  }

  /**
   * Sets new {@link #resource}.
   * 
   * @param resource
   *          new {@link #resource}
   */
  public void setResource(String resource) {
    if (resource == null) {
      this.resource = null;
    } else {
      this.resource = resource.replace("%3A", ":");
    }
  }

  @Override
  public int hashCode() {
    return (dataType + resource).hashCode();
  }

  @Override
  public boolean equals(Object aThat) {
    // check for self-comparison
    if (this == aThat) {
      return true;
    }

    // use instanceof instead of getClass here for two reasons
    // 1. if need be, it can match any supertype, and not just one class;
    // 2. it renders an explict check for "that == null" redundant, since
    // it does the check for null already - "null instanceof [type]" always
    // returns false. (See Effective Java by Joshua Bloch.)
    if (!(aThat instanceof MiriamData)) {
      return false;
    }
    // Alternative to the above line :
    // if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

    // cast to native object is now safe
    MiriamData that = (MiriamData) aThat;

    // now a proper field-by-field evaluation can be made
    return this.compareTo(that) == 0;
  }

  @Override
  public String toString() {
    String annotatorClass = annotator != null ? ":" + annotator.getName() : "";
    if (relationType != null) {
      return "[" + relationType.getStringRepresentation() + "] " + dataType + ":" + resource + annotatorClass;
    } else {
      return "[UNKNOWN] " + dataType + ":" + resource + annotatorClass;

    }
  }

  @Override
  public int compareTo(MiriamData other) {
    String name = annotator != null ? annotator.getName() : "";
    String otherName = other.annotator != null ? other.annotator.getName() : "";
    return (dataType + ":" + resource + ":" + name).toLowerCase()
        .compareTo((other.dataType + ":" + other.resource + ":" + otherName).toLowerCase());
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the dataType
   * @see #dataType
   */
  public MiriamType getDataType() {
    return dataType;
  }

  /**
   * @param dataType
   *          the dataType to set
   * @see #dataType
   */
  public void setDataType(MiriamType dataType) {
    this.dataType = dataType;
  }

  /**
   * @return the relationType
   * @see #relationType
   */
  public MiriamRelationType getRelationType() {
    return relationType;
  }

  /**
   * @param relationType
   *          the relationType to set
   * @see #relationType
   */
  public void setRelationType(MiriamRelationType relationType) {
    this.relationType = relationType;
  }

  /**
   *
   * @return {@link #annotator}
   */
  public Class<?> getAnnotator() {
    return annotator;
  }

  /**
   *
   * @param {@link
   *          #annotator}
   */
  public void setAnnotator(Class<?> annotator) {
    this.annotator = annotator;
  }
}
