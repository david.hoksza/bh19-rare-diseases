package lcsb.mapviewer.model.map.species.field;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.*;

/**
 * This structure contains information about Modification Site for one of the
 * following {@link Species}:
 * <ul>
 * <li>{@link Rna}</li>
 * <li>{@link AntisenseRna}</li>
 * <li>{@link Gene}</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("MODIFICATION_SITE")
public class ModificationSite extends AbstractSiteModification {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public ModificationSite() {
    super();
  }

  public ModificationSite(ModificationSite site) {
    super(site);
  }

  /**
   * Creates copy of the object.
   * 
   * @return copy of the object.
   */
  public ModificationSite copy() {
    if (this.getClass() == ModificationSite.class) {
      return new ModificationSite(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
