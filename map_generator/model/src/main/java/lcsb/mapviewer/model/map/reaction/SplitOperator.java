package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Class representing "split operator" (dissociation???) of one node into two or
 * more nodes in the reaction (in the product part of the reaction).
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("SPLIT_OPERATOR_NODE")
public class SplitOperator extends NodeOperator {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SplitOperator.class);

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param operator
   *          parent operator from which we copy data
   */
  public SplitOperator(SplitOperator operator) {
    super(operator);
  }

  /**
   * Default constructor.
   */
  public SplitOperator() {
    super();
  }

  @Override
  public String getOperatorText() {
    return "";
  }

  @Override
  public SplitOperator copy() {
    if (this.getClass() == SplitOperator.class) {
      return new SplitOperator(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
