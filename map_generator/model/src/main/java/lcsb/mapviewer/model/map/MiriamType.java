package lcsb.mapviewer.model.map;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;

/**
 * Type of known miriam annotation.
 * 
 * @author Piotr Gawron
 * 
 */
@SuppressWarnings("unchecked")
public enum MiriamType {

  BiGG_COMPARTMENT("BiGG Compartment",
      "http://bigg.ucsd.edu/compartments/",
      new String[] { "urn:miriam:bigg.compartment" },
      new Class<?>[] {},
      "MIR:00000555",
      new Class<?>[] {},
      "bigg.compartment",
      "c"),

  BiGG_METABOLITE("BiGG Metabolite",
      "http://bigg.ucsd.edu/universal/metabolites",
      new String[] { "urn:miriam:bigg.metabolite" },
      new Class<?>[] {}, "MIR:00000556",
      new Class<?>[] {},
      "bigg.metabolite",
      "12dgr161"),

  BiGG_REACTIONS("BiGG Reaction",
      "http://bigg.ucsd.edu/universal/reactions",
      new String[] { "urn:miriam:bigg.reaction" },
      new Class<?>[] {}, "MIR:00000557",
      new Class<?>[] {},
      "bigg.reaction",
      "13GS"),

  BIOMODELS_DATABASE("BioModels Database",
      "https://www.ebi.ac.uk/biomodels/",
      new String[] { "urn:miriam:biomodels.db", "http://identifiers.org/biomodels.db/" },
      new Class<?>[] {}, "MIR:00000007",
      new Class<?>[] {},
      "biomodels.db",
      "BIOMD0000000048"),

  /**
   * Brenda enzyme database: http://www.brenda-enzymes.org.
   */
  BRENDA("BRENDA",
      "http://www.brenda-enzymes.org",
      new String[] { "urn:miriam:brenda" },
      new Class<?>[] {}, "MIR:00000071",
      new Class<?>[] {},
      "brenda",
      "1.1.1.1"),

  /**
   * Chemical Abstracts Service database: http://commonchemistry.org.
   */
  CAS("Chemical Abstracts Service",
      "http://commonchemistry.org",
      new String[] { "urn:miriam:cas" },
      new Class<?>[] {}, "MIR:00000237",
      new Class<?>[] {},
      "cas",
      "50-00-0"),

  /**
   * The Carbohydrate-Active Enzyme (CAZy) database: http://www.cazy.org/.
   */
  CAZY("CAZy",
      "http://commonchemistry.org",
      new String[] { "urn:miriam:cazy" },
      new Class<?>[] {}, "MIR:00000195",
      new Class<?>[] {},
      "cazy",
      "GT10"),

  /**
   * Consensus CDS: http://identifiers.org/ccds/.
   */
  CCDS("Consensus CDS",
      "http://www.ncbi.nlm.nih.gov/CCDS/",
      new String[] { "urn:miriam:ccds" },
      new Class<?>[] {}, "MIR:00000375",
      new Class<?>[] {},
      "ccds",
      "CCDS13573.1"),

  /**
   * Chebi database:
   * <a href = "http://www.ebi.ac.uk/chebi/">http://www.ebi.ac.uk/chebi/</a>.
   */
  CHEBI("Chebi",
      "http://www.ebi.ac.uk/chebi/",
      new String[] { "urn:miriam:obo.chebi", "urn:miriam:chebi",
          "http://identifiers.org/chebi/", "http://identifiers.org/obo.chebi/" },
      new Class<?>[] { Chemical.class, Drug.class, }, "MIR:00000002",
      new Class<?>[] { Chemical.class },
      "",
      "CHEBI:36927"),

  /**
   * ChemSpider database:
   * <a href = "http://www.chemspider.com/">http://www.chemspider.com/</a>.
   */
  CHEMSPIDER("ChemSpider",
      "http://www.chemspider.com//",
      new String[] { "urn:miriam:chemspider" },
      new Class<?>[] {}, "MIR:00000138",
      new Class<?>[] {},
      "chemspider",
      "56586"),

  /**
   * Chembl database: https://www.ebi.ac.uk/chembldb/.
   */
  CHEMBL_COMPOUND("ChEMBL",
      "https://www.ebi.ac.uk/chembldb/",
      new String[] { "urn:miriam:chembl.compound" },
      new Class<?>[] { Drug.class }, "MIR:00000084",
      new Class<?>[] {},
      "chembl.compound",
      "CHEMBL308052"),

  /**
   * Target in chembl database: https://www.ebi.ac.uk/chembldb/.
   */
  CHEMBL_TARGET("ChEMBL target",
      "https://www.ebi.ac.uk/chembldb/",
      new String[] { "urn:miriam:chembl.target" },
      new Class<?>[] { Protein.class, Complex.class }, "MIR:00000085",
      new Class<?>[] {},
      "chembl.target",
      "CHEMBL3467"),

  CLINICAL_TRIALS_GOV("ClinicalTrials.gov",
      "https://clinicaltrials.gov/",
      new String[] { "urn:miriam:clinicaltrials" },
      new Class<?>[] {}, "MIR:00000137",
      new Class<?>[] {},
      "clinicaltrials",
      "NCT00222573"),

  /**
   * Clusters of Orthologous Groups: https://www.ncbi.nlm.nih.gov/COG/.
   */
  @Deprecated
  COG("Clusters of Orthologous Groups",
      "https://www.ncbi.nlm.nih.gov/COG/",
      new String[] { "urn:miriam:cogs" },
      new Class<?>[] { Reaction.class }, "MIR:00000296",
      new Class<?>[] {},
      "cogs",
      "COG0001"),

  /**
   * Digital Object Identifier: http://www.doi.org/.
   */
  DOI("Digital Object Identifier",
      "http://www.doi.org/",
      new String[] { "urn:miriam:doi", "http://identifiers.org/doi/" },
      new Class<?>[] { Reaction.class }, "MIR:00000019",
      new Class<?>[] {},
      "doi",
      "10.1038/nbt1156"),

  /**
   * Drugbank database: http://www.drugbank.ca/.
   */
  DRUGBANK("DrugBank",
      "http://www.drugbank.ca/",
      new String[] { "urn:miriam:drugbank" },
      new Class<?>[] { Drug.class }, "MIR:00000102",
      new Class<?>[] {},
      "drugbank",
      "DB00001"),

  /**
   * Drugbank targets: http://www.drugbank.ca/targets.
   */
  DRUGBANK_TARGET_V4("DrugBank Target v4",
      "http://www.drugbank.ca/targets",
      new String[] { "urn:miriam:drugbankv4.target" },
      new Class<?>[] {}, "MIR:00000528",
      new Class<?>[] {},
      "drugbankv4.target",
      "BE0000048"),

  /**
   * Enzyme Nomenclature: http://www.enzyme-database.org/.
   */
  EC("Enzyme Nomenclature",
      "http://www.enzyme-database.org/",
      new String[] { "urn:miriam:ec-code" },
      new Class<?>[] { Protein.class, Complex.class }, "MIR:00000004",
      new Class<?>[] {},
      "ec-code",
      "1.1.1.1"),

  /**
   * Ensembl: www.ensembl.org.
   */
  ENSEMBL("Ensembl",
      "www.ensembl.org",
      new String[] { "urn:miriam:ensembl", "http://identifiers.org/ensembl/" },
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000003",
      new Class<?>[] {},
      "ensembl",
      "ENSG00000139618"),

  /**
   * Ensembl Plants: http://plants.ensembl.org/.
   */
  ENSEMBL_PLANTS("Ensembl Plants",
      "http://plants.ensembl.org/",
      new String[] { "urn:miriam:ensembl.plant" },
      new Class<?>[] {}, "MIR:00000205",
      new Class<?>[] {},
      "ensembl.plant",
      "AT1G73965"),

  /**
   * Entrez Gene: http://www.ncbi.nlm.nih.gov/gene.
   */
  ENTREZ("Entrez Gene",
      "http://www.ncbi.nlm.nih.gov/gene",
      new String[] { "urn:miriam:ncbigene", "urn:miriam:entrez.gene" },
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000069",
      new Class<?>[] {},
      "ncbigene",
      "100010"),

  /**
   * Gene Ontology: http://amigo.geneontology.org/amigo.
   */
  GO("Gene Ontology",
      "http://amigo.geneontology.org/amigo",
      new String[] { "urn:miriam:obo.go", "urn:miriam:go", "http://identifiers.org/go/",
          "http://identifiers.org/obo.go/" },
      new Class<?>[] { Phenotype.class, Compartment.class, Complex.class }, "MIR:00000022",
      new Class<?>[] {},
      "",
      "GO:0006915"),

  /**
   * HGNC: http://www.genenames.org.
   */
  HGNC("HGNC",
      "http://www.genenames.org",
      new String[] { "urn:miriam:hgnc" },
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000080",
      new Class<?>[] { Protein.class, Gene.class, Rna.class },
      "hgnc",
      "2674"),

  /**
   * HGNC symbol: http://www.genenames.org.
   */
  HGNC_SYMBOL("HGNC Symbol",
      "http://www.genenames.org",
      new String[] { "urn:miriam:hgnc.symbol" },
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000362",
      new Class<?>[] { Protein.class, Gene.class, Rna.class },
      "hgnc.symbol",
      "DAPK1"),

  /**
   * HMDB: http://www.hmdb.ca/.
   */
  HMDB("HMDB",
      "http://www.hmdb.ca/",
      "urn:miriam:hmdb",
      new Class<?>[] { Chemical.class, Drug.class, }, "MIR:00000051",
      new Class<?>[] {},
      "hmdb",
      "HMDB00001"),

  /**
   * InterPro: http://www.ebi.ac.uk/interpro/.
   */
  INTERPRO("InterPro",
      "http://www.ebi.ac.uk/interpro/",
      new String[] { "urn:miriam:interpro", "http://identifiers.org/interpro/" },
      new Class<?>[] { Protein.class, Complex.class }, "MIR:00000011",
      new Class<?>[] {},
      "interpro",
      "IPR000100"),

  /**
   * KEGG Compound: http://www.genome.jp/kegg/ligand.html.
   */
  KEGG_COMPOUND("Kegg Compound",
      "http://www.genome.jp/kegg/ligand.html",
      new String[] { "urn:miriam:kegg.compound", "http://identifiers.org/kegg.compound/" },
      new Class<?>[] { Chemical.class }, "MIR:00000013",
      new Class<?>[] {},
      "kegg.compound",
      "C12345"),

  /**
   * KEGG Genes: http://www.genome.jp/kegg/genes.html.
   */
  KEGG_GENES("Kegg Genes",
      "http://www.genome.jp/kegg/genes.html",
      new String[] { "urn:miriam:kegg.genes" },
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000070",
      new Class<?>[] {},
      "kegg.genes",
      "syn:ssr3451"),

  /**
   * KEGG Orthology: http://www.genome.jp/kegg/ko.html.
   */
  KEGG_ORTHOLOGY("KEGG Orthology",
      "http://www.genome.jp/kegg/ko.html",
      new String[] { "urn:miriam:kegg.orthology" },
      new Class<?>[] {}, "MIR:00000116",
      new Class<?>[] {},
      "kegg.orthology",
      "K00001"),

  /**
   * KEGG Pathway: http://www.genome.jp/kegg/pathway.html.
   */
  KEGG_PATHWAY("Kegg Pathway",
      "http://www.genome.jp/kegg/pathway.html",
      new String[] { "urn:miriam:kegg.pathway", "http://identifiers.org/kegg.pathway/" },
      new Class<?>[] { Reaction.class }, "MIR:00000012",
      new Class<?>[] {},
      "kegg.pathway",
      "hsa00620"),

  /**
   * KEGG Reaction: http://www.genome.jp/kegg/reaction/.
   */
  KEGG_REACTION("Kegg Reaction",
      "http://www.genome.jp/kegg/reaction/",
      new String[] { "urn:miriam:kegg.reaction", "http://identifiers.org/kegg.reaction/" },
      new Class<?>[] { Reaction.class }, "MIR:00000014",
      new Class<?>[] {},
      "kegg.reaction",
      "R00100"),

  /**
   * MeSH 2012: http://www.nlm.nih.gov/mesh/.
   */
  MESH_2012("MeSH",
      "http://www.nlm.nih.gov/mesh/",
      new String[] { "urn:miriam:mesh", "urn:miriam:mesh.2012" },
      new Class<?>[] { Phenotype.class, Compartment.class, Complex.class }, "MIR:00000560",
      new Class<?>[] {},
      "mesh",
      "D010300"),

  /**
   * miRBase Sequence: http://www.mirbase.org/.
   */
  MI_R_BASE_SEQUENCE("miRBase Sequence Database",
      "http://www.mirbase.org/",
      new String[] { "urn:miriam:mirbase" },
      new Class<?>[] {}, "MIR:00000078",
      new Class<?>[] {},
      "mirbase",
      "MI0000001"),

  /**
   * miRBase Mature Sequence: http://www.mirbase.org/.
   */
  MI_R_BASE_MATURE_SEQUENCE("miRBase Mature Sequence Database",
      "http://www.mirbase.org/",
      new String[] { "urn:miriam:mirbase.mature" },
      new Class<?>[] {}, "MIR:00000235",
      new Class<?>[] {},
      "mirbase.mature",
      "MIMAT0000001"),

  /**
   * miRTaRBase Mature Sequence: http://mirtarbase.mbc.nctu.edu.tw/.
   */
  MIR_TAR_BASE_MATURE_SEQUENCE("miRTarBase Mature Sequence Database",
      "http://mirtarbase.mbc.nctu.edu.tw/",
      new String[] { "urn:miriam:mirtarbase" },
      new Class<?>[] {}, "MIR:00000562",
      new Class<?>[] {},
      "mirtarbase",
      "MIRT000002"),

  /**
   * Mouse Genome Database: http://www.informatics.jax.org/.
   */
  MGD("Mouse Genome Database",
      "http://www.informatics.jax.org/",
      new String[] { "urn:miriam:mgd" },
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000037",
      new Class<?>[] {},
      "",
      "MGI:2442292"),

  OBI("Ontology for Biomedical Investigations",
      "https://www.ebi.ac.uk/ols/ontologies/obi/",
      new String[] { "urn:miriam:obi", "http://identifiers.org/obi/" },
      new Class<?>[] {}, "MIR:00000127",
      new Class<?>[] {},
      "obi",
      "OBI:0000070"),

  /**
   * Online Mendelian Inheritance in Man: http://omim.org/.
   */
  OMIM("Online Mendelian Inheritance in Man",
      "http://omim.org/",
      new String[] { "urn:miriam:omim", "http://identifiers.org/mim/" },
      new Class<?>[] { Phenotype.class }, "MIR:00000016",
      new Class<?>[] {},
      "mim",
      "603903"),

  /**
   * PANTHER Family: http://www.pantherdb.org/.
   */
  PANTHER("PANTHER Family",
      "http://www.pantherdb.org/",
      new String[] { "urn:miriam:panther.family", "urn:miriam:panther" },
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000060",
      new Class<?>[] {},
      "panther.family",
      "PTHR12345"),

  /**
   * PDB: http://www.pdbe.org/.
   */
  PDB("Protein Data Bank",
      "http://www.pdbe.org/",
      "urn:miriam:pdb",
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000020",
      new Class<?>[] {},
      "pdb",
      "2gc4"),

  /**
   * Protein Family Database: http://pfam.xfam.org/.
   */
  PFAM("Protein Family Database",
      "http://pfam.xfam.org//",
      "urn:miriam:pfam",
      new Class<?>[] {}, "MIR:00000028",
      new Class<?>[] {},
      "pfam",
      "PF01234"),

  /**
   * PharmGKB Pathways: http://www.pharmgkb.org/.
   */
  PHARM("PharmGKB Pathways",
      "http://www.pharmgkb.org/",
      "urn:miriam:pharmgkb.pathways",
      new Class<?>[] {}, "MIR:00000089",
      new Class<?>[] {},
      "pharmgkb.pathways",
      "PA146123006"),

  /**
   * PubChem-compound: http://pubchem.ncbi.nlm.nih.gov/.
   */
  PUBCHEM("PubChem-compound",
      "http://pubchem.ncbi.nlm.nih.gov/",
      new String[] { "urn:miriam:pubchem.compound" },
      new Class<?>[] { Chemical.class }, "MIR:00000034",
      new Class<?>[] { Chemical.class },
      "pubchem.compound",
      "100101"),

  /**
   * PubChem-substance: http://pubchem.ncbi.nlm.nih.gov/.
   */
  PUBCHEM_SUBSTANCE("PubChem-substance",
      "http://pubchem.ncbi.nlm.nih.gov/",
      new String[] { "urn:miriam:pubchem.substance" },
      new Class<?>[] { Chemical.class }, "MIR:00000033",
      new Class<?>[] { Chemical.class },
      "pubchem.substance",
      "100101"),

  /**
   * PubMed: http://www.ncbi.nlm.nih.gov/PubMed/.
   */
  PUBMED("PubMed",
      "http://www.ncbi.nlm.nih.gov/PubMed/",
      new String[] { "urn:miriam:pubmed", "http://identifiers.org/pubmed/" },
      new Class<?>[] { BioEntity.class }, "MIR:00000015",
      new Class<?>[] { Reaction.class },
      "pubmed",
      "28725475"),

  /**
   * Reactome: http://www.reactome.org/.
   */
  REACTOME("Reactome",
      "http://www.reactome.org/",
      new String[] { "urn:miriam:reactome", "http://identifiers.org/reactome/" },
      new Class<?>[] { Reaction.class }, "MIR:00000018",
      new Class<?>[] {},
      "reactome",
      "R-HSA-201451"),

  /**
   * RefSeq: http://www.ncbi.nlm.nih.gov/projects/RefSeq/.
   */
  REFSEQ("RefSeq",
      "http://www.ncbi.nlm.nih.gov/projects/RefSeq/",
      "urn:miriam:refseq",
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000039",
      new Class<?>[] {},
      "refseq",
      "NP_012345"),

  /**
   * Rhea: http://www.rhea-db.org/.
   */
  RHEA("Rhea",
      "http://www.rhea-db.org/",
      "urn:miriam:rhea",
      new Class<?>[] { Reaction.class }, "MIR:00000082",
      new Class<?>[] {},
      "rhea",
      "12345"),

  /**
   * SGD: http://www.yeastgenome.org/.
   */
  SGD("Saccharomyces Genome Database",
      "http://www.yeastgenome.org/",
      "urn:miriam:sgd",
      new Class<?>[] {}, "MIR:00000023",
      new Class<?>[] {},
      "sgd",
      "S000003909"),

  /**
   * STITCH: http://stitch.embl.de/.
   */
  STITCH("STITCH",
      "http://stitch.embl.de/",
      "urn:miriam:stitch",
      new Class<?>[] {}, "MIR:00000266",
      new Class<?>[] {},
      "stitch",
      "BQJCRHHNABKAKU"),

  /**
   * STRING: http://string-db.org/.
   */
  STRING("STRING",
      "http://string-db.org/",
      "urn:miriam:string",
      new Class<?>[] {}, "MIR:00000265",
      new Class<?>[] {},
      "string",
      "P53350"),

  /**
   * The Arabidopsis Information Resource (TAIR) maintains a database of genetic
   * and molecular biology data for the model higher plant Arabidopsis thaliana.
   * The name of a Locus is unique and used by TAIR, TIGR, and MIPS:
   * http://arabidopsis.org/index.jsp.
   */
  TAIR_LOCUS("TAIR Locus",
      "http://arabidopsis.org/index.jsp",
      "urn:miriam:tair.locus",
      new Class<?>[] {}, "MIR:00000050",
      new Class<?>[] {},
      "tair.locus",
      "2200950"),

  /**
   * Taxonomy: http://www.ncbi.nlm.nih.gov/taxonomy/.
   */
  TAXONOMY("Taxonomy",
      "http://www.ncbi.nlm.nih.gov/taxonomy/",
      new String[] { "urn:miriam:taxonomy", "http://identifiers.org/taxonomy/" },
      new Class<?>[] {}, "MIR:00000006",
      new Class<?>[] {},
      "taxonomy",
      "9606"),

  /**
   * Toxicogenomic: Chemical: http://ctdbase.org/detail.go.
   * http://ctdbase.org/detail.go
   */
  TOXICOGENOMIC_CHEMICAL("Toxicogenomic Chemical",
      "http://ctdbase.org/",
      "urn:miriam:ctd.chemical",
      new Class<?>[] {}, "MIR:00000098",
      new Class<?>[] {},
      "ctd.chemical",
      "D001151"),

  /**
   * UniGene: http://www.ncbi.nlm.nih.gov/unigene.
   */
  UNIGENE("UniGene",
      "http://www.ncbi.nlm.nih.gov/unigene",
      "urn:miriam:unigene",
      new Class<?>[] {}, "MIR:00000346",
      new Class<?>[] {},
      "unigene",
      "4900"),

  /**
   * Uniprot: http://www.uniprot.org/.
   */
  UNIPROT("Uniprot",
      "http://www.uniprot.org/",
      new String[] { "urn:miriam:uniprot", "http://identifiers.org/uniprot/" },
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000005",
      new Class<?>[] {},
      "uniprot",
      "P0DP23"),

  /**
   * UniProt Isoform: http://www.uniprot.org/.
   */
  UNIPROT_ISOFORM("UniProt Isoform",
      "http://www.uniprot.org/",
      "urn:miriam:uniprot.isoform",
      new Class<?>[] { Protein.class }, "MIR:00000388",
      new Class<?>[] {},
      "uniprot.isoform",
      "Q5BJF6-3"),

  /**
   * Unknown reference type...
   */
  @Deprecated
  UNKNOWN("Unknown",
      null,
      new String[] {},
      new Class<?>[] {}, null,
      new Class<?>[] {},
      null,
      null),

  /**
   * VMH metabolite: https://vmh.uni.lu/.
   */
  VMH_METABOLITE("VMH metabolite",
      "https://vmh.uni.lu/",
      new String[] { "urn:miriam:vmhmetabolite", "http://identifiers.org/vmhmetabolite/" },
      new Class<?>[] { Chemical.class }, "MIR:00000636",
      new Class<?>[] {},
      "vmhmetabolite",
      "h2o"),

  /**
   * VMH reaction: https://vmh.uni.lu/.
   */
  VMH_REACTION("VMH reaction",
      "https://vmh.uni.lu/",
      new String[] { "urn:miriam:vmhreaction", "http://identifiers.org/vmhreaction/" },
      new Class<?>[] { Reaction.class }, "MIR:00000640",
      new Class<?>[] {},
      "vmhreaction",
      "HEX1"),

  /**
   * Wikidata: https://www.wikidata.org/.
   */
  WIKIDATA("Wikidata",
      "https://www.wikidata.org/",
      new String[] { "urn:miriam:wikidata" },
      new Class<?>[] {}, "MIR:00000549",
      new Class<?>[] {},
      "wikidata",
      "Q2207226"),

  /**
   * WikiPathways: http://www.wikipathways.org/.
   */
  WIKIPATHWAYS("WikiPathways",
      "http://www.wikipathways.org/",
      new String[] { "urn:miriam:wikipathways" },
      new Class<?>[] {}, "MIR:00000076",
      new Class<?>[] {},
      "wikipathways",
      "WP100"),

  /**
   * Wikipedia: http://en.wikipedia.org/wiki/Main_Page.
   */
  WIKIPEDIA("Wikipedia (English)",
      "http://en.wikipedia.org/wiki/Main_Page", // /
      new String[] { "urn:miriam:wikipedia.en" },
      new Class<?>[] {}, "MIR:00000384",
      new Class<?>[] {},
      "wikipedia.en",
      "SM_UB-81"),

  /**
   * WormBase: http://wormbase.bio2rdf.org/fct.
   */
  WORM_BASE("WormBase",
      "http://wormbase.bio2rdf.org/fct", // /
      new String[] { "urn:miriam:wormbase" },
      new Class<?>[] {}, "MIR:00000027",
      new Class<?>[] {},
      "wb",
      "WBGene00000001");

  private static Logger logger = LogManager.getLogger(MiriamType.class);
  /**
   * User friendly name.
   */
  private String commonName;

  /**
   * url to home page of given resource type.
   */
  private String dbHomepage;

  /**
   * Identifier of the database in miriam registry.
   */
  private String registryIdentifier;

  /**
   * Valid URIs to this resource.
   */
  private List<String> uris = new ArrayList<>();

  /**
   * Classes that can be annotated by this resource.
   */
  private List<Class<? extends BioEntity>> validClass = new ArrayList<>();

  /**
   * When class from this list is marked as "require at least one annotation" then
   * annotation of this type is valid.
   */
  private List<Class<? extends BioEntity>> requiredClass = new ArrayList<>();

  private String namespace;

  private String exampleIdentifier;

  /**
   * Constructor that initialize enum object.
   * 
   * @param dbHomePage
   *          home page of the resource {@link #dbHomepage}
   * @param commonName
   *          {@link #commonName}
   * @param uris
   *          {@link #uris}
   * @param classes
   *          {@link #validClass}
   * @param registryIdentifier
   *          {@link #registryIdentifier}
   * @param requiredClasses
   *          {@link #requiredClasses}
   */
  MiriamType(String commonName, String dbHomePage, String[] uris, Class<?>[] classes, String registryIdentifier,
      Class<?>[] requiredClasses, String namespace, String exampleIdentifier) {
    this.commonName = commonName;
    this.dbHomepage = dbHomePage;
    for (String string : uris) {
      this.uris.add(string);
    }
    for (Class<?> clazz : classes) {
      this.validClass.add((Class<? extends BioEntity>) clazz);
    }
    for (Class<?> clazz : requiredClasses) {
      this.requiredClass.add((Class<? extends BioEntity>) clazz);
    }
    this.registryIdentifier = registryIdentifier;
    this.namespace = namespace;
    this.exampleIdentifier = exampleIdentifier;
  }

  /**
   * Constructor that initialize enum object.
   * 
   * @param dbHomePage
   *          home page of the resource {@link #dbHomepage}
   * @param commonName
   *          {@link #commonName}
   * @param uri
   *          one of {@link #uris}
   * @param registryIdentifier
   *          {@link #registryIdentifier}
   * @param classes
   *          {@link #validClass}
   */
  MiriamType(String commonName, String dbHomePage, String uri, Class<?>[] classes, String registryIdentifier,
      Class<?>[] requiredClasses, String namespace, String exampleIdentifier) {
    this(commonName, dbHomePage, new String[] { uri }, classes, registryIdentifier, requiredClasses, namespace,
        exampleIdentifier);
  }

  /**
   * Returns {@link MiriamType} associated with parameter uri address.
   *
   * @param uri
   *          uri to check
   * @return {@link MiriamType} for given uri
   */
  public static MiriamType getTypeByUri(String uri) {
    for (MiriamType mt : MiriamType.values()) {
      for (String string : mt.getUris()) {
        if (string.equalsIgnoreCase(uri)) {
          return mt;
        }
      }
    }
    logger.warn("Unknown miriam type: " + uri);
    return null;
  }

  /**
   * Returns {@link MiriamType} associated with {@link #commonName}.
   *
   * @param string
   *          {@link #commonName}
   * @return {@link MiriamType} for given name
   */
  public static MiriamType getTypeByCommonName(String string) {
    for (MiriamType mt : MiriamType.values()) {
      if (string.equalsIgnoreCase(mt.getCommonName())) {
        return mt;
      }
    }
    return null;
  }

  /**
   * Transforms identifier into {@link MiriamData}.
   *
   * @param generalIdentifier
   *          identifier in the format NAME:IDENTIFIER. Where NAME is the name
   *          from {@link MiriamType#commonName} and IDENTIFIER is resource
   *          identifier.
   * @return {@link MiriamData} representing generalIdentifier, when identifier is
   *         invalid InvalidArgumentException is thrown
   */
  public static MiriamData getMiriamDataFromIdentifier(String generalIdentifier) {
    int index = generalIdentifier.indexOf(":");
    if (index < 0) {
      throw new InvalidArgumentException("Identifier doesn't contain type");
    }
    String type = generalIdentifier.substring(0, index);
    String id = generalIdentifier.substring(index + 1);
    for (MiriamType mt : MiriamType.values()) {
      if (mt.getCommonName().equalsIgnoreCase(type)) {
        return new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, mt, id);
      }
    }
    throw new InvalidArgumentException("Unknown miriam type: " + type + " (id: " + id + ")");
  }

  /**
   * Creates {@link MiriamData} from miriam uri.
   *
   * @param miriamUri
   *          miriam uri defining {@link MiriamData}
   * @return {@link MiriamData} from miriam uri
   */
  public static MiriamData getMiriamByUri(String miriamUri) {
    // this hack is due to CellDesigner issue (CellDesigner incorrectly handle
    // with identifiers that have ":" inside resource ":" inside resource with
    // "%3A" and also the last ":"
    miriamUri = miriamUri.replace("%3A", ":");

    String foundUri = "";
    MiriamType foundType = null;

    for (MiriamType type : MiriamType.values()) {
      for (String uri : type.getUris()) {
        if (miriamUri.startsWith(uri + ":")) {
          if (uri.length() > foundUri.length()) {
            foundType = type;
            foundUri = uri;
          }
        } else if (miriamUri.startsWith(uri) && uri.endsWith("/")) {
          if (uri.length() > foundUri.length()) {
            foundType = type;
            foundUri = uri;
          }
        } else if (miriamUri.startsWith(uri) && uri.endsWith("/")) {
          if (uri.length() > foundUri.length()) {
            foundType = type;
            foundUri = uri;
          }
        }
      }
    }
    if (foundType != null) {
      if (foundUri.length() >= miriamUri.length()) {
        throw new InvalidArgumentException("Invalid miriam uri: " + miriamUri);
      }
      return new MiriamData(foundType, miriamUri.substring(foundUri.length() + 1));
    }
    throw new InvalidArgumentException("Invalid miriam uri: " + miriamUri);
  }

  /**
   *
   * @return {@link #commonName}
   */
  public String getCommonName() {
    return commonName;
  }

  /**
   *
   * @return {@link #uris}
   */
  public List<String> getUris() {
    return uris;
  }

  /**
   *
   * @return {@link #validClass}
   */
  public List<Class<? extends BioEntity>> getValidClass() {
    return validClass;
  }

  /**
   * @return the dbHomepage
   * @see #dbHomepage
   */
  public String getDbHomepage() {
    return dbHomepage;
  }

  /**
   * @return the registryIdentifier
   * @see #registryIdentifier
   */
  public String getRegistryIdentifier() {
    return registryIdentifier;
  }

  /**
   * @return the requiredClass
   * @see #requiredClass
   */
  public List<Class<? extends BioEntity>> getRequiredClass() {
    return requiredClass;
  }

  public String getNamespace() {
    return namespace;
  }

  public String getExampleIdentifier() {
    return exampleIdentifier;
  }

}
