package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner trigger reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("TRIGGER_REACTION")
public class TriggerReaction extends Reaction implements SimpleReactionInterface, ModifierReactionNotation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public TriggerReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public TriggerReaction(Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Trigger";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public TriggerReaction copy() {
    if (this.getClass() == TriggerReaction.class) {
      return new TriggerReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
