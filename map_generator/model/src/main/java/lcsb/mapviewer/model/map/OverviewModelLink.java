package lcsb.mapviewer.model.map;

import javax.persistence.*;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;

/**
 * Link used in {@link OverviewImage parent OverviewImage} to link it to
 * {@link ModelData (sub)model}.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("MODEL_LINK")
public class OverviewModelLink extends OverviewLink {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Model to which this links is going.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData linkedModel;

  /**
   * Zoom level at which model should be opened.
   */
  private Integer zoomLevel;

  /**
   * X coordinate on {@link #linkedModel} where this links point to.
   */
  private Integer xCoord;

  /**
   * Y coordinate on {@link #linkedModel} where this links point to.
   */
  private Integer yCoord;

  /**
   * Default constructor that creates copy of the parameter object.
   * 
   * @param original
   *          original object that will be copied to this one
   */
  public OverviewModelLink(OverviewModelLink original) {
    super(original);
    this.linkedModel = original.linkedModel;
    this.xCoord = original.xCoord;
    this.yCoord = original.yCoord;
    this.zoomLevel = original.zoomLevel;
  }

  /**
   * Default constructor.
   */
  public OverviewModelLink() {
    super();
  }

  /**
   * @return the model
   * @see #linkedModel
   */
  public ModelData getLinkedModel() {
    return linkedModel;
  }

  /**
   * @param model
   *          the model to set
   * @see #linkedModel
   */
  public void setLinkedModel(Model model) {
    if (model != null) {
      this.linkedModel = model.getModelData();
    } else {
      this.linkedModel = null;
    }
  }

  @Override
  public OverviewModelLink copy() {
    if (this.getClass() == OverviewModelLink.class) {
      return new OverviewModelLink(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the zoomLevel
   * @see #zoomLevel
   */
  public Integer getZoomLevel() {
    return zoomLevel;
  }

  /**
   * @param zoomLevel
   *          the zoomLevel to set
   * @see #zoomLevel
   */
  public void setZoomLevel(Integer zoomLevel) {
    this.zoomLevel = zoomLevel;
  }

  /**
   * @return the xCoord
   * @see #xCoord
   */
  public Integer getxCoord() {
    return xCoord;
  }

  /**
   * @param xCoord
   *          the xCoord to set
   * @see #xCoord
   */
  public void setxCoord(Integer xCoord) {
    this.xCoord = xCoord;
  }

  /**
   * Sets {@link #xCoord} value. Value will be trimmed to {@link Integer}.
   *
   * @param value
   *          the xCoord to set
   * @see #xCoord
   */
  public void setxCoord(Double value) {
    if (value == null) {
      this.xCoord = null;
    } else {
      this.xCoord = value.intValue();
    }
  }

  /**
   * @return the yCoord
   * @see #yCoord
   */
  public Integer getyCoord() {
    return yCoord;
  }

  /**
   * @param yCoord
   *          the yCoord to set
   * @see #yCoord
   */
  public void setyCoord(Integer yCoord) {
    this.yCoord = yCoord;
  }

  /**
   * Sets {@link #yCoord} value. Value will be trimmed to {@link Integer}.
   * 
   * @param value
   *          the yCoord to set
   * @see #yCoord
   */
  public void setyCoord(Double value) {
    if (value == null) {
      this.yCoord = null;
    } else {
      this.yCoord = value.intValue();
    }
  }

}
