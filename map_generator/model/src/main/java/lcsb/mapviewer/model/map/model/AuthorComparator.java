package lcsb.mapviewer.model.map.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * This class implements comparator interface for {@link Author}.
 * 
 * @author Piotr Gawron
 * 
 */
public class AuthorComparator extends Comparator<Author> {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(AuthorComparator.class);

  /**
   * Default constructor.
   */
  public AuthorComparator() {
    super(Author.class);
  }

  @Override
  protected int internalCompare(Author arg0, Author arg1) {
    StringComparator stringComparator = new StringComparator();
    int status = stringComparator.compare(arg0.getEmail(), arg1.getEmail());
    if (status != 0) {
      logger.debug("email different: " + arg0.getEmail() + ", " + arg1.getEmail());
      return status;
    }
    status = stringComparator.compare(arg0.getFirstName(), arg1.getFirstName());
    if (status != 0) {
      logger.debug("first name different: " + arg0.getFirstName() + ", " + arg1.getFirstName());
      return status;
    }
    status = stringComparator.compare(arg0.getLastName(), arg1.getLastName());
    if (status != 0) {
      logger.debug("last name different: " + arg0.getLastName() + ", " + arg1.getLastName());
      return status;
    }
    status = stringComparator.compare(arg0.getOrganisation(), arg1.getOrganisation());
    if (status != 0) {
      logger.debug("organisation different: " + arg0.getOrganisation() + ", " + arg1.getOrganisation());
      return status;
    }
    return 0;
  }

}
