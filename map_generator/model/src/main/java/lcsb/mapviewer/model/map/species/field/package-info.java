/**
 * Provides data structure for complicated
 * {@link lcsb.mapviewer.model.map.species.Species Species} implementation.
 * These structures contain some additional structural information about
 * elements.
 */
package lcsb.mapviewer.model.map.species.field;