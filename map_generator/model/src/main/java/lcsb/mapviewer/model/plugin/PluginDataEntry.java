package lcsb.mapviewer.model.plugin;

import java.io.Serializable;

import javax.persistence.*;

import lcsb.mapviewer.model.user.User;

/**
 * Single entry of data stored by the plugin.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class PluginDataEntry implements Serializable {

  /**
  	* 
    */
  private static final long serialVersionUID = 1L;

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @ManyToOne(fetch = FetchType.LAZY, optional = true)
  private User user;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private Plugin plugin;

  @Column(nullable = false)
  private String key;

  @Column(nullable = false)
  private String value;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Plugin getPlugin() {
    return plugin;
  }

  public void setPlugin(Plugin plugin) {
    this.plugin = plugin;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
