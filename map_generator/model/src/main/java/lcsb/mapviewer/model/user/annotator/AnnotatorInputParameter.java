package lcsb.mapviewer.model.user.annotator;

import javax.persistence.*;

import lcsb.mapviewer.model.map.MiriamType;

/**
 * Definition of annotator input. It describes which property/identifier should
 * be used to identify element in the external database.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("INPUT_PARAMETER")
public class AnnotatorInputParameter extends AnnotatorParameter {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private BioEntityField field;

  @Enumerated(EnumType.STRING)
  private MiriamType identifierType;

  /**
   * Default constructor for hibernate.
   */
  protected AnnotatorInputParameter() {
  }

  public AnnotatorInputParameter(BioEntityField field) {
    this(field, null);
  }

  public AnnotatorInputParameter(MiriamType type) {
    this(null, type);
  }

  public AnnotatorInputParameter(BioEntityField field, MiriamType identifierType) {
    this.field = field;
    this.identifierType = identifierType;
  }

  public BioEntityField getField() {
    return field;
  }

  public MiriamType getIdentifierType() {
    return identifierType;
  }

  @Override
  public String toString() {
    return "[" + field + "," + identifierType + "]";
  }
}
