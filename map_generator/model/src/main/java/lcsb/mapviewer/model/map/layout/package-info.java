/**
 * Provides data structures that stores information used for data visualization
 * of the map.
 */
package lcsb.mapviewer.model.map.layout;
