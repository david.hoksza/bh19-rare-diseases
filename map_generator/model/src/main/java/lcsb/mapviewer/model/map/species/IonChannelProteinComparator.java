package lcsb.mapviewer.model.map.species;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;

/**
 * Comparator class used for comparing {@link IonChannelProtein} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class IonChannelProteinComparator extends Comparator<IonChannelProtein> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(IonChannelProteinComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public IonChannelProteinComparator(double epsilon) {
    super(IonChannelProtein.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public IonChannelProteinComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new ProteinComparator(epsilon);
  }

  @Override
  protected int internalCompare(IonChannelProtein arg0, IonChannelProtein arg1) {
    return 0;
  }
}
