package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner state transition reaction. It must
 * have at least one reactant and one product.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("STATE_TRANSITION_REACTION")
public class StateTransitionReaction extends Reaction implements SimpleReactionInterface {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public StateTransitionReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public StateTransitionReaction(Reaction result) {
    super(result);
  }

  public StateTransitionReaction(String reactionId) {
    super(reactionId);
  }

  @Override
  public String getStringType() {
    return "State transition";
  }

  @Override
  public ReactionRect getReactionRect() {
    return ReactionRect.RECT_EMPTY;
  }

  @Override
  public StateTransitionReaction copy() {
    if (this.getClass() == StateTransitionReaction.class) {
      return new StateTransitionReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
