package lcsb.mapviewer.model.map.species.field;

/**
 * Interface implemented by species that support {@link CodingRegion}
 * modification.
 * 
 * @author Piotr Gawron
 *
 */
public interface SpeciesWithCodingRegion extends SpeciesWithModificationResidue {

  /**
   * Adds a {@link CodingRegion} to the species.
   * 
   * @param codingRegion
   *          {@link CodingRegion} to add
   */
  void addCodingRegion(CodingRegion codingRegion);

}
