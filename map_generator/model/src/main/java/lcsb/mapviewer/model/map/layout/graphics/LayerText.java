package lcsb.mapviewer.model.map.layout.graphics;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.Drawable;

/**
 * This class describes single text in the layer.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class LayerText implements Serializable, Drawable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default font size of the text.
   */
  private static final double DEFAULT_LAYER_FONT_SIZE = 11.0;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(LayerText.class);

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Text color.
   */
  private Color color = Color.BLACK;

  /**
   * Text background color.
   */
  private Color backgroundColor = Color.LIGHT_GRAY;

  private Color borderColor = Color.LIGHT_GRAY;

  /**
   * X coordinate of text start point.
   */
  private Double x = 0.0;

  /**
   * Y coordinate of text start point.
   */
  private Double y = 0.0;

  /**
   * Z-index.
   */
  private Integer z;

  /**
   * Width of the rectangle where text is drawn.
   */
  private Double width = 0.0;

  /**
   * Height of the rectangle where text is drawn.
   */
  private Double height = 0.0;

  /**
   * Text.
   */
  private String notes;

  /**
   * Font size.
   */
  private Double fontSize = DEFAULT_LAYER_FONT_SIZE;

  /**
   * Glyph used for drawing (instead of text).
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private Glyph glyph = null;

  /**
   * Default constructor.
   */
  public LayerText() {

  }

  /**
   * Default constructor.
   * 
   * @param bound
   *          bounds in which text is placed
   * @param text
   *          {@link #notes text}
   */
  public LayerText(Rectangle2D bound, String text) {
    setX(bound.getX());
    setY(bound.getY());
    setWidth(bound.getWidth());
    setHeight(bound.getHeight());
    setNotes(text);
  }

  /**
   * Constructor that copies data from the parameter.
   * 
   * @param layerText
   *          from this parameter line data will be copied
   */
  public LayerText(LayerText layerText) {
    color = layerText.getColor();
    x = layerText.getX();
    y = layerText.getY();
    z = layerText.getZ();
    width = layerText.getWidth();
    height = layerText.getHeight();
    notes = layerText.getNotes();
    fontSize = layerText.getFontSize();
    if (layerText.getGlyph() != null) {
      setGlyph(new Glyph(layerText.getGlyph()));
    }
    setBackgroundColor(layerText.getBackgroundColor());
    setBorderColor(layerText.getBorderColor());
  }

  /**
   * Prepares a copy of the object.
   *
   * @return copy of LayerText
   */
  public LayerText copy() {
    if (this.getClass() == LayerText.class) {
      return new LayerText(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color
   *          the color to set
   * @see #color
   */
  public void setColor(Color color) {
    this.color = color;
  }

  /**
   * @return the x
   * @see #x
   */
  public Double getX() {
    return x;
  }

  /**
   * Set x from string containing double value.
   *
   * @param param
   *          x of the line in text format
   */
  public void setX(String param) {
    try {
      x = Double.parseDouble(param);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid x value: " + param, e);
    }
  }

  /**
   * @param x
   *          the x to set
   * @see #x
   */
  public void setX(Double x) {
    this.x = x;
  }

  /**
   * @return the y
   * @see #y
   */
  public Double getY() {
    return y;
  }

  /**
   * Set y from string containing double value.
   *
   * @param param
   *          y of the line in text format
   */
  public void setY(String param) {
    try {
      y = Double.parseDouble(param);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid y value: " + param, e);
    }
  }

  /**
   * @param y
   *          the y to set
   * @see #y
   */
  public void setY(Double y) {
    this.y = y;
  }

  /**
   * @return the width
   * @see #width
   */
  public Double getWidth() {
    return width;
  }

  /**
   * Set width from string containing double value.
   *
   * @param param
   *          width of the line in text format
   */
  public void setWidth(String param) {
    try {
      width = Double.parseDouble(param);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid width value: " + param, e);
    }
  }

  /**
   * @param width
   *          the width to set
   * @see #width
   */
  public void setWidth(Double width) {
    this.width = width;
  }

  /**
   * @return the height
   * @see #height
   */
  public Double getHeight() {
    return height;
  }

  /**
   * Set height from string containing double value.
   *
   * @param param
   *          height of the line in text format
   */
  public void setHeight(String param) {
    try {
      height = Double.parseDouble(param);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid height value: " + param, e);
    }
  }

  /**
   * @param height
   *          the height to set
   * @see #height
   */
  public void setHeight(Double height) {
    this.height = height;
  }

  /**
   * @return the notes
   * @see #notes
   */
  public String getNotes() {
    return notes;
  }

  /**
   * @param notes
   *          the notes to set
   * @see #notes
   */
  public void setNotes(String notes) {
    this.notes = notes;
  }

  /**
   * @return the fontSize
   * @see #fontSize
   */
  public Double getFontSize() {
    return fontSize;
  }

  /**
   * Set font size from string containing double value.
   *
   * @param param
   *          font size of the line in text format
   */
  public void setFontSize(String param) {
    try {
      fontSize = Double.parseDouble(param);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid fontSize value: " + param, e);
    }

  }

  /**
   * @param fontSize
   *          the fontSize to set
   * @see #fontSize
   */
  public void setFontSize(Double fontSize) {
    this.fontSize = fontSize;
  }

  /**
   * Returns a rectangle that determines a rectangle border.
   * 
   * @return rectangle border
   */
  public Rectangle2D getBorder() {
    return new Rectangle2D.Double(x, y, width, height);
  }

  public Color getBackgroundColor() {
    return backgroundColor;
  }

  public void setBackgroundColor(Color backgroundColor) {
    this.backgroundColor = backgroundColor;
  }

  @Override
  public String toString() {
    return "[" + this.getClass().getSimpleName() + ": " + this.getNotes() + "]";
  }

  @Override
  public Integer getZ() {
    return z;
  }

  @Override
  public void setZ(Integer z) {
    this.z = z;
  }

  @Override
  public String getElementId() {
    return "x=" + x + ";y=" + y + "; w=" + width + ", h=" + height;
  }

  @Override
  public double getSize() {
    return width * height;
  }

  public Glyph getGlyph() {
    return glyph;
  }

  public void setGlyph(Glyph glyph) {
    this.glyph = glyph;
  }

  public Color getBorderColor() {
    return borderColor;
  }

  public void setBorderColor(Color borderColor) {
    this.borderColor = borderColor;
  }

}
