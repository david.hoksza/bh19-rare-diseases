package lcsb.mapviewer.model.map.species;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.*;

/**
 * Entity representing antisense rna element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("ANTISENSE_RNA")
public class AntisenseRna extends Species
    implements SpeciesWithCodingRegion, SpeciesWithModificationSite, SpeciesWithProteinBindingDomain {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of {@link AntisenseRnaRegion regions} related to this
   * {@link AntisenseRna}.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "species")
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<ModificationResidue> regions = new ArrayList<>();

  /**
   * Empty constructor required by hibernate.
   */
  AntisenseRna() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public AntisenseRna(AntisenseRna original) {
    super(original);
    for (ModificationResidue region : original.getRegions()) {
      addModificationResidue(region.copy());
    }
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  public AntisenseRna(String elementId) {
    setElementId(elementId);
  }

  /**
   * Adds {@link AntisenseRnaRegion} to the object.
   * 
   * @param antisenseRnaRegion
   *          region to be added
   */
  private void addModificationResidue(ModificationResidue antisenseRnaRegion) {
    regions.add(antisenseRnaRegion);
    antisenseRnaRegion.setSpecies(this);
  }

  @Override
  public void addCodingRegion(CodingRegion codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public void addProteinBindingDomain(ProteinBindingDomain codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public void addModificationSite(ModificationSite codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public AntisenseRna copy() {
    if (this.getClass() == AntisenseRna.class) {
      return new AntisenseRna(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the regions
   * @see #regions
   */
  public List<ModificationResidue> getRegions() {
    return regions;
  }

  /**
   * @param regions
   *          the regions to set
   * @see #regions
   */
  public void setRegions(List<ModificationResidue> regions) {
    this.regions = regions;
  }

  @Override
  public List<ModificationResidue> getModificationResidues() {
    return getRegions();
  }

  @Override
  public String getStringType() {
    return "Antisense RNA";
  }

}
