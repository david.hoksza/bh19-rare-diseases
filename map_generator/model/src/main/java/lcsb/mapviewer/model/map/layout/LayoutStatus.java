package lcsb.mapviewer.model.map.layout;

/**
 * Status available for the layouts (based on the processing progress).
 * 
 * @author Piotr Gawron
 * 
 */
public enum LayoutStatus {

  /**
   * Default unknown status.
   */
  UNKNOWN("Unknown"),

  /**
   * Layout is not available.
   */
  NA("Not available"),

  /**
   * Layout is generating.
   */
  GENERATING("Generating"),

  /**
   * Layout is ready.
   */
  OK("OK"),

  /**
   * There was a problem during layout generation.
   */
  FAILURE("Failure");

  /**
   * Common name of the status.
   */
  private String commonName;

  /**
   * Default constructor.
   *
   * @param commonName
   *          {@link #commonName}
   */
  LayoutStatus(String commonName) {
    this.commonName = commonName;
  }

  /**
   *
   * @return {@link #commonName}
   */
  public String getCommonName() {
    return commonName;
  }

}
