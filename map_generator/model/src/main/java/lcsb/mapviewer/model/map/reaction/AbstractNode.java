package lcsb.mapviewer.model.map.reaction;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.graphics.PolylineData;

/**
 * This class defines abstract node of the reaction. By the design there are two
 * main classes of nodes (that are extended further):
 * <ul>
 * <li>end nodes with linked elements
 * {@link lcsb.mapviewer.model.map.reaction.ReactionNode ReactionNode} - defines
 * what elements participate in the reaction and in what role (product,
 * reactant)</li>
 * <li>operator nodes {@link lcsb.mapviewer.model.map.reaction.NodeOperator
 * NodeOperator} - defines relation between different participants of the
 * reaction (and operator, split/dissociation, etc.)</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "node_type_db", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC_NODE")
@Table(name = "reaction_node_table")
public abstract class AbstractNode implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * This object define the node that is next in the schema (in the flow from
   * reactants to products). In case the node is terminal product this object is
   * set to null.
   */
  @ManyToOne
  private NodeOperator nodeOperatorForInput = null;

  /**
   * This object define the node that is before in the schema (in the flow from
   * reactants to products). In case the node is reactant this object is set to
   * null.
   */
  @ManyToOne
  private NodeOperator nodeOperatorForOutput = null;

  /**
   * Reaction object where this node is located.
   */
  @ManyToOne
  private Reaction reaction = null;

  /**
   * Line that represent this node in graphical representation.
   */
  @Cascade({ CascadeType.ALL })
  @OneToOne(fetch = FetchType.EAGER, optional = false)
  private PolylineData line;

  /**
   * Default constructor.
   */
  public AbstractNode() {

  }

  /**
   * Constructor that copies the data from the original node in the parameter.
   * 
   * @param node
   *          original node based on which this one will be created
   */
  public AbstractNode(AbstractNode node) {
    this.reaction = node.reaction;
    this.line = new PolylineData(node.getLine());
  }

  /**
   * @return the reaction
   * @see #reaction
   */
  public Reaction getReaction() {
    return reaction;
  }

  /**
   * @param reaction
   *          the reaction to set
   * @see #reaction
   */
  public void setReaction(Reaction reaction) {
    this.reaction = reaction;
  }

  /**
   * @return the line
   * @see #line
   */
  public PolylineData getLine() {
    return line;
  }

  /**
   * @param line
   *          the line to set
   * @see #line
   */
  public void setLine(PolylineData line) {
    this.line = line;
  }

  /**
   * Creates a copy of the object.
   * 
   * @return copy of the object
   */
  public abstract AbstractNode copy();

  /**
   * @return the nodeOperatorForInput
   * @see #nodeOperatorForInput
   */
  public NodeOperator getNodeOperatorForInput() {
    return nodeOperatorForInput;
  }

  /**
   * @param nodeOperatorForInput
   *          the nodeOperatorForInput to set
   * @see #nodeOperatorForInput
   */
  public void setNodeOperatorForInput(NodeOperator nodeOperatorForInput) {
    this.nodeOperatorForInput = nodeOperatorForInput;
  }

  /**
   * @return the nodeOperatorForOutput
   * @see #nodeOperatorForOutput
   */
  public NodeOperator getNodeOperatorForOutput() {
    return nodeOperatorForOutput;
  }

  /**
   * @param nodeOperatorForOutput
   *          the nodeOperatorForOutput to set
   * @see #nodeOperatorForOutput
   */
  public void setNodeOperatorForOutput(NodeOperator nodeOperatorForOutput) {
    this.nodeOperatorForOutput = nodeOperatorForOutput;
  }

}
