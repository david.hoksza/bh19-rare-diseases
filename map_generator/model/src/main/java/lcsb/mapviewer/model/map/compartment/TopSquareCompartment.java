package lcsb.mapviewer.model.map.compartment;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;

/**
 * This class defines compartment that covers top part of the model up to some
 * border on the bottom.
 * 
 * @author Piotr Gawron
 * 
 */

@Entity
@DiscriminatorValue("TOP_SQUARE_COMPARTMENT")
public class TopSquareCompartment extends Compartment {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  TopSquareCompartment() {
    super();
  }

  /**
   * Constructor that creates a compartment with the new shape but takes the
   * reference data from the compartment given as parameter.
   * 
   * @param original
   *          original compartment where the data was kept
   * @param model
   *          model object to which the compartment will be assigned
   */
  public TopSquareCompartment(Compartment original, Model model) {
    super(original);
    setX(0.0);
    setWidth(model.getWidth() * 2);
    setY(0.0);
    setHeight(model.getHeight() * 2);
  }

  /**
   * Constructor that creates a compartment with the new shape and takes the
   * reference data from the compartment given as parameter.
   * 
   * @param original
   *          orignal compartment where the data was kept
   */
  public TopSquareCompartment(TopSquareCompartment original) {
    super(original);
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          identifier of the compartment
   */
  public TopSquareCompartment(String elementId) {
    setElementId(elementId);
  }

  @Override
  public TopSquareCompartment copy() {
    if (this.getClass() == TopSquareCompartment.class) {
      return new TopSquareCompartment(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
