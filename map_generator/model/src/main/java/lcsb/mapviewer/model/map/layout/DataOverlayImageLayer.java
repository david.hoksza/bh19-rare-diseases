package lcsb.mapviewer.model.map.layout;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;

/**
 * This object represents set of images generated for background visualization
 * of {@link Layout}.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class DataOverlayImageLayer implements Serializable {
  public static final Comparator<? super DataOverlayImageLayer> ID_COMPARATOR = new Comparator<DataOverlayImageLayer>() {

    @Override
    public int compare(DataOverlayImageLayer o1, DataOverlayImageLayer o2) {
      return o1.getId() - o2.getId();
    }
  };
  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(DataOverlayImageLayer.class);

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Directory where the images are stored.
   */
  private String directory;

  /**
   * {@link ModelData} for which the images were created.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData model;

  /**
   * {@link Layout} for which the images were created.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private Layout dataOverlay;

  /**
   * Default constructor.
   */
  protected DataOverlayImageLayer() {

  }

  /**
   * Constructor that initializes object with basic parameters.
   *
   * @param title
   *          title of the layout
   * @param directory
   *          directory where the images are stored
   * @param publicLayout
   *          is the layout publicly available
   */
  public DataOverlayImageLayer(ModelData model, String directory) {
    this.directory = directory;
    this.model = model;
  }

  public DataOverlayImageLayer(Model model, String directory) {
    this(model.getModelData(), directory);
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the directory
   * @see #directory
   */
  public String getDirectory() {
    return directory;
  }

  /**
   * @param directory
   *          the directory to set
   * @see #directory
   */
  public void setDirectory(String directory) {
    this.directory = directory;
  }

  public ModelData getModel() {
    return model;
  }

  public void setModel(ModelData model) {
    this.model = model;
  }

  public Layout getDataOverlay() {
    return dataOverlay;
  }

  public void setDataOverlay(Layout dataOverlay) {
    this.dataOverlay = dataOverlay;
  }

}
