package lcsb.mapviewer.model.graphics;

import java.awt.*;
import java.awt.geom.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.model.map.Drawable;

/**
 * This class represents general line structure. Line can contain several
 * segments. Beginning and the end of the line could be finished with the arrow.
 * There are also color and line width associated with the object.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class PolylineData implements Serializable, Drawable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Class used to perform some operation on points.
   */
  private static PointTransformation pt = new PointTransformation();

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(PolylineData.class);

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * List of points that build the line.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "point_table", joinColumns = @JoinColumn(name = "id"))
  @OrderColumn(name = "idx")
  @Column(name = "point_val")
  @Type(type = "lcsb.mapviewer.persist.mapper.Point2DMapper")
  @Cascade({ CascadeType.ALL })
  private List<Point2D> points = new ArrayList<>();

  /**
   * Arrow at the beginning of the line.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ArrowTypeDataMapper")
  private ArrowTypeData beginAtd = new ArrowTypeData();

  /**
   * Arrow at the end of the line.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ArrowTypeDataMapper")
  private ArrowTypeData endAtd = new ArrowTypeData();

  /**
   * Width of the line.
   */
  private double width = 1;

  /**
   * Color of the line.
   */
  private Color color = Color.BLACK;

  /**
   * Type of the line (pattern used to draw it).
   */
  @Enumerated(EnumType.STRING)
  private LineType type = LineType.SOLID;

  /**
   * Default constructor for simple to points line.
   * 
   * @param startPoint
   *          start point of the line
   * @param endPoint
   *          end point of the line
   */
  public PolylineData(Point2D startPoint, Point2D endPoint) {
    addPoint(startPoint);
    addPoint(endPoint);
  }

  /**
   * Default constructor.
   */
  public PolylineData() {
  }

  /**
   * Constructor that creates object initialized by the parameter object.
   * 
   * @param param
   *          parameter used for data initialization
   */
  public PolylineData(PolylineData param) {
    points = new ArrayList<>();
    for (Point2D point : param.getPoints()) {
      addPoint(new Point2D.Double(point.getX(), point.getY()));
    }
    beginAtd = new ArrowTypeData(param.getBeginAtd());
    endAtd = new ArrowTypeData(param.getEndAtd());
    width = param.getWidth();
    color = param.getColor();
    type = param.getType();
  }

  /**
   * Creates line from the list of points.
   * 
   * @param pointsList
   *          list of points that represent the line
   */
  public PolylineData(List<Point2D> pointsList) {
    for (Point2D point2d : pointsList) {
      addPoint(point2d);
    }
  }

  /**
   * Adds a point to the line.
   * 
   * @param point
   *          point to add
   */
  public void addPoint(Point2D point) {
    addPoint(points.size(), point);
  }

  /**
   * Adds a point to the line at specific point (the rest will be shifted
   * accordingly).
   * 
   * @param point
   *          point to add
   * @param position
   *          where the point should be inserted
   */
  public void addPoint(int position, Point2D point) {
    if (!pt.isValidPoint(point)) {
      throw new InvalidArgumentException("Invalid coordinates: " + point);
    }
    points.add(position, point);
  }

  /**
   * Set new coordinates of the point inside line.
   * 
   * @param position
   *          which point should be changed
   * @param point
   *          new point value
   */
  public void setPoint(int position, Point2D point) {
    if (!pt.isValidPoint(point)) {
      throw new InvalidArgumentException("Invalid coordinates (NaN is not accepted): " + point);
    }
    points.set(position, point);
  }

  /**
   * Returns all segment lines as a list of {@link Line2D} objects.
   * 
   * @return list of segment lines
   */
  public List<Line2D> getLines() {
    List<Line2D> result = new ArrayList<>();
    if (points.size() > 1) {
      Point2D p1 = points.get(0);

      for (int i = 1; i < points.size(); i++) {
        Point2D p2 = points.get(i);
        Line2D line = new Line2D.Double(p1, p2);
        result.add(line);
        p1 = p2;
      }
    }
    return result;
  }

  /**
   * Return a line that correspond to the part of line.
   * 
   * @param from
   *          index of the point where line should start (included)
   * @param to
   *          index of the point where line should end (excluded)
   * @return line that correspond to the part of line
   */
  public PolylineData getSubline(int from, int to) {
    PolylineData result = new PolylineData(this);
    result.setPoints(new ArrayList<>());
    for (int i = Math.max(from, 0); i < Math.min(to, points.size()); i++) {
      result.addPoint(new Point2D.Double(points.get(i).getX(), points.get(i).getY()));
    }
    return result;
  }

  /**
   * Returns coordinates of the last point in the line.
   *
   * @return coordinates of the last point in the line
   */
  public Point2D getEndPoint() {
    return points.get(points.size() - 1);
  }

  /**
   * Sets the new value of the last point in the line.
   *
   * @param point
   *          new value of the last point in the line
   */
  public void setEndPoint(Point2D point) {
    setPoint(points.size() - 1, point);
  }

  /**
   * Transforms line representation into {@link GeneralPath} class.
   * 
   * @return {@link GeneralPath} representation of line
   */
  public GeneralPath toGeneralPath() {
    GeneralPath path = new GeneralPath();
    Point2D p = points.get(0);
    path.moveTo(p.getX(), p.getY());
    for (int i = 1; i < points.size(); i++) {
      p = points.get(i);
      path.lineTo(p.getX(), p.getY());
    }
    return path;
  }

  /**
   * Trims the end of the line.
   * 
   * @param distToTrim
   *          distance by which end of line should be trimmed
   */
  public double trimEnd(double distToTrim) {
    Point2D last = points.get(points.size() - 1);
    Point2D last2 = points.get(points.size() - 2);
    double oldDist = last.distance(last2);
    if (distToTrim < 0 && oldDist <= Configuration.EPSILON) {
      logger.debug("Cannot extend empty line");
      last.setLocation(last2);
      return 0;
    } else if (oldDist <= distToTrim) {
      last.setLocation(last2);
      return oldDist;
    } else {
      double newDistr = oldDist - distToTrim;
      double ratio = newDistr / oldDist;
      double dx = last.getX() - last2.getX();
      double dy = last.getY() - last2.getY();
      dx *= ratio;
      dy *= ratio;
      last.setLocation(last2.getX() + dx, last2.getY() + dy);
      return distToTrim;
    }
  }

  /**
   * Trims the beginning of the line.
   * 
   * @param distToTrim
   *          distance by which beginning of line should be trimmed
   */
  public double trimBegin(double distToTrim) {
    Point2D last = points.get(0);
    Point2D last2 = points.get(1);
    double oldDist = last.distance(last2);
    if (oldDist <= distToTrim) {
      last.setLocation(last2);
      return oldDist;
    } else {
      double newDistr = oldDist - distToTrim;
      double ratio = newDistr / oldDist;
      double dx = last.getX() - last2.getX();
      double dy = last.getY() - last2.getY();
      dx *= ratio;
      dy *= ratio;
      last.setLocation(last2.getX() + dx, last2.getY() + dy);
      return distToTrim;
    }
  }

  /**
   * Returns the object with reversed order of line.
   * 
   * @return reversed line
   */
  public PolylineData reverse() {
    PolylineData result = new PolylineData(this);
    List<Point2D> points = new ArrayList<>();
    for (int i = getPoints().size() - 1; i >= 0; i--) {
      points.add(getPoints().get(i));
    }
    result.setPoints(points);
    result.setBeginAtd(getEndAtd());
    result.setEndAtd(getBeginAtd());
    return result;
  }

  /**
   * Returns length of the whole line.
   * 
   * @return length of the line
   */
  public double length() {
    if (points.size() < 2) {
      return 0;
    }
    double dist = 0;
    Point2D point = points.get(0);
    for (int i = 1; i < points.size(); i++) {
      dist += point.distance(points.get(i));
      point = points.get(i);
    }
    return dist;
  }

  /**
   * Sets first point in the line.
   * 
   * @param point
   *          new first point in the line
   */
  public void setStartPoint(Point2D point) {
    setPoint(0, point);
  }

  /**
   * Returns first point in the line.
   * 
   * @return first point in the line
   */
  public Point2D getBeginPoint() {
    return points.get(0);
  }

  /**
   * @return the points
   * @see #points
   */
  public List<Point2D> getPoints() {
    return points;
  }

  /**
   * @param points
   *          the points to set
   * @see #points
   */
  public void setPoints(List<Point2D> points) {
    for (Point2D point : points) {
      if (!pt.isValidPoint(point)) {
        throw new InvalidArgumentException("Invalid coordinates: " + point);
      }
    }
    this.points = points;
  }

  /**
   * @return the beginAtd
   * @see #beginAtd
   */
  public ArrowTypeData getBeginAtd() {
    return beginAtd;
  }

  /**
   * @param beginAtd
   *          the beginAtd to set
   * @see #beginAtd
   */
  public void setBeginAtd(ArrowTypeData beginAtd) {
    this.beginAtd = beginAtd;
  }

  /**
   * @return the endAtd
   * @see #endAtd
   */
  public ArrowTypeData getEndAtd() {
    return endAtd;
  }

  /**
   * @param endAtd
   *          the endAtd to set
   * @see #endAtd
   */
  public void setEndAtd(ArrowTypeData endAtd) {
    this.endAtd = endAtd;
  }

  /**
   * @return the width
   * @see #width
   */
  public double getWidth() {
    return width;
  }

  /**
   * @param width
   *          the width to set
   * @see #width
   */
  public void setWidth(double width) {
    this.width = width;
  }

  /**
   * @param string
   *          the width to set
   * @see #width
   */
  public void setWidth(String string) {
    setWidth(Double.parseDouble(string));
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color
   *          the color to set
   * @see #color
   */
  public void setColor(Color color) {
    this.color = color;
  }

  /**
   * @return the type
   * @see #type
   */
  public LineType getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(LineType type) {
    this.type = type;
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Prepares a copy of the object.
   * 
   * @return copy of {@link PolylineData}
   */
  public PolylineData copy() {
    if (this.getClass().equals(PolylineData.class)) {
      return new PolylineData(this);
    } else {
      throw new NotImplementedException("Copy method for " + this.getClass() + " class not implemented.");
    }
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("[" + this.getClass().getSimpleName() + "]: ");
    for (Point2D point : points) {
      sb.append(point.getX() + ", " + point.getY() + " ");
    }
    return sb.toString();
  }

  @Override
  public Integer getZ() {
    return 0;
  }

  @Override
  public void setZ(Integer z) {
    logger.warn("Not implemented");
  }

  @Override
  public String getElementId() {
    return toString();
  }

  @Override
  public double getSize() {
    return 0;
  }
}
