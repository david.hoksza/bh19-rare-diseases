package lcsb.mapviewer.model.map.layout.graphics;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.*;

/**
 * Comparator of {@link LayerOval} class.
 * 
 * @author Piotr Gawron
 * 
 * 
 */
public class LayerOvalComparator extends Comparator<LayerOval> {

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public LayerOvalComparator(double epsilon) {
    super(LayerOval.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public LayerOvalComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(LayerOval arg0, LayerOval arg1) {
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);
    ColorComparator colorComparator = new ColorComparator();
    IntegerComparator integerComparator = new IntegerComparator();

    if (doubleComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      return doubleComparator.compare(arg0.getWidth(), arg1.getWidth());
    }

    if (doubleComparator.compare(arg0.getHeight(), arg1.getHeight()) != 0) {
      return doubleComparator.compare(arg0.getHeight(), arg1.getHeight());
    }

    if (doubleComparator.compare(arg0.getX(), arg1.getX()) != 0) {
      return doubleComparator.compare(arg0.getX(), arg1.getX());
    }

    if (doubleComparator.compare(arg0.getY(), arg1.getY()) != 0) {
      return doubleComparator.compare(arg0.getY(), arg1.getY());
    }

    if (integerComparator.compare(arg0.getZ(), arg1.getZ()) != 0) {
      return integerComparator.compare(arg0.getZ(), arg1.getZ());
    }

    if (colorComparator.compare(arg0.getColor(), arg1.getColor()) != 0) {
      return colorComparator.compare(arg0.getColor(), arg1.getColor());
    }

    return 0;
  }

}
