package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner dissociation. It must have at
 * least two reactants and one product.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("HETERODIMER_REACTION")
public class HeterodimerAssociationReaction extends Reaction implements TwoReactantReactionInterface {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public HeterodimerAssociationReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public HeterodimerAssociationReaction(Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Heterodimer association";
  }

  @Override
  public ReactionRect getReactionRect() {
    return ReactionRect.RECT_EMPTY;
  }

  @Override
  public HeterodimerAssociationReaction copy() {
    if (this.getClass() == HeterodimerAssociationReaction.class) {
      return new HeterodimerAssociationReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
