package lcsb.mapviewer.model.map.layout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.model.map.species.Element;

/**
 * This model element is used by cell designer but we ignore it. Maybe it will
 * be useful in the future.
 * 
 * @author Piotr Gawron
 * 
 */
public class ElementGroup implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * String identifier of the group.
   */
  private String idGroup;

  /**
   * List of elements in the group.
   */
  private List<Element> elements = new ArrayList<>();

  /**
   * @return the idGroup
   * @see #idGroup
   */
  public String getIdGroup() {
    return idGroup;
  }

  /**
   * @param idGroup
   *          the idGroup to set
   * @see #idGroup
   */
  public void setIdGroup(String idGroup) {
    this.idGroup = idGroup;
  }

  /**
   * @return the elements
   * @see #elements
   */
  public List<Element> getElements() {
    return elements;
  }

  /**
   * @param elements
   *          the elements to set
   * @see #elements
   */
  public void setElements(List<Element> elements) {
    this.elements = elements;
  }

  /**
   * Add element to the group.
   * 
   * @param element
   *          object to add to the group
   */
  public void addElement(Element element) {
    elements.add(element);
  }

}
