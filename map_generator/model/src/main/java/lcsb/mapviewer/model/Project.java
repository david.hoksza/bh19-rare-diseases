package lcsb.mapviewer.model;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.OrderBy;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.graphics.MapCanvasType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.user.User;

@Entity
public class Project implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(unique = true, nullable = false)
  private String projectId;

  @Cascade({ CascadeType.ALL })
  @OneToOne
  private MiriamData disease;

  private String name;

  private String version;

  @ManyToOne(optional = false)
  private User owner;

  private String notifyEmail;

  private String directory;

  /**
   * Status of the project used during uploading the project to the system.
   */
  @Enumerated(EnumType.STRING)
  private ProjectStatus status = ProjectStatus.UNKNOWN;

  /**
   * Progress of single step of project uploading.
   */
  private double progress = 0;

  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "project", orphanRemoval = true)
  private Set<ProjectLogEntry> logEntries = new HashSet<>();

  @Enumerated(EnumType.STRING)
  private MapCanvasType mapCanvasType = MapCanvasType.OPEN_LAYERS;

  private Calendar googleMapLicenseAcceptDate;

  private Calendar creationDate = Calendar.getInstance();

  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "project", orphanRemoval = true)
  private Set<ModelData> models = new HashSet<>();

  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "project", orphanRemoval = true)
  @OrderBy("id")
  private List<Layout> layouts = new ArrayList<>();

  @Cascade({ CascadeType.SAVE_UPDATE })
  @OneToOne(fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "file_entry_id")
  private UploadedFileEntry inputData;

  @Cascade({ CascadeType.ALL })
  @OneToOne
  private MiriamData organism;

  private boolean sbgnFormat = false;

  /**
   * List of overview images describing this {@link Project}. The elements on the
   * list can create a hierarchy that describe dependencies, however now matter if
   * the image is a top-level object or a leaf it should be placed on this list.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "project", orphanRemoval = true)
  @OrderBy("id")
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<OverviewImage> overviewImages = new ArrayList<>();

  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "project", orphanRemoval = true)
  @OrderBy("id")
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<Glyph> glyphs = new ArrayList<>();

  public Project() {
  }

  public Project(String projectId) {
    this.projectId = projectId;
  }

  public void addModel(ModelData model) {
    models.add(model);
    model.setProject(this);
  }

  public void addModel(Model model) {
    models.add(model.getModelData());
    model.setProject(this);
  }

  public void removeModel(ModelData model) {
    models.remove(model);
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public ProjectStatus getStatus() {
    return status;
  }

  public void setStatus(ProjectStatus status) {
    this.status = status;
  }

  public double getProgress() {
    return progress;
  }

  public void setProgress(double progress) {
    this.progress = progress;
  }

  public Set<ModelData> getModels() {
    return models;
  }

  public void setModels(Set<ModelData> models) {
    this.models = models;
  }

  public String getDirectory() {
    return directory;
  }

  public void setDirectory(String directory) {
    this.directory = directory;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public MiriamData getDisease() {
    return disease;
  }

  public void setDisease(MiriamData disease) {
    this.disease = disease;
  }

  public MiriamData getOrganism() {
    return organism;
  }

  public void setOrganism(MiriamData organism) {
    this.organism = organism;
  }

  public boolean isSbgnFormat() {
    return sbgnFormat;
  }

  public void setSbgnFormat(boolean sbgnFormat) {
    this.sbgnFormat = sbgnFormat;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public List<OverviewImage> getOverviewImages() {
    return overviewImages;
  }

  public void addOverviewImage(OverviewImage oi) {
    this.overviewImages.add(oi);
    oi.setProject(this);
  }

  public void addOverviewImages(List<OverviewImage> parseOverviewLinks) {
    for (OverviewImage overviewImage : parseOverviewLinks) {
      this.addOverviewImage(overviewImage);
    }

  }

  public UploadedFileEntry getInputData() {
    return inputData;
  }

  public void setInputData(UploadedFileEntry inputData) {
    this.inputData = inputData;
  }

  public String getNotifyEmail() {
    return notifyEmail;
  }

  public void setNotifyEmail(String notifyEmail) {
    this.notifyEmail = notifyEmail;
  }

  public MapCanvasType getMapCanvasType() {
    return mapCanvasType;
  }

  public void setMapCanvasType(MapCanvasType mapCanvasType) {
    MapCanvasType oldMapCanvasType = this.mapCanvasType;
    this.mapCanvasType = mapCanvasType;
    if (!mapCanvasType.equals(oldMapCanvasType)) {
      if (mapCanvasType.equals(MapCanvasType.GOOGLE_MAPS_API)) {
        this.setGoogleMapLicenseAcceptDate(Calendar.getInstance());
      } else {
        this.setGoogleMapLicenseAcceptDate(null);
      }
    }
  }

  public Calendar getGoogleMapLicenseAcceptDate() {
    return googleMapLicenseAcceptDate;
  }

  public void setGoogleMapLicenseAcceptDate(Calendar googleMapLicenseAcceptDate) {
    this.googleMapLicenseAcceptDate = googleMapLicenseAcceptDate;
  }

  public void addLayout(Layout layout) {
    layouts.add(layout);
    layout.setProject(this);
  }

  public List<Layout> getLayouts() {
    return layouts;
  }

  public void setLayouts(List<Layout> layouts) {
    this.layouts = layouts;
  }

  public void removeLayout(Layout layout) {
    this.layouts.remove(layout);
    layout.setProject(null);
  }

  public void addLayout(int index, Layout layout) {
    layouts.add(index, layout);
    layout.setProject(this);
  }

  public Layout getLayoutByIdentifier(Integer id) {
    for (Layout layout : getLayouts()) {
      if (id == layout.getId()) {
        return layout;
      }
    }
    return null;
  }

  public Calendar getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Calendar creationDate) {
    this.creationDate = creationDate;
  }

  public List<Glyph> getGlyphs() {
    return glyphs;
  }

  public void setGlyphs(List<Glyph> glyphs) {
    this.glyphs = glyphs;
  }

  public void addGlyph(Glyph parseGlyph) {
    this.glyphs.add(parseGlyph);
    parseGlyph.setProject(this);
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public Set<ProjectLogEntry> getLogEntries() {
    return logEntries;
  }

  public void setLogEntries(Set<ProjectLogEntry> logEntries) {
    this.logEntries = logEntries;
  }

  public void addLogEntry(ProjectLogEntry entry) {
    logEntries.add(entry);
    entry.setProject(this);

  }

  public void addLogEntries(Set<ProjectLogEntry> createLogEntries) {
    for (ProjectLogEntry projectLogEntry : createLogEntries) {
      addLogEntry(projectLogEntry);
    }
  }

}
