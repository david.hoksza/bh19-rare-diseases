package lcsb.mapviewer.model.map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.IntegerComparator;

/**
 * This class implements comparator interface for {@link OverviewModelLink}.
 * 
 * @author Piotr Gawron
 * 
 */
public class OverviewModelLinkComparator extends Comparator<OverviewModelLink> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(OverviewModelLinkComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public OverviewModelLinkComparator(double epsilon) {
    super(OverviewModelLink.class);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public OverviewModelLinkComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  public Comparator<?> getParentComparator() {
    return new OverviewLinkComparator(epsilon);
  }

  @Override
  protected int internalCompare(OverviewModelLink arg0, OverviewModelLink arg1) {
    int result = 0;
    IntegerComparator integerComparator = new IntegerComparator();
    if (integerComparator.compare(arg0.getxCoord(), arg1.getxCoord()) != 0) {
      logger.debug("xCoord different: " + arg0.getxCoord() + ", " + arg1.getxCoord());
      return integerComparator.compare(arg0.getxCoord(), arg1.getxCoord());
    }

    if (integerComparator.compare(arg0.getyCoord(), arg1.getyCoord()) != 0) {
      logger.debug("yCoord different: " + arg0.getyCoord() + ", " + arg1.getyCoord());
      return integerComparator.compare(arg0.getyCoord(), arg1.getyCoord());
    }

    if (integerComparator.compare(arg0.getZoomLevel(), arg1.getZoomLevel()) != 0) {
      logger.debug("zoomLevel different: " + arg0.getZoomLevel() + ", " + arg1.getZoomLevel());
      return integerComparator.compare(arg0.getZoomLevel(), arg1.getZoomLevel());
    }

    if (arg0.getLinkedModel() == null) {
      if (arg1.getLinkedModel() == null) {
        result = 0;
      } else {
        result = 1;
      }
    } else if (arg1.getLinkedModel() == null) {
      result = -1;
    } else {
      result = integerComparator.compare(arg0.getLinkedModel().getId(), arg1.getLinkedModel().getId());
    }

    if (result != 0) {
      return result;
    }

    return 0;

  }

}
