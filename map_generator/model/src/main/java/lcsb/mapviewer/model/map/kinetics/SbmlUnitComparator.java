package lcsb.mapviewer.model.map.kinetics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.common.comparator.StringComparator;

public class SbmlUnitComparator extends Comparator<SbmlUnit> {
  private static Logger logger = LogManager.getLogger(SbmlUnitComparator.class);

  public SbmlUnitComparator() {
    super(SbmlUnit.class);
  }

  @Override
  protected int internalCompare(SbmlUnit arg0, SbmlUnit arg1) {
    StringComparator stringComparator = new StringComparator();
    SetComparator<SbmlUnitTypeFactor> unitTypeFactorComparator = new SetComparator<>(
        new SbmlUnitTypeFactorComparator());
    if (stringComparator.compare(arg0.getUnitId(), arg1.getUnitId()) != 0) {
      logger.debug("unitId different");
      return stringComparator.compare(arg0.getUnitId(), arg1.getUnitId());
    }
    if (stringComparator.compare(arg0.getName(), arg1.getName()) != 0) {
      logger.debug("name different");
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

    if (unitTypeFactorComparator.compare(arg0.getUnitTypeFactors(), arg1.getUnitTypeFactors()) != 0) {
      logger.debug("unit type factor different");
      return unitTypeFactorComparator.compare(arg0.getUnitTypeFactors(), arg1.getUnitTypeFactors());
    }
    return 0;
  }

}
