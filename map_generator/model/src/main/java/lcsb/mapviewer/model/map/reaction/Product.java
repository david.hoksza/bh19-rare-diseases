package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Represents product of the reaction (in the reaction).
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("PRODUCT_NODE")
public class Product extends ReactionNode {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public Product() {
    super();
  }

  /**
   * Constructor that creates product for given {@link Species}. These elements
   * reference to the objects in the
   * {@link lcsb.mapviewer.model.map.model.db.model.map.Model Model} that
   * represents this product.
   * 
   * @param element
   *          element that represent this product
   */
  public Product(Element element) {
    super(element);
    if (element == null) {
      throw new InvalidArgumentException("Element cannot be null");
    }
  }

  /**
   * Constructor that initialize the product with data from parameter.
   * 
   * @param original
   *          original product used for data initalization
   */
  protected Product(Product original) {
    super(original);
  }

  @Override
  public Product copy() {
    if (this.getClass() == Product.class) {
      return new Product(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
