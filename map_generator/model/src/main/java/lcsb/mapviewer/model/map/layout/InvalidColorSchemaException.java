package lcsb.mapviewer.model.map.layout;

/**
 * Exception that should be thrown when
 * {@link lcsb.mapviewer.model.map.layout.ColorSchema} is invalid.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidColorSchemaException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public InvalidColorSchemaException(String string) {
    super(string);
  }

  /**
   * Constructor with message and super exception passed in the argument.
   * 
   * @param string
   *          message of this exception
   * @param e
   *          super exception that caused this
   */
  public InvalidColorSchemaException(String string, Exception e) {
    super(string, e);
  }

  /**
   * Constructor with exception passed in the argument.
   * 
   * @param e
   *          super exception that caused this
   */
  public InvalidColorSchemaException(Exception e) {
    super(e);
  }
}
