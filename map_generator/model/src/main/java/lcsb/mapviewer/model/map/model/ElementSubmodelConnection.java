package lcsb.mapviewer.model.map.model;

import java.io.Serializable;

import javax.persistence.*;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class defines connection between {@link Element} and submap (submodel).
 * This means that single {@link Element} should be "expandable" into map
 * represented by {@link SubmodelConnection#submodel} object.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("ELEMENT_SUBMODEL_LINK")
public class ElementSubmodelConnection extends SubmodelConnection implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * From which {@link Element} this connection start.
   */
  @ManyToOne
  private Element fromElement;

  /**
   * This object defines reference element in submodel that represents central (or
   * identical) object in reference submodel.
   */
  @ManyToOne
  private Element toElement;

  /**
   * Default constructor that initialize some fields.
   * 
   * @param submodel
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   */
  public ElementSubmodelConnection(ModelData submodel, SubmodelType type) {
    super(submodel, type);
  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param model
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   */
  public ElementSubmodelConnection(Model model, SubmodelType type) {
    super(model.getModelData(), type);
  }

  /**
   * Default constructor.
   */
  public ElementSubmodelConnection() {
  }

  /**
   * Constructor that creates copy of the {@link ElementSubmodelConnection}
   * object.
   * 
   * @param original
   *          original object from which copy is prepared
   */
  public ElementSubmodelConnection(ElementSubmodelConnection original) {
    super(original);
    this.setFromElement(original.getFromElement());
    this.setToElement(original.getToElement());
  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param submodel
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   * @param name
   *          {@link SubmodelConnection#name}
   */
  public ElementSubmodelConnection(Model submodel, SubmodelType type, String name) {
    super(submodel, type, name);
  }

  /**
   * @return the fromElement
   * @see #fromElement
   */
  public Element getFromElement() {
    return fromElement;
  }

  /**
   * @param fromElement
   *          the fromElement to set
   * @see #fromElement
   */
  public void setFromElement(Element fromElement) {
    this.fromElement = fromElement;
  }

  /**
   * @return the toElement
   * @see #toElement
   */
  public Element getToElement() {
    return toElement;
  }

  /**
   * @param toElement
   *          the toElement to set
   * @see #toElement
   */
  public void setToElement(Element toElement) {
    this.toElement = toElement;
  }

  @Override
  public ElementSubmodelConnection copy() {
    if (this.getClass() == ElementSubmodelConnection.class) {
      ElementSubmodelConnection result = new ElementSubmodelConnection();
      result.assignValuesFromOriginal(this);
      result.setFromElement(this.getFromElement());
      result.setToElement(this.getToElement());
      return result;
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }
}
