package lcsb.mapviewer.model.graphics;

/**
 * Arrow types used in the graphics representation. Available types are:
 * <ul>
 * <li>{@link ArrowType#BLANK BLANK},</li>
 * <li>{@link ArrowType#BLANK_CROSSBAR BLANK_CROSSBAR},</li>
 * <li>{@link ArrowType#CIRCLE CIRCLE},</li>
 * <li>{@link ArrowType#CROSSBAR CROSSBAR},</li>
 * <li>{@link ArrowType#DIAMOND DIAMOND},</li>
 * <li>{@link ArrowType#FULL FULL},</li>
 * <li>{@link ArrowType#FULL_CROSSBAR FULL_CROSSBAR},</li>
 * <li>{@link ArrowType#NONE NONE},</li>
 * <li>{@link ArrowType#OPEN OPEN}.</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
public enum ArrowType {
  // CHECKSTYLE:OFF
  /**
   * The arrow end that should look as an image bellow. <br/>
   * <br/>
   * <img alt="" src=
   * "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAAAUCAYAAADStFABAAAAVElEQVR42u3WsQoAIAhFUf//p21qM0miQd890NISXKQyA4B+/HJPMoIrxQgjbCox0ghBDJeNMD1IOcIhSHpAy1XVfTK+TwJ3Aq8D/wR+jFPDAHizAFMtRsiqJUUMAAAAAElFTkSuQmCC"
   * />
   */
  BLANK_CROSSBAR,

  /**
   * The arrow end that should look as an image bellow. <br/>
   * <br/>
   * <img alt="" src=
   * "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAAAUCAYAAADStFABAAAARUlEQVR42u3WsQoAIAhAQf//p21oaQmMpuwOXBofUUYAvCcl2EdIEeb5OiJ0j3EaoWWQmwilGPnhuAneBL+DPcHGCFA1AN/ehXvm3UsqAAAAAElFTkSuQmCC"
   * />
   * 
   */
  FULL_CROSSBAR,

  /**
   * The arrow end that should look as an image bellow. <br/>
   * <br/>
   * <img alt="" src=
   * "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAAAUCAYAAADStFABAAAASElEQVR42u3UwQkAIAxD0ey/dL0oKAhW8GLzH3SAhLYSAAAwEX1sg8fgVMYSfGcuKNXib3Or6nYcC8luQsnTcH6Qcg8PAHilAdXavETyaCxKAAAAAElFTkSuQmCC"
   * />
   * 
   */
  DIAMOND,

  /**
   * The arrow end that should look as an image bellow. <br/>
   * <br/>
   * <img alt="" src=
   * "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAAAUCAYAAADStFABAAAATElEQVR42u3XMQ4AEBAFUfe/9ChEo5DVSPjzEgcwEVZrkvQHTDAikB6DKTkGq8TTwU5KEKoqMXh1nfrxZERv3jvB18E5wYnRv4N0RwevuDvTQl9v5wAAAABJRU5ErkJggg=="
   * />
   */
  BLANK,

  /**
   * The arrow end that should look as an image bellow. <br/>
   * <br/>
   * <img alt="" src=
   * "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAAAUCAYAAADStFABAAAAMElEQVR42u3SQQ0AAAwCMfybZhK2/9oEAxcSAN6rBCKIIMIWoQ/nCRKIIIIIIgBXA1mQRrru0r/rAAAAAElFTkSuQmCC"
   * />
   * 
   */
  CROSSBAR,

  /**
   * The arrow end that should look as an image bellow. <br/>
   * <br/>
   * <img alt="" src=
   * "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAAAUCAYAAADStFABAAAASElEQVR42u3XgQkAIAzEwO6/9DtCVUQw5qALhIK2ShJMmuEH6NBDZBY1RFYRQ3wfIbtIIY5HyIvjJhjBCL4ORvDH6O3gFalLBns+vGCSjHRAAAAAAElFTkSuQmCC"
   * /> *
   */
  CIRCLE,

  /**
   * The arrow end that should look as an image bellow. <br/>
   * <br/>
   * <img alt="" src=
   * "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAAAUCAYAAADStFABAAAANklEQVR42u3UMQ4AQAQEQP//tGuUCrmSmcQDbJYIAFbJGoTRhyEQgXyeSx4cTfATLK7ulgeYehEYVKxEPu3IAAAAAElFTkSuQmCC"
   * />
   */
  OPEN,

  /**
   * The arrow end that should look as an image bellow. <br/>
   * <br/>
   * <img alt="" src=
   * "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAAAUCAYAAADStFABAAAAO0lEQVR42u3WsQ0AIAwDwey/tCloKF2TOykLvCLIDABfiQQ3wjsibI2RYkRoY2Th2ARvgt/BneBiBGgdPHh6hnF4gJ8AAAAASUVORK5CYII="
   * />
   */
  FULL,

  /**
   * The arrow end that should look as an image bellow. <br/>
   * <br/>
   * <img alt="" src=
   * "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAAAUCAYAAADStFABAAAAIUlEQVR42u3DsQ0AAAgDoP7/tJ5QdyEhAQCAZh4GAOBqAUSYPMTcmIDAAAAAAElFTkSuQmCC"
   * />
   * 
   */
  NONE

  // CHECKSTYLE:ON

}
