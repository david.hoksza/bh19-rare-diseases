package lcsb.mapviewer.model.user;

import java.awt.*;
import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;

import lcsb.mapviewer.model.security.Privilege;

@Entity
public class User implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(unique = true, nullable = false)
  private String login;

  @Column(nullable = false)
  private String cryptedPassword;

  private String name;

  private String surname;

  private String email;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#MIN_COLOR_VAL}. Used for coloring minimum
   * values in overlays.
   */
  private Color minColor;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#MAX_COLOR_VAL}. Used for coloring maximum
   * values in overlays.
   */
  private Color maxColor;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#NEUTRAL_COLOR_VAL}. Used for coloring neutral
   * values (0) in overlays.
   */
  private Color neutralColor;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#SIMPLE_COLOR_VAL}. Used for coloring overlays
   * without values and colors.
   */
  private Color simpleColor;

  private boolean removed = false;

  private boolean connectedToLdap = false;

  private boolean termsOfUseConsent = false;

  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "user_terms_of_use_consent", joinColumns = @JoinColumn(name = "user_id"))
  @Column(name = "date")
  @Cascade({ org.hibernate.annotations.CascadeType.ALL })
  private Set<Calendar> termsOfUseConsentDates = new HashSet<>();

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(name = "user_privilege_map_table", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "privilege_id"))
  private Set<Privilege> privileges = new HashSet<>();

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn
  private UserAnnotationSchema annotationSchema;

  public User() {
  }

  public void addPrivilege(Privilege privilege) {
    privileges.add(privilege);
  }

  public void removePrivilege(Privilege privilege) {
    privileges.remove(privilege);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getCryptedPassword() {
    return cryptedPassword;
  }

  public void setCryptedPassword(String cryptedPassword) {
    this.cryptedPassword = cryptedPassword;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Set<Privilege> getPrivileges() {
    return privileges;
  }

  public void setPrivileges(Set<Privilege> privileges) {
    this.privileges = privileges;
  }

  public boolean isRemoved() {
    return removed;
  }

  public void setRemoved(boolean removed) {
    this.removed = removed;
  }

  public UserAnnotationSchema getAnnotationSchema() {
    return annotationSchema;
  }

  public void setAnnotationSchema(UserAnnotationSchema annotationSchema) {
    this.annotationSchema = annotationSchema;
    if (!this.equals(annotationSchema.getUser())) {
      annotationSchema.setUser(this);
    }
  }

  @Override
  public String toString() {
    return "[" + this.getClass().getSimpleName() + "] " + getName() + " " + getSurname();
  }

  public Color getMinColor() {
    return minColor;
  }

  public void setMinColor(Color minColor) {
    this.minColor = minColor;
  }

  public Color getMaxColor() {
    return maxColor;
  }

  public void setMaxColor(Color maxColor) {
    this.maxColor = maxColor;
  }

  public Color getSimpleColor() {
    return simpleColor;
  }

  public void setSimpleColor(Color simpleColor) {
    this.simpleColor = simpleColor;
  }

  public Color getNeutralColor() {
    return neutralColor;
  }

  public void setNeutralColor(Color neutralColor) {
    this.neutralColor = neutralColor;
  }

  public boolean isTermsOfUseConsent() {
    return termsOfUseConsent;
  }

  public void setTermsOfUseConsent(boolean termsOfUseConsent) {
    if (!this.termsOfUseConsent && termsOfUseConsent) {
      this.getTermsOfUseConsentDates().add(Calendar.getInstance());
    }
    this.termsOfUseConsent = termsOfUseConsent;
  }

  public boolean isConnectedToLdap() {
    return connectedToLdap;
  }

  public void setConnectedToLdap(boolean connectedToLdap) {
    this.connectedToLdap = connectedToLdap;
  }

  public Set<Calendar> getTermsOfUseConsentDates() {
    return termsOfUseConsentDates;
  }

  public void setTermsOfUseConsentDates(Set<Calendar> termsOfUseConsentDates) {
    this.termsOfUseConsentDates = termsOfUseConsentDates;
  }

}
