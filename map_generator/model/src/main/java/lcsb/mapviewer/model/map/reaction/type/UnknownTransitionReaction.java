package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner unknown transition reaction. It
 * must have at least one reactant and one product.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("UNKNOWN_TRANSITION_REACTION")
public class UnknownTransitionReaction extends Reaction implements SimpleReactionInterface {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public UnknownTransitionReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public UnknownTransitionReaction(Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Unknown transition";
  }

  @Override
  public ReactionRect getReactionRect() {
    return ReactionRect.RECT_QUESTION;
  }

  @Override
  public UnknownTransitionReaction copy() {
    if (this.getClass() == UnknownTransitionReaction.class) {
      return new UnknownTransitionReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
