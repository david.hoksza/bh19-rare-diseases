package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner unknown positive influence
 * reaction. It must have at least one reactant and one product.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("UNKNOWN_POSITIVE_INFLUENCE")
public class UnknownPositiveInfluenceReaction extends Reaction implements SimpleReactionInterface, ReducedNotation {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public UnknownPositiveInfluenceReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public UnknownPositiveInfluenceReaction(Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Unknown positive influence";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public UnknownPositiveInfluenceReaction copy() {
    if (this.getClass() == UnknownPositiveInfluenceReaction.class) {
      return new UnknownPositiveInfluenceReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
