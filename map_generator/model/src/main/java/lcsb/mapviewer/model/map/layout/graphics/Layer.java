package lcsb.mapviewer.model.map.layout.graphics;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.model.ModelData;

/**
 * Object representing layer with additional graphics element (like, lines,
 * texts, etc.) in the model.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class Layer implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(Layer.class);

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Layer identifier (unique in single model).
   */
  private String layerId;

  /**
   * Layer name.
   */
  private String name;

  /**
   * Is the layer visible.
   */
  private boolean visible;

  /**
   * Is the layer locked (can be edited).
   */
  private boolean locked;

  /**
   * List of text objects on the layer.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER)
  @OrderColumn(name = "idx")
  @JoinTable(joinColumns = {
      @JoinColumn(name = "layer_table_id", referencedColumnName = "id", nullable = false, updatable = false) })
  private List<LayerText> texts = new ArrayList<>();

  /**
   * List of line objects on the layer.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER)
  @OrderColumn(name = "idx")
  @JoinTable(joinColumns = {
      @JoinColumn(name = "layer_table_id", referencedColumnName = "id", nullable = false, updatable = false) })
  private List<PolylineData> lines = new ArrayList<>();

  /**
   * List of rectangle objects on the layer.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER)
  @OrderColumn(name = "idx")
  @JoinTable(joinColumns = {
      @JoinColumn(name = "layer_table_id", referencedColumnName = "id", nullable = false, updatable = false) })
  private List<LayerRect> rectangles = new ArrayList<>();

  /**
   * List of oval objects on the layer.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER)
  @OrderColumn(name = "idx")
  @JoinTable(joinColumns = {
      @JoinColumn(name = "layer_table_id", referencedColumnName = "id", nullable = false, updatable = false) })
  private List<LayerOval> ovals = new ArrayList<>();

  /**
   * ModelData to which layer belongs to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData model;

  /**
   * Default constructor.
   */
  public Layer() {
  }

  /**
   * Constructor that copies data from the parameter.
   * 
   * @param layer
   *          from this parameter layer data will be copied
   */
  public Layer(Layer layer) {
    layerId = layer.getLayerId();
    name = layer.getName();
    visible = layer.isVisible();
    locked = layer.isLocked();

    for (LayerText lt : layer.getTexts()) {
      addLayerText(lt.copy());
    }

    for (PolylineData lt : layer.getLines()) {
      addLayerLine(lt.copy());
    }

    for (LayerRect lt : layer.getRectangles()) {
      addLayerRect(lt.copy());
    }

    for (LayerOval lt : layer.getOvals()) {
      addLayerOval(lt.copy());
    }

    model = layer.getModel();
  }

  /**
   * Makes copy of the layer.
   * 
   * @return copy of the layer
   */
  public Layer copy() {
    if (this.getClass() == Layer.class) {
      return new Layer(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the model
   * @see #model
   */
  public ModelData getModel() {
    return model;
  }

  /**
   * @param model
   *          the model to set
   * @see #model
   */
  public void setModel(ModelData model) {
    this.model = model;
  }

  /**
   * @return the ovals
   * @see #ovals
   */
  public List<LayerOval> getOvals() {
    return ovals;
  }

  /**
   * @param ovals
   *          the ovals to set
   * @see #ovals
   */
  public void setOvals(List<LayerOval> ovals) {
    this.ovals = ovals;
  }

  /**
   * @return the rectangles
   * @see #rectangles
   */
  public List<LayerRect> getRectangles() {
    return rectangles;
  }

  /**
   * @param rectangles
   *          the rectangles to set
   * @see #rectangles
   */
  public void setRectangles(List<LayerRect> rectangles) {
    this.rectangles = rectangles;
  }

  /**
   * @return the lines
   * @see #lines
   */
  public List<PolylineData> getLines() {
    return lines;
  }

  /**
   * @param lines
   *          the lines to set
   * @see #lines
   */
  public void setLines(List<PolylineData> lines) {
    this.lines = lines;
  }

  /**
   * @return the texts
   * @see #texts
   */
  public List<LayerText> getTexts() {
    return texts;
  }

  /**
   * @param texts
   *          the texts to set
   * @see #texts
   */
  public void setTexts(List<LayerText> texts) {
    this.texts = texts;
  }

  /**
   * @return the locked
   * @see #locked
   */
  public boolean isLocked() {
    return locked;
  }

  /**
   * @param locked
   *          the locked to set
   * @see #locked
   */
  public void setLocked(boolean locked) {
    this.locked = locked;
  }

  /**
   * Sets locked param from the text input.
   *
   * @param param
   *          text representing true/false
   * @see #locked
   */
  public void setLocked(String param) {
    locked = param.equalsIgnoreCase("TRUE");
  }

  /**
   * @return the visible
   * @see #visible
   */
  public boolean isVisible() {
    return visible;
  }

  /**
   * @param visible
   *          the visible to set
   * @see #visible
   */
  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  /**
   * Sets visible param from the text input.
   *
   * @param param
   *          text representing true/false
   * @see #visible
   */
  public void setVisible(String param) {
    visible = param.equalsIgnoreCase("TRUE");
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Adds text to the layer.
   * 
   * @param layerText
   *          text to add
   */
  public void addLayerText(LayerText layerText) {
    texts.add(layerText);
  }

  /**
   * Adds rectangle to the layer.
   * 
   * @param layerRect
   *          rectangle to add
   */
  public void addLayerRect(LayerRect layerRect) {
    rectangles.add(layerRect);
  }

  /**
   * Adds oval to the layer.
   * 
   * @param layerOval
   *          oval to add
   */
  public void addLayerOval(LayerOval layerOval) {
    ovals.add(layerOval);
  }

  /**
   * Adds line to the layer.
   * 
   * @param layerLine
   *          line to add
   */
  public void addLayerLine(PolylineData layerLine) {
    lines.add(layerLine);
  }

  /**
   * @return the layerId
   * @see #layerId
   */
  public String getLayerId() {
    return layerId;
  }

  /**
   * @param layerId
   *          the layerId to set
   * @see #layerId
   */
  public void setLayerId(String layerId) {
    this.layerId = layerId;
  }

  /**
   * Adds lines to the layer.
   * 
   * @param lines
   *          lines to add
   */
  public void addLayerLines(Collection<PolylineData> lines) {
    for (PolylineData layerLine : lines) {
      addLayerLine(layerLine);
    }
  }

  /**
   * Removes {@link LayerText} from {@link Layer}.
   * 
   * @param toRemove
   *          object to remove
   */
  public void removeLayerText(LayerText toRemove) {
    texts.remove(toRemove);
  }

  public boolean isEmpty() {
    return this.getOvals().size() == 0 && this.getRectangles().size() == 0 && this.getTexts().size() == 0
        && this.getLines().size() == 0;
  }

  public Set<Drawable> getDrawables() {
    Set<Drawable> result = new HashSet<>();
    result.addAll(lines);
    result.addAll(ovals);
    result.addAll(rectangles);
    result.addAll(texts);
    return result;
  }
}
