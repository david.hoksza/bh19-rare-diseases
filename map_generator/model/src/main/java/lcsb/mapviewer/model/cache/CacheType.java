package lcsb.mapviewer.model.cache;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Defines types of objects stored in the cache and interfaces used for
 * refreshing them.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class CacheType implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Class of object that will refresh the data of given type.
   */
  private String className;

  /**
   * Unique local database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * How many days cached value is valid.
   */
  private int validity;

  /**
   * @return the validity
   * @see #validity
   */
  public int getValidity() {
    return validity;
  }

  /**
   * @param validity
   *          the validity to set
   * @see #validity
   */
  public void setValidity(int validity) {
    this.validity = validity;
  }

  /**
   * @return the className
   * @see #className
   */
  public String getClassName() {
    return className;
  }

  /**
   * @param className
   *          the className to set
   * @see #className
   */
  public void setClassName(String className) {
    this.className = className;
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return className + " [id: " + id + "]";
  }

}
