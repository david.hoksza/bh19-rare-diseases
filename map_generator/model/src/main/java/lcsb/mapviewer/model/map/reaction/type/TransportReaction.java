package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner transport reaction. It must have
 * at least one reactant and one product.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("TRANSPORT_REACTION")
public class TransportReaction extends Reaction implements SimpleReactionInterface {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public TransportReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public TransportReaction(Reaction result) {
    super(result);
  }

  public TransportReaction(String reactionId) {
    super(reactionId);
  }

  @Override
  public String getStringType() {
    return "Transport";
  }

  @Override
  public ReactionRect getReactionRect() {
    return ReactionRect.RECT_EMPTY;
  }

  @Override
  public TransportReaction copy() {
    if (this.getClass() == TransportReaction.class) {
      return new TransportReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
