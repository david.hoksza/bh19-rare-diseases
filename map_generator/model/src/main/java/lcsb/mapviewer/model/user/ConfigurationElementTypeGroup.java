package lcsb.mapviewer.model.user;

public enum ConfigurationElementTypeGroup {
  EMAIL_NOTIFICATION("Email notification details"),
  DEFAULT_USER_PRIVILEGES("Default user privileges"),
  LEGEND_AND_LOGO("Legend and logo"),
  OVERLAYS("Overlays"),
  POINT_AND_CLICK("Point and click"),
  SERVER_CONFIGURATION("Server configuration"),
  LDAP_CONFIGURATION("LDAP configuration"),
  SEARCH_VISIBLE_PARAMETERS("Search panel options"),

  ;
  private String commonName;

  ConfigurationElementTypeGroup(String commonName) {
    this.commonName = commonName;
  }

  public String getCommonName() {
    return commonName;
  }

  public void setCommonName(String commonName) {
    this.commonName = commonName;
  }
}
