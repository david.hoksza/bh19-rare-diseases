/**
 * Provides data structure for {@link lcsb.mapviewer.model.map.reaction.Reaction
 * Reaction} modifier.
 */
package lcsb.mapviewer.model.map.modifier;
