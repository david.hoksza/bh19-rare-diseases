/**
 * Contains structures used for storing cached data.
 */
package lcsb.mapviewer.model.cache;
