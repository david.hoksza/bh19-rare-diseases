package lcsb.mapviewer.model.map.layout;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Class describing single gene variation.
 * 
 * @author Piotr Gawron
 *
 */
public class GeneVariation implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(GeneVariation.class);
  /**
   * Variation position in the genome.
   */
  private Long position;

  /**
   * Original DNA (from the reference genome).
   */
  private String originalDna;

  /**
   * Alternative DNA (suggested by the variant).
   */
  private String modifiedDna;

  /**
   * Contig where variant was observed.
   */
  private String contig;

  /**
   * Allele frequency of the variant.
   */
  private String allelFrequency;

  /**
   * Amino acid change for corresponding protein.
   */
  private String aminoAcidChange;

  /**
   * Gene variant id.
   */
  private String variantIdentifier;

  /**
   * Reference genome type.
   */
  private ReferenceGenomeType referenceGenomeType;

  /**
   * Version of the reference genome.
   */
  private String referenceGenomeVersion;

  /**
   * Constructor that creates copy of the {@link GeneVariation}.
   * 
   * @param original
   *          original {@link GeneVariation}
   */
  protected GeneVariation(GeneVariation original) {
    this.setPosition(original.getPosition());
    this.setOriginalDna(original.getOriginalDna());
    this.setModifiedDna(original.getModifiedDna());
    this.setReferenceGenomeType(original.getReferenceGenomeType());
    this.setReferenceGenomeVersion(original.getReferenceGenomeVersion());
    this.setContig(original.getContig());
    this.setAllelFrequency(original.getAllelFrequency());
    this.setVariantIdentifier(original.getVariantIdentifier());
    this.setAminoAcidChange(original.getAminoAcidChange());
  }

  /**
   * Default constructor.
   */
  public GeneVariation() {
  }

  /**
   * @return the position
   * @see #position
   */
  public Long getPosition() {
    return position;
  }

  /**
   * @param position
   *          the position to set
   * @see #position
   */
  public void setPosition(Long position) {
    this.position = position;
  }

  /**
   * Sets {@link #position}.
   *
   * @param position
   *          new position value
   */
  public void setPosition(int position) {
    this.position = (long) position;
  }

  /**
   * @return the originalDna
   * @see #originalDna
   */
  public String getOriginalDna() {
    return originalDna;
  }

  /**
   * @param originalDna
   *          the originalDna to set
   * @see #originalDna
   */
  public void setOriginalDna(String originalDna) {
    this.originalDna = originalDna;
  }

  /**
   * @return the modifiedDna
   * @see #modifiedDna
   */
  public String getModifiedDna() {
    return modifiedDna;
  }

  /**
   * @param modifiedDna
   *          the modifiedDna to set
   * @see #modifiedDna
   */
  public void setModifiedDna(String modifiedDna) {
    this.modifiedDna = modifiedDna;
  }

  /**
   * @return the referenceGenomeType
   * @see #referenceGenomeType
   */
  public ReferenceGenomeType getReferenceGenomeType() {
    return referenceGenomeType;
  }

  /**
   * @param referenceGenomeType
   *          the referenceGenomeType to set
   * @see #referenceGenomeType
   */
  public void setReferenceGenomeType(ReferenceGenomeType referenceGenomeType) {
    this.referenceGenomeType = referenceGenomeType;
  }

  /**
   * @return the referenceGenomeVersion
   * @see #referenceGenomeVersion
   */
  public String getReferenceGenomeVersion() {
    return referenceGenomeVersion;
  }

  /**
   * @param referenceGenomeVersion
   *          the referenceGenomeVersion to set
   * @see #referenceGenomeVersion
   */
  public void setReferenceGenomeVersion(String referenceGenomeVersion) {
    this.referenceGenomeVersion = referenceGenomeVersion;
  }

  /**
   * Creates copy of the object.
   *
   * @return copy of the object
   */
  public GeneVariation copy() {
    if (this.getClass().equals(GeneVariation.class)) {
      return new GeneVariation(this);
    } else {
      throw new NotImplementedException("Copy method not implemented for class: " + this.getClass());
    }
  }

  /**
   * @return the contig
   * @see #contig
   */
  public String getContig() {
    return contig;
  }

  /**
   * @param contig
   *          the contig to set
   * @see #contig
   */
  public void setContig(String contig) {
    this.contig = contig;
  }

  /**
   * @return the allelFrequency
   * @see #allelFrequency
   */
  public String getAllelFrequency() {
    return allelFrequency;
  }

  /**
   * @param allelFrequency
   *          the allelFrequency to set
   * @see #allelFrequency
   */
  public void setAllelFrequency(String allelFrequency) {
    this.allelFrequency = allelFrequency;
  }

  /**
   * @return the variantIdentifier
   * @see #variantIdentifier
   */
  public String getVariantIdentifier() {
    return variantIdentifier;
  }

  /**
   * @param variantIdentifier
   *          the variantIdentifier to set
   * @see #variantIdentifier
   */
  public void setVariantIdentifier(String variantIdentifier) {
    this.variantIdentifier = variantIdentifier;
  }

  public String getAminoAcidChange() {
    return aminoAcidChange;
  }

  public void setAminoAcidChange(String aminoAcidChange) {
    this.aminoAcidChange = aminoAcidChange;
  }

}
