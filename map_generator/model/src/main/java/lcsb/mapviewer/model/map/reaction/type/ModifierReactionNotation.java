package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.model.map.species.Phenotype;

/**
 * Interface used for marking {@link lcsb.mapviewer.model.map.reaction.Reaction
 * Reaction} extensions that implements something that should be modifier but is
 * a reaction to {@link Phenotype}.
 * 
 * @author Piotr Gawron
 * 
 */
public interface ModifierReactionNotation {

}
