package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Class representing operator between two input nodes in
 * {@link lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction
 * HeterodimerAssociationReaction}.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("ASSOCIATION_OPERATOR_NODE")
public class AssociationOperator extends NodeOperator {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(AssociationOperator.class);

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param operator
   *          parent operator from which we copy data
   */
  public AssociationOperator(AssociationOperator operator) {
    super(operator);
  }

  /**
   * Default constructor.
   */
  public AssociationOperator() {
    super();
  }

  @Override
  public String getOperatorText() {
    return "&";
  }

  @Override
  public AssociationOperator copy() {
    if (this.getClass() == AssociationOperator.class) {
      return new AssociationOperator(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
