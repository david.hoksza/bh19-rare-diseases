package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Class representing "and operator" between two or more nodes in the reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("AND_OPERATOR_NODE")
public class AndOperator extends NodeOperator {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(AndOperator.class);

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param operator
   *          parent operator from which we copy data
   */
  public AndOperator(AndOperator operator) {
    super(operator);
  }

  /**
   * Default constructor.
   */
  public AndOperator() {
    super();
  }

  @Override
  public String getOperatorText() {
    return "&";
  }

  @Override
  public String getSBGNOperatorText() {
    return "AND";
  }

  @Override
  public AndOperator copy() {
    if (this.getClass() == AndOperator.class) {
      return new AndOperator(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
