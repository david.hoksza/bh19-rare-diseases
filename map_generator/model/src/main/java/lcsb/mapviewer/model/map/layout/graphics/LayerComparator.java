package lcsb.mapviewer.model.map.layout.graphics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.*;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.PolylineDataComparator;

/**
 * Comparator of {@link Layer} class.
 * 
 * @author Piotr Gawron
 * 
 * 
 */
public class LayerComparator extends Comparator<Layer> {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(LayerComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public LayerComparator(double epsilon) {
    super(Layer.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public LayerComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(Layer arg0, Layer arg1) {
    StringComparator stringComparator = new StringComparator();
    BooleanComparator booleanComparator = new BooleanComparator();
    IntegerComparator integerComparator = new IntegerComparator();

    if (stringComparator.compare(arg0.getLayerId(), arg1.getLayerId()) != 0) {
      logger.debug("layer ids different: " + arg0.getLayerId() + ", " + arg1.getLayerId());
      return stringComparator.compare(arg0.getLayerId(), arg1.getLayerId());
    }

    if (stringComparator.compare(arg0.getName(), arg1.getName()) != 0) {
      logger.debug("layer name different: " + arg0.getName() + ", " + arg1.getName());
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

    if (booleanComparator.compare(arg0.isVisible(), arg1.isVisible()) != 0) {
      logger.debug("layer visibility different: " + arg0.isVisible() + ", " + arg1.isVisible());
      return booleanComparator.compare(arg0.isVisible(), arg1.isVisible());
    }

    if (booleanComparator.compare(arg0.isLocked(), arg1.isLocked()) != 0) {
      logger.debug("layer locked different: " + arg0.isLocked() + ", " + arg1.isLocked());
      return booleanComparator.compare(arg0.isLocked(), arg1.isLocked());
    }

    if (integerComparator.compare(arg0.getTexts().size(), arg1.getTexts().size()) != 0) {
      logger.debug("layer texts different: " + arg0.getTexts().size() + ", " + arg1.getTexts().size());
      return integerComparator.compare(arg0.getTexts().size(), arg1.getTexts().size());
    }
    if (integerComparator.compare(arg0.getLines().size(), arg1.getLines().size()) != 0) {
      logger.debug("layer lines different: " + arg0.getLines().size() + ", " + arg1.getLines().size());
      return integerComparator.compare(arg0.getLines().size(), arg1.getLines().size());
    }
    if (integerComparator.compare(arg0.getRectangles().size(), arg1.getRectangles().size()) != 0) {
      logger.debug("layer rectangles different: " + arg0.getRectangles().size() + ", " + arg1.getRectangles().size());
      return integerComparator.compare(arg0.getRectangles().size(), arg1.getRectangles().size());
    }
    if (integerComparator.compare(arg0.getOvals().size(), arg1.getOvals().size()) != 0) {
      logger.debug("layer ovals different: " + arg0.getOvals().size() + ", " + arg1.getOvals().size());
      return integerComparator.compare(arg0.getOvals().size(), arg1.getOvals().size());
    }

    ListComparator<LayerText> textComparator = new ListComparator<>(new LayerTextComparator(epsilon));
    int status = textComparator.compare(arg0.getTexts(), arg1.getTexts());
    if (status != 0) {
      logger.debug("layer texts different");
      return status;
    }

    ListComparator<LayerOval> ovalComparator = new ListComparator<>(new LayerOvalComparator(epsilon));
    status = ovalComparator.compare(arg0.getOvals(), arg1.getOvals());
    if (status != 0) {
      logger.debug("layer ovals different");
      return status;
    }

    ListComparator<LayerRect> rectComparator = new ListComparator<>(new LayerRectComparator(epsilon));
    status = rectComparator.compare(arg0.getRectangles(), arg1.getRectangles());
    if (status != 0) {
      logger.debug("layer rectangles different ");
      return status;
    }

    ListComparator<PolylineData> lineComparator = new ListComparator<>(new PolylineDataComparator(epsilon));
    status = lineComparator.compare(arg0.getLines(), arg1.getLines());
    if (status != 0) {
      logger.debug("layer lines different ");
      return status;
    }

    return 0;
  }

}
