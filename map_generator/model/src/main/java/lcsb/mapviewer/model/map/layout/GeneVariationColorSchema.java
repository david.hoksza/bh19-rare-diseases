package lcsb.mapviewer.model.map.layout;

import java.util.*;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Information about coloring element accoridng to genetic variation
 * information.
 * 
 * @author Piotr Gawron
 *
 */
public class GeneVariationColorSchema extends ColorSchema {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of {@link GeneVariation gene variations} used in this "coloring".
   */
  private List<GeneVariation> geneVariations = new ArrayList<>();

  /**
   * Default constructor.
   */
  public GeneVariationColorSchema() {
  }

  /**
   * Constructor that creates copy of the object.
   * 
   * @param schema
   *          original object from which data will be copied
   */
  public GeneVariationColorSchema(GeneVariationColorSchema schema) {
    super(schema);
    this.addGeneVariations(schema.getGeneVariations());
  }

  /**
   * Adds {@link GeneVariation gene variations} to {@link #geneVariations}.
   * 
   * @param geneVariations2
   *          collection of {@link GeneVariation} objects to add
   */
  public void addGeneVariations(Collection<GeneVariation> geneVariations2) {
    for (GeneVariation geneVariation : geneVariations2) {
      addGeneVariation(geneVariation.copy());
    }
  }

  @Override
  public GeneVariationColorSchema copy() {
    if (this.getClass().equals(GeneVariationColorSchema.class)) {
      return new GeneVariationColorSchema(this);
    } else {
      throw new NotImplementedException("Copy not implemented for class: " + this.getClass());
    }
  }

  /**
   * @return the geneVariations
   * @see #geneVariations
   */
  public List<GeneVariation> getGeneVariations() {
    return geneVariations;
  }

  /**
   * @param geneVariations
   *          the geneVariations to set
   * @see #geneVariations
   */
  public void setGeneVariations(List<GeneVariation> geneVariations) {
    this.geneVariations = geneVariations;
  }

  /**
   * Adds {@link GeneVariation} to list gene variations.
   * 
   * @param gv
   *          object to add
   */
  public void addGeneVariation(GeneVariation gv) {
    this.geneVariations.add(gv);
  }
}
