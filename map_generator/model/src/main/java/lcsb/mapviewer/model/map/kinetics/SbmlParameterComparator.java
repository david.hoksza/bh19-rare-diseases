package lcsb.mapviewer.model.map.kinetics;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.StringComparator;

public class SbmlParameterComparator extends Comparator<SbmlParameter> {

  public SbmlParameterComparator() {
    super(SbmlParameter.class);
  }

  @Override
  protected int internalCompare(SbmlParameter arg0, SbmlParameter arg1) {
    StringComparator stringComparator = new StringComparator();
    DoubleComparator doubleComparator = new DoubleComparator();
    SbmlUnitComparator unitComparator = new SbmlUnitComparator();
    if (stringComparator.compare(arg0.getParameterId(), arg1.getParameterId()) != 0) {
      return stringComparator.compare(arg0.getParameterId(), arg1.getParameterId());
    }
    if (stringComparator.compare(arg0.getName(), arg1.getName()) != 0) {
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

    if (doubleComparator.compare(arg0.getValue(), arg1.getValue()) != 0) {
      return doubleComparator.compare(arg0.getValue(), arg1.getValue());
    }

    if (unitComparator.compare(arg0.getUnits(), arg1.getUnits()) != 0) {
      return unitComparator.compare(arg0.getUnits(), arg1.getUnits());
    }

    return 0;
  }

}
