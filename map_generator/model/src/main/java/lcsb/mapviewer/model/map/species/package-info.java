/**
 * Provides data structure for elements on the map. The generic class from which
 * all element classes inherit data is
 * {@link lcsb.mapviewer.db.model.map.species.Element Element}. However, for
 * species there is another class that inherits from this class:
 * {@link lcsb.mapviewer.model.map.species.Species}. The package also contains
 * comparators for these elements that compare the data of the structures, but
 * ignore database-related information (like identifiers).
 */
package lcsb.mapviewer.model.map.species;