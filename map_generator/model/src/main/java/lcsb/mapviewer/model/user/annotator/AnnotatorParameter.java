package lcsb.mapviewer.model.user.annotator;

import java.io.Serializable;

import javax.persistence.*;

import lcsb.mapviewer.model.user.UserAnnotationSchema;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_distinguisher", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("ABSTRACT_PARAMETER")
public abstract class AnnotatorParameter implements Serializable, Comparable<AnnotatorParameter> {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  /**
   * Order at which the element should appear on the {@link #annotatorData} list
   * of parameters.
   */
  private int orderPosition;

  /**
   * {@link UserAnnotationSchema} that defines which user is using this set of
   * annotators.
   */
  @ManyToOne
  private AnnotatorData annotatorData;

  /**
   * @return the id
   * @see #id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the annotationSchema
   * @see #annotatorData
   */
  public AnnotatorData getAnnotatorData() {
    return annotatorData;
  }

  /**
   * @param annotatorData
   *          the annotationSchema to set
   * @see #annotationSchema
   */
  public void setAnnotatorData(AnnotatorData annotatorData) {
    this.annotatorData = annotatorData;
  }

  public int getOrderPosition() {
    return orderPosition;
  }

  public void setOrderPosition(int order) {
    this.orderPosition = order;
  }

  @Override
  public int compareTo(AnnotatorParameter o) {
    return this.getOrderPosition() - o.getOrderPosition();
  }

}
