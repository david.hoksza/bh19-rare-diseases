package lcsb.mapviewer.model.map.model;

import java.util.Comparator;

import javax.persistence.*;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * This class defines connection between super-model (supermap) and sub-model
 * (submap).
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("MODEL_SUBMODEL_LINK")
public class ModelSubmodelConnection extends SubmodelConnection {

  public static final Comparator<? super ModelSubmodelConnection> ID_COMPARATOR = new Comparator<ModelSubmodelConnection>() {

    @Override
    public int compare(ModelSubmodelConnection o1, ModelSubmodelConnection o2) {
      return o1.getId() - o2.getId();
    }
  };
  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Super (parent) model.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData parentModel;

  /**
   * Default constructor.
   */
  public ModelSubmodelConnection() {

  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param submodel
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   */
  public ModelSubmodelConnection(ModelData submodel, SubmodelType type) {
    super(submodel, type);
  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param model
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   */
  public ModelSubmodelConnection(Model model, SubmodelType type) {
    super(model.getModelData(), type);
  }

  /**
   * Constructor that creates copy of the {@link ModelSubmodelConnection} object.
   * 
   * @param original
   *          original object from which copy is prepared
   */
  public ModelSubmodelConnection(ModelSubmodelConnection original) {
    super(original);
    this.setParentModel(original.getParentModel());
  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param submodel
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   * @param name
   *          {@link SubmodelConnection#name}
   */
  public ModelSubmodelConnection(Model submodel, SubmodelType type, String name) {
    super(submodel, type, name);
  }

  /**
   * @return the parentModel
   * @see #parentModel
   */
  public ModelData getParentModel() {
    return parentModel;
  }

  /**
   * @param parentModel
   *          the parentModel to set
   * @see #parentModel
   */
  public void setParentModel(ModelData parentModel) {
    this.parentModel = parentModel;
  }

  /**
   * @param model
   *          the parent model to set
   * @see #parentModel
   */
  public void setParentModel(Model model) {
    setParentModel(model.getModelData());
  }

  @Override
  public ModelSubmodelConnection copy() {
    if (this.getClass() == ModelSubmodelConnection.class) {
      return new ModelSubmodelConnection(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }
}
