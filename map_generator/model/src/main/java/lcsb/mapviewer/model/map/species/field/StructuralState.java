package lcsb.mapviewer.model.map.species.field;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.text.DecimalFormat;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Type;

import lcsb.mapviewer.model.map.species.Species;

/**
 * This class represent structural state of a {@link Species}.
 * 
 * @author Piotr Gawron
 * 
 */

@Entity
public class StructuralState implements Serializable {

  private static final long serialVersionUID = 1L;

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(nullable = false)
  private String value;

  @Type(type = "lcsb.mapviewer.persist.mapper.Point2DMapper")
  @Column(nullable = false)
  private Point2D position = null;

  @Column(nullable = false)
  private Double width;

  @Column(nullable = false)
  private Double height;

  @Column(nullable = false)
  private Double fontSize;

  @OneToOne(fetch = FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL, mappedBy = "structuralState", optional = false)
  private Species species;

  public StructuralState() {
  }

  /**
   * Constructor that initialize object with the data taken from the parameter.
   * 
   * @param mr
   *          original object from which data is taken
   */
  public StructuralState(StructuralState mr) {
    this.value = mr.getValue();
    this.position = mr.position;
    this.width = mr.getWidth();
    this.height = mr.getHeight();
    this.fontSize = mr.getFontSize();
  }

  public Species getSpecies() {
    return species;
  }

  public void setSpecies(Species species) {
    this.species = species;
  }

  public Point2D getPosition() {
    return position;
  }

  public void setPosition(Point2D position) {
    this.position = position;
  }

  public StructuralState copy() {
    return new StructuralState(this);
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    DecimalFormat format = new DecimalFormat("#.##");
    String positionString = "Point2D[null]";
    if (getPosition() != null) {
      positionString = "Point2D[" + format.format(getPosition().getX()) + "," + format.format(getPosition().getY())
          + "]";
    }
    String result = getValue() + "," + positionString;
    return result;
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(Double width) {
    this.width = width;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(Double height) {
    this.height = height;
  }

  public void setHeight(Integer height) {
    this.height = height.doubleValue();
  }

  public Double getFontSize() {
    return fontSize;
  }

  public void setFontSize(Double fontSize) {
    this.fontSize = fontSize;
  }

  public void setFontSize(int fontSize) {
    this.fontSize = (double) fontSize;
  }

  public Point2D getCenter() {
    return new Point2D.Double(position.getX() + width / 2, position.getY() + height / 2);
  }

}
