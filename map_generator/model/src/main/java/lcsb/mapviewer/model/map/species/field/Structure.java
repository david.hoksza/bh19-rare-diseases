package lcsb.mapviewer.model.map.species.field;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.*;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * This class stores structure information as obtained from the SIFTS API
 * (https://www.ebi.ac.uk/pdbe/api/doc/sifts.html best_structures), which
 * provides the following fields pdb_id: the PDB ID which maps to the UniProt ID
 * chain_id: the specific chain of the PDB which maps to the UniProt ID
 * coverage: the percent coverage of the entire UniProt sequence resolution: the
 * resolution of the structure start: the structure residue number which maps to
 * the start of the mapped sequence unp_start: the sequence residue number which
 * maps to the structure start end: the structure residue number which maps to
 * the end of the mapped sequence unp_end: the sequence residue number which
 * maps to the structure end experimental_method: type of experiment used to
 * determine structure tax_id: taxonomic ID of the protein's original organism
 * 
 * @author David Hoksza
 * 
 */
@Entity
public class Structure implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Uniprot record to which this structure belongs to.
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "uniprot_id", nullable = false)
  private UniprotRecord uniprot;

  /**
   * the PDB ID which maps to the UniProt ID
   */
  private String pdbId = null;

  /**
   * the specific chain of the PDB which maps to the UniProt ID
   */
  private String chainId = null;

  /**
   * the percent coverage of the entire UniProt sequence
   */
  private Double coverage = null;

  /**
   * the resolution of the structure
   */
  private Double resolution = null;

  /**
   * the structure residue number which maps to the start of the mapped sequence
   */
  private Integer structStart = null;

  /**
   * the structure residue number which maps to the end of the mapped sequence
   */
  private Integer structEnd = null;

  /**
   * the sequence residue number which maps to the structure start
   */
  private Integer unpStart = null;

  /**
   * the sequence residue number which maps to the structure end
   */
  private Integer unpEnd = null;

  /**
   * type of experiment used to determine structure
   */
  private String experimentalMethod = null;

  /**
   * taxonomic ID of the protein's original organism
   */
  private Integer taxId = null;

  /**
   * Default constructor.
   */
  public Structure() {
  }

  /**
   * Constructor that initialize object with the data taken from the parameter.
   * 
   * @param s
   *          original object from which data is taken
   */
  public Structure(Structure s) {
    this.id = s.id;
    this.uniprot = s.uniprot;
    this.pdbId = s.pdbId;
    this.chainId = s.chainId;
    this.coverage = s.coverage;
    this.resolution = s.resolution;
    this.structStart = s.structStart;
    this.structEnd = s.structEnd;
    this.unpStart = s.unpStart;
    this.unpEnd = s.unpEnd;
    this.experimentalMethod = s.experimentalMethod;
    this.taxId = s.taxId;
  }

  /**
   * Creates copy of the object.
   * 
   * @return copy of the object.
   */
  public Structure copy() {
    if (this.getClass() == Structure.class) {
      return new Structure(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the idModificationResidue
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the uniprot
   */
  public UniprotRecord getUniprot() {
    return uniprot;
  }

  /**
   * @param uniprot
   *          the uniprot to set
   */
  public void setUniprot(UniprotRecord uniprot) {
    this.uniprot = uniprot;
  }

  /**
   * @return the pdbId
   */
  public String getPdbId() {
    return pdbId;
  }

  /**
   * @param pdbId
   *          the pdbId to set
   */
  public void setPdbId(String pdbId) {
    this.pdbId = pdbId;
  }

  /**
   * @return the chainId
   */
  public String getChainId() {
    return chainId;
  }

  /**
   * @param chainId
   *          the chainId to set
   */
  public void setChainId(String chainId) {
    this.chainId = chainId;
  }

  /**
   * @return the coverage
   */
  public Double getCoverage() {
    return coverage;
  }

  /**
   * @param coverage
   *          the coverage to set
   */
  public void setCoverage(Double coverage) {
    this.coverage = coverage;
  }

  /**
   * @return the resolution
   */
  public Double getResolution() {
    return resolution;
  }

  /**
   * @param resolution
   *          the resolution to set
   */
  public void setResolution(Double resolution) {
    this.resolution = resolution;
  }

  /**
   * @return the structStart
   */
  public Integer getStructStart() {
    return structStart;
  }

  /**
   * @param structStart
   *          the structStart to set
   */
  public void setStructStart(Integer structStart) {
    this.structStart = structStart;
  }

  /**
   * @return the structEnd
   */
  public Integer getStructEnd() {
    return structEnd;
  }

  /**
   * @param structEnd
   *          the structEnd to set
   */
  public void setStructEnd(Integer structEnd) {
    this.structEnd = structEnd;
  }

  /**
   * @return the unpStart
   */
  public Integer getUnpStart() {
    return unpStart;
  }

  /**
   * @param unpStart
   *          the unpStart to set
   */
  public void setUnpStart(Integer unpStart) {
    this.unpStart = unpStart;
  }

  /**
   * @return the unpEnd
   */
  public Integer getUnpEnd() {
    return unpEnd;
  }

  /**
   * @param unpEnd
   *          the unpEnd to set
   */
  public void setUnpEnd(Integer unpEnd) {
    this.unpEnd = unpEnd;
  }

  /**
   * @return the experimentalMethod
   */
  public String getExperimentalMethod() {
    return experimentalMethod;
  }

  /**
   * @param experimentalMethod
   *          the experimentalMethod to set
   */
  public void setExperimentalMethod(String experimentalMethod) {
    this.experimentalMethod = experimentalMethod;
  }

  /**
   * @return the taxId
   */
  public Integer getTaxId() {
    return taxId;
  }

  /**
   * @param taxId
   *          the taxId to set
   */
  public void setTaxId(Integer taxId) {
    this.taxId = taxId;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Structure [pdbId=" + pdbId + ", chainId=" + chainId + ", coverage=" + coverage + ", resolution="
        + resolution + ", structStart=" + structStart + ", structEnd=" + structEnd + ", unpStart=" + unpStart
        + ", unpEnd=" + unpEnd + ", experimentalMethod=" + experimentalMethod + ", taxId=" + taxId + "]";
  }

  public Map<String, Object> toMap() {
    Map<String, Object> result = new HashMap<>();

    result.put("pdbId", this.pdbId);
    result.put("chainId", this.chainId);
    result.put("coverage", this.coverage);
    result.put("resolution", this.resolution);
    result.put("structStart", this.structStart);
    result.put("structEnd", this.structEnd);
    result.put("unpStart", this.unpStart);
    result.put("unpEnd", this.unpEnd);
    result.put("experimentalMethod", this.experimentalMethod);
    result.put("taxId", this.taxId);

    return result;
  }

}
