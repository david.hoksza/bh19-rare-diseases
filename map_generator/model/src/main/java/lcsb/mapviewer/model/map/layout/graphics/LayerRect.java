package lcsb.mapviewer.model.map.layout.graphics;

import java.awt.*;
import java.io.Serializable;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.Drawable;

/**
 * This class describes rectangle in the layer.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class LayerRect implements Serializable, Drawable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(LayerRect.class);

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Color of the rectangle.
   */
  private Color color;

  /**
   * X coordinate of top left corner.
   */
  private Double x = 0.0;

  /**
   * Y coordinate of top left corner.
   */
  private Double y = 0.0;

  /**
   * Z index.
   */
  private Integer z;

  /**
   * Width of the rectangle.
   */
  private Double width = 0.0;

  /**
   * Height of the rectangle.
   */
  private Double height = 0.0;

  /**
   * Default constructor.
   */
  public LayerRect() {

  }

  /**
   * Constructor that copies data from the parameter.
   * 
   * @param layerRect
   *          from this parameter line data will be copied
   */
  public LayerRect(LayerRect layerRect) {
    color = layerRect.getColor();
    x = layerRect.getX();
    y = layerRect.getY();
    z = layerRect.getZ();
    width = layerRect.getWidth();
    height = layerRect.getHeight();
  }

  /**
   * Prepares a copy of the object.
   *
   * @return copy of LayerRect
   */
  public LayerRect copy() {
    if (this.getClass() == LayerRect.class) {
      return new LayerRect(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color
   *          the color to set
   * @see #color
   */
  public void setColor(Color color) {
    this.color = color;
  }

  /**
   * @return the x
   * @see #x
   */
  public Double getX() {
    return x;
  }

  /**
   * Set x from string containing double value.
   *
   * @param param
   *          x of the line in text format
   */
  public void setX(String param) {
    try {
      x = Double.parseDouble(param);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid x value: " + param, e);
    }
  }

  /**
   * @param x
   *          the x to set
   * @see #x
   */
  public void setX(Double x) {
    this.x = x;
  }

  /**
   * @return the y
   * @see #y
   */
  public Double getY() {
    return y;
  }

  /**
   * Set y from string containing double value.
   *
   * @param param
   *          y of the line in text format
   */
  public void setY(String param) {
    try {
      y = Double.parseDouble(param);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid y value: " + param, e);
    }
  }

  /**
   * @param y
   *          the y to set
   * @see #y
   */
  public void setY(Double y) {
    this.y = y;
  }

  /**
   * @return the width
   * @see #width
   */
  public Double getWidth() {
    return width;
  }

  /**
   * Set width from string containing double value.
   *
   * @param param
   *          width of the line in text format
   */
  public void setWidth(String param) {
    try {
      width = Double.parseDouble(param);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid width value: " + param, e);
    }
  }

  /**
   * @param width
   *          the width to set
   * @see #width
   */
  public void setWidth(Double width) {
    this.width = width;
  }

  /**
   * @return the height
   * @see #height
   */
  public Double getHeight() {
    return height;
  }

  /**
   * Set height from string containing double value.
   *
   * @param param
   *          height of the line in text format
   */
  public void setHeight(String param) {
    try {
      height = Double.parseDouble(param);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid height value: " + param, e);
    }
  }

  /**
   * @param height
   *          the height to set
   * @see #height
   */
  public void setHeight(Double height) {
    this.height = height;
  }

  @Override
  public Integer getZ() {
    return z;
  }

  @Override
  public void setZ(Integer z) {
    this.z = z;
  }

  @Override
  public String getElementId() {
    return "x=" + x + ";y=" + y + "; w=" + width + ", h=" + height;
  }

  @Override
  public double getSize() {
    return width * height;
  }

}
