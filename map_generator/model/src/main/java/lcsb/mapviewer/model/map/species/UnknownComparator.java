package lcsb.mapviewer.model.map.species;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;

/**
 * Comparator class used for comparing {@link Unknown} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class UnknownComparator extends Comparator<Unknown> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(UnknownComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public UnknownComparator(double epsilon) {
    super(Unknown.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public UnknownComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SpeciesComparator(epsilon);
  }

  @Override
  protected int internalCompare(Unknown arg0, Unknown arg1) {
    return 0;
  }
}
