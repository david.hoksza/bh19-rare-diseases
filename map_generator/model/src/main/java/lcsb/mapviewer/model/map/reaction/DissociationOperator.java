package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Class representing "dissociation operator" of one node into two or more nodes
 * in the reaction (in the product part of the reaction).
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("DISSOCIATION_OPERATOR_NODE")
public class DissociationOperator extends NodeOperator {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(DissociationOperator.class);

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param operator
   *          parent operator from which we copy data
   */
  public DissociationOperator(DissociationOperator operator) {
    super(operator);
  }

  /**
   * Default constructor.
   */
  public DissociationOperator() {
    super();
  }

  @Override
  public String getOperatorText() {
    return "○";
  }

  @Override
  public DissociationOperator copy() {
    if (this.getClass() == DissociationOperator.class) {
      return new DissociationOperator(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
