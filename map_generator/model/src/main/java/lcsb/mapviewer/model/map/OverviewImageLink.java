package lcsb.mapviewer.model.map;

import javax.persistence.*;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Link used in {@link OverviewImage parent OverviewImage} to link it to
 * {@link OverviewImage child OverviewImage}.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("IMAGE_LINK")
public class OverviewImageLink extends OverviewLink {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Child {@link OverviewImage} that can be reached via this link.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private OverviewImage linkedOverviewImage;

  /**
   * Default constructor that copies object from the parameter.
   * 
   * @param original
   *          original object to be copied
   */
  public OverviewImageLink(OverviewImageLink original) {
    super(original);
    this.linkedOverviewImage = original.linkedOverviewImage;

  }

  /**
   * DEfault constructor.
   */
  public OverviewImageLink() {
    super();
  }

  /**
   * @return the linkedOverviewImage
   * @see #linkedOverviewImage
   */
  public OverviewImage getLinkedOverviewImage() {
    return linkedOverviewImage;
  }

  /**
   * @param linkedOverviewImage
   *          the linkedOverviewImage to set
   * @see #linkedOverviewImage
   */
  public void setLinkedOverviewImage(OverviewImage linkedOverviewImage) {
    this.linkedOverviewImage = linkedOverviewImage;
  }

  @Override
  public OverviewImageLink copy() {
    if (this.getClass() == OverviewImageLink.class) {
      return new OverviewImageLink(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
