package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner reduced modulation reaction. It
 * must have at least one reactant and one product.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("REDUCED_MODULATION_REACTION")
public class ReducedModulationReaction extends Reaction implements SimpleReactionInterface, ReducedNotation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public ReducedModulationReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public ReducedModulationReaction(Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Reduced modulation";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public ReducedModulationReaction copy() {
    if (this.getClass() == ReducedModulationReaction.class) {
      return new ReducedModulationReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
