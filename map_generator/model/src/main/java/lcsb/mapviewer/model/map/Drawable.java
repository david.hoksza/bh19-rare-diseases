package lcsb.mapviewer.model.map;

import java.util.Comparator;

import lcsb.mapviewer.common.comparator.IntegerComparator;

public interface Drawable {

  Comparator<? super Drawable> Z_INDEX_COMPARATOR = new Comparator<Drawable>() {
    private IntegerComparator integerComparator = new IntegerComparator();

    @Override
    public int compare(Drawable o1, Drawable o2) {
      int id1 = o1.getZ();
      int id2 = o2.getZ();
      return integerComparator.compare(id1, id2);
    }
  };

  /**
   * Returns z-index of the graphical representation.
   * 
   * @return z-index of the graphical representation
   */
  Integer getZ();

  /**
   * Sets z-index of the graphical representation.
   * 
   * @param z
   *          z-index of the graphical representation
   */
  void setZ(Integer z);

  String getElementId();

  double getSize();
}
