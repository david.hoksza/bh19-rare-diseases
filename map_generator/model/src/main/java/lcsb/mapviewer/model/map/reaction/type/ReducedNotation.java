package lcsb.mapviewer.model.map.reaction.type;

/**
 * 
 * Interface used for marking {@link lcsb.mapviewer.model.map.reaction.Reaction
 * Reaction} extensions that implements reduced notation in CellDesigner. These
 * classes should have more than one reactant and product and cannot have any
 * modifiers.
 * 
 * @author Piotr Gawron
 * 
 */
public interface ReducedNotation {

}
