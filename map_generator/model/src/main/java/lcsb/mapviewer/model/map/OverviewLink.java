package lcsb.mapviewer.model.map;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Abstract class representing link that connects {@link OverviewImage parent
 * OverviewImage} with some other piece of data. Right now there are two
 * implementations:
 * <ul>
 * <li>{@link OverviewImageLink} - connects to another {@link OverviewImageLink}
 * </li>
 * <li>{@link OverviewModelLink} - connects to a
 * {@link lcsb.mapviewer.model.map.model.Model Model}</li>
 * </ul>
 * 
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "link_type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC_VIEW")
public abstract class OverviewLink implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Parent {@link OverviewImage} from which this link is outgoing.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private OverviewImage overviewImage;

  /**
   * String representing polygon area on the image that should be clickable and
   * that corresponds to this link. This string should be space separated list of
   * coordinates. Example "10,10 20,20 100,0".
   */
  private String polygon;

  /**
   * Default constructor.
   */
  public OverviewLink() {

  }

  /**
   * Default constructor that creates a copy of the object from parameter.
   * 
   * @param original
   *          original object to copy
   */
  public OverviewLink(OverviewLink original) {
    this.id = original.id;
    this.overviewImage = original.overviewImage;
    this.polygon = original.polygon;
  }

  /**
   * @return the polygon
   * @see #polygon
   */
  public String getPolygon() {
    return polygon;
  }

  /**
   * @param polygon
   *          the polygon to set
   * @see #polygon
   */
  public void setPolygon(String polygon) {
    this.polygon = polygon;
  }

  /**
   * Transforms {@link #polygon} into list of points.
   * 
   * @return list of points representing {@link #polygon} area.
   */
  public List<Point2D> getPolygonCoordinates() {
    List<Point2D> coordinates = new ArrayList<>();
    String[] stringCoordinates = polygon.split(" ");
    for (String string : stringCoordinates) {
      String[] coord = string.split(",");
      Double x = Double.valueOf(coord[0]);
      Double y = Double.valueOf(coord[1]);
      Point2D point = new Point2D.Double(x, y);
      coordinates.add(point);
    }
    return coordinates;
  }

  /**
   * @return the overviewImage
   * @see #overviewImage
   */
  public OverviewImage getOverviewImage() {
    return overviewImage;
  }

  /**
   * @param overviewImage
   *          the overviewImage to set
   * @see #overviewImage
   */
  public void setOverviewImage(OverviewImage overviewImage) {
    this.overviewImage = overviewImage;
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Copies the object and returns a copy.
   * 
   * @return copy of the object
   */
  public abstract OverviewLink copy();

}
