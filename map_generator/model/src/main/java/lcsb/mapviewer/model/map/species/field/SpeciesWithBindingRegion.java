package lcsb.mapviewer.model.map.species.field;

/**
 * Interface implemented by species that support {@link BindingRegion}
 * modification.
 * 
 * @author Piotr Gawron
 *
 */
public interface SpeciesWithBindingRegion extends SpeciesWithModificationResidue {

  /**
   * Adds a {@link BindingRegion} to the species.
   * 
   * @param bindingRegion
   *          {@link BindingRegion} to add
   */
  void addBindingRegion(BindingRegion bindingRegion);

}
