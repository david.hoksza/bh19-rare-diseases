package lcsb.mapviewer.model.map.species.field;

import java.text.DecimalFormat;

import javax.persistence.*;

@Entity
@DiscriminatorValue("ABSTRACT_SITE_MODIFICATION")
public abstract class AbstractSiteModification extends ModificationResidue {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * State in which this modification is.
   */
  @Enumerated(EnumType.STRING)
  protected ModificationState state = null;

  public AbstractSiteModification() {
    super();
  }

  public AbstractSiteModification(AbstractSiteModification mr) {
    super(mr);
    this.state = mr.getState();
  }

  public AbstractSiteModification(String modificationId) {
    super(modificationId);
  }

  public ModificationState getState() {
    return state;
  }

  public void setState(ModificationState state) {
    this.state = state;
  }

  @Override
  public String toString() {
    DecimalFormat format = new DecimalFormat("#.##");
    String positionString = "Point2D[null]";
    if (getPosition() != null) {
      positionString = "Point2D[" + format.format(getPosition().getX()) + "," + format.format(getPosition().getY())
          + "]";
    }
    String result = getName() + "," + getState() + "," + positionString;
    return result;
  }

}