package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner unknown reduced modulation
 * reaction. It must have at least one reactant and one product.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("U_REDUCED_MODULATION")
public class UnknownReducedModulationReaction extends Reaction implements SimpleReactionInterface, ReducedNotation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public UnknownReducedModulationReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public UnknownReducedModulationReaction(Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Unknown reduced modulation";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public UnknownReducedModulationReaction copy() {
    if (this.getClass() == UnknownReducedModulationReaction.class) {
      return new UnknownReducedModulationReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
