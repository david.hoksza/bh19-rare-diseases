package lcsb.mapviewer.model.map.layout.graphics;

import java.io.Serializable;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.BioEntity;

/**
 * This class describes glyph used to represent {@link BioEntity} on the map.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class Glyph implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(Glyph.class);

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * File that should be used for drawing a glyph.
   */
  @Cascade({ CascadeType.SAVE_UPDATE })
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "file_entry_id")
  private UploadedFileEntry file;

  /**
   * Project in which this glyph is available.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private Project project;

  /**
   * Default constructor.
   */
  public Glyph() {
  }

  /**
   * Constructor that creates copy of the element.
   * 
   * @param original
   *          element to be copied
   */
  public Glyph(Glyph original) {
    // we should reference to the same file
    setFile(original.getFile());
  }

  public UploadedFileEntry getFile() {
    return file;
  }

  public void setFile(UploadedFileEntry file) {
    this.file = file;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }
}
