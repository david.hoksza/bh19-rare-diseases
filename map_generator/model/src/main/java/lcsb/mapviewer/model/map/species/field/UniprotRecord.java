package lcsb.mapviewer.model.map.species.field;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class stores basically only uniprot Id which is used as mapping between
 * species and uniprot record.
 * 
 * @author David Hoksza
 * 
 */

@Entity
public class UniprotRecord implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * ID of the uniprot record
   */
  @Column(nullable = false)
  private String uniprotId = "";

  /**
   * Species to which this uniprot record belongs to.
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "element_id", nullable = false)
  private Species species;

  /**
   * List of uniprot records which are associated with this species.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "uniprot", orphanRemoval = true)
  private Set<Structure> structures = new HashSet<>();

  /**
   * Default constructor.
   */
  public UniprotRecord() {
  }

  /**
   * Constructor that initialize object with the data taken from the parameter.
   * 
   * @param ur
   *          original object from which data is taken
   */
  public UniprotRecord(UniprotRecord ur) {
    this.id = ur.id;
    this.uniprotId = ur.uniprotId;
    this.species = ur.species;
  }

  /**
   * Creates copy of the object.
   * 
   * @return copy of the object.
   */
  public UniprotRecord copy() {
    if (this.getClass() == UniprotRecord.class) {
      return new UniprotRecord(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "UniprotRecord [id=" + id + ", uniprotId=" + uniprotId + ", species=" + species + ", structures="
        + structures + "]";
  }

  /**
   * @return the idModificationResidue
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the uniprot id
   * @see #uniprotId
   */
  public String getUniprotId() {
    return uniprotId;
  }

  /**
   * @param uniprot
   *          id the id to set
   * @see #idModificationResidue
   */
  public void setUniprotId(String uniprotId) {
    this.uniprotId = uniprotId;
  }

  /**
   * @return the species
   * @see #species
   */
  public Species getSpecies() {
    return species;
  }

  /**
   * @param species
   *          species to which this uniprot record belongs
   * @see #species
   */
  public void setSpecies(Species species) {
    this.species = species;
  }

  /**
   * @return the structures
   * @see #structures
   */
  public Set<Structure> getStructures() {
    return structures;
  }

  /**
   * @param structures
   *          set of structures mapped to this uniprot record
   * @see #structures
   */
  public void setStructures(Set<Structure> structures) {
    this.structures = structures;
  }

  public void addStructures(Collection<Structure> structures) {
    this.structures.addAll(structures);
  }
}
