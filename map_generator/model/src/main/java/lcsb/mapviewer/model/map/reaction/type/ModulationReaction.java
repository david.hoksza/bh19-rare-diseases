package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner modulation reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("MODULATION_REACTION")
public class ModulationReaction extends Reaction implements SimpleReactionInterface, ModifierReactionNotation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public ModulationReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public ModulationReaction(Reaction result) {
    super(result);
  }

  public ModulationReaction(String reactionId) {
    super(reactionId);
  }

  @Override
  public String getStringType() {
    return "Modulation";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public ModulationReaction copy() {
    if (this.getClass() == ModulationReaction.class) {
      return new ModulationReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
