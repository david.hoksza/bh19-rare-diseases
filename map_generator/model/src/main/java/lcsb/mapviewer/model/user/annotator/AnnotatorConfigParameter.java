package lcsb.mapviewer.model.user.annotator;

import java.io.Serializable;

import javax.persistence.*;

import lcsb.mapviewer.model.user.AnnotatorParamDefinition;

/**
 * This class defines set of annotators parameters that are used by a user.
 * 
 * @author David Hoksza
 * 
 */
@Entity
@DiscriminatorValue("CONFIG_PARAMETER")
public class AnnotatorConfigParameter extends AnnotatorParameter implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Parameter name to be set.
   */
  @Enumerated(EnumType.STRING)
  private AnnotatorParamDefinition type;

  /**
   * Parameter value to be set.
   */
  private String value;

  /**
   * Default constructor for hibernate.
   */
  protected AnnotatorConfigParameter() {

  }

  /**
   * Default constructor.
   */
  public AnnotatorConfigParameter(AnnotatorParamDefinition parameterType, String paramValue) {
    setType(parameterType);
    setValue(paramValue);
  }

  /**
   * @return the parameter type
   * @see #type
   */
  public AnnotatorParamDefinition getType() {
    return type;
  }

  /**
   * @param paramName
   *          the {@link AnnotatorConfigParameter#type} to set
   */
  public void setType(AnnotatorParamDefinition paramName) {
    this.type = paramName;
  }

  /**
   * @return the parameter value
   * @see #value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param parameter
   *          value the {@link AnnotatorConfigParameter#value} to set
   */
  public void setValue(String paramValue) {
    this.value = paramValue;
  }
}
