package lcsb.mapviewer.model.map.species;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Entity representing unknown element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("UNKNOWN")
public class Unknown extends Species {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  Unknown() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public Unknown(Unknown original) {
    super(original);
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  public Unknown(String elementId) {
    setElementId(elementId);
  }

  @Override
  public Unknown copy() {
    if (this.getClass() == Unknown.class) {
      return new Unknown(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getStringType() {
    return "Unknown";
  }

}
