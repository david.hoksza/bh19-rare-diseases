package lcsb.mapviewer.model.map.model;

/**
 * Defines type of the connection between models (maps).
 * 
 * @author Piotr Gawron
 * 
 */
public enum SubmodelType {

  /**
   * Downstream targets submap.
   */
  DOWNSTREAM_TARGETS("Downstream targets"),

  /**
   * Object that should be extended into pathway.
   */
  PATHWAY("Pathway"),

  /**
   * Unknown type.
   */
  UNKNOWN("Unknown");

  /**
   * Common name of the type.
   */
  private String commonName;

  /**
   * Default constructor.
   * 
   * @param name
   *          {@link #commonName}
   */
  SubmodelType(String name) {
    this.commonName = name;
  }

  /**
   * @return the commonName
   * @see #commonName
   */
  public String getCommonName() {
    return commonName;
  }
}
