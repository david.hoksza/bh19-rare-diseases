package lcsb.mapviewer.model.map.statistics;

/**
 * Enum that defines types of {@link SearchHistory} entry.
 * 
 * @author Piotr Gawron
 * 
 */
public enum SearchType {
  /**
   * General search event.
   */
  GENERAL,

  /**
   * Drug search event.
   */
  DRUG,

  /**
   * Chemical search event.
   */
  CHEMICAL,

  /**
   * Mi RNA search event.
   */
  MI_RNA
}
