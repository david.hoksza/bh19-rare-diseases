package lcsb.mapviewer.model.map;

import java.io.Serializable;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class defines single search index for the element. Every element can
 * have few indexes. These indexes describes somehow elements. When the indexed
 * query match this index then element should be considered as a search result.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class SearchIndex implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(Element.class);

  /**
   * Database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Element to which index is assigned.
   */
  @ManyToOne
  private Element source;

  /**
   * Data of the index.
   */
  private String value;

  /**
   * Weight of the index. Lower values mean that index is less meaningfull.
   */
  private Integer weight;

  /**
   * Default constructor.
   */
  public SearchIndex() {
  }

  /**
   * Constructor that initialize index with weight and data.
   * 
   * @param value
   *          data of the index
   * @param weight
   *          weight of the index
   */
  public SearchIndex(String value, int weight) {
    this.value = value;
    this.weight = weight;
  }

  /**
   * Constructor that initialize index with default weight and data.
   * 
   * @param value
   *          data of the index
   */
  public SearchIndex(String value) {
    this.value = value;
    this.weight = 1;
  }

  /**
   * Constructor that copies information from parameter.
   * 
   * @param searchIndex
   *          source of data initialization
   */
  public SearchIndex(SearchIndex searchIndex) {
    source = searchIndex.getSource();
    value = searchIndex.getValue();
    weight = searchIndex.getWeight();
  }

  /**
   * Creates object copy.
   * 
   * @return copy of the object
   */
  public SearchIndex copy() {
    if (this.getClass() == SearchIndex.class) {
      return new SearchIndex(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the source
   * @see #source
   */
  public Element getSource() {
    return source;
  }

  /**
   * @param source
   *          the source to set
   * @see #source
   */
  public void setSource(Element source) {
    this.source = source;
  }

  /**
   * @return the value
   * @see #value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value
   *          the value to set
   * @see #value
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * @return the weight
   * @see #weight
   */
  public Integer getWeight() {
    return weight;
  }

  /**
   * @param weight
   *          the weight to set
   * @see #weight
   */
  public void setWeight(Integer weight) {
    this.weight = weight;
  }

}
