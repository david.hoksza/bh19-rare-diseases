package lcsb.mapviewer.model.map.species;

import javax.persistence.*;

/**
 * Entity representing chemical element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("CHEMICAL")
public abstract class Chemical extends Species {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * <a href=
   * "http://en.wikipedia.org/wiki/Simplified_molecular-input_line-entry_system"
   * >Smiles</a> parameter for the chemical.
   */
  @Column(columnDefinition = "TEXT")
  private String smiles;

  /**
   * <a href= "http://en.wikipedia.org/wiki/International_Chemical_Identifier" >
   * InChI</a> parameter for the chemical.
   */

  @Column(columnDefinition = "TEXT")
  private String inChI;

  /**
   * <a href=
   * "http://en.wikipedia.org/wiki/International_Chemical_Identifier#InChIKey" >
   * InChIKey</a> parameter for the chemical.
   */

  private String inChIKey;

  /**
   * Empty constructor required by hibernate.
   */
  Chemical() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public Chemical(Chemical original) {
    super(original);
    smiles = original.getSmiles();
    inChI = original.getInChI();
    inChIKey = original.getInChIKey();
  }

  /**
   * @return the inChIKey
   * @see #inChIKey
   */
  public String getInChIKey() {
    return inChIKey;
  }

  /**
   * @param inChIKey
   *          the inChIKey to set
   * @see #inChIKey
   */
  public void setInChIKey(String inChIKey) {
    this.inChIKey = inChIKey;
  }

  /**
   * @return the inChI
   * @see #inChI
   */
  public String getInChI() {
    return inChI;
  }

  /**
   * @param inChI
   *          the inChI to set
   * @see #inChI
   */
  public void setInChI(String inChI) {
    this.inChI = inChI;
  }

  /**
   * @return the smiles
   * @see #smiles
   */
  public String getSmiles() {
    return smiles;
  }

  /**
   * @param smiles
   *          the smiles to set
   * @see #smiles
   */
  public void setSmiles(String smiles) {
    this.smiles = smiles;
  }

}
