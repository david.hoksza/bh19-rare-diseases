package lcsb.mapviewer.model.graphics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;

/**
 * This class implements comparator interface for {@link PolylineData}. It
 * compares the content, but skip database identifier.
 * 
 * @author Piotr Gawron
 * 
 */
public class PolylineDataComparator extends Comparator<PolylineData> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(PolylineDataComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public PolylineDataComparator(double epsilon) {
    super(PolylineData.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public PolylineDataComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(PolylineData arg0, PolylineData arg1) {
    IntegerComparator integerComparator = new IntegerComparator();
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);
    ArrowTypeDataComparator atdComparator = new ArrowTypeDataComparator(epsilon);

    int different1 = 0;
    int different2 = 0;
    for (int i = 1; i < arg0.getPoints().size(); i++) {
      if (arg0.getPoints().get(i).distance(arg0.getPoints().get(i - 1)) > Configuration.EPSILON) {
        different1++;
      }
    }
    for (int i = 1; i < arg1.getPoints().size(); i++) {
      if (arg1.getPoints().get(i).distance(arg1.getPoints().get(i - 1)) > Configuration.EPSILON) {
        different2++;
      }
    }
    if (integerComparator.compare(different1, different2) != 0) {
      logger.debug("Lines have different size: " + different1 + ", " + different2);
      return integerComparator.compare(arg0.getPoints().size(), arg1.getPoints().size());
    }
    // TODO this should be fixed
    // for (int i = 0; i < arg0.getPoints().size(); i++) {
    // if
    // (doubleComparator.compare(arg0.getPoints().get(i).distanceSq(arg1.getPoints().get(i)),
    // 0.0) != 0) {
    // logger.debug("Lines have different points: " + arg0.getPoints().get(i) +
    // ", " + arg1.getPoints().get(i));
    // return
    // doubleComparator.compare(arg0.getPoints().get(i).distanceSq(arg1.getPoints().get(i)),
    // 0.0);
    // }
    // }

    if (atdComparator.compare(arg0.getBeginAtd(), arg1.getBeginAtd()) != 0) {
      logger.debug("Lines have different begin adt: " + arg0.getBeginAtd() + ", " + arg1.getBeginAtd());
      return atdComparator.compare(arg0.getBeginAtd(), arg1.getBeginAtd());
    }

    if (atdComparator.compare(arg0.getEndAtd(), arg1.getEndAtd()) != 0) {
      logger.debug("Lines have different end adt: " + arg0.getEndAtd() + ", " + arg1.getEndAtd());
      return atdComparator.compare(arg0.getEndAtd(), arg1.getEndAtd());
    }

    if (doubleComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      logger.debug("Lines have different width: " + arg0.getWidth() + ", " + arg1.getWidth());
      return doubleComparator.compare(arg0.getWidth(), arg1.getWidth());
    }

    if (integerComparator.compare(arg0.getColor().getRGB(), arg1.getColor().getRGB()) != 0) {
      logger.debug("Lines have different color: " + arg0.getColor() + ", " + arg1.getColor());
      return integerComparator.compare(arg0.getColor().getRGB(), arg1.getColor().getRGB());
    }

    if (arg0.getType().compareTo(arg1.getType()) != 0) {
      logger.debug("Lines have different type: " + arg0.getType() + ", " + arg1.getType());
      return arg0.getType().compareTo(arg1.getType());
    }

    return 0;
  }

}
