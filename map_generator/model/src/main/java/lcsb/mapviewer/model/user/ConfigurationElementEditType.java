package lcsb.mapviewer.model.user;

/**
 * Defines how the {@link ConfigurationElementType} should be edited (what kind
 * of values we are storing).
 * 
 * @author Piotr Gawron
 *
 */
public enum ConfigurationElementEditType {
  /**
   * Double value.
   */
  DOUBLE,

  /**
   * Integer value.
   */
  INTEGER,

  /**
   * String value.
   */
  STRING,

  /**
   * Multiple line text value.
   */
  TEXT,

  /**
   * Color value (for color picker).
   */
  COLOR,

  /**
   * Url value.
   */
  URL,

  /**
   * Email value.
   */
  EMAIL,

  /**
   * Password value.
   */
  PASSWORD,

  /**
   * True/false value.
   */
  BOOLEAN,
}
