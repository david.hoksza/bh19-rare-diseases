package lcsb.mapviewer.model.map.model;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.kinetics.*;
import lcsb.mapviewer.model.map.layout.BlockDiagram;
import lcsb.mapviewer.model.map.layout.ElementGroup;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Representation of the model data. It contains all information about single
 * map:
 * <ul>
 * <li>species and compartments ({@link #elements} field)</li>
 * <li>list of reactions ({@link #reactions})</li>
 * <li>layers with additional graphical objects ({@link #layers})</li>
 * <li>different graphical visualizations of the whole map ({@link #layouts})
 * </li>
 * <li>some other meta data (like: creation date, version, etc)</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class ModelData implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(ModelData.class);

  /**
   * Set of all elements in the map.
   * 
   * @see Element
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  private Set<Element> elements = new HashSet<>();

  /**
   * Set of all layers in the map.
   * 
   * @see Layer
   * 
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  private Set<Layer> layers = new HashSet<>();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Collection of all reactions in the map.
   * 
   * @see Reaction
   * 
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  private Set<Reaction> reactions = new HashSet<>();

  /**
   * Collection of SBML functions in the map.
   * 
   * @see SbmlFunction
   * 
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  private Set<SbmlFunction> functions = new HashSet<>();

  @Cascade({ CascadeType.ALL })
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "model_parameters_table", joinColumns = {
      @JoinColumn(name = "model_id", referencedColumnName = "id", nullable = false, updatable = false) }, inverseJoinColumns = {
          @JoinColumn(name = "parameter_id", referencedColumnName = "id", nullable = true, updatable = true) })
  private Set<SbmlParameter> parameters = new HashSet<>();

  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  private Set<SbmlUnit> units = new HashSet<>();

  /**
   * Width of the map.
   */
  private double width;

  /**
   * Height of the map.
   */
  private double height;

  /**
   * X coordinate that should be used when initially showing map.
   */
  private Double defaultCenterX;

  /**
   * Y coordinate that should be used when initially showing map.
   */
  private Double defaultCenterY;

  /**
   * Description of the map.
   */
  @Column(columnDefinition = "TEXT")
  private String notes = "";

  /**
   * Name of the map.
   */
  private String name;

  /**
   * Another CellDesigner identifier.
   */
  private String idModel;

  /**
   * How many hierarchical levels are in this map.
   */
  private int zoomLevels;

  /**
   * Zoom level that should be used when initially showing map.
   */
  private Integer defaultZoomLevel;

  /**
   * Size of the image tile that are used in this model.
   */
  private int tileSize;

  /**
   * Project to which this model belong to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private Project project;

  // This field should be transient in hibernate and during the transformation
  // to xml
  /**
   * {@link Model} object that is a container where data is being placed.
   */
  @Transient
  @XmlTransient
  private Model model;

  /**
   * List of submodels.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "parentModel", orphanRemoval = true)
  private Set<ModelSubmodelConnection> submodels = new HashSet<>();

  /**
   * List of connections with parent model (by definition one map can be a
   * submodel of few maps).
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "submodel")
  private Set<SubmodelConnection> parentModels = new HashSet<>();

  /**
   * Set of model annotations.
   */
  @Cascade({ CascadeType.ALL })
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "model_miriam_data_table", joinColumns = {
      @JoinColumn(name = "model_id", referencedColumnName = "id", nullable = false, updatable = false) }, inverseJoinColumns = {
          @JoinColumn(name = "miriam_data_id", referencedColumnName = "id", nullable = true, updatable = true) })
  private Set<MiriamData> miriamData = new HashSet<>();

  /**
   * List of authors who created map.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER)
  @OrderColumn(name = "idx")
  @JoinTable(joinColumns = {
      @JoinColumn(name = "model_data_id", referencedColumnName = "id", nullable = false, updatable = false) }, inverseJoinColumns = {
          @JoinColumn(name = "author_id", referencedColumnName = "id", nullable = true, updatable = true) })
  private List<Author> authors = new ArrayList<>();

  /**
   * When the map was created.
   */
  private Calendar creationDate = null;

  /**
   * When the map was modified.
   */
  @ElementCollection
  @CollectionTable(name = "model_data_modification_dates", joinColumns = @JoinColumn(name = "model_data_id"))
  @OrderColumn(name = "idx")
  @Column(name = "modification_date")
  private List<Calendar> modificationDates = new ArrayList<>();

  /**
   * Default constructor.
   */
  public ModelData() {
  }

  /**
   * Adds {@link Element} to model data.
   * 
   * @param element
   *          element to add
   */
  public void addElement(Element element) {
    element.setModelData(this);
    elements.add(element);
  }

  /**
   * Adds {@link Reaction} to model data.
   * 
   * @param reaction
   *          reaction to add
   */
  public void addReaction(Reaction reaction) {
    reaction.setModelData(this);
    reactions.add(reaction);
  }

  /**
   * Adds {@link Layer} to model data.
   * 
   * @param layer
   *          layer to add
   */
  public void addLayer(Layer layer) {
    layer.setModel(this);
    layers.add(layer);
  }

  /**
   * Adds collection of {@link Element elements} to model data.
   * 
   * @param elements
   *          elements to add
   */
  public void addElements(List<? extends Element> elements) {
    for (Element element : elements) {
      addElement(element);
    }
  }

  /**
   * 
   * @return {@link #id}
   */
  public Integer getId() {
    return id;
  }

  /**
   * 
   * @param id
   *          new {@link #id}
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   *
   * @return {@link #project}
   */
  public Project getProject() {
    return project;
  }

  /**
   *
   * @param project
   *          new {@link #project}
   */
  public void setProject(Project project) {
    this.project = project;
  }

  /**
   * Adds reactions to model.
   *
   * @param reactions2
   *          list of reaction to add
   */
  public void addReactions(List<Reaction> reactions2) {
    for (Reaction reaction : reactions2) {
      addReaction(reaction);
    }
  }

  /**
   * Adds collection of {@link Layer layers} to the model data.
   *
   * @param layers
   *          objets to add
   */
  public void addLayers(Collection<Layer> layers) {
    for (Layer layer : layers) {
      addLayer(layer);
    }
  }

  /**
   * Adds {@link ElementGroup} to the model data.
   *
   * @param elementGroup
   *          object to add
   */
  public void addElementGroup(ElementGroup elementGroup) {
    // for now we ignore this information
  }

  /**
   * Adds {@link BlockDiagram} to the model data.
   *
   * @param blockDiagram
   *          object to add
   */
  public void addBlockDiagream(BlockDiagram blockDiagram) {
    // for now we ignore this information
  }

  /**
   * Removes {@link Element} from the model.
   *
   * @param element
   *          element to remove
   */
  public void removeElement(Element element) {
    if (element == null) {
      throw new InvalidArgumentException("Cannot remove null");
    }
    if (!elements.contains(element)) {
      logger.warn("Element doesn't exist in the map: " + element.getElementId());
      return;
    }

    element.setModelData(null);
    elements.remove(element);
  }

  /**
   * Removes reaction from model.
   *
   * @param reaction
   *          reaction to remove
   */
  public void removeReaction(Reaction reaction) {
    if (!reactions.contains(reaction)) {
      logger.warn("Reaction doesn't exist in the model: " + reaction.getIdReaction());
      return;
    }
    reaction.setModelData(null);
    reactions.remove(reaction);
  }

  /**
   * @return the zoomLevels
   * @see #zoomLevels
   */
  public int getZoomLevels() {
    return zoomLevels;
  }

  /**
   * @param zoomLevels
   *          the zoomLevels to set
   * @see #zoomLevels
   */
  public void setZoomLevels(int zoomLevels) {
    this.zoomLevels = zoomLevels;
  }

  /**
   * @return the tileSize
   * @see #tileSize
   */
  public int getTileSize() {
    return tileSize;
  }

  /**
   * @param tileSize
   *          the tileSize to set
   * @see #tileSize
   */
  public void setTileSize(int tileSize) {
    this.tileSize = tileSize;
  }

  /**
   * @return the idModel
   * @see #idModel
   */
  public String getIdModel() {
    return idModel;
  }

  /**
   * @param idModel
   *          the idModel to set
   * @see #idModel
   */
  public void setIdModel(String idModel) {
    this.idModel = idModel;
  }

  /**
   * @return the model
   * @see #model
   */
  @XmlTransient
  public Model getModel() {
    if (model == null) {
      logger.warn("Model not set in model data.");
    }
    return model;
  }

  /**
   * @param model
   *          the model to set
   * @see #model
   */
  public void setModel(Model model) {
    this.model = model;
  }

  /**
   * @return the elements
   * @see #elements
   */
  public Set<Element> getElements() {
    return elements;
  }

  /**
   *
   * @param elements
   *          new {@link #elements} collection
   */
  public void setElements(Set<Element> elements) {
    this.elements = elements;
  }

  /**
   * @return the layers
   * @see #layers
   */
  public Set<Layer> getLayers() {
    return layers;
  }

  /**
   * @param layers
   *          the layers to set
   * @see #layers
   */
  public void setLayers(Set<Layer> layers) {
    this.layers = layers;
  }

  /**
   * @return the reactions
   * @see #reactions
   */
  public Set<Reaction> getReactions() {
    return reactions;
  }

  /**
   * @param reactions
   *          the reactions to set
   * @see #reactions
   */
  public void setReactions(Set<Reaction> reactions) {
    this.reactions = reactions;
  }

  /**
   * @return the width
   * @see #width
   */
  public double getWidth() {
    return width;
  }

  /**
   *
   * @param width
   *          new {@link #width}
   */
  public void setWidth(int width) {
    setWidth(Double.valueOf(width));
  }

  /**
   * @param width
   *          the width to set
   * @see #width
   */
  public void setWidth(double width) {
    this.width = width;
  }

  /**
   * @return the height
   * @see #height
   */
  public double getHeight() {
    return height;
  }

  /**
   *
   * @param height
   *          new {@link #height}
   */
  public void setHeight(int height) {
    setHeight(Double.valueOf(height));
  }

  /**
   * @param height
   *          the height to set
   * @see #height
   */
  public void setHeight(double height) {
    this.height = height;
  }

  /**
   * @return the notes
   * @see #notes
   */
  public String getNotes() {
    return notes;
  }

  /**
   * @param notes
   *          the notes to set
   * @see #notes
   */
  public void setNotes(String notes) {
    this.notes = notes;
  }

  /**
   * Adds submodel.
   * 
   * @param submodel
   *          object to add
   */
  public void addSubmodel(ModelSubmodelConnection submodel) {
    this.submodels.add(submodel);
    submodel.setParentModel(this);
  }

  /**
   * Returns collection of submodels.
   * 
   * @return collection of submodels.
   */
  public Collection<ModelSubmodelConnection> getSubmodels() {
    return this.submodels;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the parentModels
   * @see #parentModels
   */
  public Set<SubmodelConnection> getParentModels() {
    return parentModels;
  }

  /**
   * @param parentModels
   *          the parentModels to set
   * @see #parentModels
   */
  public void setParentModels(Set<SubmodelConnection> parentModels) {
    this.parentModels = parentModels;
  }

  public Double getDefaultCenterX() {
    return defaultCenterX;
  }

  public void setDefaultCenterX(Double defaultCenterX) {
    this.defaultCenterX = defaultCenterX;
  }

  public Double getDefaultCenterY() {
    return defaultCenterY;
  }

  public void setDefaultCenterY(Double defaultCenterY) {
    this.defaultCenterY = defaultCenterY;
  }

  public Integer getDefaultZoomLevel() {
    return defaultZoomLevel;
  }

  public void setDefaultZoomLevel(Integer defaultZoomLevel) {
    this.defaultZoomLevel = defaultZoomLevel;
  }

  public Set<SbmlFunction> getFunctions() {
    return functions;
  }

  public Set<SbmlParameter> getParameters() {
    return parameters;
  }

  public void addUnits(Collection<SbmlUnit> units) {
    for (SbmlUnit sbmlUnit : units) {
      addUnit(sbmlUnit);
    }
  }

  public Set<SbmlUnit> getUnits() {
    return units;
  }

  public void addUnit(SbmlUnit unit) {
    this.units.add(unit);
    unit.setModel(this);
  }

  public void addParameters(Collection<SbmlParameter> sbmlParameters) {
    this.parameters.addAll(sbmlParameters);
  }

  public void addParameter(SbmlParameter sbmlParameter) {
    this.parameters.add(sbmlParameter);
  }

  public void addFunction(SbmlFunction sbmlFunction) {
    this.functions.add(sbmlFunction);
    sbmlFunction.setModel(this);
  }

  public void addFunctions(Collection<SbmlFunction> functions) {
    for (SbmlFunction sbmlFunction : functions) {
      addFunction(sbmlFunction);
    }
  }

  /**
   * Returns set of annotations for the model.
   * 
   * @return set of annotations
   */
  public Set<MiriamData> getMiriamData() {
    return miriamData;
  }

  /**
   * Adds annotation to the model.
   * 
   * @param md
   *          annotation to add
   */
  public void addMiriamData(MiriamData md) {
    miriamData.add(md);
  }

  public void addAuthor(Author author) {
    authors.add(author);
  }

  public List<Author> getAuthors() {
    return authors;
  }

  public Calendar getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Calendar creationDate) {
    this.creationDate = creationDate;
  }

  public void addModificationDate(Calendar modificationDate) {
    modificationDates.add(modificationDate);
  }

  public List<Calendar> getModificationDates() {
    return modificationDates;
  }
}