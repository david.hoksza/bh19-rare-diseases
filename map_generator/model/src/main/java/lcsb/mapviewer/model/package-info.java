/**
 * Contains structures used for modeling. They are designed to be stored in the
 * database. Package is divided into several functional subpackages:
 * <ul>
 * <li>graphics - where graphical structures are defined (not connected to the
 * model)</li>
 * <li>log - where log entries of the systems are defined</li>
 * <li>map - where map model is defined (
 * {@link lcsb.mapviewer.model.map.model.Model Model} is the main structure in
 * this package)</li>
 * <li>cache (former reactome) - where cache structure is defined</li>
 * <li>user - structure for user definition is kept there</li>
 * </ul>
 * <br/>
 * The main structure is {@link lcsb.mapviewer.model.Project}. It defines single
 * project that exists in the system. Signle project can contain few map
 * {@link lcsb.mapviewer.model.map.model.Model models} (few different versions
 * of the map).
 */
package lcsb.mapviewer.model;
