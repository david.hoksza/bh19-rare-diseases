package lcsb.mapviewer.model.cache;

import java.io.Serializable;

import javax.persistence.*;

import lcsb.mapviewer.model.user.User;

/**
 * Database object representing file uploaded into system.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("UPLOADED_FILE_ENTRY")
public class UploadedFileEntry extends FileEntry implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Length of the file.
   */
  private long length;

  @ManyToOne(fetch = FetchType.LAZY)
  private User owner;

  /**
   * Default constructor.
   */
  public UploadedFileEntry() {

  }

  /**
   * Constructor that copies data from the parameter.
   * 
   * @param original
   *          original object from which data will be copied
   */
  public UploadedFileEntry(UploadedFileEntry original) {
    super(original);
  }

  public long getLength() {
    return length;
  }

  public void setLength(long length) {
    this.length = length;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

}
