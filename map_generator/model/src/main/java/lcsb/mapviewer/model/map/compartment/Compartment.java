package lcsb.mapviewer.model.map.compartment;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.persistence.Entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.*;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class defines compartment in the model.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("COMPARTMENT")
public class Compartment extends Element {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default color of the compartment.
   */
  private static final Color DEFAULT_COLOR = Color.BLACK;

  /**
   * Default width of the inner compartment border.
   */
  private static final int DEFAULT_INNER_BORDER_WIDTH = 1;

  /**
   * Default width of the outer compartment border.
   */
  private static final int DEFAULT_OUTER_BORDER_WIDTH = 2;

  /**
   * Default thickness of compartment border.
   */
  public static final int DEFAULT_COMPARTMENT_THICKNESS = 12;

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(Compartment.class);

  /**
   * Thickness of the compartment border.
   */
  private double thickness;

  /**
   * How thick should be the outer line of the compartment border.
   */
  private double outerWidth;
  /**
   * How thick should be the inner line of the compartment border.
   */
  private double innerWidth;

  /**
   * Describes where the description of the compartment lies.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.Point2DMapper")
  private Point2D namePoint = new Point2D.Double();

  /**
   * {@link Element Elements} that are directly inside this compartment.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "compartment")
  private Set<Element> elements = new HashSet<>();

  /**
   * Constructor that creates a compartment which is initialized by the data from
   * the parameter object.
   * 
   * @param original
   *          object from which this object is initialized
   */
  public Compartment(Compartment original) {
    super(original);
    namePoint.setLocation(original.getNamePoint());
    thickness = original.thickness;
    outerWidth = original.outerWidth;
    innerWidth = original.innerWidth;
    for (Element element : original.elements) {
      addElement(element.copy());
    }
  }

  /**
   * Default constructor.
   */
  Compartment() {
    super();
    namePoint = new Point2D.Double(0, 0);
    thickness = DEFAULT_COMPARTMENT_THICKNESS;
    outerWidth = DEFAULT_OUTER_BORDER_WIDTH;
    innerWidth = DEFAULT_INNER_BORDER_WIDTH;
    setFillColor(DEFAULT_COLOR);
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          identifier of the compartment
   */
  public Compartment(String elementId) {
    setElementId(elementId);
  }

  /**
   * 
   * @param text
   *          line thickness in String format
   */
  public void setLineThickness(String text) {
    try {
      thickness = Double.parseDouble(text);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid thickness format: " + text, e);
    }
  }

  /**
   * 
   * @param text
   *          outer line width in String format
   */
  public void setLineOuterWidth(String text) {
    try {
      outerWidth = Double.parseDouble(text);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid outerWidth format: " + text, e);
    }
  }

  /**
   * 
   * @param text
   *          inner line width in String format
   */
  public void setLineInnerWidth(String text) {
    try {
      innerWidth = Double.parseDouble(text);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid innerWidth format: " + text, e);
    }
  }

  /**
   * Add element into the compartment. In case element already exists in the
   * compartment {@link InvalidArgumentException} is thrown.
   * 
   * @param elementToAdd
   *          element that should be added
   */
  public void addElement(Element elementToAdd) {
    for (Element element : elements) {
      if (element.getElementId().equals(elementToAdd.getElementId())) {
        if (element.equals(elementToAdd)) {
          return;
        } else {
          throw new InvalidArgumentException("Element " + elementToAdd.getElementId()
              + " already on the list of compartment " + getElementId() + " children");
        }
      }
    }
    elements.add(elementToAdd);
    elementToAdd.setCompartment(this);
  }

  /**
   * Return set of all {@link Element elements} inside the compartment (not
   * necessary directly in the compartment).
   * 
   * @return list of all elements in the compartment
   */
  public Set<Element> getAllSubElements() {
    Set<Element> result = new HashSet<>();
    result.addAll(getElements());
    for (Element subelement : getElements()) {
      if (subelement instanceof Compartment) {
        result.addAll(((Compartment) subelement).getAllSubElements());
      }
    }

    return result;
  }

  /**
   * Check if element given in the parameter has a common part with this
   * compartment. In case element2 lies inside complex the check with complex is
   * made.
   * 
   * @param element2
   *          element to be checked
   * @return true if the element2 cross this compartment, <code>false</code>
   *         otherwise
   */
  public boolean cross(Element element2) {
    Set<Element> parents = new HashSet<>();
    // in case of elements in complexes we have to check if the top level
    // complex cross element
    if (element2 instanceof Species) {
      while (((Species) element2).getComplex() != null) {
        element2 = ((Species) element2).getComplex();
        if (parents.contains(element2)) {
          throw new InvalidStateException("Cycled nesting in complex parents: " + element2.getName());
        } else {
          parents.add(element2);
        }
      }
    }

    Point2D argumentTopLeftCorner = new Point2D.Double(element2.getX(), element2.getY());
    Point2D argumentBottomRightCorner = new Point2D.Double(element2.getX() + element2.getWidth(),
        element2.getY() + element2.getHeight());

    Point2D topLeftCorner = new Point2D.Double(getX(), getY());
    Point2D bottomRightCorner = new Point2D.Double(getX() + getWidth(), getY() + getHeight());

    return (argumentTopLeftCorner.getX() < bottomRightCorner.getX()
        && argumentTopLeftCorner.getY() < bottomRightCorner.getY()
        && argumentBottomRightCorner.getX() > topLeftCorner.getX()
        && argumentBottomRightCorner.getY() > topLeftCorner.getY());

  }

  /**
   * Checks if there is a species with the same name inside the compartment.
   * 
   * @return <code>true</code> if there is {@link Species} with the same name
   *         inside compartment, <code>false</code> otherwise
   */
  public boolean containsIdenticalSpecies() {
    for (Element innerElement : elements) {
      if (innerElement instanceof Species) {
        String name = innerElement.getName();
        if (name.equalsIgnoreCase(getName())) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Creates a copy of this {@link Compartment}.
   * 
   * @return copy of the object
   */
  public Compartment copy() {
    if (this.getClass() == Compartment.class) {
      return new Compartment(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * Removes element from the {@link Compartment}.
   * 
   * @param element
   *          object to be removed
   */
  public void removeElement(Element element) {
    elements.remove(element);
    if (element.getCompartment() != null) {
      if (element.getCompartment() == this) {
        element.setCompartment(null);
      } else {
        logger.warn("Removing element from compartment that might not belong there");
      }
    }
  }

  /**
   * Set coordinate of the description point.
   * 
   * @param x
   *          x coordinate to be set
   * @param y
   *          y coordinate to be set
   */
  public void setNamePoint(double x, double y) {
    setNamePoint(new Point2D.Double(x, y));
  }

  /**
   * @return the thickness
   * @see #thickness
   */
  public double getThickness() {
    return thickness;
  }

  /**
   * @param thickness
   *          the thickness to set
   * @see #thickness
   */
  public void setThickness(double thickness) {
    this.thickness = thickness;
  }

  /**
   * @return the outerWidth
   * @see #outerWidth
   */
  public double getOuterWidth() {
    return outerWidth;
  }

  /**
   * @param outerWidth
   *          the outerWidth to set
   * @see #outerWidth
   */
  public void setOuterWidth(double outerWidth) {
    this.outerWidth = outerWidth;
  }

  /**
   * @return the innerWidth
   * @see #innerWidth
   */
  public double getInnerWidth() {
    return innerWidth;
  }

  /**
   * @param innerWidth
   *          the innerWidth to set
   * @see #innerWidth
   */
  public void setInnerWidth(double innerWidth) {
    this.innerWidth = innerWidth;
  }

  /**
   * @return the namePoint
   * @see #namePoint
   */
  public Point2D getNamePoint() {
    return namePoint;
  }

  /**
   * @param namePoint
   *          the namePoint to set
   * @see #namePoint
   */
  public void setNamePoint(Point2D namePoint) {
    this.namePoint = namePoint;
  }

  /**
   * @return the elements
   * @see #elements
   */
  public Set<Element> getElements() {
    return elements;
  }

  /**
   * @param elements
   *          the elements to set
   */
  public void setElements(Set<Element> elements) {
    this.elements = elements;
  }

  @Override
  public String getStringType() {
    return "Compartment";
  }

  public void removeElements(Set<Element> elements) {
    for (Element element : elements) {
      removeElement(element);
    }
  }

}
