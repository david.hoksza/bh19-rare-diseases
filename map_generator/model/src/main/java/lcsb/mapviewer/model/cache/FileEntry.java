package lcsb.mapviewer.model.cache;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Database object representing file put in the system (it can be some cached
 * file, or uploaded file). There are two ways of storing file. First is by
 * storing the content in database directly (in the {@link #fileContent} field).
 * Second way should be used for all big files - it stores content in the local
 * file system (relative path is stored in {@link #localPath} field).
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "file_type_db", discriminatorType = DiscriminatorType.STRING)
public abstract class FileEntry implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique local database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Local (relative) path where file is stored.
   */
  @Column(columnDefinition = "TEXT")
  private String localPath;

  /**
   * Original name of the file.
   */
  private String originalFileName;

  /**
   * Is the file removed from database.
   */
  private boolean removed = false;

  /**
   * Content of the file. If set to null it means it stored in file system.
   * 
   * @see #localPath
   */
  private byte[] fileContent;

  /**
   * Constructor that copies data from the parameter.
   * 
   * @param inputData
   *          original object from which data will be copied
   */
  public FileEntry(FileEntry inputData) {
    setLocalPath(inputData.getLocalPath());
    setOriginalFileName(inputData.getOriginalFileName());
    setRemoved(inputData.isRemoved());
    if (inputData.getFileContent() != null) {
      setFileContent(inputData.getFileContent().clone());
    }
  }

  /**
   * Default constructor.
   */
  public FileEntry() {
  }

  /**
   * @return the localPath
   * @see #localPath
   */
  public String getLocalPath() {
    return localPath;
  }

  /**
   * @param localPath
   *          the localPath to set
   * @see #localPath
   */
  public void setLocalPath(String localPath) {
    this.localPath = localPath;
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the removed
   * @see #removed
   */
  public boolean isRemoved() {
    return removed;
  }

  /**
   * @param removed
   *          the removed to set
   * @see #removed
   */
  public void setRemoved(boolean removed) {
    this.removed = removed;
  }

  /**
   * @return the fileContent
   * @see #fileContent
   */
  public byte[] getFileContent() {
    return fileContent;
  }

  /**
   * @param fileContent
   *          the fileContent to set
   * @see #fileContent
   */
  public void setFileContent(byte[] fileContent) {
    this.fileContent = fileContent;
  }

  /**
   * @return the originalFileName
   * @see #originalFileName
   */
  public String getOriginalFileName() {
    return originalFileName;
  }

  /**
   * @param originalFileName
   *          the originalFileName to set
   * @see #originalFileName
   */
  public void setOriginalFileName(String originalFileName) {
    this.originalFileName = originalFileName;
  }
}
