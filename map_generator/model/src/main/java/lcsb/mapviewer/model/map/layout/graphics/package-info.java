/**
 * Provides objects for additional layers with some graphical elements (like
 * lines, rectangles, etc.).
 */
package lcsb.mapviewer.model.map.layout.graphics;
