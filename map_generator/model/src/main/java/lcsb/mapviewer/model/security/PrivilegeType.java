package lcsb.mapviewer.model.security;

import lcsb.mapviewer.model.Project;

public enum PrivilegeType {

  READ_PROJECT(Project.class, "View project"),
  WRITE_PROJECT(Project.class, "Modify project"),
  IS_ADMIN(null, "Admin"),
  IS_CURATOR(null, "Curator"),
  CAN_CREATE_OVERLAYS(null, "Can create data overlays");

  private Class<?> privilegeObjectType;

  private String description;

  PrivilegeType(Class<?> objectClazz, String description) {
    this.privilegeObjectType = objectClazz;
    this.description = description;
  }

  public Class<?> getPrivilegeObjectType() {
    return privilegeObjectType;
  }

  public String getDescription() {
    return description;
  }

}