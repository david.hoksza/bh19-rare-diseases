package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner catalysis reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("CATALYSIS_REACTION")
public class CatalysisReaction extends Reaction implements SimpleReactionInterface, ModifierReactionNotation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public CatalysisReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public CatalysisReaction(Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Catalysis";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public CatalysisReaction copy() {
    if (this.getClass() == CatalysisReaction.class) {
      return new CatalysisReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
