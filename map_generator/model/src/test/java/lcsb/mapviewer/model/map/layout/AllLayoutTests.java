package lcsb.mapviewer.model.map.layout;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.model.map.layout.graphics.AllGraphicsTests;

@RunWith(Suite.class)
@SuiteClasses({ AllGraphicsTests.class,
    ColorSchemaTest.class,
    ElementGroupTest.class,
    GenericColorSchemaTest.class,
    GeneVariationTest.class,
    GeneVariationColorSchemaTest.class,
    InvalidColorSchemaExceptionTest.class,
    LayoutStatusTest.class,
    LayoutTest.class,
    ReferenceGenomeGeneMappingTest.class,
    ReferenceGenomeTest.class,
    ReferenceGenomeTypeTest.class,
})
public class AllLayoutTests {

}
