package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamType;

public class UserAnnotationSchemaTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddClassAnnotator() throws Exception {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    UserClassAnnotators ca = new UserClassAnnotators(String.class, new ArrayList<>());
    UserClassAnnotators ca2 = new UserClassAnnotators(String.class, new ArrayList<>());
    annotationSchema.addClassAnnotator(ca);
    annotationSchema.addClassAnnotator(ca2);
    assertEquals(1, annotationSchema.getClassAnnotators().size());
    assertEquals(ca2.getAnnotators(), annotationSchema.getAnnotatorsForClass(String.class));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidAddClassAnnotator() throws Exception {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    UserClassAnnotators ca = new UserClassAnnotators(String.class, new ArrayList<>());
    ca.setClassName((String) null);
    annotationSchema.addClassAnnotator(ca);
  }

  @Test
  public void testAddClassRequiredAnnotations() throws Exception {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    UserClassRequiredAnnotations cra = new UserClassRequiredAnnotations(String.class, new ArrayList<MiriamType>());
    UserClassRequiredAnnotations cra2 = new UserClassRequiredAnnotations(String.class, new ArrayList<MiriamType>());
    annotationSchema.addClassRequiredAnnotations(cra);
    annotationSchema.addClassRequiredAnnotations(cra2);
    assertEquals(1, annotationSchema.getClassRequiredAnnotators().size());
    assertEquals(cra2, annotationSchema.getClassRequiredAnnotators().get(0));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddInvalidClassRequiredAnnotations() throws Exception {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    UserClassRequiredAnnotations cra = new UserClassRequiredAnnotations(String.class, new ArrayList<MiriamType>());
    cra.setClassName((String) null);

    annotationSchema.addClassRequiredAnnotations(cra);
  }

  @Test
  public void testAddClassValidAnnotations() throws Exception {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    UserClassValidAnnotations cva = new UserClassValidAnnotations(String.class, new ArrayList<MiriamType>());
    UserClassValidAnnotations cva2 = new UserClassValidAnnotations(String.class, new ArrayList<MiriamType>());
    annotationSchema.addClassValidAnnotations(cva);
    annotationSchema.addClassValidAnnotations(cva2);
    assertEquals(1, annotationSchema.getClassValidAnnotators().size());
    assertEquals(cva2, annotationSchema.getClassValidAnnotators().get(0));
  }

  @Test
  public void testGetValidAnnotations() throws Exception {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    List<MiriamType> list = new ArrayList<>();
    list.add(MiriamType.CAS);
    UserClassValidAnnotations cva = new UserClassValidAnnotations(String.class, list);
    annotationSchema.addClassValidAnnotations(cva);
    assertEquals(1, annotationSchema.getValidAnnotations(String.class).size());
    assertEquals(0, annotationSchema.getValidAnnotations(Integer.class).size());
  }

  @Test
  public void testGetRequiredAnnotations() throws Exception {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    List<MiriamType> list = new ArrayList<>();
    list.add(MiriamType.CAS);
    UserClassRequiredAnnotations cva = new UserClassRequiredAnnotations(String.class, list);
    annotationSchema.addClassRequiredAnnotations(cva);
    assertEquals(1, annotationSchema.getRequiredAnnotations(String.class).size());
    assertEquals(0, annotationSchema.getRequiredAnnotations(Integer.class).size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddInvalidClassValidAnnotations() throws Exception {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    UserClassValidAnnotations cva = new UserClassValidAnnotations(String.class, new ArrayList<MiriamType>());
    cva.setClassName((String) null);

    annotationSchema.addClassValidAnnotations(cva);
  }

  @Test
  public void testAddGuiPreference() throws Exception {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();

    annotationSchema.addGuiPreference(new UserGuiPreference());
    assertEquals(1, annotationSchema.getGuiPreferences().size());
  }

  @Test
  public void testAddGuiPreferenceWithDuplicateKey() throws Exception {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    UserGuiPreference preference1 = new UserGuiPreference();
    preference1.setKey("test");
    preference1.setValue("value");
    annotationSchema.addGuiPreference(preference1);
    UserGuiPreference preference2 = new UserGuiPreference();
    preference2.setKey("test");
    preference2.setValue("new value");
    annotationSchema.addGuiPreference(preference2);
    assertEquals(1, annotationSchema.getGuiPreferences().size());
    assertEquals("new value", annotationSchema.getGuiPreferences().iterator().next().getValue());
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new UserAnnotationSchema());
  }

  @Test
  public void testGetters() {
    UserAnnotationSchema uas = new UserAnnotationSchema();
    Boolean validateMiriamTypes = true;
    List<UserClassAnnotators> classAnnotators = new ArrayList<>();
    List<UserClassValidAnnotations> classValidAnnotators = new ArrayList<>();
    List<UserClassRequiredAnnotations> classRequiredAnnotators = new ArrayList<>();
    User user = new User();

    Boolean sbgnFormat = true;
    Boolean networkLayoutAsDefault = true;

    uas.setValidateMiriamTypes(validateMiriamTypes);
    uas.setClassAnnotators(classAnnotators);
    uas.setClassValidAnnotators(classValidAnnotators);
    uas.setClassRequiredAnnotators(classRequiredAnnotators);
    uas.setSbgnFormat(sbgnFormat);
    uas.setNetworkLayoutAsDefault(networkLayoutAsDefault);
    uas.setUser(user);

    assertEquals(validateMiriamTypes, uas.getValidateMiriamTypes());
    assertEquals(classAnnotators, uas.getClassAnnotators());
    assertEquals(classValidAnnotators, uas.getClassValidAnnotators());
    assertEquals(classRequiredAnnotators, uas.getClassRequiredAnnotators());
    assertEquals(sbgnFormat, uas.getSbgnFormat());
    assertEquals(networkLayoutAsDefault, uas.getNetworkLayoutAsDefault());
    assertEquals(user, uas.getUser());
  }

}
