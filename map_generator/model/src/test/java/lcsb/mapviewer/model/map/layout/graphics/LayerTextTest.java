package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.*;

import java.awt.geom.Rectangle2D;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class LayerTextTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(createText());
  }

  @Test
  public void testConstructor1() {
    LayerText layerText = createText();

    LayerText copy = new LayerText(layerText);

    assertNotNull(copy);
    assertEquals(0, new LayerTextComparator().compare(layerText, copy));
  }

  private LayerText createText() {
    LayerText result = new LayerText();
    result.setZ(16);
    return result;
  }

  @Test
  public void testConstructor2() {
    LayerText copy = new LayerText(new Rectangle2D.Double(), "text");

    assertNotNull(copy);
  }

  @Test
  public void testCopy() {
    LayerText copy = createText().copy();

    assertNotNull(copy);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(LayerText.class).copy();
  }

  @Test
  public void testGetBorder() {
    LayerText copy = createText().copy();
    assertNotNull(copy.getBorder());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidX() {
    LayerText text = createText();
    text.setX("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidY() {
    LayerText text = createText();
    text.setY("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidWidth() {
    LayerText text = createText();
    text.setWidth("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidHeight() {
    LayerText text = createText();
    text.setHeight("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidFontSize() {
    LayerText text = createText();
    text.setFontSize("a1.6");
  }

  @Test
  public void testGetters() {
    LayerText text = createText();

    String yParam = "1.2";
    Double y = 1.2;

    String xParam = "2.2";
    Double x = 2.2;

    String widthParam = "10.2";
    Double width = 10.2;

    String heightParam = "72.2";
    Double height = 72.2;

    String fontSizeParam = "5.0";
    Double fontSize = 5.0;

    text.setY(yParam);
    assertEquals(y, text.getY(), Configuration.EPSILON);
    assertEquals(y, text.getY(), Configuration.EPSILON);
    text.setY((Double) null);
    assertNull(text.getY());
    text.setY(y);
    assertEquals(y, text.getY(), Configuration.EPSILON);

    text.setX(xParam);
    assertEquals(x, text.getX(), Configuration.EPSILON);
    text.setX((Double) null);
    assertNull(text.getX());
    text.setX(x);
    assertEquals(x, text.getX(), Configuration.EPSILON);

    text.setFontSize(fontSizeParam);
    assertEquals(fontSize, text.getFontSize(), Configuration.EPSILON);
    text.setFontSize((Double) null);
    assertNull(text.getFontSize());
    text.setFontSize(fontSize);
    assertEquals(fontSize, text.getFontSize(), Configuration.EPSILON);

    text.setWidth(widthParam);
    assertEquals(width, text.getWidth(), Configuration.EPSILON);
    text.setWidth((Double) null);
    assertNull(text.getWidth());
    text.setWidth(width);
    assertEquals(width, text.getWidth(), Configuration.EPSILON);

    text.setHeight(heightParam);
    assertEquals(height, text.getHeight(), Configuration.EPSILON);
    text.setHeight((Double) null);
    assertNull(text.getHeight());
    text.setHeight(height);
    assertEquals(height, text.getHeight(), Configuration.EPSILON);
  }
}
