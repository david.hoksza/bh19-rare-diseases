package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

public class RnaTest {
  Logger logger = LogManager.getLogger(RnaTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Rna());
  }

  @Test
  public void testConstructor1() {
    Rna rna = new Rna("d");
    assertNotNull(rna);
  }

  @Test
  public void testConstructor() {
    Rna rna = new Rna("d");
    rna.addCodingRegion(new CodingRegion());
    Rna rna2 = new Rna(rna);
    assertEquals(rna.getRegions().size(), rna2.getRegions().size());
  }

  @Test
  public void testGetters() {
    Rna rna = new Rna(new Rna());
    assertNotNull(rna.getStringType());
    List<ModificationResidue> regions = new ArrayList<>();

    rna.setRegions(regions);

    assertEquals(regions, rna.getRegions());
  }

  @Test
  public void testCopy() {
    Rna rna = new Rna().copy();
    assertNotNull(rna);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Rna object = Mockito.spy(Rna.class);
    object.copy();
  }

}
