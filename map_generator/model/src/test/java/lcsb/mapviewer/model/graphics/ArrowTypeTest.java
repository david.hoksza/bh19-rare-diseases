package lcsb.mapviewer.model.graphics;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class ArrowTypeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (ArrowType type : ArrowType.values()) {
      assertNotNull(type);

      // for coverage tests
      ArrowType.valueOf(type.toString());
    }
  }

}
