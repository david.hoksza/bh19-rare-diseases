package lcsb.mapviewer.model.map.statistics;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SearchHistoryTest.class,
    SearchTypeTest.class,
})
public class AllStatisticsTests {

}
