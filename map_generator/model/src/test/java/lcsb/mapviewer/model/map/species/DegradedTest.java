package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class DegradedTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Degraded());
  }

  @Test
  public void testConstructor1() {
    Degraded degraded = new Degraded("");
    assertNotNull(degraded);
  }

  @Test
  public void testGetters() {
    Degraded degraded = new Degraded(new Degraded());
    assertNotNull(degraded.getStringType());
  }

  @Test
  public void testCopy() {
    Degraded degraded = new Degraded().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Degraded object = Mockito.spy(Degraded.class);
    object.copy();
  }

}
