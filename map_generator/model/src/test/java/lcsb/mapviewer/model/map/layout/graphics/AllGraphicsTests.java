package lcsb.mapviewer.model.map.layout.graphics;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LayerComparatorTest.class,
    LayerOvalComparatorTest.class,
    LayerOvalTest.class,
    LayerRectComparatorTest.class,
    LayerRectTest.class,
    LayerTest.class,
    LayerTextComparatorTest.class,
    LayerTextTest.class,
})
public class AllGraphicsTests {

}
