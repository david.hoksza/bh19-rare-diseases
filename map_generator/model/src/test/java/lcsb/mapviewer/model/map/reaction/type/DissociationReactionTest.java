package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.*;

public class DissociationReactionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new DissociationReaction());
  }

  @Test
  public void testGetters() {
    Reaction reaction = new DissociationReaction();
    assertNotNull(reaction.getStringType());
    assertNotNull(reaction.getReactionRect());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg() {
    Reaction reaction = new Reaction();
    new DissociationReaction(reaction);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg2() {
    Reaction reaction = new Reaction();
    reaction.addProduct(new Product());
    reaction.addProduct(new Product());
    new DissociationReaction(reaction);
  }

  @Test
  public void testConstructor() {
    Reaction reaction = new Reaction();
    reaction.addProduct(new Product());
    reaction.addProduct(new Product());
    reaction.addReactant(new Reactant());
    DissociationReaction validReaction = new DissociationReaction(reaction);
    assertNotNull(validReaction);
  }

  @Test
  public void testCopy() {
    DissociationReaction original = new DissociationReaction();
    original.addProduct(new Product());
    original.addProduct(new Product());
    original.addReactant(new Reactant());
    DissociationReaction product = original.copy();
    assertNotNull(product);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(new DissociationReaction()).copy();
  }

}
