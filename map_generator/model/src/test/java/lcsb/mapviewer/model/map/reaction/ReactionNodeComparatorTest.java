package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.*;

public class ReactionNodeComparatorTest extends ModelTestFunctions {

  ReactionNodeComparator comparator = new ReactionNodeComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    assertEquals(0, comparator.compare(new Modifier(), new Modifier()));

    ReactionNode operator1 = createNodeOperator();
    ReactionNode operator2 = createNodeOperator();

    assertEquals(0, comparator.compare(operator1, operator2));

    assertEquals(0, comparator.compare(operator1, operator1));

    assertEquals(0, comparator.compare(null, null));
  }

  @Test
  public void testDifferent() {
    ReactionNode operator1 = createNodeOperator();
    ReactionNode operator2 = createNodeOperator();

    operator1.getLine().addPoint(new Point2D.Double(1, 1));
    assertTrue(comparator.compare(operator1, operator2) != 0);
    assertTrue(comparator.compare(operator2, operator1) != 0);

    operator1 = createNodeOperator();
    operator2 = createNodeOperator();

    operator1 = new Product();
    assertTrue(comparator.compare(operator1, operator2) != 0);
    assertTrue(comparator.compare(operator2, operator1) != 0);

    operator1 = createNodeOperator();
    operator2 = createNodeOperator();

    operator1 = new Reactant();
    assertTrue(comparator.compare(operator1, operator2) != 0);
    assertTrue(comparator.compare(operator2, operator1) != 0);

    operator1 = createNodeOperator();
    operator2 = createNodeOperator();

    operator1.setElement(new GenericProtein("new_id"));
    assertTrue(comparator.compare(operator1, operator2) != 0);
    assertTrue(comparator.compare(operator2, operator1) != 0);

    operator1 = createNodeOperator();
    operator2 = createNodeOperator();

    operator1.getElement().setName("bla");
    assertTrue(comparator.compare(operator1, operator2) != 0);
    assertTrue(comparator.compare(operator2, operator1) != 0);

    assertTrue(comparator.compare(null, operator1) != 0);
    assertTrue(comparator.compare(operator2, null) != 0);
  }

  private ReactionNode createNodeOperator() {

    Species species = new Gene("id_1");
    species.setName("gene b");
    species.setX(320.0);
    species.setY(30.0);
    species.setWidth(30.0);
    species.setHeight(40.0);

    Modifier modifier = new Modifier(species);

    PolylineData pd = new PolylineData();
    pd.addPoint(new Point2D.Double(1, 2));
    pd.addPoint(new Point2D.Double(1, 21));
    pd.addPoint(new Point2D.Double(11, 2));
    modifier.setLine(pd);

    return modifier;
  }

  @Test
  public void testDifferentStoichiometry() {
    ReactionNode operator1 = createNodeOperator();
    ReactionNode operator2 = createNodeOperator();

    operator1.setStoichiometry(2.0);

    assertTrue(0 != comparator.compare(operator1, operator2));
    assertTrue(0 != comparator.compare(operator2, operator1));

    operator1.setStoichiometry(3.0);

    assertTrue(0 != comparator.compare(operator1, operator2));
    assertTrue(0 != comparator.compare(operator2, operator1));
  }

}
