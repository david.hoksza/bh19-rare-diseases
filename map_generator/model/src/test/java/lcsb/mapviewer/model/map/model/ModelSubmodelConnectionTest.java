package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class ModelSubmodelConnectionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new ModelSubmodelConnection());
  }

  @Test
  public void testConstructor() {
    ModelSubmodelConnection connection = new ModelSubmodelConnection(new ModelFullIndexed(null),
        SubmodelType.DOWNSTREAM_TARGETS, "str");
    ModelSubmodelConnection connection2 = new ModelSubmodelConnection(connection);
    assertNotNull(connection2);
  }

  @Test
  public void testConstructor2() {
    ModelSubmodelConnection connection = new ModelSubmodelConnection(new ModelFullIndexed(null),
        SubmodelType.DOWNSTREAM_TARGETS, "str");

    assertNotNull(connection);
  }

  @Test
  public void testCopy() {
    ModelSubmodelConnection connection = new ModelSubmodelConnection(new ModelFullIndexed(null),
        SubmodelType.DOWNSTREAM_TARGETS, "str");
    ModelSubmodelConnection degraded = connection.copy();
    assertNotNull(degraded);
  }

  @Test
  public void testGetters() {
    ModelSubmodelConnection connection = new ModelSubmodelConnection(new ModelFullIndexed(null),
        SubmodelType.DOWNSTREAM_TARGETS, "str");

    int id = 30;
    connection.setId(id);
    assertEquals(id, connection.getId());
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(ModelSubmodelConnection.class).copy();
  }

}
