package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.*;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.species.field.PositionToCompartment;

public class SpeciesComparatorTest extends ModelTestFunctions {

  SpeciesComparator comparator = new SpeciesComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Species species1 = createSpecies();
    Species species2 = createSpecies();

    assertEquals(0, comparator.compare(species1, species2));

    assertEquals(0, comparator.compare(null, null));

    assertEquals(0, comparator.compare(new Complex(), new Complex()));
  }

  @Test(expected = NotImplementedException.class)
  public void testCompareInvalid() {
    Species speciesMock = Mockito.mock(Species.class);
    comparator.compare(speciesMock, speciesMock);
  }

  private Species createSpecies() {
    GenericProtein result = new GenericProtein();
    result.setName("a");

    result.setElementId("asd");
    result.setX(12.0);
    result.setY(123.0);
    result.setWidth(4);
    result.setHeight(5);
    result.setFontSize(9.0);
    result.setFillColor(Color.BLUE);
    result.setVisibilityLevel(14);
    result.setStateLabel("123");
    result.setStatePrefix("1234");

    return result;
  }

  @Test
  public void testDifferent() throws Exception {
    Species species1 = createSpecies();
    Species species2 = createSpecies();

    assertTrue(comparator.compare(species1, null) != 0);
    assertTrue(comparator.compare(null, species1) != 0);

    assertTrue(comparator.compare(new Complex(), species1) != 0);

    species1 = createSpecies();
    species2 = createSpecies();

    species1.setElementId("tmp");

    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSpecies();
    species2 = createSpecies();

    species1.setStateLabel("tmp");

    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSpecies();
    species2 = createSpecies();

    species1.setStatePrefix("tmp");

    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSpecies();
    species2 = createSpecies();

    species1.setName("new namne");

    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSpecies();
    species2 = createSpecies();

    species1.setActivity(true);

    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSpecies();
    species2 = createSpecies();

    species1.setLineWidth(453.75);

    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);
  }

  @Test(expected = NotImplementedException.class)
  public void testException() {
    Species mock = Mockito.mock(Species.class);
    comparator.compare(mock, mock);
  }

  @Test
  public void testEquals2() {
    SimpleMolecule species1 = createSimpleMolecule();
    SimpleMolecule species2 = createSimpleMolecule();
    assertEquals(0, comparator.compare(species1, species2));

    assertEquals(0, comparator.compare(null, null));

    assertEquals(0, comparator.compare(new AntisenseRna(), new AntisenseRna()));
    assertEquals(0, comparator.compare(new Degraded(), new Degraded()));
    assertEquals(0, comparator.compare(new Drug(), new Drug()));
    assertEquals(0, comparator.compare(new Ion(), new Ion()));
    assertEquals(0, comparator.compare(new Phenotype(), new Phenotype()));
    assertEquals(0, comparator.compare(new Rna(), new Rna()));
  }

  @Test
  public void testDifferent2() {
    SimpleMolecule species1 = createSimpleMolecule();
    SimpleMolecule species2 = createSimpleMolecule();

    species1.setCharge(99);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);
    assertTrue(comparator.compare(null, species1) != 0);
    assertTrue(comparator.compare(species1, null) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setHomodimer(1233);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setElementId("");
    species1.setElementId("ASD");
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setNotes("ASD");
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setPositionToCompartment(PositionToCompartment.TRANSMEMBRANE);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.getMiriamData().clear();
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setInitialAmount(4.0);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setInitialConcentration(4.0);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setOnlySubstanceUnits(false);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setHypothetical(true);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.getMiriamData().iterator().next().setRelationType(MiriamRelationType.BQ_BIOL_IS);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.addMiriamData(new MiriamData());
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    assertTrue(comparator.compare(new Rna(), new Drug()) != 0);
  }

  public SimpleMolecule createSimpleMolecule() {
    SimpleMolecule result = new SimpleMolecule();
    result.setHomodimer(12);
    result.setElementId("id");
    result.setName("id");
    result.setInitialAmount(12.0);
    result.setCharge(13);
    result.setInitialConcentration(14.0);
    result.setOnlySubstanceUnits(true);
    result.setPositionToCompartment(PositionToCompartment.INSIDE);
    result.setNotes("id");
    MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.UNKNOWN, "c");
    result.addMiriamData(md);
    return result;
  }

  @Test
  public void testDifferentConstant() {
    SimpleMolecule species1 = createSimpleMolecule();
    SimpleMolecule species2 = createSimpleMolecule();
    species1.setConstant(true);
    assertTrue(0 != comparator.compare(species1, species2));
  }

  @Test
  public void testDifferentBoundaryCondition() {
    SimpleMolecule species1 = createSimpleMolecule();
    SimpleMolecule species2 = createSimpleMolecule();
    species1.setBoundaryCondition(true);
    assertTrue(0 != comparator.compare(species1, species2));
  }

  @Test
  public void testDifferentSubstanceUnits() {
    SimpleMolecule species1 = createSimpleMolecule();
    SimpleMolecule species2 = createSimpleMolecule();
    species1.setSubstanceUnits(SbmlUnitType.HERTZ);
    assertTrue(0 != comparator.compare(species1, species2));
  }

}
