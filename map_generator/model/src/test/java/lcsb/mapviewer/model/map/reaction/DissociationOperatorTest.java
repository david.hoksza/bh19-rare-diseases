package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class DissociationOperatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new DissociationOperator());
  }

  @Test
  public void testConstructor() {
    DissociationOperator op = new DissociationOperator();
    op.setLine(new PolylineData());
    DissociationOperator operator = new DissociationOperator(op);
    assertNotNull(operator);
  }

  @Test
  public void testGetters() {
    assertFalse("".equals(new DissociationOperator().getSBGNOperatorText()));
    assertFalse("".equals(new DissociationOperator().getOperatorText()));
  }

  @Test
  public void testCopy1() {
    DissociationOperator op = new DissociationOperator();
    op.setLine(new PolylineData());
    DissociationOperator operator = op.copy();
    assertNotNull(operator);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    Mockito.mock(DissociationOperator.class, Mockito.CALLS_REAL_METHODS).copy();
  }

}
