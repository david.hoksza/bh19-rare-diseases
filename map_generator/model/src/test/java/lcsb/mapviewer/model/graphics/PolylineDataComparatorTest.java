package lcsb.mapviewer.model.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.*;
import java.awt.geom.Point2D;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class PolylineDataComparatorTest extends ModelTestFunctions {

  PolylineDataComparator comparator = new PolylineDataComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    PolylineData pd1 = new PolylineData();
    PolylineData pd2 = new PolylineData();
    assertEquals(0, comparator.compare(pd1, pd2));
    assertEquals(0, comparator.compare(null, null));
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    class Tmp extends PolylineData {
      private static final long serialVersionUID = 1L;
    }
    Tmp pd1 = new Tmp();
    Tmp pd2 = new Tmp();
    comparator.compare(pd1, pd2);
  }

  @Test
  public void testDifferent() {
    PolylineData pd1 = new PolylineData();
    PolylineData pd2 = new PolylineData();

    assertTrue(comparator.compare(pd2, null) != 0);
    assertTrue(comparator.compare(null, pd1) != 0);

    pd1 = new PolylineData();
    pd2 = new PolylineData();
    pd2.addPoint(new Point2D.Double(1, 2));
    pd2.addPoint(new Point2D.Double(1, 3));
    assertTrue(comparator.compare(pd1, pd2) != 0);
    assertTrue(comparator.compare(pd2, pd1) != 0);

    pd1 = new PolylineData();
    pd2 = new PolylineData();
    ArrowTypeData atd = new ArrowTypeData();
    atd.setLen(34);
    pd2.setBeginAtd(atd);
    assertTrue(comparator.compare(pd1, pd2) != 0);
    assertTrue(comparator.compare(pd2, pd1) != 0);

    pd1 = new PolylineData();
    pd2 = new PolylineData();
    atd = new ArrowTypeData();
    atd.setLen(34);
    pd2.setEndAtd(atd);
    assertTrue(comparator.compare(pd1, pd2) != 0);
    assertTrue(comparator.compare(pd2, pd1) != 0);

    pd1 = new PolylineData();
    pd2 = new PolylineData();
    pd2.setColor(Color.BLUE);
    assertTrue(comparator.compare(pd1, pd2) != 0);
    assertTrue(comparator.compare(pd2, pd1) != 0);

    pd1 = new PolylineData();
    pd2 = new PolylineData();
    pd2.setType(LineType.DASHED);
    assertTrue(comparator.compare(pd1, pd2) != 0);
    assertTrue(comparator.compare(pd2, pd1) != 0);

    assertTrue(comparator.compare(pd2, Mockito.spy(PolylineData.class)) != 0);
  }

}
