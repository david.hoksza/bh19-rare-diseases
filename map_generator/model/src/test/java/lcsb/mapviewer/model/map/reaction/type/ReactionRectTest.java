package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class ReactionRectTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (ReactionRect type : ReactionRect.values()) {
      assertNotNull(type);

      // for coverage tests
      ReactionRect.valueOf(type.toString());
      assertNotNull(type.getText());
    }
  }
}
