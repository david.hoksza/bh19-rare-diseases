package lcsb.mapviewer.model.map.modifier;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class UnknownInhibitionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new UnknownInhibition());
  }

  @Test
  public void testConstructor() {
    UnknownInhibition modifier = new UnknownInhibition(new GenericProtein("unk_id"));
    assertNotNull(modifier);
  }

  @Test
  public void testConstructor2() {
    UnknownInhibition modifier = new UnknownInhibition(new GenericProtein("unk_id"));
    modifier.setLine(new PolylineData());
    UnknownInhibition modifier2 = new UnknownInhibition(modifier);
    assertNotNull(modifier2);
  }

  @Test
  public void testCopy() {
    UnknownInhibition modifier = new UnknownInhibition(new GenericProtein("unk_id"));
    modifier.setLine(new PolylineData());
    UnknownInhibition modifier2 = modifier.copy();
    assertNotNull(modifier2);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopyInvalid() {
    Mockito.mock(UnknownInhibition.class, Mockito.CALLS_REAL_METHODS).copy();
  }

}
