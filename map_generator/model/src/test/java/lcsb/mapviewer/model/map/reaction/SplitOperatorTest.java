package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class SplitOperatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new SplitOperator());
  }

  @Test
  public void testConstructor() {
    SplitOperator op = new SplitOperator();
    op.setLine(new PolylineData());
    SplitOperator operator = new SplitOperator(op);
    assertNotNull(operator);
  }

  @Test
  public void testGetters() {
    assertNotNull(new SplitOperator().getSBGNOperatorText());
    assertNotNull(new SplitOperator().getOperatorText());
  }

  @Test
  public void testCopy1() {
    SplitOperator op = new SplitOperator();
    op.setLine(new PolylineData());
    SplitOperator operator = op.copy();
    assertNotNull(operator);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    Mockito.mock(SplitOperator.class, Mockito.CALLS_REAL_METHODS).copy();
  }

}
