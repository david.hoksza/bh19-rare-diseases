package lcsb.mapviewer.model.graphics;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class LineTypeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (LineType type : LineType.values()) {
      assertNotNull(type);
      assertNotNull(type.getStroke());

      // for coverage tests
      LineType.valueOf(type.toString());
    }
  }

}
