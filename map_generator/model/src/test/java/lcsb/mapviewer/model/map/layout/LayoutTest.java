package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.*;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.cache.UploadedFileEntry;

public class LayoutTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Layout());
  }

  @Test
  public void testConstructor() {
    String title = "TIT";
    boolean publicL = true;
    Layout layout = new Layout(title, publicL);
    assertEquals(title, layout.getTitle());
    assertEquals(publicL, layout.isPublicLayout());
  }

  @Test
  public void testConstructor2() {
    String title = "TIT";
    boolean publicL = true;
    Layout layout = new Layout(title, publicL);
    Layout layout2 = new Layout(layout);
    assertNotNull(layout2);
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setFileContent(new byte[] {});

    layout.setInputData(fileEntry);
    Layout layout3 = new Layout(layout);
    assertNotNull(layout3);
  }

  @Test
  public void testConstructorWithOrder() {
    boolean publicL = true;
    Layout overlay1 = new Layout(null, publicL);
    overlay1.setOrderIndex(12);
    Layout overlay2 = new Layout(overlay1);
    assertEquals(overlay1.getOrderIndex(), overlay2.getOrderIndex());
  }

  @Test
  public void testCopy() {
    String title = "TIT";
    boolean publicL = true;
    Layout layout = new Layout(title, publicL);
    Layout layout2 = layout.copy();
    assertNotNull(layout2);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(Layout.class).copy();
  }

  @Test
  public void testGetters() {
    boolean hierarchicalView = false;
    boolean publicLayout = true;
    int id = 62;
    String description = "qds";
    String title = "tit";
    LayoutStatus status = LayoutStatus.FAILURE;
    double progress = 1.6;
    UploadedFileEntry inputData = new UploadedFileEntry();
    Layout layout = new Layout();
    layout.setHierarchicalView(hierarchicalView);
    assertEquals(hierarchicalView, layout.isHierarchicalView());
    layout.setId(id);
    assertEquals(id, layout.getId());
    layout.setDescription(description);
    assertEquals(description, layout.getDescription());
    layout.setTitle(title);
    assertEquals(title, layout.getTitle());
    layout.setCreator(null);
    assertNull(layout.getCreator());
    layout.setStatus(status);
    assertEquals(status, layout.getStatus());
    layout.setProgress(progress);
    assertEquals(progress, layout.getProgress(), Configuration.EPSILON);
    layout.setInputData(inputData);
    assertEquals(inputData, layout.getInputData());
    layout.setPublicLayout(publicLayout);
    assertEquals(publicLayout, layout.isPublicLayout());
  }

}
