package lcsb.mapviewer.model.security;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;

public class PrivilegeTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Privilege(PrivilegeType.IS_ADMIN));
  }

}
