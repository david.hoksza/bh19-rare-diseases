package lcsb.mapviewer.model.user.annotator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AnnotatorParameterTest.class })
public class AllAnnotatorTests {

}
