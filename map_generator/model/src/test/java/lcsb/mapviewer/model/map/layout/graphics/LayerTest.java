package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.ModelData;

public class LayerTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Layer());
  }

  @Test
  public void testConstructor() {
    Layer layer = new Layer();
    layer.addLayerText(new LayerText());
    layer.addLayerLine(new PolylineData());
    layer.addLayerRect(new LayerRect());
    layer.addLayerOval(new LayerOval());
    Layer copy = new Layer(layer);

    assertNotNull(copy);
  }

  @Test
  public void testCopy() {
    Layer degraded = new Layer().copy();
    assertNotNull(degraded);
  }

  @Test
  public void testSetters() {
    Layer layer = new Layer().copy();

    ModelData model = new ModelData();
    List<LayerOval> ovals = new ArrayList<>();
    List<LayerRect> rectangles = new ArrayList<>();
    List<PolylineData> lines = new ArrayList<>();
    List<LayerText> texts = new ArrayList<>();

    boolean locked = true;
    boolean visible = true;
    String name = "sdtr";
    String layerId = "id";

    layer.setModel(model);
    layer.setOvals(ovals);
    layer.setTexts(texts);
    layer.setRectangles(rectangles);
    layer.setLines(lines);
    layer.setLocked(locked);
    layer.setVisible(visible);
    layer.setName(name);
    layer.setLayerId(layerId);

    assertEquals(model, layer.getModel());
    assertEquals(ovals, layer.getOvals());
    assertEquals(rectangles, layer.getRectangles());
    assertEquals(lines, layer.getLines());
    assertEquals(texts, layer.getTexts());
    assertEquals(locked, layer.isLocked());
    assertEquals(name, layer.getName());
    assertEquals(visible, layer.isVisible());
    assertEquals(layerId, layer.getLayerId());

    layer.setLocked("FALSE");
    layer.setVisible("FALSE");

    assertFalse(layer.isLocked());
    assertFalse(layer.isVisible());
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(Layer.class).copy();
  }

  @Test
  public void testRemoveLayerText() {
    Layer layer = new Layer();
    layer.removeLayerText(new LayerText());
  }

  @Test
  public void testAddLines() {
    Layer layer = new Layer();
    List<PolylineData> lines = new ArrayList<>();
    lines.add(new PolylineData());
    layer.addLayerLines(lines);
    assertEquals(1, layer.getLines().size());
  }

}
