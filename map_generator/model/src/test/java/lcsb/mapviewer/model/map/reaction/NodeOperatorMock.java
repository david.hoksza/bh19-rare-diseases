package lcsb.mapviewer.model.map.reaction;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class NodeOperatorMock extends NodeOperator {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public NodeOperatorMock(NodeOperator operator) {
    super(operator);
  }

  public NodeOperatorMock() {
    super();
  }

  @Override
  public String getOperatorText() {
    return "mock string";
  }

  @Override
  public AbstractNode copy() {
    throw new NotImplementedException();
  }

}
