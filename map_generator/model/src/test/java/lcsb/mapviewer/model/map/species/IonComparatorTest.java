package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class IonComparatorTest extends ModelTestFunctions {

  IonComparator comparator = new IonComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Ion ion1 = createIon();
    Ion ion2 = createIon();

    assertEquals(0, comparator.compare(ion1, ion1));

    assertEquals(0, comparator.compare(ion1, ion2));
    assertEquals(0, comparator.compare(ion2, ion1));
  }

  @Test
  public void testDifferent() {
    Ion ion2 = createIon();
    assertTrue(comparator.compare(null, ion2) != 0);
    assertTrue(comparator.compare(ion2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    Ion ion = createIon();
    ion.setName("n");
    assertTrue(comparator.compare(ion, ion2) != 0);

    assertTrue(comparator.compare(ion, Mockito.mock(Ion.class)) != 0);
  }

  public Ion createIon() {
    Ion result = new Ion();
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    Ion object = Mockito.mock(Ion.class);

    comparator.compare(object, object);
  }

}
