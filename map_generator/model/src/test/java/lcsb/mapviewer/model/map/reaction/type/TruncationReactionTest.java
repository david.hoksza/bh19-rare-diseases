package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.*;

public class TruncationReactionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new TruncationReaction());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg() {
    Reaction reaction = new Reaction();
    new TruncationReaction(reaction);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg2() {
    Reaction reaction = new Reaction();
    reaction.addProduct(new Product());
    reaction.addProduct(new Product());
    new TruncationReaction(reaction);
  }

  @Test
  public void testConstructor() {
    Reaction reaction = new Reaction();
    reaction.addProduct(new Product());
    reaction.addProduct(new Product());
    reaction.addReactant(new Reactant());
    TruncationReaction validReaction = new TruncationReaction(reaction);
    assertNotNull(validReaction);
  }

  @Test
  public void testCopy() {
    TruncationReaction original = new TruncationReaction();
    original.addProduct(new Product());
    original.addProduct(new Product());
    original.addReactant(new Reactant());
    TruncationReaction product = original.copy();
    assertNotNull(product);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(new TruncationReaction()).copy();
  }

  @Test
  public void testGetters() {
    Reaction reaction = new TruncationReaction();
    assertNotNull(reaction.getStringType());
    assertNotNull(reaction.getReactionRect());
  }

}
