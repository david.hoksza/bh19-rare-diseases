package lcsb.mapviewer.model.graphics;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ArrowTypeDataComparatorTest.class,
    ArrowTypeDataTest.class,
    ArrowTypeTest.class,
    LineTypeTest.class,
    PolylineDataTest.class,
    PolylineDataComparatorTest.class,

})
public class AllGraphicsTests {

}
