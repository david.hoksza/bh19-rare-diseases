package lcsb.mapviewer.model.map.species.field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.geom.Point2D;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.AntisenseRna;

public class CodingRegionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CodingRegion());
  }

  @Test
  public void testConstructor1() {
    CodingRegion antisenseRna = new CodingRegion();
    CodingRegion antisenseRna2 = new CodingRegion(antisenseRna);
    assertNotNull(antisenseRna2);
  }

  @Test
  public void testUpdate() {
    CodingRegion antisenseRna = new CodingRegion();
    antisenseRna.setName("as");
    antisenseRna.setPosition(new Point2D.Double(10, 0));
    CodingRegion antisenseRna2 = new CodingRegion();
    antisenseRna2.update(antisenseRna);
    assertEquals(antisenseRna.getName(), antisenseRna2.getName());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidUpdate() {
    CodingRegion antisenseRna = new CodingRegion();
    CodingRegion antisenseRna2 = new CodingRegion();
    antisenseRna.setIdModificationResidue("@1");
    antisenseRna2.setIdModificationResidue("@");
    antisenseRna2.update(antisenseRna);
  }

  @Test
  public void testGetters() {
    CodingRegion region = new CodingRegion(new CodingRegion());
    int id = 91;
    AntisenseRna species = new AntisenseRna("id");
    Point2D position = new Point2D.Double(10, 20);
    double width = 5.3;
    String name = "nam";
    String idCodingRegion = "iddd";

    region.setId(id);
    region.setSpecies(species);
    region.setPosition(position);
    region.setWidth(width);
    region.setName(name);
    region.setIdModificationResidue(idCodingRegion);

    assertEquals(id, region.getId());
    assertEquals(species, region.getSpecies());
    assertEquals(position.distance(region.getPosition()), 0, Configuration.EPSILON);
    assertEquals(width, region.getWidth(), Configuration.EPSILON);
    assertEquals(name, region.getName());
    assertEquals(idCodingRegion, region.getIdModificationResidue());
  }

  @Test
  public void testToString() {
    assertNotNull(new CodingRegion().toString());
  }

  @Test
  public void testCopy() {
    CodingRegion degraded = new CodingRegion().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.mock(CodingRegion.class, Mockito.CALLS_REAL_METHODS).copy();
  }

}
