package lcsb.mapviewer.model.map.reaction.type;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    DissociationReactionTest.class,
    HeterodimerAssociationReactionTest.class,
    KnownTransitionOmittedReactionTest.class,
    NegativeInfluenceReactionTest.class,
    PositiveInfluenceReactionTest.class,
    ReactionRectTest.class,
    ReducedModulationReactionTest.class,
    ReducedPhysicalStimulationReactionTest.class,
    ReducedTriggerReactionTest.class,
    StateTransitionReactionTest.class,
    TranscriptionReactionTest.class,
    TranslationReactionTest.class,
    TransportReactionTest.class,
    TruncationReactionTest.class,
    UnknownNegativeInfluenceReactionTest.class,
    UnknownPositiveInfluenceReactionTest.class,
    UnknownReducedModulationReactionTest.class,
    UnknownReducedPhysicalStimulationReactionTest.class,
    UnknownReducedTriggerReactionTest.class,
    UnknownTransitionReactionTest.class,
})
public class AllReactionTypeTests {

}
