package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class OrOperatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new OrOperator());
  }

  @Test
  public void testConstructor() {
    OrOperator op = new OrOperator();
    op.setLine(new PolylineData());
    OrOperator operator = new OrOperator(op);
    assertNotNull(operator);
  }

  @Test
  public void testGetters() {
    assertFalse("".equals(new OrOperator().getSBGNOperatorText()));
    assertFalse("".equals(new OrOperator().getOperatorText()));
  }

  @Test
  public void testCopy1() {
    OrOperator op = new OrOperator();
    op.setLine(new PolylineData());
    OrOperator operator = op.copy();
    assertNotNull(operator);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    Mockito.mock(OrOperator.class, Mockito.CALLS_REAL_METHODS).copy();
  }

}
