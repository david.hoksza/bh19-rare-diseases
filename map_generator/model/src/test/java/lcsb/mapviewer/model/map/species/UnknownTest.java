package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class UnknownTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Unknown());
  }

  @Test
  public void testConstructor1() {
    Unknown degraded = new Unknown(new Unknown());
    assertNotNull(degraded);
  }

  @Test
  public void testGetters() {
    Unknown degraded = new Unknown("id");
    assertNotNull(degraded.getStringType());
  }

  @Test
  public void testCopy() {
    Unknown degraded = new Unknown().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Unknown object = Mockito.spy(Unknown.class);
    object.copy();
  }

}
