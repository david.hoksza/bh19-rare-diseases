package lcsb.mapviewer.model.map;

import static org.junit.Assert.*;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class MiriamDataTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new MiriamData());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidConstructor1() {
    new MiriamData(null, null);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidConstructor2() {
    new MiriamData(null, MiriamType.CAS, null);
  }

  @Test
  public void testGetIdFromIdentifier() {
    assertEquals("id", MiriamData.getIdFromIdentifier("name:id"));
  }

  @Test
  public void testGetters() {
    int id = 6;
    MiriamData md = new MiriamData();
    md.setResource(null);
    assertNull(md.getResource());

    md.setId(id);
    assertEquals(id, md.getId());

    md.setAnnotator(this.getClass());
    assertEquals(this.getClass(), md.getAnnotator());
  }

  @Test
  public void testToString() {
    assertNotNull(new MiriamData().toString());
    assertNotNull(new MiriamData(MiriamType.CAS, "a").toString());
  }

  @Test
  public void testEqual() {
    MiriamData md = new MiriamData();
    assertFalse(md.equals(new Object()));
  }

  @Test
  public void testCompareTo() {
    MiriamData md = new MiriamData();
    assertTrue(md.compareTo(new MiriamData(MiriamType.CAS, "a")) != 0);

    MiriamData md1 = new MiriamData(MiriamType.CAS, "a");
    MiriamData md2 = new MiriamData(MiriamType.CAS, "a");
    assertTrue(md1.compareTo(md2) == 0);

    md1.setAnnotator(Integer.class);
    assertTrue(md1.compareTo(md2) != 0);

    md2.setAnnotator(Integer.class);
    assertTrue(md1.compareTo(md2) == 0);
  }
}
