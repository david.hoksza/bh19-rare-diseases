package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;

import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;

public class SpeciesTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetter() {
    Species species = new GenericProtein("id");
    String state = "as";
    species.setState(state);

    assertEquals(state, species.getState());
  }

}
