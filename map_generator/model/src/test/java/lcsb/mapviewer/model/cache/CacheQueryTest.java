package lcsb.mapviewer.model.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;

public class CacheQueryTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CacheQuery());
  }

  @Test
  public void testGetters() {
    Calendar expires = Calendar.getInstance();
    expires.add(Calendar.DATE, 1);
    Calendar accessed = Calendar.getInstance();
    accessed.add(Calendar.DATE, 10);
    int id = 120;
    String query = "query1";
    int type = 34;
    int type2 = 45;
    String value = "val";
    CacheType ctype = new CacheType();
    ctype.setId(type2);

    CacheQuery cq = new CacheQuery();
    cq.setAccessed(accessed);
    assertTrue(accessed.equals(cq.getAccessed()));
    cq.setExpires(expires);
    assertTrue(expires.equals(cq.getExpires()));
    cq.setId(id);
    assertEquals(id, cq.getId());
    cq.setQuery(query);
    assertEquals(query, cq.getQuery());
    cq.setType(ctype);
    assertEquals((Integer) type2, cq.getType());
    cq.setType(type);
    assertEquals((Integer) type, cq.getType());
    cq.setValue(value);
    assertEquals(value, cq.getValue());
  }

}
