package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class UnknownComparatorTest extends ModelTestFunctions {

  UnknownComparator comparator = new UnknownComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Unknown aRna1 = createUnknown();
    Unknown aRna2 = createUnknown();

    assertEquals(0, comparator.compare(aRna1, aRna1));

    assertEquals(0, comparator.compare(aRna1, aRna2));
    assertEquals(0, comparator.compare(aRna2, aRna1));
  }

  @Test
  public void testDifferent() {
    Unknown unknown2 = createUnknown();
    assertTrue(comparator.compare(null, unknown2) != 0);
    assertTrue(comparator.compare(unknown2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    Unknown unknown = createUnknown();
    unknown.setName("n");
    assertTrue(comparator.compare(unknown, unknown2) != 0);

    assertTrue(comparator.compare(unknown, Mockito.mock(Unknown.class)) != 0);
  }

  public Unknown createUnknown() {
    Unknown result = new Unknown();
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    Unknown object = Mockito.mock(Unknown.class);

    comparator.compare(object, object);
  }

}
