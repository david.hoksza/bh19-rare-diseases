package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class SubmodelTypeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (SubmodelType type : SubmodelType.values()) {
      assertNotNull(type);

      // for coverage tests
      SubmodelType.valueOf(type.toString());
      assertNotNull(type.getCommonName());
    }
  }

}
