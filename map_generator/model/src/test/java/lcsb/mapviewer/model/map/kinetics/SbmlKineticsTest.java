package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class SbmlKineticsTest {

  @Test
  public void testAddParameter() {
    SbmlParameter parameter = new SbmlParameter("");
    SbmlKinetics kinetics = new SbmlKinetics();
    kinetics.addParameter(parameter);
    assertEquals(kinetics.getParameters().iterator().next(), parameter);
  }

  @Test
  public void testAddFunction() {
    SbmlFunction function = new SbmlFunction("");
    SbmlKinetics kinetics = new SbmlKinetics();
    kinetics.addFunction(function);
    assertEquals(1, kinetics.getFunctions().size());
    assertEquals(1, kinetics.getArguments().size());
    assertEquals(kinetics.getArguments().iterator().next(), function);
  }

  @Test
  public void testGetArguments() {
    SbmlParameter parameter = new SbmlParameter("k1");
    SbmlKinetics kinetics = new SbmlKinetics();
    kinetics.addParameter(parameter);
    assertTrue(kinetics.getArguments().contains(parameter));
  }

  @Test
  public void testAddElement() {
    Element protein = new GenericProtein("s1");

    SbmlKinetics kinetics = new SbmlKinetics();
    kinetics.addElement(protein);
    assertEquals(kinetics.getArguments().get(0), protein);
  }

  @Test
  public void testSetDefinition() {
    String definition = "<lambda><bvar><ci> x </ci></bvar></lambda>";

    SbmlKinetics kinetics = new SbmlKinetics();
    kinetics.setDefinition(definition);
    assertEquals(definition, kinetics.getDefinition());
  }

  @Test
  public void testCopy() {
    SbmlKinetics kinetics = new SbmlKinetics();
    kinetics.setDefinition("x");
    kinetics.addArgument(new SbmlParameter("k1"));
    kinetics.addArgument(new GenericProtein("s1"));
    kinetics.addArgument(new SbmlFunction("fun1"));
    SbmlKinetics copy = kinetics.copy();
    SbmlKineticsComparator comparator = new SbmlKineticsComparator();
    assertEquals(0, comparator.compare(copy, kinetics));
  }

}
