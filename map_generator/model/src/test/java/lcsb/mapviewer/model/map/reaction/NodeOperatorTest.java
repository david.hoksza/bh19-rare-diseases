package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class NodeOperatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    NodeOperator operator = new NodeOperatorMock();
    NodeOperator operator2 = new NodeOperatorMock();
    NodeOperator operator3 = new NodeOperatorMock();
    operator.addInput(operator2);
    operator.addOutput(operator3);
    operator.setLine(new PolylineData());

    NodeOperator result = new NodeOperatorMock(operator);
    assertEquals(1, result.getInputs().size());
    assertEquals(1, result.getOutputs().size());
  }

  @Test
  public void testAddInput() {
    NodeOperator operator = new NodeOperatorMock();
    NodeOperator operator2 = new NodeOperatorMock();
    operator.addInput(operator2);
    assertEquals(1, operator.getInputs().size());
  }

  @Test
  public void testAddInputs() {
    NodeOperator operator = new NodeOperatorMock();
    NodeOperator operator2 = new NodeOperatorMock();
    List<NodeOperator> list = new ArrayList<>();
    list.add(operator2);
    operator.addInputs(list);
    assertEquals(1, operator.getInputs().size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddInput2() {
    NodeOperator operator = new NodeOperatorMock();
    NodeOperator operator2 = new NodeOperatorMock();
    operator.addInput(operator2);
    operator.addInput(operator2);
  }

  @Test
  public void testIsReactantOperator() {
    NodeOperator operator = new NodeOperatorMock();
    assertFalse(operator.isReactantOperator());

    AndOperator op1 = new AndOperator();
    operator.addInput(op1);
    assertFalse(operator.isReactantOperator());

    op1.addInput(new Reactant());
    assertTrue(operator.isReactantOperator());
  }

  @Test
  public void testAddOutputs() {
    NodeOperator operator = new NodeOperatorMock();
    NodeOperator operator2 = new NodeOperatorMock();
    List<NodeOperator> list = new ArrayList<>();
    list.add(operator2);
    operator.addOutputs(list);
    assertEquals(1, operator.getOutputs().size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddOutput2() {
    NodeOperator operator = new NodeOperatorMock();
    NodeOperator operator2 = new NodeOperatorMock();
    operator.addOutput(operator2);
    operator.addOutput(operator2);
  }

  @Test
  public void testIsProductOperator() {
    NodeOperator operator = new NodeOperatorMock();
    assertFalse(operator.isReactantOperator());

    AndOperator op1 = new AndOperator();
    operator.addInput(op1);
    assertFalse(operator.isProductOperator());

    op1.addInput(new Reactant());
    assertFalse(operator.isProductOperator());

    SplitOperator op2 = new SplitOperator();
    operator.addOutput(op2);
    assertFalse(operator.isProductOperator());

    op2.addOutput(new Product());
    assertTrue(operator.isProductOperator());
  }

  @Test
  public void testGetters() {
    NodeOperator operator = new NodeOperatorMock();
    assertNotNull(operator.getSBGNOperatorText());
    List<AbstractNode> inputs = new ArrayList<>();
    List<AbstractNode> outputs = new ArrayList<>();
    operator.setInputs(inputs);
    operator.setOutputs(outputs);
    assertEquals(inputs, operator.getInputs());
    assertEquals(outputs, operator.getOutputs());
  }

  @Test
  public void testIsModifierOperator() {
    NodeOperator operator = new NodeOperatorMock();
    assertFalse(operator.isReactantOperator());

    AndOperator op1 = new AndOperator();
    operator.addInput(op1);
    assertFalse(operator.isModifierOperator());

    op1.addInput(new Modifier());
    assertTrue(operator.isModifierOperator());
  }

}
