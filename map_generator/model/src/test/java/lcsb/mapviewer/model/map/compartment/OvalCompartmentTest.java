package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class OvalCompartmentTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new OvalCompartment());
  }

  @Test
  public void testConstructor() {
    OvalCompartment original = new OvalCompartment();
    OvalCompartment copy = new OvalCompartment(original);
    assertNotNull(copy);
  }

  @Test
  public void testCopy() {
    OvalCompartment original = new OvalCompartment();
    OvalCompartment copy = original.copy();
    assertNotNull(copy);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    OvalCompartment compartment = Mockito.spy(OvalCompartment.class);
    compartment.copy();
  }

}
