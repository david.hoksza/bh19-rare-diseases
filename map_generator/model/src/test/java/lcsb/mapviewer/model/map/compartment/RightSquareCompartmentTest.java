package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class RightSquareCompartmentTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new RightSquareCompartment());
  }

  @Test
  public void testConstructor1() {
    RightSquareCompartment compartment = new RightSquareCompartment(new RightSquareCompartment());
    assertNotNull(compartment);
  }

  @Test
  public void testConstructor3() {
    RightSquareCompartment compartment = new RightSquareCompartment("id");
    assertNotNull(compartment);
  }

  @Test
  public void testConstructor2() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(1);
    model.setHeight(1);
    RightSquareCompartment compartment = new RightSquareCompartment(new Compartment(), model);
    assertNotNull(compartment);
  }

  @Test
  public void testCopy() {
    RightSquareCompartment compartment = new RightSquareCompartment().copy();
    assertNotNull(compartment);
  }

  @Test
  public void testSetPoint() {
    RightSquareCompartment compartment = new RightSquareCompartment();
    compartment.setPoint("10", "10");
    assertTrue(compartment.getWidth() < 0);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    RightSquareCompartment compartment = Mockito.spy(RightSquareCompartment.class);
    compartment.copy();
  }

}
