package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.awt.Color;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.StructuralState;

public class ComplexComparatorTest extends ModelTestFunctions {

  Logger logger = LogManager.getLogger(ComplexComparatorTest.class);
  ComplexComparator comparator = new ComplexComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Complex complex1 = createComplex2();
    Complex complex2 = createComplex2();

    assertEquals(0, comparator.compare(complex1, complex2));

    assertEquals(0, comparator.compare(null, null));
  }

  private Complex createComplex2() {
    Complex result = new Complex();
    result.setName("complex");

    result.setElementId("asd");
    result.setX(12.0);
    result.setY(123.0);
    result.setWidth(4);
    result.setHeight(5);
    result.setFontSize(9.0);
    result.setFillColor(Color.BLUE);
    result.setVisibilityLevel("14");

    Species protein = new GenericProtein("S");
    protein.setName("a");
    result.addSpecies(protein);

    return result;
  }

  @Test
  public void testDifferent() {
    Complex complex1 = createComplex2();
    Complex complex2 = createComplex2();

    assertTrue(comparator.compare(complex1, null) != 0);
    assertTrue(comparator.compare(null, complex1) != 0);

    complex1 = createComplex2();
    complex2 = createComplex2();

    complex1.setElementId("tmp");

    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);

    complex1 = createComplex2();
    complex2 = createComplex2();

    complex1.addSpecies(new GenericProtein("id"));

    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);

    complex1 = createComplex2();
    complex2 = createComplex2();

    complex1.getElements().iterator().next().setElementId("bnu");

    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);

    complex1 = createComplex2();
    complex2 = createComplex2();

    Species species = (Species) complex1.getElements().iterator().next();
    species.setName("new namne");

    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);

    complex1 = createComplex2();
    complex2 = createComplex2();

    complex1.setName("new namne");

    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalid() {
    Complex complex1 = createComplex2();
    Complex complex2 = createComplex2();

    GenericProtein protein = new GenericProtein("1");
    complex1.getElements().add(protein);
    protein = new GenericProtein("1");
    complex1.getElements().add(protein);

    complex2.getElements().add(new GenericProtein("b"));
    complex2.getElements().add(new GenericProtein("a"));

    comparator.compare(complex1, complex2);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid3() {
    Complex invalidComplex = Mockito.mock(Complex.class);

    comparator.compare(invalidComplex, invalidComplex);

  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalid2() {
    Complex complex1 = createComplex2();
    Complex complex2 = createComplex2();

    Species protein = new GenericProtein("1");
    complex1.getElements().add(protein);
    protein = new GenericProtein("1");
    complex1.getElements().add(protein);

    complex2.getElements().add(new GenericProtein("A"));
    complex2.getElements().add(new GenericProtein("B"));

    comparator.compare(complex2, complex1);
  }

  @Test
  public void testEquals2() {
    Complex complex1 = createComplex();
    Complex complex2 = createComplex();

    assertEquals(0, comparator.compare(complex1, complex1));

    assertEquals(0, comparator.compare(complex1, complex2));
    assertEquals(0, comparator.compare(complex2, complex1));

    complex2.addSpecies(new GenericProtein("test"));
    complex1.addSpecies(new GenericProtein("test"));
    assertEquals(0, comparator.compare(complex1, complex2));
    assertEquals(0, comparator.compare(complex2, complex1));
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidComp2() throws Exception {
    Complex complex1 = createComplex();
    Complex complex2 = createComplex();

    Species mock = Mockito.mock(Species.class);
    when(mock.getElementId()).thenReturn("id");
    complex1.addSpecies(mock);
    complex2.addSpecies(mock);
    comparator.compare(complex1, complex2);
    comparator.compare(complex2, complex1);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidComp3() throws Exception {
    Complex complex1 = createComplex();
    Complex complex2 = createComplex();
    GenericProtein c = new GenericProtein("test");
    complex2.addSpecies(c);
    complex1.addSpecies(new GenericProtein("test"));
    Species mock = Mockito.mock(Species.class);
    when(mock.getElementId()).thenReturn("id");
    complex1.addSpecies(mock);
    complex2.addSpecies(mock);
    comparator.compare(complex2, complex1);
  }

  @Test
  public void testDifferent5() throws Exception {
    Complex complex1 = createComplex();
    Complex complex2 = createComplex();
    complex1.setHomodimer(123);
    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);

    complex1 = createComplex();
    complex2 = createComplex();
    complex1.getElements().iterator().next().setNotes("bla");
    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);

    complex1 = createComplex();
    complex2 = createComplex();
    complex1.getElements().clear();
    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);

    complex1 = createComplex();
    complex2 = createComplex();
    assertTrue(comparator.compare(null, complex2) != 0);
    assertTrue(comparator.compare(complex2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    complex1 = createComplex();
    complex2 = createComplex();
    Complex child = (Complex) complex1.getElements().iterator().next();
    child.getElements().iterator().next().setNotes("grand child notes");
    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);

    complex1 = createComplex();
    complex2 = createComplex();
    Protein prot = new GenericProtein();
    prot.setElementId("test");
    complex1.addSpecies(prot);
    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);

    complex1 = createComplex();
    complex2 = createComplex();
    complex1.setStructuralState(new StructuralState());
    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);

    assertTrue(comparator.compare(new Complex(), Mockito.mock(Complex.class)) != 0);
  }

  @Test
  public void testDifferent3() throws Exception {
    Complex complex1 = createComplex();
    Complex complex2 = createComplex();
    GenericProtein prot = new GenericProtein();
    prot.setElementId("test");
    complex1.addSpecies(prot);
    prot = new GenericProtein();
    prot.setElementId("test2");
    complex2.addSpecies(prot);
    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);
  }

  @Test
  public void testDifferent4() throws Exception {
    Complex complex1 = createComplex();
    Complex complex2 = createComplex();
    GenericProtein c = new GenericProtein("test");
    c.setName("a");
    complex2.addSpecies(c);
    complex2.addSpecies(new GenericProtein("test"));
    c.setName("");

    complex1.addSpecies(new GenericProtein("test"));
    GenericProtein d = new GenericProtein("test2");
    d.setName("a");
    complex1.addSpecies(d);
    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);
  }

  @Test
  public void testDifferent2() throws Exception {
    Complex complex1 = createComplex();
    Complex complex2 = createComplex();
    GenericProtein comp = new GenericProtein("test");
    comp.setFullName("X");
    complex1.addSpecies(comp);
    complex2.addSpecies(new GenericProtein("test"));
    assertTrue(comparator.compare(complex1, complex2) != 0);
    assertTrue(comparator.compare(complex2, complex1) != 0);
  }

  public Complex createComplex() {
    Complex result = new Complex();

    result.setHypothetical(true);
    result.setHomodimer(3);

    Complex child = new Complex();
    result.addSpecies(child);
    child.setCharge(12);
    child.setName("buu");
    child.setNotes("hey, hi, hello");

    Complex grandChild = new Complex();
    child.addSpecies(grandChild);
    child.setCharge(123);
    child.setName("buus");
    child.setNotes("hey, hi, hello !!");

    return result;
  }

}
