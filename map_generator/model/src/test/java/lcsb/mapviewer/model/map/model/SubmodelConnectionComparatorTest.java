package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class SubmodelConnectionComparatorTest extends ModelTestFunctions {

  SubmodelConnectionComparator comparator = new SubmodelConnectionComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() throws Exception {
    SubmodelConnection connectionA = createConnection();
    SubmodelConnection connectionB = createConnection();

    assertEquals(0, comparator.compare(new ElementSubmodelConnection(), new ElementSubmodelConnection()));
    assertEquals(0, comparator.compare(new ModelSubmodelConnection(), new ModelSubmodelConnection()));
    assertEquals(0, comparator.compare(connectionA, connectionB));
    assertEquals(0, comparator.compare(connectionA, connectionA));
    assertEquals(0, comparator.compare(null, null));
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() throws Exception {
    class Tmp extends SubmodelConnection {
      private static final long serialVersionUID = 1L;

      @Override
      public SubmodelConnection copy() {
        return null;
      }
    }
    ;
    Tmp connectionA = new Tmp();
    Tmp connectionB = new Tmp();

    comparator.compare(connectionA, connectionB);
    comparator.compare(connectionA, connectionA);
  }

  @Test
  public void testInternal() throws Exception {
    SubmodelConnection connectionA = createConnection();
    SubmodelConnection connectionB = createConnection();

    assertTrue(comparator.internalCompare(null, connectionB) != 0);
    assertTrue(comparator.internalCompare(connectionA, null) != 0);
    assertEquals(0, comparator.internalCompare(null, null));
  }

  @Test
  public void testDifferent() throws Exception {
    SubmodelConnection connectionA = createConnection();
    SubmodelConnection connectionB = createConnection();

    assertTrue(comparator.compare(null, connectionB) != 0);
    assertTrue(comparator.compare(connectionA, null) != 0);

    connectionA.setName("AA");
    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    connectionA = createConnection();
    connectionB = createConnection();

    connectionA.setType(null);

    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    connectionA = createConnection();
    connectionB = createConnection();

    connectionA.setType(SubmodelType.PATHWAY);

    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    connectionA = createConnection();
    connectionB = createConnection();

    connectionA.getSubmodel().setNotes("ASD acd");

    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    connectionA = createConnection();
    connectionB = createConnection();
    connectionB.setSubmodel(new ModelFullIndexed(null));
    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    assertTrue(comparator.compare(connectionB, Mockito.spy(SubmodelConnection.class)) != 0);
  }

  @Test
  public void testDifferent2() throws Exception {
    ElementSubmodelConnection connectionA = new ElementSubmodelConnection();
    ElementSubmodelConnection connectionB = new ElementSubmodelConnection();

    connectionB.setSubmodel(new ModelFullIndexed(null));
    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);
  }

  private Model getModel() {
    Model model = new ModelFullIndexed(null);

    model.setNotes("Some description");

    Species protein = new GenericProtein("a_id");
    protein.setName("ad");
    protein.setElementId("a_id");
    model.addElement(protein);

    model.addElement(new Compartment("default"));

    Layer layer = new Layer();
    layer.setName("layer name");
    model.addLayer(layer);

    model.addReaction(new Reaction());
    return model;
  }

  private ElementSubmodelConnection createConnection() {
    ElementSubmodelConnection result = new ElementSubmodelConnection(getModel(), SubmodelType.DOWNSTREAM_TARGETS);
    result.setName("name A");
    return result;
  }

}
