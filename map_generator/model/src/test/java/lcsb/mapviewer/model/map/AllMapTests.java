package lcsb.mapviewer.model.map;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.model.map.compartment.AllCompartmentTests;
import lcsb.mapviewer.model.map.kinetics.AllKineticsTests;
import lcsb.mapviewer.model.map.layout.AllLayoutTests;
import lcsb.mapviewer.model.map.layout.graphics.AllGraphicsTests;
import lcsb.mapviewer.model.map.model.AllModelTests;
import lcsb.mapviewer.model.map.modifier.AllModifierTests;
import lcsb.mapviewer.model.map.reaction.AllReactionTests;
import lcsb.mapviewer.model.map.species.AllSpeciesTests;
import lcsb.mapviewer.model.map.statistics.AllStatisticsTests;

@RunWith(Suite.class)
@SuiteClasses({ AllCompartmentTests.class,
    AllGraphicsTests.class,
    AllKineticsTests.class,
    AllLayoutTests.class,
    AllModelTests.class,
    AllModifierTests.class,
    AllReactionTests.class,
    AllSpeciesTests.class,
    AllStatisticsTests.class,
    CommentTest.class,
    InconsistentModelExceptionTest.class,
    MiriamDataTest.class,
    MiriamRelationTypeTest.class,
    MiriamTypeTest.class,
    MiriamTypeNameComparatorTest.class,
    OverviewImageComparatorTest.class,
    OverviewImageLinkComparatorTest.class,
    OverviewImageLinkTest.class,
    OverviewImageTest.class,
    OverviewLinkComparatorTest.class,
    OverviewLinkTest.class,
    OverviewModelLinkComparatorTest.class,
    OverviewModelLinkTest.class,
    OverviewSearchLinkTest.class,
    SearchIndexTest.class,
})

public class AllMapTests {

}
