package lcsb.mapviewer.model.map.species.field;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CodingRegionTest.class,
    ModificationStateTest.class,
    ResidueTest.class,
    PositionToCompartmentTest.class,
    ProteinBindingDomainTest.class,
    StructureTest.class,
    UniprotRecordTest.class,
})
public class AllFieldTests {

}
