package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.*;
import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class CompartmentComparatorTest extends ModelTestFunctions {

  Logger logger = LogManager.getLogger(CompartmentComparatorTest.class);

  CompartmentComparator comparator = new CompartmentComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Compartment compartment1 = createCompartment();
    Compartment compartment2 = createCompartment();

    assertEquals(0, comparator.compare(compartment1, compartment2));

    assertEquals(0, comparator.compare(null, null));
  }

  @Test
  public void testDifferent2() {
    Compartment compartment = createCompartment();

    assertTrue(comparator.compare(compartment, new PathwayCompartment()) != 0);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalid() {
    Compartment compartment1 = createCompartment();
    Compartment compartment2 = createCompartment();

    GenericProtein protein = new GenericProtein("1");
    compartment1.getElements().add(protein);
    protein = new GenericProtein("1");
    compartment1.getElements().add(protein);

    compartment2.getElements().add(new GenericProtein("b"));
    compartment2.getElements().add(new GenericProtein("a"));

    comparator.compare(compartment1, compartment2);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalid2() {
    Compartment compartment1 = createCompartment();
    Compartment compartment2 = createCompartment();

    Species protein = new GenericProtein("1");
    compartment1.getElements().add(protein);
    protein = new GenericProtein("1");
    compartment1.getElements().add(protein);

    compartment2.getElements().add(new GenericProtein("A"));
    compartment2.getElements().add(new GenericProtein("B"));

    comparator.compare(compartment2, compartment1);
  }

  private Compartment createCompartment() {
    Compartment result = new Compartment();

    result.setElementId("asd");
    result.setX(12.0);
    result.setY(123.0);
    result.setWidth(4);
    result.setHeight(5);
    result.setFontSize(9.0);
    result.setFillColor(Color.BLUE);
    result.setVisibilityLevel("14");
    result.setThickness(998);
    result.setOuterWidth(45);
    result.setInnerWidth(65);
    result.setNamePoint(new Point2D.Double(9, 2));

    GenericProtein protein = new GenericProtein("S");
    protein.setName("a");
    result.addElement(protein);

    return result;
  }

  @Test
  public void testDifferentElementList() {
    Compartment compartment1 = createCompartment();
    Compartment compartment2 = createCompartment();

    compartment1.addElement(new GenericProtein("idd"));

    assertTrue(comparator.compare(compartment1, compartment2) != 0);
    assertTrue(comparator.compare(compartment2, compartment1) != 0);
  }

  @Test
  public void testDifferentElementId() {
    Compartment compartment1 = createCompartment();
    Compartment compartment2 = createCompartment();

    compartment1.setElementId("tmp");

    assertTrue(comparator.compare(compartment1, compartment2) != 0);
    assertTrue(comparator.compare(compartment2, compartment1) != 0);
  }

  @Test
  public void testDifferent() {
    Compartment compartment1 = createCompartment();
    Compartment compartment2 = createCompartment();

    assertTrue(comparator.compare(compartment1, null) != 0);
    assertTrue(comparator.compare(null, compartment1) != 0);

    compartment1 = createCompartment();
    compartment2 = createCompartment();

    compartment1.setThickness(11111);

    assertTrue(comparator.compare(compartment1, compartment2) != 0);
    assertTrue(comparator.compare(compartment2, compartment1) != 0);

    compartment1 = createCompartment();
    compartment2 = createCompartment();

    compartment1.setNamePoint(new Point2D.Double(9, 999));

    assertTrue(comparator.compare(compartment1, compartment2) != 0);
    assertTrue(comparator.compare(compartment2, compartment1) != 0);

    compartment1 = createCompartment();
    compartment2 = createCompartment();

    compartment1.getElements().iterator().next().setElementId("bnu");

    assertTrue(comparator.compare(compartment1, compartment2) != 0);
    assertTrue(comparator.compare(compartment2, compartment1) != 0);

    compartment1 = createCompartment();
    compartment2 = createCompartment();

    Species species = (Species) compartment1.getElements().iterator().next();
    species.setName("new namne");

    assertTrue(comparator.compare(compartment1, compartment2) != 0);
    assertTrue(comparator.compare(compartment2, compartment1) != 0);

    compartment1 = createCompartment();
    compartment2 = createCompartment();

    compartment1.setOuterWidth(2);

    assertTrue(comparator.compare(compartment1, compartment2) != 0);
    assertTrue(comparator.compare(compartment2, compartment1) != 0);

    compartment1 = createCompartment();
    compartment2 = createCompartment();

    compartment1.setInnerWidth(2);

    assertTrue(comparator.compare(compartment1, compartment2) != 0);
    assertTrue(comparator.compare(compartment2, compartment1) != 0);

    compartment1 = createCompartment();
    compartment2 = createCompartment();
  }

}
