package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class GenericProteinComparatorTest extends ModelTestFunctions {

  GenericProteinComparator comparator = new GenericProteinComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    GenericProtein drug1 = createGenericProtein();
    GenericProtein drug2 = createGenericProtein();

    assertEquals(0, comparator.compare(drug1, drug1));

    assertEquals(0, comparator.compare(drug1, drug2));
    assertEquals(0, comparator.compare(drug2, drug1));
  }

  @Test
  public void testDifferent() {
    GenericProtein drug2 = createGenericProtein();
    assertTrue(comparator.compare(null, drug2) != 0);
    assertTrue(comparator.compare(drug2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    GenericProtein drug = createGenericProtein();
    drug.setName("n");
    assertTrue(comparator.compare(drug, drug2) != 0);

    assertTrue(comparator.compare(drug, Mockito.mock(GenericProtein.class)) != 0);
  }

  public GenericProtein createGenericProtein() {
    GenericProtein result = new GenericProtein();
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    GenericProtein object = Mockito.mock(GenericProtein.class);

    comparator.compare(object, object);
  }

}
