package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SbmlUnitTypeFactorComparatorTest {

  SbmlUnitTypeFactorComparator comparator = new SbmlUnitTypeFactorComparator();

  private SbmlUnitTypeFactor createUnitTypeFactor() {
    SbmlUnitTypeFactor result = new SbmlUnitTypeFactor(SbmlUnitType.AMPERE, 1, 1, 1);
    return result;
  }

  @Test
  public void testCompareEqual() {
    SbmlUnitTypeFactor parameter = createUnitTypeFactor();
    SbmlUnitTypeFactor parameter2 = createUnitTypeFactor();
    assertEquals(0, comparator.compare(parameter, parameter2));
  }

  @Test
  public void testCompareDifferentUnitType() {
    SbmlUnitTypeFactor parameter = createUnitTypeFactor();
    SbmlUnitTypeFactor parameter2 = createUnitTypeFactor();
    parameter.setUnitType(SbmlUnitType.BECQUEREL);
    assertTrue(0 != comparator.compare(parameter, parameter2));
    assertTrue(0 != comparator.compare(parameter2, parameter));
  }

  @Test
  public void testCompareDifferentExponent() {
    SbmlUnitTypeFactor parameter = createUnitTypeFactor();
    SbmlUnitTypeFactor parameter2 = createUnitTypeFactor();
    parameter.setExponent(2);
    assertTrue(0 != comparator.compare(parameter, parameter2));
    assertTrue(0 != comparator.compare(parameter2, parameter));
  }

  @Test
  public void testCompareDifferentMultiplier() {
    SbmlUnitTypeFactor parameter = createUnitTypeFactor();
    SbmlUnitTypeFactor parameter2 = createUnitTypeFactor();
    parameter.setMultiplier(2);
    assertTrue(0 != comparator.compare(parameter, parameter2));
    assertTrue(0 != comparator.compare(parameter2, parameter));
  }

  @Test
  public void testCompareDifferentScale() {
    SbmlUnitTypeFactor parameter = createUnitTypeFactor();
    SbmlUnitTypeFactor parameter2 = createUnitTypeFactor();
    parameter.setScale(3);
    assertTrue(0 != comparator.compare(parameter, parameter2));
    assertTrue(0 != comparator.compare(parameter2, parameter));
  }

}
