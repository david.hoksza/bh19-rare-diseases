package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;

public class ConfigurationTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new ConfigurationOption());
  }

  @Test
  public void testGetters() {
    ConfigurationOption conf = new ConfigurationOption();
    int id = 81;
    ConfigurationElementType type = ConfigurationElementType.DEFAULT_MAP;
    String value = "a.val";
    conf.setId(id);
    assertEquals(id, conf.getId());
    conf.setType(type);
    assertEquals(type, conf.getType());
    conf.setValue(value);
    assertEquals(value, conf.getValue());
  }
}
