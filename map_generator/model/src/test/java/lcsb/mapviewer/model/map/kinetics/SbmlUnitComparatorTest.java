package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SbmlUnitComparatorTest {
  SbmlUnitComparator comparator = new SbmlUnitComparator();

  private SbmlUnit createUnit() {
    SbmlUnit result = new SbmlUnit("unit_id");
    return result;
  }

  @Test
  public void testCompareEqual() {
    SbmlUnit parameter = createUnit();
    SbmlUnit parameter2 = createUnit();
    assertEquals(0, comparator.compare(parameter, parameter2));
  }

  @Test
  public void testCompareUnitId() {
    SbmlUnit parameter = createUnit();
    SbmlUnit parameter2 = createUnit();
    parameter.setUnitId("xxx");
    assertTrue(0 != comparator.compare(parameter, parameter2));
  }

  @Test
  public void testCompareName() {
    SbmlUnit parameter = createUnit();
    SbmlUnit parameter2 = createUnit();
    parameter.setName("xxx");
    assertTrue(0 != comparator.compare(parameter, parameter2));
  }

  @Test
  public void testCompareUnitTypes() {
    SbmlUnit parameter = createUnit();
    SbmlUnit parameter2 = createUnit();
    parameter.addUnitTypeFactor(new SbmlUnitTypeFactor(SbmlUnitType.AMPERE, 1, 1, 1));
    ;
    assertTrue(0 != comparator.compare(parameter, parameter2));
  }

}
