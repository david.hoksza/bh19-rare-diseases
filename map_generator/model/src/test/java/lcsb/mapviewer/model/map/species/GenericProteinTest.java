package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class GenericProteinTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new GenericProtein());
  }

  @Test
  public void testConstructor() {
    GenericProtein species = new GenericProtein("");
    assertNotNull(species);
  }

  @Test
  public void testCopy1() {
    GenericProtein species = new GenericProtein(new GenericProtein()).copy();
    assertNotNull(species);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    GenericProtein protein = Mockito.spy(GenericProtein.class);
    protein.copy();
  }
}
