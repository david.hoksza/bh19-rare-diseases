package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.*;

public class UnknownTransitionReactionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new UnknownTransitionReaction());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg() {
    Reaction reaction = new Reaction();
    new UnknownTransitionReaction(reaction);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg2() {
    Reaction reaction = new Reaction();
    reaction.addProduct(new Product());
    new UnknownTransitionReaction(reaction);
  }

  @Test
  public void testConstructor() {
    Reaction reaction = new Reaction();
    reaction.addProduct(new Product());
    reaction.addReactant(new Reactant());
    UnknownTransitionReaction validReaction = new UnknownTransitionReaction(reaction);
    assertNotNull(validReaction);
  }

  @Test
  public void testCopy() {
    UnknownTransitionReaction original = new UnknownTransitionReaction();
    original.addProduct(new Product());
    original.addReactant(new Reactant());
    original.addReactant(new Reactant());
    UnknownTransitionReaction product = original.copy();
    assertNotNull(product);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(new UnknownTransitionReaction()).copy();
  }

  @Test
  public void testGetters() {
    UnknownTransitionReaction original = new UnknownTransitionReaction();
    assertNotNull(original.getReactionRect());
    assertNotNull(original.getStringType());
  }
}
