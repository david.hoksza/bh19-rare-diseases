package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;

public class GeneComparatorTest extends ModelTestFunctions {

  GeneComparator comparator = new GeneComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Gene gene1 = createGene();
    Gene gene2 = createGene();

    assertEquals(0, comparator.compare(gene1, gene1));

    assertEquals(0, comparator.compare(gene1, gene2));
    assertEquals(0, comparator.compare(gene2, gene1));
  }

  @Test
  public void testDifferent() {
    Gene gene1 = createGene();
    Gene gene2 = createGene();
    gene1.getModificationResidues().get(0).setName("bla");
    assertTrue(comparator.compare(gene1, gene2) != 0);
    assertTrue(comparator.compare(gene2, gene1) != 0);

    gene1 = createGene();
    gene2 = createGene();
    gene1.getModificationResidues().clear();
    assertTrue(comparator.compare(gene1, gene2) != 0);
    assertTrue(comparator.compare(gene2, gene1) != 0);

    gene1 = createGene();
    gene2 = createGene();
    assertTrue(comparator.compare(null, gene2) != 0);
    assertTrue(comparator.compare(gene2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    Gene gene = createGene();
    gene.setName("n");
    assertTrue(comparator.compare(gene, gene2) != 0);

    assertTrue(comparator.compare(gene, Mockito.mock(Gene.class)) != 0);
  }

  public Gene createGene() {
    Gene result = new Gene();
    result.setHypothetical(true);

    ModificationSite modificationSite = new ModificationSite();
    result.addModificationSite(modificationSite);

    modificationSite.setIdModificationResidue("a");
    modificationSite.setName("name");
    modificationSite.setPosition(new Point2D.Double(10, 20));
    modificationSite.setState(ModificationState.ACETYLATED);
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    Gene object = Mockito.mock(Gene.class);

    comparator.compare(object, object);
  }

}
