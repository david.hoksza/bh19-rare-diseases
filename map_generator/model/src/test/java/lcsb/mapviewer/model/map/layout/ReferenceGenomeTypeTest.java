package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class ReferenceGenomeTypeTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (ReferenceGenomeType type : ReferenceGenomeType.values()) {
      assertNotNull(type);

      // for coverage tests
      ReferenceGenomeType.valueOf(type.toString());
      assertNotNull(type.getHomepage());
    }
  }

}
