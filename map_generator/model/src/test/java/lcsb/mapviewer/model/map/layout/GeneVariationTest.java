package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertEquals;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class GeneVariationTest extends ModelTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() throws Exception {
    String modifiedDna = "str";

    int position = 12;

    GeneVariation gv = new GeneVariation();

    gv.setModifiedDna(modifiedDna);
    assertEquals(modifiedDna, gv.getModifiedDna());

    gv.setPosition(position);

    assertEquals((long) position, (long) gv.getPosition());
  }

  @Test
  public void testCopy() throws Exception {
    String modifiedDna = "str";

    GeneVariation gv = new GeneVariation();

    gv.setModifiedDna(modifiedDna);

    GeneVariation copy = gv.copy();

    assertEquals(gv.getModifiedDna(), copy.getModifiedDna());
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() throws Exception {
    Mockito.spy(GeneVariation.class).copy();
  }

}
