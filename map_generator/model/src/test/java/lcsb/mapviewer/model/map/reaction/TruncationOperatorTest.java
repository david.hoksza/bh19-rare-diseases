package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class TruncationOperatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new TruncationOperator());
  }

  @Test
  public void testConstructor() {
    TruncationOperator op = new TruncationOperator();
    op.setLine(new PolylineData());
    TruncationOperator operator = new TruncationOperator(op);
    assertNotNull(operator);
  }

  @Test
  public void testGetters() {
    assertNotNull(new TruncationOperator().getSBGNOperatorText());
    assertNotNull(new TruncationOperator().getOperatorText());
  }

  @Test
  public void testCopy1() {
    TruncationOperator op = new TruncationOperator();
    op.setLine(new PolylineData());
    TruncationOperator operator = op.copy();
    assertNotNull(operator);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    Mockito.mock(TruncationOperator.class, Mockito.CALLS_REAL_METHODS).copy();
  }

}
