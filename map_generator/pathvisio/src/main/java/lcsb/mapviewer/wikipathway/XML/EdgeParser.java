package lcsb.mapviewer.wikipathway.XML;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.wikipathway.model.*;

/**
 * Parser class that creates {@link Edge} objects from Xml {@link Element node}.
 * 
 * @author Piotr Gawron
 *
 */
public class EdgeParser extends ElementGpmlParser<Edge> {

  /**
   * First id value used for generating identifiers during conversion.
   */
  private static final int INITIAL_ID_VALUE = 10000;
  /**
   * This is default length of lines when lines are drawn in
   * {@link GpmlLineConnectorType#ELBOW} mode, but without specifying coordinates.
   */
  private static final double DEFAULT_DISTNACE = 20;
  /**
   * Counter used for generating identifiers if identifiers aren't provided by the
   * GPML model.
   */
  private static int idCounter = INITIAL_ID_VALUE;
  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger(EdgeParser.class);
  /**
   * Parser used for extracting {@link lcsb.mapviewer.model.map.MiriamData
   * references} from GPML model.
   */
  private ReferenceParser referenceParser;
  /**
   * Parser used for extracting {@link PointData points} from GPML xml nodes.
   */
  private PointDataParser pointParser;

  /**
   * This function get new id for interactions that don't have id.
   * 
   * @return String - new id
   */
  private String getNewId() {
    idCounter++;
    return "id" + idCounter;
  }

  public EdgeParser(String mapName) {
    super(mapName);
    pointParser = new PointDataParser(mapName);
    referenceParser = new ReferenceParser(mapName);
  }

  /**
   * Creates {@link Edge} from xml node.
   * 
   * @return {@link Edge} from xml node
   * @throws ConverterException
   *           thrown when there is a problem with parsing edge
   */
  @Override
  public Edge parse(Element eElement) throws ConverterException {

    String graphId = eElement.getAttribute("GraphId");
    if (graphId == null || graphId.equals("")) {
      graphId = getNewId();
    }
    Edge edge = new Edge(graphId, getMapName());

    for (Pair<String, String> entry : getAttributes(eElement)) {
      switch (entry.getLeft()) {
      case ("GraphId"):
        break;
      case ("GroupRef"):
        edge.setGroupRef(entry.getRight());
        break;
      default:
        logger.warn("Unknown attribute of " + eElement.getNodeName() + " node: " + entry.getLeft());
        break;
      }
    }

    NodeList nodes = eElement.getChildNodes();
    boolean graphicsParsed = false;
    for (int i = 0; i < nodes.getLength(); i++) {
      Node node = nodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;
        switch (node.getNodeName()) {
        // references (annotations)
        case ("Xref"):
          MiriamData md = referenceParser.parse(element);
          if (md != null) {
            edge.addReference(md);
          }
          break;
        case ("Comment"):
          edge.addComment(node.getTextContent());
          break;
        // references (annotations)
        case ("BiopaxRef"):
          edge.getBiopaxReferences().add(node.getTextContent());
          break;
        case ("Graphics"):
          graphicsParsed = true;
          parseGraphics(edge, element);
          break;
        case ("Attribute"):
          parseAttribute(element, edge);
          break;
        default:
          logger.warn(edge.getLogMarker(), "Unknown edge node: " + node.getNodeName() + ".");
          break;
        }
      }
    }
    if (!graphicsParsed) {
      throw new ConverterException(edge.getLogMarker() + "No Graphics information found for edge.");
    }
    return edge;
  }

  @Override
  public String toXml(Edge node) throws ConverterException {
    throw new NotImplementedException();
  }

  @Override
  public String toXml(Collection<Edge> list) throws ConverterException {
    throw new NotImplementedException();
  }

  /**
   * Parses graphics xml node that is part of edge xml node.
   *
   * @param edge
   *          edge which is parsed
   * @param graphicsNode
   *          xml node
   * @throws ConverterException
   *           thrown when data for the edge is invalid
   */
  protected void parseGraphics(Edge edge, Element graphicsNode) throws ConverterException {
    List<Element> points = new ArrayList<>();
    List<Element> anchors = new ArrayList<>();

    GpmlLineConnectorType connectorType = GpmlLineConnectorType.STRAIGHT;

    for (Pair<String, String> entry : getAttributes(graphicsNode)) {
      switch (entry.getLeft()) {
      case ("ConnectorType"):
        connectorType = GpmlLineConnectorType.getByGpmlName(entry.getRight());
        break;
      case ("LineThickness"):
        edge.getLine().setWidth(entry.getRight());
        break;
      case ("LineStyle"):
        // line type
        GpmlLineType lineType = GpmlLineType.getByGpmlName(entry.getRight());
        edge.getLine().setType(lineType.getCorrespondingGlobalLineType());
        break;
      case ("ZOrder"):
        edge.setzOrder(Integer.valueOf(entry.getRight()));
        break;
      case ("Color"):
        edge.setColor(hexStringToColor(entry.getRight()));
        break;
      default:
        logger.warn("Unknown attribute of " + graphicsNode.getNodeName() + " node: " + entry.getLeft());
        break;
      }
    }

    NodeList nodes = graphicsNode.getChildNodes();
    for (int i = 0; i < nodes.getLength(); i++) {
      Node node = nodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if ("Point".equalsIgnoreCase(node.getNodeName())) {
          points.add((Element) node);
        } else if ("Anchor".equalsIgnoreCase(node.getNodeName())) {
          anchors.add((Element) node);
        } else {
          logger.warn(edge.getLogMarker(), "Unknown graphics edge node: " + node.getNodeName() + ".");
        }
      }
    }

    // lines

    List<PointData> pointDataList = new ArrayList<>();

    for (int j = 0; j < points.size(); j++) {
      Node point = points.get(j);
      PointData pointData = pointParser.parse((Element) point);

      pointDataList.add(pointData);
      if (j == 0 && pointData.hasGraphRef()) {
        edge.setStart(pointData.getGraphRef());
      }
      if (j == points.size() - 1 && pointData.hasGraphRef()) {
        edge.setEnd(pointData.getGraphRef());
      }

      edge.setType(pointData.getType());
    }
    edge.setReversibleReaction(pointDataList.get(0).getType() == GpmlInteractionType.ARROW);

    edge.getLine().setPoints(getPoints(edge.getLogMarker(), pointDataList, connectorType));
    if (edge.getLine().getPoints().size() < 2) {
      throw new ConverterException(
          edge.getLogMarker() + "Contains too less points. At least two points are required in a line.");
    }
    double lineLength = 0;
    for (Line2D line : edge.getLine().getLines()) {
      lineLength += line.getP1().distance(line.getP2());
    }
    if (lineLength <= Configuration.EPSILON) {
      throw new ConverterException(edge.getLogMarker() + "Line cannot have 0 length.");
    }

    // anchors
    for (Element anchor : anchors) {
      String anchorId = anchor.getAttribute("GraphId");
      String position = anchor.getAttribute("Position");
      if (anchorId != null && !anchorId.isEmpty()) {
        edge.addAnchor(anchorId);
        // if our edge is not connected from at first point but we have anchor
        // there, then set this anchor there (so we could merge it later on)
        if (edge.getStart() == null && "0.0".equals(position)) {
          edge.setStart(anchorId);
        }
        // if our edge is not connected from at last point but we have anchor
        // there, then set this anchor there (so we could merge it later on)
        if (edge.getEnd() == null && "1.0".equals(position)) {
          edge.setEnd(anchorId);
        }
      }
    }
  }

  /**
   * This method transforms list of {@link PointData points} from GPML xml into
   * list of standard {@link Point2D points}.
   *
   * @param warningPrefix
   *          prefix that should be used for warnings
   * @param pointDataList
   *          list of points from GPML format
   * @param connectorType
   *          {@link GpmlLineConnectorType type} defining how points are connected
   *          in GPML format
   * @return list of standard {@link Point2D points} obtained from input data
   * @throws UnknownGpmlLineConnectorTypeException
   *           thrown when {@link GpmlLineConnectorType type} is unknown
   * @throws InvalidElbowConnectorException
   *           thrown when input data define invalid line
   * @throws UnknownTypeException
   *           throw when connector type is invalid
   */
  List<Point2D> getPoints(LogMarker warningPrefix, List<PointData> pointDataList,
      GpmlLineConnectorType connectorType)
      throws InvalidElbowConnectorException, UnknownTypeException {
    List<Point2D> result = new ArrayList<>();
    switch (connectorType) {
    // line is a list of perpendicular segments
    case ELBOW:
      Direction from = pointDataList.get(0).getDirection();
      Direction to = pointDataList.get(pointDataList.size() - 1).getDirection();

      if (from == null) {
        from = Direction.WEST;
      }
      if (to == null) {
        to = Direction.EAST;
      }
      if (from == Direction.NONE) {
        from = to.nextClockwiseDirection().nextClockwiseDirection();
        for (int i = 0; i < pointDataList.size() - 1; i++) {
          from = from.nextClockwiseDirection();
        }
      }

      // how many segments do we have
      int lines = computeNumberOfPerpendicularSegments(warningPrefix, pointDataList.get(0),
          pointDataList.get(pointDataList.size() - 1));
      // now we know how many segments should be in the line

      // if segments are defined in the input the it's easy
      if (lines == pointDataList.size()) {
        List<Point2D> points = new ArrayList<>();
        for (PointData pd : pointDataList) {
          points.add(pd.toPoint());
        }
        result = preparePerpendicularLines(from, points);

        // if we know how many segments should be in the line, but we have
        // only first and last point
        // then we need to add missing points (some default behaviour).
      } else if (pointDataList.size() == 2) {
        List<Point2D> points = new ArrayList<>();
        points.add(pointDataList.get(0).toPoint());
        points.add(pointDataList.get(1).toPoint());
        // add middle point
        points.add(1, computeDefaultPoint(from, points.get(0)));
        // CHECKSTYLE:OFF
        // if something is still missing then add default beginning line
        if (lines > 3) {
          points.add(2, computeDefaultPoint(to, points.get(2)));
        }
        // if something is still missing then add default end line
        if (lines > 4) {
          double x = (points.get(1).getX() + points.get(2).getX()) / 2;
          double y = (points.get(1).getY() + points.get(2).getY()) / 2;
          points.add(2, new Point2D.Double(x, y));
        }
        // if something is still missing then there is something terrible
        // wrong
        if (lines > 5) {
          throw new InvalidElbowConnectorException(
              warningPrefix + " Line with connector type " + connectorType + " contains too many segments: " + lines);
        }
        // CHECKSTYLE:ON

        // and now transform the points into perpendicular segments
        result = preparePerpendicularLines(from, points);

        // if number of expected segments is lower than number of defined
        // segments then we probably missed something, let's assume that in
        // GPML file the data is correct and parse it
      } else if (pointDataList.size() > lines) {
        List<Point2D> points = new ArrayList<>();
        for (PointData pd : pointDataList) {
          points.add(pd.toPoint());
        }
        result = preparePerpendicularLines(from, points);

        // if number of expected segments is different than number of defined
        // segments then something is wrong
      } else {
        throw new InvalidElbowConnectorException(
            warningPrefix + "Don't know how to prepare lines. " + connectorType + " contains invalid number of points: "
                + pointDataList.size()
                + ". Expected: " + lines);
      }

      break;

    // if line is straight then pass input points into the output
    case STRAIGHT:
      for (PointData pointData : pointDataList) {
        result.add(pointData.toPoint());
      }
      break;
    // if line is segmented then pass input points into the output
    case SEGMENTED:
      for (PointData pointData : pointDataList) {
        result.add(pointData.toPoint());
      }
      break;
    // if line is curved then inform user that we don't support it and create
    // a default one
    case CURVED:
      logger.warn(warningPrefix, "Curved connections are not supported.");
      for (PointData pointData : pointDataList) {
        result.add(pointData.toPoint());
      }
      break;
    default:
      throw new UnknownTypeException(warningPrefix + "Connector type is not supported: " + connectorType);
    }
    return result;
  }

  /**
   * Method that computes number of perpendicular lines that should connect start
   * point with the end point.
   *
   * @param warningPrefix
   *          string used as a prefix for warnings/errors
   * @param start
   *          starting point
   * @param end
   *          end point
   * @return number of perpendicular lines that should connect start point with
   *         the end point
   * @throws InvalidElbowConnectorException
   *           thrown when output cannot be computed because input data is invalid
   */
  private int computeNumberOfPerpendicularSegments(LogMarker warningPrefix, PointData start, PointData end)
      throws InvalidElbowConnectorException {
    Point2D fromPoint = start.toPoint();
    Point2D toPoint = end.toPoint();

    Direction from = start.getDirection();
    if (from == null) {
      from = Direction.WEST;
    }
    Direction to = end.getDirection();
    if (to == null) {
      to = Direction.EAST;
    }

    if (from == Direction.NONE || to == Direction.NONE) {
      return 2;
    }

    // result (2 is the minimum)
    int lines = 2;

    // if line starts and ends in the same axis then number of segments
    // should be at least3
    if (from.getAxis().equals(to.getAxis())) {
      lines++;
    }

    // if they are not starting and ending in the same direction (for
    // instance they both start from NORTH)
    // then check if some addition lines are needed
    if (!from.equals(to)) {
      int addition = 0;
      // check if beginning requires additional lines because the end is
      // overlapping the beginning
      switch (from) {
      case EAST:
        if (fromPoint.getX() >= toPoint.getX()) {
          addition = 2;
        }
        break;
      case WEST:
        if (fromPoint.getX() <= toPoint.getX()) {
          addition = 2;
        }
        break;
      case NORTH:
        if (fromPoint.getY() <= toPoint.getY()) {
          addition = 2;
        }
        break;
      case SOUTH:
        if (fromPoint.getY() >= toPoint.getY()) {
          addition = 2;
        }
        break;
      default:
        throw new InvalidElbowConnectorException(warningPrefix + "Unknown direction: " + from);
      }
      // check if end requires additional lines because the beginning is
      // overlapping end part
      switch (to) {
      case EAST:
        if (fromPoint.getX() <= toPoint.getX()) {
          addition = 2;
        }
        break;
      case WEST:
        if (fromPoint.getX() >= toPoint.getX()) {
          addition = 2;
        }
        break;
      case NORTH:
        if (fromPoint.getY() >= toPoint.getY()) {
          addition = 2;
        }
        break;
      case SOUTH:
        if (fromPoint.getY() <= toPoint.getY()) {
          addition = 2;
        }
        break;
      default:
        throw new InvalidElbowConnectorException(warningPrefix + "Unknown direction: " + from);
      }
      lines += addition;
    }
    return lines;
  }

  /**
   * Creates point that defines default perpendicular line from input point.
   *
   * @param from
   *          direction from which point is connected
   * @param original
   *          point from where we want to have a line
   * @return point that defines default perpendicular line from input point
   */
  private Point2D computeDefaultPoint(Direction from, Point2D original) {
    switch (from) {
    case NORTH:
      return new Point2D.Double(original.getX(), original.getY() - DEFAULT_DISTNACE);
    case SOUTH:
      return new Point2D.Double(original.getX(), original.getY() + DEFAULT_DISTNACE);
    case EAST:
      return new Point2D.Double(original.getX() + DEFAULT_DISTNACE, original.getY());
    case WEST:
      return new Point2D.Double(original.getX() - DEFAULT_DISTNACE, original.getY());
    default:
      throw new InvalidStateException("Unknown direction: " + from);
    }
  }

  /**
   * Creates perpendicular lines going through all input points.
   *
   * @param from
   *          in which direction should go the first line
   * @param points
   *          set of points through which perpendicular line should go
   * @return perpendicular lines going through all input points
   */
  private List<Point2D> preparePerpendicularLines(Direction from, List<Point2D> points) {
    List<Point2D> result = new ArrayList<>();
    result.add(points.get(0));
    Point2D lastPoint = points.get(0);
    for (int i = 1; i < points.size(); i++) {
      Point2D point = points.get(i);
      switch (from.getAxis()) {
      case NORTH_SOUTH:
        result.add(new Point2D.Double(lastPoint.getX(), point.getY()));
        break;
      case EAST_WEST:
        result.add(new Point2D.Double(point.getX(), lastPoint.getY()));
        break;
      default:
        throw new InvalidStateException("Unknown direction axis: " + from.getAxis());
      }
      lastPoint = result.get(i);
      from = from.nextClockwiseDirection();
    }
    result.add(points.get(points.size() - 1));
    return result;
  }

  /**
   * Creates new {@link Edge} object with unique identifier.
   *
   * @return new {@link Edge} object with unique identifier
   */
  public Edge createEmpty() {
    return new Edge(getNewId(), getMapName());
  }

  /**
   * Method that parses {@link Edge} xml attribute.
   *
   * @param eTmp
   *          xml node with attribute
   * @param edge
   *          edge where data should be added
   */
  private void parseAttribute(Element eTmp, Edge edge) {
    String key = eTmp.getAttribute("Key");
    String value = eTmp.getAttribute("Value");
    switch (key) {
    case ("org.pathvisio.core.ds"):
      // skip for now (no idea what does it mean)
      break;
    case ("org.pathvisio.core.id"):
      // skip for now (no idea what does it mean)
      break;
    default:
      logger.warn(edge.getLogMarker(), "Unknown attribute of node. Key: " + key + "; value: " + value);
      break;
    }
  }

}
