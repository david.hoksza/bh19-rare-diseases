package lcsb.mapviewer.wikipathway.XML;

import java.awt.geom.Rectangle2D;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.wikipathway.model.*;

/**
 * Parser class that creates {@link DataNode} objects from Xml {@link Element
 * node}.
 * 
 * @author Piotr Gawron
 *
 */
public class DataNodeParser extends GraphicalPathwayElementParser<DataNode> {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger(DataNodeParser.class);

  /**
   * PArser used to process references.
   */
  private final ReferenceParser referenceParser;

  public DataNodeParser(String mapName) {
    super(mapName);
    referenceParser = new ReferenceParser(mapName);
  }

  @Override
  public DataNode parse(Element eElement) throws UnknownTypeException {
    if (!eElement.getNodeName().equals("DataNode")) {
      throw new InvalidArgumentException(ShapeParser.class.getSimpleName() + " can parse only DataNode xml nodes");
    }
    DataNode node = new DataNode(eElement.getAttribute("GraphId"), getMapName());

    for (Pair<String, String> entry : getAttributes(eElement)) {
      switch (entry.getLeft()) {
      case ("GraphId"):
        break;
      case ("TextLabel"):
        node.setTextLabel(entry.getRight());
        break;
      case ("GroupRef"):
        node.setGroupRef(entry.getRight());
        break;
      case ("Type"):
        node.setType(entry.getRight());
        break;
      default:
        logger.warn("Unknown attribute of " + eElement.getNodeName() + " node: " + entry.getLeft());
        break;
      }
    }

    NodeList tmpList = eElement.getChildNodes();
    for (int j = 0; j < tmpList.getLength(); j++) {
      Node tmpNode = tmpList.item(j);
      if (tmpNode.getNodeType() == Node.ELEMENT_NODE) {
        Element eTmp = (Element) tmpNode;
        switch (eTmp.getNodeName()) {
        case ("Comment"):
          node.addComment(eTmp.getTextContent());
          break;
        case ("BiopaxRef"):
          node.addBiopaxReference(eTmp.getTextContent());
          break;
        case ("Xref"):
          try {
            MiriamData data = referenceParser.parse(eTmp);
            if (data != null) {
              node.addReference(data);
            }
          } catch (NotImplementedException e) {
            if (e.getMessage().contains("Other")) {
              logger.fatal(getMapName() + "\t" + node.getGraphId() + "\tOther reference");
            } else {
              throw e;
            }
          } catch (InvalidArgumentException e) {
            logger.fatal(getMapName() + "\t" + node.getGraphId() + "\tInvalid reference");
          }
          break;
        case ("Graphics"):
          parseGraphics(eTmp, node);
          break;
        case ("Attribute"):
          parseAttribute(eTmp, node);
          break;
        default:
          logger.warn("Unknown sub-node of " + eElement.getNodeName() + " node: " + eTmp.getNodeName());
          break;
        }
      }
    }

    return node;
  }

  @Override
  public String toXml(DataNode node) throws ConverterException {
    throw new NotImplementedException();
  }

  @Override
  public String toXml(Collection<DataNode> list) throws ConverterException {
    throw new NotImplementedException();
  }

  /**
   * Parse graphics xml node in the shape node.
   *
   * @param eTmp
   *          xml node with graphics
   * @param shape
   *          shape where data should be added
   * @throws UnknownTypeException
   *           thrown when node contains unknown types
   */
  private void parseGraphics(Element eTmp, DataNode shape) throws UnknownTypeException {
    Double centerX = null;
    Double centerY = null;
    Double width = null;
    Double height = null;
    for (Pair<String, String> entry : getAttributes(eTmp)) {
      if (!parseCommonGraphicAttributes(shape, entry)) {
        switch (entry.getLeft()) {
        case ("CenterX"):
          centerX = Double.valueOf(entry.getRight());
          break;
        case ("CenterY"):
          centerY = Double.valueOf(entry.getRight());
          break;
        case ("Width"):
          width = Double.valueOf(entry.getRight());
          break;
        case ("Height"):
          height = Double.valueOf(entry.getRight());
          break;
        case ("ShapeType"):
          shape.setType(entry.getRight());
          break;
        case ("Color"):
          shape.setColor(hexStringToColor(entry.getRight()));
          break;
        case ("FillColor"):
          shape.setFillColor(hexStringToColor(entry.getRight()));
          break;
        case ("ZOrder"):
          shape.setzOrder(Integer.valueOf(entry.getRight()));
          break;
        case ("LineStyle"):
          shape.setLineType(GpmlLineType.getByGpmlName(entry.getRight()).getCorrespondingGlobalLineType());
          break;
        case ("FontSize"):
          shape.setFontSize(Double.valueOf(entry.getRight()));
          break;
        case ("FontWeight"):
          shape.setFontWeight(entry.getRight());
          break;
        case ("Valign"):
          shape.setvAlign(entry.getRight());
          break;
        default:
          logger.warn("Unknown attribute of " + eTmp.getNodeName() + " node: " + entry.getLeft());
          break;
        }
      }
    }
    shape.setRectangle(new Rectangle2D.Double(centerX - width / 2, centerY - height / 2, width, height));
  }

  /**
   * Method that parses {@link DataNode} xml attribute.
   *
   * @param eTmp
   *          xml node with attribute
   * @param shape
   *          shape where data should be added
   */
  private void parseAttribute(Element eTmp, DataNode shape) {
    String key = eTmp.getAttribute("Key");
    String value = eTmp.getAttribute("Value");
    switch (key) {
    case ("org.pathvisio.model.BackpageHead"):
      // it's deprecated in PathVisio so we can skip it
      break;
    case ("org.pathvisio.model.GenMAPP-Xref"):
      // skip it when it's empty
      if (!value.isEmpty()) {
        logger.warn(shape.getLogMarker() + "Unknown attribute of node. Key: " + key + "; value: " + value);
        break;
      }
      break;
    default:
      logger.warn(shape.getLogMarker() + "Unknown attribute of node. Key: " + key + "; value: " + value);
      break;
    }
  }

}
