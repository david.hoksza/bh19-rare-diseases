package lcsb.mapviewer.wikipathway.XML;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.wikipathway.model.Edge;

/**
 * Parser class that creates {@link Edge} objects from Xml {@link Element node}.
 * However the xml node is not typical edge node , but line node.
 * 
 * @author Piotr Gawron
 *
 */
public class EdgeLineParser extends ElementGpmlParser<Edge> {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Parser used to parse typical {@link Edge} xml nodes.
   */
  private final EdgeParser edgeParser;

  public EdgeLineParser(String mapName) {
    super(mapName);
    this.edgeParser = new EdgeParser(mapName);
  }

  /**
   * Creates {@link Edge} from xml line node.
   * 
   * @return {@link Edge} from xml node
   * @throws ConverterException
   */
  @Override
  public Edge parse(Element eElement) throws ConverterException {
    if (!eElement.getNodeName().equals("GraphicalLine")) {
      throw new InvalidArgumentException(
          ShapeParser.class.getSimpleName() + " can parse only GraphicalLine xml nodes. But " + eElement.getNodeName());
    }
    Edge line = edgeParser.createEmpty();
    NodeList nodes = eElement.getChildNodes();
    for (Pair<String, String> entry : getAttributes(eElement)) {
      switch (entry.getLeft()) {
      case ("GraphId"):
        line.setGraphId(entry.getRight());
        break;
      case ("GroupRef"):
        line.setGroupRef(entry.getRight());
        break;
      default:
        logger.warn("Unknown attribute of " + eElement.getNodeName() + " node: " + entry.getLeft());
      }
    }

    for (int i = 0; i < nodes.getLength(); i++) {
      Node node = nodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element eTmp = (Element) node;
        switch (node.getNodeName()) {
        case ("Graphics"):
          edgeParser.parseGraphics(line, (Element) node);
          break;
        case ("BiopaxRef"):
          line.addBiopaxReference(eTmp.getTextContent());
          break;
        case ("Comment"):
          line.addComment(eTmp.getTextContent());
          break;
        default:
          logger.warn("Unknown node in line: " + node.getNodeName());
        }
      }
    }
    return line;
  }

  @Override
  public String toXml(Edge node) throws ConverterException {
    throw new NotImplementedException();
  }

  @Override
  public String toXml(Collection<Edge> list) throws ConverterException {
    throw new NotImplementedException();
  }
}
