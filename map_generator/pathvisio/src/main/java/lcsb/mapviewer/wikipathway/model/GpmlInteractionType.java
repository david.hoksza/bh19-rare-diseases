package lcsb.mapviewer.wikipathway.model;

/**
 * Enum representing all known GPML interaction (line) types.
 * 
 * @author Piotr Gawron
 *
 */
public enum GpmlInteractionType {

  /**
   * Simple line.
   */
  LINE(null),

  /**
   * Arrow.
   */
  ARROW("Arrow"),

  /**
   * T-bar.
   */
  TBAR("TBar"),

  /**
   * Necessary stimulation.
   */
  NECESSARY_STIMULATION("mim-necessary-stimulation"),

  /**
   * Binding.
   */
  BINDING("mim-binding"),

  /**
   * Conversion.
   */
  CONVERSION("mim-conversion"),

  /**
   * Stimulation.
   */
  STIMULATION("mim-stimulation"),

  /**
   * Modification.
   */
  MODIFICATION("mim-modification"),

  /**
   * Catalysis.
   */
  CATALYSIS("mim-catalysis"),

  /**
   * Inhibition.
   */
  INHIBITION("mim-inhibition"),

  /**
   * Cleavage.
   */
  CLEAVAGE("mim-cleavage"),

  /**
   * Covalent bond.
   */
  COVALENT_BOND("mim-covalent-bond"),

  /**
   * Branching (to the left).
   */
  BRANCHING_LEFT("mim-branching-left"),

  /**
   * Branching (to the right).
   */
  BRANCHING_RIGHT("mim-branching-right"),

  /**
   * Transcription, translation.
   */
  TRANSCRIPTION_TRANSLATION("mim-transcription-translation"),

  /**
   * Gap.
   */
  GAP("mim-gap");

  /**
   * GPML string representing this type.
   */
  private String gpmlString;

  /**
   * Default constructor.
   * 
   * @param gpmlString
   *          {@link #gpmlString}
   */
  GpmlInteractionType(String gpmlString) {
    this.gpmlString = gpmlString;
  }

  /**
   * Returns a type identified by the GPML string.
   *
   * @param gpmlString
   *          string in GPML representing type
   * @return type identified by the GPML string
   * @throws UnknownTypeException
   *           thrown when type cannot be resolved
   */
  public static GpmlInteractionType getTypeByGpmlString(String gpmlString) throws UnknownTypeException {
    for (GpmlInteractionType type : values()) {
      if (type.getGpmlString() == null) {
        if (gpmlString == null || gpmlString.isEmpty()) {
          return type;
        }
      } else if (type.getGpmlString().equals(gpmlString)) {
        return type;
      }
    }
    throw new UnknownTypeException("Unknown gpml interaction type: " + gpmlString);
  }

  /**
   * @return the gpmlString
   * @see #gpmlString
   */
  public String getGpmlString() {
    return gpmlString;
  }
}
