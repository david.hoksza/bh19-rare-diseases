package lcsb.mapviewer.wikipathway.XML;

import lcsb.mapviewer.converter.ConverterException;

/**
 * Exception that shold be thrown when the value of the attribute in xml is
 * invalid.
 * 
 * @author Piotr Gawron
 * 
 */
public class UnknownAttributeValueException extends ConverterException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public UnknownAttributeValueException(String string) {
    super(string);
  }

}
