package lcsb.mapviewer.wikipathway.model;

/**
 * This enum describes how two different {@link GpmlInteractionType types} of
 * {@link Edge edges} should be merged (what should be the output type and
 * direction of the output edge).
 * 
 * @author Piotr Gawron
 *
 */
public enum MergeMapping {

  /**
   * Merging {@link GpmlInteractionType#LINE} and
   * {@link GpmlInteractionType#ARROW} when both of them are in right direction.
   */
  LINE_ARROW_N_N(GpmlInteractionType.LINE, false, GpmlInteractionType.ARROW, false, GpmlInteractionType.ARROW, false),

  /**
   * Merging {@link GpmlInteractionType#LINE} and
   * {@link GpmlInteractionType#ARROW} when line is reversed (user doesn't see a
   * difference) and arrow is in normal direction.
   */
  LINE_ARROW_R_N(GpmlInteractionType.LINE, true, GpmlInteractionType.ARROW, false, GpmlInteractionType.ARROW, false),

  /**
   * Merging {@link GpmlInteractionType#ARROW} and
   * {@link GpmlInteractionType#LINE} when arrow is reversed and line is in normal
   * direction.
   */
  ARROW_LINE_R_N(GpmlInteractionType.ARROW, true, GpmlInteractionType.LINE, false, GpmlInteractionType.ARROW, true),

  /**
   * Merging {@link GpmlInteractionType#ARROW} and
   * {@link GpmlInteractionType#LINE} when line is reversed and line is also
   * reversed (user doesn't see a difference) .
   */
  ARROW_LINE_R_R(GpmlInteractionType.ARROW, true, GpmlInteractionType.LINE, true, GpmlInteractionType.ARROW, true),

  /**
   * Merging {@link GpmlInteractionType#LINE} and {@link GpmlInteractionType#LINE}
   * when both of them are in right direction.
   */
  LINE_LINE_N_N(GpmlInteractionType.LINE, false, GpmlInteractionType.LINE, false, GpmlInteractionType.LINE, false),

  /**
   * Merging {@link GpmlInteractionType#LINE} and {@link GpmlInteractionType#LINE}
   * when first of them is reversed and second not.
   */
  LINE_LINE_R_N(GpmlInteractionType.LINE, true, GpmlInteractionType.LINE, false, GpmlInteractionType.LINE, false),

  /**
   * Merging {@link GpmlInteractionType#LINE} and {@link GpmlInteractionType#LINE}
   * when first is in right direction, but second reversed.
   */
  LINE_LINE_N_R(GpmlInteractionType.LINE, false, GpmlInteractionType.LINE, true, GpmlInteractionType.LINE, false),

  /**
   * Merging {@link GpmlInteractionType#LINE} and {@link GpmlInteractionType#LINE}
   * when both of them are reversed.
   */
  LINE_LINE_R_R(GpmlInteractionType.LINE, true, GpmlInteractionType.LINE, true, GpmlInteractionType.LINE, false);

  /**
   * Type of the first edge to merge.
   */
  private GpmlInteractionType type1;

  /**
   * Is the first edge reversed or not (if first edge is reversed it means that
   * the merging will be done in the beginning of first edge).
   */
  private boolean reversed1;

  /**
   * Type of the second edge.
   */
  private GpmlInteractionType type2;

  /**
   * Is the second edge reversed or not (if second edge is reversed it means that
   * the merging will be done in the end of second edge).
   */
  private boolean reversed2;

  /**
   * Which type should be used after merging.
   */
  private GpmlInteractionType resultType;

  /**
   * Should the edge after merging be reversed or not. If it's reversed it means
   * that it will go from second edge to first edge, if it's not reversed then it
   * will go from first edge to second edge.
   */
  private boolean resultReversed;

  /**
   * Default constructor.
   * 
   * @param type1
   *          {@link #type1}
   * @param reversed1
   *          {@link #reversed1}
   * @param type2
   *          {@link #type2}
   * @param reversed2
   *          {@link #reversed2}
   * @param resultType
   *          {@link #resultType}
   * @param resultReversed
   *          {@link #resultReversed}
   */
  MergeMapping(GpmlInteractionType type1, boolean reversed1, GpmlInteractionType type2, boolean reversed2,
      GpmlInteractionType resultType,
      boolean resultReversed) {
    this.type1 = type1;
    this.reversed1 = reversed1;
    this.type2 = type2;
    this.reversed2 = reversed2;
    this.resultType = resultType;
    this.resultReversed = resultReversed;
  }

  /**
   * Returns {@link MergeMapping merge rule} that should be used for merging two
   * {@link Edge edges}.
   * 
   * @param type1
   *          {@link #type1}
   * @param reversed1
   *          {@link #reversed1}
   * @param type2
   *          {@link #type2}
   * @param reversed2
   *          {@link #reversed2}
   * @return {@link MergeMapping merge rule} that should be used for merging two
   *         {@link Edge edges} or <code>null</code> if such rule cannot be found
   */
  public static MergeMapping getMergeMappingByInteractions(GpmlInteractionType type1, boolean reversed1,
      GpmlInteractionType type2, boolean reversed2) {
    for (MergeMapping mm : values()) {
      if (mm.getType1().equals(type1) && mm.isReversed1() == reversed1 && mm.getType2().equals(type2)
          && mm.isReversed2() == reversed2) {
        return mm;
      }
    }
    return null;
  }

  /**
   * @return the type1
   * @see #type1
   */
  public GpmlInteractionType getType1() {
    return type1;
  }

  /**
   * @return the reversed1
   * @see #reversed1
   */
  public boolean isReversed1() {
    return reversed1;
  }

  /**
   * @return the type2
   * @see #type2
   */
  public GpmlInteractionType getType2() {
    return type2;
  }

  /**
   * @return the reversed2
   * @see #reversed2
   */
  public boolean isReversed2() {
    return reversed2;
  }

  /**
   * @return the resultType
   * @see #resultType
   */
  public GpmlInteractionType getResultType() {
    return resultType;
  }

  /**
   * @return the resultReversed
   * @see #resultReversed
   */
  public boolean isResultReversed() {
    return resultReversed;
  }

}
