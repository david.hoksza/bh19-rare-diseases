package lcsb.mapviewer.wikipathway.XML;

import lcsb.mapviewer.converter.ConverterException;

/**
 * Exception that shold be thrown when there are problems with finding child
 * classes in the complex structures.
 * 
 * @author Piotr Gawron
 * 
 */
public class UnknownChildClassException extends ConverterException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public UnknownChildClassException(String string) {
    super(string);
  }

}
