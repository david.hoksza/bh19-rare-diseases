package lcsb.mapviewer.wikipathway.XML;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import javax.xml.parsers.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.wikipathway.model.*;

/**
 * This class allows to parse
 * <a href="http://developers.pathvisio.org/wiki/EverythingGpml">gpml</a>.
 * 
 * @author Jan Badura
 * 
 */
public class GpmlParser {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  /**
   * Parser used for creating {@link Shape shapes}.
   */
  private ShapeParser shapeParser;

  /**
   * Parser used for creating {@link lcsb.mapviewer.wikipathway.model.DataNode
   * data nodes}.
   */
  private DataNodeParser dataNodeParser;

  /**
   * Parser used for creating {@link Label labels}.
   */
  private LabelParser labelParser;

  /**
   * Parser used for creating {@link Edge edges}.
   */
  private EdgeParser edgeParser;

  /**
   * Parser used for creating {@link lcsb.mapviewer.wikipathway.model.State
   * states}.
   */
  private StateParser stateParser;

  /**
   * Parser used for creating {@link Edge edges} from line xml nodes.
   */
  private EdgeLineParser edgeLineParser;

  /**
   * Parser used for creating {@link PointData points} from line xml nodes.
   */
  private PointDataParser pointParser;

  /**
   * This function returns parent interaction for edge that has anchor at one end.
   *
   * @param graph
   *          model where data is stored
   * @param edge
   *          edge for which interaction is looked for
   * @return {@link Interaction} that is parent reaction for given edge.
   * @throws CyclicEdgeException
   *           thrown when parent interaction cannot be found because reactions
   *           are cyclic
   */
  private Interaction getParentInteraction(Graph graph, Edge edge) throws CyclicEdgeException {
    // remeber what we already processed (to detect cycles)
    Set<Edge> processedEdges = new HashSet<>();
    processedEdges.add(edge);
    while (graph.getNodeByGraphId(edge.getEnd()) == null || graph.getNodeByGraphId(edge.getStart()) == null) {
      String anchor = null;
      if (graph.getEdgeByAnchor(edge.getEnd()) != null) {
        anchor = edge.getEnd();
      } else if (graph.getEdgeByAnchor(edge.getStart()) != null) {
        anchor = edge.getStart();
      } else {
        return null;
      }

      edge = graph.getEdgeByAnchor(anchor);
      // when we have cycle then return null
      if (processedEdges.contains(edge)) {
        throw new CyclicEdgeException("Edge is a part of invalid, cyclic edge", edge);
      }
      processedEdges.add(edge);
    }
    return graph.getInteractionByGraphId(edge.getGraphId());
  }

  /**
   * This function sets width and height in given graph.
   *
   * @param node
   *          xml node where data is stored
   * @param graph
   *          object where data is stored
   */
  protected void setSize(Node node, Graph graph) {
    Element eElement = (Element) node;
    String boardWidth = eElement.getAttribute("BoardWidth");
    String boardHeight = eElement.getAttribute("BoardHeight");
    graph.setBoardWidth(Math.ceil(Double.parseDouble(boardWidth)));
    graph.setBoardHeight(Math.ceil(Double.parseDouble(boardHeight)));
  }

  /**
   * This function adds groups to graph and nest them.
   * 
   * @param groups
   *          xml nodes
   * @param graph
   *          object where data is stored
   * @throws UnknownAttributeValueException
   *           thrown when there is a problem with xml attributes
   */
  protected void addGroups(List<Node> groups, Graph graph) throws UnknownAttributeValueException {
    // Adding Groups to graph
    for (Node nNode : groups) {
      if (nNode.getNodeType() == Node.ELEMENT_NODE) {
        Element eElement = (Element) nNode;
        String graphId = eElement.getAttribute("GraphId");
        String groupId = eElement.getAttribute("GroupId");
        if (graphId.equals("") || graphId == null) {
          graphId = groupId;
        }
        String style = eElement.getAttribute("Style");
        if ("".equals(style)) {
          style = null;
        }
        if (style != null &&
            !"Complex".equalsIgnoreCase(style) &&
            !"Group".equalsIgnoreCase(style)) {
          throw new UnknownAttributeValueException(
              "Unknown value of \"style\" attribute for group node: " + style
                  + ". Only null, Complex, Group are supported.");
        }
        Group group = new Group(graphId, groupId, graph.getMapName());
        group.setStyle(style);

        String zIndex = eElement.getAttribute("ZOrder");
        if (zIndex != null && !zIndex.isEmpty()) {
          group.setzOrder(Integer.valueOf(zIndex));
        }
        graph.addGroup(group);
      }
    }
    // Handling nested groups
    Group gr1, gr2;
    for (Node nNode : groups) {
      if (nNode.getNodeType() == Node.ELEMENT_NODE) {
        Element eElement = (Element) nNode;
        String groupRef = eElement.getAttribute("GroupRef");
        String groupId = eElement.getAttribute("GroupId");
        if (groupRef != null && !groupRef.equals("")) {
          gr1 = graph.getGroupByGroupId(groupRef);
          gr2 = graph.getGroupByGroupId(groupId);
          gr1.addNode(gr2);
        }
      }
    }
  }

  /**
   * This function adds edges to graph. It ignores edges that have no connection
   * at one end and edges that connects to shapes or labels.
   *
   * @param nodes
   *          xml nodes
   * @param graph
   *          object where data is sorted
   */
  private void prepareEdges(List<Node> nodes, Graph graph) {
    for (Node nNode : nodes) {
      if (nNode.getNodeType() == Node.ELEMENT_NODE) {
        Edge edge;
        try {
          edge = edgeParser.parse((Element) nNode);

          if (graph.getLabelByGraphId(edge.getStart()) != null) {
            Label label = graph.getLabelByGraphId(edge.getStart());
            label.setTreatAsNode(true);
          }
          if (graph.getLabelByGraphId(edge.getEnd()) != null) {
            Label label = graph.getLabelByGraphId(edge.getEnd());
            label.setTreatAsNode(true);
          }

          if (graph.getShapeByGraphId(edge.getStart()) != null) {
            Shape shape = graph.getShapeByGraphId(edge.getStart());
            shape.setTreatAsNode(true);
          }
          if (graph.getShapeByGraphId(edge.getEnd()) != null) {
            Shape shape = graph.getShapeByGraphId(edge.getEnd());
            shape.setTreatAsNode(true);
          }
          if (edge.getStart() == null || edge.getEnd() == null) {
            logger.warn(edge.getLogMarker(), "Interaction is not connected");

          } else {
            graph.addEdge(edge);
          }
        } catch (ConverterException e) {
          logger.warn(e, e);
        }
      }
    }
  }

  /**
   * This function transforms Edges into Interactions. First: Edges with
   * NodeToNode connection are transformed into Interactions. Second: Edges with
   * AnchorToNode connection are added to right interaction. Edges with
   * AnchorToAnchor connection are ignored.
   *
   * @param graph
   *          object where data is sorted
   * @throws InvalidXmlSchemaException
   *           thrown when the data in input file is invalid
   */
  protected void addInteractions(Graph graph) throws InvalidXmlSchemaException {
    for (Edge edge : graph.getEdges()) {
      if (graph.getNodeByGraphId(edge.getEnd()) != null && graph.getNodeByGraphId(edge.getStart()) != null) {
        markLabelsAndShapesToBecomeSpeciesForEdge(graph, edge);

        Interaction interaction = new Interaction(edge);
        graph.addInteraction(interaction);
        InteractionMapping mapping = InteractionMapping.getInteractionMapping(edge.getType(), edge.getLine().getType());
        if (mapping != null) {
          if (mapping.isInteractionWarning()) {
            logger.warn(edge.getLogMarker(), "Invalid interaction type.");
          }
        } else {
          throw new InvalidXmlSchemaException(
              edge.getLogMarker() + "Unknown interaction type: " + edge.getType() + " and line type: "
                  + edge.getLine().getType());
        }
      }
    }
    for (Edge edge : graph.getEdges()) {
      try {
        if (graph.getEdgeByAnchor(edge.getEnd()) != null && graph.getEdgeByAnchor(edge.getStart()) != null) {
          logger.warn(edge.getLogMarker(), "Interaction can not connect two anchors.");
        } else if (graph.getEdgeByAnchor(edge.getEnd()) != null && graph.getNodeByGraphId(edge.getStart()) != null) {
          Interaction tmp = getParentInteraction(graph, edge);
          if (tmp != null) {
            InteractionMapping mapping = InteractionMapping.getInteractionMapping(edge.getType(),
                edge.getLine().getType());
            if (mapping != null) {
              if (mapping.isInputWarning()) {
                logger.warn(edge.getLogMarker(), "Invalid interaction type as an input to reaction.");
              }
              if (Modifier.class.isAssignableFrom(mapping.getModelInputReactionNodeType())) {
                tmp.addModifier(edge);
              } else if (Reactant.class.isAssignableFrom(mapping.getModelInputReactionNodeType())) {
                tmp.addReactant(edge);
              } else if (Product.class.isAssignableFrom(mapping.getModelInputReactionNodeType())) {
                tmp.addProduct(edge);
              } else {
                throw new InvalidStateException(
                    "Unknown internal model type: " + mapping.getModelInputReactionNodeType());
              }
              markLabelsAndShapesToBecomeSpeciesForEdge(graph, edge);
            } else {
              throw new InvalidXmlSchemaException("Unknown interaction type: " + edge.getType());
            }
          } else {
            logger.warn(edge.getLogMarker(), "Interaction is disconnected.");
          }
        } else if (graph.getEdgeByAnchor(edge.getStart()) != null && graph.getNodeByGraphId(edge.getEnd()) != null) {
          Interaction tmp = getParentInteraction(graph, edge);
          if (tmp != null) {
            InteractionMapping mapping = InteractionMapping.getInteractionMapping(edge.getType(),
                edge.getLine().getType());
            if (mapping != null) {
              if (mapping.isOutputWarning()) {
                logger.warn(edge.getLogMarker(), "Invalid interaction type \"" + edge.getType()
                    + "\" as an output from reaction.");
              }
              if (Modifier.class.isAssignableFrom(mapping.getModelOutputReactionNodeType())) {
                tmp.addModifier(edge);
              } else if (Reactant.class.isAssignableFrom(mapping.getModelOutputReactionNodeType())) {
                tmp.addReactant(edge);
              } else if (Product.class.isAssignableFrom(mapping.getModelOutputReactionNodeType())) {
                tmp.addProduct(edge);
              } else {
                throw new InvalidStateException(
                    "Unknown internal model type: " + mapping.getModelOutputReactionNodeType());
              }
              markLabelsAndShapesToBecomeSpeciesForEdge(graph, edge);
            } else {
              throw new InvalidXmlSchemaException("Unknown interaction type: " + edge.getType());
            }
          } else {
            logger.warn(edge.getLogMarker(), "Interaction is disconnected.");
          }
        } else if (graph.getNodeByGraphId(edge.getEnd()) == null || graph.getNodeByGraphId(edge.getStart()) == null) {
          logger.warn(edge.getLogMarker(), "Interaction edge is invalid (one end is not connected).");
        }
      } catch (CyclicEdgeException e) {
        logger.warn(e.getLogMarker(), e.getMessage(), e);
      }
    }
  }

  private void markLabelsAndShapesToBecomeSpeciesForEdge(Graph graph, Edge edge) {
    if (graph.getNodeByGraphId(edge.getEnd()) instanceof Label) {
      Label label = (Label) graph.getNodeByGraphId(edge.getEnd());
      label.setTreatAsNode(true);
    }
    if (graph.getNodeByGraphId(edge.getStart()) instanceof Label) {
      Label label = (Label) graph.getNodeByGraphId(edge.getStart());
      label.setTreatAsNode(true);
    }
    if (graph.getShapeByGraphId(edge.getEnd()) instanceof Shape) {
      Shape shape = (Shape) graph.getShapeByGraphId(edge.getEnd());
      shape.setTreatAsNode(true);
    }
    if (graph.getShapeByGraphId(edge.getStart()) instanceof Shape) {
      Shape shape = (Shape) graph.getShapeByGraphId(edge.getStart());
      shape.setTreatAsNode(true);
    }
  }

  /**
   * Creates gpml {@link Graph} model from gpml input stream.
   *
   * @param stream
   *          input stream with gpml model
   * @return {@link Graph} model
   * @throws IOException
   *           thrown when there is a problem with input file
   * @throws ConverterException
   *           thrown when there is a problem with parsing input file
   */
  public Graph createGraph(InputStream stream) throws IOException, ConverterException {
    try {
      DocumentBuilder builder;
      builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      Document document = builder.parse(stream);

      // Get the document's root XML node
      NodeList root = document.getElementsByTagName("Pathway");

      NodeList nodes = root.item(0).getChildNodes();

      Node dimensionNode = null;
      List<Node> dataNodes = new ArrayList<Node>();
      List<Node> interactions = new ArrayList<Node>();
      List<Node> labels = new ArrayList<Node>();
      List<Node> groups = new ArrayList<Node>();
      List<Node> shapes = new ArrayList<Node>();
      List<Node> lines = new ArrayList<Node>();
      List<Node> states = new ArrayList<Node>();
      List<Element> attributes = new ArrayList<Element>();
      Node biopax = null;

      Graph graph = new Graph();
      graph.setMapName(XmlParser.getNodeAttr("Name", root.item(0)));
      edgeLineParser = new EdgeLineParser(graph.getMapName());
      edgeParser = new EdgeParser(graph.getMapName());
      dataNodeParser = new DataNodeParser(graph.getMapName());
      labelParser = new LabelParser(graph.getMapName());
      shapeParser = new ShapeParser(graph.getMapName());
      stateParser = new StateParser(graph.getMapName());
      pointParser = new PointDataParser(graph.getMapName());

      for (int x = 0; x < nodes.getLength(); x++) {
        Node node = nodes.item(x);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("Graphics")) {
            dimensionNode = node;
          } else if (node.getNodeName().equalsIgnoreCase("DataNode")) {
            dataNodes.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Interaction")) {
            interactions.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Label")) {
            labels.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Group")) {
            groups.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Attribute")) {
            attributes.add((Element) node);
          } else if (node.getNodeName().equalsIgnoreCase("Shape")) {
            shapes.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("GraphicalLine")) {
            lines.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Comment")) {
            graph.addComment(node.getTextContent());
          } else if (node.getNodeName().equalsIgnoreCase("BiopaxRef")) {
            graph.addBiopaxReferences(node.getTextContent());
          } else if (node.getNodeName().equalsIgnoreCase("State")) {
            states.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Biopax")) {
            if (biopax != null) {
              throw new ConverterException("Biopax node should appear only once");
            }
            biopax = node;
          } else if (node.getNodeName().equalsIgnoreCase("InfoBox")) {
            // infobox can be skipped
            continue;
          } else if (node.getNodeName().equalsIgnoreCase("Legend")) {
            // legend can be skipped
            continue;
          } else {
            logger.warn("Unknown element of gpml file: " + node.getNodeName());
          }
        }
      }
      graph.addLines(parseLines(lines));
      graph.addEdges(parseEdgesFromLines(lines));

      if (dimensionNode != null) {
        setSize(dimensionNode, graph);
      }
      addGroups(groups, graph);

      graph.addDataNodes(dataNodeParser.parseCollection(dataNodes));
      graph.addLabels(labelParser.parseCollection(labels));
      graph.addShapes(shapeParser.parseCollection(shapes));
      graph.addStates(stateParser.parseCollection(states));

      prepareEdges(interactions, graph);

      mergeEdges(graph);

      addInteractions(graph);
      if (biopax != null) {
        graph.setBiopaxData(new BiopaxParser(graph.getMapName()).parse(biopax));
      }
      Map<String, String> attributesMap = new HashMap<String, String>();
      for (Element attribute : attributes) {
        String key = attribute.getAttribute("Key");
        String value = attribute.getAttribute("Value");
        if (attributesMap.get(key) != null) {
          logger.warn("Model xml contains duplicate attributes: " + key);
        } else {
          attributesMap.put(key, value);
        }
      }
      graph.setAttributes(attributesMap);
      return graph;
    } catch (ParserConfigurationException e) {
      throw new ConverterException("Problem with input data", e);
    } catch (SAXException e) {
      throw new ConverterException("Problem with input data", e);
    } catch (InvalidXmlSchemaException e) {
      throw new ConverterException("Problem with input data", e);
    }
  }

  /**
   * This method merge edges that should be merged into single line.
   *
   * @param graph
   *          model where edges are stored
   */
  private void mergeEdges(Graph graph) {
    List<Edge> toExtend = new ArrayList<>();
    List<Edge> toRemove = new ArrayList<>();
    List<Edge> toAdd = new ArrayList<>();
    Map<String, List<Edge>> extendable = new HashMap<>();
    for (Edge edge : graph.getEdges()) {
      if (graph.getEdgeByAnchor(edge.getStart()) == edge) {
        toExtend.add(edge);
      } else if (graph.getEdgeByAnchor(edge.getEnd()) == edge) {
        toExtend.add(edge);
      } else if (graph.getEdgeByAnchor(edge.getEnd()) != null) {
        List<Edge> list = extendable.get(edge.getEnd());
        if (list == null) {
          list = new ArrayList<>();
          extendable.put(edge.getEnd(), list);
        }
        list.add(edge);
      } else if (graph.getEdgeByAnchor(edge.getStart()) != null) {
        List<Edge> list = extendable.get(edge.getStart());
        if (list == null) {
          list = new ArrayList<>();
          extendable.put(edge.getStart(), list);
        }
        list.add(edge);
      }

    }

    for (Edge edge : toExtend) {
      String anchor = null;
      if (graph.getEdgeByAnchor(edge.getStart()) == edge) {
        anchor = edge.getStart();
      } else if (graph.getEdgeByAnchor(edge.getEnd()) == edge) {
        anchor = edge.getEnd();
      }
      List<Edge> howExtend = extendable.get(anchor);
      if (howExtend == null) {
        logger.warn(edge.getLogMarker(), " Should be connected with another element, but nothing found.");
        if (edge.getStart().equals(anchor)) {
          edge.setStart(null);
        } else if (edge.getEnd().equals(anchor)) {
          edge.setEnd(null);
        }
      } else if (howExtend.size() > 0) {
        try {
          Edge newEdge = mergeEdges(edge, howExtend.get(0));
          toRemove.add(edge);
          toRemove.add(howExtend.get(0));
          toAdd.add(newEdge);
        } catch (UnknownMergingMethodException exception) {
          toRemove.add(edge);
          toRemove.add(howExtend.get(0));

          logger.warn(edge.getLogMarker(), exception.getMessage(), exception);
        }
      }
    }
    for (Edge e : toRemove) {
      graph.removeEdge(e);
    }
    for (Edge e : toAdd) {
      graph.addEdge(e);
    }
  }

  /**
   * Method that merge two {@link Edge edges}.
   *
   * @param edge1
   *          first edge to merge
   * @param edge2
   *          second edge to merge
   * @return new edge that merges two parameters
   * @throws UnknownMergingMethodException
   *           thrown when edges cannot be merged
   */
  private Edge mergeEdges(Edge edge1, Edge edge2) throws UnknownMergingMethodException {
    MergeMapping mapping = null;
    if (edge1.getStart() != null && edge1.getStart().equals(edge2.getStart())) {
      mapping = MergeMapping.getMergeMappingByInteractions(edge1.getType(), true, edge2.getType(), false);
    } else if (edge1.getStart() != null && edge1.getStart().equals(edge2.getEnd())) {
      mapping = MergeMapping.getMergeMappingByInteractions(edge1.getType(), true, edge2.getType(), true);
    } else if (edge1.getEnd() != null && edge1.getEnd().equals(edge2.getStart())) {
      mapping = MergeMapping.getMergeMappingByInteractions(edge1.getType(), false, edge2.getType(), false);
    } else if (edge1.getEnd() != null && edge1.getEnd().equals(edge2.getEnd())) {
      mapping = MergeMapping.getMergeMappingByInteractions(edge1.getType(), false, edge2.getType(), true);
    }
    if (mapping == null) {
      throw new UnknownMergingMethodException("Don't know how to merge interactions");
    }
    Edge first;
    if (mapping.isReversed1()) {
      first = edge1.reverse();
    } else {
      first = new Edge(edge1);
    }
    Edge second;
    if (mapping.isReversed2()) {
      second = edge2.reverse();
    } else {
      second = new Edge(edge2);
    }
    first.extend(second);

    if (!first.getLine().getType().equals(second.getLine().getType())) {
      logger.warn(first.getLogMarker(), "Merging edges with different line types: "
          + first.getLine().getType() + ", "
          + second.getLine().getType());
    }
    first.setType(mapping.getResultType());
    if (mapping.isResultReversed()) {
      return first.reverse();
    } else {
      return first;
    }
  }

  /**
   * Creates edges from lines when it's possible.
   *
   * @param lines
   *          xml nodes with lines
   * @return list of edges that could be created from xml nodes
   */
  private Collection<Edge> parseEdgesFromLines(List<Node> lines) {
    List<Edge> result = new ArrayList<Edge>();
    for (Node lNode : lines) {
      try {
        Edge line = edgeLineParser.parse((Element) lNode);
        if (line.getStart() != null && line.getEnd() != null) {
          result.add(line);
        }
      } catch (ConverterException e) {
        logger.warn(e, e);

      }
    }
    return result;
  }

  /**
   * Creates lines from the list of gpml xml nodes.
   *
   * @param lines
   *          list of xml nodes
   * @return list of {@link PolylineData lines}
   * @throws UnknownTypeException
   *           thrown when the type of line defined in xml node is unknown
   */
  private Collection<PolylineData> parseLines(List<Node> lines) throws UnknownTypeException {
    List<PolylineData> result = new ArrayList<PolylineData>();
    for (Node lNode : lines) {
      int refs = 0;
      PolylineData line = new PolylineData();
      NodeList nodes = lNode.getChildNodes();
      for (int i = 0; i < nodes.getLength(); i++) {
        Node node = nodes.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("Graphics")) {
            NodeList nodes2 = node.getChildNodes();
            for (int j = 0; j < nodes2.getLength(); j++) {
              Node node2 = nodes2.item(j);
              if (node2.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node2;
                if (node2.getNodeName().equalsIgnoreCase("Point")) {
                  PointData point = pointParser.parse(element);
                  if (point.hasGraphRef()) {
                    refs++;
                  }
                  line.addPoint(point.toPoint());
                } else {
                  logger.warn("Unknown node in line: " + node2.getNodeName());
                }
              }
            }

          } else {
            logger.warn("Unknown node in line: " + node.getNodeName());
          }
        }
      }
      if (refs < 2) {
        result.add(line);
      }
    }
    return result;
  }
}
