/**
 * Contains plugin for pathvisio that supports import and export to CellDEsigner
 * file. Main plugin class is located in
 * {@link lcsb.mapviewer.wikipathway.ImportExport ImportExport} class. According
 * to
 * <a href="http://developers.pathvisio.org/wiki/PluginDevelopment ">pathvisio
 * guidlines</a>, plugin is implemented as an
 * <a href="http://en.wikipedia.org/wiki/OSGi#Bundles">OSGI Bundle</a>.
 */
package lcsb.mapviewer.wikipathway;
