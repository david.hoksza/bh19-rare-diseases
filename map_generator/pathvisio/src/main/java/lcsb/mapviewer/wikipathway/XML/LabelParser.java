package lcsb.mapviewer.wikipathway.XML;

import java.awt.geom.Rectangle2D;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.wikipathway.model.Label;
import lcsb.mapviewer.wikipathway.model.UnknownTypeException;

/**
 * Parser class that creates {@link Label} objects from Xml {@link Element node}
 * .
 * 
 * @author Piotr Gawron
 *
 */
public class LabelParser extends GraphicalPathwayElementParser<Label> {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  private ReferenceParser referenceParser;

  public LabelParser(String mapName) {
    super(mapName);
    referenceParser = new ReferenceParser(mapName);
  }

  @Override
  public Label parse(Element eElement) throws UnknownTypeException {
    if (!eElement.getNodeName().equals("Label")) {
      throw new InvalidArgumentException(ShapeParser.class.getSimpleName() + " can parse only Label xml nodes");
    }
    Label label = new Label(eElement.getAttribute("GraphId"), getMapName());

    for (Pair<String, String> entry : getAttributes(eElement)) {
      switch (entry.getLeft()) {
      case ("GraphId"):
        break;
      case ("TextLabel"):
        label.setTextLabel(entry.getRight());
        break;
      case ("Href"):
        MiriamData md = referenceParser.hrefToMiriamData(entry.getRight());
        if (md == null) {
          label.addComment(entry.getRight());
        } else {
          label.addReference(md);
        }
        break;
      case ("GroupRef"):
        label.setGroupRef(entry.getRight());
        break;
      default:
        logger.warn("Unknown attribute of " + eElement.getNodeName() + " node: " + entry.getLeft());
        break;
      }
    }

    NodeList tmpList = eElement.getChildNodes();
    for (int j = 0; j < tmpList.getLength(); j++) {
      Node tmpNode = tmpList.item(j);
      if (tmpNode.getNodeType() == Node.ELEMENT_NODE) {
        Element eTmp = (Element) tmpNode;
        switch (eTmp.getNodeName()) {
        case ("Comment"):
          label.addComment(eTmp.getTextContent());
          break;
        case ("BiopaxRef"):
          label.addBiopaxReference(eTmp.getTextContent());
          break;
        case ("Graphics"):
          parseGraphics(eTmp, label);
          break;
        case ("Attribute"):
          parseAttribute(eTmp, label);
          break;
        default:
          logger.warn("Unknown sub-node of " + eElement.getNodeName() + " node: " + eTmp.getNodeName());
          break;
        }
      }
    }

    return label;
  }

  @Override
  public String toXml(Label node) throws ConverterException {
    throw new NotImplementedException();
  }

  @Override
  public String toXml(Collection<Label> list) throws ConverterException {
    throw new NotImplementedException();
  }

  /**
   * Parse graphics xml node in the shape node.
   *
   * @param eTmp
   *          xml node with graphics
   * @param shape
   *          shape where data should be added
   * @throws UnknownTypeException
   *           thrown when some elements contain unknown typevalues
   */
  protected void parseGraphics(Element eTmp, Label shape) throws UnknownTypeException {
    Double centerX = null;
    Double centerY = null;
    Double width = null;
    Double height = null;
    for (Pair<String, String> entry : getAttributes(eTmp)) {
      if (!parseCommonGraphicAttributes(shape, entry)) {
        switch (entry.getLeft()) {
        case ("CenterX"):
          centerX = Double.valueOf(entry.getRight());
          break;
        case ("CenterY"):
          centerY = Double.valueOf(entry.getRight());
          break;
        case ("Width"):
          width = Double.valueOf(entry.getRight());
          break;
        case ("Height"):
          height = Double.valueOf(entry.getRight());
          break;
        case ("Color"):
          shape.setColor(hexStringToColor(entry.getRight()));
          break;
        case ("FillColor"):
          shape.setFillColor(hexStringToColor(entry.getRight()));
          break;
        case ("ZOrder"):
          shape.setzOrder(Integer.valueOf(entry.getRight()));
          break;
        case ("FontSize"):
          shape.setFontSize(Double.valueOf(entry.getRight()));
          break;
        case ("FontWeight"):
          shape.setFontWeight(entry.getRight());
          break;
        case ("Valign"):
          shape.setvAlign(entry.getRight());
          break;
        case ("ShapeType"):
          shape.setShape(entry.getRight());
          break;
        default:
          logger.warn("Unknown attribute of " + eTmp.getNodeName() + " node: " + entry.getLeft() + "; value: "
              + entry.getRight());
          break;
        }
      }
    }
    shape.setRectangle(new Rectangle2D.Double(centerX - width / 2, centerY - height / 2, width, height));
  }

  /**
   * Method that parses {@link Label} xml attribute.
   *
   * @param eTmp
   *          xml node with attribute
   * @param shape
   *          shape where data should be added
   */
  private void parseAttribute(Element eTmp, Label shape) {
    String key = eTmp.getAttribute("Key");
    String value = eTmp.getAttribute("Value");
    switch (key) {
    case ("org.pathvisio.model.GenMAPP-Xref"):
      // skip it when it's empty
      if (!value.isEmpty()) {
        logger.warn(shape.getLogMarker() + "Unknown attribute of node. Key: " + key + "; value: " + value);
        break;
      }
      break;
    default:
      logger.warn(shape.getLogMarker() + "Unknown attribute of node. Key:" + key + "; value: " + value);
      break;
    }
  }

}
