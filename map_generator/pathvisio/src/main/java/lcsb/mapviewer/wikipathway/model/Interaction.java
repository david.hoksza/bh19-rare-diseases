package lcsb.mapviewer.wikipathway.model;

import java.awt.Color;
import java.io.Serializable;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Class used to store data from Interaction from GPML.
 * 
 * @author Jan Badura
 * 
 */
public class Interaction implements Serializable {
  static Logger logger = LogManager.getLogger();

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Identifier in GPML model.
   */
  private String graphId;

  /**
   * Where this {@link Interaction} starts (in which {@link Edge}/
   * {@link Interaction}).
   */
  private String start;

  /**
   * Where this {@link Interaction} ends (in which {@link Edge}/
   * {@link Interaction}).
   */
  private String end;

  /**
   * Line representing this edge.
   */
  private PolylineData line;

  /**
   * Z order of the {@link Interaction} (how far it should be located in the z
   * coordinate).
   */
  private Integer zOrder;

  /**
   * Gpml interaction type (arrow).
   */
  private GpmlInteractionType type = GpmlInteractionType.LINE;

  /**
   * List of anchors placed on the edge.
   */
  private Set<String> anchors;

  /**
   * Comments.
   */
  private List<String> comments = new ArrayList<>();

  /**
   * List of edges representing reactants in interaction.
   */
  private Set<Edge> reactants = new HashSet<>();

  /**
   * List of edges representing products in interaction.
   */
  private Set<Edge> products = new HashSet<>();

  /**
   * List of edges representing modifiers in interaction.
   */
  private Set<Edge> modifiers = new HashSet<>();

  /**
   * Identifiers of BioPax references.
   */
  private Set<String> biopaxReferences = new HashSet<>();

  /**
   * References for given edge.
   */
  private List<MiriamData> references = new ArrayList<>();

  private boolean reversible = false;

  /**
   * Default constructor.
   * 
   * @param edge
   *          object will be created from this {@link Edge}
   */
  public Interaction(Edge edge) {
    graphId = edge.getGraphId();
    setStart(edge.getStart());
    setEnd(edge.getEnd());
    setLine(edge.getLine());
    setType(edge.getType());
    setzOrder(edge.getzOrder());
    for (MiriamData md : edge.getReferences()) {
      addReference(new MiriamData(md));
    }
    setColor(edge.getColor());
    this.comments.addAll(edge.getComments());
    biopaxReferences.addAll(edge.getBiopaxReferences());
    reversible = edge.isReversibleReaction();
  }

  /**
   * Empty constructor that should be used only by serialization tools and
   * subclasses.
   */
  Interaction() {
  }

  /**
   * Adds reference.
   * 
   * @param reference
   *          object to add
   */
  private void addReference(MiriamData reference) {
    references.add(reference);
  }

  /**
   * @return the graphId
   * @see #graphId
   */
  public String getGraphId() {
    return graphId;
  }

  /**
   * @param graphId
   *          the graphId to set
   * @see #graphId
   */
  public void setGraphId(String graphId) {
    this.graphId = graphId;
  }

  /**
   * @return the start
   * @see #start
   */
  public String getStart() {
    return start;
  }

  /**
   * @param start
   *          the start to set
   * @see #start
   */
  public void setStart(String start) {
    this.start = start;
  }

  /**
   * @return the end
   * @see #end
   */
  public String getEnd() {
    return end;
  }

  /**
   * @param end
   *          the end to set
   * @see #end
   */
  public void setEnd(String end) {
    this.end = end;
  }

  /**
   * @return the line
   * @see #line
   */
  public PolylineData getLine() {
    return line;
  }

  /**
   * @param line
   *          the line to set
   * @see #line
   */
  public void setLine(PolylineData line) {
    this.line = line;
  }

  /**
   * @return the anchors
   * @see #anchors
   */
  public Set<String> getAnchors() {
    return anchors;
  }

  /**
   * @param anchors
   *          the anchors to set
   * @see #anchors
   */
  public void setAnchors(Set<String> anchors) {
    this.anchors = anchors;
  }

  /**
   * @return the comment
   * @see #comment
   */
  public List<String> getComments() {
    return comments;
  }

  /**
   * @param comment
   *          the comment to set
   * @see #comment
   */
  public void addComment(String comment) {
    this.comments.add(comment);
  }

  /**
   * @return the reactants
   * @see #reactants
   */
  public Set<Edge> getReactants() {
    return reactants;
  }

  /**
   * @return the products
   * @see #products
   */
  public Set<Edge> getProducts() {
    return products;
  }

  /**
   * @return the modifiers
   * @see #modifiers
   */
  public Set<Edge> getModifiers() {
    return modifiers;
  }

  /**
   * Adds product.
   * 
   * @param interaction
   *          product to add
   */
  public void addProduct(Edge interaction) {
    this.products.add(interaction);
    interaction.setColor(getLine().getColor());
    biopaxReferences.addAll(interaction.getBiopaxReferences());
  }

  /**
   * Add modifier.
   * 
   * @param interaction
   *          modifier to add
   */
  public void addModifier(Edge interaction) {
    this.modifiers.add(interaction);
    interaction.setColor(getLine().getColor());
    biopaxReferences.addAll(interaction.getBiopaxReferences());
  }

  /**
   * Add modifier.
   * 
   * @param interaction
   *          reactant to add
   */
  public void addReactant(Edge interaction) {
    this.reactants.add(interaction);
    interaction.setColor(getLine().getColor());
    biopaxReferences.addAll(interaction.getBiopaxReferences());
  }

  /**
   * @return the biopaxReferences
   * @see #biopaxReferences
   */
  public Set<String> getBiopaxReferences() {
    return biopaxReferences;
  }

  /**
   * @param biopaxReferences
   *          the biopaxReferences to set
   * @see #biopaxReferences
   */
  public void setBiopaxReferences(Set<String> biopaxReferences) {
    this.biopaxReferences = biopaxReferences;
  }

  /**
   * @return the type
   * @see #type
   */
  public GpmlInteractionType getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(GpmlInteractionType type) {
    this.type = type;
  }

  /**
   * @return the references
   * @see #references
   */
  public List<MiriamData> getReferences() {
    return references;
  }

  /**
   * @param references
   *          the references to set
   * @see #references
   */
  public void setReferences(List<MiriamData> references) {
    this.references = references;
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return line.getColor();
  }

  /**
   * @param color
   *          the color to set
   * @see #color
   */
  public void setColor(Color color) {
    if (color != null) {
      line.setColor(color);
      for (Edge edge : reactants) {
        edge.setColor(color);
      }
      for (Edge edge : products) {
        edge.setColor(color);
      }
      for (Edge edge : modifiers) {
        edge.setColor(color);
      }
    }
  }

  /**
   * @return the zOrder
   * @see #zOrder
   */
  public Integer getzOrder() {
    return zOrder;
  }

  /**
   * @param zOrder
   *          the zOrder to set
   * @see #zOrder
   */
  public void setzOrder(Integer zOrder) {
    this.zOrder = zOrder;
  }

  public boolean isReversible() {
    return reversible;
  }

  public void setReversible(boolean reversible) {
    this.reversible = reversible;
  }

}
