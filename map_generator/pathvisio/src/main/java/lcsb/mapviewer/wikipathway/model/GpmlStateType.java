package lcsb.mapviewer.wikipathway.model;

/**
 * This enum represents type of line in GPML format.
 * 
 * @author Piotr Gawron
 *
 */
public enum GpmlStateType {

  /**
   * Gpml type representing "GTP"
   * {@link lcsb.mapviewer.model.map.species.Protein#structuralState state of
   * Protein}.
   */
  GTP("GTP", "GTP"),

  /**
   * Gpml type representing "GDP"
   * {@link lcsb.mapviewer.model.map.species.Protein#structuralState state of
   * Protein}.
   */
  GDP("GDP", "GDP");

  /**
   * String in GPML format representig this type.
   */
  private String gpmlString;
  /**
   * String that should be used in our representation.
   */
  private String stringRepresenation;

  /**
   * Default constructor.
   *
   * @param gpmlString
   *          {@link #gpmlString}
   * @param stringRepresenation
   *          {@link #stringRepresenation}
   */
  GpmlStateType(String gpmlString, String stringRepresenation) {
    this.gpmlString = gpmlString;
    this.stringRepresenation = stringRepresenation;
  }

  /**
   * Returns {@link GpmlStateType type} identified by {@link #gpmlName gpml
   * string} identifing the type.
   *
   * @param gpmlName
   *          {@link #gpmlString}
   * @return {@link GpmlStateType type} identified by {@link #gpmlName gpml
   *         string} identifing the type
   * @throws UnknownTypeException
   *           thrown when type cannot be found
   */
  public static GpmlStateType getByGpmlName(String gpmlName) throws UnknownTypeException {
    for (GpmlStateType type : values()) {
      if (type.getGpmlString() == null) {
        if (gpmlName == null) {
          return type;
        }
      } else if (type.getGpmlString().equalsIgnoreCase(gpmlName)) {
        return type;
      }
    }
    throw new UnknownTypeException("Unknown state type: " + gpmlName);
  }

  /**
   * @return the gpmlString
   * @see #gpmlString
   */
  public String getGpmlString() {
    return gpmlString;
  }

  /**
   * @return the stringRepresenation
   * @see #stringRepresenation
   */
  public String getStringRepresenation() {
    return stringRepresenation;
  }
}
