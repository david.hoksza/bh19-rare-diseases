package lcsb.mapviewer.wikipathway.model.biopax;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.*;

public class BiopaxFactory {
  private Logger logger = LogManager.getLogger();
  private String mapName;

  public BiopaxFactory(String mapName) {
    this.mapName = mapName;
  }

  /**
   * Creates {@link MiriamData annotation} from {@link BiopaxPublication}.
   * 
   * @param publication
   *          input BioPax structure
   * @return {@link MiriamData annotation}
   */
  public MiriamData createMiriamData(BiopaxPublication publication) {
    if ("PubMed".equals(publication.getDb())) {
      if (publication.getId() == null || publication.getId().equals("")) {
        return null;
      } else {
        return new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, publication.getId());
      }
    } else {
      throw new InvalidArgumentException("Unknown biopax database: " + publication.getDb());
    }
  }

  /**
   * Returns list of {@link MiriamData} that are referenced in {@link BiopaxData}
   * with identifier given in biopaxReference.
   * 
   * @param biopaxData
   *          {@link BiopaxData} where annotations are stored
   * @param biopaxReference
   *          list of references (to data in {@link BiopaxData}) that we want to
   *          convert into {@link MiriamData}
   * @return list of {@link MiriamData} that are referenced in {@link BiopaxData}
   *         with identifier given in biopaxReference.
   */
  public Collection<MiriamData> getMiriamData(BiopaxData biopaxData, Collection<String> biopaxReference) {
    List<MiriamData> result = new ArrayList<>();
    for (String string : biopaxReference) {
      if (biopaxData != null) {
        BiopaxPublication bp = biopaxData.getPublicationByReference(string);
        if (bp != null) {
          MiriamData md = createMiriamData(bp);
          if (md != null) {
            result.add(md);
          } else {
            logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, "BiopaxReference", string, getMapName()),
                "Publication is invalid.");
          }
        } else {
          logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, "BiopaxReference", string, getMapName()),
              "Biopax publication doesn't exist.");
        }
      } else {
        logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, "BiopaxReference", string, getMapName()),
            "Invalid BioPax reference.");
      }
    }
    return result;
  }

  public String getMapName() {
    return mapName;
  }

  public void setMapName(String mapName) {
    this.mapName = mapName;
  }
}
