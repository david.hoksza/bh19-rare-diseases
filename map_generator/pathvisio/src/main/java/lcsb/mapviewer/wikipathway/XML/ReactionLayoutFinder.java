package lcsb.mapviewer.wikipathway.XML;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.ReactionCellDesignerConverter;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.wikipathway.model.Edge;
import lcsb.mapviewer.wikipathway.model.Interaction;
import lcsb.mapviewer.wikipathway.utils.Geo;

class ReactionLayoutFinder {

  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();

  private LineTransformation lt = new LineTransformation();
  private PointTransformation pt = new PointTransformation();

  private Interaction interaction;

  /**
   * Return coordinates where all {@link Reactant} should end, all
   * {@link Modifier} should end, and all {@link Product} should start.
   * 
   * @param interaction
   *          GPML {@link Interaction}
   * @return map consisted of three entries for classes: {@link Product},
   *         {@link Reactant}, {@link Modifier}
   */
  public Map<Class<?>, Point2D> getNodeStartPoints(Interaction interaction) {
    this.interaction = interaction;
    List<Pair<Class<?>, Point2D>> possiblePoints = getPointsInOrder(interaction);
    Point2D modifierPoint = getModifierPoint(possiblePoints);
    Point2D reactantPoint = getReactantPoint(possiblePoints, modifierPoint);
    Point2D productPoint = getProductPoint(possiblePoints, modifierPoint);

    Map<Class<?>, Point2D> result = new HashMap<>();
    result.put(Reactant.class, reactantPoint);
    result.put(Product.class, productPoint);
    result.put(Modifier.class, modifierPoint);

    return result;
  }

  public Map<Class<?>, PolylineData> getNodeLines(Interaction interaction) {
    Map<Class<?>, Point2D> points = getNodeStartPoints(interaction);
    

    Map<Class<?>, PolylineData> result = new HashMap<>();
    PolylineData reactantLine = getSubline(interaction.getLine(), interaction.getLine().getBeginPoint(),
        points.get(Reactant.class));
    PolylineData inputReactionLine = getSubline(interaction.getLine(), points.get(Reactant.class),
        points.get(Modifier.class));
    inputReactionLine.trimEnd(ReactionCellDesignerConverter.RECT_SIZE / 2);

    PolylineData outputReactionLine = getSubline(interaction.getLine(),
        points.get(Modifier.class), points.get(Product.class));
    
    outputReactionLine.trimBegin(ReactionCellDesignerConverter.RECT_SIZE / 2);
    PolylineData productLine = getSubline(interaction.getLine(),
        points.get(Product.class), interaction.getLine().getEndPoint());
    PolylineData modifierLine = new PolylineData(pt.copyPoint(inputReactionLine.getEndPoint()),
        pt.copyPoint(outputReactionLine.getBeginPoint()));
    modifierLine.setColor(interaction.getColor());
    modifierLine.setWidth(productLine.getWidth());

    result.put(Reactant.class, reactantLine);
    result.put(Product.class, productLine);
    result.put(Modifier.class, modifierLine);
    result.put(AndOperator.class, inputReactionLine);
    result.put(SplitOperator.class, outputReactionLine);

    return result;
  }

  private List<Pair<Class<?>, Point2D>> getPointsInOrder(Interaction interaction) {
    PolylineData pd = interaction.getLine();
    List<Pair<Class<?>, Point2D>> possiblePoints = new ArrayList<>();
    for (Edge e : interaction.getReactants()) {
      if (pointOnPolyline(pd, e.getLine().getEndPoint())) {
        possiblePoints.add(new Pair<>(Reactant.class, e.getLine().getEndPoint()));
      } else if (pointOnPolyline(pd, e.getLine().getBeginPoint())) {
        possiblePoints.add(new Pair<>(Reactant.class, e.getLine().getBeginPoint()));
      }
    }
    for (Edge e : interaction.getProducts()) {
      if (pointOnPolyline(pd, e.getLine().getEndPoint())) {
        possiblePoints.add(new Pair<>(Product.class, e.getLine().getEndPoint()));
      } else if (pointOnPolyline(pd, e.getLine().getBeginPoint())) {
        possiblePoints.add(new Pair<>(Product.class, e.getLine().getBeginPoint()));
      }
    }
    for (Edge e : interaction.getModifiers()) {
      if (pointOnPolyline(pd, e.getLine().getEndPoint())) {
        possiblePoints.add(new Pair<>(Modifier.class, e.getLine().getEndPoint()));
      } else if (pointOnPolyline(pd, e.getLine().getBeginPoint())) {
        possiblePoints.add(new Pair<>(Modifier.class, e.getLine().getBeginPoint()));
      }
    }
    Comparator<Pair<Class<?>, Point2D>> comparator = new Comparator<Pair<Class<?>, Point2D>>() {

      @Override
      public int compare(Pair<Class<?>, Point2D> o1, Pair<Class<?>, Point2D> o2) {
        double dist1 = distanceFromPolylineStart(pd, o1.getRight());
        double dist2 = distanceFromPolylineStart(pd, o2.getRight());
        return new DoubleComparator(Configuration.EPSILON).compare(dist1, dist2);
      }
    };

    Collections.sort(possiblePoints, comparator);
    return possiblePoints;
  }

  private Point2D getProductPoint(List<Pair<Class<?>, Point2D>> possiblePoints, Point2D modifierPoint) {
    Point2D result = null;
    for (Pair<Class<?>, Point2D> pair : possiblePoints) {
      if (pair.getLeft().equals(Product.class)) {
        if (distanceFromPolylineStart(interaction.getLine(),
            modifierPoint) < distanceFromPolylineStart(interaction.getLine(), pair.getRight())) {
          result = pair.getRight();
          break;
        }
      }
    }
    if (result == null) {
      for (Point2D point : interaction.getLine().getPoints()) {
        if (distanceFromPolylineStart(interaction.getLine(),
            modifierPoint) < distanceFromPolylineStart(interaction.getLine(), point)) {
          double x = modifierPoint.getX() + (point.getX() - modifierPoint.getX()) / 2;
          double y = modifierPoint.getY() + (point.getY() - modifierPoint.getY()) / 2;
          result = new Point2D.Double(x, y);
          break;
        }
      }
    }
    return result;
  }

  private Point2D getReactantPoint(List<Pair<Class<?>, Point2D>> possiblePoints, Point2D modifierPoint) {
    Point2D result = null;
    for (Pair<Class<?>, Point2D> pair : possiblePoints) {
      if (pair.getLeft().equals(Reactant.class)) {
        if (distanceFromPolylineStart(interaction.getLine(),
            modifierPoint) > distanceFromPolylineStart(interaction.getLine(), pair.getRight())) {
          result = pair.getRight();
        }
      }
    }
    if (result == null) {
      for (Point2D point : interaction.getLine().getPoints()) {
        if (distanceFromPolylineStart(interaction.getLine(),
            modifierPoint) > distanceFromPolylineStart(interaction.getLine(), point)) {
          double x = modifierPoint.getX() + (point.getX() - modifierPoint.getX()) / 2;
          double y = modifierPoint.getY() + (point.getY() - modifierPoint.getY()) / 2;
          result = new Point2D.Double(x, y);
        }
      }
    }
    return result;
  }

  private Point2D getModifierPoint(List<Pair<Class<?>, Point2D>> points) {
    List<Pair<Class<?>, Point2D>> possiblePoints = new ArrayList<>(points);
    int productPoints = 0;
    int reactantPoints = 0;
    for (Pair<Class<?>, Point2D> pair : possiblePoints) {
      if (pair.getLeft().equals(Product.class)) {
        productPoints++;
      } else if (pair.getLeft().equals(Reactant.class)) {
        reactantPoints++;
      }
    }
    int countedReactants = 0;
    int countedProducts = 0;
    int score = Integer.MIN_VALUE;
    Point2D point = null;
    for (Pair<Class<?>, Point2D> pair : possiblePoints) {
      if (pair.getLeft().equals(Product.class)) {
        countedProducts++;
      } else if (pair.getLeft().equals(Reactant.class)) {
        countedReactants++;
      } else if (pair.getLeft().equals(Modifier.class)) {
        int currentScore = countedReactants + (productPoints - countedProducts);
        if (point == null || score < currentScore) {
          score = currentScore;
          point = pair.getRight();
        }
      }
    }

    if (point == null) {
      possiblePoints.add(0, new Pair<Class<?>, Point2D>(null, interaction.getLine().getBeginPoint()));
      possiblePoints.add(new Pair<Class<?>, Point2D>(null, interaction.getLine().getEndPoint()));
      countedReactants = 0;
      countedProducts = 0;

      Point2D previousPoint = null;
      for (Pair<Class<?>, Point2D> pair : possiblePoints) {
        if (previousPoint != null) {
          int currentScore = countedReactants + (productPoints - countedProducts);
          if (point == null || score < currentScore) {
            score = currentScore;
            if (reactantPoints == 0) { // shift to most right possible line when there are no other reactants
              point = getRightCenterPoint(interaction.getLine(), previousPoint, pair.getRight());
            } else if (productPoints == 0) {
              point = getLeftCenterPoint(interaction.getLine(), previousPoint, pair.getRight());
            } else {
              point = getCenterPoint(interaction.getLine(), previousPoint, pair.getRight());
            }
          }
        }
        if (pair.getLeft() == Product.class) {
          countedProducts++;
        } else if (pair.getLeft() == Reactant.class) {
          countedReactants++;
        }
        previousPoint = pair.getRight();
      }
    }
    return point;
  }

  private Point2D getCenterPoint(PolylineData originalPolylineData, Point2D startPoint, Point2D endPoint) {
    PolylineData pd = getSubline(originalPolylineData, startPoint, endPoint);
    return getCenterPoint(pd);
  }

  private Point2D getLeftCenterPoint(PolylineData originalPolylineData, Point2D startPoint, Point2D endPoint) {
    PolylineData pd = getSubline(originalPolylineData, startPoint, endPoint);
    double x = pd.getPoints().get(0).getX() + (pd.getPoints().get(1).getX() - pd.getPoints().get(0).getX()) / 2;
    double y = pd.getPoints().get(0).getY() + (pd.getPoints().get(1).getY() - pd.getPoints().get(0).getY()) / 2;
    return new Point2D.Double(x, y);
  }

  private Point2D getRightCenterPoint(PolylineData originalPolylineData, Point2D startPoint, Point2D endPoint) {
    PolylineData pd = getSubline(originalPolylineData, startPoint, endPoint);
    int points = pd.getPoints().size();
    double x = pd.getPoints().get(points - 2).getX()
        + (pd.getPoints().get(points - 1).getX() - pd.getPoints().get(points - 2).getX()) / 2;
    double y = pd.getPoints().get(points - 2).getY()
        + (pd.getPoints().get(points - 1).getY() - pd.getPoints().get(points - 2).getY()) / 2;
    return new Point2D.Double(x, y);
  }

  private PolylineData getSubline(PolylineData originalPolylineData, Point2D startPoint, Point2D endPoint) {
    int start = 0;
    int end = originalPolylineData.getPoints().size() - 1;
    for (int i = 0; i < originalPolylineData.getPoints().size(); i++) {
      if (distanceFromPolylineStart(originalPolylineData, startPoint) > distanceFromPolylineStart(originalPolylineData,
          originalPolylineData.getPoints().get(i))) {
        start = i;
      }
      if (distanceFromPolylineStart(originalPolylineData, endPoint) < distanceFromPolylineStart(originalPolylineData,
          originalPolylineData.getPoints().get(i))) {
        end = Math.min(end, i);
      }
    }
    PolylineData result = originalPolylineData.getSubline(start, end + 1);
    result.setStartPoint(pt.copyPoint(startPoint));
    result.setEndPoint(pt.copyPoint(endPoint));
    return PolylineDataFactory.removeCollinearPoints(result);
  }

  private Point2D getCenterPoint(PolylineData pd) {
    List<Line2D> lines = pd.getLines();
    double lenght = 0.0;
    for (Line2D line : lines) {
      lenght += Geo.lineLen(line);
    }
    double tmp = 0.0;
    for (Line2D line : lines) {
      if (tmp + Geo.lineLen(line) > lenght / 2) {
        double x = line.getX1() + (line.getX2() - line.getX1()) / 2;
        double y = line.getY1() + (line.getY2() - line.getY1()) / 2;
        return new Point2D.Double(x, y);
      } else {
        tmp += Geo.lineLen(line);
      }
    }
    return pt.copyPoint(pd.getEndPoint());
  }

  double distanceFromPolylineStart(PolylineData line, Point2D point) {
    double distance = 0;
    for (Line2D l : line.getLines()) {
      if (lt.distBetweenPointAndLineSegment(l, point) <= Configuration.EPSILON) {
        return distance + point.distance(l.getP1());
      }
      distance += l.getP1().distance(l.getP2());
    }
    throw new InvalidArgumentException("Point doesn't lay on the line");
  }

  private boolean pointOnPolyline(PolylineData line, Point2D point) {
    for (Line2D l : line.getLines()) {
      if (lt.distBetweenPointAndLineSegment(l, point) <= Configuration.EPSILON) {
        return true;
      }
    }
    return false;
  }
}
