package lcsb.mapviewer.wikipathway.XML;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.wikipathway.model.*;

/**
 * Parser class that creates {@link State} objects from Xml {@link Element node}
 * .
 * 
 * @author Piotr Gawron
 *
 */
public class StateParser extends ElementGpmlParser<State> {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger(StateParser.class);

  /**
   * Parser used for extracting {@link lcsb.mapviewer.model.map.MiriamData
   * references} from GPML model.
   */
  private ReferenceParser referenceParser;

  public StateParser(String mapName) {
    super(mapName);
    referenceParser = new ReferenceParser(mapName);
  }

  @Override
  public State parse(Element eElement) throws ConverterException {
    if (!eElement.getNodeName().equals("State")) {
      throw new InvalidArgumentException(StateParser.class.getSimpleName() + " can parse only State xml nodes");
    }
    State state = new State(eElement.getAttribute("GraphId"), getMapName());
    for (Pair<String, String> entry : getAttributes(eElement)) {
      switch (entry.getLeft()) {
      case ("GraphId"):
        break;
      case ("TextLabel"):
        try {
          GpmlModificationType type = GpmlModificationType.getByGpmlName(entry.getRight());
          state.setType(type.getCorrespondingModificationState());
        } catch (UnknownTypeException e) {
          try {
            GpmlStateType type = GpmlStateType.getByGpmlName(entry.getRight());
            state.setStructuralState(type.getStringRepresenation());
          } catch (UnknownTypeException e2) {
            throw new ConverterException("Unknown state type: " + entry.getRight());
          }
        }
        break;
      case ("GraphRef"):
        state.setGraphRef(entry.getRight());
        break;
      default:
        logger.warn("Unknown attribute of " + eElement.getNodeName() + " node: " + entry.getLeft());
        break;
      }
    }

    NodeList tmpList = eElement.getChildNodes();
    for (int j = 0; j < tmpList.getLength(); j++) {
      Node tmpNode = tmpList.item(j);
      if (tmpNode.getNodeType() == Node.ELEMENT_NODE) {
        Element eTmp = (Element) tmpNode;
        switch (eTmp.getNodeName()) {
        case ("Comment"):
          state.addComment(eTmp.getTextContent());
          break;
        case ("Graphics"):
          parseGraphics(eTmp, state);
          break;
        case ("Attribute"):
          parseAttribute(eTmp, state);
          break;
        case ("BiopaxRef"):
          state.addBiopaxReference(eTmp.getTextContent());
          break;
        case ("Xref"):
          MiriamData md = referenceParser.parse(eTmp);
          if (md != null) {
            state.addReference(md);
          }
          break;
        default:
          logger.warn("Unknown sub-node of " + eElement.getNodeName() + " node: " + eTmp.getNodeName());
          break;
        }
      }
    }
    return state;
  }

  @Override
  public String toXml(State node) throws ConverterException {
    throw new NotImplementedException();
  }

  @Override
  public String toXml(Collection<State> list) throws ConverterException {
    throw new NotImplementedException();
  }

  /**
   * Parse graphics xml node in the state node.
   *
   * @param eTmp
   *          xml node with graphics
   * @param state
   *          state where data should be added
   * @throws UnknownTypeException
   *           thrown when some types in the xml node are unknown
   */
  private void parseGraphics(Element eTmp, State state) throws UnknownTypeException {
    for (Pair<String, String> entry : getAttributes(eTmp)) {
      switch (entry.getLeft()) {
      case ("RelX"):
        state.setRelX(Double.valueOf(entry.getRight()));
        break;
      case ("RelY"):
        state.setRelY(Double.valueOf(entry.getRight()));
        break;
      case ("Width"):
        state.setWidth(Double.valueOf(entry.getRight()));
        break;
      case ("Height"):
        state.setHeight(Double.valueOf(entry.getRight()));
        break;
      case ("ShapeType"):
        state.setShape(entry.getRight());
        break;
      case ("FillColor"):
        state.setFillColor(hexStringToColor(entry.getRight()));
        break;
      default:
        logger.warn("Unknown attribute of " + eTmp.getNodeName() + " node: " + entry.getLeft());
        break;
      }
    }
  }

  /**
   * Method that parses {@link State} xml attribute.
   *
   * @param eTmp
   *          xml node with attribute
   * @param state
   *          state where data should be added
   */
  private void parseAttribute(Element eTmp, State state) {
    String key = eTmp.getAttribute("Key");
    String value = eTmp.getAttribute("Value");
    switch (key) {
    default:
      logger.warn(state.getLogMarker() + "Unknown attribute of node. Key:" + key + "; value: " + value);
      break;
    }
  }

}
