package lcsb.mapviewer.wikipathway.model;

import lcsb.mapviewer.model.map.MiriamType;

/**
 * This enum lists mapping between strings representing annotations in gpml into
 * {@link MiriamType}.
 * 
 * @author Piotr Gawron
 *
 */
public enum ReferenceMapping {

  /**
   * {@link MiriamType#CAS}.
   */
  CAS("CAS", MiriamType.CAS),

  /**
   * {@link MiriamType#CHEBI}.
   */
  CHEBI("Chebi", MiriamType.CHEBI),

  /**
   * {@link MiriamType#CHEMBL_COMPOUND}.
   */
  CHEMBL_COMPOUND(null, MiriamType.CHEMBL_COMPOUND),

  /**
   * {@link MiriamType#CHEMBL_TARGET}.
   */
  CHEMBL_TARGET(null, MiriamType.CHEMBL_TARGET),

  /**
   * {@link MiriamType#CHEMSPIDER}.
   */
  CHEMSPIDER("ChemSpider", MiriamType.CHEMSPIDER),

  /**
   * {@link MiriamType#EC}.
   */
  EC("Enzyme Nomenclature", MiriamType.EC),

  /**
   * {@link MiriamType#ENSEMBL}.
   */
  ENSEMBL("Ensembl", MiriamType.ENSEMBL),

  /**
   * {@link MiriamType#ENTREZ}.
   */
  ENTREZ("Entrez Gene", MiriamType.ENTREZ),

  /**
   * Alternative name for {@link MiriamType#ENTREZ}.
   */
  GEN_BANK("GenBank", MiriamType.ENTREZ),

  /**
   * {@link MiriamType#GO}.
   */
  GO("GeneOntology", MiriamType.GO),

  /**
   * {@link MiriamType#HGNC}.
   */
  HGNC("HGNC Accession number", MiriamType.HGNC),

  /**
   * {@link MiriamType#HGNC_SYMBOL}.
   */
  HGNC_SYMBOL("HGNC", MiriamType.HGNC_SYMBOL),

  /**
   * {@link MiriamType#HMDB}.
   */
  HMDB("HMDB", MiriamType.HMDB),

  /**
   * {@link MiriamType#INTERPRO}.
   */
  INTERPRO("InterPro", MiriamType.INTERPRO),

  /**
   * {@link MiriamType#MESH_2012}.
   */
  MESH_2012(null, MiriamType.MESH_2012),

  /**
   * {@link MiriamType#MI_RBASE_SEQUENCE}.
   */
  MI_RBASE_SEQUENCE2("miRBase", MiriamType.MI_R_BASE_SEQUENCE),

  /**
   * {@link MiriamType#KEGG_COMPOUND}.
   */
  KEGG_COMPOUND("KEGG Compound", MiriamType.KEGG_COMPOUND),

  /**
   * {@link MiriamType#KEGG_GENES}.
   */
  KEGG_GENES("KEGG Genes", MiriamType.KEGG_GENES),

  /**
   * {@link MiriamType#KEGG_ORTHOLOGY}.
   */
  KEGG_ORTHOLOGY("Kegg ortholog", MiriamType.KEGG_ORTHOLOGY),

  /**
   * {@link MiriamType#KEGG_PATHWAY}.
   */
  KEGG_PATHWAY("KEGG Pathway", MiriamType.KEGG_PATHWAY),

  /**
   * {@link MiriamType#KEGG_REACTION}.
   */
  KEGG_REACTION("KEGG Reaction", MiriamType.KEGG_REACTION),

  /**
   * {@link MiriamType#MGD}.
   */
  MGD("MGI", MiriamType.MGD),

  /**
   * {@link MiriamType#MI_RBASE_SEQUENCE}.
   */
  MI_RBASE_SEQUENCE("miRBase Sequence", MiriamType.MI_R_BASE_SEQUENCE),

  /**
   * {@link MiriamType#PANTHER}.
   */
  PANTHER(null, MiriamType.PANTHER),

  /**
   * {@link MiriamType#PFAM}.
   */
  PFAM("Pfam", MiriamType.PFAM),

  /**
   * {@link MiriamType#PHARM}.
   */
  PHARM("PharmGKB Pathways", MiriamType.PHARM),

  /**
   * {@link MiriamType#PUBCHEM}.
   */
  PUBCHEM("PubChem-compound", MiriamType.PUBCHEM),

  /**
   * {@link MiriamType#PUBCHEM_SUBSTANCE}.
   */
  PUBCHEM_SUBSTANCE("PubChem-substance", MiriamType.PUBCHEM_SUBSTANCE),

  /**
   * {@link MiriamType#REACTOME}.
   */
  REACTOME("Reactome", MiriamType.REACTOME),

  /**
   * {@link MiriamType#REFSEQ}.
   */
  REFSEQ("RefSeq", MiriamType.REFSEQ),

  /**
   * {@link MiriamType#SGD }.
   */
  SGD("SGD", MiriamType.SGD),

  /**
   * {@link MiriamType#TAIR_LOCUS}.
   */
  TAIR_LOCUS("TAIR", MiriamType.TAIR_LOCUS),

  /**
   * {@link MiriamType#UNIPROT}.
   */
  UNIPROT("Uniprot-TrEMBL", MiriamType.UNIPROT),

  /**
   * {@link MiriamType#UNIPROT_ISOFORM}.
   */
  UNIPROT_ISOFORM(null, MiriamType.UNIPROT_ISOFORM),

  /**
   * {@link MiriamType#WIKIPATHWAYS}.
   */
  WIKIPATHWAYS("Wikipathways", MiriamType.WIKIPATHWAYS),

  /**
   * {@link MiriamType#WIKIPEDIA}.
   */
  WIKIPEDIA("Wikipedia", MiriamType.WIKIPEDIA);

  /**
   * Gpml string representing specific database type.
   */
  private String gpmlString;

  /**
   * {@link MiriamType} corresponding to {@link #gpmlString}.
   */
  private MiriamType type;

  /**
   * Default constructor.
   * 
   * @param gpmlString
   *          {@link #getGpmlString()}
   * @param type
   *          {@link #type}
   */
  ReferenceMapping(String gpmlString, MiriamType type) {
    this.type = type;
    this.gpmlString = gpmlString;
  }

  /**
   * Returns {@link ReferenceMapping mapping} to {@link MiriamType} that should be
   * used for gpml resource type.
   *
   * @param gpmlString
   *          {@link #gpmlString}
   * @return {@link ReferenceMapping mapping} to {@link MiriamType} that should be
   *         used for gpml resource type
   */
  public static ReferenceMapping getMappingByGpmlString(String gpmlString) {
    for (ReferenceMapping mm : values()) {
      if (mm.getGpmlString() != null && mm.getGpmlString().equals(gpmlString)) {
        return mm;
      }
    }
    return null;
  }

  /**
   * Returns {@link ReferenceMapping mapping} that should be used for
   * {@link #gpmlString gpml string} representing type of annotation.
   *
   * @param dataType
   *          {@link #type}
   *
   * @return {@link ReferenceMapping mapping} that should be used for
   *         {@link #gpmlString gpml string} representing type of annotation
   */
  public static ReferenceMapping getMappingByMiriamType(MiriamType dataType) {
    for (ReferenceMapping mm : values()) {
      if (mm.getType().equals(dataType)) {
        return mm;
      }
    }
    return null;
  }

  /**
   * @return the gpmlString
   * @see #gpmlString
   */
  public String getGpmlString() {
    return gpmlString;
  }

  /**
   * @return the type
   * @see #type
   */
  public MiriamType getType() {
    return type;
  }

}
