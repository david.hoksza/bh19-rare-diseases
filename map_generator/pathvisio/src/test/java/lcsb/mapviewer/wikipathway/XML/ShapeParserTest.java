package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.*;

import java.awt.*;

import org.junit.After;
import org.junit.Test;
import org.w3c.dom.Element;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.Shape;

public class ShapeParserTest extends WikipathwaysTestFunctions {
  ShapeParser parser = new ShapeParser("mapName");

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseArc() throws Exception {
    Element node = fileToNode("testFiles/elements/arc.xml");
    Shape shape = parser.parse(node);
    assertNotNull(shape);

    assertEquals("arc l", shape.getTextLabel());
    assertEquals("d3af4", shape.getGraphId());
    assertEquals((Integer) 16384, shape.getzOrder());
    assertEquals((Double) 10.0, shape.getFontSize());
    assertEquals("Middle", shape.getvAlign());
    assertEquals("Arc", shape.getShape());
    assertEquals((Double) 0.0, shape.getRotation());
    assertTrue(shape.getComments().contains("arc c"));
    assertTrue(shape.getComments().contains("Type your comment here"));

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseBrace() throws Exception {
    Element node = fileToNode("testFiles/elements/brace.xml");
    Shape shape = parser.parse(node);
    assertNotNull(shape);

    assertEquals("dfdfs fds ", shape.getTextLabel());
    assertEquals("eb55e", shape.getGraphId());
    assertEquals(1, shape.getBiopaxReference().size());
    assertEquals("d04", shape.getBiopaxReference().get(0));

    assertEquals((Integer) 16384, shape.getzOrder());
    assertEquals((Double) 10.0, shape.getFontSize());
    assertEquals("Middle", shape.getvAlign());
    assertEquals("Brace", shape.getShape());
    assertEquals((Double) 0.44830711094422787, shape.getRotation(), Configuration.EPSILON);
    assertTrue(shape.getComments().contains("1"));
    assertTrue(shape.getComments().contains("Type your comment here"));

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseCompartment() throws Exception {
    Element node = fileToNode("testFiles/elements/compartment.xml");
    Shape shape = parser.parse(node);
    assertNotNull(shape);

    assertEquals("compartment title", shape.getTextLabel());
    assertEquals("c5869", shape.getGraphId());
    assertEquals(1, shape.getBiopaxReference().size());
    assertEquals("b80", shape.getBiopaxReference().get(0));

    assertEquals((Integer) 16384, shape.getzOrder());
    assertEquals((Double) 10.0, shape.getFontSize());
    assertEquals((Double) 3.0, shape.getLineThickness());
    assertEquals(LineType.DOUBLE, shape.getLineType());
    assertEquals("Middle", shape.getvAlign());
    assertEquals("RoundedRectangle", shape.getShape());
    assertEquals((Double) 0.0, shape.getRotation(), Configuration.EPSILON);
    assertTrue(shape.getComments().contains("compartment comment here"));
    assertTrue(shape.getComments().contains("Type your comment here"));
    assertTrue(shape.isCompartment());
    assertEquals(Color.LIGHT_GRAY, shape.getColor());

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseDegraded() throws Exception {
    Element node = fileToNode("testFiles/elements/degraded.xml");
    Shape shape = parser.parse(node);
    assertNotNull(shape);

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseHex() throws Exception {
    Element node = fileToNode("testFiles/elements/hex.xml");
    Shape shape = parser.parse(node);
    assertNotNull(shape);

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseOval() throws Exception {
    Element node = fileToNode("testFiles/elements/oval.xml");
    Shape shape = parser.parse(node);
    assertNotNull(shape);

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParsePent() throws Exception {
    Element node = fileToNode("testFiles/elements/pent.xml");
    Shape shape = parser.parse(node);
    assertNotNull(shape);

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseRect() throws Exception {
    Element node = fileToNode("testFiles/elements/rect.xml");
    Shape shape = parser.parse(node);
    assertNotNull(shape);

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseRoundedREct() throws Exception {
    Element node = fileToNode("testFiles/elements/rounded_rect.xml");
    Shape shape = parser.parse(node);
    assertNotNull(shape);

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseTriangle() throws Exception {
    Element node = fileToNode("testFiles/elements/triangle.xml");
    Shape shape = parser.parse(node);
    assertNotNull(shape);

    assertEquals(0, getWarnings().size());
  }

}
