package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.CALLS_REAL_METHODS;

import java.awt.Color;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

public class ElementGpmlParserTest extends WikipathwaysTestFunctions {

  ElementGpmlParser<?> parser = Mockito.mock(ElementGpmlParser.class, CALLS_REAL_METHODS);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseTransparentColor() {
    Color color = parser.hexStringToColor("Transparent");
    assertNotNull(color);
  }

}
