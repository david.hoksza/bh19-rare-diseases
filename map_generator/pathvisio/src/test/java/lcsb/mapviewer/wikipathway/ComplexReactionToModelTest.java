package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.*;

import java.awt.Color;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.wikipathway.XML.GPMLToModel;

public class ComplexReactionToModelTest extends WikipathwaysTestFunctions {

  static Logger logger = LogManager.getLogger(ComplexReactionToModelTest.class);

  @Test
  public void ComplexReactionTest() throws Exception {
    String fileName = "testFiles/reactions/two_segment_reaction.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, reaction.getReactants().size());
    assertEquals(1, reaction.getProducts().size());

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void ComplexReactantReactionTest() throws Exception {
    String fileName = "testFiles/reactions/two_reactants_reaction.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(2, reaction.getReactants().size());
    assertEquals(1, reaction.getProducts().size());

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void ComplexSplitReactionTest() throws Exception {
    String fileName = "testFiles/reactions/split_two_segment_reaction.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, reaction.getReactants().size());
    assertEquals(2, reaction.getProducts().size());

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void problematicReactantTest() throws Exception {
    ModelComparator mc = new ModelComparator(1.0);

    String fileName = "testFiles/small/problematic_reactant.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    Model model2 = serializeModelOverCellDesignerParser(model1);
    assertEquals("File " + fileName + " different after transformation", 0, mc.compare(model1, model2));

    assertEquals(7, getWarnings().size());
  }

  @Test
  public void problematicReactant2Test() throws Exception {
    ModelComparator mc = new ModelComparator(1.0);

    String fileName = "testFiles/small/problematic_reactant_2.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    Model model2 = serializeModelOverCellDesignerParser(model1);
    assertEquals("File " + fileName + " different after transformation", 0, mc.compare(model1, model2));

    assertEquals(3, getWarnings().size());
  }

  @Test
  public void missingNodesInReactionTest() throws Exception {
    ModelComparator mc = new ModelComparator(1.0);

    String fileName = "testFiles/small/missing_nodes_in_reaction.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals("File " + fileName + " different after transformation", 0, mc.compare(model1, model2));

    assertEquals(4, getWarnings().size());
  }

  @Test
  public void testColorReaction() throws Exception {
    String fileName = "testFiles/small/color_reaction.gpml";
    Model model = new GPMLToModel().getModel(fileName);

    assertEquals(1, model.getReactions().size());

    Reaction reaction = model.getReactions().iterator().next();

    assertFalse(Color.BLACK.equals(reaction.getReactants().get(0).getLine().getColor()));

    Element redAlias = model.getElementByElementId("d9620");
    Element blackAlias = model.getElementByElementId("d046f");
    assertFalse(Color.BLACK.equals(redAlias.getFillColor()));
    assertTrue(Color.WHITE.equals(blackAlias.getFillColor()));

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testReactionWithTwoSegments() throws Exception {
    String fileName = "testFiles/small/color_reaction.gpml";
    Model model = new GPMLToModel().getModel(fileName);

    Reaction reaction = model.getReactions().iterator().next();

    int lineCount = 0;
    for (AbstractNode node : reaction.getNodes()) {
      lineCount += node.getLine().getLines().size();
    }

    assertEquals(3, lineCount);

    assertEquals(0, getWarnings().size());
  }

}
