package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.*;

import org.junit.*;
import org.w3c.dom.Document;

import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.biopax.BiopaxData;
import lcsb.mapviewer.wikipathway.model.biopax.BiopaxPublication;

public class BiopaxParserTest extends WikipathwaysTestFunctions {

  BiopaxParser parser ;

  @Before
  public void setUp() throws Exception {
    parser = new BiopaxParser("mapName");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParse() throws Exception {
    Document document = getXmlDocumentFromFile("testFiles/biopax/small.xml");
    BiopaxData data = parser.parse(document.getFirstChild());
    assertNotNull(data);
    assertEquals(2, data.getPublications().size());
    BiopaxPublication publication = data.getPublicationByReference("cf2");
    assertEquals("23456", publication.getId());
    assertEquals("PubMed", publication.getDb());
    assertEquals("[Radical resection of foci in tuberculosis of the shoulder joint].", publication.getTitle());
    assertEquals("Magy Traumatol Orthop Helyreallito Seb", publication.getSource());
    assertEquals("1977", publication.getYear());
    assertEquals("Udvarhelyi I", publication.getAuthors());
    assertEquals("cf2", publication.getReferenceId());
  }

  @Test
  public void testIncorrectPumeds() throws Exception {
    Document document = getXmlDocumentFromFile("testFiles/biopax/invalid_pubmed_reference.xml");
    BiopaxData data = parser.parse(document.getFirstChild());
    assertNotNull(data);
    assertTrue(getWarnings().size() > 0);
  }

}
