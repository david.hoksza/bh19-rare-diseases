package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.*;
import lcsb.mapviewer.wikipathway.XML.GPMLToModel;

public class ReactionGpmlToModelTest extends WikipathwaysTestFunctions {
  /**
   * Default class logger.
   */
  static Logger logger = LogManager.getLogger(ReactionGpmlToModelTest.class);

  private ModelComparator mc = new ModelComparator(1.0);

  @Test
  public void LineInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_line.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertTrue(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ArrowInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_arrow.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(0, getWarnings().size());
    assertTrue(reaction instanceof StateTransitionReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void DashedArrowInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_arrow.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(0, getWarnings().size());
    assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void DashedLineInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_line.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertTrue(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BindingInteractionTest() throws Exception {
    // invalid mim_binding (contains only one reactant)
    String fileName = "testFiles/reactions/interaction_mim_binding.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BindingInteractionTest2() throws Exception {
    // proper mim_binding (contains two reactants)
    String fileName = "testFiles/reactions/interaction_mim_binding2.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(getWarnings().toString(), 0, getWarnings().size());
    assertTrue(reaction instanceof HeterodimerAssociationReaction);
    assertFalse(reaction.isReversible());
    assertEquals(2, reaction.getReactants().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);
    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingLeftInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_branching_left.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertTrue(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingRightInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_branching_right.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertTrue(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CatalystInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_catalyst.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof PositiveInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CleavageInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_cleavage.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(0, getWarnings().size());
    assertTrue(reaction instanceof StateTransitionReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ConversionInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_conversion.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(0, getWarnings().size());
    assertTrue(reaction instanceof StateTransitionReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CovalentBondInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_covalent_bond.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertTrue(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void GapInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_gap.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertTrue(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void InhibitionInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_inhibition.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(0, getWarnings().size());
    assertTrue(reaction instanceof NegativeInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ModificationInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_modification.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof PositiveInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void NecessaryStimulationInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_necessary_stimulation.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(0, getWarnings().size());
    assertTrue(reaction instanceof ReducedPhysicalStimulationReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void StimulationInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_stimulation.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof ReducedPhysicalStimulationReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TranscriptionTranslationInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_transcription_translation.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof PositiveInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TbarTranslationInteractionTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_tbar.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof NegativeInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineAsInteractionTest2() throws Exception {
    // proper mim_binding (contains two reactants)
    String fileName = "testFiles/reactions/line_as_interaction.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertTrue(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BindingInteractionDashedTest() throws Exception {
    // invalid mim_binding (contains only one reactant)
    String fileName = "testFiles/reactions/interaction_dashed_mim_binding.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(0, getWarnings().size());
    assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingLeftInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_branching_left.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertTrue(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingRightInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_branching_right.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertTrue(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CatalystInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_catalyst.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CleavageInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_cleavage.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ConversionInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_conversion.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(0, getWarnings().size());
    assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CovalentBondInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_covalent_bond.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertTrue(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void GapInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_gap.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownTransitionReaction);
    assertTrue(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void InhibitionInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_inhibition.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownNegativeInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ModificationInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_modification.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(0, getWarnings().size());
    assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void NecessaryStimulationInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_necessary_stimulation.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void StimulationInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_stimulation.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TranscriptionTranslationInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_transcription_translation.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof PositiveInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TbarTranslationInteractionDashedTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_tbar.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    assertEquals(1, getWarnings().size());
    assertTrue(reaction instanceof UnknownNegativeInfluenceReaction);
    assertFalse(reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void lineLineCombinedReactionsTest() throws Exception {
    String fileName = "testFiles/reactions/line_line_combined_reaction.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(4, model1.getReactions().size());

    assertEquals(4, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

}
