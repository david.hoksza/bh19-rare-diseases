package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

public class BugTest extends WikipathwaysTestFunctions {
  Logger logger = LogManager.getLogger(BugTest.class);
  private ModelComparator mc = new ModelComparator(1.0);

  @Test
  public void testBug319() throws Exception {
    String fileName = "testFiles/bugs/error_319.gpml";
    FileInputStream fis = new FileInputStream(fileName);
    new GpmlParser().createGraph(fis);
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testBug328() throws Exception {
    String filename = "testFiles/bugs/error_328.gpml";
    Model model1 = new GPMLToModel().getModel(filename);

    assertEquals(7, getWarnings().size());

    int complexes = 0;
    for (Element alias : model1.getElements()) {
      if (alias instanceof Complex) {
        complexes++;
      }
    }
    assertEquals("Invalid number of complexes", 1, complexes);

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals("File " + filename + " different after transformation", 0, mc.compare(model1, model2));
  }

}
