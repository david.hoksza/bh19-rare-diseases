package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

public class ModelToGPMLTest extends WikipathwaysTestFunctions {
  Logger logger = LogManager.getLogger(ModelToGPMLTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() throws Exception {
    CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();
    Model model = cellDesignerXmlParser
        .createModel(new ConverterParams().filename("testFiles/other_full/GSTP1 subnetwork_220214.xml"));
    ModelToGPML parser = new ModelToGPML(model.getName());
    String xml = parser.getGPML(model);
    assertNotNull(xml);
    assertEquals(10, getWarnings().size());

    Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
    assertNotNull(model2);
  }

  @Test
  public void testAnnotations() throws Exception {

    Model model = new ModelFullIndexed(null);
    model.setWidth(1000);
    model.setHeight(1000);

    GenericProtein alias = createProtein();
    alias.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    alias.addMiriamData(new MiriamData(MiriamType.PUBMED, "123"));
    alias.addMiriamData(new MiriamData(MiriamType.PUBMED, "1234"));

    model.addElement(alias);

    ModelToGPML parser = new ModelToGPML(model.getName());
    String xml = parser.getGPML(model);
    assertNotNull(xml);
    assertEquals(0, getWarnings().size());

    Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));

    Element p = model2.getElementByElementId("sa");

    assertEquals(alias.getMiriamData().size(), p.getMiriamData().size());
    for (MiriamData md : p.getMiriamData()) {
      assertTrue(alias.getMiriamData().contains(md));
    }
  }

  private GenericProtein createProtein() {
    GenericProtein alias = new GenericProtein("sa");
    alias.setName("s2");
    alias.setWidth(10);
    alias.setHeight(10);
    alias.setX(1);
    alias.setY(1);
    alias.setZ(15);
    return alias;
  }
}
