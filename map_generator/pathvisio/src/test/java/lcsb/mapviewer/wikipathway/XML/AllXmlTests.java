package lcsb.mapviewer.wikipathway.XML;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BiopaxParserTest.class,
    DataNodeParserTest.class,
    BugTest.class,
    EdgeLineParserTest.class,
    EdgeParserTest.class,
    GpmlParserTest.class,
    LabelParserTest.class,
    ModelToGPMLTest.class,
    ModelContructorTest.class,
    ReactionLayoutFinderTest.class,
    ReferenceParserTest.class,
    ShapeParserTest.class,
    StateParserTest.class,
})
public class AllXmlTests {

}
