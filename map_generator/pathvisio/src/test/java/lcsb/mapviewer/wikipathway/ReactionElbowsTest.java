package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Line2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.wikipathway.XML.GPMLToModel;

public class ReactionElbowsTest extends WikipathwaysTestFunctions {
  private final static double EPSILON = 1e-6;

  /**
   * Default class logger.
   */
  static Logger logger = LogManager.getLogger(ReactionElbowsTest.class);

  private ModelComparator mc = new ModelComparator(1.0);

  @Test
  public void LineReactionNorthToNorth() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_n_n.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(4, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionNorthToEast() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_n_e.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(5, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionNorthToEast2() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_n_e_2.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(3, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionNorthToSouth() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_n_s.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(6, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionNorthToWest() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_n_w.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(5, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionNorthToWest2() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_n_w_2.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(3, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionEastToNorth() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_e_n.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(3, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionEastToNorth2() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_e_n_2.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(5, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionEastToEast() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_e_e.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(4, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionEastToSouth() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_e_s.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(5, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionEastToSouth2() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_e_s_2.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(3, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionEastToWest() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_e_w.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(4, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionSouthToNorth() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_s_n.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(4, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionSouthToEast() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_s_e.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(5, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionSouthToEast2() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_s_e_2.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(3, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionSouthToSouth() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_s_s.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(4, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionSouthToWest() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_s_w.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(3, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionSouthToWest2() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_s_w_2.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(5, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionWestToNorth() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_w_n.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(5, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionWestToNorth2() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_w_n_2.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(3, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionWestToEast() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_w_e.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(6, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionWestToSouth() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_w_s.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(5, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionWestToSouth2() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_w_s_2.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(3, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void LineReactionWestToWest() throws Exception {
    String fileName = "testFiles/elbow/elbow_line_w_w.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    int lines = 0;
    for (AbstractNode node : reaction.getNodes()) {
      for (Line2D line : node.getLine().getLines()) {
        assertTrue("Lines should be horizontal or vertical, but found: " + line.getP1() + " - " + line.getP2(),
            isHvLine(line));
        lines++;
      }
    }

    assertEquals(4, lines);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  private boolean isHvLine(Line2D line) {
    return Math.abs(line.getX1() - line.getX2()) < EPSILON || Math.abs(line.getY1() - line.getY2()) < EPSILON;
  }
}
