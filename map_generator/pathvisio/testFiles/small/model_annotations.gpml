<?xml version="1.0" encoding="UTF-8"?>
<Pathway xmlns="http://pathvisio.org/GPML/2013a" Name="Binding of RNA by Insulin-like Growth Factor-2 mRNA Binding Proteins (IGF2BPs/IMPs/VICKZs)" Data-Source="Reactome - http://www.reactome.org" Version="48" Author="May, B" Maintainer="May, B" Organism="Homo sapiens">
  <Comment Source="WikiPathways-description">Insulin-like Growth Factor-2 mRNA Binding Proteins (IGF2BPs) bind specific sets of RNA and regulate their translation, stability, and subcellular localization. IGF2BP1, IGF2BP2, and IGF2BP3 bind about 8400 protein-coding transcripts. The target RNAs contain the sequence motif CAUH (where H is A, U, or, C) and binding of IGFBPs increases the stability of the target RNAs.Original Pathway at Reactome: http://www.reactome.org/PathwayBrowser/#DB=gk_current&amp;FOCUS_SPECIES_ID=48887&amp;FOCUS_PATHWAY_ID=428359</Comment>
  <BiopaxRef>bd0</BiopaxRef>
  <BiopaxRef>c79</BiopaxRef>
  <BiopaxRef>d31</BiopaxRef>
  <BiopaxRef>edc</BiopaxRef>
  <BiopaxRef>cbc</BiopaxRef>
  <BiopaxRef>efd</BiopaxRef>
  <Biopax>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="d5c">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">12894594</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">CRD-BP: a c-Myc mRNA stabilizing protein with an oncofetal pattern of expression.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Ioannidis P, Mahaira L, Papadopoulou A, Teixeira MR, Heim S, Andersen JA, Evangelou E, Dafni U, Pandis N, Trangas T.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="cbc">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">15601260</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">VICKZ proteins: a multi-talented family of regulatory RNA-binding proteins.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Yisraeli JK.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="c9e">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">20080952</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ZBP1 recognition of beta-actin zipcode induces RNA looping.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Chao JA, Patskovsky Y, Patel V, Levy M, Almo SC, Singer RH.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="f4d">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">16306994</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Spatial regulation of beta-actin translation by Src-dependent phosphorylation of ZBP1.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Hüttelmaier S, Zenklusen D, Lederer M, Dictenberg J, Lorenz M, Meng X, Bassell GJ, Condeelis J, Singer RH.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="d31">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">16541107</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">RNA-binding IMPs promote cell adhesion and invadopodia formation.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Vikesaa J, Hansen TV, Jønson L, Borup R, Wewer UM, Christiansen J, Nielsen FC.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="adb">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">18618095</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Variants of CDKAL1 and IGF2BP2 affect first-phase insulin secretion during hyperglycaemic clamps.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Groenewoud MJ, Dekker JM, Fritsche A, Reiling E, Nijpels G, Heine RJ, Maassen JA, Machicao F, Schäfer SA, Häring HU, 't Hart LM, van Haeften TW.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="c79">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">9891060</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">A family of insulin-like growth factor II mRNA-binding proteins represses translation in late development.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Nielsen J, Christiansen J, Lykke-Andersen J, Johnsen AH, Wewer UM, Nielsen FC.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="b7b">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">16778892</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">CRD-BP mediates stabilization of betaTrCP1 and c-myc mRNA in response to beta-catenin signalling.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Noubissi FK, Elcheva I, Bhatia N, Shakoori A, Ougolkov A, Liu J, Minamoto T, Ross J, Fuchs SY, Spiegelman VS.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="edc">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">17652133</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">VICKZ proteins mediate cell migration via their RNA binding activity.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Oberman F, Rand K, Maizels Y, Rubinstein AM, Yisraeli JK.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="efd">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">20371350</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Transcriptome-wide identification of RNA-binding protein and microRNA target sites by PAR-CLIP.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Hafner M, Landthaler M, Burger L, Khorshid M, Hausser J, Berninger P, Rothballer A, Ascano M Jr, Jungkamp AC, Munschauer M, Ulrich A, Wardle GS, Dewell S, Zavolan M, Tuschl T.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="e62">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">12024010</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Regulation of c-myc mRNA decay by translational pausing in a coding region instability determinant.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Lemm I, Ross J.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="bd0">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">11713986</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">A family of IGF-II mRNA binding proteins (IMP) involved in RNA trafficking.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Nielsen FC, Nielsen J, Christiansen J.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="d36">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">19429674</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">IGF2 mRNA-binding protein 2: biological function and putative role in type 2 diabetes.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Christiansen J, Kolte AM, Hansen Tv, Nielsen FC.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="c4e">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">19029303</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Control of c-myc mRNA stability by IGF2BP1-associated cytoplasmic RNPs.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Weidensdorfer D, Stöhr N, Baude A, Lederer M, Köhn M, Schierhorn A, Buchmeier S, Wahle E, Hüttelmaier S.</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="af5">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">10875929</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">H19 RNA binds four molecules of insulin-like growth factor II mRNA-binding protein.</bp:TITLE>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Runge S, Nielsen FC, Nielsen J, Lykke-Andersen J, Wewer UM, Christiansen J.</bp:AUTHORS>
    </bp:PublicationXref>
  </Biopax>
</Pathway>

