package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;

@RunWith(Parameterized.class)
public class CellDesignerToMultiExportTest extends SbmlTestFunctions {

  static Logger logger = LogManager.getLogger(CellDesignerToMultiExportTest.class.getName());

  private Path filePath;

  public CellDesignerToMultiExportTest(Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/cd_for_multi")).forEach(fPath -> {
      if (Files.isRegularFile(fPath) && fPath.toString().endsWith(".xml")) {
        data.add(new Object[] { fPath });
      }
    });
    return data;
  }

  @Test
  public void createModelTest() throws Exception {
    Converter converter = new CellDesignerXmlParser();

    Model model = converter.createModel(new ConverterParams().filename(filePath.toString()));
    model.setName(null);

    SbmlExporter sbmlExporter = new SbmlExporter();
    SbmlParser sbmlParser = new SbmlParser();
    String xmlString = sbmlExporter.toXml(model);

    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = sbmlParser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
    model2.setName(null);

    assertNotNull(model2);
    ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(model, model2));
  }

}
