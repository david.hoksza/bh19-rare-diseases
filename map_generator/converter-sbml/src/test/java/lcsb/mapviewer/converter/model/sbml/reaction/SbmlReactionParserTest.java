package lcsb.mapviewer.converter.model.sbml.reaction;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.sbml.jsbml.ext.layout.*;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.modifier.*;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.*;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class SbmlReactionParserTest extends SbmlTestFunctions {
  Logger logger = LogManager.getLogger(SbmlReactionParserTest.class);
  SbmlParser parser = new SbmlParser();

  @Test
  public void testParseCatalysis() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/modifier/catalysis.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction.getModifiers().iterator().next() instanceof Catalysis);
  }

  @Test
  public void testParseInhibition() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/modifier/inhibition.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction.getModifiers().iterator().next() instanceof Inhibition);
  }

  @Test
  public void testParseUnknownCatalysis() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/modifier/unknown_catalysis.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction.getModifiers().iterator().next() instanceof UnknownCatalysis);
  }

  @Test
  public void testParseUnknownInhibition() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/modifier/unknown_inhibition.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction.getModifiers().iterator().next() instanceof UnknownInhibition);
  }

  @Test
  public void testParseStateTransition() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/state_transition.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof StateTransitionReaction);
  }

  @Test
  public void testParseTranscription() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/transcription.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof TranscriptionReaction);
  }

  @Test
  public void testParseTranslation() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/translation.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof TranslationReaction);
  }

  @Test
  public void testParseTransport() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/transport.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof TransportReaction);
  }

  @Test
  public void testParseKnownTransitionOmitted() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/known_transition_omitted.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof KnownTransitionOmittedReaction);
  }

  @Test
  public void testParseUnknownTransition() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/unknown_transition.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof UnknownTransitionReaction);
  }

  @Test
  public void testParseHeterodimerAssociation() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/heterodimer_association.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof HeterodimerAssociationReaction);
  }

  @Test
  public void testParseHeterodimerAssociationWithSingleReactant() throws Exception {
    Model model = parser
        .createModel(new ConverterParams()
            .filename("testFiles/small/reaction/heterodimer_association_with_single_reactant.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof Reaction);
  }

  @Test
  public void testParseDissociation() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/dissociation.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof DissociationReaction);
  }

  @Test
  public void testParseTruncation() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/truncation.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof TruncationReaction);
  }

  @Test
  public void testParsePositiveInfluence() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/positive_influence.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof PositiveInfluenceReaction);
  }

  @Test
  public void testParseUnknownPositiveInfluence() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/unknown_positive_influence.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue("Invalid class type: " + reaction.getClass(), reaction instanceof UnknownPositiveInfluenceReaction);
  }

  @Test
  public void testParseNegativeInfluence() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/negative_influence.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof NegativeInfluenceReaction);
  }

  @Test
  public void testParseUnknownNegativeInfluence() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/unknown_negative_influence.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof UnknownNegativeInfluenceReaction);
  }

  @Test
  public void testParseReactionWithoutReactant() throws Exception {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/reaction_without_reactant.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof Reaction);
  }

  @Test
  public void testParseReactionWithoutProduct() throws Exception {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/reaction_without_product.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof Reaction);
  }

  @Test
  public void testParseReactionKineticsWithDefaultCompartment() throws Exception {
    Model model = parser
        .createModel(new ConverterParams()
            .filename("testFiles/small/reaction/reaction_with_kinetics_including_default_compartment.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof Reaction);
  }

  @Test
  public void testParseReactionWithEmptyKinetics() throws Exception {
    Model model = parser
        .createModel(new ConverterParams()
            .filename("testFiles/small/reaction/reaction_with_empty_kinetics.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof Reaction);
  }

  @Test
  public void testParseReactionWithProductWithoutLayout() throws Exception {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/product_without_layout.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof Reaction);
  }

  @Test
  public void testParseReactionWithReactantWithoutLayout() throws Exception {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/reactant_without_layout.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof Reaction);
  }

  @Test
  public void testParseReactionWithZIndex() throws Exception {
    Model model = new ModelFullIndexed(null);
    GenericProtein p1 = createProtein();
    GenericProtein p2 = createProtein();
    Reaction reaction = createReaction(p1, p2);
    model.addElement(p1);
    model.addElement(p2);
    model.addReaction(reaction);
    Model model2 = getModelAfterSerializing(model);

    assertEquals(reaction.getZ(), model2.getReactions().iterator().next().getZ());
  }

  @Test
  public void testGetPointOnSegment() throws Exception {
    SbmlReactionParser parser = new SbmlReactionParser(new org.sbml.jsbml.Model(), null, null, null);
    CurveSegment segment = new LineSegment(3, 2);
    segment.setStart(new Point(12, 2, 0, 3, 2));
    segment.setEnd(new Point(2, 8, 0, 3, 2));
    Point p = parser.getPointOnSegment(segment, 0.4);

    assertEquals(8, p.getX(), Configuration.EPSILON);
    assertEquals(4.4, p.getY(), Configuration.EPSILON);
  }

  @Test
  public void testGetCurveFromEmptyBoundingBox() throws Exception {
    SbmlReactionParser sbmlReactionParser = new SbmlReactionParser(new org.sbml.jsbml.Model(), null, null, null);
    ReactionGlyph glyph = new ReactionGlyph(3, 2);
    BoundingBox box = new BoundingBox(3, 2);
    box.setPosition(new Point(1, 1, 1, 3, 2));
    box.setDimensions(new Dimensions(3, 2));
    glyph.setBoundingBox(box);

    Curve result = sbmlReactionParser.getCurve(glyph);
    assertNotNull(result);
    assertEquals(0, result.getCurveSegmentCount());
  }

  @Test
  public void testGetCurveFromNonEmptyBoundingBox() throws Exception {
    SbmlReactionParser sbmlReactionParser = new SbmlReactionParser(new org.sbml.jsbml.Model(), null, null, null);
    ReactionGlyph glyph = new ReactionGlyph(3, 2);
    BoundingBox box = new BoundingBox(3, 2);
    box.setPosition(new Point(1, 1, 1, 3, 2));
    box.setDimensions(new Dimensions(2, 2, 2, 3, 2));
    glyph.setBoundingBox(box);

    Curve result = sbmlReactionParser.getCurve(glyph);
    assertNotNull(result);
    assertEquals(1, result.getCurveSegmentCount());
  }

}
