package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.*;

import java.awt.*;

import org.junit.Test;
import org.mockito.Mockito;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.render.ColorDefinition;

import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class SbmlBioEntityExporterTest extends SbmlTestFunctions {

  @Test
  public void testEmptyGetRenderPlugin() {
    @SuppressWarnings("rawtypes")
    SbmlBioEntityExporter exporter = createMockExporter();
    Mockito.when(exporter.getLayout()).thenReturn(new Layout());

    assertNull(exporter.getRenderPlugin());
  }

  @Test
  public void testGetRenderPluginWithLayout() {
    SbmlBioEntityExporter<?, ?> exporter = createMockExporter();
    assertNotNull(exporter.getRenderPlugin());
  }

  @SuppressWarnings("rawtypes")
  private SbmlBioEntityExporter createMockExporter() {
    SbmlBioEntityExporter exporter = Mockito.mock(SbmlBioEntityExporter.class, Mockito.CALLS_REAL_METHODS);

    SBMLDocument doc = new SBMLDocument(3, 1);
    Model result = doc.createModel();

    SbmlExporter sbmlExporter = new SbmlExporter();
    Layout layout = sbmlExporter.createSbmlLayout(new ModelFullIndexed(null), result);

    Mockito.when(exporter.getLayout()).thenReturn(layout);
    return exporter;
  }

  @Test
  public void testAddColors() {
    SbmlBioEntityExporter<?, ?> exporter = createMockExporter();
    ColorDefinition c1 = exporter.getColorDefinition(Color.BLUE);
    ColorDefinition c2 = exporter.getColorDefinition(Color.BLUE);
    assertEquals(c1, c2);
  }

  @Test
  public void testCreateLineEndingStyle() {
    SbmlBioEntityExporter<?, ?> exporter = createMockExporter();
    for (ArrowType arrowType : ArrowType.values()) {
      assertNotNull("Line ending wasn't created for arrow type: " + arrowType,
          exporter.createLineEndingStyle(arrowType.name()));
    }
  }
}
