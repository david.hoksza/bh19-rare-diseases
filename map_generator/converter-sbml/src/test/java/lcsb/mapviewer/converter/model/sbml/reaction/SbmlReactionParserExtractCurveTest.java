package lcsb.mapviewer.converter.model.sbml.reaction;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.*;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.*;

@RunWith(Parameterized.class)
public class SbmlReactionParserExtractCurveTest extends SbmlTestFunctions {

  static Logger logger = LogManager.getLogger(SbmlReactionParserExtractCurveTest.class.getName());

  private ReactionGlyph glyph;
  private int length;

  private SbmlReactionParser sbmlReactionParser;
  private Curve curve;

  public SbmlReactionParserExtractCurveTest(String name, ReactionGlyph reactionGlyph, int length) {
    this.glyph = reactionGlyph;
    this.length = length;

    sbmlReactionParser = new SbmlReactionParser(new Model(3, 2), null, null, null);
  }

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<>();
    for (int segmentCount = 0; segmentCount < 7; segmentCount++) {
      ReactionGlyph reactionGlyph = new ReactionGlyph(3, 2);
      Curve curve = new Curve(3, 2);
      reactionGlyph.setCurve(curve);
      for (int i = 0; i < segmentCount; i++) {
        LineSegment curveSegment = new LineSegment(3, 2);
        int startX = ((i + 1) / 2) * 100;
        int startY = (i / 2) * 100;
        int endX = ((i + 2) / 2) * 100;
        int endY = ((i + 1) / 2) * 100;
        curveSegment.setStart(createPoint(startX, startY));
        curveSegment.setEnd(createPoint(endX, endY));
        curve.addCurveSegment(curveSegment);
      }
      data.add(new Object[] { "curve with " + segmentCount + " segments", reactionGlyph, segmentCount * 100 });
    }

    return data;
  }

  private static Point createPoint(int startX, int startY) {
    Point result = new Point(3, 2);
    result.setX(startX);
    result.setY(startY);
    return result;
  }

  @Before
  public final void setUp() throws Exception {
    curve = new Curve(glyph.getCurve());
  }

  @After
  public final void tearDown() throws Exception {
    glyph.setCurve(curve);
  }

  @Test
  public void testCenterLine() throws Exception {
    Reaction reaction = createReaction(createProtein(), createProtein());

    PolylineData line = sbmlReactionParser.extractCurve(glyph, reaction, null);
    assertNotNull("Extracted line cannot be null", line);
    assertEquals(length, line.length(), Configuration.EPSILON);
  }

  @Test
  public void testCenterLineWithInputOperator() throws Exception {
    Reaction reaction = createReaction(createProtein(), createProtein());
    reaction.addReactant(new Reactant(createProtein()));

    PolylineData centerLine = sbmlReactionParser.extractCurve(glyph, reaction, null);
    assertNotNull("Extracted center line cannot be null", centerLine);
    if (length > 0) {
      assertTrue(centerLine.length() > 0);
    }

    PolylineData inputLine = sbmlReactionParser.extractCurve(glyph, reaction, AndOperator.class);
    assertNotNull("Extracted input line cannot be null", inputLine);
    if (length > 0) {
      assertTrue(inputLine.length() > 0);
    }

    assertEquals(length, inputLine.length() + centerLine.length(), Configuration.EPSILON);
  }

  @Test
  public void testCenterLineWithOutputOperator() throws Exception {
    Reaction reaction = createReaction(createProtein(), createProtein());
    reaction.addProduct(new Product(createProtein()));

    PolylineData centerLine = sbmlReactionParser.extractCurve(glyph, reaction, null);
    assertNotNull("Extracted center line cannot be null", centerLine);
    if (length > 0) {
      assertTrue(centerLine.length() > 0);
    }

    PolylineData outputLine = sbmlReactionParser.extractCurve(glyph, reaction, SplitOperator.class);
    assertNotNull("Extracted output line cannot be null", outputLine);
    if (length > 0) {
      assertTrue(outputLine.length() > 0);
    }

    assertEquals(length, outputLine.length() + centerLine.length(), Configuration.EPSILON);
  }

  @Test
  public void testCenterLineWithInputAndOutputOperator() throws Exception {
    Reaction reaction = createReaction(createProtein(), createProtein());
    reaction.addProduct(new Product(createProtein()));
    reaction.addReactant(new Reactant(createProtein()));

    PolylineData centerLine = sbmlReactionParser.extractCurve(glyph, reaction, null);
    assertNotNull("Extracted center line cannot be null", centerLine);
    if (length > 0) {
      assertTrue(centerLine.length() > 0);
    }

    PolylineData inputLine = sbmlReactionParser.extractCurve(glyph, reaction, AndOperator.class);
    assertNotNull("Extracted input line cannot be null", inputLine);
    if (length > 0) {
      assertTrue(inputLine.length() > 0);
    }

    PolylineData outputLine = sbmlReactionParser.extractCurve(glyph, reaction, SplitOperator.class);
    assertNotNull("Extracted output line cannot be null", outputLine);
    if (length > 0) {
      assertTrue(outputLine.length() > 0);
    }

    assertEquals(length, outputLine.length() + inputLine.length() + centerLine.length(), Configuration.EPSILON);
  }

}
