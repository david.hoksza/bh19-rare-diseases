package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

@RunWith(Parameterized.class)
public class ElementPropertiesExportToLayoutTest extends SbmlTestFunctions {

  static Logger logger = LogManager.getLogger(ElementPropertiesExportToLayoutTest.class.getName());

  private Model model;

  public ElementPropertiesExportToLayoutTest(String propertyName, Model model) {
    this.model = model;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<>();

    data.add(createTestEntry("Empty", createProtein()));

    Species element = createProtein();
    element.setFontSize(40);
    data.add(createTestEntry("Font size", element));

    return data;
  }

  private static Object[] createTestEntry(String string, Element element) {
    Model result = new ModelFullIndexed(null);
    result.setIdModel("X");
    result.setWidth(200);
    result.setHeight(200);
    result.addElement(element);
    return new Object[] { string, result };
  }

  @Test
  public void createModelTest() throws Exception {
    SbmlExporter sbmlExporter = new SbmlExporter();
    sbmlExporter.setProvideDefaults(false);
    SbmlParser sbmlParser = new SbmlParser();
    String xmlString = sbmlExporter.toXml(model);

    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = sbmlParser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
    model2.setName(model.getName());

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
  }

}
