package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.*;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class SbmlTestFunctions {
  private Logger logger = LogManager.getLogger();

  private static int identifierCounter = 0;
  
  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  SbmlParser parser = new SbmlParser();
  SbmlExporter exporter;
  private MinervaLoggerAppender appender;

  public SbmlTestFunctions() {
    exporter = new SbmlExporter();
  }

  protected static GenericProtein createProtein() {
    GenericProtein result = new GenericProtein("sa" + (identifierCounter++));
    result.setName("SNCA");
    result.setX(10);
    result.setWidth(10);
    result.setY(10);
    result.setHeight(10);
    result.setZ(123);
    return result;
  }

  protected static Compartment createCompartment() {
    Compartment result = new Compartment("c" + (identifierCounter++));
    result.setZ(124);

    return result;
  }

  @Before
  public final void _setUp() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    appender = MinervaLoggerAppender.createAppender();
  }

  @After
  public final void _tearDown() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
  }

  protected List<LogEvent> getWarnings() {
    return appender.getWarnings();
  }

//  protected void showImage(Model model) throws Exception {
//    String dir = Files.createTempDirectory("sbml-temp-images-dir").toFile().getAbsolutePath();
//    AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().height(model.getHeight())
//        .width(model.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model);
//    NormalImageGenerator nig = new PngImageGenerator(params);
//    String pathWithouExtension = dir + "/" + model.getName();
//    String pngFilePath = pathWithouExtension.concat(".png");
//    nig.saveToFile(pngFilePath);
//    Desktop.getDesktop().open(new File(pngFilePath));
//
//  }

  protected Reaction createReaction(Element p1, Element p2) {
    Reaction reaction = new StateTransitionReaction("r" + (identifierCounter++));
    Reactant reactant = new Reactant(p1);
    Product product = new Product(p2);

    Point2D centerLineStart = new Point2D.Double((p1.getCenter().getX() + p2.getCenter().getX()) / 3,
        (p1.getCenter().getY() + p2.getCenter().getY()) / 3);
    Point2D centerLineEnd = new Point2D.Double(2 * (p1.getCenter().getX() + p2.getCenter().getX()) / 3,
        2 * (p1.getCenter().getY() + p2.getCenter().getY()) / 3);

    PolylineData reactantLine = new PolylineData();
    reactantLine.addPoint(p1.getCenter());
    reactantLine.addPoint(centerLineStart);
    reactant.setLine(reactantLine);
    PolylineData productLine = new PolylineData();
    productLine.addPoint(centerLineEnd);
    productLine.addPoint(p2.getCenter());
    product.setLine(productLine);
    reaction.addReactant(reactant);
    reaction.addProduct(product);
    reaction.setZ(125);

    PolylineData centerLine = new PolylineData(centerLineStart, centerLineEnd);
    reaction.setLine(centerLine);
    return reaction;
  }

  protected Model getModelAfterSerializing(Model originalModel) throws Exception {
    String xml = exporter.toXml(originalModel);
    // logger.debug(xml);
    ByteArrayInputStream stream = new ByteArrayInputStream(xml.getBytes("UTF-8"));
    Model result = parser.createModel(new ConverterParams().inputStream(stream));
    // showImage(originalModel);
    // showImage(result);
    return result;
  }

  protected Model getModelAfterCellDEsignerSerializing(Model model) throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    String xmlString = parser.model2String(model);
    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
    for (BioEntity bioEntity : model2.getBioEntities()) {
      bioEntity.setZ(null);
    }
    for (BioEntity bioEntity : model.getBioEntities()) {
      bioEntity.setZ(null);
    }
    return model2;
  }

  protected void validateSBML(String xmlContent, String filename) throws IOException, ClientProtocolException, InvalidXmlSchemaException {
    CloseableHttpClient httpClient = HttpClients.createDefault();
    HttpPost uploadFile = new HttpPost("http://sbml.org/validator/");
    MultipartEntityBuilder builder = MultipartEntityBuilder.create();
    builder.addTextBody("file", xmlContent, ContentType.TEXT_PLAIN);
    builder.addBinaryBody(
        "file",
        new ByteArrayInputStream(xmlContent.getBytes(StandardCharsets.UTF_8)),
        ContentType.APPLICATION_OCTET_STREAM,
        filename);
    builder.addTextBody("output", "xml", ContentType.TEXT_PLAIN);
    builder.addTextBody("offcheck", "u,r", ContentType.TEXT_PLAIN);

    HttpEntity multipart = builder.build();
    uploadFile.setEntity(multipart);
    CloseableHttpResponse response = httpClient.execute(uploadFile);
    String responseXml = EntityUtils.toString(response.getEntity());
    Document document = XmlParser.getXmlDocumentFromString(responseXml);
    List<Node> problems = XmlParser.getAllNotNecessirellyDirectChild("problem", document);
    if (problems.size() > 0) {
      logger.debug(responseXml);
    }
    assertEquals("SBML is invalid", 0, problems.size());
  }


}
