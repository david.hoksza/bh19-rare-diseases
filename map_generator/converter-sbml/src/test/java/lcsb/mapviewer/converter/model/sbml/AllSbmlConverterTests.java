package lcsb.mapviewer.converter.model.sbml;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.converter.model.sbml.compartment.AllCompartmentTests;
import lcsb.mapviewer.converter.model.sbml.reaction.AllSbmlReactionParserTests;
import lcsb.mapviewer.converter.model.sbml.species.AllSbmlSpeciesTests;

@RunWith(Suite.class)
@SuiteClasses({ AllCompartmentTests.class,
    AllSbmlReactionParserTests.class,
    AllSbmlSpeciesTests.class,
    GenericSbmlParserTest.class,
    GenericSbmlToXmlParserTest.class,
    CellDesignerToMultiExportTest.class,
    CopasiImportTest.class,
    ElementPropertiesExport.class,
    ElementPropertiesExportToMultiTest.class,
    ElementPropertiesExportToLayoutTest.class,
    GeneratedSbmlValidationTests.class,
    MultiParserTest.class,
    ReactionPropertiesExportToMultiTest.class,
    SbmlBioEntityExporterTest.class,
    SbmlExporterTest.class,
    SbmlExporterFromCellDesignerTest.class,
    SbmlPareserForInvalidReactionTest.class,
    SbmlParserTest.class,
    SbmlValidationTests.class,
})
public class AllSbmlConverterTests {

}
