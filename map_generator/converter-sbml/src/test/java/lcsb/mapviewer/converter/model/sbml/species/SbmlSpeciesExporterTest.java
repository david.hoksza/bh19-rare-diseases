package lcsb.mapviewer.converter.model.sbml.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.sbml.jsbml.SBMLDocument;

import lcsb.mapviewer.converter.model.sbml.*;
import lcsb.mapviewer.converter.model.sbml.compartment.SbmlCompartmentExporter;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.StructuralState;

public class SbmlSpeciesExporterTest extends SbmlTestFunctions {

  Logger logger = LogManager.getLogger(SbmlSpeciesExporterTest.class);

  org.sbml.jsbml.Model sbmlModel;

  @Test
  public void testOneElementWithTwoAliases() throws InconsistentModelException {
    Element protein1 = createProtein();
    Element protein2 = createProtein();
    Model model = new ModelFullIndexed(null);
    model.addElement(protein1);
    model.addElement(protein2);

    SbmlSpeciesExporter exporter = createExporter(model);

    exporter.exportElements();

    assertEquals(1, sbmlModel.getSpeciesCount());
  }

  @Test
  public void testOneElementButInTwoCompartments() throws InconsistentModelException {
    Compartment compartment = createCompartment();
    compartment.setName("test");
    Element protein1 = createProtein();
    Element protein2 = createProtein();
    protein2.setCompartment(compartment);

    Model model = new ModelFullIndexed(null);
    model.addElement(protein1);
    model.addElement(protein2);
    model.addElement(compartment);

    SbmlSpeciesExporter exporter = createExporter(model);

    exporter.exportElements();

    assertEquals(2, sbmlModel.getSpeciesCount());
  }

  private SbmlSpeciesExporter createExporter(Model model) throws InconsistentModelException {
    return createExporter(model, Arrays.asList(new SbmlExtension[] { SbmlExtension.RENDER, SbmlExtension.LAYOUT }));
  }

  private SbmlSpeciesExporter createExporter(Model model, Collection<SbmlExtension> sbmlExtensions)
      throws InconsistentModelException {
    SBMLDocument doc = new SBMLDocument(3, 1);
    sbmlModel = doc.createModel(model.getIdModel());

    SbmlExporter sbmlExporter = new SbmlExporter();
    sbmlExporter.createSbmlLayout(model, sbmlModel);

    SbmlCompartmentExporter compartmentExporter = new SbmlCompartmentExporter(sbmlModel, model,
        Arrays.asList(SbmlExtension.values()));
    compartmentExporter.exportElements();

    SbmlSpeciesExporter result = new SbmlSpeciesExporter(sbmlModel, model, sbmlExtensions, compartmentExporter);
    return result;
  }

  @Test
  public void testIdsOfSpeciesWithStructuralStates() throws InconsistentModelException {
    Species protein1 = createProtein();
    Protein protein2 = createProtein();
    StructuralState state = new StructuralState();
    state.setValue("X");
    protein2.setStructuralState(state);
    Model model = new ModelFullIndexed(null);
    model.addElement(protein1);
    model.addElement(protein2);

    SbmlSpeciesExporter exporter = createExporter(model, Arrays.asList(SbmlExtension.values()));

    assertFalse(exporter.getSbmlIdKey(protein1).equals(exporter.getSbmlIdKey(protein2)));
  }

}
