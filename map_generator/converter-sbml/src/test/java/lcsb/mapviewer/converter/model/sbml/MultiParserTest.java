package lcsb.mapviewer.converter.model.sbml;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class MultiParserTest extends SbmlTestFunctions {

  static Logger logger = LogManager.getLogger(MultiParserTest.class.getName());

  private Path filePath;

  public MultiParserTest(Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/multi")).forEach(fPath -> {
      if (Files.isRegularFile(fPath) && fPath.toString().endsWith(".xml")) {
        data.add(new Object[] { fPath });
      }
    });
    return data;
  }

  @Test
  public void createModelTest() throws Exception {
//    String dir = Files.createTempDirectory("sbml-temp-images-dir").toFile().getAbsolutePath();
//
//    Converter converter = new SbmlParser();
//
//    Model model = converter.createModel(new ConverterParams().filename(filePath.toString()));
//    model.setName(null);
//
//    // Create and display image of parsed map
//    AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().height(model.getHeight())
//        .width(model.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model);
//    NormalImageGenerator nig = new PngImageGenerator(params);
//    String pathWithouExtension = dir + "/"
//        + filePath.getFileName().toString().substring(0, filePath.getFileName().toString().indexOf(".xml"));
//    String pngFilePath = pathWithouExtension.concat(".png");
//    nig.saveToFile(pngFilePath);
//
//    CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();
//    String xmlString = cellDesignerXmlParser.model2String(model);
//
//    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));
//
//    Model model2 = cellDesignerXmlParser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
//
//    AbstractImageGenerator.Params params2 = new AbstractImageGenerator.Params().height(model2.getHeight())
//        .width(model2.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model2);
//    NormalImageGenerator nig2 = new PngImageGenerator(params2);
//    String pngFilePath2 = pathWithouExtension.concat("_2.png");
//    nig2.saveToFile(pngFilePath2);
//
//    assertNotNull(model2);
//    ModelComparator comparator = new ModelComparator(1.0);
//    assertEquals(0, comparator.compare(model, model2));
//    FileUtils.deleteDirectory(new File(dir));
  }

}
