package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;

import java.io.*;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

@RunWith(Parameterized.class)
public class ElementPropertiesExportToMultiTest extends SbmlTestFunctions {

  static Logger logger = LogManager.getLogger(ElementPropertiesExportToMultiTest.class.getName());

  private Model model;

  public ElementPropertiesExportToMultiTest(String propertyName, Model model) {
    this.model = model;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<>();

    data.add(createTestEntry("Empty", createProtein()));

    Species element = createProtein();
    element.setAbbreviation("xyz");
    data.add(createTestEntry("Abbreviation", element));

    element = createProtein();
    element.setFormerSymbols(Arrays.asList(new String[] { "symbol 1", "s2" }));
    data.add(createTestEntry("Former Symbols", element));

    element = createProtein();
    element.setFormula("C2H5OH");
    data.add(createTestEntry("Formula", element));

    element = createProtein();
    element.setFullName("Amazing element");
    data.add(createTestEntry("Full name", element));

    element = createProtein();
    element.setHomodimer(4);
    data.add(createTestEntry("Homodimer", element));

    element = createProtein();
    element.setCharge(5);
    data.add(createTestEntry("Charge", element));

    element = createProtein();
    element.setHypothetical(true);
    data.add(createTestEntry("Hypothetical", element));

    element = createProtein();
    element.setActivity(true);
    data.add(createTestEntry("Activity", element));

    element = createProtein();
    element.setSymbol("H2O");
    data.add(createTestEntry("Symbol", element));

    element = createProtein();
    element.setSynonyms(Arrays.asList(new String[] { "syn 1", "s2" }));
    data.add(createTestEntry("Synonyms", element));

    return data;
  }

  private static Object[] createTestEntry(String string, Element element) {
    Model result = new ModelFullIndexed(null);
    result.setIdModel("X");
    result.setWidth(200);
    result.setHeight(200);
    result.addElement(element);
    return new Object[] { string, result };
  }

  @Test
  public void createModelTest() throws Exception {
    SbmlExporter sbmlExporter = new SbmlExporter();
    sbmlExporter.setProvideDefaults(false);
    SbmlParser sbmlParser = new SbmlParser();
    String xmlString = sbmlExporter.toXml(model);

    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = sbmlParser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
    model2.setName(model.getName());

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
  }

}
