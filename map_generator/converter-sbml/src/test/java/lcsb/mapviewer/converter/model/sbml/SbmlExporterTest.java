package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.File;
import java.lang.reflect.Modifier;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.reflections.Reflections;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.ext.multi.*;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.ListComparator;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.PolylineDataComparator;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.*;

public class SbmlExporterTest extends SbmlTestFunctions {
  Logger logger = LogManager.getLogger();

  ModelComparator comparator = new ModelComparator();

  public SbmlExporterTest() {
    exporter.setProvideDefaults(false);
  }

  @Test
  public void testExportCompartment() throws Exception {
    Model model = getModelAfterSerializing("testFiles/layoutExample/CompartmentGlyph_Example_level2_level3.xml");
    assertNotNull(model);
    assertEquals(1, model.getCompartments().size());
    Compartment compartment = model.getElementByElementId("CompartmentGlyph_1");
    assertNotNull(compartment);
    assertNotNull(compartment.getX());
    assertNotNull(compartment.getY());
    assertTrue(compartment.getWidth() > 0);
    assertTrue(compartment.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= compartment.getX() + compartment.getWidth());
    assertTrue(model.getHeight() >= compartment.getY() + compartment.getHeight());
    assertFalse(compartment.getClass().equals(Compartment.class));
  }

  private Model getModelAfterSerializing(String filename) throws Exception {
    Model originalModel = parser.createModel(new ConverterParams().filename(filename));
    return getModelAfterSerializing(originalModel);
  }

  @Test
  public void testExportSpecies() throws Exception {
    Model model = getModelAfterSerializing("testFiles/layoutExample/SpeciesGlyph_Example_level2_level3.xml");
    assertNotNull(model);
    assertEquals(1, model.getElements().size());
    Species glucoseSpecies = model.getElementByElementId("SpeciesGlyph_Glucose");
    assertNotNull(glucoseSpecies.getX());
    assertNotNull(glucoseSpecies.getY());
    assertTrue(glucoseSpecies.getWidth() > 0);
    assertTrue(glucoseSpecies.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= glucoseSpecies.getX() + glucoseSpecies.getWidth());
    assertTrue(model.getHeight() >= glucoseSpecies.getY() + glucoseSpecies.getHeight());
    assertFalse(glucoseSpecies.getClass().equals(Species.class));
  }

  @Test
  public void testExportSpeciesInCompartments() throws Exception {
    Model model = getModelAfterSerializing("testFiles/layoutExample/SpeciesGlyph_Example.xml");
    assertNotNull(model);
    assertEquals(2, model.getElements().size());
    Compartment compartment = model.getElementByElementId("Yeast");
    assertNotNull(compartment.getX());
    assertNotNull(compartment.getY());

    assertNotNull(compartment.getX() >= 0);
    assertNotNull(compartment.getY() >= 0);
    assertTrue(compartment.getWidth() > 0);
    assertTrue(compartment.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= compartment.getX() + compartment.getWidth());
    assertTrue(model.getHeight() >= compartment.getY() + compartment.getHeight());
    assertFalse(compartment.getClass().equals(Compartment.class));
    assertTrue(compartment.getElements().size() > 0);
  }

  @Test
  public void testExportReaction() throws Exception {
    Model model = getModelAfterSerializing("testFiles/layoutExample/Complete_Example.xml");
    assertNotNull(model);
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    for (ReactionNode node : reaction.getReactionNodes()) {
      assertNotNull(node.getLine());
      assertTrue(node.getLine().length() > 0);
    }
    assertEquals(2, reaction.getOperators().size());
  }

  @Test
  public void testExportModelId() throws Exception {
    Model model = new ModelFullIndexed(null);
    model.setIdModel("Test123");
    Model deserializedModel = getModelAfterSerializing(model);

    assertEquals(model.getIdModel(), deserializedModel.getIdModel());
  }

  @Test
  public void testExportModelDimension() throws Exception {
    Model model = new ModelFullIndexed(null);
    model.setWidth(200);
    model.setHeight(300);
    Model deserializedModel = getModelAfterSerializing(model);

    assertEquals(model.getWidth(), deserializedModel.getWidth(), Configuration.EPSILON);
    assertEquals(model.getHeight(), deserializedModel.getHeight(), Configuration.EPSILON);
  }

  @Test
  public void testExportEmptyModel() throws Exception {
    Model model = new ModelFullIndexed(null);
    Model deserializedModel = getModelAfterSerializing(model);

    assertNotNull(deserializedModel);
  }

  @Test
  public void testExportReactionWithLayout() throws Exception {
    Model model = createModelWithReaction();
    Reaction reaction = model.getReactions().iterator().next();

    Model deserializedModel = getModelAfterSerializing(model);
    Reaction deserializedReaction = deserializedModel.getReactions().iterator().next();

    assertEquals(reaction.getReactants().get(0).getLine().length(),
        deserializedReaction.getReactants().get(0).getLine().length(), Configuration.EPSILON);
    assertEquals(reaction.getProducts().get(0).getLine().length(),
        deserializedReaction.getProducts().get(0).getLine().length(), Configuration.EPSILON);

  }

  private Model createModelWithReaction() {
    Model model = createEmptyModel();
    GenericProtein p1 = createProtein();
    p1.setY(10);
    model.addElement(p1);
    GenericProtein p2 = createProtein();
    p2.setY(50);
    model.addElement(p2);
    Reaction reaction = createReaction(p1, p2);
    model.addReaction(reaction);
    return model;
  }

  @Test
  public void testExportReactionWithNotes() throws Exception {
    Model model = createModelWithReaction();
    Reaction reaction = model.getReactions().iterator().next();
    reaction.setNotes("XYZ");

    Model deserializedModel = getModelAfterSerializing(model);
    Reaction deserializedReaction = deserializedModel.getReactions().iterator().next();

    assertEquals(reaction.getNotes(), deserializedReaction.getNotes());

  }

  @Test
  public void testExportModelWithReaction() throws Exception {
    String tempFilename = File.createTempFile("tmp", ".xml").getAbsolutePath();
    SbmlParser converter = new SbmlParser();
    converter.setProvideDefaults(false);

    Model model = converter.createModel(new ConverterParams().filename("testFiles/layoutExample/example1.xml"));
    model.setName(null);

    converter.model2File(model, tempFilename);

    Model model2 = converter.createModel(new ConverterParams().filename(tempFilename));
    model2.setName(null);

    assertNotNull(model2);
    ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(model, model2));

  }

  @Test
  public void testExportModelName() throws Exception {
    String tempFilename = File.createTempFile("tmp", ".xml").getAbsolutePath();
    SbmlParser converter = new SbmlParser();
    converter.setProvideDefaults(false);

    Model model = createEmptyModel();

    converter.model2File(model, tempFilename);

    Model model2 = converter.createModel(new ConverterParams().filename(tempFilename));

    assertNotNull(model2);
    ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(model, model2));

  }

  private Model createEmptyModel() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(1000);
    model.setHeight(1000);
    model.setName("UNKNOWN DISEASE MAP");
    model.setIdModel("id1");
    return model;
  }

  @Test
  public void testExportProblematicNotes() throws Exception {
    Model model = createModelWithReaction();
    Reaction reaction = model.getReactions().iterator().next();
    reaction.setNotes("X=Y<Z");

    Model deserializedModel = getModelAfterSerializing(model);
    Reaction deserializedReaction = deserializedModel.getReactions().iterator().next();

    assertEquals(reaction.getNotes(), deserializedReaction.getNotes());

  }

  @Test
  public void testColorParsing() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/example1.xml"));
    for (Species element : model.getSpeciesList()) {
      element.setFillColor(Color.BLUE);
    }
    Model model2 = getModelAfterSerializing(model);
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testModelAnnotationExport() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/model_with_annotations.xml"));
    Model model2 = getModelAfterSerializing(model);
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testReactionColorParsing() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/example1.xml"));
    for (Reaction element : model.getReactions()) {
      for (AbstractNode node : element.getNodes()) {
        node.getLine().setColor(Color.BLUE);
      }
    }
    Model model2 = getModelAfterSerializing(model);
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testReactionLineWidthParsing() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/example1.xml"));
    for (Reaction element : model.getReactions()) {
      for (AbstractNode node : element.getNodes()) {
        node.getLine().setWidth(12.7);
      }
    }
    Model model2 = getModelAfterSerializing(model);
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testExportSpeciesType() throws Exception {
    Reflections reflections = new Reflections("lcsb.mapviewer.model.map");
    Set<Class<? extends Species>> classes = reflections.getSubTypesOf(Species.class);
    for (Class<? extends Element> class1 : classes) {
      if (!Modifier.isAbstract(class1.getModifiers())) {
        Model model = createEmptyModel();
        Element element = class1.getConstructor(String.class).newInstance("x");
        element.setName("test name");
        element.setX(10);
        element.setWidth(10);
        element.setY(10);
        element.setHeight(10);
        element.setZ(127);
        model.addElement(element);
        Model deserializedModel = getModelAfterSerializing(model);

        assertEquals("Class " + class1 + " not exported/imported properly", 0,
            comparator.compare(model, deserializedModel));
      }
    }
  }

  @Test
  public void testSetUsedExtensions() throws Exception {
    SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));
    exporter.addSbmlExtension(SbmlExtension.LAYOUT);

    assertEquals(1, exporter.getSbmlExtensions().size());
    assertEquals(SbmlExtension.LAYOUT, exporter.getSbmlExtensions().iterator().next());

    exporter.removeSbmlExtension(SbmlExtension.LAYOUT);
    assertEquals(0, exporter.getSbmlExtensions().size());
  }

  @Test
  public void testExportWithLayoutExtension() throws Exception {
    SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));
    exporter.addSbmlExtension(SbmlExtension.LAYOUT);

    Model model = createModelWithReaction();
    String xml = exporter.toXml(model);

    assertTrue(xml.contains("layout:listOfLayouts"));
  }

  @Test
  public void testExportWithoutLayoutExtension() throws Exception {
    SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));

    Model model = createModelWithReaction();
    String xml = exporter.toXml(model);

    assertFalse(xml.contains("layout:listOfLayouts"));
  }

  @Test
  public void testExportWithRenderExtension() throws Exception {
    SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));
    exporter.addSbmlExtension(SbmlExtension.LAYOUT);
    exporter.addSbmlExtension(SbmlExtension.RENDER);

    Model model = createModelWithReaction();
    String xml = exporter.toXml(model);

    assertTrue(xml.contains("render:listOfRenderInformation"));
  }

  @Test
  public void testExportWithoutRenderExtension() throws Exception {
    SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));
    exporter.addSbmlExtension(SbmlExtension.LAYOUT);

    Model model = createModelWithReaction();
    String xml = exporter.toXml(model);

    assertFalse(xml.contains("render:listOfRenderInformation"));
  }

  @Test
  public void testExportWithoutMultiExtensionSupportSpeciesTypes() throws Exception {
    SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));
    exporter.addSbmlExtension(SbmlExtension.MULTI);

    org.sbml.jsbml.Model sbmlModel = exporter.toSbmlDocument(createModelWithReaction()).getModel();
    MultiModelPlugin multiPlugin = (MultiModelPlugin) sbmlModel.getExtension("multi");
    assertNotNull("Multi plugin is not present in sbml model", multiPlugin);

    assertTrue("Species types are not exported", multiPlugin.getListOfSpeciesTypes().size() > 0);
  }

  @Test
  public void testMultiExtensionSBOTermsForTypes() throws Exception {
    SbmlExporter exporter = new SbmlExporter();
    SBMLDocument doc = new SBMLDocument(3, 2);
    org.sbml.jsbml.Model result = doc.createModel("id");
    MultiModelPlugin multiPlugin = exporter.createSbmlMultiPlugin(result);

    for (MultiSpeciesType speciesType : multiPlugin.getListOfSpeciesTypes()) {
      assertNotNull("SBO term not defined for type " + speciesType.getName(), speciesType.getSBOTermID());
      assertFalse("SBO term not defined for type " + speciesType.getName(), speciesType.getSBOTermID().isEmpty());
    }

  }

  @Test
  public void testExportProteinState() throws Exception {
    Model model = createEmptyModel();
    GenericProtein element = createProtein();
    element.setStructuralState(createStructuralState(element));
    model.addElement(element);
    Model deserializedModel = getModelAfterSerializing(model);

    assertEquals("Structural state not exported/imported properly", 0, comparator.compare(model, deserializedModel));
  }

  @Test
  public void testExportPositionToCompartment() throws Exception {
    Model model = createEmptyModel();
    GenericProtein element = createProtein();
    element.setPositionToCompartment(PositionToCompartment.INSIDE);
    model.addElement(element);
    Model deserializedModel = getModelAfterSerializing(model);

    assertEquals("Postion to compartment not exported/imported properly", 0,
        comparator.compare(model, deserializedModel));
  }

  @Test
  public void testExportOuterSurfacePositionToCompartment() throws Exception {
    Model model = createEmptyModel();
    GenericProtein element = createProtein();
    element.setPositionToCompartment(PositionToCompartment.OUTER_SURFACE);
    model.addElement(element);
    Model deserializedModel = getModelAfterSerializing(model);

    assertEquals("Postion to compartment not exported/imported properly", 0,
        comparator.compare(model, deserializedModel));
  }

  @Test
  public void testMultiExtensionProteinStateInTypes() throws Exception {
    Model model = createEmptyModel();
    GenericProtein element = createProtein();
    StructuralState structuralState = createStructuralState(element);
    element.setStructuralState(structuralState);
    model.addElement(element);
    org.sbml.jsbml.Model sbmlModel = exporter.toSbmlDocument(model).getModel();
    MultiModelPlugin multiPlugin = (MultiModelPlugin) sbmlModel.getExtension("multi");

    boolean structuralStateValueFound = false;
    for (MultiSpeciesType speciesType : multiPlugin.getListOfSpeciesTypes()) {
      for (SpeciesFeatureType featureType : speciesType.getListOfSpeciesFeatureTypes()) {
        for (PossibleSpeciesFeatureValue featureValue : featureType.getListOfPossibleSpeciesFeatureValues()) {
          if (featureValue.getName().equals(structuralState.getValue())) {
            structuralStateValueFound = true;
          }
        }
      }
    }
    assertTrue("Structural state not defined in the list of possible values", structuralStateValueFound);

  }

  @Test
  public void testMultiExtensionProteinStateSuplicateInTypes() throws Exception {
    Model model = createEmptyModel();
    GenericProtein element = createProtein();
    element.setStructuralState(createStructuralState(element));
    model.addElement(element);
    element = createProtein();
    element.setStructuralState(createStructuralState(element));
    model.addElement(element);
    org.sbml.jsbml.Model sbmlModel = exporter.toSbmlDocument(model).getModel();
    MultiModelPlugin multiPlugin = (MultiModelPlugin) sbmlModel.getExtension("multi");

    for (MultiSpeciesType speciesType : multiPlugin.getListOfSpeciesTypes()) {
      for (SpeciesFeatureType featureType : speciesType.getListOfSpeciesFeatureTypes()) {
        assertTrue(featureType.getListOfPossibleSpeciesFeatureValues().size() <= 2);
      }
    }
  }

  @Test
  public void testMultiExtensionTypeDefinition() throws Exception {
    Model model = createEmptyModel();
    GenericProtein element = createProtein();
    model.addElement(element);
    org.sbml.jsbml.Model sbmlModel = exporter.toSbmlDocument(model).getModel();

    MultiModelPlugin multiPlugin = (MultiModelPlugin) sbmlModel.getExtension("multi");

    org.sbml.jsbml.Species sbmlSpecies = sbmlModel.getSpecies(0);
    MultiSpeciesPlugin speciesExtension = (MultiSpeciesPlugin) sbmlSpecies.getExtension("multi");
    assertNotNull("Multi extension not defined for species", speciesExtension);
    String speciesTypeString = speciesExtension.getSpeciesType();

    MultiSpeciesType speciesType = null;
    for (MultiSpeciesType type : multiPlugin.getListOfSpeciesTypes()) {
      if (type.getId().equals(speciesTypeString)) {
        speciesType = type;
      }
    }
    assertNotNull("Species type is not set in multi extension", speciesType);

  }

  @Test
  public void testMultiExtensionStructuralStateTypeDefinition() throws Exception {
    Model model = createEmptyModel();
    GenericProtein element = createProtein();
    StructuralState structuralState = createStructuralState(element);
    element.setStructuralState(structuralState);
    model.addElement(element);
    org.sbml.jsbml.Model sbmlModel = exporter.toSbmlDocument(model).getModel();

    org.sbml.jsbml.Species sbmlSpecies = sbmlModel.getSpecies(0);
    MultiSpeciesPlugin speciesExtension = (MultiSpeciesPlugin) sbmlSpecies.getExtension("multi");
    assertNotNull("Multi extension not defined for species", speciesExtension);
    assertTrue("structural state feature not defined in multi extension",
        speciesExtension.getListOfSpeciesFeatures().size() > 0);
  }

  private StructuralState createStructuralState(Element element) {
    StructuralState structuralState = new StructuralState();
    structuralState.setValue("xxx");
    structuralState.setPosition(new Point2D.Double(element.getX(), element.getY() - 10));
    structuralState.setWidth(element.getWidth());
    structuralState.setHeight(20.0);
    structuralState.setFontSize(10.0);
    return structuralState;
  }

  @Test
  public void testExportResidue() throws Exception {
    Model model = createEmptyModel();
    GenericProtein element = createProtein();
    Residue mr = new Residue("x1");
    mr.setName("217U");
    mr.setState(ModificationState.PHOSPHORYLATED);
    mr.setPosition(new Point2D.Double(10, 11));
    element.addResidue(mr);
    mr = new Residue("Y");
    mr.setName("218");
    mr.setState(ModificationState.PHOSPHORYLATED);
    mr.setPosition(new Point2D.Double(10, 12));
    element.addResidue(mr);
    mr = new Residue("Z");
    mr.setName("219");
    mr.setState(ModificationState.UBIQUITINATED);
    mr.setPosition(new Point2D.Double(10, 13));
    element.addResidue(mr);
    model.addElement(element);
    Model deserializedModel = getModelAfterSerializing(model);

    GenericProtein protein = deserializedModel.getElementByElementId(element.getElementId());
    assertEquals("Residues weren't exported/imported properly", 3, protein.getModificationResidues().size());

  }

  @Test
  public void testExportNotes() throws Exception {
    Model model = createEmptyModel();
    model.setNotes("XX");
    Model deserializedModel = getModelAfterSerializing(model);

    assertEquals("Notes weren't exported/imported properly", model.getNotes(), deserializedModel.getNotes());

  }

  @Test
  public void testExportImportOfAdvancedInputOperator() throws Exception {
    Model originalModel = new CellDesignerXmlParser().createModel(new ConverterParams()
        .filename("testFiles/cell_designer_problems/heterodimer_association_with_additional_reactant.xml"));
    Model model = getModelAfterSerializing(originalModel);
    Reaction r1 = originalModel.getReactions().iterator().next();
    Reaction r2 = model.getReactions().iterator().next();
    Product p1 = r1.getProducts().get(0);
    Product p2 = r1.getProducts().get(0);
    NodeOperator o1 = r1.getOperators().get(0);
    NodeOperator o11 = r1.getOperators().get(1);
    NodeOperator o2 = r2.getOperators().get(0);

    assertEquals(p1.getLine().length() + o1.getLine().length() + o11.getLine().length() + r1.getLine().length(),
        p2.getLine().length() + o2.getLine().length() + r2.getLine().length(), Configuration.EPSILON);

  }

  @Test
  public void testExportImportBooleanGate() throws Exception {
    Model originalModel = new CellDesignerXmlParser()
        .createModel(new ConverterParams().filename("testFiles/cell_designer_problems/boolean_logic_gate.xml"));
    Model model = getModelAfterSerializing(originalModel);

    List<PolylineData> lines1 = new ArrayList<>();
    List<PolylineData> lines2 = new ArrayList<>();

    for (ReactionNode node : originalModel.getReactions().iterator().next().getReactionNodes()) {
      lines1.add(node.getLine());
    }
    for (ReactionNode node : model.getReactions().iterator().next().getReactionNodes()) {
      lines2.add(node.getLine());
    }

    ListComparator<PolylineData> comparator = new ListComparator<>(new PolylineDataComparator(Configuration.EPSILON));
    assertEquals(0, comparator.compare(lines1, lines2));
  }

  @Test
  public void testExportIronMetabolismReaction() throws Exception {
    Model originalModel = new CellDesignerXmlParser()
        .createModel(new ConverterParams().filename("testFiles/cell_designer_problems/iron_metabolism_reaction.xml"));
    exporter.setProvideDefaults(false);
    String xml = exporter.toXml(originalModel);
    assertNotNull(xml);
  }

  @Test
  public void testCreatorExport() throws Exception {
    Model originalModel = parser.createModel(
        new ConverterParams().filename("testFiles/small/model_with_creator.xml"));
    Model model = getModelAfterSerializing(originalModel);

    assertEquals(0, comparator.compare(model, originalModel));
  }

  @Test
  public void testInvalidSpeciesId() throws Exception {
    Model model = createEmptyModel();

    GenericProtein p = createProtein();
    p.setElementId("a-b");
    model.addElement(p);

    String xml = exporter.toXml(model);
    assertNotNull(xml);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testInvalidReactionId() throws Exception {
    Model model = createEmptyModel();

    Protein p1 = createProtein();
    Protein p2 = createProtein();
    Reaction p = createReaction(p1, p2);
    p.setIdReaction("a-b");
    model.addElement(p1);
    model.addElement(p2);
    model.addReaction(p);

    String xml = exporter.toXml(model);
    assertNotNull(xml);
    assertEquals(1, getWarnings().size());

  }

  @Test
  public void testInvalidSpeciesIdInReaction() throws Exception {
    Model model = createEmptyModel();

    Protein p1 = createProtein();
    Protein p2 = createProtein();
    Reaction p = createReaction(p1, p2);
    p1.setElementId("a-b");
    model.addElement(p1);
    model.addElement(p2);
    model.addReaction(p);

    String xml = exporter.toXml(model);
    assertNotNull(xml);
    assertEquals(1, getWarnings().size());

  }

  @Test
  public void testInvalidCompartmentId() throws Exception {
    Model model = createEmptyModel();

    Compartment p = createCompartment();
    p.setElementId("a-b");
    model.addElement(p);

    String xml = exporter.toXml(model);
    assertNotNull(xml);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testInvalidModelId() throws Exception {
    Model model = createEmptyModel();
    model.setIdModel("a-b");

    String xml = exporter.toXml(model);
    assertNotNull(xml);
    assertEquals(1, getWarnings().size());

  }


}
