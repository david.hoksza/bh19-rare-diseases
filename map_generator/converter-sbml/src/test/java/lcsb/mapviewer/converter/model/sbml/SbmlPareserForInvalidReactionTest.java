package lcsb.mapviewer.converter.model.sbml;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.*;

@RunWith(Parameterized.class)
public class SbmlPareserForInvalidReactionTest extends SbmlTestFunctions {

  static Logger logger = LogManager.getLogger(SbmlPareserForInvalidReactionTest.class.getName());

  private Path filePath;

  public SbmlPareserForInvalidReactionTest(Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/invalidReaction")).forEach(fPath -> {
      if (Files.isRegularFile(fPath) && fPath.toString().endsWith(".xml")) {
        data.add(new Object[] { fPath });
      }
    });
    return data;
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void createModelTest() throws Exception {
    Converter converter = new SbmlParser();
    converter.createModel(new ConverterParams().filename(filePath.toString()));
  }

}
