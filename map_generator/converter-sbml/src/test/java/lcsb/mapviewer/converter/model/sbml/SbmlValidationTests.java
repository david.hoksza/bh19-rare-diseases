package lcsb.mapviewer.converter.model.sbml;

import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.model.Model;

@RunWith(Parameterized.class)
public class SbmlValidationTests extends SbmlTestFunctions {
  Logger logger = LogManager.getLogger();

  String filename;

  public SbmlValidationTests(String filename) {
    this.filename = filename;
  }

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() throws IOException {
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[] { "testFiles/small/empty.xml" });
    result.add(new Object[] { "testFiles/small/reaction/dissociation.xml" });
    return result;
  }

  @Test
  public void testIsValidSbml() throws Exception {
    SbmlParser parser = new SbmlParser();
    Model model = parser.createModel(new ConverterParams().filename(filename));
    String xml = parser.model2String(model);

    validateSBML(xml, filename);
  }

}
