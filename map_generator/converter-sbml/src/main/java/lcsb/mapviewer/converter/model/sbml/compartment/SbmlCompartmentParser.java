package lcsb.mapviewer.converter.model.sbml.compartment;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.*;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.render.LocalStyle;

import lcsb.mapviewer.commands.layout.ApplySimpleLayoutModelCommand;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlElementParser;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.species.Element;

public class SbmlCompartmentParser extends SbmlElementParser<org.sbml.jsbml.Compartment> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SbmlCompartmentParser.class);

  public SbmlCompartmentParser(Model sbmlModel, lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(sbmlModel, minervaModel);
  }

  @Override
  protected ListOf<org.sbml.jsbml.Compartment> getSbmlElementList() {
    return getSbmlModel().getListOfCompartments();
  }

  @Override
  public List<Element> mergeLayout(List<? extends Element> elements)
      throws InvalidInputDataExecption {
    List<Element> result = super.mergeLayout(elements);

    for (Element element : result) {
      Compartment parent = element.getCompartment();
      for (Compartment compartment : getMinervaModel().getCompartments()) {
        compartment.setNamePoint(compartment.getX() + ApplySimpleLayoutModelCommand.COMPARTMENT_BORDER,
            compartment.getY() + ApplySimpleLayoutModelCommand.COMPARTMENT_BORDER);
        if (parent == null || parent.getSize() > compartment.getSize()) {
          if (compartment.contains(element)) {
            parent = compartment;
          }
        }
      }
      if (parent != null) {
        parent.addElement(element);
      }
    }

    for (Pair<String, AbstractReferenceGlyph> idGlyphPair : getGlyphs()) {
      String id = idGlyphPair.getLeft();
      Compartment source = (Compartment) getAnyElementBySbmlElementId(id);
      if (source == null) {
        throw new InvalidInputDataExecption("Layout contains invalid Species id: " + idGlyphPair.getLeft());
      }

      for (TextGlyph textGlyph : getTextGlyphsByLayoutElementId(idGlyphPair.getRight())) {
        if (textGlyph.getBoundingBox() != null) {
          Point p = textGlyph.getBoundingBox().getPosition();
          source.setNamePoint(new Point2D.Double(p.getX(), p.getY()));
        }
      }
    }

    return result;
  }

  @Override
  protected void applyStyleToElement(Element elementWithLayout, LocalStyle style) {
    if (!style.getGroup().isSetFill() && style.getGroup().isSetStroke()) {
      Color backgroundColor = getColorByColorDefinition(style.getGroup().getStroke());
      elementWithLayout.setFillColor(backgroundColor);
      if (elementWithLayout instanceof Compartment) {
        elementWithLayout.setBorderColor(backgroundColor);
      }
    }
  }

  @Override
  protected List<Pair<String, AbstractReferenceGlyph>> getGlyphs() {
    List<Pair<String, AbstractReferenceGlyph>> result = new ArrayList<>();
    for (CompartmentGlyph glyph : getLayout().getListOfCompartmentGlyphs()) {
      if (!glyph.getCompartment().equals("default")) {
        result.add(new Pair<>(glyph.getCompartment(), glyph));
      }
    }
    return result;
  }

  @Override
  protected Compartment parse(org.sbml.jsbml.Compartment compartment)
      throws InvalidInputDataExecption {
    if (compartment.getId().equals("default")) {
      return null;
    }
    Compartment result = new SquareCompartment(compartment.getId());
    assignBioEntityData(compartment, result);
    return result;
  }

}
