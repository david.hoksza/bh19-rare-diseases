package lcsb.mapviewer.converter.model.sbml.reaction;

import java.awt.*;
import java.awt.geom.Point2D;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.*;
import org.sbml.jsbml.ext.layout.*;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.RenderGroup;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.*;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.geometry.ReactionCellDesignerConverter;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierType;
import lcsb.mapviewer.converter.model.sbml.SbmlBioEntityParser;
import lcsb.mapviewer.converter.model.sbml.SbmlParameterParser;
import lcsb.mapviewer.converter.model.sbml.compartment.SbmlCompartmentParser;
import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesParser;
import lcsb.mapviewer.model.graphics.*;
import lcsb.mapviewer.model.map.kinetics.SbmlArgument;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.modifier.*;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.*;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;

public class SbmlReactionParser extends SbmlBioEntityParser {
  private static Logger logger = LogManager.getLogger(SbmlReactionParser.class);

  private ElementUtils eu = new ElementUtils();

  private SbmlSpeciesParser speciesParser;
  private SbmlCompartmentParser compartmentParser;

  private PointTransformation pt = new PointTransformation();

  public SbmlReactionParser(Model sbmlModel, lcsb.mapviewer.model.map.model.Model minervaModel,
      SbmlSpeciesParser speciesParser, SbmlCompartmentParser compartmentParser) {
    super(sbmlModel, minervaModel);
    this.speciesParser = speciesParser;
    this.compartmentParser = compartmentParser;
  }

  public List<Reaction> parseList() throws InvalidInputDataExecption {
    List<Reaction> result = new ArrayList<>();
    for (org.sbml.jsbml.Reaction sbmlElement : getSbmlElementList()) {
      result.add(parse(sbmlElement));
    }
    return result;
  }

  protected ListOf<org.sbml.jsbml.Reaction> getSbmlElementList() {
    return getSbmlModel().getListOfReactions();
  }

  public void mergeLayout(Collection<Reaction> reactions)
      throws InvalidInputDataExecption {
    Set<Reaction> used = new HashSet<>();
    Map<String, Reaction> reactionById = new HashMap<>();
    for (Reaction reaction : reactions) {
      if (reactionById.get(reaction.getElementId()) != null) {
        throw new InvalidInputDataExecption("Duplicated reaction id: " + reaction.getElementId());
      }
      reactionById.put(reaction.getElementId(), reaction);
    }

    for (ReactionGlyph glyph : getLayout().getListOfReactionGlyphs()) {
      Reaction source = reactionById.get(glyph.getReaction());
      if (source == null) {
        throw new InvalidInputDataExecption("Layout contains invalid Species id: " + glyph.getReaction());
      }
      used.add(source);
      try {
        Map<ReactionNode, SpeciesReferenceGlyph> glyphByNode = new HashMap<>();
        Reaction reactionWithLayout = source.copy();
        // getId doesn't have to be unique, therefore we concatenate with reaction
        reactionWithLayout.setIdReaction(glyph.getReaction() + "__" + glyph.getId());

        // z index
        if (glyph.getBoundingBox() != null && glyph.getBoundingBox().getPosition() != null
            && glyph.getBoundingBox().getPosition().isSetZ()) {
          reactionWithLayout.setZ((int) glyph.getBoundingBox().getPosition().getZ());
        }
        for (SpeciesReferenceGlyph speciesRefernceGlyph : glyph.getListOfSpeciesReferenceGlyphs()) {
          SpeciesGlyph speciesGlyph = getLayout().getSpeciesGlyph(speciesRefernceGlyph.getSpeciesGlyph());
          ReactionNode minervaNode = null;
          Class<? extends ReactionNode> nodeClass = getReactionNodeClass(speciesRefernceGlyph);

          if (reactionWithLayout.isReversible() && (nodeClass == Reactant.class || nodeClass == Product.class)) {
            nodeClass = null;
          }
          for (ReactionNode node : reactionWithLayout.getReactionNodes()) {
            if (node.getElement().getElementId().equals(speciesGlyph.getSpecies())) {
              if (nodeClass == null) {
                minervaNode = node;
                nodeClass = node.getClass();
              } else if (node.getClass().isAssignableFrom(nodeClass)) {
                minervaNode = node;
              } else if (nodeClass.isAssignableFrom(node.getClass())) {
                minervaNode = node;
                nodeClass = node.getClass();
              }
            }
          }
          if (nodeClass == Modifier.class) {
            nodeClass = Modulation.class;
          }
          if (minervaNode == null) {
            throw new InvalidInputDataExecption(
                "Cannot find reaction node for layouted reaction: " + speciesGlyph.getSpecies() + ", " + glyph.getId());
          }
          glyphByNode.put(minervaNode, speciesRefernceGlyph);
          Element minervaElement = getMinervaModel().getElementByElementId(speciesGlyph.getId());
          if (minervaElement == null) {
            throw new InvalidInputDataExecption("Cannot find layouted reaction node for layouted reaction: "
                + speciesGlyph.getId() + ", " + glyph.getId());
          }

          PolylineData line = getLineFromReferenceGlyph(speciesRefernceGlyph);
          if (line.length() == 0) {
            logger.warn("Line undefined for element " + minervaElement.getElementId()
                + " in reaction " + reactionWithLayout.getElementId());
          }
          if (minervaNode instanceof Reactant) {
            // in SBML line goes from reaction to reactant
            line = line.reverse();
            if (reactionWithLayout.isReversible()) {
              line.getBeginAtd().setArrowType(ArrowType.FULL);
            }
          }
          if (minervaNode instanceof Product) {
            ArrowTypeData atd = new ArrowTypeData();
            atd.setArrowType(ArrowType.FULL);
            line.setEndAtd(atd);
          } else if (minervaNode instanceof Modifier) {
            for (ModifierType mt : ModifierType.values()) {
              if (mt.getClazz().equals(nodeClass)) {
                line.setEndAtd(mt.getAtd());
                line.setType(mt.getLineType());
              }
            }
          }
          updateKinetics(reactionWithLayout, minervaElement.getElementId(), minervaNode.getElement().getElementId());
          if (minervaElement.getCompartment() != null) {
            String newCompartmentId = minervaElement.getCompartment().getElementId();
            String oldCompartmentId = compartmentParser.getSbmlIdByElementId(newCompartmentId);
            updateKinetics(reactionWithLayout, newCompartmentId, oldCompartmentId);
          }
          minervaNode.setLine(line);
          minervaNode.setElement(minervaElement);
          if (nodeClass != minervaNode.getClass()) {
            reactionWithLayout.removeNode(minervaNode);

            try {
              ReactionNode newNode = nodeClass.getConstructor().newInstance();
              newNode.setElement(minervaElement);
              newNode.setLine(line);
              reactionWithLayout.addNode(newNode);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException | NoSuchMethodException | SecurityException e) {
              throw new InvalidStateException(e);
            }
          }
        }

        if (reactionWithLayout.getReactants().size() > 1 && reactionWithLayout.getProducts().size() > 0) {
          NodeOperator operator = createInputOperator(reactionWithLayout, glyph);
          reactionWithLayout.addNode(operator);
        }
        if (reactionWithLayout.getReactants().size() > 0 && reactionWithLayout.getProducts().size() > 1) {
          NodeOperator operator = createOutputOperator(reactionWithLayout, glyph);
          reactionWithLayout.addNode(operator);
        }
        reactionWithLayout.setLine(extractCurve(glyph, reactionWithLayout, null));
        createCenterLineForReaction(reactionWithLayout);
        assignRenderDataToReaction(glyph, reactionWithLayout);

        for (ReactionNode reactionNode : reactionWithLayout.getReactionNodes()) {
          SpeciesReferenceGlyph speciesGlyph = glyphByNode.get(reactionNode);
          if (speciesGlyph != null) {
            assignRenderDataToReactionNode(speciesGlyph, reactionNode, reactionNode instanceof Reactant);
          }
        }

        getMinervaModel().addReaction(reactionWithLayout);

      } catch (Exception e) {
        throw new InvalidInputDataExecption(
            new ElementUtils().getElementTag(source) + "Problem with parsing reaction layout", e);
      }
    }
    Set<Reaction> elementsToRemove = new HashSet<>();
    for (Reaction reaction : reactions) {
      if (!used.contains(reaction)) {
        for (ReactionNode node : reaction.getReactionNodes()) {
          // we might have different elements here, the reason is that single SBML species
          // can be split into two or more (due to layout)
          node.setElement(speciesParser.getAnyElementBySbmlElementId(node.getElement().getElementId()));
        }
      } else {
        elementsToRemove.add(reaction);
      }
    }
    for (Reaction reaction : elementsToRemove) {
      getMinervaModel().removeReaction(reaction);
    }
  }

  /**
   * Creates center line for assembled reaction with operators.
   * 
   * @param reaction
   */
  private void createCenterLineForReaction(Reaction reaction) {
    if (reaction.getLine().length() == 0) {
      AbstractNode startNode = reaction.getReactants().get(0);
      AbstractNode endNode = reaction.getProducts().get(0);

      for (NodeOperator operator : reaction.getOperators()) {
        if (operator.isReactantOperator()) {
          startNode = operator;
        }
      }

      for (NodeOperator operator : reaction.getOperators()) {
        if (operator.isProductOperator()) {
          endNode = operator;
        }
      }

      PolylineData line = new PolylineData();
      reaction.setLine(line);
      // if there is no layout don't create center line
      if (startNode.getLine() != null && startNode.getLine().length() > Configuration.EPSILON &&
          endNode.getLine() != null && endNode.getLine().length() > Configuration.EPSILON) {
        startNode.getLine().trimEnd(ReactionCellDesignerConverter.RECT_SIZE);
        endNode.getLine().trimBegin(ReactionCellDesignerConverter.RECT_SIZE);

        line.addPoint(pt.copyPoint(startNode.getLine().getEndPoint()));
        line.addPoint(pt.copyPoint(endNode.getLine().getBeginPoint()));
        line.setType(startNode.getLine().getType());
        line.setColor(startNode.getLine().getColor());
      }
    }
  }

  private void assignRenderDataToReactionNode(SpeciesReferenceGlyph glyph, ReactionNode modifier, boolean reverse)
      throws InvalidInputDataExecption {
    LocalStyle style = getStyleForGlyph(glyph);
    if (style != null) {
      applyStyleToReactionNode(modifier, style, reverse);
    }
  }

  private void applyStyleToReactionNode(ReactionNode modifier, LocalStyle style, boolean reverse) {
    RenderGroup group = style.getGroup();
    if (group.isSetStroke()) {
      Color color = getColorByColorDefinition(group.getStroke());
      modifier.getLine().setColor(color);
    }
    if (group.isSetStrokeWidth()) {
      modifier.getLine().setWidth(group.getStrokeWidth());
    }
    if (group.isSetStrokeDashArray()) {
      LineType type = LineType.getTypeByDashArray(group.getStrokeDashArray());
      modifier.getLine().setType(type);
    }
    if (group.isSetEndHead()) {
      try {
        ArrowType type = ArrowType.valueOf(group.getEndHead().replaceAll("line_ending_", ""));
        if (reverse) {
          modifier.getLine().getBeginAtd().setArrowType(type);
        } else {
          modifier.getLine().getEndAtd().setArrowType(type);
        }
      } catch (Exception e) {
        logger.warn(new ElementUtils().getElementTag(modifier.getReaction()) + "Problematic arrow type: "
            + group.getStroke(), e);
      }
    }
  }

  private NodeOperator createOutputOperator(Reaction reactionWithLayout, ReactionGlyph reactionGlyph) {
    NodeOperator operator;
    if (reactionWithLayout instanceof TruncationReaction) {
      operator = new TruncationOperator();
    } else if (reactionWithLayout instanceof DissociationReaction) {
      operator = new DissociationOperator();
    } else {
      operator = new SplitOperator();
    }
    PolylineData line = extractCurve(reactionGlyph, reactionWithLayout, operator.getClass());

    operator.addOutputs(reactionWithLayout.getProducts());
    operator.setLine(line.reverse());
    return operator;
  }

  private NodeOperator createInputOperator(Reaction reactionWithLayout, ReactionGlyph reactionGlyph) {

    NodeOperator operator;
    if (reactionWithLayout instanceof HeterodimerAssociationReaction) {
      operator = new AssociationOperator();
    } else {
      operator = new AndOperator();
    }
    PolylineData line = extractCurve(reactionGlyph, reactionWithLayout, operator.getClass());
    operator.addInputs(reactionWithLayout.getReactants());
    operator.setLine(line);
    return operator;
  }

  /**
   * Extracts {@link Curve} associated to {@link ReactionGlyph}. If curve doesn't
   * exist then method will try to create it from {@link BoundingBox} (due to SBML
   * specification). This {@link Curve} is split into parts belonging to:
   * <ul>
   * <li>input operator</li>
   * <li>center line</li>
   * <li>output operator</li>
   * </ul>
   * . {@link PolylineData} corresponding to the proper part is returned.
   * 
   * @param reactionGlyph
   * @param operator
   *          class of {@link NodeOperator} that requires the line (this line can
   *          be separated into 3 different segments - reactant, center, product
   *          parts)
   * @return
   */
  PolylineData extractCurve(ReactionGlyph reactionGlyph, Reaction reaction,
      Class<? extends NodeOperator> operator) {
    Curve curve = getCurve(reactionGlyph);
    if (curve == null) {
      curve = new Curve(getSbmlModel().getLevel(), getSbmlModel().getVersion());
      logger.warn(new ElementUtils().getElementTag(reaction) + " invalid curve line for reaction (node: "
          + reactionGlyph.getId() + ")");
    }
    if (curve.getCurveSegmentCount() == 0) {
      return new PolylineData();
    }

    int requiredLines = 1;
    if (reaction.getReactants().size() > 1) {
      requiredLines++;
    }
    if (reaction.getProducts().size() > 1) {
      requiredLines++;
    }
    int existingLines = curve.getCurveSegmentCount();

    if (requiredLines == 3 && existingLines == 1) {
      CurveSegment segment = curve.getCurveSegment(0);
      Point secondPoint = getPointOnSegment(segment, 0.4);
      Point thirdPoint = getPointOnSegment(segment, 0.6);

      LineSegment curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
      curveSegment.setStart(secondPoint);
      curveSegment.setEnd(thirdPoint);
      curve.addCurveSegment(curveSegment);

      curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
      curveSegment.setStart(thirdPoint);
      curveSegment.setEnd(segment.getEnd());
      curve.addCurveSegment(curveSegment);

      segment.setEnd(secondPoint);
    } else if (requiredLines == 3 && existingLines == 2) {
      if (getSegmentLength(curve.getCurveSegment(0)) > getSegmentLength(curve.getCurveSegment(1))) {
        CurveSegment segment = curve.getCurveSegment(0);
        Point centerPoint = getCenter(segment);
        LineSegment curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
        curveSegment.setStart(centerPoint);
        curveSegment.setEnd(segment.getEnd());

        curve.addCurveSegment(1, curveSegment);
        segment.setEnd(centerPoint);
      } else {
        CurveSegment segment = curve.getCurveSegment(1);
        Point centerPoint = getCenter(segment);
        LineSegment curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
        curveSegment.setStart(centerPoint);
        curveSegment.setEnd(segment.getEnd());

        curve.addCurveSegment(curveSegment);
        segment.setEnd(centerPoint);

      }
    } else if (requiredLines == 2 && existingLines == 1) {
      CurveSegment segment = curve.getCurveSegment(0);
      Point centerPoint = getCenter(segment);
      LineSegment curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
      curveSegment.setStart(centerPoint);
      curveSegment.setEnd(segment.getEnd());

      curve.addCurveSegment(curveSegment);

      segment.setEnd(centerPoint);
    }
    existingLines = curve.getCurveSegmentCount();

    Integer reactantLineEnds;
    Integer productLineStarts;
    if (requiredLines == 3) {
      reactantLineEnds = existingLines / 2;
      productLineStarts = reactantLineEnds + 1;
    } else if (reaction.getReactants().size() > 1) {
      reactantLineEnds = existingLines - 1;
      productLineStarts = reactantLineEnds + 1;
    } else if (reaction.getProducts().size() > 1) {
      reactantLineEnds = 0;
      productLineStarts = reactantLineEnds + 1;
    } else if (requiredLines == 1) {
      reactantLineEnds = 0;
      productLineStarts = existingLines;
    } else {
      throw new InvalidArgumentException(
          new ElementUtils().getElementTag(reaction) + "Reaction has no products and reactants");
    }

    PolylineData result = new PolylineData();
    if (operator == TruncationOperator.class ||
        operator == DissociationOperator.class ||
        operator == SplitOperator.class) {
      // Product operator

      CurveSegment segment = curve.getCurveSegment(productLineStarts);
      result.addPoint(new Point2D.Double(segment.getStart().getX(), segment.getStart().getY()));
      for (int i = productLineStarts; i < curve.getCurveSegmentCount(); i++) {
        segment = curve.getCurveSegment(i);
        result.addPoint(new Point2D.Double(segment.getEnd().getX(), segment.getEnd().getY()));
      }
    } else if (operator == AssociationOperator.class || operator == AndOperator.class) {
      // Reactant operator
      CurveSegment segment = curve.getCurveSegment(0);
      result.addPoint(new Point2D.Double(segment.getStart().getX(), segment.getStart().getY()));
      for (int i = 0; i < reactantLineEnds; i++) {
        segment = curve.getCurveSegment(i);
        result.addPoint(new Point2D.Double(segment.getEnd().getX(), segment.getEnd().getY()));
      }
    } else if (operator == null) {
      // Center line
      CurveSegment segment = curve.getCurveSegment(reactantLineEnds);
      result.addPoint(new Point2D.Double(segment.getStart().getX(), segment.getStart().getY()));
      for (int i = reactantLineEnds; i < productLineStarts; i++) {
        segment = curve.getCurveSegment(i);
        result.addPoint(new Point2D.Double(segment.getEnd().getX(), segment.getEnd().getY()));
      }
    } else {
      throw new InvalidArgumentException("Unknown operator class: " + operator);
    }
    return result;
  }

  private double getSegmentLength(CurveSegment curveSegment) {
    double dx = curveSegment.getStart().getX() - curveSegment.getEnd().getX();
    double dy = curveSegment.getStart().getY() - curveSegment.getEnd().getY();
    return Math.sqrt(dx * dx + dy * dy);
  }

  private Point getCenter(CurveSegment segment) {
    return getPointOnSegment(segment, 0.5);
  }

  Point getPointOnSegment(CurveSegment segment, double coef) {
    double centerX = segment.getStart().getX() + (segment.getEnd().getX() - segment.getStart().getX()) * coef;
    double centerY = segment.getStart().getY() + (segment.getEnd().getY() - segment.getStart().getY()) * coef;
    Point centerPoint = new Point(getSbmlModel().getLevel(), getSbmlModel().getVersion());
    centerPoint.setX(centerX);
    centerPoint.setY(centerY);
    return centerPoint;
  }

  Curve getCurve(ReactionGlyph reactionGlyph) {
    Curve curve = reactionGlyph.getCurve();
    if (curve == null) {
      BoundingBox box = reactionGlyph.getBoundingBox();
      if (box != null && box.getDimensions() != null && box.getPosition() != null) {
        curve = new Curve(getSbmlModel().getLevel(), getSbmlModel().getVersion());
        if (box.getDimensions().getWidth() > 0 || box.getDimensions().getHeight() > 0) {
          LineSegment curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
          Point start = new Point(getSbmlModel().getLevel(), getSbmlModel().getVersion());
          start.setX(box.getPosition().getX());
          start.setY(box.getPosition().getY());
          start.setZ(box.getPosition().getZ());
          curveSegment.setStart(start);
          curveSegment.setEnd(new Point(box.getPosition().getX() + box.getDimensions().getWidth(),
              box.getPosition().getY() + box.getDimensions().getHeight()));
          curve.addCurveSegment(curveSegment);
        }
      } else {
        curve = null;
      }
    }
    if (curve != null) {
      curve = new Curve(curve);
      for (int i = curve.getChildCount() - 1; i >= 0; i--) {
        Point start = curve.getCurveSegment(i).getStart();
        Point end = curve.getCurveSegment(i).getEnd();
        if (start.equals(end)) {
          curve.removeCurveSegment(i);
        }
      }
    }
    return curve;
  }

  private Class<? extends ReactionNode> getReactionNodeClass(SpeciesReferenceGlyph speciesRefernceGlyph) {
    Class<? extends ReactionNode> nodeClass = null;
    if (speciesRefernceGlyph.getRole() != null) {
      switch (speciesRefernceGlyph.getRole()) {
      case ACTIVATOR:
        nodeClass = Trigger.class;
        break;
      case INHIBITOR:
        nodeClass = Inhibition.class;
        break;
      case PRODUCT:
        nodeClass = Product.class;
        break;
      case SIDEPRODUCT:
        nodeClass = Product.class;
        break;
      case SIDESUBSTRATE:
        nodeClass = Reactant.class;
        break;
      case SUBSTRATE:
        nodeClass = Reactant.class;
        break;
      case MODIFIER:
        nodeClass = Modifier.class;
        break;
      case UNDEFINED:
        break;
      }
    }
    return nodeClass;
  }

  private void assignRenderDataToReaction(ReactionGlyph glyph, Reaction reactionWithLayout)
      throws InvalidInputDataExecption {
    LocalStyle style = getStyleForGlyph(glyph);
    if (style != null) {
      applyStyleToReaction(reactionWithLayout, style);
    }
  }

  public void applyStyleToReaction(Reaction reactionWithLayout, LocalStyle style) {
    RenderGroup group = style.getGroup();
    if (group.isSetStroke()) {
      Color color = getColorByColorDefinition(group.getStroke());
      for (AbstractNode node : reactionWithLayout.getNodes()) {
        node.getLine().setColor(color);
      }
      reactionWithLayout.getLine().setColor(color);
    }
    if (group.isSetStrokeWidth()) {
      for (AbstractNode node : reactionWithLayout.getNodes()) {
        node.getLine().setWidth(group.getStrokeWidth());
      }
      reactionWithLayout.getLine().setWidth(group.getStrokeWidth());
    }
    if (group.isSetStrokeDashArray()) {
      LineType type = LineType.getTypeByDashArray(group.getStrokeDashArray());
      for (AbstractNode node : reactionWithLayout.getNodes()) {
        node.getLine().setType(type);
      }
      reactionWithLayout.getLine().setType(type);
    }
    if (group.isSetEndHead()) {
      try {
        ArrowType type = ArrowType.valueOf(group.getEndHead().replaceAll("line_ending_", ""));
        for (Product node : reactionWithLayout.getProducts()) {
          node.getLine().getEndAtd().setArrowType(type);
        }
      } catch (Exception e) {
        logger.warn(new ElementUtils().getElementTag(reactionWithLayout) + "Problematic arrow type: "
            + group.getStroke(), e);
      }
    }
  }

  private PolylineData getLineFromReferenceGlyph(SpeciesReferenceGlyph speciesRefernceGlyph) {
    PolylineData line = null;
    for (CurveSegment segment : speciesRefernceGlyph.getCurve().getListOfCurveSegments()) {
      Point2D start = new Point2D.Double(segment.getStart().getX(), segment.getStart().getY());
      Point2D end = new Point2D.Double(segment.getEnd().getX(), segment.getEnd().getY());
      if (!start.equals(end)) {
        if (line == null) {
          line = new PolylineData(start, end);
        } else {
          line.addPoint(end);
        }
      }
    }
    if (line == null) {
      return new PolylineData();
    }
    return line;
  }

  private void updateKinetics(Reaction reactionWithLayout, String newElementId, String oldElementId) {
    SbmlKinetics kinetics = reactionWithLayout.getKinetics();
    if (kinetics != null) {
      String newDefinition = kinetics.getDefinition().replace(">" + oldElementId + "<", ">" + newElementId + "<");
      kinetics.setDefinition(newDefinition);
      Element elementToRemove = null;
      for (Element element : kinetics.getElements()) {
        if (element.getElementId().equals(oldElementId)) {
          elementToRemove = element;
        }
      }
      if (elementToRemove != null) {
        kinetics.removeElement(elementToRemove);
        kinetics.addElement(getMinervaModel().getElementByElementId(newElementId));
      }
    }

  }

  protected Reaction parse(org.sbml.jsbml.Reaction sbmlReaction) throws InvalidInputDataExecption {
    Reaction reaction = new Reaction();
    assignBioEntityData(sbmlReaction, reaction);
    reaction.setIdReaction(sbmlReaction.getId());
    reaction.setReversible(sbmlReaction.isReversible());
    if (sbmlReaction.getKineticLaw() != null) {
      if (sbmlReaction.getKineticLaw().getMath() == null) {
        logger.warn(eu.getElementTag(reaction) + "Reaction contains empty kinetic law.");
      } else {
        reaction.setKinetics(createMinervaKinetics(sbmlReaction.getKineticLaw()));
      }
    }
    for (SpeciesReference reactant : sbmlReaction.getListOfReactants()) {
      Species element = getMinervaModel().getElementByElementId(reactant.getSpecies());
      if (element == null) {
        throw new InvalidInputDataExecption(
            eu.getElementTag(reaction) + "Species with id " + reactant.getSpecies() + " cannot be found");
      }
      reaction.addReactant(new Reactant(element));
    }
    for (SpeciesReference product : sbmlReaction.getListOfProducts()) {
      Species element = getMinervaModel().getElementByElementId(product.getSpecies());
      if (element == null) {
        throw new InvalidInputDataExecption(
            eu.getElementTag(reaction) + "Species with id " + product.getSpecies() + " cannot be found");
      }
      reaction.addProduct(new Product(element));
    }

    for (ModifierSpeciesReference modifier : sbmlReaction.getListOfModifiers()) {
      Species element = getMinervaModel().getElementByElementId(modifier.getSpecies());
      Class<? extends Modifier> nodeClass = SBOTermModifierType.getTypeSBOTerm(modifier.getSBOTermID());
      try {
        Modifier newNode = nodeClass.getConstructor(Element.class).newInstance(element);
        reaction.addModifier(newNode);
      } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
          | NoSuchMethodException | SecurityException e) {
        throw new InvalidInputDataExecption("Problem with creating modifier", e);
      }
    }

    if (reaction.getReactants().size() == 0) {
      Species element = speciesParser.getArtifitialInput();
      logger.warn(eu.getElementTag(reaction) + "Reaction doesn't have any reactant. Adding artifitial.");
      reaction.addReactant(new Reactant(element));
    }

    if (reaction.getProducts().size() == 0) {
      Species element = speciesParser.getArtifitialOutput();
      logger.warn(eu.getElementTag(reaction) + "Reaction doesn't have any products. Adding artifitial.");
      reaction.addProduct(new Product(element));
    }

    String sboTerm = sbmlReaction.getSBOTermID();

    reaction = createProperReactionClassForSboTerm(reaction, sboTerm);

    return reaction;
  }

  private Reaction createProperReactionClassForSboTerm(Reaction reaction, String sboTerm)
      throws InvalidInputDataExecption {
    Class<? extends Reaction> reactionClass = SBOTermReactionType.getTypeSBOTerm(sboTerm);
    try {
      reaction = reactionClass.getConstructor(Reaction.class).newInstance(reaction);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
        | NoSuchMethodException | SecurityException e) {
      if (e.getCause() instanceof InvalidReactionParticipantNumberException) {
        try {
          logger.warn(eu.getElementTag(reaction) + "SBO term indicated " + reactionClass.getSimpleName()
              + " class. However, number of reactants/products is insufficient for such class type. Using default.");
          reactionClass = SBOTermReactionType.getTypeSBOTerm(null);
          reaction = reactionClass.getConstructor(Reaction.class).newInstance(reaction);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
            | NoSuchMethodException | SecurityException e1) {
          throw new InvalidInputDataExecption(eu.getElementTag(reaction) + "Problem with creating reaction", e);
        }
      } else {
        throw new InvalidInputDataExecption(eu.getElementTag(reaction) + "Problem with creating reaction", e);
      }
    }
    return reaction;
  }

  private SbmlKinetics createMinervaKinetics(KineticLaw kineticLaw) throws InvalidInputDataExecption {
    SbmlKinetics result = new SbmlKinetics();
    result.setDefinition(kineticLaw.getMath().toMathML());

    SbmlParameterParser parameterParser = new SbmlParameterParser(getSbmlModel(), getMinervaModel());
    result.addParameters(parameterParser.parseList((Collection<LocalParameter>) kineticLaw.getListOfLocalParameters()));

    try {
      Node node = XmlParser.getXmlDocumentFromString(result.getDefinition());
      Set<SbmlArgument> elementsUsedInKinetics = new HashSet<>();
      for (Node ciNode : XmlParser.getAllNotNecessirellyDirectChild("ci", node)) {
        List<String> attributesToRemove = new ArrayList<>();
        for (int y = 0; y < ciNode.getAttributes().getLength(); y++) {
          Node attr = ciNode.getAttributes().item(y);
          attributesToRemove.add(attr.getNodeName());
        }
        for (String attributeName : attributesToRemove) {
          ciNode.getAttributes().removeNamedItem(attributeName);
          logger.warn("Kinetics attribute not supported: " + attributeName);
        }

        String id = XmlParser.getNodeValue(ciNode).trim();
        SbmlArgument element = getMinervaModel().getElementByElementId(id);
        if (element == null) {
          element = result.getParameterById(id);
        }
        if (element == null) {
          element = getMinervaModel().getParameterById(id);
        }
        if (element == null) {
          element = getMinervaModel().getFunctionById(id);
        }
        if (element != null) {
          ciNode.setTextContent(element.getElementId());
          elementsUsedInKinetics.add(element);
        } else if ("default".equals(id)) {
          // default compartment is skipped
        } else {
          throw new InvalidXmlSchemaException("Unknown symbol in kinetics: " + id);
        }
      }
      result.addArguments(elementsUsedInKinetics);
      result.setDefinition(XmlParser.nodeToString(node));
    } catch (InvalidXmlSchemaException e) {
      throw new InvalidInputDataExecption(e);
    }

    return result;
  }

  public void validateReactions(Set<Reaction> reactions) throws InvalidInputDataExecption {
    for (Reaction reaction : reactions) {
      if (reaction.getReactants().size() == 0) {
        throw new InvalidInputDataExecption(
            eu.getElementTag(reaction) + "At least one reactant is required for reaction");
      }
      if (reaction.getProducts().size() == 0) {
        throw new InvalidInputDataExecption(
            eu.getElementTag(reaction) + "At least one product is required for reaction");
      }
    }

  }

}
