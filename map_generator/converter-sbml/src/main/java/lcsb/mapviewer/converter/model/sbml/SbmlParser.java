package lcsb.mapviewer.converter.model.sbml;

import java.util.*;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;
import org.sbml.jsbml.ext.SBasePlugin;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;
import org.sbml.jsbml.ext.multi.MultiModelPlugin;
import org.springframework.stereotype.Component;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.layout.ApplySimpleLayoutModelCommand;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.converter.*;
import lcsb.mapviewer.converter.model.sbml.compartment.SbmlCompartmentParser;
import lcsb.mapviewer.converter.model.sbml.reaction.SbmlReactionParser;
import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesParser;
import lcsb.mapviewer.converter.model.sbml.units.SbmlUnitsParser;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;

@Component
public class SbmlParser extends Converter {

  /**
   * Default class logger.
   */
  Logger logger = LogManager.getLogger(SbmlParser.class);

  boolean provideDefaults = true;

  @Override
  public Model createModel(ConverterParams params) throws InvalidInputDataExecption {
    try {
      Model model = new ModelFullIndexed(null);

      if (params.getFilename() != null) {
        model.setName(FilenameUtils.getBaseName(params.getFilename()));
      }
      SBMLDocument sbmlDocument = SBMLReader.read(params.getInputStream());
      org.sbml.jsbml.Model sbmlModel = sbmlDocument.getModel();
      model.setIdModel(sbmlModel.getId());
      if (model.getIdModel() != null && model.getIdModel().isEmpty()) {
        model.setIdModel(null);
      }
      model.setName(sbmlModel.getName());
      model.setNotes(NotesUtility.extractNotes(sbmlModel));

      checkAvailableExtensions(sbmlModel);
      Layout layout = getSbmlLayout(sbmlModel);
      boolean layoutExists = layout != null;

      SbmlCompartmentParser compartmentParser = new SbmlCompartmentParser(sbmlModel, model);
      SbmlSpeciesParser speciesParser = new SbmlSpeciesParser(sbmlModel, model);
      SbmlReactionParser reactionParser = new SbmlReactionParser(sbmlModel, model, speciesParser, compartmentParser);
      SbmlUnitsParser unitParser = new SbmlUnitsParser(sbmlModel, model);
      SbmlParameterParser parameterParser = new SbmlParameterParser(sbmlModel, model);
      SbmlFunctionParser functionParser = new SbmlFunctionParser(sbmlModel, model);

      model.addMiriamData(compartmentParser.extractMiriamDataFromAnnotation(sbmlModel.getAnnotation()));
      model.addAuthors(compartmentParser.extractAuthorsFromAnnotation(sbmlModel.getAnnotation()));
      model.addModificationDates(compartmentParser.extractModificationDatesFromAnnotation(sbmlModel.getAnnotation()));
      model.setCreationDate(compartmentParser.extractCreationDateFromAnnotation(sbmlModel.getAnnotation()));

      model.addUnits(unitParser.parseList(sbmlModel));
      model.addParameters(parameterParser.parseList(sbmlModel));
      model.addFunctions(functionParser.parseList(sbmlModel));

      model.addElements(compartmentParser.parseList());
      model.addElements(speciesParser.parseList());
      model.addReactions(reactionParser.parseList());

      if (layoutExists) {
        if (layout.getDimensions() != null) {
          model.setWidth(layout.getDimensions().getWidth());
          model.setHeight(layout.getDimensions().getHeight());

        }
        compartmentParser.mergeLayout(model.getCompartments());
        speciesParser.mergeLayout(model.getSpeciesList());
        reactionParser.mergeLayout(model.getReactions());
      }

      reactionParser.validateReactions(model.getReactions());

      if (sbmlModel.getConstraintCount() > 0) {
        logger.warn("Constraints not implemented for model");
      }
      if (sbmlModel.getConversionFactorInstance() != null) {
        logger.warn("ConversionFactor not implemented for model");
      }
      if (sbmlModel.getEventCount() > 0 || sbmlModel.getEventAssignmentCount() > 0) {
        logger.warn("Handling of Events is not implemented for model");
      }
      if (sbmlModel.getInitialAssignmentCount() > 0) {
        logger.warn("InitialAssignment not implemented for model");
      }
      if (sbmlModel.getRuleCount() > 0) {
        logger.warn("Rule not implemented for model");
      }
      if (layoutExists) {
        addComplexRelationsBasedOnCoordinates(model);
      }
      createLayout(model, layout, params.isSizeAutoAdjust(), reactionParser);
      return model;
    } catch (XMLStreamException e) {
      throw new InvalidInputDataExecption(e);
    }
  }

  @Override
  public String model2String(Model model) throws ConverterException {
    try {
      SbmlExporter sbmlExporter = new SbmlExporter();
      sbmlExporter.setProvideDefaults(provideDefaults);
      return sbmlExporter.toXml(model);
    } catch (InconsistentModelException e) {
      throw new ConverterException(e);
    }
  }

  @Override
  public String getCommonName() {
    return "SBML";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.SBML;
  }

  @Override
  public String getFileExtension() {
    return "xml";
  }

  private void addComplexRelationsBasedOnCoordinates(Model model) {
    for (Element element : model.getElements()) {
      if (element instanceof Species) {
        Species species = (Species) element;
        if (species.getComplex() == null && element.getWidth() != 0 || element.getHeight() != 0) {
          Complex complex = findComplexForElement(species, model);
          if (complex != null) {
            complex.addSpecies(species);
          }
        }
      }
    }
  }

  private Complex findComplexForElement(Species species, Model model) {
    Complex result = null;
    for (Element element : model.getElements()) {
      if (element instanceof Complex && element != species) {
        Complex complex = (Complex) element;
        if (complex.getWidth() != 0 || complex.getHeight() != 0) {
          if (complex.contains(species) && complex.getSize() > species.getSize()) {
            if (result == null) {
              result = complex;
            } else if (result.getSize() > complex.getSize()) {
              result = complex;
            }
          }
        }
      }
    }
    return result;
  }

  private void createLayout(Model model, Layout layout, boolean resize, SbmlReactionParser parser)
      throws InvalidInputDataExecption {
    if (model.getWidth() <= Configuration.EPSILON) {
      double maxY = 1;
      double maxX = 1;
      for (Element element : model.getElements()) {
        maxY = Math.max(maxY, element.getY() + element.getHeight() + 10);
        maxX = Math.max(maxX, element.getX() + element.getWidth() + 10);
      }
      if (resize) {
        model.setWidth(maxX);
        model.setHeight(maxY);
      }
    }
    Collection<BioEntity> bioEntitesRequiringLayout = new HashSet<>();

    for (Element element : model.getElements()) {
      if (element.getWidth() == 0 || element.getHeight() == 0) {
        bioEntitesRequiringLayout.add(element);
      } else if (element instanceof SpeciesWithModificationResidue) {
        for (ModificationResidue mr : ((SpeciesWithModificationResidue) element).getModificationResidues()) {
          if (mr.getPosition() == null) {
            bioEntitesRequiringLayout.add(element);
          }
        }
      }
    }
    for (Reaction reaction : model.getReactions()) {
      if (!hasLayout(reaction)) {
        updateModifierTypes(reaction);
        bioEntitesRequiringLayout.add(reaction);
      }
    }
    try {
      if (bioEntitesRequiringLayout.size() > 0) {
        new ApplySimpleLayoutModelCommand(model, bioEntitesRequiringLayout, true).execute();
      }
    } catch (CommandExecutionException e) {
      throw new InvalidInputDataExecption("Problem with generating layout", e);
    }
    new ZIndexPopulator().populateZIndex(model);
  }

  private void updateModifierTypes(Reaction reaction) {
    Set<Modifier> modifiersToBeRemoved = new HashSet<>();
    Set<Modifier> modifiersToBeAdded = new HashSet<>();
    for (Modifier modifier : reaction.getModifiers()) {
      if (modifier.getClass() == Modifier.class) {
        modifiersToBeRemoved.add(modifier);
        modifiersToBeAdded.add(new Modulation(modifier.getElement()));
      }
    }
    for (Modifier modifier : modifiersToBeRemoved) {
      reaction.removeModifier(modifier);
    }
    for (Modifier modifier : modifiersToBeAdded) {
      reaction.addModifier(modifier);
    }
  }

  private boolean hasLayout(Reaction reaction) {
    for (AbstractNode node : reaction.getNodes()) {
      if (node.getLine() == null) {
        return false;
      } else if (node.getLine().length() == 0) {
        return false;
      }
    }

    return true;
  }

  private Layout getSbmlLayout(org.sbml.jsbml.Model sbmlModel) {
    Layout layout = null;

    if (sbmlModel.getExtensionCount() > 0) {
      for (SBasePlugin plugin : sbmlModel.getExtensionPackages().values()) {
        if (plugin.getClass().equals(org.sbml.jsbml.ext.layout.LayoutModelPlugin.class)) {
          LayoutModelPlugin layoutPlugin = (LayoutModelPlugin) plugin;
          if (layoutPlugin.getLayoutCount() == 0) {
            logger.warn("Layout plugin available but no layouts defined");
          } else if (layoutPlugin.getLayoutCount() > 1) {
            logger.warn(layoutPlugin.getLayoutCount() + " layouts defined. Using first one.");
            layout = layoutPlugin.getLayout(0);
          } else {
            layout = layoutPlugin.getLayout(0);
          }
        }
      }
    }
    return layout;
  }

  private void checkAvailableExtensions(org.sbml.jsbml.Model sbmlModel) {
    if (sbmlModel.getExtensionCount() > 0) {
      for (SBasePlugin plugin : sbmlModel.getExtensionPackages().values()) {
        if (!plugin.getClass().equals(LayoutModelPlugin.class) &&
            !plugin.getClass().equals(MultiModelPlugin.class)) {
          logger.warn("Unknown sbml plugin: " + plugin);
        }
      }
    }
  }

  public void setProvideDefaults(boolean b) {
    this.provideDefaults = b;
  }

}
