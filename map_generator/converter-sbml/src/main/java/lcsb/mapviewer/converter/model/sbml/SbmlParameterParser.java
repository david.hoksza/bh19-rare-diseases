package lcsb.mapviewer.converter.model.sbml;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.*;

import lcsb.mapviewer.model.map.kinetics.SbmlParameter;

public class SbmlParameterParser extends SbmlBioEntityParser {
  Logger logger = LogManager.getLogger(SbmlParameterParser.class);

  public SbmlParameterParser(Model sbmlModel, lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(sbmlModel, minervaModel);
  }

  protected SbmlParameter parse(org.sbml.jsbml.QuantityWithUnit unitDefinition) {
    SbmlParameter result = new SbmlParameter(unitDefinition.getId());
    result.setName(unitDefinition.getName());
    result.setValue(unitDefinition.getValue());
    result.setUnits(getMinervaModel().getUnitsByUnitId(unitDefinition.getUnits()));
    return result;
  }

  protected ListOf<org.sbml.jsbml.Parameter> getSbmlElementList(Model sbmlModel) {
    return sbmlModel.getListOfParameters();
  }

  public Collection<SbmlParameter> parseList(Model sbmlModel) {
    Collection<SbmlParameter> result = parseList(getSbmlElementList(sbmlModel));
    return result;
  }

  public Collection<SbmlParameter> parseList(Collection<? extends QuantityWithUnit> parameters) {
    List<SbmlParameter> result = new ArrayList<>();
    for (org.sbml.jsbml.QuantityWithUnit unitDefinition : parameters) {
      result.add(parse(unitDefinition));
    }
    return result;
  }

}
