package lcsb.mapviewer.converter.model.sbml;

import java.awt.*;
import java.util.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.render.LocalStyle;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Element;

public abstract class SbmlElementParser<T extends org.sbml.jsbml.Symbol> extends SbmlBioEntityParser {

  Map<String, Element> elementBySbmlId = new HashMap<>();
  Map<String, String> sbmlIdByElementId = new HashMap<>();
  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(SbmlElementParser.class);

  public SbmlElementParser(Model sbmlModel, lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(sbmlModel, minervaModel);
  }

  public List<Element> parseList() throws InvalidInputDataExecption {
    List<Element> result = new ArrayList<>();
    for (T sbmlElement : getSbmlElementList()) {
      Element element = parse(sbmlElement);
      if (element != null) {
        result.add(element);
        elementBySbmlId.put(element.getElementId(), element);
      }
    }
    return result;
  }

  protected abstract ListOf<T> getSbmlElementList();

  public Element getAnyElementBySbmlElementId(String id) {
    return elementBySbmlId.get(id);
  }

  public String getSbmlIdByElementId(String compartmentId) {
    return sbmlIdByElementId.get(compartmentId);
  }

  protected List<Element> mergeLayout(List<? extends Element> elements)
      throws InvalidInputDataExecption {
    Set<String> used = new HashSet<>();
    List<Element> result = new ArrayList<>();
    for (Element species : elements) {
      elementBySbmlId.put(species.getElementId(), species);
    }

    for (Pair<String, AbstractReferenceGlyph> idGlyphPair : getGlyphs()) {
      String id = idGlyphPair.getLeft();
      Element source = elementBySbmlId.get(id);
      if (source == null) {
        throw new InvalidInputDataExecption("Layout contains invalid Species id: " + idGlyphPair.getLeft());
      }
      LocalStyle style = getStyleForGlyph(idGlyphPair.getRight());

      if (style != null) {
        applyStyleToElement(source, style);
      }

      used.add(source.getElementId());
      AbstractReferenceGlyph glyph = idGlyphPair.getRight();
      if (glyph.getId() == null || glyph.getId().equals("")) {
        throw new InvalidInputDataExecption("Glyph for Species " + idGlyphPair.getLeft() + " doesn't have id");
      }
      Element elementWithLayout = createElementWithLayout(source, glyph);
      getMinervaModel().addElement(elementWithLayout);
      result.add(elementWithLayout);
      elementBySbmlId.put(id, elementWithLayout);
      elementBySbmlId.put(elementWithLayout.getElementId(), elementWithLayout);
      sbmlIdByElementId.put(elementWithLayout.getElementId(), id);
    }
    for (Element element : elements) {
      if (!used.contains(element.getElementId())) {
        logger.warn("Layout doesn't contain information about Element: " + element.getElementId());
        result.add(element);
      } else {
        getMinervaModel().removeElement(element);
      }
    }
    return result;
  }

  private Element createElementWithLayout(Element source, AbstractReferenceGlyph glyph)
      throws InvalidInputDataExecption {
    Element elementWithLayout = source.copy();
    elementWithLayout.setElementId(glyph.getId());
    elementWithLayout.setX(glyph.getBoundingBox().getPosition().getX());
    elementWithLayout.setY(glyph.getBoundingBox().getPosition().getY());
    if (glyph.getBoundingBox().getPosition().isSetZ()) {
      elementWithLayout.setZ((int) glyph.getBoundingBox().getPosition().getZ());
    }
    elementWithLayout.setWidth(glyph.getBoundingBox().getDimensions().getWidth());
    elementWithLayout.setHeight(glyph.getBoundingBox().getDimensions().getHeight());

    LocalStyle style = getStyleForGlyph(glyph);
    if (style != null) {
      applyStyleToElement(elementWithLayout, style);
    }
    return elementWithLayout;
  }

  protected void applyStyleToElement(Element elementWithLayout, LocalStyle style) {
    if (style.getGroup().isSetFill()) {
      Color backgroundColor = getColorByColorDefinition(style.getGroup().getFill());
      elementWithLayout.setFillColor(backgroundColor);
      if (elementWithLayout instanceof Compartment) {
        elementWithLayout.setBorderColor(backgroundColor);
      }
    }
    if (style.getGroup().isSetFontSize()) {
      elementWithLayout.setFontSize(style.getGroup().getFontSize());
    }
  }

  protected abstract List<Pair<String, AbstractReferenceGlyph>> getGlyphs();

  protected abstract Element parse(T species) throws InvalidInputDataExecption;

}
