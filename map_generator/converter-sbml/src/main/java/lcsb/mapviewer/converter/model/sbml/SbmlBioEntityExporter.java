package lcsb.mapviewer.converter.model.sbml;

import java.awt.Color;
import java.util.*;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.SBasePlugin;
import org.sbml.jsbml.ext.layout.*;
import org.sbml.jsbml.ext.multi.MultiModelPlugin;
import org.sbml.jsbml.ext.render.*;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.modelutils.map.ElementUtils;

public abstract class SbmlBioEntityExporter<T extends BioEntity, S extends org.sbml.jsbml.AbstractNamedSBase> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(SbmlBioEntityExporter.class);
  protected Map<String, S> sbmlElementByElementNameAndCompartmentName = new HashMap<>();
  Map<String, AbstractReferenceGlyph> sbmlGlyphByElementId = new HashMap<>();
  /**
   * SBML Layout used when exporting map.
   */
  private Layout layout;
  /**
   * Map that we are exporting.
   */
  private lcsb.mapviewer.model.map.model.Model minervaModel;
  /**
   * SBML model to which we are exporting.
   */
  private Model sbmlModel;
  private Map<String, S> sbmlElementByElementId = new HashMap<>();
  private int idCounter = 0;

  private Set<SbmlExtension> sbmlExtensions = new HashSet<>();

  private boolean provideDefaults;

  private Pattern sbmlValidIdMatcher;

  /**
   * Map with elementId mapping for identifiers that cannot be used in sbml.
   */
  private Map<String, String> correctedElementId = new HashMap<>();

  public SbmlBioEntityExporter(Model sbmlModel, lcsb.mapviewer.model.map.model.Model minervaModel,
      Collection<SbmlExtension> sbmlExtensions) {
    this.sbmlModel = sbmlModel;
    this.layout = getLayout(sbmlModel);
    this.minervaModel = minervaModel;
    this.sbmlExtensions.addAll(sbmlExtensions);

    String underscore = "_";
    String letter = "a-zA-Z";
    String digit = "0-9";
    String idChar = '[' + letter + digit + underscore + ']';
    sbmlValidIdMatcher = Pattern.compile("^[" + letter + underscore + "]" + idChar + '*');

  }

  public void exportElements() throws InconsistentModelException {
    Collection<T> speciesList = getElementList();
    for (T bioEntity : speciesList) {
      try {
        S sbmlElement = getSbmlElement(bioEntity);

        if (sbmlElementByElementId.get(getElementId(bioEntity)) != null) {
          throw new InconsistentModelException("More than one species with id: " + getElementId(bioEntity));
        }
        sbmlElementByElementId.put(getElementId(bioEntity), sbmlElement);
      } catch (Exception e) {
        throw new InconsistentModelException(new ElementUtils().getElementTag(bioEntity) +
            "Problem with exporting bioEntity", e);
      }
    }
    if (isExtensionEnabled(SbmlExtension.LAYOUT)) {
      for (T bioEntity : speciesList) {
        try {
          AbstractReferenceGlyph elementGlyph = createGlyph(bioEntity);
          sbmlGlyphByElementId.put(getElementId(bioEntity), elementGlyph);
        } catch (Exception e) {
          throw new InconsistentModelException(new ElementUtils().getElementTag(bioEntity) +
              "Problem with exporting bioEntity", e);
        }
      }
    }
  }

  /**
   * Checks if exporter should used extension.
   * 
   * @param sbmlExtension
   *          {@link SbmlExtension} to be checked
   * @return <code>true</code> if extension should be supported by exported file
   */
  protected boolean isExtensionEnabled(SbmlExtension sbmlExtension) {
    return sbmlExtensions.contains(sbmlExtension);
  }

  protected abstract Collection<T> getElementList();

  public abstract S createSbmlElement(T element) throws InconsistentModelException;

  public S getSbmlElement(T element) throws InconsistentModelException {
    String mapKey = getSbmlIdKey(element);
    if (sbmlElementByElementNameAndCompartmentName.get(mapKey) == null) {
      S sbmlElement = createSbmlElement(element);
      XmlAnnotationParser parser = new XmlAnnotationParser();
      String rdf = parser.dataSetToXmlString(element.getMiriamData(), sbmlElement.getId());
      try {
        sbmlElement.setAnnotation(rdf);
      } catch (XMLStreamException e1) {
        throw new InconsistentModelException(e1);
      }
      sbmlElement.setName(element.getName());
      try {
        sbmlElement.setNotes(NotesUtility.prepareEscapedXmlNotes(element.getNotes()));
      } catch (XMLStreamException e) {
        throw new InvalidStateException(e);
      }
      sbmlElementByElementNameAndCompartmentName.put(mapKey, sbmlElement);
    }
    return sbmlElementByElementNameAndCompartmentName.get(mapKey);
  }

  protected abstract String getSbmlIdKey(T element);

  protected abstract void assignLayoutToGlyph(T element, AbstractReferenceGlyph compartmentGlyph);

  protected AbstractReferenceGlyph createGlyph(T element) {
    String sbmlElementId = sbmlElementByElementId.get(getElementId(element)).getId();
    String glyphId = getElementId(element);
    AbstractReferenceGlyph elementGlyph = createElementGlyph(sbmlElementId, glyphId);
    assignLayoutToGlyph(element, elementGlyph);
    return elementGlyph;
  }

  protected abstract AbstractReferenceGlyph createElementGlyph(String sbmlCompartmentId, String glyphId);

  protected String getNextId() {
    return (idCounter++) + "";
  }

  private Layout getLayout(org.sbml.jsbml.Model sbmlModel) {
    Layout layout = null;

    if (sbmlModel.getExtensionCount() > 0) {
      for (SBasePlugin plugin : sbmlModel.getExtensionPackages().values()) {
        if (plugin.getClass().equals(org.sbml.jsbml.ext.layout.LayoutModelPlugin.class)) {
          LayoutModelPlugin layoutPlugin = (LayoutModelPlugin) plugin;
          if (layoutPlugin.getLayoutCount() == 0) {
            logger.warn("Layout plugin available but no layouts defined");
          } else if (layoutPlugin.getLayoutCount() > 1) {
            logger.warn(layoutPlugin.getLayoutCount() + " layouts defined. Using first one.");
            layout = layoutPlugin.getLayout(0);
          } else {
            layout = layoutPlugin.getLayout(0);
          }
        }
      }
    }
    return layout;
  }

  protected Layout getLayout() {
    return layout;
  }

  protected RenderLayoutPlugin getRenderPlugin() {
    if (getLayout().getExtensionCount() > 0) {
      return (RenderLayoutPlugin) getLayout().getExtension("render");
    }
    return null;
  }

  protected MultiModelPlugin getMultiPlugin() {
    return (MultiModelPlugin) sbmlModel.getExtension("multi");
  }

  protected lcsb.mapviewer.model.map.model.Model getMinervaModel() {
    return minervaModel;
  }

  protected Model getSbmlModel() {
    return sbmlModel;
  }

  public S getSbmlElementByElementId(String id) {
    return sbmlElementByElementId.get(id);
  }

  public AbstractReferenceGlyph getSbmlGlyphByElementId(String elementId) {
    return sbmlGlyphByElementId.get(elementId);
  }

  protected ColorDefinition getColorDefinition(Color color) {
    RenderLayoutPlugin renderPlugin = getRenderPlugin();

    LocalRenderInformation renderInformation = getRenderInformation(renderPlugin);

    for (ColorDefinition cd : renderInformation.getListOfColorDefinitions()) {
      if (cd.getValue().equals(color)) {
        return cd;
      }
    }
    ColorDefinition colorDefinition = new ColorDefinition("color_" + XmlParser.colorToString(color), color);

    renderInformation.addColorDefinition(colorDefinition);
    return colorDefinition;
  }

  protected LocalRenderInformation getRenderInformation(RenderLayoutPlugin renderPlugin) {
    LocalRenderInformation renderInformation = null;
    for (LocalRenderInformation lri : renderPlugin.getListOfLocalRenderInformation()) {
      if (lri.getId().equals("minerva_definitions")) {
        renderInformation = lri;
      }
    }
    if (renderInformation == null) {
      renderInformation = new LocalRenderInformation("minerva_definitions");
      renderPlugin.addLocalRenderInformation(renderInformation);
    }
    return renderInformation;
  }

  protected void assignStyleToGlyph(AbstractReferenceGlyph speciesGlyph, LocalStyle style) {
    RenderGraphicalObjectPlugin rgop = new RenderGraphicalObjectPlugin(speciesGlyph);
    if (style.getGroup().isSetEndHead()) {
      LineEnding lineEnding = createLineEndingStyle(style.getGroup().getEndHead());
      style.getGroup().setEndHead(lineEnding.getId());
    }
    style.getIDList().add(speciesGlyph.getId());
    speciesGlyph.addExtension(RenderConstants.shortLabel, rgop);
  }

  protected LineEnding createLineEndingStyle(String endHead) {
    String arrowTypeId = endHead.replaceAll("line_ending_", "");
    LocalRenderInformation renderInformation = getRenderInformation(getRenderPlugin());
    LineEnding result = renderInformation.getListOfLineEndings().get("line_ending_" + arrowTypeId);
    if (result != null) {
      return result;
    }
    ArrowType arrowType = ArrowType.valueOf(arrowTypeId);
    result = new LineEnding();
    result.setId("line_ending_" + arrowTypeId);
    result.setGroup(new RenderGroup());
    renderInformation.getListOfLineEndings().add(result);
    switch (arrowType) {
    case FULL:
      createFullLineEnding(result);
      break;
    case BLANK:
      createBlankLineEnding(result);
      break;
    case FULL_CROSSBAR:
      createFullCrossBarLineEnding(result);
      break;
    case BLANK_CROSSBAR:
      createBlankCrossBarLineEnding(result);
      break;
    case CROSSBAR:
      createCrossBarLineEnding(result);
      break;
    case DIAMOND:
      createDiamondLineEnding(result);
      break;
    case OPEN:
      createOpenLineEnding(result);
      break;
    case CIRCLE:
      createCircleLineEnding(result);
      break;
    default:
      logger.warn("Unknown arrow type: " + arrowType);
    case NONE:
      break;
    }

    return result;
  }

  private void createCircleLineEnding(LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(-2, 0, 4, 4, 0);
    result.getGroup().setFill(getColorDefinition(Color.WHITE).getId());
    result.setBoundingBox(boundingBox);
    Ellipse ellipse = new Ellipse();
    ellipse.setAbsoluteCx(false);
    ellipse.setAbsoluteCy(false);
    ellipse.setAbsoluteRx(true);
    ellipse.setAbsoluteRy(true);
    ellipse.setCx(0.0);
    ellipse.setCy(0.0);
    ellipse.setRx(4.0);
    ellipse.setRy(4.0);
    result.getGroup().addElement(ellipse);
  }

  private void createOpenLineEnding(LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(-12, -6, 12, 12, 0);
    result.setBoundingBox(boundingBox);
    Polygon polygon = new Polygon();
    polygon.addElement(createRenderPoint(0, 0));
    polygon.addElement(createRenderPoint(100, 50));
    polygon.addElement(createRenderPoint(0, 100));
    polygon.addElement(createRenderPoint(100, 50));
    result.getGroup().addElement(polygon);
  }

  private void createDiamondLineEnding(LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(-18, -6, 18, 12, 0);
    result.setBoundingBox(boundingBox);
    result.getGroup().setFill(getColorDefinition(Color.WHITE).getId());
    Polygon polygon = new Polygon();
    polygon.addElement(createRenderPoint(50, 0));
    polygon.addElement(createRenderPoint(100, 50));
    polygon.addElement(createRenderPoint(50, 100));
    polygon.addElement(createRenderPoint(0, 50));
    polygon.addElement(createRenderPoint(50, 0));
    result.getGroup().addElement(polygon);
  }

  private void createCrossBarLineEnding(LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(0, -6, 1, 12, 0);
    result.setBoundingBox(boundingBox);
    Polygon crossBar = new Polygon();
    crossBar.addElement(createRenderPoint(0, 0));
    crossBar.addElement(createRenderPoint(0, 100));
    result.getGroup().addElement(crossBar);
  }

  private void createBlankLineEnding(LineEnding result) {
    createFullLineEnding(result);
    result.getGroup().setFill(getColorDefinition(Color.WHITE).getId());
  }

  private void createFullLineEnding(LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(-12, -6, 12, 12, 0);
    result.setBoundingBox(boundingBox);
    result.getGroup().setFill(getColorDefinition(Color.BLACK).getId());
    Polygon polygon = new Polygon();
    polygon.addElement(createRenderPoint(0, 0));
    polygon.addElement(createRenderPoint(100, 50));
    polygon.addElement(createRenderPoint(0, 100));
    polygon.addElement(createRenderPoint(0, 0));
    result.getGroup().addElement(polygon);
  }

  private void createBlankCrossBarLineEnding(LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(-18, -6, 18, 12, 0);
    result.setBoundingBox(boundingBox);
    result.getGroup().setFill(getColorDefinition(Color.WHITE).getId());
    Polygon polygon = new Polygon();
    polygon.addElement(createRenderPoint(33, 0));
    polygon.addElement(createRenderPoint(100, 50));
    polygon.addElement(createRenderPoint(33, 100));
    polygon.addElement(createRenderPoint(33, 0));
    result.getGroup().addElement(polygon);

    Polygon crossBar = new Polygon();
    crossBar.addElement(createRenderPoint(0, 0));
    crossBar.addElement(createRenderPoint(0, 100));
    result.getGroup().addElement(crossBar);
  }

  private void createFullCrossBarLineEnding(LineEnding result) {
    createBlankCrossBarLineEnding(result);
    result.getGroup().setFill(getColorDefinition(Color.BLACK).getId());
  }

  protected RenderPoint createRenderPoint(double percentX, int percentY) {
    RenderPoint result = new RenderPoint();
    result.setAbsoluteX(false);
    result.setX(percentX);
    result.setAbsoluteY(false);
    result.setY(percentY);
    return result;
  }

  protected BoundingBox createBoundingBox(double x, double y, int z, double w, double h) {
    BoundingBox boundingBox = new BoundingBox();

    boundingBox.setPosition(new Point(x, y, z));
    Dimensions dimensions = new Dimensions();

    dimensions.setWidth(w);
    dimensions.setHeight(h);
    boundingBox.setDimensions(dimensions);
    return boundingBox;
  }

  protected LocalStyle createStyle(T element) {
    LocalRenderInformation renderInformation = getRenderInformation(getRenderPlugin());
    LocalStyle style = new LocalStyle();
    style.setGroup(new RenderGroup());
    renderInformation.addLocalStyle(style);
    return style;
  }

  protected boolean isProvideDefaults() {
    return provideDefaults;
  }

  public void setProvideDefaults(boolean provideDefaults) {
    this.provideDefaults = provideDefaults;
  }

  public String getElementId(BioEntity bioEntity) {
    String result = bioEntity.getElementId();
    if (!sbmlValidIdMatcher.matcher(result).matches()) {
      if (correctedElementId.get(result) == null) {
        String newElementId = this.getClass().getSimpleName() + "_" + getNextId();
        logger.warn(new ElementUtils().getElementTag(bioEntity) + " Element id cannot be used in sbml. Changing to: "
            + newElementId);
        correctedElementId.put(result, newElementId);
      }
      result = correctedElementId.get(result);
    }

    return result;
  }

}
