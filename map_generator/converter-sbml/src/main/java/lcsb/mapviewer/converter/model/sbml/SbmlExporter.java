package lcsb.mapviewer.converter.model.sbml;

import java.io.UnsupportedEncodingException;
import java.util.*;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.*;
import org.sbml.jsbml.ext.layout.*;
import org.sbml.jsbml.ext.multi.MultiModelPlugin;
import org.sbml.jsbml.ext.render.GlobalRenderInformation;
import org.sbml.jsbml.ext.render.RenderLayoutPlugin;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.sbml.compartment.SbmlCompartmentExporter;
import lcsb.mapviewer.converter.model.sbml.reaction.SbmlReactionExporter;
import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesExporter;
import lcsb.mapviewer.converter.model.sbml.units.SbmlUnitExporter;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.species.Species;

public class SbmlExporter {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SbmlExporter.class);

  /**
   * Set of SBML extensions that should be used during export.
   */
  private Set<SbmlExtension> usedExtensions = new HashSet<>(Arrays.asList(SbmlExtension.values()));

  private boolean provideDefaults = true;

  /**
   * Export input model into SBML xml.
   * 
   * @param model
   *          input model
   * @return SBML xml string
   * @throws InconsistentModelException
   *           thrown when there is problem with input model
   */
  public String toXml(lcsb.mapviewer.model.map.model.Model model)
      throws InconsistentModelException {
    try {
      SBMLDocument doc = toSbmlDocument(model);

      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      SBMLWriter.write(doc, stream, "minerva", Configuration.getSystemVersion(null));
      return stream.toString("UTF-8")
          // TODO bug: https://github.com/sbmlteam/jsbml/issues/158
          .replace("<listOfSpeciesFeatures>", "<multi:listOfSpeciesFeatures>")
          .replace("</listOfSpeciesFeatures>", "</multi:listOfSpeciesFeatures>");
    } catch (UnsupportedEncodingException | XMLStreamException e) {
      throw new InvalidStateException(e);
    }
  }

  /**
   * Translates input model into SBML model. {@link #usedExtensions} define which
   * SBML extensions should be enabled in the SBML model.
   * 
   * @param model
   *          input model
   * @return SBML model
   * @throws InconsistentModelException
   *           thrown when there is problem with input model
   */
  protected SBMLDocument toSbmlDocument(lcsb.mapviewer.model.map.model.Model model) throws InconsistentModelException {
    SBMLDocument doc = new SBMLDocument(3, 2);
    Model result = doc.createModel();
    try {
      result.setId(model.getIdModel());
    } catch (IllegalArgumentException e) {
      logger.warn("Invalid model identifier: \"" + model.getIdModel() + "\". Ignoring.");
    }
    result.setName(model.getName());
    try {
      result.setNotes(NotesUtility.prepareEscapedXmlNotes(model.getNotes()));
    } catch (XMLStreamException e) {
      throw new InvalidStateException(e);
    }

    if (usedExtensions.contains(SbmlExtension.LAYOUT)) {
      createSbmlLayout(model, result);
    }

    if (usedExtensions.contains(SbmlExtension.MULTI)) {
      createSbmlMultiPlugin(result);
    }

    SbmlCompartmentExporter compartmentExporter = new SbmlCompartmentExporter(result, model, usedExtensions);
    compartmentExporter.setProvideDefaults(provideDefaults);

    XmlAnnotationParser parser = new XmlAnnotationParser();
    String rdf = parser.dataSetToXmlString(model.getMiriamData(), model.getAuthors(), model.getCreationDate(),
        model.getModificationDates(), model.getIdModel());
    try {
      result.setAnnotation(rdf);
    } catch (XMLStreamException e1) {
      throw new InconsistentModelException(e1);
    }

    SbmlBioEntityExporter<Species, org.sbml.jsbml.Species> speciesExporter = new SbmlSpeciesExporter(result, model,
        usedExtensions,
        compartmentExporter);
    speciesExporter.setProvideDefaults(provideDefaults);
    SbmlReactionExporter reactionExporter = new SbmlReactionExporter(result, model, speciesExporter,
        usedExtensions, compartmentExporter);
    SbmlUnitExporter unitExporter = new SbmlUnitExporter(model);
    unitExporter.setProvideDefaults(provideDefaults);
    SbmlParameterExporter parameterExporter = new SbmlParameterExporter(model);
    SbmlFunctionExporter functionExporter = new SbmlFunctionExporter(model);

    compartmentExporter.exportElements();
    speciesExporter.exportElements();
    reactionExporter.exportElements();
    unitExporter.exportUnits(result);

    parameterExporter.exportParameter(result);
    functionExporter.exportFunction(result);
    return doc;
  }

  /**
   * Create SBML layout for the given model.
   * 
   * @param model
   *          input model
   * @param result
   *          SBML model where layout should be embedded
   * @return SBML layout
   */
  public Layout createSbmlLayout(lcsb.mapviewer.model.map.model.Model model, Model result) {
    LayoutModelPlugin layoutPlugin = new LayoutModelPlugin(result);
    Layout layout = new Layout();
    layout.setId("minerva_layout");
    Dimensions dimensions = new Dimensions();
    if (model.getHeight() > 0) {
      dimensions.setHeight(model.getHeight());
    } else {
      dimensions.setHeight(640);
    }
    if (model.getWidth() > 0) {
      dimensions.setWidth(model.getWidth());
    } else {
      dimensions.setWidth(480);
    }
    layout.setDimensions(dimensions);
    layoutPlugin.add(layout);
    result.addExtension("layout", layoutPlugin);

    if (usedExtensions.contains(SbmlExtension.RENDER)) {
      createSbmlRenderPlugin(layout);
    }
    return layout;
  }

  protected MultiModelPlugin createSbmlMultiPlugin(Model result) {
    MultiModelPlugin multiPlugin = new MultiModelPlugin(result);
    result.addExtension("multi", multiPlugin);
    return multiPlugin;
  }

  /**
   * Creates SBML render plugin for SBML model.
   * 
   * @param layout
   *          SBML layout where render package will be used
   * @return render plugin
   */
  private RenderLayoutPlugin createSbmlRenderPlugin(Layout layout) {
    RenderLayoutPlugin renderPlugin = new RenderLayoutPlugin(layout);
    renderPlugin.setRenderInformation(new GlobalRenderInformation());
    layout.addExtension("render", renderPlugin);
    return renderPlugin;
  }

  /**
   * Removes set of extensions from export.
   * 
   * @param sbmlExtensions
   *          set of extensions that shouldn't be used during export
   */
  public void removeSbmlExtensions(Collection<SbmlExtension> sbmlExtensions) {
    usedExtensions.removeAll(sbmlExtensions);
  }

  /**
   * Adds {@link SbmlExtension} to export
   * 
   * @param sbmlExtension
   *          extension that should be used during export
   */
  public void addSbmlExtension(SbmlExtension sbmlExtension) {
    usedExtensions.add(sbmlExtension);
  }

  /**
   * @return set of extensions that will be used during export
   */
  public Set<SbmlExtension> getSbmlExtensions() {
    return usedExtensions;
  }

  /**
   * Removes extension from export
   * 
   * @param sbmlExtension
   *          {@link SbmlExtension} that shouldn't be used during export
   */
  public void removeSbmlExtension(SbmlExtension sbmlExtension) {
    usedExtensions.remove(sbmlExtension);
  }

  public void setProvideDefaults(boolean provideDefaults) {
    this.provideDefaults = provideDefaults;
  }
}
