package lcsb.mapviewer.converter.model.sbml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.FunctionDefinition;

import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlFunctionExporter {
  Logger logger = LogManager.getLogger(SbmlFunctionExporter.class);
  private Model minervaModel;

  public SbmlFunctionExporter(lcsb.mapviewer.model.map.model.Model minervaModel) {
    this.minervaModel = minervaModel;
  }

  public void exportFunction(org.sbml.jsbml.Model result) {
    for (SbmlFunction unit : minervaModel.getFunctions()) {
      result.addFunctionDefinition(createFunction(unit));
    }
  }

  private FunctionDefinition createFunction(SbmlFunction unit) {
    FunctionDefinition result = new FunctionDefinition();
    result.setName(unit.getName());
    String definition = unit.getDefinition();
    definition = definition.replace("lambda xmlns=\"http://www.sbml.org/sbml/level2/version4\"", "lambda");
    result.setMath(ASTNode.parseMathML(definition));
    result.setId(unit.getFunctionId());
    return result;
  }

}
