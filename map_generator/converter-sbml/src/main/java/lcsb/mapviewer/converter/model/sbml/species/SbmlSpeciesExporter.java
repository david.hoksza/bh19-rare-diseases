package lcsb.mapviewer.converter.model.sbml.species;

import java.awt.Color;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.*;
import org.sbml.jsbml.ext.multi.*;
import org.sbml.jsbml.ext.render.*;

import lcsb.mapviewer.converter.model.sbml.SbmlElementExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlExtension;
import lcsb.mapviewer.converter.model.sbml.compartment.SbmlCompartmentExporter;
import lcsb.mapviewer.converter.model.sbml.extension.multi.BioEntityFeature;
import lcsb.mapviewer.converter.model.sbml.extension.multi.MultiPackageNamingUtils;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.*;

public class SbmlSpeciesExporter extends SbmlElementExporter<Species, org.sbml.jsbml.Species> {

  private static int idCounter = 0;

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(SbmlSpeciesExporter.class);

  private SbmlCompartmentExporter compartmentExporter;
  private int modificationResidueCounter = 0;
  private int structuralStateCounter = 0;
  private Map<String, String> modificationResidueIds = new HashMap<>();

  public SbmlSpeciesExporter(Model sbmlModel,
      lcsb.mapviewer.model.map.model.Model minervaModel,
      Collection<SbmlExtension> sbmlExtensions,
      SbmlCompartmentExporter compartmentExporter) {
    super(sbmlModel, minervaModel, sbmlExtensions);
    this.compartmentExporter = compartmentExporter;
  }

  private void assignMultiExtensionData(Species element, org.sbml.jsbml.Species result) {
    MultiSpeciesPlugin multiExtension = new MultiSpeciesPlugin(result);
    MultiSpeciesType speciesType = getMultiSpeciesType(element);
    multiExtension.setSpeciesType(speciesType.getId());
    result.addExtension("multi", multiExtension);
    assignStructuralStateToMulti(element, multiExtension, speciesType);
    assignPostionToCompartmentToMulti(element, multiExtension, speciesType);
    assignElementModificationResiduesToMulti(element, multiExtension, speciesType);
    assignListOfSynonymsToMulti(element, multiExtension, speciesType);
    assignListOfFormerSymbolsToMulti(element, multiExtension, speciesType);
    assignSymbolToMulti(element, multiExtension, speciesType);
    assignFullNameToMulti(element, multiExtension, speciesType);
    assignFormulaToMulti(element, multiExtension, speciesType);
    assignDimerToMulti(element, multiExtension, speciesType);
    assignHypotheticalToMulti(element, multiExtension, speciesType);
    assignAbbreviationToMulti(element, multiExtension, speciesType);
    assignChargeToMulti(element, multiExtension, speciesType);
    assignActivityToMulti(element, multiExtension, speciesType);
  }

  private void assignElementModificationResiduesToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    if (element instanceof Protein) {
      assignModificationResiduesToMulti(((Protein) element).getModificationResidues(), multiExtension, speciesType);
    } else if (element instanceof Gene) {
      assignModificationResiduesToMulti(((Gene) element).getModificationResidues(), multiExtension, speciesType);
    } else if (element instanceof Rna) {
      assignModificationResiduesToMulti(((Rna) element).getRegions(), multiExtension, speciesType);
    } else if (element instanceof AntisenseRna) {
      assignModificationResiduesToMulti(((AntisenseRna) element).getRegions(), multiExtension, speciesType);
    }

  }

  private void assignModificationResiduesToMulti(Collection<ModificationResidue> modificationResidues,
      MultiSpeciesPlugin multiExtension, MultiSpeciesType speciesType) {
    for (ModificationResidue mr : modificationResidues) {
      SpeciesFeatureType feature = null;
      PossibleSpeciesFeatureValue value;

      if (mr instanceof BindingRegion) {
        feature = getBindingRegionFeature((BindingRegion) mr, speciesType);
      } else if (mr instanceof CodingRegion) {
        feature = getCodingRegionFeature((CodingRegion) mr, speciesType);
      } else if (mr instanceof ProteinBindingDomain) {
        feature = getProteinBindingDomainFeature((ProteinBindingDomain) mr, speciesType);
      } else if (mr instanceof RegulatoryRegion) {
        feature = getRegulatoryRegionFeature((RegulatoryRegion) mr, speciesType);
      } else if (mr instanceof TranscriptionSite) {
        feature = getTranscriptionSiteFeature((TranscriptionSite) mr, speciesType);
      } else if (mr instanceof ModificationSite) {
        feature = getModificationSiteFeature((ModificationSite) mr, speciesType);
      } else if (mr instanceof Residue) {
        feature = getResidueFeature((Residue) mr, speciesType);
      } else {
        logger.warn("Don't know how to export modification: " + mr.getClass());
      }
      value = getPosibleFeatureIdByName(mr.getName(), feature);
      SpeciesFeatureValue featureValue = addSpeciesFeatureValue(multiExtension, feature, value);
      featureValue.setId(getModificationResidueUniqueId(mr));
    }
  }

  private SpeciesFeatureType getCodingRegionFeature(CodingRegion mr, MultiSpeciesType speciesType) {
    String featureId = MultiPackageNamingUtils.getModificationFeatureId(mr);
    String featureName = "Coding region";
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getBindingRegionFeature(BindingRegion bindingRegion, MultiSpeciesType speciesType) {
    String featureId = MultiPackageNamingUtils.getModificationFeatureId(bindingRegion);
    String featureName = "Binding region";
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getProteinBindingDomainFeature(ProteinBindingDomain proteinBindingDomain,
      MultiSpeciesType speciesType) {
    String featureId = MultiPackageNamingUtils.getModificationFeatureId(proteinBindingDomain);
    String featureName = "Protein binding domain";
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getRegulatoryRegionFeature(RegulatoryRegion regulatoryRegion,
      MultiSpeciesType speciesType) {
    String featureId = MultiPackageNamingUtils.getModificationFeatureId(regulatoryRegion);
    String featureName = "Regulatory region";
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getTranscriptionSiteFeature(TranscriptionSite transcriptionSite,
      MultiSpeciesType speciesType) {
    String featureId = MultiPackageNamingUtils.getModificationFeatureId(transcriptionSite);
    String featureName = "Transcription site";
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getModificationSiteFeature(ModificationSite modificationSite,
      MultiSpeciesType speciesType) {
    String featureId = MultiPackageNamingUtils.getModificationFeatureId(modificationSite);
    String featureName = "";
    if (modificationSite.getState() != null) {
      featureName = modificationSite.getState().getFullName();
    }
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getResidueFeature(Residue residue, MultiSpeciesType speciesType) {
    String featureId = MultiPackageNamingUtils.getModificationFeatureId(residue);
    String featureName = "";
    if (residue.getState() != null) {
      featureName = residue.getState().getFullName();
    }
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getOrCreateFeatureById(String featureId, String featureName,
      MultiSpeciesType speciesType) {
    SpeciesFeatureType feature = speciesType.getSpeciesFeatureType(featureId);
    if (feature == null) {
      feature = new SpeciesFeatureType();
      feature.setName(featureName);
      feature.setId(featureId);
      feature.setOccur(1);
      speciesType.getListOfSpeciesFeatureTypes().add(feature);
    }
    return feature;
  }

  private void assignStructuralStateToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    StructuralState structuralState = null;
    if (element instanceof Protein) {
      structuralState = ((Protein) element).getStructuralState();
    } else if (element instanceof Complex) {
      structuralState = ((Complex) element).getStructuralState();
    }
    if (structuralState != null) {
      assignValueToFeature(element, multiExtension, speciesType, structuralState.getValue(),
          BioEntityFeature.STRUCTURAL_STATE);
    }
  }

  private SpeciesFeatureValue addSpeciesFeatureValue(MultiSpeciesPlugin multiExtension, SpeciesFeatureType fetureType,
      PossibleSpeciesFeatureValue possibleValue) {
    SpeciesFeature feature = null;
    for (SpeciesFeature existingFeature : multiExtension.getListOfSpeciesFeatures()) {
      if (existingFeature.getSpeciesFeatureType().equals(fetureType.getId())) {
        feature = existingFeature;
      }
    }
    if (feature == null) {
      feature = multiExtension.createSpeciesFeature();
      feature.setSpeciesFeatureType(fetureType.getId());
      feature.setOccur(1);
    }
    SpeciesFeatureValue value = new SpeciesFeatureValue();
    value.setValue(possibleValue.getId());
    feature.addSpeciesFeatureValue(value);
    return value;
  }

  private PossibleSpeciesFeatureValue getPosibleFeatureIdByName(String featureValueName,
      SpeciesFeatureType speciesFeature) {
    PossibleSpeciesFeatureValue result = null;
    for (PossibleSpeciesFeatureValue value : speciesFeature.getListOfPossibleSpeciesFeatureValues()) {
      if (value.getName().equals(featureValueName)) {
        result = value;
      }
    }
    if (result == null) {
      result = addPosibleValueToFeature(speciesFeature, featureValueName);
    }
    return result;
  }

  private void assignPostionToCompartmentToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    String value = MultiPackageNamingUtils.NULL_REPRESENTATION;
    if (element.getPositionToCompartment() != null) {
      value = element.getPositionToCompartment().getStringName();
    }
    assignValueToFeature(element, multiExtension, speciesType, value, BioEntityFeature.POSITION_TO_COMPARTMENT);
  }

  private void assignListOfSynonymsToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    for (String synonym : element.getSynonyms()) {
      assignValueToFeature(element, multiExtension, speciesType, synonym, BioEntityFeature.SYNONYM);
    }
  }

  private void assignListOfFormerSymbolsToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    for (String formerSymbol : element.getFormerSymbols()) {
      assignValueToFeature(element, multiExtension, speciesType, formerSymbol, BioEntityFeature.FORMER_SYMBOL);
    }
  }

  private void assignSymbolToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    if (element.getSymbol() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getSymbol(), BioEntityFeature.SYMBOL);
    }
  }

  private void assignFullNameToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    if (element.getFullName() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getFullName(), BioEntityFeature.FULL_NAME);
    }
  }

  private void assignFormulaToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    if (element.getFormula() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getFormula(), BioEntityFeature.FORMULA);
    }
  }

  private void assignDimerToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    if (element.getHomodimer() != 1) {
      assignValueToFeature(element, multiExtension, speciesType, element.getHomodimer() + "", BioEntityFeature.DIMER);
    }
  }

  private void assignChargeToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    if (element.getCharge() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getCharge().toString(),
          BioEntityFeature.CHARGE);
    }
  }

  private void assignHypotheticalToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    if (element.getHypothetical() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getHypothetical().toString(),
          BioEntityFeature.HYPOTHETICAL);
    }
  }

  private void assignActivityToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    if (element.getActivity() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getActivity().toString(),
          BioEntityFeature.ACTIVITY);
    }
  }

  private void assignAbbreviationToMulti(Species element, MultiSpeciesPlugin multiExtension,
      MultiSpeciesType speciesType) {
    if (element.getAbbreviation() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getAbbreviation(),
          BioEntityFeature.ABBREVIATION);
    }
  }

  private void assignValueToFeature(Species element, MultiSpeciesPlugin multiExtension, MultiSpeciesType speciesType,
      String value, BioEntityFeature feature) {
    SpeciesFeatureType structuralStateFeature = getFeature(element.getClass(), speciesType, feature);
    PossibleSpeciesFeatureValue structuralStateFeatureValue = getPosibleFeatureIdByName(value,
        structuralStateFeature);

    addSpeciesFeatureValue(multiExtension, structuralStateFeature, structuralStateFeatureValue);
  }

  private MultiSpeciesType getMultiSpeciesType(Species element) {
    MultiSpeciesType speciesType = getMultiPlugin().getSpeciesType(MultiPackageNamingUtils.getSpeciesTypeId(element));
    if (speciesType == null) {
      speciesType = createSpeciesTypeForClass(getMultiPlugin(), element.getClass());
    }
    return speciesType;
  }

  @Override
  protected List<Species> getElementList() {
    return getMinervaModel().getSpeciesList();
  }

  @Override
  public org.sbml.jsbml.Species createSbmlElement(Species element) throws InconsistentModelException {
    org.sbml.jsbml.Species result = getSbmlModel().createSpecies("species_" + (getNextId()));
    result.setSBOTerm(SBOTermSpeciesType.getTermByType(element.getClass()));
    result.setCompartment(compartmentExporter.getSbmlElement(element.getCompartment()));
    if (element.getInitialAmount() != null) {
      result.setInitialAmount(element.getInitialAmount());
    }
    if (element.getInitialConcentration() != null) {
      result.setInitialConcentration(element.getInitialConcentration());
    }
    if (element.getInitialAmount() == null && element.getInitialConcentration() == null) {
      result.setInitialConcentration(0);
    }
    if (element.hasOnlySubstanceUnits() != null) {
      result.setHasOnlySubstanceUnits(element.hasOnlySubstanceUnits());
    }
    if (element.getBoundaryCondition() != null) {
      result.setBoundaryCondition(element.getBoundaryCondition());
    } else {
      result.setBoundaryCondition(false);
    }
    if (element.getConstant() != null) {
      result.setConstant(element.getConstant());
    } else {
      result.setConstant(false);
    }
    if (isExtensionEnabled(SbmlExtension.MULTI)) {
      assignMultiExtensionData(element, result);
    }
    return result;
  }

  @Override
  protected String getSbmlIdKey(Species element) {
    String compartmentName = null;
    String complexName = null;
    if (element.getCompartment() != null) {
      compartmentName = element.getCompartment().getName();
    }
    if (element.getComplex() != null) {
      complexName = element.getComplex().getName();
    }

    List<String> annotations = new ArrayList<>();
    for (MiriamData md : element.getMiriamData()) {
      annotations.add(md.toString());
    }
    Collections.sort(annotations);
    String annotationNames = StringUtils.join(annotations, ",");

    String multiDistinguisher = null;
    if (isExtensionEnabled(SbmlExtension.MULTI)) {
      StructuralState structuralState = null;
      if (element instanceof Protein) {

        structuralState = ((Protein) element).getStructuralState();
      } else if (element instanceof Complex) {
        structuralState = ((Complex) element).getStructuralState();
      }
      if (structuralState != null) {
        multiDistinguisher = structuralState.getValue();
      }

      if (element instanceof SpeciesWithModificationResidue) {
        List<String> modifications = new ArrayList<>();
        for (ModificationResidue mr : ((SpeciesWithModificationResidue) element).getModificationResidues()) {
          String modificationId = mr.getName() + mr.getClass().getSimpleName();
          if (mr instanceof AbstractSiteModification) {
            modificationId += ((AbstractSiteModification) mr).getState();
          }
          modifications.add(modificationId);
        }
        multiDistinguisher += "\n" + StringUtils.join(modifications, ",");
      }
      multiDistinguisher += "\n" + element.getPositionToCompartment();
      if (element instanceof Complex) {
        List<String> complexChildIds = new ArrayList<>();
        for (Species child : ((Complex) element).getAllChildren()) {
          complexChildIds.add(getSbmlIdKey(child));
        }
        Collections.sort(complexChildIds);
        multiDistinguisher += "\n" + StringUtils.join(complexChildIds, "\n");
      }
      multiDistinguisher += "\n" + element.getActivity();
      multiDistinguisher += "\n" + element.getCharge();
      multiDistinguisher += "\n" + element.getHomodimer();
    }
    String result = element.getClass().getSimpleName() + "\n" + annotationNames + "\n" + element.getName() + "\n"
        + compartmentName + "\n"
        + complexName
        + "\n" + multiDistinguisher;
    return result;
  }

  @Override
  protected AbstractReferenceGlyph createElementGlyph(String sbmlElementId, String glyphId) {
    AbstractReferenceGlyph speciesGlyph = getLayout().createSpeciesGlyph(glyphId, sbmlElementId);
    return speciesGlyph;
  }

  @Override
  protected LocalStyle createStyle(Species element) {
    LocalStyle style = super.createStyle(element);
    style.getGroup().setStrokeWidth(element.getLineWidth());
    if (element instanceof Chemical) {
      Ellipse ellipse = new Ellipse();
      ellipse.setAbsoluteRx(false);
      ellipse.setAbsoluteRy(false);
      ellipse.setAbsoluteCx(false);
      ellipse.setAbsoluteCy(false);
      ellipse.setCx(50);
      ellipse.setCy(50.0);
      ellipse.setRx(50.0);
      ellipse.setRy(50.0);
      ellipse.setStroke(getColorDefinition(Color.BLACK).getId());
      style.getGroup().addElement(ellipse);
    } else {
      Rectangle rectangle = new Rectangle();
      rectangle.setAbsoluteRx(false);
      rectangle.setAbsoluteRy(false);
      rectangle.setAbsoluteX(false);
      rectangle.setAbsoluteY(false);
      rectangle.setAbsoluteHeight(false);
      rectangle.setAbsoluteWidth(false);
      rectangle.setRx(0);
      rectangle.setRy(0);
      rectangle.setX(0);
      rectangle.setY(0);
      rectangle.setWidth(100);
      rectangle.setHeight(100);
      rectangle.setStroke(getColorDefinition(Color.BLACK).getId());
      style.getGroup().addElement(rectangle);
    }

    return style;
  }

  @Override
  protected void assignLayoutToGlyph(Species element, AbstractReferenceGlyph speciesGlyph) {
    super.assignLayoutToGlyph(element, speciesGlyph);
    if (isExtensionEnabled(SbmlExtension.MULTI)) {
      if (element instanceof SpeciesWithModificationResidue) {
        for (ModificationResidue mr : ((SpeciesWithModificationResidue) element).getModificationResidues()) {
          createModificationGlyph(mr, speciesGlyph);
        }
      }
      if (element.getStructuralState() != null) {
        createStructuralStateGlyph(element, speciesGlyph);
      }
    }
    TextGlyph textGlyph = getLayout().createTextGlyph("text_" + getElementId(element), element.getName());
    textGlyph
        .setBoundingBox(createBoundingBox(element.getX(), element.getY(), element.getZ() + 1, element.getWidth(),
            element.getHeight()));
    textGlyph.setReference(speciesGlyph.getId());

    if (isExtensionEnabled(SbmlExtension.RENDER)) {
      LocalRenderInformation renderInformation = getRenderInformation(getRenderPlugin());
      LocalStyle style = new LocalStyle();
      style.getIDList().add(textGlyph.getId());
      style.setGroup(new RenderGroup());
      style.getGroup().setVTextAnchor(VTextAnchor.MIDDLE);
      style.getGroup().setTextAnchor(HTextAnchor.MIDDLE);
      renderInformation.addLocalStyle(style);
    }

  }

  private void createStructuralStateGlyph(Species element, AbstractReferenceGlyph speciesGlyph) {
    org.sbml.jsbml.Species sbmlSpecies = getSbmlModel().getSpecies(speciesGlyph.getReference());

    MultiSpeciesPlugin speciesExtension = (MultiSpeciesPlugin) sbmlSpecies.getExtension("multi");

    SpeciesFeatureType structuralStateFeature = getFeature(element.getClass(), getMultiSpeciesType(element),
        BioEntityFeature.STRUCTURAL_STATE);

    PossibleSpeciesFeatureValue possibleSpeciesFeatureValue = getPosibleFeatureIdByName(
        element.getStructuralState().getValue(), structuralStateFeature);

    SpeciesFeature feature = null;
    for (SpeciesFeature existingFeature : speciesExtension.getListOfSpeciesFeatures()) {
      if (existingFeature.getSpeciesFeatureType().equals(structuralStateFeature.getId())) {
        feature = existingFeature;
      }
    }

    SpeciesFeatureValue modificationFeatureValue = null;
    for (SpeciesFeatureValue featureValue : feature.getListOfSpeciesFeatureValues()) {
      if (possibleSpeciesFeatureValue.getId().equals(featureValue.getValue())) {
        modificationFeatureValue = featureValue;
      }
    }

    modificationFeatureValue.setId(getStructuralStateUniqueId(element.getStructuralState()));

    GeneralGlyph modificationGlyph = getLayout().createGeneralGlyph("structural_state_" + (idCounter++),
        modificationFeatureValue.getId());
    ReferenceGlyph referenceGlyph = new ReferenceGlyph("structural_state_reference_" + (idCounter++));
    referenceGlyph.setGlyph(speciesGlyph.getId());
    referenceGlyph.setBoundingBox(createBoundingBoxForStructuralState(element.getStructuralState()));

    modificationGlyph.addReferenceGlyph(referenceGlyph);
    modificationGlyph.setBoundingBox(createBoundingBoxForStructuralState(element.getStructuralState()));

  }

  @SuppressWarnings("unchecked")
  private MultiSpeciesType createSpeciesTypeForClass(MultiModelPlugin multiPlugin, Class<? extends Element> clazz) {
    MultiSpeciesType speciesType = new MultiSpeciesType();
    speciesType.setName(clazz.getSimpleName());
    speciesType.setId(MultiPackageNamingUtils.getSpeciesTypeId(clazz));
    multiPlugin.getListOfSpeciesTypes().add(speciesType);
    speciesType.setSBOTerm(SBOTermSpeciesType.getTermByType((Class<? extends Species>) clazz));
    return speciesType;
  }

  private SpeciesFeatureType getFeature(Class<? extends Element> clazz, MultiSpeciesType speciesType,
      BioEntityFeature bioEntityFeature) {
    SpeciesFeatureType feature = speciesType
        .getSpeciesFeatureType(MultiPackageNamingUtils.getFeatureId(clazz, bioEntityFeature));
    if (feature == null) {
      feature = new SpeciesFeatureType();
      feature.setName(bioEntityFeature.getFeatureName());
      feature.setId(MultiPackageNamingUtils.getFeatureId(clazz, bioEntityFeature));
      feature.setOccur(1);
      if (bioEntityFeature.getDefaultValue() != null) {
        addPosibleValueToFeature(feature, bioEntityFeature.getDefaultValue());
      }
      speciesType.getListOfSpeciesFeatureTypes().add(feature);
    }
    return feature;
  }

  private PossibleSpeciesFeatureValue addPosibleValueToFeature(SpeciesFeatureType feature, String value) {
    if (value == null) {
      value = MultiPackageNamingUtils.NULL_REPRESENTATION;
    }

    PossibleSpeciesFeatureValue result = null;
    for (PossibleSpeciesFeatureValue existingValue : feature.getListOfPossibleSpeciesFeatureValues()) {
      if (existingValue.getName().equals(value)) {
        result = existingValue;
      }
    }
    if (result == null) {
      result = new PossibleSpeciesFeatureValue();
      result.setId(feature.getId() + "_" + (idCounter++));
      result.setName(value);
      feature.getListOfPossibleSpeciesFeatureValues().add(result);
    }
    return result;
  }

  private void createModificationGlyph(ModificationResidue mr, AbstractReferenceGlyph speciesGlyph) {
    org.sbml.jsbml.Species sbmlSpecies = getSbmlModel().getSpecies(speciesGlyph.getReference());

    MultiSpeciesPlugin speciesExtension = (MultiSpeciesPlugin) sbmlSpecies.getExtension("multi");
    SpeciesFeature feature = null;
    String featureTypeId = MultiPackageNamingUtils.getModificationFeatureId(mr);
    for (SpeciesFeature existingFeature : speciesExtension.getListOfSpeciesFeatures()) {
      if (existingFeature.getSpeciesFeatureType().equals(featureTypeId)) {
        feature = existingFeature;
      }
    }

    SpeciesFeatureValue modificationFeatureValue = null;
    for (SpeciesFeatureValue featureValue : feature.getListOfSpeciesFeatureValues()) {
      if (featureValue.getId().equals(getModificationResidueUniqueId(mr))) {
        modificationFeatureValue = featureValue;
      }
    }

    GeneralGlyph modificationGlyph = getLayout().createGeneralGlyph("modification_" + (idCounter++),
        modificationFeatureValue.getId());
    ReferenceGlyph referenceGlyph = new ReferenceGlyph("modification_reference_" + (idCounter++));
    referenceGlyph.setGlyph(speciesGlyph.getId());
    referenceGlyph.setBoundingBox(createBoundingBoxForModification(mr));

    modificationGlyph.addReferenceGlyph(referenceGlyph);
    modificationGlyph.setBoundingBox(createBoundingBoxForModification(mr));
  }

  private BoundingBox createBoundingBoxForStructuralState(StructuralState structuralState) {
    double width = structuralState.getWidth();
    double height = structuralState.getHeight();
    double x = structuralState.getPosition().getX();
    double y = structuralState.getPosition().getY();
    BoundingBox boundingBox = new BoundingBox();
    boundingBox.setPosition(new Point(x, y, structuralState.getSpecies().getZ() + 1));
    Dimensions dimensions = new Dimensions();
    dimensions.setWidth(width);
    dimensions.setHeight(height);
    boundingBox.setDimensions(dimensions);
    return boundingBox;
  }

  private BoundingBox createBoundingBoxForModification(ModificationResidue mr) {
    double width, height;
    if (mr instanceof AbstractRegionModification) {
      width = ((AbstractRegionModification) mr).getWidth();
      height = ((AbstractRegionModification) mr).getHeight();
    } else {
      width = 12;
      height = 12;
    }
    double x = mr.getPosition().getX() - width / 2;
    double y = mr.getPosition().getY() - height / 2;
    BoundingBox boundingBox = new BoundingBox();
    boundingBox.setPosition(new Point(x, y, mr.getSpecies().getZ() + 1));
    Dimensions dimensions = new Dimensions();
    dimensions.setWidth(width);
    dimensions.setHeight(height);
    boundingBox.setDimensions(dimensions);
    return boundingBox;
  }

  private String getModificationResidueUniqueId(ModificationResidue mr) {
    SpeciesWithModificationResidue species = (SpeciesWithModificationResidue) mr.getSpecies();
    String modificationId = "mod" + species.getModificationResidues().indexOf(mr);
    String result = modificationResidueIds.get(modificationId + "_" + getSbmlIdKey(mr.getSpecies()));

    if (result == null) {
      result = "modification_residue_" + (modificationResidueCounter++);
      modificationResidueIds.put(modificationId + "_" + getSbmlIdKey(mr.getSpecies()), result);
    }
    return result;
  }

  private String getStructuralStateUniqueId(StructuralState mr) {
    String modificationId = "structural_state_" + getSbmlIdKey(mr.getSpecies());
    String result = modificationResidueIds.get(modificationId);
    if (result == null) {
      result = "structural_state_" + (structuralStateCounter++);
      modificationResidueIds.put(modificationId, result);
    }
    return result;
    
  }

}
