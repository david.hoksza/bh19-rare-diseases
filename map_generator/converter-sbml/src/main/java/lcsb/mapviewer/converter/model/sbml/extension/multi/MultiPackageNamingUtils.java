package lcsb.mapviewer.converter.model.sbml.extension.multi;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ext.multi.MultiSpeciesType;

import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.*;

/**
 * Class responsible for providing identifiers of structures inside multi
 * package.
 * 
 * @author Piotr Gawron
 *
 */
public final class MultiPackageNamingUtils {

  public static final String NULL_REPRESENTATION = "NULL";
  private static final String MINERVA_MODIFICATION_TYPE_PREFIX = "minerva_modification_type_";
  /**
   * Default logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(MultiPackageNamingUtils.class);

  /**
   * Returns id of the {@link MultiSpeciesType} for a given minerva class.
   * 
   * @param speciesClass
   *          minerva {@link Element} class.
   * @return id of the {@link MultiSpeciesType} for a given minerva class
   */
  public static final String getSpeciesTypeId(Class<?> speciesClass) {
    return "minerva_species_type_" + speciesClass.getSimpleName();
  }

  /**
   * Returns id of the {@link MultiSpeciesType} for a given minerva element
   * object.
   * 
   * @param speciesClass
   *          object for which we want to get id
   * 
   * @return id of the {@link MultiSpeciesType} for a given minerva object
   */
  public static final String getSpeciesTypeId(Element element) {
    return getSpeciesTypeId(element.getClass());
  }

  public static final String getFeatureId(Class<?> speciesClass, BioEntityFeature feature) {
    return feature.getIdPrefix() + speciesClass.getSimpleName();
  }

  public static String getFeatureId(Species element, BioEntityFeature feature) {
    return getFeatureId(element.getClass(), feature);
  }

  public static boolean isFeatureId(String featureTypeId, BioEntityFeature feature) {
    return featureTypeId.startsWith(feature.getIdPrefix());
  }

  public static String getModificationFeatureId(ModificationResidue mr) {
    String stateSuffix = "";
    if (mr instanceof AbstractSiteModification) {
      if (((AbstractSiteModification) mr).getState() != null) {
        stateSuffix = "_" + ((AbstractSiteModification) mr).getState().name();
      } else {
        stateSuffix = "_null";
      }
    } else if (mr instanceof TranscriptionSite) {
      stateSuffix = "_" + ((TranscriptionSite) mr).getActive() + "_" + ((TranscriptionSite) mr).getDirection();
    }

    return MINERVA_MODIFICATION_TYPE_PREFIX + mr.getSpecies().getClass().getSimpleName() + "_"
        + mr.getClass().getSimpleName() + stateSuffix;
  }

  public static boolean isModificationFeatureId(String featureTypeString) {
    return featureTypeString.startsWith(MINERVA_MODIFICATION_TYPE_PREFIX);
  }

  public static boolean isModificationFeatureId(String speciesFeatureType,
      Class<? extends ModificationResidue> class1) {
    return isModificationFeatureId(speciesFeatureType) && speciesFeatureType.contains("_" + class1.getSimpleName());
  }

  public static ModificationState getModificationStateFromFeatureTypeName(String featureTypeString) {
    for (ModificationState state : ModificationState.values()) {
      if (featureTypeString.endsWith(state.name())) {
        return state;
      }
    }
    return null;
  }

  public static Boolean getTranscriptionFactorActiveStateFromFeatureTypeName(String featureTypeString) {
    String tmp[] = featureTypeString.split("_");
    Boolean result = null;
    if (tmp.length >= 2) {
      if (tmp[tmp.length - 2].equalsIgnoreCase("TRUE")) {
        result = true;
      } else if (tmp[tmp.length - 2].equalsIgnoreCase("FALSE")) {
        result = false;
      }
    }
    return result;
  }

  public static String getTranscriptionFactorDirectionStateFromFeatureTypeName(String featureTypeString) {
    String tmp[] = featureTypeString.split("_");
    String result = null;
    if (tmp.length >= 1) {
      result = tmp[tmp.length - 1];
    }
    return result;
  }

}
