package lcsb.mapviewer.converter.model.sbml.reaction;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.*;

public enum SBOTermReactionType {
  CATALYSIS(CatalysisReaction.class, new String[] { "SBO:0000013" }),
  DISSOCIATION(DissociationReaction.class, new String[] { "SBO:0000180" }),
  HETERODIMER_ASSOCIATION(HeterodimerAssociationReaction.class, new String[] { "SBO:0000177" }),
  INHIBITION(InhibitionReaction.class, new String[] { "SBO:0000537" }),
  KNOWN_TRANSITION_OMITTED(KnownTransitionOmittedReaction.class, new String[] { "SBO:0000205" }),
  MODULATION(ModulationReaction.class, new String[] { "SBO:0000594" }),
  NEGATIVE_INFLUENCE(NegativeInfluenceReaction.class, new String[] { "SBO:0000407" }),
  PHYSICAL_STIMULATION(PhysicalStimulationReaction.class, new String[] { "SBO:0000459" }),
  POSITIVE_INFLUENCE(PositiveInfluenceReaction.class, new String[] { "SBO:0000171" }),
  REDUCED_MODULATION(ReducedModulationReaction.class, new String[] { "SBO:0000632" }),
  REDUCED_PHYSICAL_STIMULATION(ReducedPhysicalStimulationReaction.class, new String[] { "SBO:0000411" }),
  REDUCED_TRIGGER(ReducedTriggerReaction.class, new String[] { "SBO:0000533" }),
  STATE_TRANSITION(StateTransitionReaction.class, new String[] { "SBO:0000176" }),
  TRANSCRIPTION(TranscriptionReaction.class, new String[] { "SBO:0000183" }),
  TRANSLATION(TranslationReaction.class, new String[] { "SBO:0000184" }),
  TRANSPORT(TransportReaction.class, new String[] { "SBO:0000185" }),
  TRIGGER(TriggerReaction.class, new String[] { "SBO:0000461" }),
  TRUNCATION(TruncationReaction.class, new String[] { "SBO:0000178" }),
  UNKNOWN_CATALYSIS(UnknownCatalysisReaction.class, new String[] { "SBO:0000462" }),
  UNKNOWN_INHIBITION(UnknownInhibitionReaction.class, new String[] { "SBO:0000536" }),
  UNKNOWN_NEGATIVE_INFLUENCE(UnknownNegativeInfluenceReaction.class, new String[] { "SBO:0000169" }),
  UNKNOWN_POSITIVE_INFLUENCE(UnknownPositiveInfluenceReaction.class, new String[] { "SBO:0000172" }),
  UNKNOWN_REDUCED_MODULATION(UnknownReducedModulationReaction.class, new String[] { "SBO:0000631" }),
  UNKNOWN_REDUCED_PHYSICAL_STIMULATION(UnknownReducedPhysicalStimulationReaction.class, new String[] { "SBO:0000170" }),
  UNKNOWN_REDUCED_TRIGGER(UnknownReducedTriggerReaction.class, new String[] { "SBO:0000534" }),
  UNKNOWN_TRANSITION(UnknownTransitionReaction.class, new String[] { "SBO:0000396" }),
  ;

  private static Logger logger = LogManager.getLogger(SBOTermReactionType.class);
  Class<? extends Reaction> clazz;
  private List<String> sboTerms = new ArrayList<>();

  private SBOTermReactionType(Class<? extends Reaction> clazz, String[] inputSboTerms) {
    this.clazz = clazz;
    for (String string : inputSboTerms) {
      sboTerms.add(string);
    }
  }

  public static Class<? extends Reaction> getTypeSBOTerm(String sboTerm) {
    if (sboTerm == null || sboTerm.isEmpty()) {
      return StateTransitionReaction.class;
    }
    Class<? extends Reaction> result = null;
    for (SBOTermReactionType term : values()) {
      for (String string : term.sboTerms) {
        if (string.equalsIgnoreCase(sboTerm)) {
          result = term.clazz;
        }
      }
    }
    if (result == null) {
      logger.warn("Don't know how to handle SBOTerm " + sboTerm + " for reaction");
      result = StateTransitionReaction.class;
    }
    return result;
  }

  public static String getTermByType(Class<? extends Reaction> clazz) {
    for (SBOTermReactionType term : values()) {
      if (clazz.equals(term.clazz)) {
        return term.getSBO();
      }
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }

  private String getSBO() {
    if (sboTerms.size() != 0) {
      return sboTerms.get(0);
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }
}
