package lcsb.mapviewer.converter.model.sbml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Parameter;

import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlParameterExporter {
  Logger logger = LogManager.getLogger(SbmlParameterExporter.class);
  private Model minervaModel;

  public SbmlParameterExporter(lcsb.mapviewer.model.map.model.Model minervaModel) {
    this.minervaModel = minervaModel;
  }

  public void exportParameter(org.sbml.jsbml.Model result) {
    for (SbmlParameter unit : minervaModel.getParameters()) {
      result.addParameter(createParameter(unit));
    }
  }

  private Parameter createParameter(SbmlParameter unit) {
    Parameter result = new Parameter();
    result.setName(unit.getName());
    result.setValue(unit.getValue());
    result.setId(unit.getParameterId());
    if (unit.getUnits() != null) {
      result.setUnits(unit.getUnits().getUnitId());
    }
    return result;
  }

}
