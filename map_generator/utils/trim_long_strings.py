""""
The purpose of this script is to shorten too long annotations, because the size limit in MINERVA is 256 characters.
If there exists an annotation which is longer, the import will fail.
"""

import logging
import argparse
import xml.etree.ElementTree as ET
import utils

def trim(path: str) -> str:

    utils.register_namespaces(path)

    tree = ET.parse(path)
    root = tree.getroot()

    for elem in root.iter():
        atts = elem.attrib
        # if 'name' in atts:
        #     print(elem.tag, atts)
        for k, v in atts.items():
            if len(v) > 250:
                logging.warning("Trimming {} (key {} too long)".format(elem, k))
                if "resource" in k:
                    #  We want to keep the RDF resource still a valid resource otherwise MINERVA will fail during import
                    atts[k] = atts[k].split(";")[0]
                else:
                    atts[k] = atts[k][:255]

    return ET.tostring(root, encoding='utf8').decode('utf8')

if __name__ == '__main__':

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s] %(module)s - %(message)s',
        datefmt='%H:%M:%S')

    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--map_path",
                        required=True,
                        help="SBML file with a map")
    args = parser.parse_args()
    print(trim(args.map_path))