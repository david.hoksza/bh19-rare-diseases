"""
The purpose of this file is to enrich the annotations with UniProt identifiers because variant overlay file
has the uniprot id field which matches the individual variants to uniprot records. This is important in
situations where one gene has variants in different uniprot records (e.g. in case of multiple isoforms).
"""

import logging
import argparse
import pandas as pd
import xml.etree.ElementTree as ET
import utils

def get_full_name(elem_name, ns, nss) -> str:
    return "{{{}}}{}".format(nss[ns], elem_name)

def implant(map_path: str, variants_path: str) -> str:

    df_vars = pd.read_csv(variants_path, sep='\t', skiprows=4)
    gene_uniprot = {}
    for name, g in df_vars.groupby('gene_name')['identifier_uniprot']:
        gene_uniprot[name] = set(g)

    namespaces = utils.register_namespaces(map_path)
    tree = ET.parse(map_path)
    root = tree.getroot()

    for elem in list(root.iter(get_full_name("species", "", namespaces))):
    # for elem in root.iterfind(get_full_name("layout", "layout", namespaces)):
        elem_name = elem.attrib["name"]
        elem_id = elem.attrib["id"]

        if elem_name  in gene_uniprot:
            rdfs = list(elem.iter(get_full_name("RDF", "rdf", namespaces)))

            if len(rdfs) > 0:
                rdf = rdfs[0]
            else:
                rdf = ET.SubElement(elem, 'rdf:RDF')

            rdf_descs = list(rdf.iter(get_full_name("Description", "rdf", namespaces)))
            if len(rdf_descs) > 0:
                rdf_desc = rdf_descs[0]
            else:
                rdf_desc = ET.SubElement(rdf, 'rdf:about', {"#{}".format(elem_id)})


            for uniprot_id in gene_uniprot[elem_name ]:
                idb = ET.SubElement(rdf_desc, 'bqbiol:isDescribedBy')
                b = ET.SubElement(idb, 'rdf:Bag')
                urn_key = 'urn:miriam:uniprot' if '-' not in uniprot_id else 'urn:miriam:uniprot.isoform'
                ET.SubElement(b, 'rdf:li', {'rdf:resource': "{}:{}".format(urn_key, uniprot_id)})


    # return ET.tostring(root)
    return ET.tostring(root, encoding='unicode')

if __name__ == '__main__':

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s] %(module)s - %(message)s',
        datefmt='%H:%M:%S')

    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--map_path",
                        required=True,
                        help="SBML map file")
    parser.add_argument("-v", "--variants_path",
                        required=True,
                        help="MINERVA variant file")
    args = parser.parse_args()
    print(implant(args.map_path, args.variants_path))