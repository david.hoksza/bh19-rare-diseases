package lcsb.mapviewer.converter.model.sbgnml;

import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.Residue;

public class SbgnmlXmlParserTest2 extends SbgnmlTestFunctions {

  Logger logger = LogManager.getLogger(SbgnmlXmlParserTest2.class.getName());

  @Test
  public void testAdjustModificationCoordinates() throws Exception {
    SbgnmlXmlParser parser = new SbgnmlXmlParser();
    GenericProtein protein = new GenericProtein("id");
    protein.setWidth(50);
    protein.setHeight(30);
    protein.setX(200);
    protein.setX(300);
    Residue mr = new Residue();
    Point2D position = new Point2D.Double(100, 20);
    mr.setPosition(position);
    protein.addResidue(mr);
    parser.adjustModificationCoordinates(protein);

    assertTrue(mr.getPosition().distance(position) > Configuration.EPSILON);

  }

  @Test
  public void createModelWithCompartmentsTest() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/elements_inside_compartment.xml"));
    Complex complexInsideCompartment = model.getElementByElementId("csa1830");
    assertNotNull("Complex inside compartment has undefined compartment", complexInsideCompartment.getCompartment());
    Complex complexOutsideCompartment = model.getElementByElementId("csa1831");
    assertNull("Complex outside compartment has not null compartment", complexOutsideCompartment.getCompartment());
    Compartment compartment = model.getElementByElementId("ca107");
    assertNull("Top compartment has not null compartment", compartment.getCompartment());
  }

  @Test
  public void parseModelWithXSDProblems() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    converter.createModel(new ConverterParams().filename("testFiles/file_that_does_not_pass_xsd.sbgn"));
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void createModelWithZIndex() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/elements_inside_compartment.xml"));
    Complex complex = model.getElementByElementId("csa1830");
    assertNotNull(complex.getZ());
  }

  @Test
  public void testCoordinatesOfOperators() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/clone-marker.sbgn"));

    Reaction reaction = model.getReactionByReactionId("glyph9");

    Reactant r1 = reaction.getReactants().get(0);
    Reactant r2 = reaction.getReactants().get(1);

    Product p1 = reaction.getProducts().get(0);
    Product p2 = reaction.getProducts().get(1);

    NodeOperator inputOperator = null;
    NodeOperator outputOperator = null;
    for (NodeOperator operator : reaction.getOperators()) {
      if (operator.isProductOperator()) {
        outputOperator = operator;
      } else {
        inputOperator = operator;
      }
    }

    assertEquals(r1.getLine().getEndPoint(), inputOperator.getLine().getBeginPoint());
    assertEquals(r2.getLine().getEndPoint(), inputOperator.getLine().getBeginPoint());

    assertEquals(p1.getLine().getBeginPoint(), outputOperator.getLine().getBeginPoint());
    assertEquals(p2.getLine().getBeginPoint(), outputOperator.getLine().getBeginPoint());
  }

  @Test
  public void testProteinState() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams().filename("testFiles/sbgnmlCellDesignerInompatible/stateVariable.sbgn"));

    Protein protein = model.getElementByElementId("glyph_n20");
    assertEquals("inactive", protein.getStructuralState().getValue());
    assertEquals(0, protein.getModificationResidues().size());
  }

  @Test
  public void testParseColors() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlCellDesignerInompatible/neuronal_muscle_signalling_color.sbgn"));
    assertFalse(model.getElementByElementId("glyph0").getFillColor().equals(Color.WHITE));
    assertTrue(super.getWarnings().size() <= 1);
  }

  @Test
  public void testProblematicProduct() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/problematic_product_line.sbgn"));

    Reaction r = model.getReactionByReactionId("reactionVertex_10725167_27892");
    Product p = r.getProducts().get(0);
    for (Product product : r.getProducts()) {
      assertEquals(2, product.getLine().getPoints().size());
      assertEquals("Product lines should start at the same point", 0,
          product.getLine().getBeginPoint().distance(p.getLine().getBeginPoint()), Configuration.EPSILON);
    }
    SplitOperator operator = null;
    for (NodeOperator o : r.getOperators()) {
      if (o instanceof SplitOperator) {
        operator = (SplitOperator) o;
      }
    }

    assertEquals(0, operator.getLine().getBeginPoint().distance(p.getLine().getBeginPoint()), Configuration.EPSILON);

  }

}
