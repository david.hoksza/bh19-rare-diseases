package lcsb.mapviewer.converter.model.sbgnml;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;

@RunWith(Parameterized.class)
public class CellDesigner2SbgnmlConversionTest extends SbgnmlTestFunctions {

  private static Logger logger = LogManager.getLogger(CellDesigner2SbgnmlConversionTest.class.getName());

  private Path testPath;

  public CellDesigner2SbgnmlConversionTest(Path testPath) {
    this.testPath = testPath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<>();
    Files.walk(Paths.get("../converter-CellDesigner/testFiles")).forEach(fPath -> {
      if (Files.isRegularFile(fPath) && fPath.toString().endsWith(".xml")
          && fPath.toString().indexOf("invalid") == -1) {
        try {
          String tempPath = fPath.toString()
              .substring(fPath.toString().indexOf("testFiles") + "testFiles".length() + 1);
          if (!tempPath.contains("_full")) {
            String content = FileUtils.readFileToString(fPath.toFile(), StandardCharsets.UTF_8);
            if (content.indexOf("xmlns:celldesigner") >= 0) {
              data.add(new Object[] { fPath });
            }
          }
        } catch (Exception e) {
          throw new RuntimeException();
        }
      }
    });
    return data;
  }

  private void parseAndExport(Path testPath) throws Exception {
    Converter cellDesignerConverter = new CellDesignerXmlParser();
    Converter sbgnmlConverter = new SbgnmlXmlConverter();

    String testName = "";
    Path tempPath = testPath;
    while (!tempPath.getParent().toString().endsWith("testFiles")) {
      // Add parent folder to path
      tempPath = tempPath.getParent();
      testName = tempPath.getFileName().toString().concat("/").concat(testName);
    }
    testName = testName
        .concat(testPath.getFileName().toString().substring(0, testPath.getFileName().toString().indexOf(".xml")));
    Model model;
    try {
      model = cellDesignerConverter.createModel(
          new ConverterParams().filename("../converter-CellDesigner/testFiles/".concat(testName).concat(".xml")));
    } catch (Exception ex) {
      logger.warn("Problem encountered when parsing CellDesigner file: " + testName);
      return;
    }

    String outputFile = "testFiles/sbgnmlExporterTestFiles/fromCellDesigner/".concat(testName).concat(".sbgn");
    sbgnmlConverter.model2File(model, outputFile);
    new File(outputFile).delete();
  }

  @Test
  public void test() throws Exception {
    parseAndExport(testPath);
  }

}
