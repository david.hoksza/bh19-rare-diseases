package lcsb.mapviewer.converter.model.sbgnml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.StructuralStateComparator;
import lcsb.mapviewer.modelutils.map.ElementUtils;

public class CellDesignerToSbgnTest extends SbgnmlTestFunctions {
  Logger logger = LogManager.getLogger(CellDesignerToSbgnTest.class);
  ElementUtils eu = new ElementUtils();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSample() throws Exception {
    Converter converter = new CellDesignerXmlParser();
    Converter converter2 = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/sample.xml"));

    String output = File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath();
    converter2.model2File(model, output);

    try (FileInputStream inputStream = new FileInputStream(output)) {
      String everything = IOUtils.toString(inputStream, "UTF-8");
      assertTrue("Warnings are not exported into sbgn", everything.contains("Element type is not supported"));
    }
    converter2.createModel(new ConverterParams().filename(output));

    new File(output).delete();
  }

  @Test
  public void testSample2() throws Exception {
    Converter converter = new CellDesignerXmlParser();
    Converter converter2 = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/bubbles.xml"));

    String output = File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath();
    converter2.model2File(model, output);
    new File(output).delete();
  }

  @Test
  public void testCompartmentsExport() throws Exception {
    Converter converter = new CellDesignerXmlParser();
    Converter converter2 = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/neuron.xml"));

    String output = File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath();
    converter2.model2File(model, output);

    converter2.createModel(new ConverterParams().filename(output));

    String fileContent;
    try (FileInputStream inputStream = new FileInputStream(output)) {
      fileContent = IOUtils.toString(inputStream, "UTF-8");
    }

    for (Element element : model.getElements()) {
      if (element instanceof Compartment) {
        assertTrue(eu.getElementTag(element) + " comparmtent is not exported",
            fileContent.contains(element.getElementId()));
      }
    }

    new File(output).delete();
  }

  @Test
  public void testProteinState() throws Exception {
    Converter converter = new CellDesignerXmlParser();
    Converter converter2 = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/state.xml"));

    String output = File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath();
    converter2.model2File(model, output);

    Model model2 = converter2.createModel(new ConverterParams().filename(output));

    Protein protein1 = model.getElementByElementId("sa1");
    Protein protein2 = model2.getElementByElementId("sa1");
    assertEquals(protein1.getStructuralState(), protein2.getStructuralState());

    protein1 = model.getElementByElementId("sa2");
    protein2 = model2.getElementByElementId("sa2");
    StructuralStateComparator comparator = new StructuralStateComparator();

    assertEquals(0, comparator.compare(protein1.getStructuralState(), protein2.getStructuralState()));
  }

  @Test
  public void testModifications() throws Exception {
    Converter converter = new CellDesignerXmlParser();
    Converter converter2 = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/modifications.xml"));

    String output = File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath();
    converter2.model2File(model, output);
    
    Model model2 = converter2.createModel(new ConverterParams().filename(output).sizeAutoAdjust(false));
    
    Set<String> set1 = new HashSet<>();
    Set<String> set2 = new HashSet<>();

    for (ModificationResidue region : ((Protein)model.getElementByElementId("sa3988")).getModificationResidues()) {
      set1.add(region.toString());
    }

    for (ModificationResidue region : ((Protein)model2.getElementByElementId("sa3988")).getModificationResidues()) {
      set2.add(region.toString());
    }

    SetComparator<String> comparator = new SetComparator<>(new StringComparator());
    
    assertEquals(0, comparator.compare(set1, set2));

    new File(output).delete();
  }


}
