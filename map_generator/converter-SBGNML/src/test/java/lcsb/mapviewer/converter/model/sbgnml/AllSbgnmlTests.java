package lcsb.mapviewer.converter.model.sbgnml;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CellDesigner2SbgnmlConversionTest.class,
    CellDesignerToSbgnTest.class,
    DbSerializationTest.class,
    SbgnmlXmlExporterTest.class,
    SbgnmlXmlParserTest.class,
    SbgnmlXmlParserTest2.class,
})
public class AllSbgnmlTests {

}

