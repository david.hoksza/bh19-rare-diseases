package lcsb.mapviewer.converter.model.sbgnml;

import java.awt.Desktop;
import java.io.File;
import java.nio.file.Files;
import java.util.List;

import org.apache.logging.log4j.core.LogEvent;
import org.junit.*;

import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.graphics.*;
import lcsb.mapviewer.model.map.model.Model;

public class SbgnmlTestFunctions {

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  private MinervaLoggerAppender appender;

  @Before
  public final void _setUp() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    appender = MinervaLoggerAppender.createAppender();
  }

  @After
  public final void _tearDown() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
  }

  protected List<LogEvent> getWarnings() {
    return appender.getWarnings();
  }

  protected void showImage(Model model) throws Exception {
    String dir = Files.createTempDirectory("sbml-temp-images-dir").toFile().getAbsolutePath();
    AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().height(model.getHeight())
        .width(model.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model);
    NormalImageGenerator nig = new PngImageGenerator(params);
    String pathWithouExtension = dir + "/" + model.getName();
    String pngFilePath = pathWithouExtension.concat(".png");
    nig.saveToFile(pngFilePath);
    Desktop.getDesktop().open(new File(pngFilePath));
  }
}
