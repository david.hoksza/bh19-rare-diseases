/**
 * lcsb.mapviewer.converter.model.sbgnml is the main package of SBGN-ML
 * converter.
 */
package lcsb.mapviewer.converter.model.sbgnml;