package lcsb.mapviewer.commands;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.commands.layout.AllLayoutTests;
import lcsb.mapviewer.commands.properties.AllPropertyCommandTests;

@RunWith(Suite.class)
@SuiteClasses({ AllLayoutTests.class,
    AllPropertyCommandTests.class,
    ColorModelCommandTest.class,
    CopyCommandTest.class,
    CreateHierarchyCommandTest.class,
    MoveElementsCommandTest.class,
    SubModelCommandTest.class,
})
public class AllCommandsTests {

}
