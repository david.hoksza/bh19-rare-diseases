package lcsb.mapviewer.commands.properties;

import static org.junit.Assert.*;

import org.junit.*;

import lcsb.mapviewer.commands.CommandTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.Element;

public class ChangeElementSymbolCommandTest extends CommandTestFunctions {

  Model model;

  ModelComparator modelComparator = new ModelComparator();

  @Before
  public void setUp() throws Exception {
    model = createSimpleModel();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testChangeAliasSymbol() throws Exception {
    Element alias = model.getElementByElementId("alias_id");
    ChangeElementSymbolCommand command = new ChangeElementSymbolCommand(model, alias, "test");
    assertFalse("test".equals(alias.getSymbol()));
    command.execute();
    assertTrue(alias.getSymbol().equalsIgnoreCase("test"));
  }

  @Test
  public void testUndo() throws Exception {
    Model model2 = createSimpleModel();
    Element alias = model.getElementByElementId("alias_id");

    // models should be equal before move
    assertEquals(0, modelComparator.compare(model, model2));

    ChangeElementSymbolCommand command = new ChangeElementSymbolCommand(model, alias, "test");
    command.execute();

    // after move models should be different
    assertTrue(0 != modelComparator.compare(model, model2));

    // undo command
    command.undo();

    // after undo they should be the same again
    assertEquals(0, modelComparator.compare(model, model2));

    command.redo();

    // after redo they should be different again
    assertTrue(0 != modelComparator.compare(model, model2));
  }

}
