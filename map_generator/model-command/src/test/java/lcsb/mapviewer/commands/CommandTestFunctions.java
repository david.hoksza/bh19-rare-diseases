package lcsb.mapviewer.commands;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;

import lcsb.mapviewer.common.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.GenericProtein;

public abstract class CommandTestFunctions {
  private static Map<String, Model> models = new HashMap<String, Model>();
  public double EPSILON = 1e-6;
  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  Logger logger = LogManager.getLogger(CommandTestFunctions.class);

  protected Model getModelForFile(String fileName, boolean fromCache) throws Exception {
    if (!fromCache) {
      logger.debug("File without cache: " + fileName);
      Model result = new CellDesignerXmlParser().createModel(new ConverterParams().filename(fileName));
      result.setName(null);
      return result;
    }
    Model result = models.get(fileName);
    if (result == null) {
      logger.debug("File to cache: " + fileName);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      result = parser.createModel(new ConverterParams().filename(fileName).sizeAutoAdjust(false));
      result.setName(null);
      models.put(fileName, result);
    }
    return result;
  }

  protected Model createSimpleModel() {
    Model model = new ModelFullIndexed(null);

    GenericProtein alias = new GenericProtein("alias_id");
    alias.setNotes(null);
    List<String> list = new ArrayList<>();
    list.add("synonym");
    alias.addSynonyms(list);
    List<String> list2 = new ArrayList<>();
    list2.add("f_symbol");
    alias.setFormerSymbols(list2);

    Complex complexAlias = new Complex("complex_alias_id");
    model.addElement(complexAlias);

    complexAlias.addSpecies(alias);

    model.addElement(alias);

    return model;
  }

  protected Model serializeViaCellDesigner(Model original)
      throws InconsistentModelException, InvalidInputDataExecption {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    String xmlString = parser.model2String(original);
    InputStream stream = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));
    Model result = parser.createModel(new ConverterParams().inputStream(stream).sizeAutoAdjust(false));

    for (Drawable bioEntity : original.getDrawables()) {
      bioEntity.setZ(null);
    }
    for (Drawable bioEntity : result.getDrawables()) {
      bioEntity.setZ(null);
    }
    return result;
  }

}
