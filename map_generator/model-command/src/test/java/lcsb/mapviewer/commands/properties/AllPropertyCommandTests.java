package lcsb.mapviewer.commands.properties;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ChangeElementAbbreviationCommandTest.class,
    ChangeElementColorCommandTest.class,
    ChangeElementFontSizeCommandTest.class,
    ChangeElementFormerSymbolCommandTest.class,
    ChangeElementFormulaCommandTest.class,
    ChangeElementFullNameCommandTest.class,
    ChangeElementMiriamDataCommandTest.class,
    ChangeElementNameCommandTest.class,
    ChangeElementNotesCommandTest.class,
    ChangeElementSymbolCommandTest.class,
    ChangeElementSynonymCommandTest.class,

})
public class AllPropertyCommandTests {

}
