package lcsb.mapviewer.commands;

/**
 * Describes status of the {@link ModelCommand}.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ModelCommandStatus {
  /**
   * {@link ModelCommand} was created.
   */
  CREATED,
  /**
   * {@link ModelCommand} was executed.
   */
  EXECUTED,
  /**
   * {@link ModelCommand} was undone.
   */
  UNDONE;
}
