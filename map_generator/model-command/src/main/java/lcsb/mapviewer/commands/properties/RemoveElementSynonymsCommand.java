package lcsb.mapviewer.commands.properties;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that removes
 * synonym from {@link Element Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class RemoveElementSynonymsCommand extends RemoveElementPropertyListEntryCommand<String> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(RemoveElementSynonymsCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link lcsb.mapviewer.commands.ModelCommand#model}
   * @param alias
   *          {@link ChangeElementPropertyCommand#alias}
   * @param values
   *          {@link Element#synonym} values to be removed
   */
  public RemoveElementSynonymsCommand(Model model, Element alias, List<String> values) {
    super(model, alias, values);
  }

  @Override
  protected void undoImplementation() {
    // synonyms are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    getAlias().getSynonyms().addAll(getValues());

    // synonyms are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {

    // synonyms are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    for (String string : getValues()) {
      if (!getAlias().getSynonyms().contains(string)) {
        throw new CommandExecutionException(
            "Cannot remove synonym from the list: " + string + ". Element doesn't exist.");
      }
    }

    getAlias().getSynonyms().removeAll(getValues());

    // synonyms are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());
  }
}
