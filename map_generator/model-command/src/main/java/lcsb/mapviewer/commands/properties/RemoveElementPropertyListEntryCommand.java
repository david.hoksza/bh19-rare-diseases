package lcsb.mapviewer.commands.properties;

import java.util.List;

import lcsb.mapviewer.commands.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that removes
 * entries from a property of the {@link Element Element} that is list.
 * 
 * @param <T>
 *          class of property to edit
 * 
 * @author Piotr Gawron
 *
 */
public abstract class RemoveElementPropertyListEntryCommand<T> extends ModelCommand {

  /**
   * Rlement property values to be removed.
   */
  private List<T> values;

  /**
   * {@link Element} for which we will change the property.
   */
  private Element alias;

  /**
   * Default constructor.
   * 
   * @param model
   *          model to move
   * 
   * @param alias
   *          alias to be changed
   * 
   * @param values
   *          values of the element property to remove
   */
  public RemoveElementPropertyListEntryCommand(Model model, Element alias, List<T> values) {
    super(model);
    this.alias = alias;
    this.values = values;
  }

  @Override
  protected void redoImplementation() throws CommandExecutionException {
    executeImplementation();
    setStatus(ModelCommandStatus.EXECUTED);
  }

  /**
   * @return the alias
   * @see #alias
   */
  protected Element getAlias() {
    return alias;
  }

  /**
   * @param alias
   *          the alias to set
   * @see #alias
   */
  protected void setAlias(Element alias) {
    this.alias = alias;
  }

  /**
   * @return the values
   * @see #values
   */
  public List<T> getValues() {
    return values;
  }

  /**
   * @param values
   *          the values to set
   * @see #values
   */
  public void setValues(List<T> values) {
    this.values = values;
  }

}
