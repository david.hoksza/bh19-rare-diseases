package lcsb.mapviewer.commands.properties;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that removes
 * annotations from {@link Element Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class RemoveElementMiriamDataCommand extends RemoveElementPropertyListEntryCommand<MiriamData> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(RemoveElementMiriamDataCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link lcsb.mapviewer.commands.ModelCommand#model}
   * @param alias
   *          {@link ChangeElementPropertyCommand#alias}
   * @param values
   *          {@link Element#synonym} values to be removed
   */
  public RemoveElementMiriamDataCommand(Model model, Element alias, List<MiriamData> values) {
    super(model, alias, values);
  }

  @Override
  protected void undoImplementation() {
    // miriam data is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    getAlias().getMiriamData().addAll(getValues());

    // miriam data is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {

    // miriam data is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    for (MiriamData string : getValues()) {
      if (!getAlias().getMiriamData().contains(string)) {
        throw new CommandExecutionException(
            "Cannot remove miriam \"" + string + "\" from the list. Element doesn't exist.");
      }
    }

    getAlias().getMiriamData().removeAll(getValues());

    // miriam data is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());
  }
}
