package lcsb.mapviewer.commands;

import java.awt.geom.Path2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.*;

/**
 * Creates a new instance of the model with data limited to region described by
 * polygon.
 * 
 * @author Piotr Gawron
 * 
 */
public class SubModelCommand extends NewModelCommand {

  Logger logger = LogManager.getLogger(SubModelCommand.class);

  /**
   * Polygon that limits the area for the new model.
   * 
   */
  private Path2D polygon;

  private Set<Integer> elementIds;

  private Set<Integer> reactionIds;

  /**
   * Default constructor.
   * 
   * @param model
   *          original {@link NewModelCommand#model}
   * @param polygon
   *          #polygon that limits the area for the new model
   */
  public SubModelCommand(Model model, Path2D polygon) {
    this(model, polygon, new HashSet<>(), new HashSet<>());
  }

  /**
   * Default constructor.
   * 
   * @param model
   *          original {@link NewModelCommand#model}
   * @param polygon
   *          #polygon that limits the area for the new model
   */
  public SubModelCommand(Model model, Path2D polygon, Set<Integer> elementIds, Set<Integer> reactionIds) {
    super(model);
    this.polygon = polygon;
    this.elementIds = elementIds;
    this.reactionIds = reactionIds;
  }

  @Override
  public Model execute() {
    CopyCommand copy = new CopyCommand(getModel());
    Model result = copy.execute();

    Set<Element> inputElements = new HashSet<>();
    if (elementIds.size() == 0) {
      inputElements.addAll(result.getElements());
    } else {
      for (Element element : getModel().getElements()) {
        if (elementIds.contains(element.getId())) {
          inputElements.add(result.getElementByElementId(element.getElementId()));
        }
      }
    }

    Set<Element> aliasNotToRemove = new HashSet<>();

    for (Element alias : result.getElements()) {
      if (polygon.intersects(alias.getBorder())) {
        aliasNotToRemove.add(alias);
      }
    }
    aliasNotToRemove.retainAll(inputElements);

    boolean added = false;
    do {
      Set<Element> iterativeAliasNotToRemove = new HashSet<>();
      for (Element alias : aliasNotToRemove) {
        Element parent = alias.getCompartment();
        if (parent != null) {
          if (!aliasNotToRemove.contains(parent)) {
            iterativeAliasNotToRemove.add(parent);
          }
        }
        if (alias instanceof Species) {
          parent = ((Species) alias).getComplex();
          if (parent != null) {
            if (!aliasNotToRemove.contains(parent)) {
              iterativeAliasNotToRemove.add(parent);
            }
          }
        }
      }

      added = iterativeAliasNotToRemove.size() != 0;
      aliasNotToRemove.addAll(iterativeAliasNotToRemove);
    } while (added);

    List<Element> aliasToRemove = new ArrayList<>();
    for (Element element : result.getElements()) {
      if (!(polygon.intersects(element.getBorder())) || !inputElements.contains(element)) {
        boolean remove = true;
        if (element instanceof Species) {
          remove = ((Species) element).getComplex() == null;
        }
        if (aliasNotToRemove.contains(element)) {
          remove = false;
        }
        if (remove) {
          aliasToRemove.add(element);
          if (element instanceof Complex) {
            List<Species> aliases = ((Complex) element).getAllChildren();
            aliasToRemove.addAll(aliases);
          }
        }
      }

    }
    for (Element alias : aliasToRemove) {
      result.removeElement(alias);
    }

    Set<String> reactionStringIds = new HashSet<>();
    for (Reaction reaction : getModel().getReactions()) {
      if (reactionIds.size() == 0 || reactionIds.contains(reaction.getId())) {
        reactionStringIds.add(reaction.getElementId());
      }
    }

    List<Reaction> reactionsToRemove = new ArrayList<>();
    for (Reaction reaction : result.getReactions()) {
      boolean remove = false;
      if (!reactionStringIds.contains(reaction.getElementId())) {
        remove = true;
      } else {
        for (ReactionNode node : reaction.getReactionNodes()) {
          if (!result.getElements().contains(node.getElement())) {
            remove = true;
            break;
          }
        }
      }
      if (remove) {
        reactionsToRemove.add(reaction);
      }
    }
    for (Reaction reaction : reactionsToRemove) {
      result.removeReaction(reaction);
    }

    for (Layer layer : result.getLayers()) {
      List<LayerText> textToRemove = new ArrayList<>();
      for (LayerText text : layer.getTexts()) {
        if (!(polygon.intersects(text.getBorder()))) {
          textToRemove.add(text);
        }
      }
      for (LayerText text : textToRemove) {
        layer.removeLayerText(text);
      }
    }

    return result;
  }

}
