package lcsb.mapviewer.commands.properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes
 * annotations of the {@link Element Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class ChangeElementMiriamDataCommand extends ChangeElementPropertyListEntryCommand<MiriamData> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(ChangeElementMiriamDataCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link lcsb.mapviewer.commands.ModelCommand#model}
   * @param alias
   *          {@link ChangeElementPropertyCommand#alias}
   * @param newValue
   *          new annotation value
   * @param oldValue
   *          old annotation value
   */
  public ChangeElementMiriamDataCommand(Model model, Element alias, MiriamData newValue, MiriamData oldValue) {
    super(model, alias, newValue, oldValue);
  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {
    // if there is no change then return
    if (getOldValue().equals(getNewValue())) {
      return;
    }

    // miriam data is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    if (!getAlias().getMiriamData().contains(getOldValue())) {
      throw new CommandExecutionException("Miriam " + getOldValue() + " doesn't exist.");
    }
    if (getAlias().getMiriamData().contains(getNewValue())) {
      throw new CommandExecutionException("Miriam " + getNewValue() + " already exist.");
    }
    getAlias().getMiriamData().remove(getOldValue());
    getOldValue().setDataType(getNewValue().getDataType());
    getOldValue().setRelationType(getNewValue().getRelationType());
    getOldValue().setResource(getNewValue().getResource());
    getAlias().getMiriamData().add(getOldValue());

    // miriam data is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());
  }
}
