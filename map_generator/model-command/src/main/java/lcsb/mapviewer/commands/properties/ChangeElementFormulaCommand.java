package lcsb.mapviewer.commands.properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes
 * formula of the element connected to {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class ChangeElementFormulaCommand extends ChangeElementPropertyCommand<String> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(ChangeElementFormulaCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link lcsb.mapviewer.commands.ModelCommand#model}
   * @param alias
   *          {@link ChangeElementPropertyCommand#alias}
   * @param newName
   *          new {@link Element#formula} value
   */
  public ChangeElementFormulaCommand(Model model, Element alias, String newName) {
    super(model, alias, newName);
  }

  @Override
  protected void executeImplementation() {

    // formula is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    setOldValue(getAlias().getFormula());
    getAlias().setFormula((String) getNewValue());

    // formula is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());
  }
}
