package lcsb.mapviewer.commands.properties;

import lcsb.mapviewer.commands.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes single
 * entry in a property of the {@link Element Element} that is list.
 * 
 * @param <T>
 *          class of property to edit
 * 
 * @author Piotr Gawron
 *
 */
public abstract class ChangeElementPropertyListEntryCommand<T> extends ModelCommand {

  /**
   * New element property value.
   */
  private T newValue;

  /**
   * Old element property value.
   */
  private T oldValue;

  /**
   * {@link Element} for which we will change the property.
   */
  private Element alias;

  /**
   * Default constructor.
   * 
   * @param model
   *          model to move
   * 
   * @param alias
   *          alias to be changed
   * 
   * @param newValue
   *          new value of the element property
   * 
   * @param oldValue
   *          old value of the element property
   */
  public ChangeElementPropertyListEntryCommand(Model model, Element alias, T newValue, T oldValue) {
    super(model);
    this.alias = alias;
    this.oldValue = oldValue;
    this.newValue = newValue;
  }

  @Override
  protected void undoImplementation() throws CommandExecutionException {
    T tmpNewValue = newValue;
    T tmpOldValue = oldValue;
    newValue = tmpOldValue;
    oldValue = tmpNewValue;
    executeImplementation();
    newValue = tmpNewValue;
    oldValue = tmpOldValue;
    setStatus(ModelCommandStatus.UNDONE);
  }

  @Override
  protected void redoImplementation() throws CommandExecutionException {
    executeImplementation();
    setStatus(ModelCommandStatus.EXECUTED);
  }

  /**
   * @return the alias
   * @see #alias
   */
  protected Element getAlias() {
    return alias;
  }

  /**
   * @param alias
   *          the alias to set
   * @see #alias
   */
  protected void setAlias(Element alias) {
    this.alias = alias;
  }

  /**
   * @return the oldValue
   * @see #oldValue
   */
  protected T getOldValue() {
    return oldValue;
  }

  /**
   * @param oldValue
   *          the oldValue to set
   * @see #oldValue
   */
  protected void setOldValue(T oldValue) {
    this.oldValue = oldValue;
  }

  /**
   * @return the newValue
   * @see #newValue
   */
  protected T getNewValue() {
    return newValue;
  }

  /**
   * @param newValue
   *          the newValue to set
   * @see #newValue
   */
  protected void setNewValue(T newValue) {
    this.newValue = newValue;
  }
}
