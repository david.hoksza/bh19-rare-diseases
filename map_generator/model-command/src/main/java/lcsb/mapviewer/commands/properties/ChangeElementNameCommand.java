package lcsb.mapviewer.commands.properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes name
 * of the element connected to {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class ChangeElementNameCommand extends ChangeElementPropertyCommand<String> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(ChangeElementNameCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link lcsb.mapviewer.commands.ModelCommand#model}
   * @param alias
   *          {@link ChangeElementPropertyCommand#alias}
   * @param newName
   *          new name of the elemnt
   */
  public ChangeElementNameCommand(Model model, Element alias, String newName) {
    super(model, alias, newName);
  }

  @Override
  protected void executeImplementation() {

    includeInAffectedRegion(getAlias());

    setOldValue(getAlias().getName());
    getAlias().setName((String) getNewValue());

    includeInAffectedRegion(getAlias());
  }
}
