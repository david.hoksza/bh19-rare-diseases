package lcsb.mapviewer.commands.properties;

import java.util.List;

import lcsb.mapviewer.commands.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that add entries
 * into a property of the {@link Element Element} that is list.
 * 
 * @param <T>
 *          class of property to edit
 * 
 * @author Piotr Gawron
 *
 */
public abstract class AddElementPropertyListEntryCommand<T> extends ModelCommand {

  /**
   * List of new values that should be added to property.
   */
  private List<T> newValues;

  /**
   * {@link Element} for which we will change the property.
   */
  private Element alias;

  /**
   * Default constructor.
   * 
   * @param model
   *          model to move
   * 
   * @param alias
   *          alias to be changed
   * 
   * @param newValues
   *          new values of the element property that will be added
   * 
   */
  public AddElementPropertyListEntryCommand(Model model, Element alias, List<T> newValues) {
    super(model);
    this.alias = alias;
    this.newValues = newValues;
  }

  @Override
  protected void redoImplementation() throws CommandExecutionException {
    executeImplementation();
    setStatus(ModelCommandStatus.EXECUTED);
  }

  /**
   * @return the alias
   * @see #alias
   */
  protected Element getAlias() {
    return alias;
  }

  /**
   * @param alias
   *          the alias to set
   * @see #alias
   */
  protected void setAlias(Element alias) {
    this.alias = alias;
  }

  /**
   * @return the newValue
   * @see #newValue
   */
  protected List<T> getNewValues() {
    return newValues;
  }

  /**
   * @param newValue
   *          the newValue to set
   * @see #newValue
   */
  protected void setNewValues(List<T> newValue) {
    this.newValues = newValue;
  }
}
