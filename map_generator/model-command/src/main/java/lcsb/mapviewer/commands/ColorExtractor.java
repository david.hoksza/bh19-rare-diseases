package lcsb.mapviewer.commands;

import java.awt.*;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.layout.ColorSchema;

/**
 * Class that extracts color that should be used for drawing from
 * {@link ColorSchema}.
 * 
 * @author Piotr Gawron
 *
 */
public class ColorExtractor {

  /**
   * Color that should be used for min values of {@link ColorSchema#value}.
   */
  private Color minColor;

  /**
   * Color that should be used for maxvalues of {@link ColorSchema#value}.
   */
  private Color maxColor;

  private Color simpleColor;

  /**
   * Default constructor.
   * 
   * @param minColor
   *          Color that should be used for min values of
   *          {@link ColorSchema#value}
   * @param maxColor
   *          Color that should be used for max values of
   *          {@link ColorSchema#value}
   */
  public ColorExtractor(Color minColor, Color maxColor, Color simpleColor) {
    if (minColor == null || maxColor == null || simpleColor == null) {
      throw new InvalidArgumentException("Parameters cannot be null");
    }
    this.minColor = minColor;
    this.maxColor = maxColor;
    this.simpleColor = simpleColor;
  }

  /**
   * Extracts color from {@link ColorSchema} object.
   * 
   * @param colorSchema
   *          {@link ColorSchema} from which {@link Color} should be extracted
   * 
   * @return color from {@link ColorSchema} object
   */
  public Color getNormalizedColor(ColorSchema colorSchema) {
    if (colorSchema.getColor() != null) {
      return colorSchema.getColor();
    } else if (colorSchema.getValue() == null) {
      return simpleColor;
    } else {
      return getColorForValue(colorSchema.getValue());
    }
  }

  /**
   * Returns color from red - green scale for the given normalized double value
   * (from range -1,1).
   * 
   * @param value
   *          double value that should be converted into color
   * @return color for the double value
   */
  protected Color getColorForValue(Double value) {
    if (value > 0) {
      double ratio = value;
      return new Color((int) (maxColor.getRed() * ratio), (int) (maxColor.getGreen() * ratio),
          (int) (maxColor.getBlue() * ratio));
    }
    if (value < 0) {
      double ratio = -value;
      return new Color((int) (minColor.getRed() * ratio), (int) (minColor.getGreen() * ratio),
          (int) (minColor.getBlue() * ratio));
    }
    return Color.WHITE;
  }

  /**
   * @return the minColor
   * @see #minColor
   */
  public Color getMinColor() {
    return minColor;
  }

  /**
   * @return the maxColor
   * @see #maxColor
   */
  public Color getMaxColor() {
    return maxColor;
  }

  public Color getSimpleColor() {
    return simpleColor;
  }
}
