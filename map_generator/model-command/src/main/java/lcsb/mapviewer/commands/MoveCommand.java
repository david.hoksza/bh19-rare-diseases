package lcsb.mapviewer.commands;

import java.awt.geom.Point2D;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.*;

/**
 * Command which moves model by dx, dy coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class MoveCommand extends ModelCommand {

  /**
   * Delta x.
   */
  private double dx;
  /**
   * Delta y.
   */
  private double dy;

  /**
   * Default constructor.
   * 
   * @param model
   *          model to move
   * @param dx
   *          delta x
   * @param dy
   *          delat y
   */
  public MoveCommand(Model model, double dx, double dy) {
    super(model);
    this.dx = dx;
    this.dy = dy;
  }

  @Override
  protected void undoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void redoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void executeImplementation() {

    Model model = getModel();

    for (Element alias : model.getElements()) {
      alias.setX(alias.getX() + dx);
      alias.setY(alias.getY() + dy);
      if (alias instanceof Compartment) {
        ((Compartment) alias).setNamePoint(((Compartment) alias).getNamePoint().getX() + dx,
            ((Compartment) alias).getNamePoint().getY() + dy);
      }
      if (alias instanceof SpeciesWithModificationResidue) {
        for (ModificationResidue mr : ((SpeciesWithModificationResidue) alias).getModificationResidues()) {
          mr.setPosition(new Point2D.Double(mr.getPosition().getX() + dx, mr.getPosition().getY() + dy));
        }
      }
      StructuralState state = null;
      if (alias instanceof Protein) {
        state = ((Protein) alias).getStructuralState();
      }
      if (alias instanceof Complex) {
        state = ((Complex) alias).getStructuralState();
      }
      if (state != null) {
        state.setPosition(
            new Point2D.Double(state.getPosition().getX() + dx, state.getPosition().getY() + dy));
      }

    }
    for (Reaction reaction : model.getReactions()) {
      for (AbstractNode node : reaction.getNodes()) {
        for (Point2D point : node.getLine().getPoints()) {
          point.setLocation(point.getX() + dx, point.getY() + dy);
        }
      }
      for (Point2D point : reaction.getLine().getPoints()) {
        point.setLocation(point.getX() + dx, point.getY() + dy);
      }

    }
  }

}
