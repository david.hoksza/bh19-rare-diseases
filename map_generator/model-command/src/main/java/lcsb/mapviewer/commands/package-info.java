/**
 * This package contains commands (some standalone operations) that can be
 * performed on {@link lcsb.mapviewer.model.map.model.Model}.
 * 
 */
package lcsb.mapviewer.commands;
