package lcsb.mapviewer.commands.properties;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes former
 * sybmols of the {@link Element element}.
 * 
 * @author Piotr Gawron
 *
 */
public class AddElementFormerSymbolsCommand extends AddElementPropertyListEntryCommand<String> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(AddElementFormerSymbolsCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link lcsb.mapviewer.commands.ModelCommand#model}
   * @param alias
   *          {@link ChangeElementPropertyCommand#alias}
   * @param values
   *          new {@link Element#synonym} values to be added
   */
  public AddElementFormerSymbolsCommand(Model model, Element alias, List<String> values) {
    super(model, alias, values);
  }

  @Override
  protected void undoImplementation() {
    // former symbols are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    getAlias().getFormerSymbols().removeAll(getNewValues());

    // former symbols are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {

    // former symbols are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    for (String string : getNewValues()) {
      if (getAlias().getFormerSymbols().contains(string)) {
        throw new CommandExecutionException(
            "Cannot add former synonym \"" + string + "\"to the list. Element already exists.");
      }
    }

    getAlias().getFormerSymbols().addAll(getNewValues());

    // former symbols are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());
  }

}
