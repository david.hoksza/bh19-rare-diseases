package lcsb.mapviewer.commands.properties;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes
 * synonyms of the {@link Element Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class AddElementSynonymsCommand extends AddElementPropertyListEntryCommand<String> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(AddElementSynonymsCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link lcsb.mapviewer.commands.ModelCommand#model}
   * @param alias
   *          {@link ChangeElementPropertyCommand#alias}
   * @param values
   *          new {@link Element#synonym} values to be added
   */
  public AddElementSynonymsCommand(Model model, Element alias, List<String> values) {
    super(model, alias, values);
  }

  @Override
  protected void undoImplementation() {
    // synonyms are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    getAlias().getSynonyms().removeAll(getNewValues());

    // synonyms are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {

    // synonyms are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    for (String string : getNewValues()) {
      if (getAlias().getSynonyms().contains(string)) {
        throw new CommandExecutionException("Cannot add synonym to the list: " + string + ". Element already exists.");
      }
    }

    getAlias().getSynonyms().addAll(getNewValues());

    // synonyms are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());
  }
}
