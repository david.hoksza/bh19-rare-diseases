package lcsb.mapviewer.commands.layout;

import java.awt.geom.*;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.DoubleDimension;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierTypeUtils;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.type.*;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;

public class ApplySimpleLayoutModelCommand extends ApplyLayoutModelCommand {

  public static final double COMPARTMENT_BORDER = 10;
  private static final double BORDER_FROM_EXISTING_ELEMENTS = 10;
  private static final int SPECIES_WIDTH = 90;
  private static final int SPECIES_HEIGHT = 30;
  private static final double COMPLEX_PADDING = 5;

  private double borderOffset = COMPARTMENT_BORDER;
  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(ApplySimpleLayoutModelCommand.class);
  private PointTransformation pt = new PointTransformation();

  public ApplySimpleLayoutModelCommand(Model model, Collection<BioEntity> bioEntities, Double minX, Double minY,
      Double maxX, Double maxY, boolean addReactionPrefixes) {
    super(model, bioEntities, minX, minY, maxX, maxY, addReactionPrefixes);
  }

  public ApplySimpleLayoutModelCommand(Model model, Collection<BioEntity> bioEntities) {
    this(model, bioEntities, null, null, null, null, false);
  }

  public ApplySimpleLayoutModelCommand(Model model, Collection<BioEntity> bioEntities, boolean addReactionPrefixes) {
    this(model, bioEntities, null, null, null, null, addReactionPrefixes);
  }

  public ApplySimpleLayoutModelCommand(Model model) {
    this(model, null);
  }

  private void createLayout(Model model, Collection<BioEntity> bioEntites, Compartment compartment, Point2D minPoint,
      Dimension2D dimension) {
    if (dimension == null) {
      dimension = estimateDimension(bioEntites);
    }
    if (minPoint == null) {
      minPoint = estimateLayoutMinPoint(model);
    }
    modifyModelSize(model, minPoint, dimension);
    Collection<Element> elements = new HashSet<>();
    Collection<Reaction> reactions = new HashSet<>();
    for (BioEntity bioEntity : bioEntites) {
      if (bioEntity instanceof Element) {
        elements.add((Element) bioEntity);
      } else if (bioEntity instanceof Reaction) {
        reactions.add((Reaction) bioEntity);
      } else {
        throw new InvalidArgumentException("Unknown class type: " + bioEntity);
      }
    }

    modifyElementLocation(elements, null, minPoint, dimension);
    modifyReactionLocation(reactions);

  }

  protected void modifyElementLocation(Collection<Element> elements, Compartment parent, Point2D minPoint,
      Dimension2D dimension) {
    Set<Compartment> compartments = new HashSet<>();
    Set<Species> elementToAlign = new HashSet<>();
    Map<Compartment, Set<Element>> elementsByStaticCompartment = new HashMap<>();
    for (Element element : elements) {
      if (element.getWidth() == null || element.getWidth() == 0) {
        element.setWidth(SPECIES_WIDTH);
        element.setHeight(SPECIES_HEIGHT);

      }
      if (element.getCompartment() == null || element.getCompartment() == parent) {
        if (element instanceof Compartment) {
          compartments.add((Compartment) element);
        } else if (element instanceof Species) {
          elementToAlign.add((Species) element);
        } else {
          throw new InvalidArgumentException("Unknown element type: " + element.getClass());
        }
      } else {
        Set<Element> elementsSet = elementsByStaticCompartment.get(element.getCompartment());
        if (elementsSet == null) {
          elementsSet = new HashSet<>();
          elementsByStaticCompartment.put(element.getCompartment(), elementsSet);
        }
        elementsSet.add(element);
      }
    }

    if (compartments.size() == 1 && elementToAlign.size() == 0) {
      Compartment compartment = compartments.iterator().next();
      Rectangle2D border = computeCompartmentBorder(compartment, elements, minPoint, dimension);
      compartment.setBorder(border);
      compartment.setNamePoint(border.getX() + COMPARTMENT_BORDER * 2, border.getY() + COMPARTMENT_BORDER * 2);
      Point2D point = new Point2D.Double(border.getX(), border.getY());
      Dimension2D recursiveDimension = new DoubleDimension(border.getWidth(), border.getHeight());
      Set<Element> elementsToModify = new HashSet<>(compartment.getElements());
      elementsToModify.retainAll(elements);
      modifyElementLocation(elementsToModify, compartment, point, recursiveDimension);
    } else if (compartments.size() == 0) {
      modifySpeciesListLocation(elementToAlign, minPoint, dimension);
    } else {
      Collection<Element> firstHalf = new HashSet<>();
      Collection<Element> secondHalf = new HashSet<>();

      int firstHalfCount = 0;
      int secondHalfCount = 0;

      for (Compartment compartment : compartments) {
        int compSize = compartment.getAllSubElements().size() + 1;
        if (firstHalfCount < secondHalfCount) {
          firstHalf.add(compartment);
          firstHalfCount += compSize;
        } else {
          secondHalf.add(compartment);
          secondHalfCount += compSize;
        }
      }
      if (firstHalfCount < secondHalfCount) {
        firstHalf.addAll(elementToAlign);
        firstHalfCount += elementToAlign.size();
      } else {
        secondHalf.addAll(elementToAlign);
        secondHalfCount += elementToAlign.size();
      }
      if (dimension.getWidth() > dimension.getHeight()) {
        Dimension2D recursiveDimension = new DoubleDimension(dimension.getWidth() / 2, dimension.getHeight());
        modifyElementLocation(firstHalf, parent, minPoint, recursiveDimension);
        Point2D point = new Point2D.Double(minPoint.getX() + dimension.getWidth() / 2, minPoint.getY());
        modifyElementLocation(secondHalf, parent, point, recursiveDimension);
      } else {
        Dimension2D recursiveDimension = new DoubleDimension(dimension.getWidth(), dimension.getHeight() / 2);
        modifyElementLocation(firstHalf, parent, minPoint, recursiveDimension);
        Point2D point = new Point2D.Double(minPoint.getX(), minPoint.getY() + dimension.getHeight() / 2);
        modifyElementLocation(secondHalf, parent, point, recursiveDimension);
      }
    }
    List<Compartment> compartmentWithElementsToRearrange = new ArrayList<>();
    compartmentWithElementsToRearrange.addAll(elementsByStaticCompartment.keySet());
    Collections.sort(compartmentWithElementsToRearrange, Element.SIZE_COMPARATOR);
    for (Compartment compartment : compartmentWithElementsToRearrange) {
      Point2D point = new Point2D.Double(compartment.getX(), compartment.getY());
      Dimension2D compDimension = new DoubleDimension(compartment.getWidth(), compartment.getHeight());
      modifyElementLocation(elementsByStaticCompartment.get(compartment), compartment, point, compDimension);
    }

  }

  private Rectangle2D computeCompartmentBorder(Compartment compartment, Collection<Element> elements, Point2D minPoint,
      Dimension2D dimension) {
    Rectangle2D border = new Rectangle2D.Double(minPoint.getX() + borderOffset,
        minPoint.getY() + borderOffset, dimension.getWidth() - borderOffset * 2,
        dimension.getHeight() - borderOffset * 2);
    for (Element element : compartment.getElements()) {
      if (!elements.contains(element) && element.getBorder() != null && element.getX() != 0 && element.getY() != 0) {
        border.add(element.getBorder());
      }
    }
    return border;
  }

  private void modifyReactionLocation(Collection<Reaction> reactions) {
    for (Reaction reaction : reactions) {
      modifyReaction(reaction);
    }
  }

  private void modifyReaction(Reaction reaction) {
    for (AbstractNode node : reaction.getOperators()) {
      reaction.removeNode(node);
    }

    Element productElement = reaction.getProducts().get(0).getElement();
    Element reactantElement = reaction.getReactants().get(0).getElement();

    Point2D centerLineStart = pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.4);
    Point2D centerLineEnd = pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.6);
    Point2D middle = pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.5);

    PolylineData centerLine = new PolylineData();
    // for self reactions
    if (centerLineStart.equals(centerLineEnd)) {
      centerLine.addPoint(pt.copyPoint(centerLineStart));
      centerLineStart.setLocation(centerLineStart.getX(), centerLineStart.getY() + 50);
      centerLine.addPoint(pt.copyPoint(centerLineStart));
      centerLine.addPoint(pt.copyPoint(centerLineEnd));
      centerLineEnd.setLocation(centerLineStart.getX(), centerLineEnd.getY() + 50);
    } else {
      centerLine.addPoint(pt.copyPoint(centerLineStart));
      centerLine.addPoint(pt.copyPoint(centerLineEnd));
    }
    reaction.setLine(centerLine);

    modifyReactants(reaction, centerLineStart);
    modifyProducts(reaction, centerLineEnd);
    modifyModifiers(reaction, middle);

    if (super.isAddReactionPrefixes()) {
      super.getModel().removeReaction(reaction);
      reaction.setIdReaction("reaction_glyph_" + getNextId() + "__" + reaction.getIdReaction());
      super.getModel().addReaction(reaction);
    }
  }

  private void modifyReactants(Reaction reaction, Point2D middle) {
    NodeOperator operator = null;
    if (reaction.getReactants().size() > 1) {
      if (reaction instanceof HeterodimerAssociationReaction) {
        operator = new AssociationOperator();
      } else {
        operator = new AndOperator();
      }
      Point2D productPoint = reaction.getReactants().get(0).getElement().getCenter();
      Point2D operatorPoint = pt.getPointOnLine(middle, productPoint, 0.5);
      operator.setLine(new PolylineData(operatorPoint, middle));
      reaction.addNode(operator);
      middle = operatorPoint;
    }
    for (Reactant reactant : reaction.getReactants()) {
      PolylineData line = new PolylineData();
      line.addPoint(reactant.getElement().getCenter());
      line.addPoint(middle);
      if (reaction.isReversible()) {
        line.getBeginAtd().setArrowType(ArrowType.FULL);
      }
      reactant.setLine(line);
      if (operator != null) {
        operator.addInput(reactant);
      }
    }
  }

  private void modifyModifiers(Reaction reaction, Point2D middle) {
    for (Modifier modifier : reaction.getModifiers()) {
      PolylineData line = new PolylineData(middle, modifier.getElement().getCenter());
      modifier.setLine(line);
      new ModifierTypeUtils().updateLineEndPoint(modifier);
    }
  }

  private void modifyProducts(Reaction reaction, Point2D middle) {
    NodeOperator operator = null;
    if (reaction.getProducts().size() > 1) {
      if (reaction instanceof DissociationReaction) {
        operator = new DissociationOperator();
      } else if (reaction instanceof TruncationReaction) {
        operator = new TruncationOperator();
      } else {
        operator = new SplitOperator();
      }
      Point2D productPoint = reaction.getProducts().get(0).getElement().getCenter();
      Point2D operatorPoint = pt.getPointOnLine(middle, productPoint, 0.5);
      operator.setLine(new PolylineData(operatorPoint, middle));
      reaction.addNode(operator);
      middle = operatorPoint;
    }
    for (Product product : reaction.getProducts()) {
      PolylineData line = new PolylineData(middle, product.getElement().getCenter());
      line.getEndAtd().setArrowType(ArrowType.FULL);
      product.setLine(line);
      if (operator != null) {
        operator.addOutput(product);
      }
    }

  }

  protected void modifySpeciesListLocation(Set<Species> speciesList, Point2D minPoint, Dimension2D dimension) {
    double middleX = dimension.getWidth() / 2 + minPoint.getX();
    double middleY = dimension.getHeight() / 2 + minPoint.getY();

    double radiusX = Math.min(dimension.getWidth() / 3, dimension.getWidth() - SPECIES_WIDTH / 2);
    if (radiusX < 1) {
      radiusX = 1;
    }
    double radiusY = Math.min(dimension.getHeight() / 3, dimension.getHeight() - SPECIES_HEIGHT / 2);
    if (radiusY < 1) {
      radiusY = 1;
    }
    double radius = Math.min(radiusX, radiusY);

    double size = speciesList.size();
    double index = 0;

    for (Species element : speciesList) {
      if (element.getComplex() == null) {
        double angle = index / size * 2 * Math.PI;
        double elementCenterX = middleX + Math.sin(angle) * radius;
        double elementCenterY = middleY + Math.cos(angle) * radius;
        if (element instanceof Complex) {
          modifyComplexLocation((Complex) element, elementCenterX, elementCenterY);
        } else {
          element.setWidth(SPECIES_WIDTH);
          element.setHeight(SPECIES_HEIGHT);
        }
        element.setX(elementCenterX - SPECIES_WIDTH / 2);
        element.setY(elementCenterY - SPECIES_HEIGHT / 2);
        index++;
      }
    }

    for (Species element : speciesList) {
      if (element instanceof SpeciesWithModificationResidue) {
        modifyElementModificationResiduesLocation((SpeciesWithModificationResidue) element);
      }
    }

  }

  private void modifyElementModificationResiduesLocation(SpeciesWithModificationResidue element) {
    for (ModificationResidue mr : element.getModificationResidues()) {
      List<Point2D> usedPoints = new ArrayList<>();
      for (ModificationResidue inUseModification : element.getModificationResidues()) {
        if (inUseModification.getPosition() != null) {
          usedPoints.add(inUseModification.getPosition());
        }
      }
      mr.setPosition(findFarthestEmptyPositionOnRectangle(((Species) element).getBorder(), usedPoints));
    }
  }

  Point2D findFarthestEmptyPositionOnRectangle(Rectangle2D border, List<Point2D> usedPoints) {
    // list of points reduced to one dimension
    // every point is taking two position - so we can easier solve wrapping issues
    List<Double> linearizedPoints = new ArrayList<>();
    // there are two "guarding" points at the beginning and the end
    linearizedPoints.add(0.0);
    linearizedPoints.add(4 * (border.getWidth() + border.getHeight()));

    for (Point2D point : usedPoints) {
      Double linearizedPoint = null;
      if (Math.abs(point.getY() - border.getMinY()) < Configuration.EPSILON) {
        // upper edge
        linearizedPoint = point.getX() - border.getMinX();
      } else if (Math.abs(point.getX() - border.getMaxX()) < Configuration.EPSILON) {
        // right edge
        linearizedPoint = point.getY() - border.getMinY() + border.getWidth();
      } else if (Math.abs(point.getY() - border.getMaxY()) < Configuration.EPSILON) {
        // bottom edge
        linearizedPoint = border.getMaxX() - point.getX() + border.getWidth() + border.getHeight();
      } else if (Math.abs(point.getX() - border.getMinX()) < Configuration.EPSILON) {
        // left edge
        linearizedPoint = border.getMaxY() - point.getY() + border.getWidth() + border.getHeight() + border.getWidth();
      } else {
        logger.warn("Cannot align modification position on the species border");
      }
      linearizedPoints.add(linearizedPoint);
      linearizedPoints.add(linearizedPoint + 2 * (border.getWidth() + border.getHeight()));

    }
    Collections.sort(linearizedPoints);

    Double longest = linearizedPoints.get(1) - linearizedPoints.get(0);
    Double position = (linearizedPoints.get(1) + linearizedPoints.get(0)) / 2;
    for (int i = 1; i < linearizedPoints.size() - 1; i++) {
      if (longest < linearizedPoints.get(i + 1) - linearizedPoints.get(i)) {
        longest = linearizedPoints.get(i + 1) - linearizedPoints.get(i);
        position = (linearizedPoints.get(i + 1) + linearizedPoints.get(i)) / 2;
      }
    }

    if (position > 2 * (border.getWidth() + border.getHeight())) {
      position -= 2 * (border.getWidth() + border.getHeight());
    }

    if (position < border.getWidth()) {
      // upper edge
      return new Point2D.Double(border.getMinX() + position, border.getMinY());
    } else if (position < border.getWidth() + border.getHeight()) {
      // right edge
      return new Point2D.Double(border.getMaxX(), border.getMinY() + position - border.getWidth());
    } else if (position < border.getWidth() + border.getHeight() + border.getWidth()) {
      // bottom edge
      return new Point2D.Double(border.getMinX() + position - border.getHeight() - border.getWidth(), border.getMaxY());
    } else {
      // left edge
      return new Point2D.Double(border.getMinX(),
          border.getMinY() + position - border.getWidth() - border.getHeight() - border.getWidth());
    }
  }

  protected void modifyModelSize(Model model, Point2D minPoint, Dimension2D dimension) {
    double width = model.getWidth();
    double height = model.getHeight();
    width = Math.max(width, minPoint.getX() + dimension.getWidth());
    height = Math.max(height, minPoint.getY() + dimension.getHeight());
    model.setWidth(width);
    model.setHeight(height);
    borderOffset = Math.max(borderOffset, Math.max(width, height) * 0.01);
  }

  protected Point2D estimateLayoutMinPoint(Model model) {
    double minX = 0.0;
    double minY = 0.0;
    for (Element element : model.getElements()) {
      if (!getBioEntities().contains(element)) {
        if (element.getWidth() > 0 && element.getHeight() > 0) {
          minY = Math.max(minY, element.getBorder().getMaxY() + BORDER_FROM_EXISTING_ELEMENTS);
        }
      }
    }
    return new Point2D.Double(minX, minY);
  }

  protected Dimension2D estimateDimension(Collection<BioEntity> bioEntites) {
    int elementCount = 0;
    for (BioEntity bioEntity : bioEntites) {
      if (bioEntity instanceof Element) {
        elementCount++;
      }
    }
    double suggestedSize = Math.max(100 * elementCount,
        Math.sqrt((SPECIES_WIDTH + 10) * (SPECIES_HEIGHT + 10) * elementCount));

    if (bioEntites.size() > 0) {
      suggestedSize = Math.max(suggestedSize, 2 * (SPECIES_WIDTH + 10));
    }
    return new DoubleDimension(suggestedSize, suggestedSize);
  }

  @Override
  protected void undoImplementation() throws CommandExecutionException {
    throw new NotImplementedException();

  }

  @Override
  protected void redoImplementation() throws CommandExecutionException {
    throw new NotImplementedException();
  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {
    Set<Model> models = new HashSet<>();
    models.add(this.getModel());
    models.addAll(this.getModel().getSubmodels());
    for (Model model : models) {
      Collection<BioEntity> bioEntites = new HashSet<>();
      for (BioEntity bioEntity : this.getBioEntities()) {
        if (bioEntity.getModel() == model) {
          bioEntites.add(bioEntity);
        }
      }
      if (bioEntites.size() > 0) {
        Dimension2D dimension = null;
        Point2D minPoint = null;
        if (model == this.getModel()) {
          dimension = getStartDimension();
          minPoint = getStartPoint();
        }
        createLayout(model, bioEntites, null, minPoint, dimension);
      }
    }
    computeZIndex(this.getBioEntities());
  }

  private void computeZIndex(Collection<BioEntity> bioEntities) {
    int zIndex = 0;
    List<Element> elements = new ArrayList<>();
    for (BioEntity bioEntity : bioEntities) {
      if (bioEntity instanceof Element) {
        elements.add((Element) bioEntity);
      } else {
        Reaction r = (Reaction) bioEntity;
        if (r.getZ() == null) {
          r.setZ(zIndex);
        }
      }
    }
    elements.sort(new Comparator<Element>() {
      @Override
      public int compare(Element o1, Element o2) {
        Double size1 = o1.getHeight() * o1.getHeight();
        Double size2 = o2.getHeight() * o2.getHeight();
        return -size1.compareTo(size2);
      }
    });
    for (Element element : elements) {
      if (element.getZ() != null) {
        zIndex = Math.max(zIndex, element.getZ() + 1);
      } else {
        element.setZ(zIndex++);
      }
    }
  }

  protected void modifyComplexLocation(Complex complex, double centerX, double centerY) {
    if (complex.getWidth() == null || complex.getHeight() == null) {
      computeComplexSize(complex);
    }
    double childWidth = SPECIES_WIDTH;
    double childHeight = SPECIES_WIDTH;
    for (Species species : complex.getElements()) {
      childWidth = Math.max(childWidth, species.getWidth());
      childHeight = Math.max(childHeight, species.getHeight());
    }
    int rowSize = (int) Math.ceil(Math.sqrt(complex.getElements().size()));

    complex.setX(centerX - complex.getWidth() / 2);
    complex.setY(centerY - complex.getHeight() / 2);

    int x = 0;
    int y = 0;

    for (Species species : complex.getElements()) {
      species.setX(complex.getX() + (COMPLEX_PADDING + childWidth) * x);
      species.setY(complex.getY() + (COMPLEX_PADDING + childHeight) * y);
      x++;
      if (x == rowSize) {
        x = 0;
        y++;
      }
      if (species instanceof Complex) {
        modifyComplexLocation((Complex) species, species.getCenterX(), species.getCenterY());
      }
    }

  }

  private void computeComplexSize(Complex complex) {
    double childWidth = SPECIES_WIDTH;
    double childHeight = SPECIES_HEIGHT;
    for (Species species : complex.getElements()) {
      if (species instanceof Complex) {
        computeComplexSize((Complex) species);
      } else {
        species.setWidth(SPECIES_WIDTH);
        species.setHeight(SPECIES_HEIGHT);
      }
      childWidth = Math.max(childWidth, species.getWidth());
      childHeight = Math.max(childHeight, species.getHeight());
    }
    int rowSize = (int) Math.ceil(Math.sqrt(complex.getElements().size()));
    int rowCount = 0;
    if (rowSize == 0) {
      rowSize = 1;
      rowCount = 1;
    } else {
      rowCount = (complex.getElements().size() + rowSize - 1) / rowSize;
    }
    complex.setWidth((childWidth + COMPLEX_PADDING) * rowSize + COMPLEX_PADDING);
    complex.setHeight((childHeight + COMPLEX_PADDING) * rowCount + COMPLEX_PADDING);
  }

}
