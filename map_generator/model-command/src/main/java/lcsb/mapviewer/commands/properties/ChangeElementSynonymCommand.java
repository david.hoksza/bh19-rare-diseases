package lcsb.mapviewer.commands.properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes
 * synonym of the {@link Element Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class ChangeElementSynonymCommand extends ChangeElementPropertyListEntryCommand<String> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(ChangeElementSynonymCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link lcsb.mapviewer.commands.ModelCommand#model}
   * @param alias
   *          {@link ChangeElementPropertyCommand#alias}
   * @param newValue
   *          new {@link Element#synonym} value
   * @param oldValue
   *          old {@link Element#synonym} value
   */
  public ChangeElementSynonymCommand(Model model, Element alias, String newValue, String oldValue) {
    super(model, alias, newValue, oldValue);
  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {
    if (getOldValue().equals(getNewValue())) {
      return;
    }

    // synonyms are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    int index = getAlias().getSynonyms().indexOf(getOldValue());
    if (index < 0) {
      throw new CommandExecutionException("Synonym " + getOldValue() + " doesn't exist.");
    }
    int index2 = getAlias().getSynonyms().indexOf(getNewValue());
    if (index2 >= 0) {
      throw new CommandExecutionException("Synonym " + getNewValue() + " already exist.");
    }
    getAlias().getSynonyms().set(index, getNewValue());

    // synonyms are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());
  }
}
