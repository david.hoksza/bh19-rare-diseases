package lcsb.mapviewer.converter.model.celldesigner.compartment;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CompartmentCollectionXmlParserTest.class,
    CompartmentParserTests.class,
    CompartmentXmlParserTest.class,
})
public class AllCompartmentTests {

}
