package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.geom.Point2D;

import org.junit.*;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.SimpleMolecule;

public class SimpleMoleculeCellDesignerAliasConverterTest extends CellDesignerTestFunctions {

  SimpleMoleculeCellDesignerAliasConverter converter = new SimpleMoleculeCellDesignerAliasConverter(false);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = NotImplementedException.class)
  public void testPathIterator() {
    converter.getBoundPathIterator(null);
  }

  @Test
  public void testGetSbgnPointCoordinates() {
    SimpleMoleculeCellDesignerAliasConverter converter = new SimpleMoleculeCellDesignerAliasConverter(true);

    Model model = new ModelFullIndexed(null);
    SimpleMolecule alias = new SimpleMolecule("id");
    alias.setX(4);
    alias.setY(5);
    alias.setWidth(6);
    alias.setHeight(7);
    alias.setModel(model);

    Point2D point = converter.getPointCoordinates(alias, CellDesignerAnchor.E);
    assertNotNull(point);

  }

  @Test
  public void testGetSbgnAnchorPointCoordinates() {
    SimpleMoleculeCellDesignerAliasConverter converter = new SimpleMoleculeCellDesignerAliasConverter(true);

    Model model = new ModelFullIndexed(null);
    SimpleMolecule alias = new SimpleMolecule("id");
    alias.setX(4);
    alias.setY(5);
    alias.setWidth(6);
    alias.setHeight(7);
    alias.setModel(model);

    Point2D point = converter.getAnchorPointCoordinates(alias, 2);
    assertNotNull(point);

  }

  @Test
  public void testGetAnchorPointCoordinatesForEmptyAlias() {
    SimpleMoleculeCellDesignerAliasConverter converter = new SimpleMoleculeCellDesignerAliasConverter(true);

    Model model = new ModelFullIndexed(null);
    SimpleMolecule alias = new SimpleMolecule("id");
    alias.setX(4);
    alias.setY(5);
    alias.setWidth(0);
    alias.setHeight(0);
    alias.setModel(model);

    Point2D point = converter.getAnchorPointCoordinates(alias, 2);
    assertEquals(0.0, point.distance(new Point2D.Double(4, 5)), Configuration.EPSILON);

  }

}
