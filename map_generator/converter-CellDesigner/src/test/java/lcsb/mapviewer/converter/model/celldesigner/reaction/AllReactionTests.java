package lcsb.mapviewer.converter.model.celldesigner.reaction;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ KineticsXmlParserTest.class,
    ModificationReactionTest.class,
    ReactionCollectionXmlParserTest.class,
    ReactionFromXmlTest.class,
    ReactionLineDataTest.class,
    ReactionParserExceptionTest.class,
    ReactionParserTests.class,
    ReactionToXmlTest.class,
    UnknownModifierClassExceptionTest.class,
})
public class AllReactionTests {

}
