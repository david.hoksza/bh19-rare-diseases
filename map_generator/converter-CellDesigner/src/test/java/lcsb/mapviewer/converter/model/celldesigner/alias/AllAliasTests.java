package lcsb.mapviewer.converter.model.celldesigner.alias;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ AliasCollectionXmlParserTest.class,
    ComplexAliasXmlParserTest.class,
    CompartmentAliasXmlParserTest.class,
    SpeciesAliasXmlParserTest.class,
})
public class AllAliasTests {

}
