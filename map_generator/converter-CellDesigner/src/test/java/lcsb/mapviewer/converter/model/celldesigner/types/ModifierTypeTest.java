package lcsb.mapviewer.converter.model.celldesigner.types;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class ModifierTypeTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() {
    for (ModifierType type : ModifierType.values()) {
      assertNotNull(ModifierType.valueOf(type.toString()));
    }
  }

}
