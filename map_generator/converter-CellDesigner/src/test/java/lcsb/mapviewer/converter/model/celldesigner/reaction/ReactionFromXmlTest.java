package lcsb.mapviewer.converter.model.celldesigner.reaction;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.type.TwoProductReactionInterface;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class ReactionFromXmlTest extends CellDesignerTestFunctions {
  ReactionXmlParser parser;
  Model model = new ModelFullIndexed(null);

  CellDesignerElementCollection elements;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    parser = new ReactionXmlParser(elements, false, false);

    Species alias = new GenericProtein("sa1");
    model.addElement(alias);

    alias = new GenericProtein("sa2");
    model.addElement(alias);

    alias = new GenericProtein("sa3");
    model.addElement(alias);

    alias = new GenericProtein("sa4");
    model.addElement(alias);

    elements.addElement(new CellDesignerGenericProtein("s1"));
    elements.addElement(new CellDesignerGenericProtein("s2"));
    elements.addElement(new CellDesignerGenericProtein("s3"));
    elements.addElement(new CellDesignerGenericProtein("s4"));
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid3() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction3.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid4() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction4.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid5() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction5.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid6() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction6.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid7() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction7.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid8() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction8.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid9() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction9.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid10() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction10.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid11() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction11.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid12() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction12.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid14() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction14.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid15() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction15.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid16() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction16.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid17() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction17.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid19() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction19.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid20() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction20.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid21() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction21.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid23() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction23.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid24() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction24.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid26() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction26.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid28() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction28.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid29() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction29.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid30() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction30.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid31() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction31.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid33() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction33.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid34() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction34.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalid36() throws Exception {
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction36.xml")), model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalidReaction() throws Exception {
    // test situation when createProperTypeReaction returns reaction of
    // unknown type
    ReactionFromXml parser = new ReactionFromXml(null, false, false) {
      Reaction createProperTypeReaction(String type, Reaction result) {
        return result;
      }
    };
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_transport.xml")),
        model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalidReaction2() throws Exception {
    // test situation when createProperTypeReaction returns reaction of
    // unknown type that implements TwoProductReactionInterface

    ReactionFromXml parser = new ReactionFromXml(null, false, false) {
      Reaction createProperTypeReaction(String type, Reaction reaction) {
        Reaction result = Mockito.mock(Reaction.class,
            Mockito.withSettings().extraInterfaces(TwoProductReactionInterface.class));
        Mockito.when(result.getReactants()).thenReturn(reaction.getReactants());
        Mockito.when(result.getProducts()).thenReturn(reaction.getProducts());
        return result;
      }
    };

    parser.getReaction(
        super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_dissociation_with_addition.xml")),
        model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalidReaction3() throws Exception {
    // test situation when createOperatorsForTwoProductReaction encounter
    // reaction with two many base reactants

    ReactionFromXml parser = new ReactionFromXml(null, false, false) {
      Reaction createProperTypeReaction(String type, Reaction result) throws ReactionParserException {
        Reaction r = super.createProperTypeReaction(type, result);
        r.addReactant(new Reactant());
        return r;
      }
    };
    parser.getReaction(
        super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_dissociation_with_addition.xml")),
        model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalidReaction4() throws Exception {
    // test situation when createOperatorsForTwoProductReaction encounter
    // reaction with two many base products

    ReactionFromXml parser = new ReactionFromXml(null, false, false) {
      Reaction createProperTypeReaction(String type, Reaction result) throws ReactionParserException {
        Reaction r = super.createProperTypeReaction(type, result);
        r.addProduct(new Product());
        return r;
      }
    };
    parser.getReaction(
        super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_dissociation_with_addition.xml")),
        model);
  }

  @Test(expected = ReactionParserException.class)
  public void testInvalidReaction7() throws Exception {
    // test situation when createOperatorsForTwoReactantReaction encounter
    // reaction with two many base products

    ReactionFromXml parser = new ReactionFromXml(null, false, false) {
      Reaction createProperTypeReaction(String type, Reaction result) throws ReactionParserException {
        Reaction r = super.createProperTypeReaction(type, result);
        r.addProduct(new Product());
        return r;
      }
    };
    parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_heterodimer.xml")),
        model);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testParseInvalidEditPointsString() throws Exception {
    ReactionFromXml parser = new ReactionFromXml(null, false, false);

    parser.parseEditPointsString("1");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testParseInvalidEditPointsString2() throws Exception {
    ReactionFromXml parser = new ReactionFromXml(null, false, false);

    parser.parseEditPointsString("1,Infinity");
  }

}
