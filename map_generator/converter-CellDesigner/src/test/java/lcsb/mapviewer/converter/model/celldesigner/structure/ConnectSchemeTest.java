package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.*;

import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ConnectScheme;

public class ConnectSchemeTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    String connectIndex = "1";
    Integer integerConnectIndex = 1;
    String connectPolicy = "2";
    Map<String, String> directions = new HashMap<>();
    ConnectScheme cs = new ConnectScheme();
    cs.setConnectPolicy(connectPolicy);
    cs.setConnectIndex(connectIndex);
    cs.setLineDirections(directions);

    assertEquals(integerConnectIndex, cs.getConnectIndex());
    assertEquals(connectPolicy, cs.getConnectPolicy());
    assertEquals(directions, cs.getLineDirections());
  }

}
