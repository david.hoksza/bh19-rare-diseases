package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertEquals;

import org.junit.*;

import lcsb.mapviewer.model.map.reaction.Reaction;

public class ReactionParserExceptionTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    Reaction reaction = new Reaction();
    reaction.setIdReaction("1");
    ReactionParserException exception = new ReactionParserException("text", reaction);
    assertEquals("1", exception.getReactionId());
  }

  @Test
  public void testConstructor2() {
    Reaction reaction = new Reaction();
    reaction.setIdReaction("1");
    ReactionParserException exception = new ReactionParserException("text", reaction, new Exception());
    assertEquals("1", exception.getReactionId());
  }

}
