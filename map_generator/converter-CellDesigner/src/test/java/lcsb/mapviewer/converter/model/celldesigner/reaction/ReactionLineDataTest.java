package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class ReactionLineDataTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetByLineType() {
    assertNull(ReactionLineData.getByLineType(null, null));
  }

  @Test
  public void test() {
    assertNotNull(ReactionLineData.getByCellDesignerString("UNKNOWN_REDUCED_MODULATION"));
  }

  @Test
  public void testAllValues() {
    for (ReactionLineData value : ReactionLineData.values()) {
      assertNotNull(ReactionLineData.valueOf(value.toString()));
    }
  }

  @Test(expected = ReactionParserException.class)
  public void testCreateInvalidReaction() throws Exception {
    ReactionLineData.TRANSPORT.createReaction(Mockito.mock(Reaction.class));
  }

}
