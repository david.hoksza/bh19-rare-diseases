package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertNotNull;

import java.awt.geom.Point2D;

import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.field.ModificationSite;

public class ModificationResidueXmlParserTest extends CellDesignerTestFunctions {

  ModificationResidueXmlParser parser;

  @Before
  public void setUp() throws Exception {
    parser = new ModificationResidueXmlParser(new CellDesignerElementCollection());
  }

  @Test
  public void testGeneModificationResidueToXml() throws Exception {
    Gene protein = new Gene("id");
    protein.setX(10);
    protein.setY(10);
    protein.setWidth(10);
    protein.setHeight(10);
    ModificationSite mr = new ModificationSite();
    mr.setIdModificationResidue("i");
    mr.setName("a");
    mr.setPosition(new Point2D.Double(3.0, 2.0));
    mr.setSpecies(protein);
    String xmlString = parser.toXml(mr);
    assertNotNull(xmlString);
  }

}
