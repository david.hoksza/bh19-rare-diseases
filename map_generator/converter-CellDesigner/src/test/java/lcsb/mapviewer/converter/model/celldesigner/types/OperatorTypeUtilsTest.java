package lcsb.mapviewer.converter.model.celldesigner.types;

import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.*;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;

public class OperatorTypeUtilsTest extends CellDesignerTestFunctions {

  OperatorTypeUtils utils = new OperatorTypeUtils();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetUnknownOperatorTypeForClazz() {
    OperatorType result = utils
        .getOperatorTypeForClazz(Mockito.mock(NodeOperator.class, Mockito.CALLS_REAL_METHODS).getClass());

    assertNull(result);
  }

  @Test
  public void testGetUnkownOperatorTypeForStringType() {
    OperatorType result = utils.getOperatorTypeForStringType("unknown");

    assertNull(result);
  }

  @Test
  public void testGetUnknownStringTypeByOperator() {

    String result = utils.getStringTypeByOperator(Mockito.mock(NodeOperator.class, Mockito.CALLS_REAL_METHODS));

    assertNull(result);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testCreateUnknownModifierForStringType() {
    utils.createModifierForStringType("blabla");
  }

  @Test(expected = InvalidStateException.class)
  public void testCreateInvalidModifierForStringType() throws Exception {
    // artificial implementation of Modifier that is invalid
    class InvalidModifier extends Modifier {
      private static final long serialVersionUID = 1L;

      @SuppressWarnings("unused")
      public InvalidModifier() {
        throw new NotImplementedException();
      }
    }

    // mopdify one of the elements of OperatorType so it will have invalid
    // implementation
    OperatorType typeToModify = OperatorType.AND_OPERATOR_STRING;
    Class<? extends NodeOperator> clazz = typeToModify.getClazz();

    try {
      Field field = typeToModify.getClass().getDeclaredField("clazz");
      field.setAccessible(true);
      field.set(typeToModify, InvalidModifier.class);

      // and check if we catch properly information about problematic
      // implementation
      utils.createModifierForStringType(typeToModify.getStringName());
    } finally {
      // restore correct values for the modified type (if not then other test
      // might fail...)
      Field field = typeToModify.getClass().getDeclaredField("clazz");
      field.setAccessible(true);
      field.set(typeToModify, clazz);
    }
  }

}
