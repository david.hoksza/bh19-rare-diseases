package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.IonChannelProtein;

public class IonTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerIon());
  }

  @Test
  public void testConstructor1() {
    CellDesignerIon degraded = new CellDesignerIon(new CellDesignerSpecies<IonChannelProtein>());
    assertNotNull(degraded);
  }

  @Test
  public void testCopy() {
    CellDesignerIon degraded = new CellDesignerIon().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    CellDesignerIon ion = Mockito.spy(CellDesignerIon.class);
    ion.copy();
  }

}
