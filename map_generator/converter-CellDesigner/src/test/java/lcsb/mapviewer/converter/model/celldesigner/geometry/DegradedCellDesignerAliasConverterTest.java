package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.*;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Species;

public class DegradedCellDesignerAliasConverterTest {

  DegradedCellDesignerAliasConverter converter = new DegradedCellDesignerAliasConverter(false);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAnchorPointCoordinatesForInvalidAlias() {
    Degraded alias = new Degraded("id");
    alias.setWidth(-1);
    Point2D point = converter.getAnchorPointCoordinates(alias, 0);
    assertNotNull(point);
  }

  @Test
  public void testGetAnchorPointCoordinatesForInvalidAlias2() {
    Species alias = new Degraded("id");
    Point2D point = converter.getAnchorPointCoordinates(alias, 0);
    assertNotNull(point);
  }

  @Test
  public void testNotImplementedMethod() {
    try {
      converter.getBoundPathIterator(null);
    } catch (NotImplementedException e) {
      assertTrue(e.getMessage().contains("This class doesn't have bound"));
    }
  }

}
