package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.*;

import java.io.StringReader;
import java.util.HashSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.*;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.*;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.SpeciesState;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.species.*;

public class SpeciesSbmlParserTest extends CellDesignerTestFunctions {
  SpeciesSbmlParser parser;
  String testGeneFile = "testFiles/xmlNodeTestExamples/sbml_gene.xml";
  String testDegradedFile = "testFiles/xmlNodeTestExamples/sbml_degraded.xml";
  String testDrugFile = "testFiles/xmlNodeTestExamples/sbml_drug.xml";
  String testIonFile = "testFiles/xmlNodeTestExamples/sbml_ion.xml";
  String testPhenotypeFile = "testFiles/xmlNodeTestExamples/sbml_phenotype.xml";
  String testProteinFile = "testFiles/xmlNodeTestExamples/sbml_protein.xml";
  String testRnaFile = "testFiles/xmlNodeTestExamples/sbml_rna.xml";
  String testSimpleMoleculeFile = "testFiles/xmlNodeTestExamples/sbml_simple_molecule.xml";
  String testUnknownFile = "testFiles/xmlNodeTestExamples/sbml_unknown.xml";
  CellDesignerElementCollection elements;
  int idCounter = 0;
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(SpeciesSbmlParserTest.class.getName());

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    parser = new SpeciesSbmlParser(elements, new HashSet<>());
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlSpeciesAntisenseRna() throws Exception {
    String xmlString = readFile("testFiles/xmlNodeTestExamples/sbml_antisense_rna.xml");
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerAntisenseRna species = (CellDesignerAntisenseRna) result.getRight();
    assertEquals("s2", species.getElementId());
    assertEquals("s3", species.getName());
    assertEquals(2, species.getInitialAmount(), Configuration.EPSILON);
    assertEquals(Boolean.TRUE, species.hasOnlySubstanceUnits());
    assertEquals(new Integer(0), species.getCharge());
  }

  @Test
  public void testParseXmlSpeciesWithKineticsData() throws Exception {
    String xmlString = readFile("testFiles/kinetics/species_with_kinetics_param.xml");
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerProtein<?> species = (CellDesignerProtein<?>) result.getRight();
    assertEquals(3.7, species.getInitialConcentration(), Configuration.EPSILON);
    assertEquals(SbmlUnitType.MOLE, species.getSubstanceUnit());
  }

  @Test
  public void testSpeciesUnitsToXml() throws Exception {
    GenericProtein protein = new GenericProtein("s1");
    protein.setSubstanceUnits(SbmlUnitType.MOLE);
    String xml = parser.toXml(protein);
    assertTrue("Cannot find substance unit in xml", xml.indexOf("mole") >= 0);
  }

  @Test
  public void testSpeciesConstantToXml() throws Exception {
    GenericProtein protein = new GenericProtein("s1");
    protein.setConstant(true);
    String xml = parser.toXml(protein);
    assertTrue("Cannot find constant in xml", xml.indexOf("constant") >= 0);
  }

  @Test
  public void testSpeciesBoundaryConditionToXml() throws Exception {
    GenericProtein protein = new GenericProtein("s1");
    protein.setBoundaryCondition(true);
    String xml = parser.toXml(protein);
    assertTrue("Cannot find boundary condition in xml", xml.indexOf("boundaryCondition") >= 0);
  }

  @Test
  public void testParseXmlSpeciesWithKineticsData2() throws Exception {
    String xmlString = readFile("testFiles/kinetics/species_with_kinetics_param2.xml");
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerProtein<?> species = (CellDesignerProtein<?>) result.getRight();
    assertEquals(2.5, species.getInitialAmount(), Configuration.EPSILON);
    assertEquals(SbmlUnitType.GRAM, species.getSubstanceUnit());
    assertEquals(Boolean.TRUE, species.hasOnlySubstanceUnits());
    assertEquals("Boundary condition wasn't parsed", Boolean.TRUE, species.isBoundaryCondition());
    assertEquals("Constant property wasn't parsed", Boolean.TRUE, species.isConstant());
    assertEquals(new Integer(0), species.getCharge());
  }

  @Test
  public void testToXmlAntisenseRna() throws Exception {
    String xmlString = readFile("testFiles/xmlNodeTestExamples/sbml_antisense_rna.xml");
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerAntisenseRna species = (CellDesignerAntisenseRna) result.getRight();

    String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    CellDesignerAntisenseRna species2 = (CellDesignerAntisenseRna) result2.getRight();

    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getPositionToCompartment(), species2.getPositionToCompartment());
  }

  @Test
  public void testParseXmlSpeciesComplex() throws Exception {
    String xmlString = readFile("testFiles/xmlNodeTestExamples/sbml_complex.xml");
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerComplexSpecies species = (CellDesignerComplexSpecies) result.getRight();
    assertNotNull(species);
    assertEquals("s6549", species.getElementId());
    assertEquals("LC3-II", species.getName());
    assertEquals(0.0, species.getInitialAmount(), Configuration.EPSILON);
  }

  @Test
  public void testToXmlComplex() throws Exception {
    String xmlString = readFile("testFiles/xmlNodeTestExamples/sbml_complex.xml");
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerComplexSpecies species = (CellDesignerComplexSpecies) result.getRight();

    String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    CellDesignerComplexSpecies species2 = (CellDesignerComplexSpecies) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
  }

  @Test
  public void testParseXmlSpeciesDegraded() throws Exception {
    String xmlString = readFile(testDegradedFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerDegraded species = (CellDesignerDegraded) result.getRight();
    assertNotNull(species);
    assertEquals("s1275", species.getElementId());
    assertEquals("s1275", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
  }

  @Test
  public void testToXmlDegraded() throws Exception {
    String xmlString = readFile(testDegradedFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerDegraded species = (CellDesignerDegraded) result.getRight();

    String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    CellDesignerDegraded species2 = (CellDesignerDegraded) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
  }

  @Test
  public void testParseXmlSpeciesDrug() throws Exception {
    String xmlString = readFile(testDrugFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerDrug species = (CellDesignerDrug) result.getRight();
    assertEquals("s6104", species.getElementId());
    assertEquals("geldanamycin", species.getName());
    assertEquals(new Integer(0), species.getCharge());
  }

  @Test
  public void testToXmlDrug() throws Exception {
    String xmlString = readFile(testDrugFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerDrug species = (CellDesignerDrug) result.getRight();

    String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    CellDesignerDrug species2 = (CellDesignerDrug) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
  }

  @Test
  public void testParseXmlSpeciesGene() throws Exception {
    String xmlString = readFile(testGeneFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerGene species = (CellDesignerGene) result.getRight();

    assertEquals("s5916", species.getElementId());
    assertEquals("Ptgr1", species.getName());
    assertEquals(new Integer(0), species.getCharge());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
  }

  @Test
  public void testParseXmlSpeciesGeneWithModelUpdate() throws Exception {
    CellDesignerSpecies<?> gene = new CellDesignerGene();
    gene.setElementId("s5916");
    InternalModelSpeciesData modelData = new InternalModelSpeciesData();
    modelData.updateSpecies(gene, "");

    SpeciesSbmlParser complexParser = new SpeciesSbmlParser(elements, new HashSet<>());
    CellDesignerGene oldGene = new CellDesignerGene();
    oldGene.setElementId("s5916");
    oldGene.setName("Ptgr1");
    modelData.updateSpecies(oldGene, "gn95");

    String xmlString = readFile(testGeneFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = complexParser.parseXmlElement(xmlString);
    CellDesignerGene species = (CellDesignerGene) result.getRight();
    modelData.updateSpecies(species, result.getLeft());

    assertEquals("s5916", species.getElementId());
    assertEquals("Ptgr1", species.getName());
    assertTrue(species.getNotes().contains("prostaglandin reductase 1"));
  }

  @Test
  public void testToXmlGene() throws Exception {
    String xmlString = readFile(testGeneFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerGene species = (CellDesignerGene) result.getRight();

    String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    CellDesignerGene species2 = (CellDesignerGene) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertTrue(species2.getNotes().trim().contains(species.getNotes().trim()));
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesIon() throws Exception {
    String xmlString = readFile(testIonFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerIon species = (CellDesignerIon) result.getRight();

    assertEquals("s6029", species.getElementId());
    assertEquals("Pi", species.getName());
    assertEquals(new Integer(0), species.getCharge());
    assertEquals(0, species.getInitialConcentration(), Configuration.EPSILON);
  }

  @Test
  public void testToXmlIon() throws Exception {
    String xmlString = readFile(testIonFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerIon species = (CellDesignerIon) result.getRight();

    String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser.parseXmlElement(xmlString);
    CellDesignerIon species2 = (CellDesignerIon) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getElementId(), species2.getElementId());
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertEquals(species.getNotes().trim(), species2.getNotes().trim());
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesPhenotype() throws Exception {
    String xmlString = readFile(testPhenotypeFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerPhenotype species = (CellDesignerPhenotype) result.getRight();

    assertEquals("s5462", species.getElementId());
    assertEquals("Neuronal damage and death", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
  }

  @Test
  public void testToXmlPhenotype() throws Exception {
    String xmlString = readFile(testPhenotypeFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerPhenotype species = (CellDesignerPhenotype) result.getRight();

    String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    CellDesignerPhenotype species2 = (CellDesignerPhenotype) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertEquals(species.getNotes(), species2.getNotes());
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesProtein() throws Exception {
    String xmlString = readFile(testProteinFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerProtein<?> species = (CellDesignerProtein<?>) result.getRight();

    assertEquals("s5456", species.getElementId());
    assertEquals("PTPRC", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
    assertEquals(new Integer(0), species.getCharge());
    assertTrue(species.getNotes().contains("protein tyrosine phosphatase, receptor type, C"));
    assertNull(species.getStructuralState());
  }

  @Test
  public void testToXmlProtein() throws Exception {
    String xmlString = readFile(testProteinFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerGenericProtein species = new CellDesignerGenericProtein(result.getRight());

    String transformedXml = parser.toXml(species.createModelElement("" + idCounter++));
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement("" + idCounter++)));
    CellDesignerProtein<?> species2 = (CellDesignerProtein<?>) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertTrue(species2.getNotes().contains(species.getNotes()));
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesRna() throws Exception {
    String xmlString = readFile(testRnaFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerRna species = (CellDesignerRna) result.getRight();

    assertEquals("s5914", species.getElementId());
    assertEquals("Fmo3", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
    assertEquals(new Integer(0), species.getCharge());
    assertTrue(species.getNotes().contains("Dimethylaniline monooxygenase [N-oxide-forming] 3"));
  }

  @Test
  public void testToXmlRna() throws Exception {
    String xmlString = readFile(testRnaFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerRna species = (CellDesignerRna) result.getRight();

    String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    CellDesignerRna species2 = (CellDesignerRna) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertTrue(species2.getNotes().trim().contains(species.getNotes().trim()));
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesSimpleMolecule() throws Exception {
    String xmlString = readFile(testSimpleMoleculeFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerSimpleMolecule species = (CellDesignerSimpleMolecule) result.getRight();
    assertEquals("s5463", species.getElementId());
    assertEquals("Peroxides", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
  }

  @Test
  public void testToXmlSimpleMolecule() throws Exception {
    String xmlString = readFile(testSimpleMoleculeFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerSimpleMolecule species = (CellDesignerSimpleMolecule) result.getRight();

    String transformedXml = parser.toXml(species.createModelElement("" + idCounter++));
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement("" + idCounter++)));
    CellDesignerSimpleMolecule species2 = (CellDesignerSimpleMolecule) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertEquals(species.getNotes(), species2.getNotes());
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesUnknown() throws Exception {
    String xmlString = readFile(testUnknownFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerUnknown species = (CellDesignerUnknown) result.getRight();
    assertEquals("s1356", species.getElementId());
    assertEquals("unidentified caspase acting on Occludin", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
    assertEquals(new Integer(0), species.getCharge());
  }

  @Test
  public void testToXmlUnknown() throws Exception {
    String xmlString = readFile(testUnknownFile);
    Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    CellDesignerUnknown species = (CellDesignerUnknown) result.getRight();

    String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    CellDesignerUnknown species2 = (CellDesignerUnknown) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertEquals(species.getNotes(), species2.getNotes());
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid2() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein2.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid3() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein3.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid4() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein4.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid5() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein5.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid6() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein6.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid7() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein7.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid8() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein8.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid9() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein9.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid10() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein10.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid11() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein11.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid12() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein12.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid13() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein13.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid14() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_sbml_protein14.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test
  public void testToXmlWithDefaultCompartment() throws Exception {
    CellDesignerGenericProtein species = new CellDesignerGenericProtein();
    String xml = parser.toXml(species.createModelElement("EL_ID"));
    assertTrue(xml.contains("EL_ID"));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidSpeciesIdentityToXml() throws Exception {
    Species species = Mockito.mock(Species.class);
    parser.speciesIdentityToXml(species);
  }

  @Test
  public void testSpeciesIdentityToXml() throws Exception {
    GenericProtein species = new GenericProtein("xx");
    species.setHypothetical(true);
    String xml = parser.speciesIdentityToXml(species);
    assertTrue(xml.contains("<celldesigner:hypothetical>true</celldesigner:hypothetical>"));
  }

  @Test
  public void testSpeciesStateToXml() throws Exception {
    SpeciesState state = new SpeciesState();
    state.setHomodimer(2);
    state.setStructuralState("xxxState");
    String xml = parser.speciesStateToXml(state);
    assertTrue(xml.contains("xxxState"));
    assertTrue(xml.contains("celldesigner:homodimer"));
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies() throws Exception {
    SpeciesState state = new SpeciesState();
    state.addModificationResidue(new CellDesignerModificationResidue());
    CellDesignerComplexSpecies species = new CellDesignerComplexSpecies();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies2() throws Exception {
    SpeciesState state = new SpeciesState();
    state.setStructuralState("state");
    CellDesignerGene species = new CellDesignerGene();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies3() throws Exception {
    SpeciesState state = new SpeciesState();
    state.setStructuralState("state");
    CellDesignerRna species = new CellDesignerRna();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies4() throws Exception {
    SpeciesState state = new SpeciesState();
    state.addModificationResidue(new CellDesignerModificationResidue());
    CellDesignerSimpleMolecule species = new CellDesignerSimpleMolecule();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies5() throws Exception {
    SpeciesState state = new SpeciesState();
    state.setStructuralState("state");
    CellDesignerSimpleMolecule species = new CellDesignerSimpleMolecule();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies6() throws Exception {
    SpeciesState state = new SpeciesState();
    state.setStructuralState("state");
    CellDesignerAntisenseRna species = new CellDesignerAntisenseRna();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies7() throws Exception {
    SpeciesState state = new SpeciesState();
    state.setStructuralState("state");
    CellDesignerSpecies<?> species = new CellDesignerSpecies<Gene>();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies8() throws Exception {
    SpeciesState state = new SpeciesState();
    state.addModificationResidue(new CellDesignerModificationResidue());
    CellDesignerSpecies<?> species = new CellDesignerSpecies<Gene>();

    parser.processStateDataInSpecies(species, state);
  }

  @Test
  public void testProcessAntisenseRnaStateDataInSpecies() throws Exception {
    SpeciesState state = new SpeciesState();
    state.addModificationResidue(new CellDesignerModificationResidue());
    CellDesignerAntisenseRna species = new CellDesignerAntisenseRna();

    parser.processStateDataInSpecies(species, state);

    assertEquals(1, species.getRegions().size());
  }

}
