package lcsb.mapviewer.converter.model.celldesigner.geometry;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.Species;

public class CellDesignerAliasConverterTest extends CellDesignerTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg() {
    new CellDesignerAliasConverter(null, false);
  }

  @Test(expected = NotImplementedException.class)
  public void testConstructorWithInvalidArg2() {
    Species alias = Mockito.mock(Species.class);
    new CellDesignerAliasConverter(alias, false);
  }

}
