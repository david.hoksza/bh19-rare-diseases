package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.model.map.model.Model;

@RunWith(Parameterized.class)
public class SbmlValidationTests extends CellDesignerTestFunctions {
  Logger logger = LogManager.getLogger();

  String filename;

  public SbmlValidationTests(String filename) {
    this.filename = filename;
  }

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() throws IOException {
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[] { "testFiles/empty.xml" });
    result.add(new Object[] { "testFiles/parameter.xml" });
    result.add(new Object[] { "testFiles/model_with_annotations.xml" });
    result.add(new Object[] { "testFiles/reactions/modifier_with_operator.xml" });
    return result;
  }

  @Test
  public void testIsValidSbml() throws Exception {
    Model model = super.getModelForFile(filename);
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    String xml = parser.model2String(model);

    CloseableHttpClient httpClient = HttpClients.createDefault();
    HttpPost uploadFile = new HttpPost("http://sbml.org/validator/");
    MultipartEntityBuilder builder = MultipartEntityBuilder.create();
    builder.addTextBody("file", xml, ContentType.TEXT_PLAIN);
    builder.addBinaryBody(
        "file",
        new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)),
        ContentType.APPLICATION_OCTET_STREAM,
        filename);
    builder.addTextBody("output", "xml", ContentType.TEXT_PLAIN);
    builder.addTextBody("offcheck", "u,r", ContentType.TEXT_PLAIN);

    HttpEntity multipart = builder.build();
    uploadFile.setEntity(multipart);
    CloseableHttpResponse response = httpClient.execute(uploadFile);
    String responseXml = EntityUtils.toString(response.getEntity());
    Document document = XmlParser.getXmlDocumentFromString(responseXml);
    List<Node> problems = XmlParser.getAllNotNecessirellyDirectChild("problem", document);
    if (problems.size() > 0) {
      logger.debug(responseXml);
    }
    assertEquals("SBML is invalid", 0, problems.size());
  }

}
