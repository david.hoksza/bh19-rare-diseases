package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.*;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.model.graphics.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.type.*;
import lcsb.mapviewer.model.map.species.*;

public class ReactionParserTests extends CellDesignerTestFunctions {

  ReactionXmlParser parser;
  CellDesignerElementCollection elements;
  private Logger logger = LogManager.getLogger();

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();

    parser = new ReactionXmlParser(elements, false, false);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testColorReaction() throws Exception {
    Model model = getModelForFile("testFiles/colorfull_reaction.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    PolylineData line = reaction.getReactants().get(0).getLine();
    assertFalse(
        "000".equals(line.getColor().getRed() + "" + line.getColor().getGreen() + "" + line.getColor().getBlue()));
    line = reaction.getProducts().get(0).getLine();
    assertFalse(
        "000".equals(line.getColor().getRed() + "" + line.getColor().getGreen() + "" + line.getColor().getBlue()));
    line = reaction.getModifiers().get(0).getLine();
    assertFalse(
        "000".equals(line.getColor().getRed() + "" + line.getColor().getGreen() + "" + line.getColor().getBlue()));
    line = reaction.getOperators().get(0).getLine();
    assertFalse(
        "000".equals(line.getColor().getRed() + "" + line.getColor().getGreen() + "" + line.getColor().getBlue()));
  }

  @Test
  public void testColorReactionWithModifierOperator() throws Exception {
    Model model = getModelForFile("testFiles/colorfull_reaction_2.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    Modifier m1 = reaction.getModifiers().get(0);
    Modifier m2 = reaction.getModifiers().get(1);
    NodeOperator o = reaction.getOperators().get(0);

    Set<Color> colors = new HashSet<>();
    colors.add(m1.getLine().getColor());
    colors.add(m2.getLine().getColor());
    colors.add(o.getLine().getColor());
    assertEquals("Each part has different color", 3, colors.size());

    assertFalse("000".equals(getStringColor(m1.getLine())));
    assertFalse("000".equals(getStringColor(m2.getLine())));
    assertFalse("Operator line not parsed properly", "000".equals(getStringColor(o.getLine())));
  }

  private String getStringColor(PolylineData line) {
    return line.getColor().getRed() + "" + line.getColor().getGreen() + "" + line.getColor().getBlue();
  }

  @Test
  public void testMissingLines() throws Exception {
    Model model = getModelForFile("testFiles/problematic/pd_map_with_problematic_reaction_line.xml");
    assertTrue(model.getElementByElementId("sa5003") instanceof GenericProtein);
    Set<Reaction> list = model.getReactions();
    for (Reaction reaction : list) {
      // reaction re1607 in this model was problematic, but in fact the
      // problem
      // can be in any reaction line
      // if (reaction.getId().equals("re1607")) {
      List<Line2D> lines = reaction.getLines();
      for (Line2D line2d : lines) {
        assertFalse(Double.isNaN(line2d.getX1()));
        assertFalse(Double.isNaN(line2d.getX2()));
        assertFalse(Double.isNaN(line2d.getY1()));
        assertFalse(Double.isNaN(line2d.getY2()));
      }
      // }
    }
  }

  @Test
  public void testTransitionReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transition.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof StateTransitionReaction);
    assertEquals(1, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);

    assertTrue(reaction.isReversible());
    assertEquals(ArrowType.FULL, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(3, reactant.getLine().getLines().size());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(2, product.getLine().getLines().size());

    assertTrue(reactant.getLine().getEndPoint().distance(product.getLine().getPoints().get(0)) < 20);
    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTransition2() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transitionWithAdditionalNodes.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(2, reaction.getOperators().size());
    for (Reactant reactant : reaction.getReactants()) {
      assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
      assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());

      assertTrue(
          reactant.getLine().getEndPoint().distance(reaction.getReactants().get(0).getLine().getEndPoint()) < 1e-6);
    }

    for (Product product : reaction.getProducts()) {
      assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
      assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());

      assertTrue(
          product.getLine().getBeginPoint().distance(reaction.getProducts().get(0).getLine().getBeginPoint()) < 1e-6);
    }
    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTransiotionOmitted() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transition_omitted.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(0, reaction.getOperators().size());
    assertEquals(KnownTransitionOmittedReaction.class, reaction.getClass());
    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testUnknownTransition() throws Exception {
    Model model = getModelForFile("testFiles/reactions/unknown_transition.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction.getReactionRect());

  }

  @Test
  public void testTranscription() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transcription.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof TranscriptionReaction);
    assertEquals(1, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(LineType.DASH_DOT_DOT, reactant.getLine().getType());
    assertEquals(1, reactant.getLine().getLines().size());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(1, product.getLine().getLines().size());

    assertTrue(reactant.getLine().getEndPoint().distance(product.getLine().getPoints().get(0)) < 20);
    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTranscription2() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transcription_with_additions.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof TranscriptionReaction);
    assertEquals(2, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(LineType.DASH_DOT_DOT, reactant.getLine().getType());
    assertEquals(1, reactant.getLine().getLines().size());
    NodeOperator operator = null;
    for (NodeOperator operator2 : reaction.getOperators()) {
      if (operator2 instanceof SplitOperator)
        operator = operator2;
    }
    assertEquals(LineType.DASH_DOT_DOT, operator.getLine().getType());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(1, product.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());

    super.testXmlSerialization(model);
  }

  @Test
  public void testTranslation() throws Exception {
    Model model = getModelForFile("testFiles/reactions/translation.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof TranslationReaction);
    assertEquals(2, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(LineType.DASH_DOT, reactant.getLine().getType());
    assertEquals(1, reactant.getLine().getLines().size());
    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.DASH_DOT, operator.getLine().getType());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(1, product.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTransport() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transport.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof TransportReaction);
    assertEquals(1, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(1, reactant.getLine().getLines().size());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL_CROSSBAR, product.getLine().getEndAtd().getArrowType());
    assertEquals(1, product.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testHeterodimer() throws Exception {
    Model model = getModelForFile("testFiles/reactions/heterodimer.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof HeterodimerAssociationReaction);
    assertEquals(2, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(2, reactant.getLine().getLines().size());
    reactant = reaction.getReactants().get(1);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(3, reactant.getLine().getLines().size());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(3, product.getLine().getLines().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.SOLID, operator.getLine().getType());
    assertEquals(7, operator.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testHeterodimerWithAdditions() throws Exception {
    Model model = getModelForFile("testFiles/reactions/heterodimerWithAdditions.xml");

    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof HeterodimerAssociationReaction);
    assertEquals(3, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(2, reactant.getLine().getLines().size());
    reactant = reaction.getReactants().get(1);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(3, reactant.getLine().getLines().size());

    for (Product product : reaction.getProducts()) {
      assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
      assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    }

    assertEquals(3, reaction.getOperators().size());
    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.SOLID, operator.getLine().getType());
    assertEquals(7, operator.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testDissociation() throws Exception {
    Model model = getModelForFile("testFiles/reactions/dissociation.xml");
    assertEquals(2, model.getReactions().size());
    Reaction reaction = null;
    for (Reaction reaction2 : model.getReactions()) {
      if (reaction2.getIdReaction().equals("re1"))
        reaction = reaction2;
    }
    assertTrue(reaction instanceof DissociationReaction);
    assertEquals(1, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(2, reactant.getLine().getLines().size());

    assertEquals(2, reaction.getProducts().size());
    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(5, product.getLine().getLines().size());
    product = reaction.getProducts().get(1);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(4, product.getLine().getLines().size());

    assertEquals(1, reaction.getOperators().size());
    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.SOLID, operator.getLine().getType());
    assertEquals(5, operator.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testDissociationWithAddition() throws Exception {
    Model model = getModelForFile("testFiles/reactions/dissociationWithAddition.xml");
    assertEquals(2, model.getReactions().size());
    Reaction reaction = null;
    for (Reaction reaction2 : model.getReactions()) {
      if (reaction2.getIdReaction().equals("re1"))
        reaction = reaction2;
    }
    assertTrue(reaction instanceof DissociationReaction);
    assertEquals(2, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(2, reactant.getLine().getLines().size());

    assertEquals(3, reaction.getProducts().size());
    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(5, product.getLine().getLines().size());
    product = reaction.getProducts().get(1);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(4, product.getLine().getLines().size());

    assertEquals(3, reaction.getOperators().size());
    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.SOLID, operator.getLine().getType());
    assertEquals(5, operator.getLine().getLines().size());
    assertEquals(DissociationOperator.class, operator.getClass());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTruncation() throws Exception {
    Model model = getModelForFile("testFiles/reactions/truncation.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof TruncationReaction);
    assertEquals(1, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(2, reactant.getLine().getLines().size());

    assertEquals(2, reaction.getProducts().size());
    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(3, product.getLine().getLines().size());
    product = reaction.getProducts().get(1);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(4, product.getLine().getLines().size());

    assertEquals(1, reaction.getOperators().size());
    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.SOLID, operator.getLine().getType());
    assertEquals(1, operator.getLine().getLines().size());
    assertEquals(TruncationOperator.class, operator.getClass());
    assertEquals(reaction.getReactionRect(), ReactionRect.RECT_BOLT);

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTruncationWithModifier() throws Exception {
    Model model = getModelForFile("testFiles/reactions/truncationWithModifier.xml");
    Reaction reaction = model.getReactions().iterator().next();
    Modifier m = reaction.getModifiers().get(0);
    assertEquals(ArrowType.CIRCLE, m.getLine().getEndAtd().getArrowType());

    m = reaction.getModifiers().get(1);
    assertEquals(ArrowType.CROSSBAR, m.getLine().getEndAtd().getArrowType());
  }

  @Test
  public void testComplexModifier1() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexModifier1.xml");

    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(1, reaction.getOperators().size());
    assertEquals(2, reaction.getModifiers().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertTrue(operator.isModifierOperator());
    assertEquals(AndOperator.class, operator.getClass());

  }

  @Test
  public void testComplexModifier2() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexModifier2.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(1, reaction.getOperators().size());
    assertEquals(2, reaction.getModifiers().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertTrue(operator.isModifierOperator());
    assertEquals(OrOperator.class, operator.getClass());
  }

  @Test
  public void testComplexModifier3() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexModifier3.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(1, reaction.getOperators().size());
    assertEquals(2, reaction.getModifiers().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertTrue(operator.isModifierOperator());
    assertEquals(NandOperator.class, operator.getClass());
  }

  @Test
  public void testComplexModifier4() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexModifier4.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(1, reaction.getOperators().size());
    assertEquals(2, reaction.getModifiers().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertTrue(operator.isModifierOperator());
    assertEquals(UnknownOperator.class, operator.getClass());
  }

  @Test
  public void testComplexReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexReaction.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(0, reaction.getModifiers().size());
    assertEquals(2, reaction.getOperators().size());
    assertEquals(2, reaction.getProducts().size());
    assertEquals(2, reaction.getReactants().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(AndOperator.class, operator.getClass());
  }

  @Test
  public void testComplexModifiers5() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexModifier5.xml");

    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(4, reaction.getModifiers().size());
    assertEquals(1, reaction.getOperators().size());
    assertEquals(2, reaction.getOperators().iterator().next().getInputs().size());
  }

  @Test
  public void testProblematicAnchors() throws Exception {
    Model model = getModelForFile("testFiles/reactions/problemWithAnchors2.xml");

    Reaction reaction1 = null;
    Reaction reaction2 = null;
    for (Reaction reaction : model.getReactions()) {
      if (reaction.getIdReaction().equals("re2"))
        reaction1 = reaction;
      if (reaction.getIdReaction().equals("re3"))
        reaction2 = reaction;
    }
    Reactant reactant = reaction1.getReactants().get(0);
    Element alias1 = reaction1.getReactants().get(0).getElement();
    Element alias2 = reaction1.getProducts().get(0).getElement();
    Product product = reaction1.getProducts().get(0);
    Point2D point = new Point2D.Double(alias1.getX() + alias1.getWidth() / 2, alias1.getY() + alias1.getHeight());
    Point2D point2 = new Point2D.Double(alias2.getX(), alias2.getY() + alias2.getHeight() / 2);
    assertTrue(point.distance(reactant.getLine().getPoints().get(0)) < 1);
    assertTrue(point2.distance(product.getLine().getEndPoint()) < 1);

    reactant = reaction2.getReactants().get(0);
    alias1 = reaction2.getReactants().get(0).getElement();
    alias2 = reaction2.getProducts().get(0).getElement();
    product = reaction2.getProducts().get(0);
    point = new Point2D.Double(alias1.getX() + alias1.getWidth(), alias1.getY() + alias1.getHeight() / 2);
    point2 = new Point2D.Double(alias2.getX(), alias2.getY() + alias2.getHeight() / 2);
    assertTrue(point.distance(reactant.getLine().getPoints().get(0)) < 1);
    assertTrue(point2.distance(product.getLine().getEndPoint()) < 1);
  }

  @Test
  public void testProblematicAnchors3() throws Exception {
    Model model = getModelForFile("testFiles/reactions/problemWithAnchors3.xml");
    Reaction reaction = null;
    for (Reaction reaction2 : model.getReactions()) {
      if (reaction2.getIdReaction().equals("re3"))
        reaction = reaction2;
    }
    Point2D point = new Point2D.Double(164.85583789974368, 86.060142902597);
    Point2D point2 = new Point2D.Double(397.06477630152193, 284.99999999999994);

    assertTrue(point.distance(reaction.getModifiers().get(0).getLine().getPoints().get(0)) < 1);
    assertTrue(point2.distance(reaction.getModifiers().get(1).getLine().getPoints().get(0)) < 1);
  }

  @Test
  public void testPositiveInfluence() throws Exception {
    Model model = getModelForFile("testFiles/reactions/positive_influence.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertNull(reaction.getReactionRect());
  }

  @Test
  public void testProblematicAnchors2() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexReactionWithModifier.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(reaction.getProducts().get(0).getLine().length(), reaction.getReactants().get(0).getLine().length(),
        1e-6);
  }

  @Test
  public void testProblematicAnchorsWithTwoReactantReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/centeredAnchorInModifier.xml");
    Reaction reaction = model.getReactions().iterator().next();
    Reactant r = null;
    for (Reactant reactant : reaction.getReactants()) {
      if (reactant.getElement().getName().equals("s3")) {
        r = reactant;
      }
    }
    assertNotNull(r);
    // I think there is still a bug because it should work with smaller delta
    // :)
    assertEquals(r.getLine().getPoints().get(1).getX(), r.getLine().getPoints().get(2).getX(), 1);
  }

  @Test
  public void testReactionWithModifiers() throws Exception {
    Model model = getModelForFile("testFiles/reactions/reactionWithModifiers.xml");
    Reaction reaction = model.getReactions().iterator().next();
    List<Modifier> modifiers = reaction.getModifiers();
    Modifier sa3 = null;
    Modifier sa4 = null;
    for (Modifier modifier : modifiers) {
      if (modifier.getElement().getElementId().equals("sa3")) {
        sa3 = modifier;
      }
      if (modifier.getElement().getElementId().equals("sa4")) {
        sa4 = modifier;
      }
    }
    assertEquals(sa3.getLine().getPoints().get(0).distance(new Point2D.Double(101.9591678008944, 68.0)), 0, EPSILON);
    assertEquals(sa3.getLine().getPoints().get(1).distance(new Point2D.Double(101.86750788643532, 112.89589905362774)),
        0, EPSILON);
    assertEquals(sa3.getLine().getPoints().get(2).distance(new Point2D.Double(190.66666666666666, 117.66666666666667)),
        0, EPSILON);

    assertEquals(sa4.getLine().getPoints().get(0).distance(new Point2D.Double(267.7075561388218, 54.00000000000001)), 0,
        EPSILON);
    assertEquals(sa4.getLine().getPoints().get(1).distance(new Point2D.Double(298.3735877669656, 71.67109819284718)), 0,
        EPSILON);
    assertEquals(sa4.getLine().getPoints().get(2).distance(new Point2D.Double(190.66666666666666, 117.66666666666667)),
        0, EPSILON);

  }

  @Test
  public void testReactionOperatorsWithOperators() throws Exception {
    Model model = getModelForFile("testFiles/reactions/reactionWithOperators.xml");
    Reaction reaction = model.getReactions().iterator().next();
    NodeOperator operator1 = reaction.getOperators().get(0);
    NodeOperator operator2 = reaction.getOperators().get(1);
    NodeOperator operator3 = reaction.getOperators().get(2);

    Product product1 = reaction.getProducts().get(0);
    Product product2 = reaction.getProducts().get(1);

    assertEquals(0, operator1.getLine().getPoints().get(0).distance(new Point2D.Double(287.0, 242.00000000000009)),
        EPSILON);
    assertEquals(0,
        operator1.getLine().getPoints().get(1).distance(new Point2D.Double(287.97349260719136, 204.40292958328325)),
        EPSILON);
    assertEquals(0, operator1.getLine().getEndPoint().distance(operator2.getLine().getBeginPoint()), EPSILON);

    assertEquals(0,
        operator2.getLine().getPoints().get(0).distance(new Point2D.Double(359.1840955643148, 203.94471254019686)),
        EPSILON);
    assertEquals(0,
        operator2.getLine().getPoints().get(1).distance(new Point2D.Double(372.9868291110932, 203.8558964441427)),
        EPSILON);

    assertEquals(0,
        operator3.getLine().getPoints().get(0).distance(new Point2D.Double(394.78939704287654, 203.71560401865366)),
        EPSILON);
    assertEquals(0,
        operator3.getLine().getPoints().get(1).distance(new Point2D.Double(380.9866634960982, 203.80442011470782)),
        EPSILON);

    assertEquals(0,
        product1.getLine().getPoints().get(0).distance(new Point2D.Double(394.78939704287654, 203.71560401865366)),
        EPSILON);
    assertEquals(0, product1.getLine().getPoints().get(1).distance(new Point2D.Double(466.0, 203.25738697556727)),
        EPSILON);

    assertEquals(0,
        product2.getLine().getPoints().get(0).distance(new Point2D.Double(394.78939704287654, 203.71560401865366)),
        EPSILON);
    assertEquals(0,
        product2.getLine().getPoints().get(1).distance(new Point2D.Double(452.96894321929705, 107.00000000000001)),
        EPSILON);
  }

  @Test
  public void testReactionProductsWithOperators() throws Exception {
    Model model = getModelForFile("testFiles/reactions/reactionWithOperators.xml");
    Reaction reaction = model.getReactions().iterator().next();

    Product product1 = reaction.getProducts().get(0);
    Product product2 = reaction.getProducts().get(1);

    assertEquals(
        product1.getLine().getPoints().get(0).distance(new Point2D.Double(394.78939704287654, 203.71560401865366)), 0,
        EPSILON);
    assertEquals(product1.getLine().getPoints().get(1).distance(new Point2D.Double(466.0, 203.25738697556727)), 0,
        EPSILON);

    assertEquals(
        product2.getLine().getPoints().get(0).distance(new Point2D.Double(394.78939704287654, 203.71560401865366)), 0,
        EPSILON);
    assertEquals(
        product2.getLine().getPoints().get(1).distance(new Point2D.Double(452.96894321929705, 107.00000000000001)), 0,
        EPSILON);
  }

  @Test
  public void testReactionReactantsWithOperators() throws Exception {
    Model model = getModelForFile("testFiles/reactions/reactionWithOperators.xml");
    Reaction reaction = model.getReactions().iterator().next();

    Reactant reactant1 = reaction.getReactants().get(0);
    Reactant reactant2 = reaction.getReactants().get(1);
    Reactant reactant3 = reaction.getReactants().get(2);

    assertEquals(
        reactant1.getLine().getPoints().get(0).distance(new Point2D.Double(112.53618421052632, 167.46381578947367)),
        0, EPSILON);
    assertEquals(reactant1.getLine().getPoints().get(1).distance(new Point2D.Double(287.0, 242.00000000000009)), 0,
        EPSILON);

    assertEquals(reactant2.getLine().getPoints().get(0).distance(new Point2D.Double(121.0, 242.0)), 0, EPSILON);
    assertEquals(reactant2.getLine().getPoints().get(1).distance(new Point2D.Double(287.0, 242.00000000000009)), 0,
        EPSILON);

    assertEquals(
        reactant3.getLine().getPoints().get(0).distance(new Point2D.Double(116.65392247520951, 72.34607752479052)), 0,
        EPSILON);
    assertEquals(
        reactant3.getLine().getPoints().get(1).distance(new Point2D.Double(359.1840955643148, 203.94471254019686)), 0,
        EPSILON);
  }

  @Test
  public void testTransitionReactionToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transition.xml");

    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getClass(), reaction2.getClass());
    assertEquals(reaction1.getReactants().size(), reaction2.getReactants().size());
    Reactant reactant1 = reaction1.getReactants().get(0);
    Reactant reactant2 = reaction2.getReactants().get(0);
    assertEquals(reactant1.getLine().getBeginAtd().getArrowType(), reactant2.getLine().getBeginAtd().getArrowType());
    assertEquals(reactant1.getLine().getEndAtd().getArrowType(), reactant2.getLine().getEndAtd().getArrowType());
    assertEquals(reactant1.getLine().getLines().size(), reactant2.getLine().getLines().size());

    Product product1 = reaction1.getProducts().get(0);
    Product product2 = reaction2.getProducts().get(0);
    assertEquals(product1.getLine().getBeginAtd().getArrowType(), product2.getLine().getBeginAtd().getArrowType());
    assertEquals(product1.getLine().getEndAtd().getArrowType(), product2.getLine().getEndAtd().getArrowType());
    assertEquals(product1.getLine().getLines().size(), product2.getLine().getLines().size());

    assertEquals(reactant1.getLine().getEndPoint().getX(), reactant2.getLine().getEndPoint().getX(), EPSILON);
    assertEquals(reactant1.getLine().getEndPoint().getY(), reactant2.getLine().getEndPoint().getY(), EPSILON);

    assertEquals(reaction1.getReactionRect(), reaction2.getReactionRect());

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }

    assertEquals(product1.getLine().getColor(), product2.getLine().getColor());
    assertEquals(product1.getLine().getWidth(), product2.getLine().getWidth(), 1e-6);
    assertEquals(reaction1.getLine().getColor(), product2.getLine().getColor());
  }

  @Test
  public void testTransitionWidthAdditionalNodelReactionToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transitionWithAdditionalNodes.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getClass(), reaction2.getClass());
    assertEquals(reaction1.getReactants().size(), reaction2.getReactants().size());
    Reactant reactant1 = reaction1.getReactants().get(1);
    Reactant reactant2 = reaction2.getReactants().get(1);
    assertEquals(reactant1.getLine().getBeginAtd().getArrowType(), reactant2.getLine().getBeginAtd().getArrowType());
    assertEquals(reactant1.getLine().getEndAtd().getArrowType(), reactant2.getLine().getEndAtd().getArrowType());
    assertEquals(reactant1.getLine().getLines().size(), reactant2.getLine().getLines().size());

    Product product1 = reaction1.getProducts().get(1);
    Product product2 = reaction2.getProducts().get(1);
    assertEquals(product1.getLine().getBeginAtd().getArrowType(), product2.getLine().getBeginAtd().getArrowType());
    assertEquals(product1.getLine().getEndAtd().getArrowType(), product2.getLine().getEndAtd().getArrowType());
    assertEquals(product1.getLine().getLines().size(), product2.getLine().getLines().size());

    assertTrue(reactant1.getLine().getEndPoint().distance(reactant2.getLine().getEndPoint()) < 1e-6);
    assertEquals(reaction1.getReactionRect(), reaction2.getReactionRect());

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }

    assertEquals(product1.getLine().getColor(), product2.getLine().getColor());
    assertEquals(product1.getLine().getWidth(), product2.getLine().getWidth(), 1e-6);
  }

  @Test
  public void testTranslationToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_translation.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getClass(), reaction2.getClass());
    assertEquals(reaction1.getReactants().size(), reaction2.getReactants().size());
    Reactant reactant1 = reaction1.getReactants().get(1);
    Reactant reactant2 = reaction2.getReactants().get(1);
    assertEquals(reactant1.getLine().getBeginAtd().getArrowType(), reactant2.getLine().getBeginAtd().getArrowType());
    assertEquals(reactant1.getLine().getEndAtd().getArrowType(), reactant2.getLine().getEndAtd().getArrowType());
    assertEquals(reactant1.getLine().getLines().size(), reactant2.getLine().getLines().size());

    Product product1 = reaction1.getProducts().get(1);
    Product product2 = reaction2.getProducts().get(1);
    assertEquals(product1.getLine().getBeginAtd().getArrowType(), product2.getLine().getBeginAtd().getArrowType());
    assertEquals(product1.getLine().getEndAtd().getArrowType(), product2.getLine().getEndAtd().getArrowType());
    assertEquals(product1.getLine().getLines().size(), product2.getLine().getLines().size());

    assertTrue(reactant1.getLine().getEndPoint().distance(reactant2.getLine().getEndPoint()) < 1e-6);
    assertEquals(reaction1.getReactionRect(), reaction2.getReactionRect());

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }

    assertEquals(product1.getLine().getColor(), product2.getLine().getColor());
    assertEquals(product1.getLine().getWidth(), product2.getLine().getWidth(), 1e-6);
  }

  @Test
  public void testTransportToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transport.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
    assertEquals(reaction1.getMiriamData().size(), reaction2.getMiriamData().size());
    assertEquals(reaction1.getNotes().trim(), reaction2.getNotes().trim());
  }

  @Test
  public void testTruncationToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_truncation.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testTruncationWithModifierToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_truncationWithModifier.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }

    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testUnknownTransitionToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_unknown_transition.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testTransitionOmittedToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transition_omitted.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testTranscriptionToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transcription.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testTranscriptionWithAdditionsToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transcription_with_additions.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testReactionWithOperatorsToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_with_operators.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testReactionWithModifiersToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_with_modifiers.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);

    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testReactionPositiveInfluenceToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_positive_influence.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testReactionHeterodimerWithAdditionsToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_heterodimer_with_additions.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getNodes().size(), reaction2.getNodes().size());

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testReactionHeterodimerToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_heterodimer.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getNodes().size(), reaction2.getNodes().size());

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testOperatorInReactionHeterodimer() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_heterodimer.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);
    for (NodeOperator operator : reaction1.getOperators()) {
      assertTrue(operator instanceof AssociationOperator);
    }
  }

  @Test
  public void testReactionDissociationWithAdditionToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_dissociation_with_addition.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getName(), reaction2.getName());
    assertNotNull(reaction1.getName());
    assertFalse(reaction1.getName().trim().equals(""));
    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  private Model getModelFilledWithSpecies() {
    Model model = new ModelFullIndexed(null);

    Species sa1 = new GenericProtein("sa1");
    sa1.setX(100.0);
    sa1.setY(200.0);
    sa1.setWidth(300.0);
    sa1.setHeight(400.0);
    model.addElement(sa1);
    elements.addModelElement(sa1, new CellDesignerGenericProtein("s1"));

    Species sa2 = new GenericProtein("sa2");
    sa2.setX(1050.0);
    sa2.setY(2050.0);
    sa2.setWidth(300.0);
    sa2.setHeight(450.0);
    model.addElement(sa2);
    elements.addModelElement(sa2, new CellDesignerGenericProtein("s2"));

    Species sa3 = new GenericProtein("sa3");
    sa3.setX(600.0);
    sa3.setY(250.0);
    sa3.setWidth(300.0);
    sa3.setHeight(400.0);
    model.addElement(sa3);
    elements.addModelElement(sa3, new CellDesignerGenericProtein("s3"));

    Species sa4 = new GenericProtein("sa4");
    sa4.setX(550.0);
    sa4.setY(350.0);
    sa4.setWidth(300.0);
    sa4.setHeight(450.0);
    model.addElement(sa4);
    elements.addElement(new CellDesignerGenericProtein("s4"));

    Species sa5 = new GenericProtein("sa5");
    sa5.setX(10.0);
    sa5.setY(250.0);
    sa5.setWidth(300.0);
    sa5.setHeight(450.0);
    model.addElement(sa5);
    elements.addElement(new CellDesignerGenericProtein("s5"));

    Species sa6 = new GenericProtein("sa6");
    sa6.setX(10.0);
    sa6.setY(250.0);
    sa6.setWidth(300.0);
    sa6.setHeight(450.0);
    model.addElement(sa6);

    elements.addElement(new CellDesignerGenericProtein("s6"));

    Species sa10 = new GenericProtein("sa10");
    sa10.setX(210.0);
    sa10.setY(220.0);
    sa10.setWidth(320.0);
    sa10.setHeight(250.0);
    model.addElement(sa10);
    elements.addElement(new CellDesignerGenericProtein("s10"));

    Species sa11 = new GenericProtein("sa11");
    sa11.setX(11.0);
    sa11.setY(320.0);
    sa11.setWidth(321.0);
    sa11.setHeight(150.0);
    model.addElement(sa11);
    elements.addElement(new CellDesignerGenericProtein("s11"));

    Species sa12 = new GenericProtein("sa12");
    sa12.setX(12.0);
    sa12.setY(20.0);
    sa12.setWidth(321.0);
    sa12.setHeight(150.0);
    model.addElement(sa12);
    elements.addElement(new CellDesignerGenericProtein("s12"));

    Species sa13 = new GenericProtein("sa13");
    sa13.setX(513.0);
    sa13.setY(20.0);
    sa13.setWidth(321.0);
    sa13.setHeight(150.0);
    model.addElement(sa13);
    elements.addElement(new CellDesignerGenericProtein("s13"));

    Species sa14 = new GenericProtein("sa14");
    sa14.setX(14.0);
    sa14.setY(820.0);
    sa14.setWidth(321.0);
    sa14.setHeight(150.0);
    model.addElement(sa14);
    elements.addElement(new CellDesignerGenericProtein("s14"));

    Species sa15 = new GenericProtein("sa15");
    sa15.setX(815.0);
    sa15.setY(620.0);
    sa15.setWidth(321.0);
    sa15.setHeight(150.0);
    model.addElement(sa15);
    elements.addElement(new CellDesignerGenericProtein("s15"));

    return model;
  }

  @Test
  public void testLogiGateAndReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateAnd.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof ReducedTriggerReaction);
    assertEquals(1, reaction.getOperators().size());
    assertTrue(reaction.getOperators().get(0) instanceof AndOperator);
    assertEquals(2, reaction.getReactants().size());
    assertEquals(1, reaction.getProducts().size());

    assertEquals(0.0, reaction.getReactants().get(0).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getReactants().get(1).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getOperators().get(0).getLine().getBeginPoint().distance(200.0, 127.0), EPSILON);

    super.testXmlSerialization(model);
  }

  @Test
  public void testLogiGateAndWithThreeInputsReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateAndWithThreeInputs.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof ReducedTriggerReaction);

    super.testXmlSerialization(model);
  }

  @Test
  public void testLogiGateOrReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateOr.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof NegativeInfluenceReaction);
    assertEquals(1, reaction.getOperators().size());
    assertTrue(reaction.getOperators().get(0) instanceof OrOperator);
    assertEquals(2, reaction.getReactants().size());
    assertEquals(1, reaction.getProducts().size());

    assertEquals(0.0, reaction.getReactants().get(0).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getReactants().get(1).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getOperators().get(0).getLine().getBeginPoint().distance(200.0, 127.0), EPSILON);

    super.testXmlSerialization(model);
  }

  @Test
  public void testLogiGateNotReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateNot.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof UnknownReducedPhysicalStimulationReaction);
    assertEquals(1, reaction.getOperators().size());
    assertTrue(reaction.getOperators().get(0) instanceof NandOperator);
    assertEquals(2, reaction.getReactants().size());
    assertEquals(1, reaction.getProducts().size());

    assertEquals(0.0, reaction.getReactants().get(0).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getReactants().get(1).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getOperators().get(0).getLine().getBeginPoint().distance(200.0, 127.0), EPSILON);

    assertNotNull(reaction.getLine());

    super.testXmlSerialization(model);
  }

  @Test
  public void testLogiGateUnknownReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateUnknown.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof ReducedTriggerReaction);
    assertEquals(1, reaction.getOperators().size());
    assertTrue(reaction.getOperators().get(0) instanceof UnknownOperator);
    assertEquals(2, reaction.getReactants().size());
    assertEquals(1, reaction.getProducts().size());

    assertEquals(0.0, reaction.getReactants().get(0).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getReactants().get(1).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getOperators().get(0).getLine().getBeginPoint().distance(200.0, 127.0), EPSILON);

    super.testXmlSerialization(model);
  }

  @Test
  public void testHeterodimerWithAdditionalReactant() throws Exception {
    Model model = getModelForFile("testFiles/reactions/heterodimer_association_with_additional_reactant.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();

    NodeOperator o1 = reaction.getOperators().get(0);
    NodeOperator o2 = reaction.getOperators().get(1);

    assertEquals(0, o1.getLine().getEndPoint().distance(o2.getLine().getBeginPoint()), Configuration.EPSILON);
  }

  @Test
  public void testProblematicDrawing() throws Exception {
    Model model = getModelForFile("testFiles/problematic/reaction_drawing_problem.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    assertNotNull(reaction.getLine());
  }

  @Test
  public void testProteinsInsideComplex() throws Exception {
    Model model = getModelForFile("testFiles/problematic/proteins_inside_complex.xml");
    for (Element el : model.getElements()) {
      assertFalse(el.getClass().equals(Protein.class));
    }
  }

  @Test
  public void testKinetcs() throws Exception {
    Model model = getModelForFile("testFiles/reactions/kinetics.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    assertNotNull(reaction.getKinetics());
  }

  @Test
  public void testKinetcsWithCompartment() throws Exception {
    Model model = getModelForFile("testFiles/reactions/kinetics_with_compartment.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    assertNotNull(reaction.getKinetics());
  }

  @Test
  public void testModifierWithOperator() throws Exception {
    Model model = getModelForFile("testFiles/reactions/modifier_with_operator.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    assertEquals(3, reaction.getOperators().iterator().next().getLine().getPoints().size());
    assertEquals(3, reaction.getModifiers().get(0).getLine().getPoints().size());
    assertEquals(3, reaction.getModifiers().get(1).getLine().getPoints().size());
    Model model2 = super.serializeModel(model);
    reaction = model2.getReactionByReactionId("re1");
    assertEquals(3, reaction.getOperators().iterator().next().getLine().getPoints().size());
    assertEquals(3, reaction.getModifiers().get(0).getLine().getPoints().size());
    assertEquals(3, reaction.getModifiers().get(1).getLine().getPoints().size());
  }

  @Test
  public void testProblematicHeterodimerAssociation() throws Exception {
    Model model = getModelForFile("testFiles/problematic/problematic_heterodimer_association.xml");
    Reaction reaction = model.getReactions().iterator().next();
    for (Line2D line : reaction.getLines()) {
      assertTrue("Expected coordinates must be not negative. Found: " + line.getP1(), line.getP1().getX() >= 0);
      assertTrue("Expected coordinates must be not negative. Found: " + line.getP1(), line.getP1().getY() >= 0);
      assertTrue("Expected coordinates must be not negative. Found: " + line.getP2(), line.getP2().getX() >= 0);
      assertTrue("Expected coordinates must be not negative. Found: " + line.getP2(), line.getP2().getY() >= 0);
    }
  }

  @Test
  public void testGateReactionWithMoreParticipants() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateWithMoreParticipants.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    assertEquals(3, reaction.getReactants().size());
    Reactant r1 = reaction.getReactants().get(0);
    Reactant r2 = reaction.getReactants().get(1);
    Reactant r3 = reaction.getReactants().get(2);
    Product p = reaction.getProducts().get(0);

    assertEquals(3, r1.getLine().getPoints().size());
    assertEquals(2, r2.getLine().getPoints().size());
    assertEquals(3, r3.getLine().getPoints().size());

    assertEquals(2, p.getLine().getPoints().size());

    super.testXmlSerialization(model);
  }

}
