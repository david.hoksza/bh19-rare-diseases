package lcsb.mapviewer.converter.model.celldesigner.alias;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.species.*;

public class SpeciesAliasXmlParserTest extends CellDesignerTestFunctions {
  static Logger logger = LogManager.getLogger(SpeciesAliasXmlParser.class);

  Model model;
  CellDesignerElementCollection elements;
  private SpeciesAliasXmlParser parser;
  private String testCompartmentAliasId2 = "s3";
  private String testCompartmentAliasId = "s4";
  private String testSpeciesId = "s5";
  private String testAliasId = "s6";

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();

    model = createStubModel();

    CellDesignerGenericProtein species = new CellDesignerGenericProtein();
    species.setElementId("s78");
    elements.addElement(species);

    parser = new SpeciesAliasXmlParser(elements, model);
  }

  private Model createStubModel() {
    Model model = new ModelFullIndexed(null);
    model.addElement(new Compartment("ca1"));
    return model;
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlAliasNode() throws Exception {
    String xmlString = readFile("testFiles/xmlNodeTestExamples/species_alias.xml");
    Species alias = parser.parseXmlAlias(xmlString);
    assertEquals(0, getWarnings().size());
    assertEquals(false, alias.getActivity());
    assertEquals("sa36", alias.getElementId());
    assertEquals(16.0, alias.getFontSize(), 1e-6);
    assertEquals(25.0, alias.getHeight(), 1e-6);
    assertEquals(80, alias.getWidth(), 1e-6);
    assertEquals(11813.0, alias.getX(), 1e-6);
    assertEquals(2840.5, alias.getY(), 1e-6);
    assertEquals("ca1", alias.getCompartment().getElementId());
  }

  @Test
  public void testParseXmlAliasFont() throws Exception {
    String xmlString = readFile("testFiles/xmlNodeTestExamples/species_alias_with_font.xml");
    Species alias = parser.parseXmlAlias(xmlString);
    assertEquals(30.0, alias.getFontSize(), 1e-6);
  }

  @Test
  public void testToXml() throws Exception {
    Model model2 = createStubModel();
    Species protein = createProtein();
    protein.setCompartment(model2.getCompartments().iterator().next());
    elements.addElement(new CellDesignerGenericProtein(elements.getElementId(protein)));
    String xmlString = parser.toXml(protein);

    parser = new SpeciesAliasXmlParser(elements, model2);
    Species alias2 = parser.parseXmlAlias(xmlString);
    assertEquals(0, getWarnings().size());

    assertEquals(protein.getActivity(), alias2.getActivity());
    assertEquals(protein.getElementId(), alias2.getElementId());
    assertEquals(protein.getFontSize(), alias2.getFontSize(), 1e-6);
    assertEquals(protein.getHeight(), alias2.getHeight(), 1e-6);
    assertEquals(protein.getWidth(), alias2.getWidth(), 1e-6);
    assertEquals(protein.getX(), alias2.getX(), 1e-6);
    assertEquals(protein.getY(), alias2.getY(), 1e-6);
    assertEquals(protein.getCompartment().getElementId(), alias2.getCompartment().getElementId());
    assertEquals("Alias state label different", protein.getStateLabel(), alias2.getStateLabel());
    assertEquals("Alias state prefix different", protein.getStatePrefix(), alias2.getStatePrefix());
  }

  private Species createProtein() {
    GenericProtein protein = new GenericProtein("id");
    protein.setActivity(true);
    protein.setFontSize(4);
    protein.setHeight(10);
    protein.setWidth(20);
    protein.setX(30);
    protein.setY(40);
    protein.setStateLabel("xxx");
    protein.setStatePrefix("yyy");
    return protein;
  }

  @Test
  public void testToXmlAliasWithStateNode() throws Exception {
    Model model = createStubModel();
    Species protein = createProtein();
    protein.setState("sssstate");
    protein.setCompartment(model.getCompartments().iterator().next());
    elements.addElement(new CellDesignerGenericProtein(elements.getElementId(protein)));
    String xmlString = parser.toXml(protein);

    Species alias2 = parser.parseXmlAlias(xmlString);
    assertEquals(0, getWarnings().size());

    assertEquals("Alias state label different", protein.getStateLabel(), alias2.getStateLabel());
    assertEquals("Alias state prefix different", protein.getStatePrefix(), alias2.getStatePrefix());
  }

  @Test
  public void testParseXmlAliasWithStateNode() throws Exception {
    String xmlString = readFile("testFiles/xmlNodeTestExamples/species_alias_with_state.xml");
    Species alias = parser.parseXmlAlias(xmlString);
    assertEquals(0, getWarnings().size());
    assertNotNull(alias);
    assertEquals("test", alias.getStateLabel());
    assertEquals("free input", alias.getStatePrefix());
  }

  @Test
  public void testParseXmlAliasWithoutStateNode() throws Exception {
    String xmlString = readFile("testFiles/xmlNodeTestExamples/species_alias_without_state.xml");
    Species alias = parser.parseXmlAlias(xmlString);
    assertEquals(0, getWarnings().size());
    assertNotNull(alias);
    assertNull(alias.getStateLabel());
    assertNull(alias.getStatePrefix());
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlAliasNode() throws Exception {
    String xmlString = readFile("testFiles/invalid/species_alias.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlAliasNode2() throws Exception {
    String xmlString = readFile("testFiles/invalid/species_alias2.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlAliasNode3() throws Exception {
    String xmlString = readFile("testFiles/invalid/species_alias3.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlAliasNode4() throws Exception {
    parser = new SpeciesAliasXmlParser(elements, model);

    String xmlString = readFile("testFiles/invalid/species_alias4.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlAliasNode5() throws Exception {
    parser = new SpeciesAliasXmlParser(elements, model);

    String xmlString = readFile("testFiles/invalid/species_alias5.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlAliasNode6() throws Exception {
    parser = new SpeciesAliasXmlParser(elements, model);

    String xmlString = readFile("testFiles/invalid/species_alias6.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test
  public void testToXmlWithUnknownCompartment() throws Exception {
    Model model = Mockito.mock(ModelFullIndexed.class);
    ModelData md = new ModelData();
    md.setModel(model);

    Compartment ca2 = new Compartment(testCompartmentAliasId2);
    ca2.setElementId(testCompartmentAliasId2);
    ca2.setX(6);
    ca2.setY(6);
    ca2.setWidth(190);
    ca2.setHeight(190);

    Compartment ca = new Compartment(testCompartmentAliasId);
    ca.setElementId(testCompartmentAliasId);
    ca.setX(5);
    ca.setY(5);
    ca.setWidth(200);
    ca.setHeight(200);

    List<Compartment> list = new ArrayList<>();
    list.add(ca);
    list.add(ca2);

    // ensure that we return list (firts bigger compartment, then smaller)
    when(model.getCompartments()).thenReturn(list);
    when(model.getModelData()).thenReturn(md);

    Species alias = new Complex(testAliasId);
    alias.setX(10);
    alias.setY(10);
    alias.setWidth(100);
    alias.setHeight(100);
    alias.setModel(model);
    alias.setActivity(true);

    elements.addElement(new CellDesignerComplexSpecies(testSpeciesId));
    model.addElement(alias);
    SpeciesAliasXmlParser parser = new SpeciesAliasXmlParser(elements, model);

    String xml = parser.toXml(alias);

    assertTrue(xml.contains(testAliasId));
    assertTrue(xml.contains(testCompartmentAliasId2));
    assertTrue(xml.contains("<celldesigner:activity>active</celldesigner:activity>"));
  }

}
