package lcsb.mapviewer.converter.model.celldesigner.structure;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.converter.model.celldesigner.structure.fields.AllFieldsTests;

@RunWith(Suite.class)
@SuiteClasses({ AllFieldsTests.class,
    AntisenseRnaTest.class,
    CellDesignerChemicalTest.class,
    CompartmentTest.class,
    ComplexSpeciesTest.class,
    ConnectSchemeTest.class,
    DegradedTest.class,
    DrugTest.class,
    ElementTest.class,
    GenericProteinTest.class,
    GeneTest.class,
    IonChannelProteinTest.class,
    IonTest.class,
    LinePropertiesTest.class,
    ModificationResidueTest.class,
    PhenotypeTest.class,
    ProteinTest.class,
    ReceptorProteinTest.class,
    RnaTest.class,
    SimpleMoleculeTest.class,
    SpeciesStateTest.class,
    SpeciesTest.class,
    TruncatedProteinTest.class,
    UnknownTest.class,
    ViewTest.class,
})
public class AllStructureTests {

}
