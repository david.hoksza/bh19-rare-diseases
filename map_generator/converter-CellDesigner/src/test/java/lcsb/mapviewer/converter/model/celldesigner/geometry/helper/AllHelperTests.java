package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CellDesignerAnchorTest.class,
    CellDesignerEllipseTransformationTest.class,
    CellDesignerLineTransformationTest.class,
    CellDesignerPointTransformationTest.class,
    CellDesignerPolygonTransformationTest.class,
    CellDesignerRectangleTransformationTest.class,
    PolylineDataFactoryTest.class,
})
public class AllHelperTests {

}
