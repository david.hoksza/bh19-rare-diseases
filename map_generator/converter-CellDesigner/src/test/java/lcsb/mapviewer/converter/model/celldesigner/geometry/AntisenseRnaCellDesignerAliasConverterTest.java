package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertNotNull;

import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;

import org.junit.*;

import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class AntisenseRnaCellDesignerAliasConverterTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetPointCoordinatesForSbgn() {
    AntisenseRnaCellDesignerAliasConverter converter = new AntisenseRnaCellDesignerAliasConverter(true);

    Model model = new ModelFullIndexed(null);
    Species alias = new GenericProtein("id");
    alias.setWidth(10);
    alias.setHeight(10);
    alias.setModel(model);
    Point2D point = converter.getPointCoordinates(alias, CellDesignerAnchor.E);
    assertNotNull(point);
  }

  @Test
  public void testGetAntisenseRnaPathForSbgn() {
    AntisenseRnaCellDesignerAliasConverter converter = new AntisenseRnaCellDesignerAliasConverter(true);

    Model model = new ModelFullIndexed(null);
    Species alias = new GenericProtein("id");
    alias.setWidth(10);
    alias.setHeight(10);
    alias.setModel(model);
    PathIterator path = converter.getBoundPathIterator(alias);
    assertNotNull(path);
  }

}
