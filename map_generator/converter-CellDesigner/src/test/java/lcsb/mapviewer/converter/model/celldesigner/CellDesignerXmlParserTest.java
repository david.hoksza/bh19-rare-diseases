package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.awt.Color;
import java.awt.geom.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.*;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.*;

public class CellDesignerXmlParserTest extends CellDesignerTestFunctions {
  Logger logger = LogManager.getLogger(CellDesignerXmlParserTest.class);

  ModelComparator modelComparator = new ModelComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testOpenFromInputStream() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/sample.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertNotNull(model);
  }

  @Test
  public void testZIndexAvailableForBioEntities() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));
    for (BioEntity bioEntity : model.getBioEntities()) {
      assertNotNull(bioEntity.getZ());
    }
  }

  @Test
  public void testOpenFromFile() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));
    assertNotNull(model);
    assertEquals("sample", model.getName());
  }

  @Test
  public void testSubstanceUnitAsType() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/problematic/substance_defined_as_type.xml")
            .sizeAutoAdjust(false));
    assertNotNull(model);
    assertNotNull(model.getSpeciesList().get(0).getSubstanceUnits());
    super.testXmlSerialization(model);
  }

  @Test
  public void testCellDesigner2_5() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/problematic/MYO_signaling_pathway.xml")
            .sizeAutoAdjust(false));
    assertNotNull(model);
    super.testXmlSerialization(model);
  }

  @Test
  public void testUnknownCatalysisWithGate() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/problematic/unknown_catalysis_with_gate.xml")
            .sizeAutoAdjust(false));

    Reaction reaction = model.getReactionByReactionId("re1");
    NodeOperator operator = reaction.getOperators().get(0);
    for (Modifier modifiers : reaction.getModifiers()) {
      assertEquals(operator.getLine().getType(), modifiers.getLine().getType());
    }
    super.testXmlSerialization(model);
  }

  @Test
  public void testParseTransparency() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/model_with_transparency.xml"));
    assertFalse(model.getCompartments().get(0).getTransparencyLevel().isEmpty());
  }

  @Test
  public void testParseTransparencyDeprecated() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/model_with_transparency_old.xml"));
    assertFalse(model.getCompartments().get(0).getTransparencyLevel().isEmpty());
  }

  @Test
  public void testParseVcard() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/model_with_vcard.xml"));
    assertEquals("There is one author defined", 1, model.getAuthors().size());
    Author author = model.getAuthors().get(0);
    assertNotNull("Author data cannot be null", author);
    assertEquals("Piotr", author.getFirstName());
    assertEquals("Gawron", author.getLastName());
    assertEquals("piotr.gawron@uni.lu", author.getEmail());
    assertEquals("LCSB", author.getOrganisation());
    assertEquals("Modification date is not defined", 1, model.getModificationDates().size());
    assertNotNull("Creation date is not defined", model.getCreationDate());
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidFile() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidFile2() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample2.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidFile3() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample10.xml"));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseAnnotationComplexAliasesConnections() throws Exception {
    Model model = new ModelFullIndexed(null);
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.parseAnnotationComplexAliasesConnections(model,
        super.getNodeFromXmlString(readFile("testFiles/invalid/complex_species_alias.xml")));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseAnnotationComplexAliasesConnections2() throws Exception {
    Model model = new ModelFullIndexed(null);
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.parseAnnotationComplexAliasesConnections(model,
        super.getNodeFromXmlString(readFile("testFiles/invalid/complex_species_alias2.xml")));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseAnnotationAliasesConnections() throws Exception {
    Model model = new ModelFullIndexed(null);
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.parseAnnotationAliasesConnections(model,
        super.getNodeFromXmlString(readFile("testFiles/invalid/list_of_species_alias.xml")));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseAnnotationAliasesConnections2() throws Exception {
    Model model = new ModelFullIndexed(null);
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.parseAnnotationAliasesConnections(model,
        super.getNodeFromXmlString(readFile("testFiles/invalid/list_of_species_alias2.xml")));
  }

  @Test
  public void testEmptyModelToXml() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/empty.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    String xmlString = parser.model2String(model);
    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = parser.createModel(new ConverterParams().inputStream(is));

    assertNotNull(model2);

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));

    // default compartment definition looks like (but properties order might
    // be different)
    // <compartment metaid="default" id="default" size="1" units="volume"/>

    assertTrue("There is no default compartment", xmlString.indexOf("id=\"default\"") >= 0);
  }

  @Test
  public void testSimpleModelToXml() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/sample.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    String xmlString = parser.model2String(model);

    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = parser.createModel(new ConverterParams().inputStream(is));

    // we have to replace size of the map because of auto resizing during
    // reading
    model2.setWidth(model.getWidth());
    model2.setHeight(model.getHeight());

    assertNotNull(model2);

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testModifierReactionModelToXml() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/reactions/modification_reaction/catalysis.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    String xmlString = parser.model2String(model);

    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testAutoMapWidth() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/autoMapWidth.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertTrue(model.getWidth() > 700);
    assertTrue(model.getHeight() > 500);
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testThrowIOException() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    InputStream fis = Mockito.mock(InputStream.class);
    when(fis.read()).thenThrow(new IOException());

    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidInputFile1() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_1.xml");
    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidInputFile2() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_2.xml");
    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidInputFile3() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_3.xml");
    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidInputFile4() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_4.xml");
    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidBrokenTypeReaction() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/invalid/broken_type_reaction.xml");
    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test
  public void testToXmlAfterAnnotating() throws Exception {
    Model model = new ModelFullIndexed(null);
    model.setIdModel("as");
    model.setWidth(10);
    model.setHeight(10);
    Species speciesAlias = new GenericProtein("id1");
    speciesAlias.setName("ROS");
    RestAnnotationParser rap = new RestAnnotationParser();
    rap.processNotes(
        "Symbol: ROS1\r\nName: c-ros oncogene 1 , receptor tyrosine kinase\r\nDescription: RecName: Full=Proto-oncogene tyrosine-protein kinase ROS; EC=2.7.10.1; AltName: Full=Proto-oncogene c-Ros; AltName: Full=Proto-oncogene c-Ros-1; AltName: Full=Receptor tyrosine kinase c-ros oncogene 1; AltName: Full=c-Ros receptor tyrosine kinase; Flags: Precursor;\r\nPrevious Symbols:\r\nSynonyms: ROS, MCF3",
        speciesAlias);
    model.addElement(speciesAlias);

    GenericProtein alias = new GenericProtein("id");
    alias.setName("ROS2");
    model.addElement(alias);

    SimpleMolecule speciesAlias2 = new SimpleMolecule("id2");
    speciesAlias2.setName("PDK1");
    rap.processNotes(
        "Symbol: ROS1\r\nName: c-ros oncogene 1 , receptor tyrosine kinase\r\nDescription: RecName: Full=Proto-oncogene tyrosine-protein kinase ROS; EC=2.7.10.1; AltName: Full=Proto-oncogene c-Ros; AltName: Full=Proto-oncogene c-Ros-1; AltName: Full=Receptor tyrosine kinase c-ros oncogene 1; AltName: Full=c-Ros receptor tyrosine kinase; Flags: Precursor;\r\nPrevious Symbols:\r\nSynonyms: ROS, MCF3",
        speciesAlias2);
    model.addElement(speciesAlias2);

    SimpleMolecule alias2 = new SimpleMolecule("id3");
    alias2.setName("PDK2");
    model.addElement(alias2);

    Model model2 = serializeModel(model);

    assertEquals(0, modelComparator.compare(model, model2));
  }

  @Test
  public void testModelWithNullNotesToXml() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = new ModelFullIndexed(null);
    model.setWidth(20);
    model.setHeight(20);
    model.setNotes(null);
    model.setIdModel("id");
    String xmlString = parser.model2String(model);
    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = parser.createModel(new ConverterParams().inputStream(is));

    assertNotNull(model2);

    assertTrue(model2.getNotes() == null || model2.getNotes().isEmpty());
  }

  @Test
  public void testExportXmlForEmptyModel() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = new ModelFullIndexed(null);
    model.setWidth(20);
    model.setHeight(20);
    model.setNotes(null);
    model.setIdModel("id");
    Species alias = new GenericProtein("a");
    alias.setName("AA");
    model.addElement(alias);

    parser.model2String(model);
  }

  @Test
  public void testExportXmlForEmptyModel2() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = new ModelFullIndexed(null);
    model.setWidth(20);
    model.setHeight(20);
    model.setNotes(null);
    model.setIdModel("id");

    parser.model2String(model);
  }

  @Test
  public void testExportImportModelWithSpecialCharacterInNotes() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = new ModelFullIndexed(null);
    model.setWidth(20);
    model.setHeight(20);
    model.setNotes(">");
    model.setIdModel("id");
    String xmlString = parser.model2String(model);
    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = parser.createModel(new ConverterParams().inputStream(is));

    assertNotNull(model2);

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testHomodimerSpecies() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/homodimer.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    for (Element element : model.getElements()) {
      if (element instanceof Species) {
        assertTrue("Homodimer value for class" + element.getClass() + " not upadted",
            ((Species) element).getHomodimer() > 1);
      }
    }
  }

  @Test
  public void testReactionWithNegativeCoords() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/negativeCoords.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertTrue(model.getWidth() >= 0);
    assertTrue(model.getHeight() >= 0);
  }

  @Test
  public void testaAnnotations() throws Exception {
    CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();
    Model model = cellDesignerXmlParser
        .createModel(new ConverterParams().filename("testFiles/problematic/invalidAlias.xml"));
    assertNotNull(model);
  }

  @Test
  public void testProblematicModification() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/problematic/problematic_modification.xml"));
    assertNotNull(model);
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidModifierType() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/problematic/problematic_modifier_type.xml"));
  }

  @Test
  public void testHtmlTagInSymbolName() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/notes_with_html_coding.xml"));

    Element p = model.getElementByElementId("sa1");

    assertEquals(">symbol<", p.getSymbol());
  }

  @Test
  public void testModelBound() {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();

    Model model = new ModelFullIndexed(null);
    model.setWidth(0);
    model.setHeight(0);

    Rectangle2D bound = parser.getModelBound(model);
    assertEquals(0, bound.getWidth(), EPSILON);
    assertEquals(0, bound.getHeight(), EPSILON);
  }

  @Test
  public void testModelBoundWithLine() {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();

    Layer layer = new Layer();
    PolylineData line = new PolylineData(new Point2D.Double(2, 3), new Point2D.Double(20, 30));
    layer.addLayerLine(line);

    Model model = new ModelFullIndexed(null);
    model.addLayer(layer);
    model.setWidth(100);
    model.setHeight(100);

    Rectangle2D bound = parser.getModelBound(model);
    assertEquals(18, bound.getWidth(), EPSILON);
    assertEquals(27, bound.getHeight(), EPSILON);
  }

  @Test
  public void testModelBoundWithRectangle() {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();

    Layer layer = new Layer();
    LayerRect rect = new LayerRect();
    rect.setX(10.0);
    rect.setY(20.0);
    rect.setWidth(30.0);
    rect.setHeight(45.0);
    layer.addLayerRect(rect);

    Model model = new ModelFullIndexed(null);
    model.addLayer(layer);
    model.setWidth(100);
    model.setHeight(100);

    Rectangle2D bound = parser.getModelBound(model);
    assertEquals(30, bound.getWidth(), EPSILON);
    assertEquals(45, bound.getHeight(), EPSILON);
  }

  @Test
  public void testModelBoundWithOval() {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();

    Layer layer = new Layer();
    LayerOval oval = new LayerOval();
    oval.setX(11.0);
    oval.setY(21.0);
    oval.setWidth(31.0);
    oval.setHeight(46.0);
    layer.addLayerOval(oval);

    Model model = new ModelFullIndexed(null);
    model.addLayer(layer);
    model.setWidth(100);
    model.setHeight(100);

    Rectangle2D bound = parser.getModelBound(model);
    assertEquals(31, bound.getWidth(), EPSILON);
    assertEquals(46, bound.getHeight(), EPSILON);
  }

  @Test
  public void testModelBoundWithText() {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();

    Layer layer = new Layer();
    LayerText text = new LayerText();
    text.setX(12.0);
    text.setY(31.0);
    text.setWidth(56.0);
    text.setHeight(52.0);
    layer.addLayerText(text);

    Model model = new ModelFullIndexed(null);
    model.addLayer(layer);
    model.setWidth(100);
    model.setHeight(100);

    Rectangle2D bound = parser.getModelBound(model);
    assertEquals(56, bound.getWidth(), EPSILON);
    assertEquals(52, bound.getHeight(), EPSILON);
  }

  @Test
  public void testGetters() {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    assertNotNull(parser.getCommonName());
    assertNotNull(parser.getMimeType());
    assertNotNull(parser.getFileExtension());
  }

  @Test
  public void testToXmlWithGene() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = new ModelFullIndexed(null);
    model.setWidth(100);
    model.setHeight(100);
    Gene gene = new Gene("gene_id_1");
    gene.setName("geneNAME");
    model.addElement(gene);
    String xmlString = parser.model2String(model);
    assertTrue(xmlString.contains("gene_id_1"));
    assertTrue(xmlString.contains("geneNAME"));
  }

  @Test
  public void testToInputString() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = new ModelFullIndexed(null);
    model.setWidth(100);
    model.setHeight(100);
    InputStream is = parser.model2InputStream(model);
    BufferedReader in = new BufferedReader(new InputStreamReader(is));
    String str = in.readLine();
    assertNotNull(str);
    assertFalse(str.isEmpty());
    is.close();
  }

  @Test
  public void testToFile() throws Exception {
    String filename = "tmp.xml";
    if (new File(filename).exists()) {
      new File(filename).delete();
    }
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = new ModelFullIndexed(null);
      model.setWidth(100);
      model.setHeight(100);
      parser.model2File(model, filename);

      File file = new File(filename);
      assertTrue(file.exists());
    } finally {
      if (new File(filename).exists()) {
        new File(filename).delete();
      }
    }
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidSpeciesStateFile() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample5.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidSpeciesStateFile2() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample6.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidSpeciesStateFile3() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample7.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidSpeciesStateFile5() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample8.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidSpeciesStateFile6() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample9.xml"));
  }

  @Test
  public void testOpenProblematicFile() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/problematic/problematic_notes.xml"));
    Element element = model.getElementByElementId("sa2338");
    assertFalse("Element note cannot contain head html tag", element.getNotes().contains("</head>"));
  }

  @Test
  public void testNestedComp() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/compartment/nested_compartments.xml"));

    assertNotNull(model.getElementByElementId("ca2").getCompartment());
    assertNotNull(model.getElementByElementId("sa1").getCompartment());

    String xml = parser.model2String(model);

    Model model2 = parser.createModel(
        new ConverterParams().inputStream(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))));

    ModelComparator comparator = new ModelComparator();

    model.setName(null);
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testCompartmentWithNotes() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/compartment_with_notes.xml"));
    model.setName(null);
    assertNotNull(model);
    String str = parser.model2String(model);
    Model model2 = parser.createModel(
        new ConverterParams().inputStream(new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8))));
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void testCompartmentWithSubcompartments() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(
        new ConverterParams().filename("testFiles/compartment/nested_compartments_in_few_compartments.xml"));
    Compartment c3 = model.getElementByElementId("ca3");
    Compartment c4 = model.getElementByElementId("ca4");
    Compartment c1 = model.getElementByElementId("ca1");
    Compartment c2 = model.getElementByElementId("ca2");
    assertEquals("ca1", c3.getCompartment().getElementId());
    assertEquals("ca2", c4.getCompartment().getElementId());

    assertEquals(1, c1.getElements().size());
    assertEquals(1, c2.getElements().size());
    assertEquals(0, c3.getElements().size());
    assertEquals(0, c4.getElements().size());
  }

  @Test
  public void testReactionWithStochiometry() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/stochiometry.xml"));
    Reaction reaction = model.getReactionByReactionId("re1");

    assertEquals(2.0, reaction.getReactants().get(0).getStoichiometry(), Configuration.EPSILON);
    assertNull(reaction.getProducts().get(0).getStoichiometry());
  }

  @Test
  public void testModelWithSelfReactionToXml() throws Exception {
    Model model = new ModelFullIndexed(null);
    model.setIdModel("as");
    model.setWidth(10);
    model.setHeight(10);
    Species protein = new GenericProtein("id1");
    protein.setName("ROS");
    model.addElement(protein);

    Reaction reaction = new StateTransitionReaction();
    reaction.setIdReaction("re1");
    Product product = new Product(protein);
    product.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(20, 20)));
    reaction.addProduct(product);
    Reactant reactant = new Reactant(protein);
    reactant.setLine(new PolylineData(new Point2D.Double(20, 20), new Point2D.Double(0, 0)));
    reaction.addReactant(reactant);
    model.addReaction(reaction);

    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    String xmlString = parser.model2String(model);

    assertNotNull(xmlString);
    assertTrue(xmlString.contains("omitted"));
    assertTrue(xmlString.contains("re1"));
  }

  @Test
  public void testSpeciesWithSpecialSynonym() throws Exception {
    Model model = new ModelFullIndexed(null);
    model.setIdModel("as");
    model.setWidth(10);
    model.setHeight(10);
    Species protein = new GenericProtein("id1");
    protein.setWidth(10);
    protein.setHeight(10);
    protein.setName("ROS");
    protein.addSynonym("&");
    model.addElement(protein);

    Model model2 = serializeModel(model);

    assertEquals(0, modelComparator.compare(model, model2));
  }

  @Test
  public void testReactionCoordsEqual() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/reaction_coords_different.xml"));

    String xmlString = parser.model2String(model);
    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = parser.createModel(new ConverterParams().inputStream(is));

    Reaction r1 = model.getReactions().iterator().next();
    Reaction r2 = model2.getReactions().iterator().next();

    List<Line2D> lines1 = r1.getLines();
    List<Line2D> lines2 = r2.getLines();
    for (int i = 0; i < lines1.size(); i++) {
      Line2D line1 = lines1.get(i);
      Line2D line2 = lines2.get(i);
      assertEquals("Distance between points too big:" + line1.getP1() + ";" + line2.getP1(), 0,
          line1.getP1().distance(line2.getP1()), Configuration.EPSILON);
      assertEquals("Distance between points too big:" + line1.getP2() + ";" + line2.getP2(), 0,
          line1.getP2().distance(line2.getP2()), Configuration.EPSILON);
    }
  }

  @Test
  public void testParseReactionWithColors() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/reactions/coloring.xml"));

    Reaction r1 = model.getReactionByReactionId("re1");
    Set<Color> colors = new HashSet<>();
    for (AbstractNode node : r1.getNodes()) {
      colors.add(node.getLine().getColor());
    }
    assertEquals("Four different colors were used to draw the reaction", 4, colors.size());
    assertFalse("Black wasn't used in reaction coloring", colors.contains(Color.BLACK));
  }

  @Test
  public void testExportReactionWithColors() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/reactions/coloring.xml"));

    model.setName(null);
    assertNotNull(model);
    String str = parser.model2String(model);
    Model model2 = parser.createModel(
        new ConverterParams().inputStream(new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8))));
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void testParseBooleanReactionWithColors() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/reactions/boolean-colors.xml"));

    Reaction r1 = model.getReactionByReactionId("re1");
    Set<Color> colors = new HashSet<>();
    for (AbstractNode node : r1.getNodes()) {
      colors.add(node.getLine().getColor());
    }
    assertEquals("Three different colors were used to draw the reaction", 3, colors.size());
    assertFalse("Black wasn't used in reaction coloring", colors.contains(Color.BLACK));
  }

  @Test
  public void testExportBooleanReactioWithColors() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/reactions/boolean-colors.xml"));

    model.setName(null);
    assertNotNull(model);
    String str = parser.model2String(model);
    Model model2 = parser.createModel(
        new ConverterParams().inputStream(new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8))));
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void testParseDottedBooleanReaction() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename("testFiles/reactions/boolean-colors.xml"));

    Reaction r1 = model.getReactionByReactionId("re1");
    Set<LineType> reactionTypes = new HashSet<>();
    for (AbstractNode node : r1.getNodes()) {
      reactionTypes.add(node.getLine().getType());
    }
    assertEquals("Whole reaction should use the same reaction type", 1, reactionTypes.size());
  }

  @Test
  public void testExportReactoinWithLinesNotAttachedToSpecies() throws Exception {
    Model model = new ModelFullIndexed(null);
    model.setIdModel("as");
    model.setWidth(1000);
    model.setHeight(1000);

    Species protein = new SimpleMolecule("id1");
    protein.setX(383);
    protein.setY(584);
    protein.setWidth(140);
    protein.setHeight(60);
    model.addElement(protein);

    Species protein2 = new SimpleMolecule("id2");
    protein2.setX(351);
    protein2.setY(697);
    protein2.setWidth(100);
    protein2.setHeight(60);
    model.addElement(protein2);

    Reaction reaction = new StateTransitionReaction();
    reaction.setIdReaction("re1");
    reaction.setLine(new PolylineData(new Point2D.Double(401.0, 673.0), new Point2D.Double(401.0, 673.0)));

    Reactant reactant = new Reactant(protein);
    reactant.setLine(new PolylineData(Arrays.asList(
        new Point2D.Double(420.0, 644.0),
        new Point2D.Double(420.0, 654.0),
        new Point2D.Double(401.0, 654.0),
        new Point2D.Double(401.0, 665.0))));
    reaction.addReactant(reactant);
    model.addReaction(reaction);

    Product product = new Product(protein2);
    product.setLine(new PolylineData(new Point2D.Double(401.0, 673.0), new Point2D.Double(401.0, 697.0)));
    reaction.addProduct(product);

    Model model2 = serializeModel(model);

    Reaction reaction2 = model2.getReactionByReactionId("re1");

    Reactant newReactant = reaction2.getReactants().get(0);

    // center part of the line shouldn't change - edges should be aligned to touch
    // species
    assertEquals(0, newReactant.getLine().getPoints().get(1).distance(reactant.getLine().getPoints().get(1)),
        Configuration.EPSILON);
    assertEquals(0, newReactant.getLine().getPoints().get(2).distance(reactant.getLine().getPoints().get(2)),
        Configuration.EPSILON);
  }

}
