package lcsb.mapviewer.converter.model.celldesigner;

import org.junit.*;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;

public class CellDesignerElementCollectionTest extends CellDesignerTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddTheSameElementTwice() {
    CellDesignerElementCollection collection = new CellDesignerElementCollection();
    CellDesignerComplexSpecies complex = new CellDesignerComplexSpecies();
    complex.setElementId("id");
    collection.addElement(complex);
    collection.addElement(complex);
  }

}
