package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;

public class CellDesignerPointTransformationTest extends CellDesignerTestFunctions {
  Logger logger = LogManager.getLogger(CellDesignerPointTransformationTest.class);

  CellDesignerPointTransformation pt = new CellDesignerPointTransformation();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  /**
   * Check if coordinates are transformed in both ways correctly for this strange
   * transformation with new base consisted of 3 points
   */
  @Test
  public void testConversionOfPointBases() {
    Point2D pointA = new Point2D.Double(1, 2);
    Point2D pointB = new Point2D.Double(10, 12);
    Point2D pointC = new Point2D.Double(4, 3);
    Point2D pointP = new Point2D.Double(0.1, 2);

    Point2D pointO = pt.getCoordinatesInNormalBase(pointA, pointB, pointC, pointP);
    Point2D pointQ = pt.getCoordinatesInCellDesignerBase(pointA, pointB, pointC, pointO);
    assertEquals(0, pointQ.distance(pointP), 1e-6);
  }

  @Test
  public void testNanTransformation() {
    Point2D pointA = new Point2D.Double(11429.886363636364, 11966.818181818182);
    Point2D pointB = new Point2D.Double(11300.886363636364, 12056.363636363636);
    Point2D pointC = new Point2D.Double(11429.886363636364, 12170.272727272728);
    Point2D pointP = new Point2D.Double(11430.886363636364, 12052.045875614034);

    Point2D pointO = pt.getCoordinatesInNormalBase(pointA, pointB, pointC, pointP);
    Point2D pointQ = pt.getCoordinatesInCellDesignerBase(pointA, pointB, pointC, pointO);
    assertTrue(Double.isFinite(pointQ.getX()));
    assertTrue(Double.isFinite(pointQ.getY()));
    assertEquals(0, pointQ.distance(pointP), 1e-6);
  }

}
