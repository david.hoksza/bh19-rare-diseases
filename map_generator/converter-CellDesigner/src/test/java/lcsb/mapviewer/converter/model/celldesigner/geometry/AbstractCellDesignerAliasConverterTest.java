package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.*;
import lcsb.mapviewer.model.map.species.*;

public class AbstractCellDesignerAliasConverterTest extends CellDesignerTestFunctions {
  Logger logger = LogManager.getLogger(AbstractCellDesignerAliasConverterTest.class);

  AbstractCellDesignerAliasConverter<Species> converter;

  @SuppressWarnings("unchecked")
  @Before
  public void setUp() {
    converter = Mockito.spy(AbstractCellDesignerAliasConverter.class);
  }

  @Test
  public void testGetAnchorPointCoordinatesForInvalidImplementation() {
    Protein alias = new GenericProtein("id");
    alias.setWidth(1);
    alias.setHeight(1);
    converter.getAnchorPointCoordinates(alias, 0);
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testGetters() {
    CellDesignerEllipseTransformation ellipseTransformation = new CellDesignerEllipseTransformation();
    LineTransformation lineTransformation = new LineTransformation();
    CellDesignerPolygonTransformation polygonTransformation = new CellDesignerPolygonTransformation();
    CellDesignerRectangleTransformation rectangleTransformation = new CellDesignerRectangleTransformation();
    converter.setEllipseTransformation(ellipseTransformation);
    converter.setLineTransformation(lineTransformation);
    converter.setPolygonTransformation(polygonTransformation);
    converter.setRectangleTransformation(rectangleTransformation);

    assertEquals(ellipseTransformation, converter.getEllipseTransformation());
    assertEquals(lineTransformation, converter.getLineTransformation());
    assertEquals(polygonTransformation, converter.getPolygonTransformation());
    assertEquals(rectangleTransformation, converter.getRectangleTransformation());
  }

}
