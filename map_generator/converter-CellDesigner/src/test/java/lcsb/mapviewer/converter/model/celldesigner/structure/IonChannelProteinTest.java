package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.IonChannelProtein;

public class IonChannelProteinTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerIonChannelProtein());
  }

  @Test
  public void testConstructor() {
    CellDesignerIonChannelProtein species = new CellDesignerIonChannelProtein(
        new CellDesignerSpecies<IonChannelProtein>());
    assertNotNull(species);
  }

  @Test
  public void testCopy1() {
    CellDesignerIonChannelProtein species = new CellDesignerIonChannelProtein(
        new CellDesignerSpecies<IonChannelProtein>()).copy();
    assertNotNull(species);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    CellDesignerIonChannelProtein protein = Mockito.spy(CellDesignerIonChannelProtein.class);
    protein.copy();
  }

}
