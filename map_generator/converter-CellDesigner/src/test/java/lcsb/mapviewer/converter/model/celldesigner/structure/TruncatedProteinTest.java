package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.TruncatedProtein;

public class TruncatedProteinTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerTruncatedProtein());
  }

  @Test
  public void testConstructor() {
    CellDesignerTruncatedProtein species = new CellDesignerTruncatedProtein(
        new CellDesignerSpecies<TruncatedProtein>());
    assertNotNull(species);
  }

  @Test
  public void testCopy1() {
    CellDesignerTruncatedProtein species = new CellDesignerTruncatedProtein(new CellDesignerSpecies<TruncatedProtein>())
        .copy();
    assertNotNull(species);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    CellDesignerTruncatedProtein protein = Mockito.spy(CellDesignerTruncatedProtein.class);
    protein.copy();
  }

}
