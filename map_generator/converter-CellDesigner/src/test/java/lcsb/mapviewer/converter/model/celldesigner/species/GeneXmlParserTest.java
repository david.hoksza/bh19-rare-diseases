package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.*;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGene;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;

public class GeneXmlParserTest extends CellDesignerTestFunctions {
  Logger logger = LogManager.getLogger(GeneXmlParserTest.class.getName());

  GeneXmlParser geneParser;
  String testGeneFile = "testFiles/xmlNodeTestExamples/gene.xml";

  CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    geneParser = new GeneXmlParser(elements);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlSpecies() throws Exception {
    String xmlString = readFile(testGeneFile);
    Pair<String, CellDesignerGene> result = geneParser.parseXmlElement(xmlString);
    CellDesignerGene gene = result.getRight();
    assertEquals("gn3", result.getLeft());
    assertEquals("BCL6", gene.getName());
    assertTrue(gene.getNotes().contains("B-cell CLL/lymphoma 6"));
  }

  @Test
  public void testToXml() throws Exception {
    String xmlString = readFile(testGeneFile);
    Pair<String, CellDesignerGene> result = geneParser.parseXmlElement(xmlString);
    CellDesignerGene gene = result.getRight();
    String transformedXml = geneParser.toXml(gene.createModelElement());
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("celldesigner:gene", root.item(0).getNodeName());

    Pair<String, CellDesignerGene> result2 = geneParser.parseXmlElement(geneParser.toXml(gene.createModelElement()));
    CellDesignerGene gene2 = result2.getRight();
    assertEquals(gene.getName(), gene2.getName());
    assertTrue(gene2.getNotes().trim().contains(gene.getNotes().trim()));
  }

  @Test
  public void testParsePhosphorylatedGene() throws Exception {
    Model model = getModelForFile("testFiles/problematic/phosphorylated_gene.xml");
    Gene gene = model.getElementByElementId("sa1");
    assertEquals(1, gene.getModificationResidues().size());
    ModificationSite residue = (ModificationSite) gene.getModificationResidues().get(0);
    assertEquals(ModificationState.PHOSPHORYLATED, residue.getState());
    assertEquals("some name", residue.getName());

    // check xml transformation
    String xml = geneParser.toXml(gene);
    assertNotNull(xml);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidGeneInput() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_gene_1.xml");
    geneParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidGeneInput2() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_gene_2.xml");
    geneParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidModificationResidue() throws Exception {
    String xmlString = readFile("testFiles/invalid/invalid_modification_residue.xml");
    geneParser.getModificationResidue(getNodeFromXmlString(xmlString));
  }

}
