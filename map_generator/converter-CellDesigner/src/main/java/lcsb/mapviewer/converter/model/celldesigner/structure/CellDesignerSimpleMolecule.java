package lcsb.mapviewer.converter.model.celldesigner.structure;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.SimpleMolecule;

/**
 * Class representing CellDesigner {@link SimpleMolecule}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerSimpleMolecule extends CellDesignerChemical<SimpleMolecule> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(CellDesignerSimpleMolecule.class.getName());

  /**
   * Constructor that initializes molecule with the data passed in the argument.
   * 
   * @param species
   *          original species used for data initialization
   */
  public CellDesignerSimpleMolecule(CellDesignerSpecies<?> species) {
    super(species);
  }

  /**
   * Default constructor.
   */
  public CellDesignerSimpleMolecule() {
  }

  @Override
  public CellDesignerSimpleMolecule copy() {
    if (this.getClass() == CellDesignerSimpleMolecule.class) {
      return new CellDesignerSimpleMolecule(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public SimpleMolecule createModelElement(String aliasId) {
    SimpleMolecule result = new SimpleMolecule(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

}
