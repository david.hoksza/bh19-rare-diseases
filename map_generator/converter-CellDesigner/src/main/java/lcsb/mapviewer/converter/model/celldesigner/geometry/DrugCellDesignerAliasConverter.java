package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.*;
import java.awt.geom.*;
import java.util.ArrayList;

import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class that provides CellDesigner specific graphical information for Drug.
 * It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class DrugCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<Species> {

  /**
   * How big should be the arc in rectangle for drug representation.
   */
  private static final int RECTANGLE_CORNER_ARC_SIZE = 40;

  /**
   * Default constructor.
   *
   * @param sbgn
   *          Should the converter use sbgn standard
   */
  protected DrugCellDesignerAliasConverter(boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(Species alias, CellDesignerAnchor anchor) {
    if (invalidAnchorPosition(alias, anchor)) {
      return alias.getCenter();
    }
    ArrayList<Point2D> list = getDrugPoints(alias);

    return getPolygonTransformation().getPointOnPolygonByAnchor(list, anchor);
  }

  @Override
  protected PathIterator getBoundPathIterator(Species alias) {
    return getDrugShape(alias).getPathIterator(new AffineTransform());
  }

  /**
   * Returns shape of the Drug as a list of points.
   * 
   * @param alias
   *          alias for which we are looking for a border
   * @return list of points defining border of the given alias
   */
  protected ArrayList<Point2D> getDrugPoints(Species alias) {
    ArrayList<Point2D> list = new ArrayList<Point2D>();

    double x = alias.getX();
    double y = alias.getY();
    double width = alias.getWidth();
    double height = alias.getHeight();

    // CHECKSTYLE:OFF
    list.add(new Point2D.Double(x, y + height / 2));
    list.add(new Point2D.Double(x + width / 12, y));
    list.add(new Point2D.Double(x + width / 2, y));
    list.add(new Point2D.Double(x + width * 11 / 12, y));
    list.add(new Point2D.Double(x + width, y + height / 2));
    list.add(new Point2D.Double(x + width * 11 / 12, y + height));
    list.add(new Point2D.Double(x + width / 2, y + height));
    list.add(new Point2D.Double(x + width / 12, y + height));
    // CHECKSTYLE:ON
    return list;
  }

  /**
   * Returns shape of the Drug.
   * 
   * @param alias
   *          alias for which we are looking for a border
   * @return Shape object defining given alias
   */
  private Shape getDrugShape(Element alias) {
    return new RoundRectangle2D.Double(alias.getX(), alias.getY(), alias.getWidth(), alias.getHeight(),
        RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE);
  }

}
