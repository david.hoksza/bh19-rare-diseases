package lcsb.mapviewer.converter.model.celldesigner.structure;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class representing CellDesigner {@link Complex}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerComplexSpecies extends CellDesignerSpecies<Complex> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(CellDesignerComplexSpecies.class.getName());

  /**
   * Elements that exists in this complex.
   */
  private Set<CellDesignerElement<?>> elements = new HashSet<>();

  /**
   * State of the complex species.
   */
  private String structuralState = null;

  private Double structuralStateAngle = null;

  /**
   * Default constructor.
   */
  public CellDesignerComplexSpecies() {
    super();
  }

  /**
   * Constructor that initialize the complex with the data from the object passed
   * as an argument.
   * 
   * @param original
   *          object used for initializing data
   */
  public CellDesignerComplexSpecies(CellDesignerSpecies<?> original) {
    super(original);
    if (original instanceof CellDesignerComplexSpecies) {
      CellDesignerComplexSpecies complex = (CellDesignerComplexSpecies) original;
      structuralState = complex.getStructuralState();
      for (CellDesignerElement<?> element : complex.getElements()) {
        addElement(element.copy());
      }
    }
  }

  /**
   * Constructor with species id.
   * 
   * @param complexId
   *          {@link CellDesignerSpecies#idSpecies}
   */
  public CellDesignerComplexSpecies(String complexId) {
    super(complexId);
  }

  @Override
  public CellDesignerComplexSpecies copy() {
    if (this.getClass() == CellDesignerComplexSpecies.class) {
      return new CellDesignerComplexSpecies(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public Complex createModelElement(String aliasId) {
    Complex result = new Complex(aliasId);
    super.setModelObjectFields(result);

    if (elements.size() > 0) {
      throw new NotImplementedException();
    }

    return result;
  }

  @Override
  public void updateModelElementAfterLayoutAdded(Species element) {
    ((Complex) element).setStructuralState(createStructuralState(element, structuralState, structuralStateAngle));
  }

  /**
   * Adds element to the complex.
   *
   * @param element
   *          object to add
   */
  public void addElement(CellDesignerElement<?> element) {
    if (element instanceof CellDesignerSpecies) {
      for (CellDesignerElement<?> el : elements) {
        if (el instanceof CellDesignerSpecies) {
          if (el.getElementId().equals(element.getElementId())) {
            throw new InvalidArgumentException("Cannot add two species with the same id: " + el.getElementId());
          }
        }
      }
    }
    this.elements.add(element);
  }

  /**
   * Returns list of all elements in the complex. Also subelements are included.
   *
   * @return list of all elements in the complex.
   */
  public Set<CellDesignerSpecies<?>> getAllSimpleChildren() {
    Set<CellDesignerSpecies<?>> result = new HashSet<>();
    for (CellDesignerElement<?> element : getElements()) {
      if (element instanceof CellDesignerComplexSpecies) {
        result.addAll(((CellDesignerComplexSpecies) element).getAllSimpleChildren());
      } else if (element instanceof CellDesignerSpecies) {
        result.add((CellDesignerSpecies<?>) element);
      }
    }
    return result;
  }

  /**
   * @return the structuralState
   * @see #structuralState
   */
  public String getStructuralState() {
    return structuralState;
  }

  /**
   * @param structuralState
   *          the structuralState to set
   * @see #structuralState
   */
  public void setStructuralState(String structuralState) {
    if (this.structuralState != null && !this.structuralState.equals("")
        && !this.structuralState.equals(structuralState)) {
      logger.warn("replacing structural state, Old: " + this.structuralState + " into new: " + structuralState);
    }
    this.structuralState = structuralState;
  }

  /**
   * @return the elements
   * @see #elements
   */
  public Set<CellDesignerElement<?>> getElements() {
    return elements;
  }

  /**
   * @param elements
   *          the elements to set
   * @see #elements
   */
  public void setElements(Set<CellDesignerElement<?>> elements) {
    this.elements = elements;
  }

  public Double getStructuralStateAngle() {
    return structuralStateAngle;
  }

  public void setStructuralStateAngle(Double structuralStateAngle) {
    this.structuralStateAngle = structuralStateAngle;
  }

}
