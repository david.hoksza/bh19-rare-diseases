package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This class is designed to obtain CellDesigner specific data from
 * {@link Element}.
 * 
 * @author Piotr Gawron
 * 
 */

public class CellDesignerAliasConverter implements ICellDesignerAliasConverter<Element> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(CellDesignerAliasConverter.class);

  /**
   * Class helping with transforming objects into meaningful identifiers.
   */
  private static ElementUtils eu = new ElementUtils();
  /**
   * Converter used for operations on the {@link Element} given in constructor.
   */
  @SuppressWarnings("rawtypes")
  private ICellDesignerAliasConverter converter = null;

  /**
   * Default constructor.
   *
   * @param sbgn
   *          Should the converter use SBGN standard
   * @param element
   *          element for which this converter will be used
   */
  public CellDesignerAliasConverter(Element element, boolean sbgn) {
    converter = getConverterForAlias(element, sbgn);
  }

  /**
   * Returns a converter for given {@link Element}. If converter doesn't exist
   * exception is thrown.
   *
   * @param element
   *          element for which we are looking for a converter
   * @param sbgn
   *          Should the converter use SBGN standard
   * @return converter that can be applied for the given element
   */
  private ICellDesignerAliasConverter<? extends Element> getConverterForAlias(Element element, boolean sbgn) {
    if (element == null) {
      throw new InvalidArgumentException("element cannot be null");
    }
    if (element instanceof Protein) {
      return new ProteinCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Degraded) {
      return new DegradedCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Complex) {
      return new ComplexCellDesignerAliasConverter(sbgn);
    } else if (element instanceof SimpleMolecule) {
      return new SimpleMoleculeCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Drug) {
      return new DrugCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Ion) {
      return new IonCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Phenotype) {
      return new PhenotypeCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Rna) {
      return new RnaCellDesignerAliasConverter(sbgn);
    } else if (element instanceof AntisenseRna) {
      return new AntisenseRnaCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Gene) {
      return new GeneCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Unknown) {
      return new UnknownCellDesignerAliasConverter(sbgn);
    } else {
      throw new NotImplementedException(eu.getElementTag(element) + "Unknown converter for class");
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public CellDesignerAnchor getAnchorForCoordinates(Element element, Point2D point) {
    return converter.getAnchorForCoordinates(element, point);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Point2D getPointCoordinates(Element element, CellDesignerAnchor anchor) {
    return converter.getPointCoordinates(element, anchor);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Point2D getAnchorPointCoordinates(Element element, CellDesignerAnchor anchor, PolylineData line) {
    return converter.getAnchorPointCoordinates(element, anchor, line);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Point2D getResidueCoordinates(Element species, double angle) {
    return converter.getResidueCoordinates(species, angle);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Double getAngleForPoint(Element element, Point2D position) {
    return converter.getAngleForPoint(element, position);
  }

  @Override
  public Double getCellDesignerPositionByCoordinates(ModificationResidue mr) {
    return converter.getCellDesignerPositionByCoordinates(mr);
  }

  @Override
  public Point2D getCoordinatesByPosition(Element element, Double pos) {
    return converter.getCoordinatesByPosition(element, pos);
  }

  @Override
  public Point2D getCoordinatesByPosition(Element element, Double pos, Double width) {
    return converter.getCoordinatesByPosition(element, pos, width);
  }

  @Override
  public Double getCellDesignerSize(ModificationResidue mr) {
    return converter.getCellDesignerSize(mr);
  }

  @Override
  public Double getWidthBySize(Element element, Double size) {
    return converter.getWidthBySize(element, size);
  }

}
