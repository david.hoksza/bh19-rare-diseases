package lcsb.mapviewer.converter.model.celldesigner.species;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;

/**
 * This collection performs some magic, so please try no to understand it :). It
 * is required by the CellDEsigner xml schema. It helps to update and clone
 * properly species elements during parsing CellDesigner map, because definition
 * of species is split between three different places in the xml and in every
 * place almost any subset of information about species can appear. However some
 * of these information should appear in more than one copy of the element...
 * 
 * @author Piotr Gawron
 * 
 * @param <C>
 *          class of the species
 */
public class SpeciesCollection<C extends CellDesignerSpecies<?>> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(SpeciesCollection.class.getName());

  /**
   * List of all elements added to this collection.
   */
  private ArrayList<C> allElements;

  /**
   * Map of species identified by specific identifier of the species (like
   * {@link lcsb.mapviewer.db.model.map.species.CellDesignerProtein#proteinId
   * Protein#proteinId}.
   */
  private Map<String, ArrayList<C>> speciesListByLocalId;

  /**
   * Default Constructor. Initializes collection. Checks if method signature is
   * proper.
   * 
   */
  public SpeciesCollection() {
    allElements = new ArrayList<C>();
    speciesListByLocalId = new HashMap<String, ArrayList<C>>();
  }

  /**
   * Adds element to collection.
   * 
   * @param localId
   *          celldesigner identifier
   * @param element
   *          object to add
   */
  public void add(C element, String localId) {
    ArrayList<C> list = speciesListByLocalId.get(localId);
    if (list == null) {
      list = new ArrayList<C>();
      speciesListByLocalId.put(localId, list);
    }
    list.add(element);
    allElements.add(element);
  }

  /**
   * Returns list of species identifierd by local species specific identifier.
   * 
   * @param localId
   *          identifier of this species will be used to return the list
   * @return list of species identifierd by local species specific identifier
   */
  public ArrayList<C> getSpeciesListByLocalId(String localId) {
    return speciesListByLocalId.get(localId);
  }

  /**
   * This method is magic :). It updates information in every element in the
   * collection when the information should be propagated. And add species to the
   * collection when necessary.
   * 
   * @param sp
   *          data from this species should be propagated to other species
   * @param identifier
   *          celldesigner identifier that determine which elements should be
   *          modified
   */
  public void updateSpeciesByLocalId(C sp, String identifier) {
    C result = null;

    ArrayList<C> list = getSpeciesListByLocalId(identifier);
    if (list == null || list.size() == 0) {
      result = sp;
      add(sp, identifier);
    } else {
      ArrayList<C> toRemove = new ArrayList<C>();
      ArrayList<C> toAdd = new ArrayList<C>();
      for (C c : list) {
        // propagate the data only when idSpecies is not set or the idSpecies is
        // equlas to the one from which we updating data
        if (c.getElementId().equals("") || c.getElementId().equals(sp.getElementId())) {
          c.update(sp);
          if (c.getClass() != sp.getClass() && c.getClass().isAssignableFrom(sp.getClass())) {
            toRemove.add(c);
            C newInstance = createNewInstance(sp.getClass(), c);
            toAdd.add(newInstance);
            result = newInstance;
          } else {
            result = c;
          }
        } else if (sp.getElementId().equals("")) {
          c.update(sp);
          if (c.getClass() != sp.getClass() && c.getClass().isAssignableFrom(sp.getClass())) {
            toRemove.add(c);
            C newInstance = createNewInstance(sp.getClass(), c);
            toAdd.add(newInstance);
            result = newInstance;
          } else {
            result = c;
          }
        }
      }
      list.removeAll(toRemove);
      list.addAll(toAdd);
      allElements.removeAll(toRemove);
      allElements.addAll(toAdd);

      // if we haven't found any object to update then probably we have to add
      // species to the collection
      if (result == null) {
        result = sp;
        add(result, identifier);
      }
    }
  }

  /**
   * Creates a new instance of the object defined by clazz parameter and
   * initialized by c parameter.
   * 
   * @param clazz
   *          class of the object
   * @param c
   *          original object and param of the constructor
   * @return new instance of the object
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  protected C createNewInstance(Class clazz, C c) {
    try {
      return (C) clazz.getConstructor(CellDesignerSpecies.class).newInstance(c);
    } catch (Exception e) {
      throw new InvalidStateException("Problem with creating object", e);
    }
  }

  /**
   * Returns all elements from collection.
   * 
   * @return collection with all elements
   */
  public Collection<C> getAll() {
    return allElements;
  }

}
