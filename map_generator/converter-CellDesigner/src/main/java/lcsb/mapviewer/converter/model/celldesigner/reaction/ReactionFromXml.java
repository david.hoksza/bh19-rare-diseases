package lcsb.mapviewer.converter.model.celldesigner.reaction;

import java.awt.geom.Point2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.*;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.geometry.ReactionCellDesignerConverter;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.*;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.*;
import lcsb.mapviewer.converter.model.celldesigner.types.*;
import lcsb.mapviewer.model.graphics.*;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.type.*;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This is a part of {@link ReactionXmlParser} class functionality that allows
 * to read reaction from CellDesigner xml node.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionFromXml {

  /**
   * Reactant lines in cell designer ends in the 2/5 of the center line.
   */
  private static final double REACTANT_END_RATIO = 0.4;

  /**
   * Product lines in cell designer starts in the 3/5 of the center line.
   */
  private static final double PRODUCT_START_RATIO = 0.6;
  CellDesignerElementCollection elements;
  /**
   * Stores information about {@link CellDesignerAnchor} for a node.
   */
  private Map<ReactionNode, CellDesignerAnchor> anchorsByNodes = new HashMap<>();
  /**
   * Stores information about operator type that should be used for a modifier
   * node.
   */
  private Map<ReactionNode, String> typeByModifier = new HashMap<>();
  /**
   * Stores information to which point on the central rectangle modifier should be
   * connected.
   */
  private Map<ReactionNode, String> lineTypeByModifier = new HashMap<>();
  /**
   * Stores information about list of points that create line describing modifier.
   */
  private Map<ReactionNode, List<Point2D>> pointsByModifier = new HashMap<>();
  /**
   * Stores information about list of points that create line describing modifier.
   */
  private Map<ReactionNode, LineProperties> linePropertiesByModifier = new HashMap<>();
  /**
   * Helps to determine if the key Modifier should be treats as part of
   * NodeOperator (value in the map).
   */
  private Map<Modifier, Modifier> modifierParentOperator = new HashMap<>();
  /**
   * Identifies central line segment in {@link TwoProductReactionInterface} and
   * {@link TwoReactantReactionInterface} reactions.
   */
  private Map<ReactionNode, Integer> indexByComplexReaction = new HashMap<>();
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ReactionXmlParser.class.getName());
  /**
   * Xml parser used for processing notes into structured data.
   */
  private RestAnnotationParser rap = new RestAnnotationParser();
  /**
   * Helper object used for manipulation on the point coordinates in CellDesigner
   * format.
   */
  private CellDesignerPointTransformation pointTransformation = new CellDesignerPointTransformation();
  /**
   * Should SBGN standard be used.
   */
  private boolean sbgn;

  private boolean oldCellDesignerVersion;

  /**
   * Default constructor.
   * 
   * @param sbgn
   *          Should the converter use SBGN standard
   */
  public ReactionFromXml(CellDesignerElementCollection elements, boolean sbgn, boolean oldCellDesignerVersion) {
    this.elements = elements;
    this.sbgn = sbgn;
    this.oldCellDesignerVersion = oldCellDesignerVersion;
  }

  /**
   * Returns {@link Reaction} object from CellDesigner xml node.
   * 
   * @param reactionNode
   *          xml node
   * @param model
   *          model where the reaction is placed
   * @return reaction from xml node
   * @throws ReactionParserException
   *           thrown when the xml is invalid
   * @throws InvalidXmlSchemaException
   *           thrown when reactionNode is invalid xml
   */
  public Reaction getReaction(Node reactionNode, Model model) throws ReactionParserException {
    Reaction result = new Reaction();
    // we ignore metaid - it's useless and obstruct data model
    // result.setMetaId(XmlParser.getNodeAttr("metaid", reactionNode));
    result.setIdReaction(XmlParser.getNodeAttr("id", reactionNode));
    result.setName(XmlParser.getNodeAttr("name", reactionNode));
    // by default this value is true...
    result.setReversible(!(XmlParser.getNodeAttr("reversible", reactionNode).equalsIgnoreCase("false")));

    NodeList nodes = reactionNode.getChildNodes();
    Node annotationNode = null;
    Node kineticsNode = null;
    Node reactantsNode = null;
    Node productsNode = null;
    Node modifiersNode = null;
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("annotation")) {
          annotationNode = node;
        } else if (node.getNodeName().equalsIgnoreCase("listOfReactants")) {
          reactantsNode = node;
        } else if (node.getNodeName().equalsIgnoreCase("listOfProducts")) {
          productsNode = node;
        } else if (node.getNodeName().equalsIgnoreCase("listOfModifiers")) {
          modifiersNode = node;
        } else if (node.getNodeName().equalsIgnoreCase("notes")) {
          rap.processNotes(rap.getNotes(node), result);
        } else if (node.getNodeName().equalsIgnoreCase("kineticLaw")) {
          kineticsNode = node;
        } else {
          throw new ReactionParserException("Unknown element of reaction: " + node.getNodeName(), result);
        }
      }
    }
    if (annotationNode != null) {
      result = parseReactionAnnotation(annotationNode, result, model);
    } else {
      throw new ReactionParserException("No annotation node in reaction", result);
    }
    Map<String, Element> elements = getSpeciesIdToElementMappingFromAnnotationNode(annotationNode, model);
    if (kineticsNode != null) {
      KineticsXmlParser kineticsParser = new KineticsXmlParser(model);
      try {
        result.setKinetics(kineticsParser.parseKinetics(kineticsNode, elements));
      } catch (InvalidXmlSchemaException e) {
        throw new ReactionParserException(result, e);
      }
    }
    try {
      assignStochiometry(result.getReactants(), reactantsNode, elements);
      assignStochiometry(result.getProducts(), productsNode, elements);
      assignStochiometry(result.getModifiers(), modifiersNode, elements);
    } catch (InvalidStateException e) {
      throw new ReactionParserException(result, e);
    }
    return result;
  }

  private void assignStochiometry(Collection<? extends ReactionNode> reactionNodes, Node nodes,
      Map<String, Element> speciesIdToElement) {
    if (nodes != null) {
      for (Node child : XmlParser.getNodes("speciesReference", nodes.getChildNodes())) {
        String param = XmlParser.getNodeAttr("stoichiometry", child);
        if (param != null && !param.isEmpty()) {
          Double stoichometry = Double.parseDouble(param);
          String speciesId = XmlParser.getNodeAttr("species", child);
          Element e = speciesIdToElement.get(speciesId);
          boolean assigned = false;
          for (ReactionNode reactionNode : reactionNodes) {
            if (reactionNode.getElement().equals(e)) {
              assigned = true;
              reactionNode.setStoichiometry(stoichometry);
            }
          }
          if (!assigned) {
            throw new InvalidStateException("Cannot assign stoichiometry for element: " + speciesId);
          }
        }
      }
    }
  }

  private Map<String, Element> getSpeciesIdToElementMappingFromAnnotationNode(Node annotationNode, Model model) {
    Map<String, Element> result = new HashMap<>();

    List<Node> elementNodes = new ArrayList<>();
    elementNodes.addAll(XmlParser.getAllNotNecessirellyDirectChild("celldesigner:baseReactant", annotationNode));
    elementNodes.addAll(XmlParser.getAllNotNecessirellyDirectChild("celldesigner:baseProduct", annotationNode));
    elementNodes.addAll(XmlParser.getAllNotNecessirellyDirectChild("celldesigner:linkTarget", annotationNode));

    for (Node node : elementNodes) {
      String speciesId = XmlParser.getNodeAttr("species", node);
      String aliasId = XmlParser.getNodeAttr("alias", node);
      addElementMapping(model, result, speciesId, aliasId);
    }

    for (Node node : XmlParser.getAllNotNecessirellyDirectChild("celldesigner:reactantLink", annotationNode)) {
      String speciesId = XmlParser.getNodeAttr("reactant", node);
      String aliasId = XmlParser.getNodeAttr("alias", node);
      addElementMapping(model, result, speciesId, aliasId);
    }

    for (Node node : XmlParser.getAllNotNecessirellyDirectChild("celldesigner:productLink", annotationNode)) {
      String speciesId = XmlParser.getNodeAttr("product", node);
      String aliasId = XmlParser.getNodeAttr("alias", node);
      addElementMapping(model, result, speciesId, aliasId);
    }
    for (Node node : XmlParser.getAllNotNecessirellyDirectChild("celldesigner:modification", annotationNode)) {
      String speciesId = XmlParser.getNodeAttr("modifiers", node);
      String aliasId = XmlParser.getNodeAttr("aliases", node);
      addElementMapping(model, result, speciesId, aliasId);
    }

    return result;
  }

  private void addElementMapping(Model model, Map<String, Element> result, String speciesId, String aliasId) {
    String[] aliasIds = aliasId.split(",");
    String[] speciesIds = speciesId.split(",");
    for (int i = 0; i < aliasIds.length; i++) {

      Element element = model.getElementByElementId(aliasIds[i]);
      result.put(speciesIds[i], element);
      addCompartmentMapping(result, element);
    }
  }

  private void addCompartmentMapping(Map<String, Element> result, Element element) {
    Compartment compartment = element.getCompartment();
    if (compartment != null) {
      // in kinetics we can have reference to compartment (so we need to find SBML
      // compartment id)
      String compartmentId = elements.getElementByElementId(compartment.getElementId()).getElementId();
      result.put(compartmentId, compartment);
    }
  }

  /**
   * Parses reaction annotation node and update reaction.
   * 
   * @param annotationNode
   *          xml node
   * @param result
   *          reaction to update
   * @param model
   *          model where reaction is placed
   * @return updated reaction
   * @throws ReactionParserException
   *           thrown when the xml is invalid
   * @throws InvalidXmlSchemaException
   *           thrown when annotationNode is invalid xml
   */
  private Reaction parseReactionAnnotation(Node annotationNode, Reaction result, Model model)
      throws ReactionParserException {
    NodeList nodes = annotationNode.getChildNodes();
    if (oldCellDesignerVersion) {
      result = parseReactionExtension(result, annotationNode, model);
    }
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:extension")) {
          result = parseReactionExtension(result, node, model);
        } else if (node.getNodeName().equalsIgnoreCase("rdf:RDF")) {
          try {
            XmlAnnotationParser xmlParser = new XmlAnnotationParser();
            result.addMiriamData(xmlParser.parseRdfNode(node));
          } catch (InvalidXmlSchemaException e) {
            throw new ReactionParserException("Problem with parsing RDF", result, e);
          }
        } else if (!oldCellDesignerVersion) {
          throw new ReactionParserException("Unknown element of reaction/annotation: " + node.getNodeName(), result);
        }
      }
    }

    return result;
  }

  /**
   * Parses CellDesigner extension to sbml reaction node and updates reaction.
   * 
   * @param result
   *          reaction to update
   * @param node
   *          xml node
   * @param model
   *          model where reaction is placed
   * @return updated reaction
   * @throws ReactionParserException
   *           thrown when the xml is invalid and reason is more specific
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   * 
   */
  private Reaction parseReactionExtension(Reaction result, Node node, Model model) throws ReactionParserException {
    try {
      NodeList extensionNodes = node.getChildNodes();
      LineProperties line = null;
      ConnectScheme connectScheme = null;
      EditPoints points = null;
      Node reactantsLinkNode = null;
      Node productsLinkNode = null;
      Node modifiersLinkNode = null;
      Node gateMembers = null;
      String type = null;
      String booleanLogicGateType = null;
      for (int y = 0; y < extensionNodes.getLength(); y++) {
        Node nodeReaction = extensionNodes.item(y);
        if (nodeReaction.getNodeType() == Node.ELEMENT_NODE) {
          if (nodeReaction.getNodeName().equalsIgnoreCase("celldesigner:reactionType")) {
            type = XmlParser.getNodeValue(nodeReaction);
          } else if (nodeReaction.getNodeName().equalsIgnoreCase("celldesigner:baseReactants")) {
            NodeList reactantsNodes = nodeReaction.getChildNodes();
            for (int z = 0; z < reactantsNodes.getLength(); z++) {
              Node reactantNode = reactantsNodes.item(z);
              if (reactantNode.getNodeType() == Node.ELEMENT_NODE) {
                if (reactantNode.getNodeName().equalsIgnoreCase("celldesigner:baseReactant")) {
                  result.addReactant(parseBaseReactant(reactantNode, model));
                } else {
                  throw new ReactionParserException(
                      "Unknown element of celldesigner:baseReactants: " + node.getNodeName(), result);
                }
              }
            }
          } else if (nodeReaction.getNodeName().equalsIgnoreCase("celldesigner:baseProducts")) {
            NodeList reactantsNodes = nodeReaction.getChildNodes();
            for (int z = 0; z < reactantsNodes.getLength(); z++) {
              Node reactantNode = reactantsNodes.item(z);
              if (reactantNode.getNodeType() == Node.ELEMENT_NODE) {
                if (reactantNode.getNodeName().equalsIgnoreCase("celldesigner:baseProduct")) {
                  result.addProduct(parseBaseProduct(model, reactantNode));
                } else {
                  throw new ReactionParserException(
                      "Unknown element of celldesigner:baseProducts: " + node.getNodeName(), result);
                }
              }
            }
          } else if (nodeReaction.getNodeName().equalsIgnoreCase("celldesigner:line")) {
            line = getLineProperties(nodeReaction);
          } else if (nodeReaction.getNodeName().equalsIgnoreCase("celldesigner:connectScheme")) {
            connectScheme = parseConnectScheme(nodeReaction);
          } else if (nodeReaction.getNodeName().equalsIgnoreCase("celldesigner:listOfReactantLinks")) {
            reactantsLinkNode = nodeReaction;
          } else if (nodeReaction.getNodeName().equalsIgnoreCase("celldesigner:listOfProductLinks")) {
            productsLinkNode = nodeReaction;
          } else if (nodeReaction.getNodeName().equalsIgnoreCase("celldesigner:listOfModification")) {
            modifiersLinkNode = nodeReaction;
          } else if (nodeReaction.getNodeName().equalsIgnoreCase("celldesigner:editPoints")) {
            points = parseEditPoints(nodeReaction);
          } else if (nodeReaction.getNodeName().equalsIgnoreCase("celldesigner:name")) {
            result.setName(XmlParser.getNodeValue(nodeReaction));
          } else if (nodeReaction.getNodeName().equalsIgnoreCase("celldesigner:listOfGateMember")) {
            gateMembers = nodeReaction;
          } else {
            throw new ReactionParserException(
                "Unknown element of reaction/celldesigner:extension: " + nodeReaction.getNodeName(), result);
          }
        }
      }
      if (gateMembers != null) {
        points = gateMembersToPoints(result, gateMembers);
        booleanLogicGateType = type;
        type = gateMembersToReactionType(gateMembers);
      }
      result = createProperTypeReaction(type, result);
      if (connectScheme == null) {
        throw new ReactionParserException("No connectScheme node", result);
      }
      if (points == null) {
        points = new EditPoints();
      }
      if (result instanceof TwoReactantReactionInterface || booleanLogicGateType != null) {
        createLinesForTwoReactantReaction(result, points);
        createOperatorsForTwoReactantReaction(result, gateMembers);

      } else if (result instanceof SimpleReactionInterface) {
        createLinesForSimpleReaction(result, points, connectScheme);
      } else if (result instanceof TwoProductReactionInterface) {
        createLinesForTwoProductReaction(result, points);
        createOperatorsForTwoProductReaction(result);
      } else {
        throw new ReactionParserException(
            "Problem with parsing lines. Unknown reaction: " + type + "; " + result.getClass().getName(), result);
      }

      for (AbstractNode rNode : result.getNodes()) {
        applyStylingToLine(line, rNode.getLine());
      }
      applyStylingToLine(line, result.getLine());

      if (booleanLogicGateType != null) {
        assignColorFromGateMembers(result, gateMembers);
      }
      if (reactantsLinkNode != null) {
        parseReactantLinks(result, reactantsLinkNode, model);
      }
      if (productsLinkNode != null) {
        parseProductLinks(result, productsLinkNode, model);
      }

      // create operators
      createOperators(result);

      // now try to create modifiers (we must have set fixed layout data for the
      // core of the reaction)
      if (modifiersLinkNode != null) {
        parseReactionModification(result, modifiersLinkNode, model);
      }
      for (int i = 0; i < result.getModifiers().size(); i++) {
        Modifier modifier = result.getModifiers().get(i);
        if (modifier.getElement() == null) {
          List<Modifier> modifiers = new ArrayList<>();
          modifiers.add(modifier);
          for (Modifier modifier2 : result.getModifiers()) {
            if (modifierParentOperator.get(modifier2) == modifier) {
              modifiers.add(modifier2);
            }
          }
          computeLineForModification(result, modifiers);
          createOperatorFromModifiers(result, modifiers);
          result.removeModifier(modifier);
          i--;
        } else if (modifier.getLine() == null) {
          createLineForModifier(modifier);
        }
      }
      if (result.isReversible()) {
        for (Reactant reactant : result.getReactants()) {
          reactant.getLine().getBeginAtd()
              .setArrowType(result.getProducts().get(0).getLine().getEndAtd().getArrowType());
        }
      }

      return result;
    } catch (InvalidXmlSchemaException e) {
      throw new ReactionParserException(result, e);
    }
  }

  private String gateMembersToReactionType(Node gateMembers) {
    String result = null;
    for (int y = 0; y < gateMembers.getChildNodes().getLength(); y++) {
      Node node = gateMembers.getChildNodes().item(y);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:GateMember")) {
          if (!XmlParser.getNodeAttr("modificationType", node).isEmpty()) {
            result = XmlParser.getNodeAttr("modificationType", node);
          }
        }
      }
    }
    return result;
  }

  private void applyStylingToLine(LineProperties style, PolylineData line) {
    if (style != null) {
      line.setWidth(style.getWidth());
      line.setColor(style.getColor());
    }
  }

  /**
   * Creates reaction with a new type.
   * 
   * @param type
   *          CellDesigner type of a reaction
   * @param result
   *          initial data
   * @return reaction with a new type
   * @throws ReactionParserException
   */
  Reaction createProperTypeReaction(String type, Reaction result) throws ReactionParserException {
    ReactionLineData rdl = ReactionLineData.getByCellDesignerString(type);
    if (rdl == null) {
      throw new ReactionParserException("Unknown CellDesigner class type: " + type + ".", result);
    }
    return rdl.createReaction(result);
  }

  /**
   * Creates CellDesigner {@link EditPoints} structure for gate members node.
   * 
   * @param gateMembers
   *          xml node
   * @return {@link EditPoints} structure representing line information for the
   *         xml node
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   * @throws ReactionParserException
   */
  private EditPoints gateMembersToPoints(Reaction reaction, Node gateMembers)
      throws InvalidXmlSchemaException, ReactionParserException {
    Node lastMember = null;
    EditPoints result = new EditPoints();
    for (int i = 0; i < gateMembers.getChildNodes().getLength(); i++) {
      Node child = gateMembers.getChildNodes().item(i);
      if (child.getNodeType() == Node.ELEMENT_NODE) {

        if (child.getNodeName().equalsIgnoreCase("celldesigner:GateMember")) {
          String type = XmlParser.getNodeAttr("type", child);
          if (type.startsWith("BOOLEAN_LOGIC_GATE")) {
            lastMember = child;
          } else {
            String pointsString = XmlParser.getNodeAttr("editPoints", child);
            List<Point2D> points = parseEditPointsString(pointsString);
            result.addNum(points.size());
            result.getPoints().addAll(points);

            ReactionNode reactionNode = null;
            String elementId = XmlParser.getNodeAttr("aliases", child);
            for (ReactionNode rn : reaction.getReactionNodes()) {
              if (rn.getElement().getElementId().equals(elementId)) {
                reactionNode = rn;
              }
            }
            if (reactionNode == null) {
              throw new ReactionParserException("Cannot find node for: " + elementId, reaction);
            }

            CellDesignerAnchor anchor = getAnchorFromLinkTarget(XmlParser.getNode("celldesigner:linkTarget", child));
            anchorsByNodes.put(reactionNode, anchor);
          }
        } else {
          throw new ReactionParserException("Unknown node type: " + child.getNodeName(), reaction);
        }
      }
    }
    if (lastMember == null) {
      throw new ReactionParserException("Missing gate member connecting members", reaction);
    } else {
      String pointsString = XmlParser.getNodeAttr("editPoints", lastMember);
      List<Point2D> points = parseEditPointsString(pointsString);
      result.addNum(points.size() - 1);
      result.getPoints().addAll(points);
    }
    if (result.getLineStartingPoints().size() < 3) {
      throw new ReactionParserException("No information about line separation", reaction);
    }
    return result;
  }

  /**
   * Creates line information for the modifier.
   * 
   * @param modifier
   *          modifier to update
   * @throws ReactionParserException
   */
  private void createLineForModifier(Modifier modifier) throws ReactionParserException {
    try {
      Element element = modifier.getElement();
      CellDesignerAliasConverter converter = new CellDesignerAliasConverter(element, sbgn);
      Point2D startPoint = converter.getPointCoordinates(element, anchorsByNodes.get(modifier));
      ModifierTypeUtils modifierTypeUtils = new ModifierTypeUtils();

      Point2D p = modifierTypeUtils.getAnchorPointOnReactionRect(modifier.getReaction(),
          lineTypeByModifier.get(modifier));
      PolylineData line = PolylineDataFactory.createPolylineDataFromEditPoints(startPoint, p,
          pointsByModifier.get(modifier));

      startPoint = converter.getAnchorPointCoordinates(element, anchorsByNodes.get(modifier), line);
      line.setStartPoint(startPoint);
      modifier.setLine(line);
      modifierTypeUtils.updateLineEndPoint(modifier);
      applyStylingToLine(linePropertiesByModifier.get(modifier), line);
    } catch (Exception e) {
      throw new ReactionParserException("Problem with creating layout for modifier.", modifier.getReaction(), e);
    }
  }

  /**
   * Creates operators for CellDesigner reaction that belongs to
   * {@link TwoProductReactionInterface}.
   * 
   * @param result
   *          reaction to be updated
   * @throws ReactionParserException
   *           thrown when there is a problem with creating operators (input data
   *           is invalid)
   */
  private void createOperatorsForTwoProductReaction(Reaction result) throws ReactionParserException {
    int inputs = 0;
    NodeOperator operator = null;
    if (result.getClass() == DissociationReaction.class) {
      operator = new DissociationOperator();
    } else if (result.getClass() == TruncationReaction.class) {
      operator = new TruncationOperator();
    } else {
      throw new ReactionParserException("Invalid reaction type", result);
    }
    for (AbstractNode node : result.getNodes()) {
      if (node.getClass() == Product.class) {
        operator.addOutput(node);
      } else if (node.getClass() == Reactant.class) {
        inputs++;
        if (inputs > 1) {
          throw new ReactionParserException("Reaction has more than one reactant", result);
        }
      }
    }
    if (operator.getOutputs().size() > 2) {
      throw new ReactionParserException("Too many products: " + operator.getOutputs().size(), result);
    }

    // and now we have to modify lines

    Reactant reactant = result.getReactants().get(0);
    Integer index = indexByComplexReaction.get(reactant);

    Point2D p1, p2;

    p1 = reactant.getLine().getPoints().get(index);
    p2 = reactant.getLine().getPoints().get(index + 1);

    p1 = new Point2D.Double(p1.getX(), p1.getY());
    p2 = new Point2D.Double(p2.getX(), p2.getY());

    Point2D p = new Point2D.Double((p2.getX() + p1.getX()) / 2, (p2.getY() + p1.getY()) / 2);
    operator.setLine(reactant.getLine().getSubline(0, index + 1));
    operator.getLine().addPoint(p);

    p = new Point2D.Double((p2.getX() + p1.getX()) / 2, (p2.getY() + p1.getY()) / 2);

    reactant.setLine(reactant.getLine().getSubline(index + 1, reactant.getLine().getPoints().size()).reverse());
    reactant.getLine().getPoints().add(p);

    reactant.getLine().trimEnd(ReactionCellDesignerConverter.RECT_SIZE / 2 - 1);
    operator.getLine().trimEnd(ReactionCellDesignerConverter.RECT_SIZE / 2 - 1);

    result.addNode(operator);

    PolylineData centerLine = new PolylineData();
    centerLine.addPoint(pointTransformation.copyPoint(reactant.getLine().getEndPoint()));
    centerLine.addPoint(pointTransformation.copyPoint(operator.getLine().getEndPoint()));
    centerLine.setType(reactant.getLine().getType());

    result.setLine(centerLine);
  }

  /**
   * Creates general input/output operators for the model.
   * 
   * @param result
   *          reaction to be updated
   */
  private void createOperators(Reaction result) {
    // central line points
    Point2D p1 = result.getReactants().get(0).getLine().getPoints()
        .get(result.getReactants().get(0).getLine().getPoints().size() - 2);
    Point2D p2 = result.getProducts().get(0).getLine().getPoints().get(1);
    Point2D tmp = result.getProducts().get(0).getLine().getPoints().get(0);
    Point2D productSplitOperatorBeginPoint = new Point2D.Double(tmp.getX(), tmp.getY());

    // where the line from reactants ends
    tmp = result.getReactants().get(0).getLine().getPoints()
        .get(result.getReactants().get(0).getLine().getPoints().size() - 1);
    Point2D reactantAndOperatorEndPoint = new Point2D.Double(tmp.getX(), tmp.getY());

    Set<AbstractNode> toExclude = new HashSet<AbstractNode>();
    Set<AbstractNode> toInclude = new HashSet<AbstractNode>();
    for (NodeOperator operator : result.getOperators()) {
      toExclude.addAll(operator.getInputs());
      if (operator.isReactantOperator()) {
        toInclude.add(operator);
        // if we have operator in input then central line changes
        p1 = operator.getLine().getPoints().get(operator.getLine().getPoints().size() - 2);
        tmp = operator.getLine().getPoints().get(operator.getLine().getPoints().size() - 1);
        reactantAndOperatorEndPoint = new Point2D.Double(tmp.getX(), tmp.getY());
      }
      if (operator.isProductOperator()) {
        // if we have operator in output then central line changes
        p2 = operator.getLine().getPoints().get(operator.getLine().getPoints().size() - 2);
        tmp = operator.getLine().getPoints().get(operator.getLine().getPoints().size() - 1);
        productSplitOperatorBeginPoint = new Point2D.Double(tmp.getX(), tmp.getY());
      }
    }

    double dx = p2.getX() - p1.getX();
    double dy = p2.getY() - p1.getY();

    Point2D reactantAndOperatorBeginPoint = new Point2D.Double(p1.getX() + dx * REACTANT_END_RATIO,
        p1.getY() + dy * REACTANT_END_RATIO);

    PolylineData ld = new PolylineData(reactantAndOperatorBeginPoint, reactantAndOperatorEndPoint);

    LineType lineType = null;
    Set<AbstractNode> nodes = new LinkedHashSet<AbstractNode>();
    for (Reactant reactant : result.getReactants()) {
      if (!toExclude.contains(reactant)) {
        nodes.add(reactant);
        if (lineType == null) {
          lineType = reactant.getLine().getType();
        }
      }
    }
    nodes.addAll(toInclude);
    nodes.removeAll(toExclude);

    // add an operator only when the number of input nodes is greater than one
    if (nodes.size() > 1) {
      AndOperator inputOperator = new AndOperator();
      inputOperator.setLine(ld);
      applyStylingToLine(new LineProperties(nodes.iterator().next().getLine()), ld);
      inputOperator.addInputs(nodes);
      for (AbstractNode node : nodes) {
        node.getLine().getEndPoint().setLocation(reactantAndOperatorBeginPoint.getX(),
            reactantAndOperatorBeginPoint.getY());
      }
      if (lineType != null) {
        inputOperator.getLine().setType(lineType);
      }
      result.addNode(inputOperator);
    }

    // and now we try to handle with output operators

    toExclude = new HashSet<AbstractNode>();
    toInclude = new HashSet<AbstractNode>();
    for (NodeOperator nOperator : result.getOperators()) {
      toExclude.addAll(nOperator.getOutputs());
      if (nOperator.isProductOperator()) {
        toInclude.add(nOperator);
      }
    }

    Point2D productSplitOperatorEndPoint = new Point2D.Double(p1.getX() + dx * PRODUCT_START_RATIO,
        p1.getY() + dy * PRODUCT_START_RATIO);

    ld = new PolylineData(productSplitOperatorEndPoint, productSplitOperatorBeginPoint);

    lineType = null;
    nodes = new LinkedHashSet<AbstractNode>();
    for (Product product : result.getProducts()) {
      if (!toExclude.contains(product)) {
        nodes.add(product);
        if (lineType == null) {
          lineType = product.getLine().getType();
        }
      }
    }
    nodes.addAll(toInclude);
    nodes.removeAll(toExclude);
    if (nodes.size() > 1) {
      SplitOperator outputOperator = new SplitOperator();
      outputOperator.setLine(ld);
      applyStylingToLine(new LineProperties(nodes.iterator().next().getLine()), ld);
      outputOperator.addOutputs(nodes);
      for (Product product : result.getProducts()) {
        if (!toExclude.contains(product)) {
          // outputOperator.addOutput(product);
          product.getLine().getPoints().get(0).setLocation(productSplitOperatorEndPoint.getX(),
              productSplitOperatorEndPoint.getY());
        }
      }
      if (lineType != null) {
        outputOperator.getLine().setType(lineType);
      }
      result.addNode(outputOperator);
    }
  }

  /**
   * Creates operators in modifiers.
   * 
   * @param reaction
   *          reaction to update
   * @param modifiers
   *          list of modifiers that must be put into operators
   */
  private void createOperatorFromModifiers(Reaction reaction, List<Modifier> modifiers) {
    OperatorTypeUtils otu = new OperatorTypeUtils();
    String type = typeByModifier.get(modifiers.get(0));
    NodeOperator operator = otu.createModifierForStringType(type);

    operator.setLine(modifiers.get(0).getLine());

    for (int i = 1; i < modifiers.size(); i++) {
      operator.addInput(modifiers.get(i));
    }

    ModifierTypeUtils modifierTypeUtils = new ModifierTypeUtils();
    modifierTypeUtils.updateLineEndPoint(operator);
    for (AbstractNode inputs : operator.getInputs()) {
      inputs.getLine().setType(operator.getLine().getType());
    }

    reaction.addNode(operator);
  }

  /**
   * Creates operators for CellDesigner reaction that belongs to
   * {@link TwoReactantReactionInterface}.
   * 
   * @param result
   *          reaction to be updated
   * @param gateMembers
   *          node representing line information for the operator
   * @throws ReactionParserException
   *           thrown when the xml is invalid
   */
  private void createOperatorsForTwoReactantReaction(Reaction result, Node gateMembers) throws ReactionParserException {
    NodeOperator andOperator = null;
    if (result instanceof HeterodimerAssociationReaction) {
      andOperator = new AssociationOperator();
    } else {
      andOperator = new AndOperator();
    }
    int outputs = 0;
    for (AbstractNode node : result.getNodes()) {
      if (node.getClass() == Reactant.class) {
        andOperator.addInput(node);
      } else if (node.getClass() == Product.class) {
        outputs++;
        if (outputs > 1) {
          throw new ReactionParserException("Reaction has more than one product", result);
        }
      }
    }

    // and now we have to modify lines

    Product product = result.getProducts().get(0);
    Integer index = indexByComplexReaction.get(product);
    if (index != null) {
      Point2D p1, p2;

      p1 = product.getLine().getPoints().get(index);
      p2 = product.getLine().getPoints().get(index + 1);

      p1 = new Point2D.Double(p1.getX(), p1.getY());
      p2 = new Point2D.Double(p2.getX(), p2.getY());

      Point2D p = new Point2D.Double((p2.getX() + p1.getX()) / 2, (p2.getY() + p1.getY()) / 2);
      andOperator.setLine(product.getLine().getSubline(0, index + 1));
      andOperator.getLine().addPoint(p);
      andOperator.getLine().trimEnd(ReactionCellDesignerConverter.RECT_SIZE / 2 - 1);

      p = new Point2D.Double((p2.getX() + p1.getX()) / 2, (p2.getY() + p1.getY()) / 2);

      product.setLine(product.getLine().getSubline(index, product.getLine().getPoints().size()));
      product.getLine().getPoints().set(0, p);
      product.getLine().trimBegin(ReactionCellDesignerConverter.RECT_SIZE / 2 - 1);

      product.getLine().getEndAtd().setArrowType(ArrowType.FULL);

      result.addNode(andOperator);

      PolylineData centerLine = new PolylineData();
      centerLine.addPoint(pointTransformation.copyPoint(andOperator.getLine().getEndPoint()));
      centerLine.addPoint(pointTransformation.copyPoint(product.getLine().getBeginPoint()));
      centerLine.setType(product.getLine().getType());

      result.setLine(centerLine);
    } else {
      NodeOperator operator = null;
      Set<String> undefinedTypes = new HashSet<>();
      for (int i = 0; i < gateMembers.getChildNodes().getLength(); i++) {
        Node child = gateMembers.getChildNodes().item(i);
        if (child.getNodeType() == Node.ELEMENT_NODE) {

          if (child.getNodeName().equalsIgnoreCase("celldesigner:GateMember")) {
            String type = XmlParser.getNodeAttr("type", child);

            if (type.equalsIgnoreCase(OperatorType.AND_OPERATOR_STRING.getStringName())) {
              operator = new AndOperator();
            } else if (type.equalsIgnoreCase(OperatorType.OR_OPERATOR_STRING.getStringName())) {
              operator = new OrOperator();
            } else if (type.equalsIgnoreCase(OperatorType.NAND_OPERATOR_STRING.getStringName())) {
              operator = new NandOperator();
            } else if (type.equalsIgnoreCase(OperatorType.UNKNOWN_OPERATOR_STRING.getStringName())) {
              operator = new UnknownOperator();
            } else {
              undefinedTypes.add(type);
            }
          }
        }
      }
      if (operator == null) {
        String types = "";
        for (String string : undefinedTypes) {
          types += string + ", ";
        }
        throw new ReactionParserException(
            "Couldn't find type of BOOLEAN_LOGIC_GATE. Unknown types identified: " + types, result);
      }

      operator.addInputs(andOperator.getInputs());

      // operator line
      PolylineData line = new PolylineData();
      line.addPoint(pointTransformation.copyPoint(product.getLine().getBeginPoint()));

      Point2D secondPoint = pointTransformation.getPointOnLine(product.getLine().getPoints().get(0),
          product.getLine().getPoints().get(1), 0.4);
      Point2D thirdPoint = pointTransformation.getPointOnLine(product.getLine().getPoints().get(0),
          product.getLine().getPoints().get(1), 0.6);
      line.addPoint(pointTransformation.copyPoint(secondPoint));
      line.setType(product.getLine().getType());
      operator.setLine(line);

      // center line
      PolylineData centerLine = new PolylineData();
      centerLine.addPoint(pointTransformation.copyPoint(secondPoint));
      centerLine.addPoint(pointTransformation.copyPoint(thirdPoint));
      centerLine.setType(product.getLine().getType());
      result.setLine(centerLine);

      // product line is not trimmed
      product.getLine().getPoints().get(0).setLocation(thirdPoint);

      result.addNode(operator);
    }
  }

  private void assignColorFromGateMembers(Reaction result, Node gateMembers) {
    int elementIndex = 0;
    for (int i = 0; i < gateMembers.getChildNodes().getLength(); i++) {
      Node child = gateMembers.getChildNodes().item(i);
      if (child.getNodeType() == Node.ELEMENT_NODE) {
        if (child.getNodeName().equalsIgnoreCase("celldesigner:GateMember")) {
          Node lineNode = XmlParser.getNode("celldesigner:line", child);
          if (lineNode != null) {
            LineProperties line = getLineProperties(lineNode);
            Set<PolylineData> polylines = new HashSet<>();
            if (elementIndex == 0) {
              if (result.getOperators().size() > 0) {
                polylines.add(result.getOperators().get(result.getOperators().size() - 1).getLine());
                polylines.add(result.getLine());
              }
            } else {
              if (result.getReactants().size() >= elementIndex) {
                polylines.add(result.getReactants().get(elementIndex - 1).getLine());
              }
            }
            for (PolylineData polyline : polylines) {
              polyline.setColor(line.getColor());
              polyline.setWidth(line.getWidth());
            }
          }
          elementIndex++;
        }
      }
    }
  }

  /**
   * Creates lines for reaction that belongs to
   * {@link TwoProductReactionInterface}.
   * 
   * @param reaction
   *          reaction to update
   * @param points
   *          information about points
   */
  private void createLinesForTwoProductReaction(Reaction reaction, EditPoints points) {
    Point2D p = points.getPoints().get(points.size() - 1);
    Product product1 = reaction.getProducts().get(0);
    Product product2 = reaction.getProducts().get(1);
    Reactant reactant = reaction.getReactants().get(0);

    CellDesignerAliasConverter reactantConverter = new CellDesignerAliasConverter(reactant.getElement(), sbgn);
    CellDesignerAliasConverter product1Converter = new CellDesignerAliasConverter(product1.getElement(), sbgn);
    CellDesignerAliasConverter product2Converter = new CellDesignerAliasConverter(product2.getElement(), sbgn);

    Point2D p1 = reactantConverter.getPointCoordinates(reactant.getElement(), anchorsByNodes.get(reactant));
    Point2D p2 = product1Converter.getPointCoordinates(product1.getElement(), anchorsByNodes.get(product1));
    Point2D p3 = product2Converter.getPointCoordinates(product2.getElement(), anchorsByNodes.get(product2));

    Point2D centerPoint = pointTransformation.getCoordinatesInNormalBase(product1.getElement().getCenter(),
        product2.getElement().getCenter(), reactant.getElement().getCenter(), p);

    int startId0 = 0;
    int num0 = points.getLineStartingPoints().get(0);
    int startId1 = num0;
    int num1 = num0 + points.getLineStartingPoints().get(1);
    int startId2 = num1;
    int num2 = num1 + points.getLineStartingPoints().get(2);

    ArrayList<Point2D> linePoints1 = new ArrayList<Point2D>(points.getPoints().subList(startId0, num0));
    ArrayList<Point2D> linePoints2 = new ArrayList<Point2D>(points.getPoints().subList(startId1, num1));
    ArrayList<Point2D> linePoints3 = new ArrayList<Point2D>(points.getPoints().subList(startId2, num2));

    PolylineData product1Line = PolylineDataFactory.createPolylineDataFromEditPoints(centerPoint, p2, linePoints2);
    PolylineData product2Line = PolylineDataFactory.createPolylineDataFromEditPoints(centerPoint, p3, linePoints3);

    PolylineData reactantLine = PolylineDataFactory.createPolylineDataFromEditPoints(centerPoint, p1, linePoints1);

    p1 = product2Converter.getAnchorPointCoordinates(product2.getElement(), anchorsByNodes.get(product2),
        product2Line.reverse());
    product2Line.setEndPoint(p1);

    p1 = product1Converter.getAnchorPointCoordinates(product1.getElement(), anchorsByNodes.get(product1),
        product1Line.reverse());
    product1Line.setEndPoint(p1);

    p1 = reactantConverter.getAnchorPointCoordinates(reactant.getElement(), anchorsByNodes.get(reactant),
        reactantLine.reverse());
    reactantLine.setEndPoint(p1);

    product2Line.getEndAtd().setArrowType(ArrowType.FULL);
    product1Line.getEndAtd().setArrowType(ArrowType.FULL);
    product1.setLine(product1Line);
    product2.setLine(product2Line);
    reactant.setLine(reactantLine);

    indexByComplexReaction.put(reactant, points.getReactionCenterLineIndex());

  }

  /**
   * Creates lines for reaction that belongs to
   * {@link TwoReactantReactionInterface}.
   * 
   * @param reaction
   *          reaction to update
   * @param points
   *          information about points
   * @param hasGateMemebers
   *          does the reaction has gate members
   * @param rld
   * @throws ReactionParserException
   *           thrown when data for reaction is invalid
   */
  private void createLinesForTwoReactantReaction(Reaction reaction, EditPoints points)
      throws ReactionParserException {
    ReactionLineData rld = null;
    if (!(reaction instanceof HeterodimerAssociationReaction)) {
      rld = ReactionLineData.getByReactionType(reaction.getClass());
    }

    Product product = reaction.getProducts().get(0);
    Reactant reactant1 = reaction.getReactants().get(0);
    Reactant reactant2 = reaction.getReactants().get(1);
    CellDesignerAliasConverter productConverter = new CellDesignerAliasConverter(product.getElement(), sbgn);
    CellDesignerAliasConverter reactantConverter1 = new CellDesignerAliasConverter(reactant1.getElement(), sbgn);
    CellDesignerAliasConverter reactantConverter2 = new CellDesignerAliasConverter(reactant2.getElement(), sbgn);

    Point2D pointA = reactantConverter2.getPointCoordinates(reactant2.getElement(), anchorsByNodes.get(reactant2));
    Point2D pointC = reactantConverter1.getPointCoordinates(reactant1.getElement(), anchorsByNodes.get(reactant1));
    Point2D pointB = productConverter.getPointCoordinates(product.getElement(), anchorsByNodes.get(product));

    if (rld == null) { // heterodimer association computes based on different coordinates rules...
      pointA = reactant2.getElement().getCenter();
      pointC = reactant1.getElement().getCenter();
      pointB = product.getElement().getCenter();
    }

    Point2D pointP = points.getPoints().get(points.size() - 1);
    Point2D centerPoint = null;

    if (rld == null && !pointA.equals(pointB)) {
      centerPoint = pointTransformation.getCoordinatesInNormalBase(pointA, pointB, pointC, pointP);
    } else {
      centerPoint = pointP;
    }

    int productStartId = 0;

    for (int reactantIndex = 0; reactantIndex < reaction.getReactants().size(); reactantIndex++) {
      Reactant reactant = reaction.getReactants().get(reactantIndex);
      CellDesignerAliasConverter reactantConverter = new CellDesignerAliasConverter(reactant.getElement(), sbgn);
      Point2D p1 = reactantConverter.getPointCoordinates(reactant.getElement(), anchorsByNodes.get(reactant));

      int reactantStartId = productStartId;
      int reactantEndId = reactantStartId + points.getLineStartingPoints().get(reactantIndex);

      productStartId = reactantEndId;

      List<Point2D> linePoints = new ArrayList<>(points.getPoints().subList(reactantStartId, reactantEndId));

      PolylineData reactantLine = null;
      if (rld == null) {
        reactantLine = PolylineDataFactory.createPolylineDataFromEditPoints(centerPoint, p1, linePoints);
      } else {
        reactantLine = PolylineDataFactory.createPolylineDataFromEditPoints(p1, centerPoint, linePoints);
        reactantLine = reactantLine.reverse();
        reactantLine.setType(rld.getLineType());
      }

      p1 = reactantConverter.getAnchorPointCoordinates(reactant.getElement(), anchorsByNodes.get(reactant),
          reactantLine.reverse());
      reactantLine.setEndPoint(p1);
      reactantLine = reactantLine.reverse();

      reactant.setLine(reactantLine);
    }
    int productEndId = productStartId + points.getLineStartingPoints().get(reaction.getReactants().size());
    List<Point2D> linePoints3 = new ArrayList<>(points.getPoints().subList(productStartId, productEndId));
    PolylineData productLine = PolylineDataFactory.createPolylineDataFromEditPoints(centerPoint,
        productConverter.getPointCoordinates(product.getElement(), anchorsByNodes.get(product)), linePoints3);
    Point2D p1 = productConverter.getAnchorPointCoordinates(product.getElement(), anchorsByNodes.get(product),
        productLine.reverse());
    productLine.setEndPoint(p1);
    if (rld != null) {
      productLine.setType(rld.getLineType());
      productLine.getEndAtd().setArrowType(rld.getProductArrowType());
      productLine.trimEnd(rld.getProductLineTrim());
    }

    product.setLine(productLine);

    indexByComplexReaction.put(product, points.getReactionCenterLineIndex());
  }

  /**
   * Creates lines for reaction that belongs to {@link SimpleReactionInterface}
   * (only one standard product and one standard reactant).
   * 
   * @param reaction
   *          reaction to update
   * @param editPoints
   *          information about points
   * @param scheme
   *          some additional information about the line
   */
  private void createLinesForSimpleReaction(Reaction reaction, EditPoints editPoints, ConnectScheme scheme) {
    Product product = reaction.getProducts().get(0);
    Reactant reactant = reaction.getReactants().get(0);
    CellDesignerAliasConverter productConverter = new CellDesignerAliasConverter(product.getElement(), sbgn);
    CellDesignerAliasConverter reactantConverter = new CellDesignerAliasConverter(reactant.getElement(), sbgn);

    Point2D endPoint = productConverter.getPointCoordinates(product.getElement(), anchorsByNodes.get(product));
    Point2D startPoint = reactantConverter.getPointCoordinates(reactant.getElement(), anchorsByNodes.get(reactant));

    PolylineData ld = PolylineDataFactory.createPolylineDataFromEditPoints(startPoint, endPoint,
        editPoints.getPoints());

    // first place where the index of rectangle is kept
    Integer index = editPoints.getReactionCenterLineIndex();

    // second place where index of rectangle is kept
    if (index == null) {
      index = scheme.getConnectIndex();
      if (index != null) {
        // but...
        // direction of the index is reversed...
        index = ld.getPoints().size() - index - 2;
      }
    }
    // but sometimes there is no information about index...
    if (index == null) {
      index = 0;
      // remove collinear points (because just by chance we can pickup the wrong
      // segment which is too small)
      ld = PolylineDataFactory.removeCollinearPoints(ld);
    }
    startPoint = reactantConverter.getAnchorPointCoordinates(reactant.getElement(), anchorsByNodes.get(reactant), ld);
    endPoint = productConverter.getAnchorPointCoordinates(product.getElement(), anchorsByNodes.get(product),
        ld.reverse());
    ld.getPoints().set(0, startPoint);
    ld.getPoints().set(ld.getPoints().size() - 1, endPoint);

    PolylineData line = new PolylineData(ld);

    PolylineData reactantLine = line.getSubline(0, ld.getPoints().size() - index - 1);
    PolylineData productLine = line.getSubline(ld.getPoints().size() - index - 2, ld.getPoints().size());

    Point2D p1 = ld.getPoints().get(ld.getPoints().size() - index - 2);
    Point2D p2 = ld.getPoints().get(ld.getPoints().size() - index - 1);
    Point2D p = new Point2D.Double((p2.getX() + p1.getX()) / 2, (p2.getY() + p1.getY()) / 2);
    reactantLine.getPoints().add(p);
    reactantLine.trimEnd(ReactionCellDesignerConverter.RECT_SIZE / 2 - 1);

    p = new Point2D.Double((p2.getX() + p1.getX()) / 2, (p2.getY() + p1.getY()) / 2);
    productLine.getPoints().set(0, p);
    productLine.trimBegin(ReactionCellDesignerConverter.RECT_SIZE / 2 - 1);

    productLine.getEndAtd().setArrowType(ArrowType.FULL);

    reactant.setLine(reactantLine);
    product.setLine(productLine);

    ReactionLineData rld = ReactionLineData.getByReactionType(reaction.getClass());
    reactantLine.setType(rld.getLineType());
    productLine.setType(rld.getLineType());
    productLine.getEndAtd().setArrowType(rld.getProductArrowType());
    productLine.trimEnd(rld.getProductLineTrim());

    PolylineData reactionLine = new PolylineData();
    reactionLine.addPoint(pointTransformation.copyPoint(reactantLine.getEndPoint()));
    reactionLine.addPoint(pointTransformation.copyPoint(productLine.getBeginPoint()));
    reactionLine.setType(reactantLine.getType());
    reaction.setLine(reactionLine);
  }

  /**
   * Prepares {@link EditPoints} structure from xml node.
   * 
   * @param rootNode
   *          xml node
   * @return {@link EditPoints} object containing CellDesigner line data
   */
  private EditPoints parseEditPoints(Node rootNode) {
    EditPoints result = new EditPoints();
    String num0 = XmlParser.getNodeAttr("num0", rootNode);
    if (!num0.equals("")) {
      result.addNum(Integer.parseInt(num0));
    }
    String num1 = XmlParser.getNodeAttr("num1", rootNode);
    if (!num1.isEmpty()) {
      if (num0.isEmpty()) {
        throw new InvalidStateException();
      }
      result.addNum(Integer.parseInt(num1));
    }

    String num2 = XmlParser.getNodeAttr("num2", rootNode);
    if (!num2.isEmpty()) {
      if (num0.isEmpty() || num1.isEmpty()) {
        throw new InvalidStateException();
      }
      result.addNum(Integer.parseInt(num2));
    }

    String index = XmlParser.getNodeAttr("tShapeIndex", rootNode);
    if (!index.isEmpty()) {
      result.setReactionCenterLineIndex(Integer.parseInt(index));
    }

    String pointsString = XmlParser.getNodeValue(rootNode);
    result.setPoints(parseEditPointsString(pointsString));
    return result;
  }

  /**
   * Parses CellDesigner point by point string.
   * 
   * @param pointsString
   *          String containing points
   * @return list of points
   */
  ArrayList<Point2D> parseEditPointsString(String pointsString) {
    ArrayList<Point2D> points2 = new ArrayList<Point2D>();
    if (pointsString.isEmpty()) {
      return points2;
    }
    String[] points = pointsString.trim().split(" ");
    for (String string : points) {
      String[] p = string.split(",");
      if (p.length != 2) {
        throw new InvalidArgumentException("Invalid editPoint string: " + string);
      } else {
        double posX = Double.parseDouble(p[0]);
        double posY = Double.parseDouble(p[1]);
        Point2D point = new Point2D.Double(posX, posY);
        if (!pointTransformation.isValidPoint(point)) {
          throw new InvalidArgumentException(
              "Invalid point parsed from input string: " + string + ". Result point: " + point);
        }
        points2.add(point);
      }
    }
    return points2;
  }

  /**
   * Parses xml node with reactantLink nodes and creates reactants in the
   * reaction.
   * 
   * @param result
   *          reaction to be updated
   * @param rootNode
   *          xml node to parse
   * @param model
   *          model where reaction is placed
   * @throws ReactionParserException
   *           thrown when the xml is invalid
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  private void parseReactantLinks(Reaction result, Node rootNode, Model model) throws ReactionParserException {
    NodeList list = rootNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:reactantLink")) {
          Reactant newReactant = getReactantLink(node, model, result);
          result.addReactant(newReactant);
        } else {
          throw new ReactionParserException(
              "Unknown element of celldesigner:listOfReactantLinks: " + node.getNodeName(), result);
        }
      }
    }
  }

  /**
   * Parses xml node for reactanLink and creates reactant from it.
   * 
   * @param rootNode
   *          xml node to parse
   * @param model
   *          model where reaction is placed
   * @param reaction
   *          reaction that is being parsed
   * @return reactant obtained from xml node
   * @throws ReactionParserException
   *           thrown when the xml is invalid and reason is more specific
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  private Reactant getReactantLink(Node rootNode, Model model, Reaction reaction) throws ReactionParserException {
    String aliasId = XmlParser.getNodeAttr("alias", rootNode);
    Element element = model.getElementByElementId(aliasId);
    if (element == null) {
      throw new ReactionParserException("Alias doesn't exist (id: " + aliasId + ")", reaction);
    }

    Reactant result = new Reactant(element);
    CellDesignerAnchor anchor = null;
    EditPoints points = null;
    LineProperties style = null;

    NodeList nodes = rootNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:connectScheme")) {
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:linkAnchor")) {
          anchor = CellDesignerAnchor.valueOf(XmlParser.getNodeAttr("position", node).toUpperCase());
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:line")) {
          style = getLineProperties(node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:editPoints")) {
          points = parseEditPoints(node);
        } else {
          throw new ReactionParserException("Unknown element of celldesigner:reactantLink: " + node.getNodeName(),
              reaction);
        }
      }
    }

    CellDesignerAliasConverter reactantConverter = new CellDesignerAliasConverter(element, sbgn);
    Point2D additionalPoint = reactantConverter.getPointCoordinates(element, anchor);
    ArrowTypeData atd = new ArrowTypeData();
    atd.setArrowType(ArrowType.NONE);

    Point2D endPoint = getPointForAdditionalNode(reaction, REACTANT_END_RATIO);

    PolylineData polyline = PolylineDataFactory.createPolylineDataFromEditPoints(additionalPoint, endPoint, points);

    additionalPoint = reactantConverter.getAnchorPointCoordinates(element, anchor, polyline);
    polyline.setStartPoint(additionalPoint);
    result.setLine(polyline);
    applyStylingToLine(style, polyline);

    return result;
  }

  /**
   * Returns end point for additional reactants/products.
   * 
   * @param reaction
   *          reaction for which we need to compute end point
   * @param coef
   *          coefficient determining location of the reactant product end point.
   *          It should be one of hard-coded values: {@link #REACTANT_END_RATIO},
   *          {@link #PRODUCT_START_RATIO}.
   * @return
   */
  private Point2D getPointForAdditionalNode(Reaction reaction, double coef) {
    Point2D endPoint;

    // central line points
    Point2D p1 = reaction.getReactants().get(0).getLine().getPoints()
        .get(reaction.getReactants().get(0).getLine().getPoints().size() - 2);
    Point2D p2 = reaction.getProducts().get(0).getLine().getPoints().get(1);

    Set<AbstractNode> toExclude = new HashSet<AbstractNode>();
    Set<AbstractNode> toInclude = new HashSet<AbstractNode>();
    for (NodeOperator operator : reaction.getOperators()) {
      toExclude.addAll(operator.getInputs());
      if (operator.isReactantOperator()) {
        toInclude.add(operator);
        // if we have operator in input then central line changes
        p1 = operator.getLine().getPoints().get(operator.getLine().getPoints().size() - 2);
      }
      if (operator.isProductOperator()) {
        // if we have operator in output then central line changes
        p2 = operator.getLine().getPoints().get(operator.getLine().getPoints().size() - 2);
      }
    }
    double dx = p2.getX() - p1.getX();
    double dy = p2.getY() - p1.getY();
    endPoint = new Point2D.Double(p1.getX() + dx * coef, p1.getY() + dy * coef);
    return endPoint;
  }

  /**
   * Parses xml node with productLink nodes and creates products in the reaction.
   * 
   * @param result
   *          reaction to be updated
   * @param rootNode
   *          xml node to parse
   * @param model
   *          model where reaction is placed
   * @throws ReactionParserException
   *           thrown when the xml is invalid
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  private void parseProductLinks(Reaction result, Node rootNode, Model model) throws ReactionParserException {
    NodeList list = rootNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:productLink")) {
          Product link = getProductLink(node, model, result);
          result.addProduct(link);
        } else {
          throw new ReactionParserException("Unknown element of celldesigner:listOfProductLinks: " + node.getNodeName(),
              result);
        }
      }
    }
  }

  /**
   * Parses xml node for productLink and creates product from it.
   * 
   * @param rootNode
   *          xml node to parse
   * @param model
   *          model where reaction is placed
   * @param reaction
   *          reaction that is being parsed
   * @return product obtained from xml node
   * @throws ReactionParserException
   *           thrown when the xml is invalid
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  private Product getProductLink(Node rootNode, Model model, Reaction reaction) throws ReactionParserException {
    String aliasId = XmlParser.getNodeAttr("alias", rootNode);
    Element element = model.getElementByElementId(aliasId);
    if (element == null) {
      throw new ReactionParserException("Alias doesn't exist (id: " + aliasId + ")", reaction);
    }

    Product result = new Product(element);

    CellDesignerAnchor anchor = null;
    EditPoints points = null;
    LineProperties style = null;

    NodeList nodes = rootNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:connectScheme")) {
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:linkAnchor")) {
          anchor = CellDesignerAnchor.valueOf(XmlParser.getNodeAttr("position", node).toUpperCase());
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:line")) {
          style = getLineProperties(node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:editPoints")) {
          points = parseEditPoints(node);
        } else {
          throw new ReactionParserException("Unknown element of celldesigner:reactantLink: " + node.getNodeName(),
              reaction);
        }
      }
    }

    CellDesignerAliasConverter reactantConverter = new CellDesignerAliasConverter(element, sbgn);
    Point2D additionalPoint = reactantConverter.getPointCoordinates(element, anchor);
    ArrowTypeData atd = new ArrowTypeData();
    atd.setArrowType(ArrowType.NONE);

    Point2D endPoint = getPointForAdditionalNode(reaction, PRODUCT_START_RATIO);

    PolylineData polyline = PolylineDataFactory.createPolylineDataFromEditPoints(endPoint, additionalPoint, points);
    additionalPoint = reactantConverter.getAnchorPointCoordinates(element, anchor, polyline.reverse());
    polyline.setEndPoint(additionalPoint);

    polyline.getEndAtd().setArrowType(ArrowType.FULL);
    result.setLine(polyline);
    applyStylingToLine(style, polyline);
    return result;
  }

  /**
   * Creates {@link LineProperties} object from the node.
   * 
   * @param node
   *          xml node to parse
   * @return {@link LineProperties} object
   */
  private LineProperties getLineProperties(Node node) {
    LineProperties line = new LineProperties();
    String tmp = XmlParser.getNodeAttr("width", node);
    line.setWidth(Double.parseDouble(tmp));
    tmp = XmlParser.getNodeAttr("color", node);
    line.setColor(XmlParser.stringToColor(tmp));
    line.setType(XmlParser.getNodeAttr("type", node));
    return line;
  }

  /**
   * PArse reaction modifications and add them into reaction.
   * 
   * @param result
   *          reaction currently processed
   * @param rootNode
   *          xml node to parse
   * @param model
   *          model where reaction is placed
   * @throws ReactionParserException
   *           thrown when the xml is invalid, and reason is more specific
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  private void parseReactionModification(Reaction result, Node rootNode, Model model) throws ReactionParserException {
    NodeList nodes = rootNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:modification")) {
          parseModificationReaction(result, node, model);
        } else {
          throw new ReactionParserException("Unknown element of celldesigner:listOfModification: " + node.getNodeName(),
              result);
        }
      }
    }
  }

  /**
   * PArses modification node and adds this modification to reaction.
   * 
   * @param reaction
   *          reaction currently processed
   * @param rootNode
   *          xml node
   * @param model
   *          model where reaction is placed
   * @throws ReactionParserException
   *           thrown when node cannot be parsed properly
   */
  private void parseModificationReaction(Reaction reaction, Node rootNode, Model model) throws ReactionParserException {
    ModifierTypeUtils modifierTypeUtils = new ModifierTypeUtils();
    String type = XmlParser.getNodeAttr("type", rootNode);
    String modificationType = XmlParser.getNodeAttr("modificationType", rootNode);
    String aliasId = XmlParser.getNodeAttr("aliases", rootNode);
    String lineConnectionType = XmlParser.getNodeAttr("targetLineIndex", rootNode);
    String points = XmlParser.getNodeAttr("editPoints", rootNode);

    List<Species> aliasList = new ArrayList<Species>();

    String[] list = aliasId.split(",");
    for (String string : list) {
      Species species = model.getElementByElementId(string);
      if (species != null) {
        aliasList.add(species);
      } else {
        throw new ReactionParserException("Unknown alias: " + string, reaction);
      }
    }
    Modifier result = null;
    if (aliasList.size() > 1) {
      result = new Modifier(null);
      reaction.addModifier(result);
      for (int i = 0; i < aliasList.size(); i++) {
        Modifier mod = modifierTypeUtils.createModifierForStringType(modificationType, aliasList.get(i));
        modifierParentOperator.put(mod, result);
        reaction.addModifier(mod);
      }
    } else {
      ModifierType mType = modifierTypeUtils.getModifierTypeForStringType(type);
      if (mType == null) {
        String errorInfo = "[" + reaction.getClass().getSimpleName() + "\t" + reaction.getIdReaction()
            + "]\tUnknown modifier type: " + type;
        if (ReactionLineData.getByCellDesignerString(type) != null) {
          errorInfo += ".\tThis type can be applied to reaction type only, not modifier.";
        }
        throw new UnknownModifierClassException(errorInfo, type, reaction.getIdReaction());
      }
      for (Modifier modifier : reaction.getModifiers()) {
        if (modifier.getElement() == aliasList.get(0) && modifier.getClass() == mType.getClazz()) {
          result = modifier;
        }
      }
      if (result == null) {
        result = modifierTypeUtils.createModifierForStringType(type, aliasList.get(0));
        reaction.addModifier(result);
      }
    }
    pointsByModifier.put(result, parseEditPointsString(points));
    typeByModifier.put(result, type);
    lineTypeByModifier.put(result, lineConnectionType);

    NodeList nodes = rootNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:connectScheme")) {
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:line")) {
          linePropertiesByModifier.put(result, getLineProperties(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:linkTarget")) {
          try {
            CellDesignerAnchor anchor = getAnchorFromLinkTarget(node);
            anchorsByNodes.put(result, anchor);
          } catch (InvalidXmlSchemaException e) {
            throw new ReactionParserException(reaction, e);
          }
        } else {
          throw new ReactionParserException("Unknown element of celldesigner:listOfModification: " + node.getNodeName(),
              reaction);
        }
      }

    }

  }

  /**
   * Creates lines for modifiers.
   * 
   * @param reaction
   *          reaction where modifiers are placed
   * @param modifiers
   *          list of modifiers for which lines should be generated
   */
  private void computeLineForModification(Reaction reaction, List<Modifier> modifiers) {
    ModifierTypeUtils modifierTypeUtils = new ModifierTypeUtils();
    Point2D p = modifierTypeUtils.getAnchorPointOnReactionRect(reaction, lineTypeByModifier.get(modifiers.get(0)));

    // in case we have more then one alias in modification then we need to
    // define the start point as a different one then one on the alias
    Modifier modifier = modifiers.get(0);
    List<Point2D> points = pointsByModifier.get(modifier);
    Point2D startPoint = points.get(points.size() - 1);
    List<Point2D> subPoints = points.subList(0, points.size() - 1);
    PolylineData line = PolylineDataFactory.createPolylineDataFromEditPoints(startPoint, p, subPoints);
    modifier.setLine(line);
    applyStylingToLine(linePropertiesByModifier.get(modifier), line);

    Point2D endPoint = startPoint;

    for (int i = 1; i < modifiers.size(); i++) {
      Modifier param = modifiers.get(i);
      CellDesignerAliasConverter reactantConverter = new CellDesignerAliasConverter(param.getElement(), sbgn);
      startPoint = reactantConverter.getPointCoordinates(param.getElement(), anchorsByNodes.get(param));

      PolylineData polyline = PolylineDataFactory.createPolylineDataFromEditPoints(startPoint, endPoint,
          pointsByModifier.get(param));

      startPoint = reactantConverter.getAnchorPointCoordinates(param.getElement(), anchorsByNodes.get(param), polyline);
      polyline.setStartPoint(startPoint);
      param.setLine(polyline);
      applyStylingToLine(linePropertiesByModifier.get(param), polyline);
    }
  }

  /**
   * Returns {@link CellDesignerAnchor} point from linkAnchor xml node.
   * 
   * @param rootNode
   *          xml node
   * @return {@link CellDesignerAnchor} object representing anchor point
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  private CellDesignerAnchor getAnchorFromLinkTarget(Node rootNode) throws InvalidXmlSchemaException {
    if (rootNode == null) {
      return null;
    }
    CellDesignerAnchor result = null;

    NodeList nodes = rootNode.getChildNodes();
    for (int z = 0; z < nodes.getLength(); z++) {
      Node node = nodes.item(z);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:linkAnchor")) {
          result = CellDesignerAnchor.valueOf(XmlParser.getNodeAttr("position", node).toUpperCase());
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:connectScheme: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Creates {@link ConnectScheme} object from xml node.
   * 
   * @param nodeReaction
   *          xml node to parse
   * @return {@link ConnectScheme} object for given xml
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  private ConnectScheme parseConnectScheme(Node nodeReaction) throws InvalidXmlSchemaException {
    ConnectScheme result = new ConnectScheme();
    result.setConnectPolicy(XmlParser.getNodeAttr("connectPolicy", nodeReaction));
    result.setConnectIndex(XmlParser.getNodeAttr("rectangleIndex", nodeReaction));
    NodeList reactantsNodes = nodeReaction.getChildNodes();
    for (int z = 0; z < reactantsNodes.getLength(); z++) {
      Node reactantNode = reactantsNodes.item(z);
      if (reactantNode.getNodeType() == Node.ELEMENT_NODE) {
        if (reactantNode.getNodeName().equalsIgnoreCase("celldesigner:listOfLineDirection")) {
          result.setLineDirections(getlineDirectionMapForReactions(reactantNode));
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:connectScheme: " + nodeReaction.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Parse lineDirectionMap. This structure is nowhere used.
   * 
   * @param reactantNode
   *          xml node
   * @return map with directions for every line
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  private Map<String, String> getlineDirectionMapForReactions(Node reactantNode) throws InvalidXmlSchemaException {
    Map<String, String> result = new HashMap<String, String>();
    NodeList nodes = reactantNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:lineDirection")) {
          String index = XmlParser.getNodeAttr("index", node);
          String value = XmlParser.getNodeAttr("value", node);
          result.put(index, value);
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of reaction/celldesigner:baseReactant: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * PArses baseReactant node from CellDEsigenr xml node and adds it into
   * reaction.
   * 
   * @param reactantNode
   *          xml node to parse
   * @param model
   *          model where reaction is placed
   * @throws ReactionParserException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  private Reactant parseBaseReactant(Node reactantNode, Model model) throws InvalidXmlSchemaException {
    String aliasId = XmlParser.getNodeAttr("alias", reactantNode);
    Species alias = model.getElementByElementId(aliasId);
    if (alias == null) {
      throw new InvalidXmlSchemaException("Alias with id=" + aliasId + " doesn't exist.");
    }
    Reactant reactant = new Reactant(alias);
    NodeList nodes = reactantNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:linkAnchor")) {
          anchorsByNodes.put(reactant, CellDesignerAnchor.valueOf(XmlParser.getNodeAttr("position", node)));
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of reaction/celldesigner:baseReactant: " + node.getNodeName());
        }
      }
    }
    return reactant;
  }

  /**
   * Parses baseProduct node from CellDEsigenr xml node and adds it into reaction.
   * 
   * @param reactantNode
   *          xml node to parse
   * @param model
   *          model where reaction is placed
   * @throws ReactionParserException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  private Product parseBaseProduct(Model model, Node reactantNode) throws InvalidXmlSchemaException {
    String aliasId = XmlParser.getNodeAttr("alias", reactantNode);
    Species species = model.getElementByElementId(aliasId);
    if (species == null) {
      throw new InvalidXmlSchemaException("Alias with id=" + aliasId + " doesn't exist.");
    }
    Product product = new Product(species);
    NodeList nodes = reactantNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:linkAnchor")) {
          anchorsByNodes.put(product, CellDesignerAnchor.valueOf(XmlParser.getNodeAttr("position", node)));
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of reaction/celldesigner:baseProduct: " + node.getNodeName());
        }
      }
    }
    return product;

  }

}
