package lcsb.mapviewer.converter.model.celldesigner.reaction;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.parameter.ParameterCollectionXmlParser;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.kinetics.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

public class KineticsXmlParser {
  /**
   * Default class logger
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(KineticsXmlParser.class);

  ParameterCollectionXmlParser parameterParser;

  Model model;

  public KineticsXmlParser(Model model) {
    this.model = model;
    parameterParser = new ParameterCollectionXmlParser(model);
  }

  public SbmlKinetics parseKinetics(Node kineticsNode, Map<String, Element> elements) throws InvalidXmlSchemaException {
    SbmlKinetics result = new SbmlKinetics();
    Node mathNode = XmlParser.getNode("math", kineticsNode);
    if (mathNode == null) {
      throw new InvalidXmlSchemaException("kineticLaw node doesn't have math subnode");
    }

    Node parametersNode = XmlParser.getNode("listOfParameters", kineticsNode);
    if (parametersNode != null) {
      result.addParameters(parameterParser.parseXmlParameterCollection(parametersNode));
    }

    Set<SbmlArgument> elementsUsedInKinetics = new HashSet<>();
    for (Node ciNode : XmlParser.getAllNotNecessirellyDirectChild("ci", mathNode)) {
      String id = XmlParser.getNodeValue(ciNode).trim();
      SbmlArgument element = elements.get(id);
      if (element == null) {
        element = result.getParameterById(id);
      }
      if (element == null) {
        element = model.getParameterById(id);
      }
      if (element == null) {
        element = model.getFunctionById(id);
      }
      if (element == null) {
        element = model.getElementByElementId(id);
      }
      if (element != null) {
        ciNode.setTextContent(element.getElementId());
        elementsUsedInKinetics.add(element);
      } else if (!id.equals("default")) {
        throw new InvalidXmlSchemaException("Unknown symbol in kinetics: " + id);
      }
    }
    result.addArguments(elementsUsedInKinetics);

    String definition = XmlParser.nodeToString(mathNode, true);
    definition = definition.replace(" xmlns=\"http://www.sbml.org/sbml/level2/version4\"", "");
    definition = definition.replace("<math>", "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">");
    result.setDefinition(definition);

    return result;
  }

  public String toXml(SbmlKinetics kinetics, CellDesignerElementCollection elements) throws InconsistentModelException {
    try {
      StringBuilder result = new StringBuilder();
      result.append("<kineticLaw>");
      Node mathNode = XmlParser.getXmlDocumentFromString(kinetics.getDefinition()).getFirstChild();
      for (Node ciNode : XmlParser.getAllNotNecessirellyDirectChild("ci", mathNode)) {
        String id = XmlParser.getNodeValue(ciNode).trim();
        Element element = model.getElementByElementId(id);
        if (element != null) {
          ciNode.setTextContent(elements.getElementId(element));
        }
      }
      result.append(XmlParser.nodeToString(mathNode, true));

      Set<SbmlParameter> parametersToAdd = new HashSet<>();
      for (SbmlParameter parameter : kinetics.getParameters()) {
        if (model.getParameterById(parameter.getParameterId()) == null) {
          parametersToAdd.add(parameter);
        }
      }

      result.append(parameterParser.toXml(parametersToAdd));
      result.append("</kineticLaw>");
      return result.toString();
    } catch (InvalidXmlSchemaException e) {
      throw new InconsistentModelException("Invalid xml kinetics definition: " + kinetics.getDefinition());
    }
  }

}
