package lcsb.mapviewer.converter.model.celldesigner.structure;

import lcsb.mapviewer.model.map.species.Chemical;

/**
 * Class representing CellDesigner {@link Chemical}.
 * 
 * @param <T>
 *          model class tha corresponds to this cell designer structure
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class CellDesignerChemical<T extends Chemical> extends CellDesignerSpecies<T> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * <a href=
   * "http://en.wikipedia.org/wiki/Simplified_molecular-input_line-entry_system"
   * >Smiles</a> parameter for the chemical.
   */
  private String smiles;

  /**
   * <a href= "http://en.wikipedia.org/wiki/International_Chemical_Identifier" >
   * InChI</a> parameter for the chemical.
   */

  private String inChI;

  /**
   * <a href=
   * "http://en.wikipedia.org/wiki/International_Chemical_Identifier#InChIKey" >
   * InChIKey</a> parameter for the chemical.
   */

  private String inChIKey;

  /**
   * Creates a chemical from the species given in the parameter.
   * 
   * @param species
   *          original species from which this chemical will be created
   */
  public CellDesignerChemical(CellDesignerSpecies<?> species) {
    super(species);
    if (species instanceof CellDesignerChemical) {
      CellDesignerChemical<?> chemical = (CellDesignerChemical<?>) species;
      this.smiles = chemical.smiles;
      this.inChI = chemical.inChI;
      this.inChIKey = chemical.inChIKey;
    }
  }

  /**
   * Default constructor.
   */
  public CellDesignerChemical() {
    super();
  }

  /**
   * @return the smiles
   * @see #smiles
   */
  public String getSmiles() {
    return smiles;
  }

  /**
   * @param smiles
   *          the smiles to set
   * @see #smiles
   */
  public void setSmiles(String smiles) {
    this.smiles = smiles;
  }

  /**
   * @return the inChI
   * @see #inChI
   */
  public String getInChI() {
    return inChI;
  }

  /**
   * @param inChI
   *          the inChI to set
   * @see #inChI
   */
  public void setInChI(String inChI) {
    this.inChI = inChI;
  }

  /**
   * @return the inChIKey
   * @see #inChIKey
   */
  public String getInChIKey() {
    return inChIKey;
  }

  /**
   * @param inChIKey
   *          the inChIKey to set
   * @see #inChIKey
   */
  public void setInChIKey(String inChIKey) {
    this.inChIKey = inChIKey;
  }

  @Override
  protected void setModelObjectFields(T result) {
    super.setModelObjectFields(result);
    result.setInChI(inChI);
    result.setInChIKey(inChIKey);
    result.setSmiles(smiles);
  }

}
