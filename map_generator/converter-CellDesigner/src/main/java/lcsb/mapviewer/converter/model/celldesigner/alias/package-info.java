/**
 * This package contains converters for CellDesigner xml nodes representing
 * aliases.
 */
package lcsb.mapviewer.converter.model.celldesigner.alias;
