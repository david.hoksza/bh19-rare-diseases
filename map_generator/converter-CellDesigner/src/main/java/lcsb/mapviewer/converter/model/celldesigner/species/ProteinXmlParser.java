package lcsb.mapviewer.converter.model.celldesigner.species;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.*;

/**
 * Class that performs parsing of the CellDesigner xml for
 * {@link CellDesignerProtein} object.
 * 
 * @author Piotr Gawron
 * 
 */

public class ProteinXmlParser extends AbstractElementXmlParser<CellDesignerProtein<?>, Protein> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ProteinXmlParser.class.getName());

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  private ModificationResidueXmlParser modificationResidueXmlParser;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   * 
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public ProteinXmlParser(CellDesignerElementCollection elements) {
    this.elements = elements;
    this.modificationResidueXmlParser = new ModificationResidueXmlParser(elements);
  }

  @Override
  public Pair<String, CellDesignerProtein<?>> parseXmlElement(Node proteinNode) throws InvalidXmlSchemaException {
    CellDesignerProtein<?> protein = null;
    String value = XmlParser.getNodeAttr("type", proteinNode);
    ProteinMapping mapping = ProteinMapping.getMappingByString(value);
    if (mapping != null) {
      protein = mapping.createProtein();
    } else {
      throw new InvalidXmlSchemaException("Protein node in Sbml model is of unknown type: " + value);
    }

    String identifier = XmlParser.getNodeAttr("id", proteinNode);
    protein.setName(decodeName(XmlParser.getNodeAttr("name", proteinNode)));
    NodeList list = proteinNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:listOfModificationResidues")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:modificationResidue")) {
                protein.addModificationResidue(getModificationResidue(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfModificationResidues " + residueNode.getNodeName());
              }
            }
          }
        } else if (node.getNodeName().equals("celldesigner:listOfBindingRegions")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:bindingRegion")) {
                protein.addModificationResidue(getBindingRegion(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfModificationResidues " + residueNode.getNodeName());
              }
            }
          }
        } else if (node.getNodeName().equals("celldesigner:notes")) {
          protein.setNotes(getRap().getNotes(node));
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:protein " + node.getNodeName());
        }
      }
    }

    return new Pair<String, CellDesignerProtein<?>>(identifier, protein);
  }

  @Override
  public String toXml(Protein protein) {
    String attributes = "";
    String result = "";
    attributes += " id=\"p_" + elements.getElementId(protein) + "\"";
    if (protein.getName() != null && !protein.getName().equals("")) {
      attributes += " name=\"" + XmlParser.escapeXml(encodeName(protein.getName())) + "\"";
    }
    String type = null;
    ProteinMapping mapping = ProteinMapping.getMappingByClass(protein.getClass());
    if (mapping != null) {
      type = mapping.getCellDesignerString();
    } else {
      throw new InvalidArgumentException("Invalid protein class type: " + protein.getClass().getName());
    }
    attributes += " type=\"" + type + "\"";
    result += "<celldesigner:protein" + attributes + ">\n";

    List<Residue> residues = new ArrayList<>();
    List<BindingRegion> bindingRegions = new ArrayList<>();
    for (ModificationResidue mr : protein.getModificationResidues()) {
      if (mr instanceof Residue) {
        residues.add((Residue) mr);
      } else if (mr instanceof BindingRegion) {
        bindingRegions.add((BindingRegion) mr);
      } else {
        throw new InvalidArgumentException("Don't know how to handle: " + mr.getClass());
      }
    }
    if (residues.size() > 0) {
      result += "<celldesigner:listOfModificationResidues>";
      for (Residue mr : residues) {
        result += modificationResidueXmlParser.toXml(mr);
      }
      result += "</celldesigner:listOfModificationResidues>\n";
    }
    if (bindingRegions.size() > 0) {
      result += "<celldesigner:listOfBindingRegions>";
      for (BindingRegion mr : bindingRegions) {
        result += modificationResidueXmlParser.toXml(mr);
      }
      result += "</celldesigner:listOfBindingRegions>\n";
    }
    // ignore notes - all notes will be stored in species
    // result +=
    // "<celldesigner:notes>"+protein.getNotes()+"</celldesigner:notes>";
    result += "</celldesigner:protein>\n";
    return result;
  }

  /**
   * Parses CellDesigner xml node for ModificationResidue.
   * 
   * @param residueNode
   *          xml node to parse
   * @return {@link CellDesignerModificationResidue} object created from the node
   * @throws InvalidXmlSchemaException
   *           thrown when input xml node doesn't follow defined schema
   */
  private CellDesignerModificationResidue getModificationResidue(Node residueNode) throws InvalidXmlSchemaException {
    CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(XmlParser.getNodeAttr("id", residueNode));
    residue.setName(XmlParser.getNodeAttr("name", residueNode));
    residue.setSide(XmlParser.getNodeAttr("side", residueNode));
    residue.setAngle(XmlParser.getNodeAttr("angle", residueNode));
    residue.setModificationType(ModificationType.RESIDUE);
    NodeList list = residueNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException(
            "Unknown element of celldesigner:modificationResidue " + node.getNodeName());
      }
    }
    return residue;
  }

  /**
   * Parses CellDesigner xml node for ModificationResidue.
   * 
   * @param residueNode
   *          xml node to parse
   * @return {@link CellDesignerModificationResidue} object created from the node
   * @throws InvalidXmlSchemaException
   *           thrown when input xml node doesn't follow defined schema
   */
  private CellDesignerModificationResidue getBindingRegion(Node residueNode) throws InvalidXmlSchemaException {
    CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(XmlParser.getNodeAttr("id", residueNode));
    residue.setName(XmlParser.getNodeAttr("name", residueNode));
    residue.setSize(XmlParser.getNodeAttr("size", residueNode));
    residue.setAngle(XmlParser.getNodeAttr("angle", residueNode));
    residue.setModificationType(ModificationType.BINDING_REGION);
    NodeList list = residueNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException(
            "Unknown element of celldesigner:modificationResidue " + node.getNodeName());
      }
    }
    return residue;
  }

}
