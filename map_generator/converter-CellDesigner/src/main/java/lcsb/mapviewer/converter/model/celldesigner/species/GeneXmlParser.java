package lcsb.mapviewer.converter.model.celldesigner.species;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGene;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * Class that performs parsing of the CellDesigner xml for
 * {@link CellDesignerGene} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class GeneXmlParser extends AbstractElementXmlParser<CellDesignerGene, Gene> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(GeneXmlParser.class.getName());

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  private ModificationResidueXmlParser modificationResidueXmlParser;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   * 
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public GeneXmlParser(CellDesignerElementCollection elements) {
    this.elements = elements;
    this.modificationResidueXmlParser = new ModificationResidueXmlParser(elements);
  }

  @Override
  public Pair<String, CellDesignerGene> parseXmlElement(Node geneNode) throws InvalidXmlSchemaException {
    CellDesignerGene gene = new CellDesignerGene();
    String identifier = XmlParser.getNodeAttr("id", geneNode);
    gene.setName(decodeName(XmlParser.getNodeAttr("name", geneNode)));
    NodeList list = geneNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:notes")) {
          gene.setNotes(getRap().getNotes(node));
        } else if (node.getNodeName().equals("celldesigner:listOfRegions")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:region")) {
                gene.addModificationResidue(getModificationResidue(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfRegions " + residueNode.getNodeName());
              }
            }
          }
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:gene " + node.getNodeName());
        }
      }
    }
    return new Pair<String, CellDesignerGene>(identifier, gene);
  }

  @Override
  public String toXml(Gene gene) {
    String attributes = "";
    String result = "";
    attributes += " id=\"g_" + elements.getElementId(gene) + "\"";
    if (!gene.getName().equals("")) {
      attributes += " name=\"" + XmlParser.escapeXml(encodeName(gene.getName())) + "\"";
    }
    result += "<celldesigner:gene" + attributes + ">";
    result += "<celldesigner:notes>";
    result += "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title/></head><body>";
    RestAnnotationParser rap = new RestAnnotationParser();
    result += rap.createAnnotationString(gene);
    result += gene.getNotes();
    result += "</body></html>";
    result += "</celldesigner:notes>";

    if (gene.getModificationResidues().size() > 0) {
      result += "<celldesigner:listOfRegions>\n";
      for (ModificationResidue mr : gene.getModificationResidues()) {
        result += modificationResidueXmlParser.toXml(mr);
      }
      result += "</celldesigner:listOfRegions>\n";
    }

    result += "</celldesigner:gene>";
    return result;
  }

  /**
   * Parse modification for a gene.
   *
   * @param residueNode
   *          source xml node
   * @return object representing modification
   * @throws InvalidXmlSchemaException
   *           thrown when input xml node doesn't follow defined schema
   */
  CellDesignerModificationResidue getModificationResidue(Node residueNode) throws InvalidXmlSchemaException {
    CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(XmlParser.getNodeAttr("id", residueNode));
    residue.setName(XmlParser.getNodeAttr("name", residueNode));
    residue.setSide(XmlParser.getNodeAttr("side", residueNode));
    residue.setSize(XmlParser.getNodeAttr("size", residueNode));
    residue.setActive(XmlParser.getNodeAttr("active", residueNode));
    residue.setAngle(XmlParser.getNodeAttr("pos", residueNode));
    String type = XmlParser.getNodeAttr("type", residueNode);
    try {
      residue.setModificationType(ModificationType.getByCellDesignerName(type));
    } catch (InvalidArgumentException e) {
      throw new InvalidXmlSchemaException(e);
    }
    NodeList list = residueNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException("Unknown element of celldesigner:region " + node.getNodeName());
      }
    }
    return residue;
  }

}
