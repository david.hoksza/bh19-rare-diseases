package lcsb.mapviewer.converter.model.celldesigner.structure;

import java.awt.*;
import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.PositionToCompartment;
import lcsb.mapviewer.model.map.species.field.StructuralState;

/**
 * Generic CellDesigner element.
 * 
 * @param <T>
 *          model class that corresponds to this cell designer structure
 * @author Piotr Gawron
 * 
 */
public class CellDesignerSpecies<T extends Species> extends CellDesignerElement<T> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private static final int STRUCTURAL_STATE_FONT_SIZE = 10;

  private static final double MIN_STRUCTURAL_STATE_WIDTH = 60;

  private static final int TEXT_MARGIN_FOR_STRUCTURAL_STATE_DESC = 40;

  private static final int MIN_STRUCTURAL_STATE_HEIGHT = 20;

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(CellDesignerSpecies.class.getName());

  /**
   * Identifier of the species. It must be unique within the model.
   */
  private String idSpecies;

  /**
   * Initial amount of species.
   */
  private Double initialAmount = null;

  /**
   * Charge of the species.
   */
  private Integer charge = null;

  /**
   * Initial concentration of species.
   */
  private Double initialConcentration = null;

  /**
   * Is only substance units allowed.
   */
  private Boolean onlySubstanceUnits = null;

  /**
   * How many dimers are in this species.
   */
  private int homodimer = 1;

  /**
   * Position on the compartment.
   */
  private PositionToCompartment positionToCompartment = null;

  /**
   * Is species hypothetical.
   */
  private Boolean hypothetical = null;

  /**
   * SBML allows to store substanceUnit as either basic SI unit type or complex
   * unit definition. To map it properly to database we need to separate the type
   * into two properties. This property is responsible for basic SI unit type.
   */
  private SbmlUnitType substanceUnitRawType;

  /**
   * SBML allows to store substanceUnit as either basic SI unit type or complex
   * unit definition. To map it properly to database we need to separate the type
   * into two properties. This property is responsible for complex unit
   * definition.
   */
  private SbmlUnit substanceUnitComlexType;

  private Boolean boundaryCondition;

  private Boolean constant;

  /**
   * Constructor that copies the data from species given in the argument.
   *
   * @param species
   *          parent species from which we want to copy data
   */
  public CellDesignerSpecies(CellDesignerSpecies<?> species) {
    super(species);
    this.hypothetical = species.hypothetical;
    this.idSpecies = species.idSpecies;
    this.positionToCompartment = species.positionToCompartment;
    this.initialAmount = species.initialAmount;
    this.charge = species.charge;
    this.initialConcentration = species.initialConcentration;
    this.onlySubstanceUnits = species.onlySubstanceUnits;
    this.homodimer = species.homodimer;
    this.constant = species.constant;
    this.substanceUnitComlexType = species.substanceUnitComlexType;
    this.substanceUnitRawType = species.substanceUnitRawType;
    this.boundaryCondition = species.boundaryCondition;
  }

  /**
   * Default constructor.
   */
  public CellDesignerSpecies() {
    super();
    idSpecies = "";
  }

  /**
   * Default constructor with species identifier as a parameter.
   *
   * @param id
   *          species identifier
   */
  public CellDesignerSpecies(String id) {
    this();
    this.idSpecies = id;
    this.setName(id);
  }

  /**
   * Updates species with the value from the species given in the parameter.
   *
   * @param species
   *          object from which we are updating information
   */
  public void update(CellDesignerSpecies<?> species) {
    if (getName() == null || getName().equals("")) {
      setName(species.getName());
    } else if (!getName().trim().equals(species.getName().trim())) {
      String id = species.getElementId();
      if (id == null || id.equals("")) {
        id = getElementId();
      }
      logger.warn(
          "Two different names in species with id =" + id + ": \"" + species.getName() + "\", \"" + getName() + "\"");
      setName(species.getName());
    }
    if (idSpecies == null || idSpecies.equals("")) {
      setElementId(species.getElementId());
    }
    for (MiriamData md : species.getMiriamData()) {
      if (!getMiriamData().contains(md)) {
        addMiriamData(new MiriamData(md));
      }
    }
    if (this.getNotes() == null || this.getNotes().trim().equals("")) {
      setNotes(species.getNotes());
    } else if (species.getNotes() != null && !species.getNotes().trim().equals("")
        && !this.getNotes().equals(species.getNotes())) {
      String string1 = this.getNotes();

      String string2 = species.getNotes();

      string1 = string1.trim();
      string2 = string2.trim();
      if (string2.toLowerCase().contains(string1.toLowerCase())) {
        setNotes(species.getNotes());
      } else if (!string1.toLowerCase().contains(string2.toLowerCase())) {
        // insert new information
        setNotes(string2 + getNotes());
      }
    }

    if (getParent() == null) {
      setParent(species.getParent());
    }

    if (species.getHypothetical() != null) {
      setHypothetical(species.getHypothetical());
    }

    if (getSymbol() != null && !getSymbol().equals("")) {
      if (species.getSymbol() != null && !species.getSymbol().equals("")) {
        if (!species.getSymbol().equals(getSymbol())) {
          logger.warn("Different symbol names: " + species.getSymbol() + ", " + getSymbol() + ". Ignoring...");
        }
      }
    } else {
      setSymbol(species.getSymbol());
    }

    if (getFullName() != null && !getFullName().equals("")) {
      if (species.getFullName() != null && !species.getFullName().equals("")) {
        if (!species.getFullName().equals(getFullName())) {
          logger.warn("Different symbol names: " + species.getFullName() + ", " + getFullName() + ". Ignoring...");
        }
      }
    } else {
      setFullName(species.getFullName());
    }

    for (String string : species.getSynonyms()) {
      if (!getSynonyms().contains(string)) {
        getSynonyms().add(string);
      }
    }

    for (String string : species.getFormerSymbols()) {
      if (!getFormerSymbols().contains(string)) {
        getFormerSymbols().add(string);
      }
    }
    if (species.getHomodimer() != 1) {
      setHomodimer(species.getHomodimer());
    }
    if (species.getOnlySubstanceUnits() != null) {
      setOnlySubstanceUnits(species.getOnlySubstanceUnits());
    }
    if (species.getInitialAmount() != null) {
      setInitialAmount(species.getInitialAmount());
    }
    if (species.getInitialConcentration() != null) {
      setInitialConcentration(species.getInitialConcentration());
    }
    if (species.isConstant() != null) {
      setConstant(species.isConstant());
    }
    if (species.isBoundaryCondition() != null) {
      setBoundaryCondition(species.isBoundaryCondition());
    }
  }

  /**
   * Is species hypothetical or not.
   *
   * @return <code>true</code> if species is hypothetical, <code>false</code>
   *         otherwise
   */
  public boolean isHypothetical() {
    if (hypothetical == null) {
      return false;
    }
    return hypothetical;
  }

  @Override
  public String getElementId() {
    return this.idSpecies;
  }

  @Override
  public void setElementId(String id) {
    if (this.idSpecies.equals(id) || this.idSpecies.equals("")) {
      this.idSpecies = id;
    } else if (!id.equals("")) {
      throw new InvalidArgumentException(
          "Cannot change identifier of the species in the runtime. OLD: " + this.idSpecies + ", NEW: " + id);
    } else {
      this.idSpecies = id;
    }
  }

  @Override
  public CellDesignerSpecies<T> copy() {
    CellDesignerSpecies<T> result = new CellDesignerSpecies<T>(this);

    result.idSpecies = idSpecies;
    result.setNotes(getNotes());
    result.setHypothetical(hypothetical);
    // correct this

    result.setParent(getParent());

    return result;
  }

  @Override
  public T createModelElement(String aliasId) {
    throw new NotImplementedException("" + this.getClass());
  }

  @Override
  protected void setModelObjectFields(T result) {
    super.setModelObjectFields(result);
    result.setInitialAmount(initialAmount);
    result.setCharge(charge);
    result.setInitialConcentration(initialConcentration);
    if (result.getInitialAmount() == null && result.getInitialConcentration() == null) {
      result.setInitialConcentration(0.0);
    }
    if (onlySubstanceUnits != null) {
      result.setOnlySubstanceUnits(onlySubstanceUnits);
    }
    if (constant != null) {
      result.setConstant(constant);
    }
    if (boundaryCondition != null) {
      result.setBoundaryCondition(boundaryCondition);
    }
    if (substanceUnitComlexType != null) {
      result.setSubstanceUnits(substanceUnitComlexType);
    } else {
      result.setSubstanceUnits(substanceUnitRawType);
    }
    result.setHomodimer(homodimer);
    result.setPositionToCompartment(positionToCompartment);
    result.setHypothetical(hypothetical);
  }

  /**
   * @return the initialAmount
   * @see #initialAmount
   */
  public Double getInitialAmount() {
    return initialAmount;
  }

  /**
   * Sets initial amount from text.
   *
   * @param text
   *          initial amount in string format
   * @see #initialAmount
   */

  public void setInitialAmount(String text) {
    if (text != null && !text.trim().equals("")) {
      try {
        initialAmount = Double.parseDouble(text);
      } catch (NumberFormatException e) {
        throw new InvalidArgumentException("Invalid species amount: " + text, e);
      }
    } else {
      initialAmount = null;
    }

  }

  /**
   * @param initialAmount
   *          the initialAmount to set
   * @see #initialAmount
   */
  public void setInitialAmount(Double initialAmount) {
    this.initialAmount = initialAmount;
  }

  /**
   * @return the charge
   * @see #charge
   */
  public Integer getCharge() {
    return charge;
  }

  /**
   * Sets charge from text.
   *
   * @param text
   *          charge in string format
   * @see #charge
   */
  public void setCharge(String text) {
    if (text != null && !text.trim().equals("")) {
      try {
        charge = Integer.parseInt(text);
      } catch (NumberFormatException e) {
        throw new InvalidArgumentException("Invalid species charge: " + text, e);
      }
    } else {
      charge = null;
    }

  }

  /**
   * @param charge
   *          the charge to set
   * @see #charge
   */
  public void setCharge(Integer charge) {
    this.charge = charge;
  }

  /**
   * @return the onlySubstanceUnits
   * @see #onlySubstanceUnits
   */
  public Boolean getOnlySubstanceUnits() {
    return onlySubstanceUnits;
  }

  /**
   * Sets OnlySubstanceUnits amount from text.
   *
   * @param text
   *          OnlySubstanceUnits in string format
   * @see #onlySubstanceUnits
   */
  public void setOnlySubstanceUnits(String text) {
    onlySubstanceUnits = textToBoolean(text, "Invalid species value for only substance unit boolean: " + text);
  }

  /**
   * @param onlySubstanceUnits
   *          the onlySubstanceUnits to set
   * @see #onlySubstanceUnits
   */
  public void setOnlySubstanceUnits(Boolean onlySubstanceUnits) {
    this.onlySubstanceUnits = onlySubstanceUnits;
  }

  /**
   * @return the initialConcentration
   * @see #initialConcentration
   */
  public Double getInitialConcentration() {
    return initialConcentration;
  }

  /**
   * Sets initial concentration amount from text.
   *
   * @param text
   *          initial concentration in string format
   * @see #initialConcentration
   */
  public void setInitialConcentration(String text) {
    if (text != null && !text.trim().equals("")) {
      try {
        initialConcentration = Double.parseDouble(text);
      } catch (NumberFormatException e) {
        throw new InvalidArgumentException("Invalid species initial concentration: " + text, e);
      }
    } else {
      initialConcentration = null;
    }
  }

  /**
   * @param initialConcentration
   *          the initialConcentration to set
   * @see #initialConcentration
   */
  public void setInitialConcentration(Double initialConcentration) {
    this.initialConcentration = initialConcentration;
  }

  /**
   * @return the hypothetical
   * @see #hypothetical
   */
  public Boolean getHypothetical() {
    return hypothetical;
  }

  /**
   * Set hypothetical flag from text input.
   *
   * @param text
   *          string with true/false value that determines hypothetical state.
   * @see #hypothetical
   */
  public void setHypothetical(String text) {
    hypothetical = text.equals("true");
  }

  /**
   * @param hypothetical
   *          the hypothetical to set
   * @see #hypothetical
   */
  public void setHypothetical(Boolean hypothetical) {
    this.hypothetical = hypothetical;
  }

  /**
   * @return the onlySubstanceUnits
   * @see #onlySubstanceUnits
   */
  public Boolean hasOnlySubstanceUnits() {
    return onlySubstanceUnits;
  }

  /**
   * @return the positionToCompartment
   * @see #positionToCompartment
   */
  public PositionToCompartment getPositionToCompartment() {
    return positionToCompartment;
  }

  /**
   * @param positionToCompartment
   *          the positionToCompartment to set
   * @see #positionToCompartment
   */
  public void setPositionToCompartment(PositionToCompartment positionToCompartment) {
    this.positionToCompartment = positionToCompartment;
  }

  /**
   * @return the homodimer
   * @see #homodimer
   */
  public int getHomodimer() {
    return homodimer;
  }

  /**
   * @param homodimer
   *          the homodimer to set
   * @see #homodimer
   */
  public void setHomodimer(int homodimer) {
    this.homodimer = homodimer;
  }

  public void setSubstanceUnits(SbmlUnitType substanceUnits) {
    this.substanceUnitRawType = substanceUnits;
  }

  public void setSubstanceUnits(SbmlUnit substanceUnits) {
    this.substanceUnitComlexType = substanceUnits;
  }

  public Object getSubstanceUnit() {
    if (substanceUnitComlexType != null) {
      return substanceUnitComlexType;
    } else if (substanceUnitRawType != null) {
      return substanceUnitRawType;
    } else {
      return null;
    }
  }

  public Boolean isBoundaryCondition() {
    return boundaryCondition;
  }

  public Boolean isConstant() {
    return constant;
  }

  public void setBoundaryCondition(String text) {
    boundaryCondition = textToBoolean(text, "Invalid species value for only substance unit boolean: " + text);
  }

  private Boolean textToBoolean(String text, String errorMessage) {
    Boolean result;
    if (text != null && !text.trim().equals("")) {
      if (text.equalsIgnoreCase("TRUE")) {
        result = true;
      } else if (text.equalsIgnoreCase("FALSE")) {
        result = false;
      } else {
        throw new InvalidArgumentException(errorMessage);
      }
    } else {
      result = false;
    }
    return result;
  }

  public void setConstant(String text) {
    constant = textToBoolean(text, "Invalid species value for only constant boolean: " + text);
  }

  public void setBoundaryCondition(Boolean boundaryCondition) {
    this.boundaryCondition = boundaryCondition;
  }

  public void setConstant(Boolean constant) {
    this.constant = constant;
  }

  public void updateModelElementAfterLayoutAdded(Species element) {
  }

  protected StructuralState createStructuralState(Species species, String structuralState, Double angle) {
    if (structuralState == null) {
      return null;
    }

    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(species, false);

    Font font = new Font(Font.SANS_SERIF, 0, STRUCTURAL_STATE_FONT_SIZE);
    Canvas c = new Canvas();
    FontMetrics fm = c.getFontMetrics(font);

    double width = MIN_STRUCTURAL_STATE_WIDTH;
    double height = MIN_STRUCTURAL_STATE_HEIGHT;
    if (!structuralState.trim().equals("")) {
      width = Math.max(MIN_STRUCTURAL_STATE_WIDTH,
          fm.stringWidth(structuralState) + TEXT_MARGIN_FOR_STRUCTURAL_STATE_DESC);
    }
    width = Math.min(width, species.getWidth());

    StructuralState result = new StructuralState();
    result.setFontSize(STRUCTURAL_STATE_FONT_SIZE);
    result.setHeight(height);
    result.setWidth(width);
    result.setValue(structuralState);

    if (angle == null) {
      logger.warn("Angle is not defined using 0 as default");
      angle = 0.0;
    }
    Point2D position = converter.getResidueCoordinates(species, angle);
    position.setLocation(position.getX() - width / 2, position.getY() - height / 2);
    result.setPosition(position);

    return result;
  }

}
