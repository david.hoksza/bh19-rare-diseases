package lcsb.mapviewer.converter.model.celldesigner.alias;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.*;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.View;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Parser of CellDesigner xml used for parsing complex aliases. Important: Only
 * one instance per model should be used.
 * 
 * @author Piotr Gawron
 * 
 * @see Complex
 */
public class ComplexAliasXmlParser extends AbstractAliasXmlParser<Complex> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ComplexAliasXmlParser.class.getName());

  /**
   * Because of the CellDesigner xml model we have to store information about all
   * processed Complexes. This information later on is used for connecting
   * complexes in hierarchical view.
   */
  private Map<String, Complex> complexAliasesMapById = new HashMap<String, Complex>();

  /**
   * Because of the CellDesigner xml model we have to store information about
   * parents of all Complexes. This information later on is used for connecting
   * complexes in hierarchical view. We cannot do it immediately because some
   * complexes doesn't exist yet.
   */
  private Map<String, String> parents = new HashMap<String, String>();

  /**
   * Model for which we are parsing aliases.
   */
  private Model model = null;

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  /**
   * Default constructor with model object for which we parse data.
   * 
   * @param model
   *          model for which we parse elements
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public ComplexAliasXmlParser(CellDesignerElementCollection elements, Model model) {
    this.model = model;
    this.elements = elements;
  }

  @Override
  Complex parseXmlAlias(Node aliasNode) throws InvalidXmlSchemaException {

    String aliasId = XmlParser.getNodeAttr("id", aliasNode);

    String speciesId = XmlParser.getNodeAttr("species", aliasNode);
    CellDesignerComplexSpecies species = (CellDesignerComplexSpecies) elements.getElementByElementId(speciesId);
    if (species == null) {
      throw new InvalidXmlSchemaException(
          "No species with id=\"" + speciesId + "\" for complex alias \"" + aliasId + "\"");
    }
    Complex result = species.createModelElement(aliasId);
    elements.addElement(species, aliasId);

    String state = "usual";
    NodeList nodes = aliasNode.getChildNodes();
    View usualView = null;
    View briefView = null;
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:activity")) {
          result.setActivity(XmlParser.getNodeValue(node).equalsIgnoreCase("active"));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          result.setX(XmlParser.getNodeAttr("X", node));
          result.setY(XmlParser.getNodeAttr("Y", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:font")) {
          result.setFontSize(XmlParser.getNodeAttr("size", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:view")) {
          state = XmlParser.getNodeAttr("state", node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:usualView")) {
          usualView = getCommonParser().getView(node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:briefView")) {
          briefView = getCommonParser().getView(node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:backupView")) {
          // not handled
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:info")) {
          processAliasState(node, result);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:backupSize")) {
          // not handled
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:structuralState")) {
          species.setStructuralStateAngle(Double.parseDouble(XmlParser.getNodeAttr("angle", node)));
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:speciesAlias: " + node.getNodeName());
        }
      }
    }

    View view = null;
    if (state.equalsIgnoreCase("usual")) {
      view = usualView;
    } else if (state.equalsIgnoreCase("brief")) {
      view = briefView;
    } else if (state.equalsIgnoreCase("complexparentbrief")) {
      view = briefView;
    }

    if (view != null) {
      // inner position defines the position in compartment or complexAlias
      // result.moveBy(view.innerPosition);
      result.setWidth(view.getBoxSize().width);
      result.setHeight(view.getBoxSize().height);
      result.setLineWidth(view.getSingleLine().getWidth());
      result.setFillColor(view.getColor());
    } else if (!state.equalsIgnoreCase("complexnoborder")) {
      throw new InvalidXmlSchemaException("No view (" + state + ") in ComplexAlias for " + result.getElementId());
    }
    result.setState(state);
    String compartmentAliasId = XmlParser.getNodeAttr("compartmentAlias", aliasNode);
    if (!compartmentAliasId.isEmpty()) {
      Compartment ca = model.getElementByElementId(compartmentAliasId);
      if (ca == null) {
        throw new InvalidXmlSchemaException("CompartmentAlias does not exist: " + compartmentAliasId);
      } else {
        result.setCompartment(ca);
        ca.addElement(result);
      }
    }
    String complexSpeciesAliasId = XmlParser.getNodeAttr("complexSpeciesAlias", aliasNode);
    if (!complexSpeciesAliasId.equals("")) {
      parents.put(result.getElementId(), complexSpeciesAliasId);
    }
    complexAliasesMapById.put(result.getElementId(), result);
    species.updateModelElementAfterLayoutAdded(result);
    return result;
  }

  @Override
  public String toXml(Complex complex) {
    Compartment ca = null;
    // we have to exclude artificial compartment aliases, because they aren't
    // exported to CellDesigner file
    if (complex.getCompartment() != null && !(complex.getCompartment() instanceof PathwayCompartment)) {
      ca = complex.getCompartment();
    } else if (complex.getComplex() == null) {
      ModelData modelData = complex.getModelData();
      if (modelData != null) {
        for (Compartment cAlias : modelData.getModel().getCompartments()) {
          if (!(cAlias instanceof PathwayCompartment) && cAlias.cross(complex)) {
            if (ca == null) {
              ca = cAlias;
            } else if (ca.getSize() > cAlias.getSize()) {
              ca = cAlias;
            }
          }
        }
      }
    }

    Complex complexAlias = complex.getComplex();

    String compartmentAliasId = null;
    if (ca != null) {
      compartmentAliasId = ca.getElementId();
    }
    StringBuilder sb = new StringBuilder("");
    sb.append("<celldesigner:complexSpeciesAlias ");
    sb.append("id=\"" + complex.getElementId() + "\" ");
    sb.append("species=\"" + elements.getElementId(complex) + "\" ");
    if (compartmentAliasId != null) {
      sb.append("compartmentAlias=\"" + compartmentAliasId + "\" ");
    }
    if (complexAlias != null) {
      sb.append("complexSpeciesAlias=\"" + complexAlias.getElementId() + "\"");
    }
    sb.append(">\n");

    if (complex.getActivity() != null) {
      if (complex.getActivity()) {
        sb.append("<celldesigner:activity>active</celldesigner:activity>\n");
      } else {
        sb.append("<celldesigner:activity>inactive</celldesigner:activity>\n");
      }
    }

    sb.append("<celldesigner:bounds ");
    sb.append("x=\"" + complex.getX() + "\" ");
    sb.append("y=\"" + complex.getY() + "\" ");
    sb.append("w=\"" + complex.getWidth() + "\" ");
    sb.append("h=\"" + complex.getHeight() + "\" ");
    sb.append("/>\n");

    sb.append(createFontTag(complex));

    sb.append("<celldesigner:view state=\"usual\"/>\n");
    sb.append("<celldesigner:usualView>");
    sb.append("<celldesigner:innerPosition x=\"" + complex.getX() + "\" y=\"" + complex.getY() + "\"/>");
    sb.append("<celldesigner:boxSize width=\"" + complex.getWidth() + "\" height=\"" + complex.getHeight() + "\"/>");
    sb.append("<celldesigner:singleLine width=\"" + complex.getLineWidth() + "\"/>");
    sb.append("<celldesigner:paint color=\"" + XmlParser.colorToString(complex.getFillColor()) + "\" scheme=\"Color\"/>");
    sb.append("</celldesigner:usualView>\n");
    sb.append("<celldesigner:briefView>");
    sb.append("<celldesigner:innerPosition x=\"" + complex.getX() + "\" y=\"" + complex.getY() + "\"/>");
    sb.append("<celldesigner:boxSize width=\"" + complex.getWidth() + "\" height=\"" + complex.getHeight() + "\"/>");
    sb.append("<celldesigner:singleLine width=\"" + complex.getWidth() + "\"/>");
    sb.append("<celldesigner:paint color=\"" + XmlParser.colorToString(complex.getFillColor()) + "\" scheme=\"Color\"/>");
    sb.append("</celldesigner:briefView>\n");

    sb.append(createStructuralStateTag(complex));
    sb.append("</celldesigner:complexSpeciesAlias>\n");
    return sb.toString();
  }

  /**
   * Process node with information about alias state and puts data into alias.
   *
   * @param node
   *          node where information about alias state is stored
   * @param alias
   *          alias object to be modified if necessary
   */
  private void processAliasState(Node node, Species alias) {
    String state = XmlParser.getNodeAttr("state", node);
    if ("open".equalsIgnoreCase(state)) {
      String prefix = XmlParser.getNodeAttr("prefix", node);
      String label = XmlParser.getNodeAttr("label", node);
      alias.setStatePrefix(prefix);
      alias.setStateLabel(label);
    } else if ("empty".equalsIgnoreCase(state)) {
      return;
    } else if (state == null || state.isEmpty()) {
      return;
    } else {
      throw new NotImplementedException("[Alias: " + alias.getElementId() + "] Unkown alias state: " + state);
    }

  }

  /**
   * Adds parent reference for the complexAlias.
   *
   * @param alias
   *          alias for which we want to add parent information
   */
  public void addReference(Complex alias) {
    String parentId = parents.get(alias.getElementId());
    if (parentId != null) {
      Complex ca = complexAliasesMapById.get(parentId);
      if (ca == null) {
        throw new InvalidArgumentException(
            "Parent complex alias does not exist: " + parentId + " for alias: " + alias.getElementId());
      } else {
        alias.setComplex(ca);
        ca.addSpecies(alias);
      }
    }
  }
}
