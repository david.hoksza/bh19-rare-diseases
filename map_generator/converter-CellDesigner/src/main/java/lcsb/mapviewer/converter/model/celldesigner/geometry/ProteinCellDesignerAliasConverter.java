package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.*;
import java.awt.geom.*;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * Class that provides CellDesigner specific graphical information for Protein.
 * It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<Protein> {

  /**
   * How big should be the arc in rectangle for generic protein representation.
   */
  private static final int GENERIC_PROTEIN_RECTANGLE_CORNER_ARC_SIZE = 10;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(ProteinCellDesignerAliasConverter.class);
  /**
   * Helps in providing human readable identifiers of elements for logging.
   */
  private ElementUtils eu = new ElementUtils();

  /**
   * Default constructor.
   *
   * @param sbgn
   *          Should the converter use sbgn standard
   */
  protected ProteinCellDesignerAliasConverter(boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(Protein alias, CellDesignerAnchor anchor) {
    Point2D result = null;
    if (invalidAnchorPosition(alias, anchor)) {
      result = alias.getCenter();
    } else {
      int homodir = alias.getHomodimer();

      alias.setWidth(alias.getWidth() - AbstractCellDesignerAliasConverter.HOMODIMER_OFFSET * (homodir - 1));
      alias.setHeight(alias.getHeight() - AbstractCellDesignerAliasConverter.HOMODIMER_OFFSET * (homodir - 1));

      if (alias instanceof GenericProtein) {
        result = getRectangleTransformation().getPointOnRectangleByAnchor(alias.getX(), alias.getY(), alias.getWidth(),
            alias.getHeight(), anchor);
      } else if (alias instanceof ReceptorProtein) {
        ArrayList<Point2D> points = getReceptorPoints(alias);
        result = getPolygonTransformation().getPointOnPolygonByAnchor(points, anchor);
      } else if (alias instanceof IonChannelProtein) {
        result = getRectangleTransformation().getPointOnRectangleByAnchor(alias.getX(), alias.getY(), alias.getWidth(),
            alias.getHeight(), anchor);
      } else if (alias instanceof TruncatedProtein) {
        ArrayList<Point2D> points = getTruncatedPoints(alias);
        result = getPolygonTransformation().getPointOnPolygonByAnchor(points, anchor);
      } else {
        throw new NotImplementedException(eu.getElementTag(alias) + "Unknown type: " + alias.getClass());
      }

      alias.setWidth(alias.getWidth() + AbstractCellDesignerAliasConverter.HOMODIMER_OFFSET * (homodir - 1));
      alias.setHeight(alias.getHeight() + AbstractCellDesignerAliasConverter.HOMODIMER_OFFSET * (homodir - 1));
    }
    return result;
  }

  @Override
  public PathIterator getBoundPathIterator(Protein alias) {
    if (alias instanceof GenericProtein) {
      return getGenericShape(alias).getPathIterator(new AffineTransform());
    } else if (alias instanceof ReceptorProtein) {
      return getReceptorShape(alias).getPathIterator(new AffineTransform());
    } else if (alias instanceof IonChannelProtein) {
      return getGenericShape(alias).getPathIterator(new AffineTransform());
    } else if (alias instanceof TruncatedProtein) {
      return getTruncatedShape(alias).getPathIterator(new AffineTransform());
    } else {
      throw new NotImplementedException(
          eu.getElementTag(alias) + "Not implemented protein converter for type: " + alias.getClass());
    }
  }

  /**
   * Returns shape of generic protein.
   * 
   * @param alias
   *          alias for which we are looking for a border
   * @return Shape object defining given alias
   */
  private Shape getGenericShape(Element alias) {
    return new RoundRectangle2D.Double(
        alias.getX(), alias.getY(), alias.getWidth(), alias.getHeight(), GENERIC_PROTEIN_RECTANGLE_CORNER_ARC_SIZE,
        GENERIC_PROTEIN_RECTANGLE_CORNER_ARC_SIZE);
  }

  /**
   * Returns shape of the Truncated protein as a list of points.
   * 
   * @param alias
   *          alias for which we are looking for a border
   * @return list of points defining border of the given alias
   */
  private ArrayList<Point2D> getTruncatedPoints(Element alias) {
    double x = alias.getX();
    double y = alias.getY();
    double width = alias.getWidth();
    double height = alias.getHeight();
    ArrayList<Point2D> points = new ArrayList<Point2D>();
    // CHECKSTYLE:OFF
    points.add(new Point2D.Double(x, y + height / 2));
    points.add(new Point2D.Double(x, y + height / 4));
    points.add(new Point2D.Double(x, y));
    points.add(new Point2D.Double(x + width / 4, y));
    points.add(new Point2D.Double(x + width / 2, y));
    points.add(new Point2D.Double(x + width * 3 / 4, y));
    points.add(new Point2D.Double(x + width, y));
    points.add(new Point2D.Double(x + width, y + height * 3 / 10));
    points.add(new Point2D.Double(x + width, y + height * 3 / 5));
    points.add(new Point2D.Double(x + width * 4 / 5, y + height * 2 / 5));
    points.add(new Point2D.Double(x + width * 4 / 5, y + height * 7 / 10));
    points.add(new Point2D.Double(x + width * 4 / 5, y + height));
    points.add(new Point2D.Double(x + width / 2, y + height));
    points.add(new Point2D.Double(x + width / 4, y + height));
    points.add(new Point2D.Double(x, y + height));
    points.add(new Point2D.Double(x, y + height * 3 / 4));

    // CHECKSTYLE:ON

    return points;
  }

  /**
   * Returns shape of receptor protein.
   * 
   * @param alias
   *          alias for which we are looking for a border
   * @return Shape object defining given alias
   */
  protected Shape getReceptorShape(Element alias) {
    Shape shape;
    GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
    ArrayList<Point2D> points = getReceptorPoints(alias);
    path.moveTo(points.get(0).getX(), points.get(0).getY());
    for (int i = 1; i < points.size(); i++) {
      path.lineTo(points.get(i).getX(), points.get(i).getY());
    }
    path.closePath();
    shape = path;
    return shape;
  }

  /**
   * Returns shape of truncated protein.
   * 
   * @param alias
   *          alias for which we are looking for a border
   * @return Shape object defining given alias
   */
  protected Shape getTruncatedShape(Element alias) {
    Shape shape;
    GeneralPath path = new GeneralPath();

    // CHECKSTYLE:OFF
    path.moveTo(alias.getX() + 10, alias.getY());
    path.lineTo(alias.getX() + alias.getWidth(), alias.getY());
    path.lineTo(alias.getX() + alias.getWidth(), alias.getY() + alias.getHeight() * 3 / 5);
    path.lineTo(alias.getX() + alias.getWidth() * 4 / 5, alias.getY() + alias.getHeight() * 2 / 5);
    path.lineTo(alias.getX() + alias.getWidth() * 4 / 5, alias.getY() + alias.getHeight());
    path.lineTo(alias.getX() + 10, alias.getY() + alias.getHeight());
    path.curveTo(
        alias.getX() + 5, alias.getY() + alias.getHeight() - 2, alias.getX() + 2, alias.getY() + alias.getHeight() - 5,
        alias.getX(),
        alias.getY() + alias.getHeight() - 10);
    path.lineTo(alias.getX(), alias.getY() + 10);
    path.curveTo(alias.getX() + 2, alias.getY() + 5, alias.getX() + 5, alias.getY() + 2, alias.getX() + 10,
        alias.getY());

    // CHECKSTYLE:ON

    path.closePath();
    shape = path;
    return shape;
  }

  /**
   * Returns shape of receptor protein as a list of points.
   * 
   * @param alias
   *          alias for which we are looking for a border
   * @return list of points defining border of the given alias
   */
  private ArrayList<Point2D> getReceptorPoints(Element alias) {
    double x = alias.getX();
    double y = alias.getY();
    double width = alias.getWidth();
    double height = alias.getHeight();
    ArrayList<Point2D> points = new ArrayList<Point2D>();

    // CHECKSTYLE:OFF
    points.add(new Point2D.Double(x, y + height * 2 / 5));
    points.add(new Point2D.Double(x, y));
    points.add(new Point2D.Double(x + width / 2, y + height / 5));
    points.add(new Point2D.Double(x + width, y));
    points.add(new Point2D.Double(x + width, y + height * 2 / 5));
    points.add(new Point2D.Double(x + width, y + height * 4 / 5));
    points.add(new Point2D.Double(x + width / 2, y + height));
    points.add(new Point2D.Double(x, y + height * 4 / 5));
    // CHECKSTYLE:ON

    return points;
  }

}
