package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * CellDesigner structure for storing information about base lines in the
 * reaction.
 * 
 * @author Piotr Gawron
 * 
 */
public class EditPoints {

  /**
   * List of internal points in CellDesigner format. Usually it means that edge
   * values represents base in which the internal points are stored. However, it
   * doesn't have to be true in all case. For more detail analyze usage of this
   * class in the parser.
   * 
   * @see lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory#createPolylineDataFromEditPoints(Point2D,
   *      Point2D, EditPoints)
   */
  private List<Point2D> points = new ArrayList<>();

  /**
   * This list in reactions with three base reactants+products (reactions that
   * implements
   * {@link lcsb.mapviewer.db.model.map.reaction.type.TwoReactantReactionInterface
   * TwoReactantReactionInterface} or
   * {@link lcsb.mapviewer.db.model.map.reaction.type.TwoProductReactionInterface
   * TwoProductReactionInterface}) defines where the different lines start in this
   * strange line description. See also usage of this field in CellDesigner
   * parser.
   */

  private List<Integer> lineStartingPoints = new ArrayList<>();

  /**
   * This value defines in which segment center of the reaction is put.
   */
  private Integer reactionCenterLineIndex = null;

  /**
   * @return the points
   * @see #points
   */
  public List<Point2D> getPoints() {
    return points;
  }

  /**
   * @param points
   *          the points to set
   * @see #points
   */
  public void setPoints(List<Point2D> points) {
    this.points = points;
  }

  public void addNum(int num) {
    lineStartingPoints.add(num);
  }

  public Integer getReactionCenterLineIndex() {
    return reactionCenterLineIndex;
  }

  public void setReactionCenterLineIndex(Integer index) {
    this.reactionCenterLineIndex = index;
  }

  /**
   * Returns number of points in the line.
   * 
   * @return number of points in the line
   */
  public int size() {
    return points.size();
  }

  public List<Integer> getLineStartingPoints() {
    return lineStartingPoints;
  }

}
