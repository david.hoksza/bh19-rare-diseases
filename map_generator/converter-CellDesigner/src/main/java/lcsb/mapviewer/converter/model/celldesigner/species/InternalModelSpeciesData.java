package lcsb.mapviewer.converter.model.celldesigner.species;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.structure.*;

/**
 * This class contains information about all species in model that are relevant
 * during parsing via CellDesigner parser.
 * 
 * @author Piotr Gawron
 * 
 */
public class InternalModelSpeciesData {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(InternalModelSpeciesData.class);

  /**
   * Collection of proteins. Used only by the CellDesigner parser.
   */
  private SpeciesCollection<CellDesignerProtein<?>> proteins;

  /**
   * Collection of genes. Used only by the CellDesigner parser.
   */
  private SpeciesCollection<CellDesignerGene> genes;

  /**
   * Collection of genes. Used only by the CellDesigner parser.
   */
  private SpeciesCollection<CellDesignerRna> rnas;

  /**
   * Collection of antisense rnas. Used only by the CellDesigner parser.
   */
  private SpeciesCollection<CellDesignerAntisenseRna> antisenseRnas;

  /**
   * Collection of phenotypes. Used only by the CellDesigner parser.
   */
  private SpeciesCollection<CellDesignerPhenotype> phenotypes;

  /**
   * Collection of ions. Used only by the CellDesigner parser.
   */
  private SpeciesCollection<CellDesignerIon> ions;

  /**
   * Collection of simple molecules. Used only by the CellDesigner parser.
   */
  private SpeciesCollection<CellDesignerSimpleMolecule> simpleMolecules;

  /**
   * Collection of drugs. Used only by the CellDesigner parser.
   */
  private SpeciesCollection<CellDesignerDrug> drugs;

  /**
   * Collection of unknowns. Used only by the CellDesigner parser.
   */
  private SpeciesCollection<CellDesignerUnknown> unknowns;

  /**
   * Collection of degraded elements. Used only by the CellDesigner parser.
   */
  private SpeciesCollection<CellDesignerDegraded> degradeds;

  /**
   * Collection of complexes.
   */
  private List<CellDesignerComplexSpecies> complexes = new ArrayList<>();

  /**
   * Default constructor.
   */
  public InternalModelSpeciesData() {
    proteins = new SpeciesCollection<>();
    genes = new SpeciesCollection<>();
    rnas = new SpeciesCollection<>();
    antisenseRnas = new SpeciesCollection<>();
    phenotypes = new SpeciesCollection<>();
    ions = new SpeciesCollection<>();
    simpleMolecules = new SpeciesCollection<>();
    drugs = new SpeciesCollection<>();
    unknowns = new SpeciesCollection<>();
    degradeds = new SpeciesCollection<>();
  }

  /**
   * 
   * @return {@link #proteins}
   */
  public Collection<CellDesignerProtein<?>> getProteins() {
    return proteins.getAll();
  }

  /**
   * 
   * @return {@link #genes}
   */
  public Collection<CellDesignerGene> getGenes() {
    return genes.getAll();
  }

  /**
   * 
   * @return {@link #rnas}
   */
  public Collection<CellDesignerRna> getRnas() {
    return rnas.getAll();
  }

  /**
   * 
   * @return {@link #antisenseRnas}
   */
  public Collection<CellDesignerAntisenseRna> getAntisenseRnas() {
    return antisenseRnas.getAll();
  }

  /**
   * Updates information about species in the dataset.
   * 
   * @param collection
   *          set of species to be updated, first element in pair defines
   *          celldesigner identifier, second is the species
   */
  public void updateSpecies(Collection<Pair<String, ? extends CellDesignerSpecies<?>>> collection) {
    for (Pair<String, ? extends CellDesignerSpecies<?>> species : collection) {
      updateSpecies(species.getRight(), species.getLeft());
    }
  }

  /**
   * Updates information about species in the dataset (identified by celldesigner
   * specific identifier).
   * 
   * @param sp
   *          species to be updated
   * @param identifier
   *          celldesigner identifier
   */
  public void updateSpecies(CellDesignerSpecies<?> sp, String identifier) {
    if (sp instanceof CellDesignerGene) {
      genes.updateSpeciesByLocalId((CellDesignerGene) sp, identifier);
    } else if (sp instanceof CellDesignerProtein) {
      proteins.updateSpeciesByLocalId((CellDesignerProtein<?>) sp, identifier);
    } else if (sp instanceof CellDesignerRna) {
      rnas.updateSpeciesByLocalId((CellDesignerRna) sp, identifier);
    } else if (sp instanceof CellDesignerAntisenseRna) {
      antisenseRnas.updateSpeciesByLocalId((CellDesignerAntisenseRna) sp, identifier);
    } else if (sp instanceof CellDesignerDegraded) {
      degradeds.updateSpeciesByLocalId((CellDesignerDegraded) sp, identifier);
    } else if (sp instanceof CellDesignerUnknown) {
      unknowns.updateSpeciesByLocalId((CellDesignerUnknown) sp, identifier);
    } else if (sp instanceof CellDesignerDrug) {
      drugs.updateSpeciesByLocalId((CellDesignerDrug) sp, identifier);
    } else if (sp instanceof CellDesignerSimpleMolecule) {
      simpleMolecules.updateSpeciesByLocalId((CellDesignerSimpleMolecule) sp, identifier);
    } else if (sp instanceof CellDesignerIon) {
      ions.updateSpeciesByLocalId((CellDesignerIon) sp, identifier);
    } else if (sp instanceof CellDesignerPhenotype) {
      phenotypes.updateSpeciesByLocalId((CellDesignerPhenotype) sp, identifier);
    } else if (sp instanceof CellDesignerComplexSpecies) {
      complexes.add((CellDesignerComplexSpecies) sp);
    } else {
      throw new InvalidArgumentException("Unknown species type: " + sp.getClass().getName());
    }
  }

  /**
   * Returns list of all species in dataset.
   * 
   * @return list of all species in dataset
   */
  public Collection<CellDesignerSpecies<?>> getAll() {
    List<CellDesignerSpecies<?>> result = new ArrayList<>();

    result.addAll(genes.getAll());
    result.addAll(rnas.getAll());
    result.addAll(antisenseRnas.getAll());
    result.addAll(phenotypes.getAll());
    result.addAll(ions.getAll());
    result.addAll(simpleMolecules.getAll());
    result.addAll(drugs.getAll());
    result.addAll(unknowns.getAll());
    result.addAll(degradeds.getAll());
    result.addAll(proteins.getAll());
    result.addAll(complexes);

    return result;
  }
}
