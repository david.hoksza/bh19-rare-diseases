/**
 * Provides classes that helps to transform geometry structures in CellDesigner
 * into more intuitive structures.
 */
package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;
