package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.*;
import java.awt.geom.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.field.AbstractRegionModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * Class that provides CellDesigner specific graphical information for Gene.
 * It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class GeneCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<Gene> {

  /**
   * How big should be the arc in rectangle for nucleic acid feature
   * representation.
   */
  private static final int RECTANGLE_CORNER_ARC_SIZE = 5;
  /**
   * Default class logger
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(GeneCellDesignerAliasConverter.class);

  /**
   * Default constructor.
   *
   * @param sbgn
   *          Should the converter use SBGN standard
   */
  protected GeneCellDesignerAliasConverter(boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(Gene alias, CellDesignerAnchor anchor) {
    if (invalidAnchorPosition(alias, anchor)) {
      return alias.getCenter();
    }
    return getRectangleTransformation().getPointOnRectangleByAnchor(alias.getX(), alias.getY(), alias.getWidth(),
        alias.getHeight(), anchor);
  }

  @Override
  public PathIterator getBoundPathIterator(Gene alias) {
    return getGeneShape(alias).getPathIterator(new AffineTransform());
  }

  @Override
  public Double getCellDesignerPositionByCoordinates(ModificationResidue mr) {
    Double result = (mr.getPosition().getX() - mr.getSpecies().getX()) / mr.getSpecies().getWidth();
    result = Math.min(result, 1.0);
    result = Math.max(result, 0.0);
    return result;
  }

  @Override
  public Point2D getCoordinatesByPosition(Element element, Double pos, Double modificationWidth) {
    double x = element.getX() + element.getWidth() * pos;
    x = Math.max(element.getX() + modificationWidth / 2, x);
    x = Math.min(element.getX() + element.getWidth() - modificationWidth / 2, x);

    Point2D result = new Point2D.Double(x, element.getY());
    return result;
  }

  @Override
  public Double getCellDesignerSize(ModificationResidue mr) {
    if (mr instanceof AbstractRegionModification) {
      return ((AbstractRegionModification) mr).getWidth() / mr.getSpecies().getWidth();
    }
    throw new NotImplementedException("Not implemented for: " + this.getClass() + ", " + mr.getClass());
  }

  @Override
  public Double getWidthBySize(Element element, Double size) {
    return size * element.getWidth();
  }

  /**
   * Shape representation of the gene alias.
   *
   * @param alias
   *          alias for which we are looking for a Shape
   * @return Shape object that represents alias
   */
  private Shape getGeneShape(Element alias) {
    if (!isSbgn()) {
      Shape shape;
      shape = new Rectangle2D.Double(alias.getX(), alias.getY(), alias.getWidth(), alias.getHeight());
      return shape;
    } else {
      GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
      double x = alias.getX(), y = alias.getY(), width = alias.getWidth(), height = alias.getHeight();

      path.moveTo(x, y);
      path.lineTo(x, y + height - RECTANGLE_CORNER_ARC_SIZE);
      path.curveTo(x, y + height, x + RECTANGLE_CORNER_ARC_SIZE, y + height, x + RECTANGLE_CORNER_ARC_SIZE, y + height);
      path.lineTo(x + width - RECTANGLE_CORNER_ARC_SIZE, y + height);
      path.curveTo(x + width, y + height, x + width, y + height - RECTANGLE_CORNER_ARC_SIZE, x + width,
          y + height - RECTANGLE_CORNER_ARC_SIZE);
      path.lineTo(x + width, y);
      path.closePath();
      return path;
    }
  }

}
