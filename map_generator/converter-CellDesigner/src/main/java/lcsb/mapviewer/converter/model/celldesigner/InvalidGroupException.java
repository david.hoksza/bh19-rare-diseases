package lcsb.mapviewer.converter.model.celldesigner;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;

/**
 * Exception that should be thrown when the group in xml schema is invalid.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidGroupException extends InvalidXmlSchemaException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor - initializes instance variable to unknown.
   */

  public InvalidGroupException() {
    super();
  }

  /**
   * Constructor receives some kind of message.
   * 
   * @param err
   *          message associated with exception
   */

  public InvalidGroupException(final String err) {
    super(err);
  }

  /**
   * Constructor receives some kind of message and parent exception.
   * 
   * @param err
   *          message associated with exception
   * @param throwable
   *          parent exception
   */
  public InvalidGroupException(final String err, final Throwable throwable) {
    super(err, throwable);
  }

  /**
   * Public constructor with parent exception that was catched.
   * 
   * @param e
   *          parent exception
   */

  public InvalidGroupException(final Exception e) {
    super(e);
  }
}
