package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

/**
 * Strange CellDEsigner structure to store width of single line...
 * 
 * @author Piotr Gawron
 * 
 */
public class SingleLine {

  /**
   * Width of the line.
   */
  private Double width;

  /**
   * @return the width
   * @see #width
   */
  public Double getWidth() {
    return width;
  }

  /**
   * @param width
   *          the width to set
   * @see #width
   */
  public void setWidth(Double width) {
    this.width = width;
  }

}
