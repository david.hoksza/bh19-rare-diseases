/**
 * This package contains internal CellDesigner structures used in the xml to
 * store some data. Structures from this package should be used only during
 * convertion from/to CellDEsigner format.
 */
package lcsb.mapviewer.converter.model.celldesigner.structure;