package lcsb.mapviewer.converter.model.celldesigner;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.model.graphics.*;
import lcsb.mapviewer.model.map.layout.BlockDiagram;
import lcsb.mapviewer.model.map.layout.ElementGroup;
import lcsb.mapviewer.model.map.layout.graphics.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Parser used for parsing CellDesigner xml to get {@link Layer} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class LayerXmlParser {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(LayerXmlParser.class.getName());

  /**
   * Parser for common CellDesigner structures.
   */
  private CommonXmlParser commonParser = new CommonXmlParser();

  /**
   * PArses CellDesigner xml node with collection of layers into collection of
   * Layers.
   * 
   * @param layersNode
   *          xml node
   * @return collection of layers
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  public Collection<Layer> parseLayers(Node layersNode) throws InvalidXmlSchemaException {
    List<Layer> result = new ArrayList<Layer>();
    NodeList nodes = layersNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:layer")) {
          Layer layer = getLayer(node);
          result.add(layer);
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:listOfLayers: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Transforms collection of layers into CellDesigner xml node.
   * 
   * @param layers
   *          collection of layers
   * @return xml node representing layers collection
   */
  public String layerCollectionToXml(Collection<Layer> layers) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:listOfLayers>");
    for (Layer layer : layers) {
      result.append(layerToXml(layer));
    }
    result.append("</celldesigner:listOfLayers>\n");
    return result.toString();
  }

  /**
   * Parses collection of block diagrams and adds them into the model.
   * 
   * @param model
   *          model where the data is stored.
   * @param xmlNode
   *          xml node
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  public void parseBlocks(Model model, Node xmlNode) throws InvalidXmlSchemaException {
    NodeList nodes = xmlNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:blockDiagram")) {
          model.addBlockDiagream(getBlockDiagram(node, model));
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:listOfBlockDiagrams: " + node.getNodeName());
        }
      }
    }
  }

  /**
   * Parses block diagrams from CellDesigner xml. Important! It's not yet
   * implemented.
   * 
   * @param node
   *          xml node to parse
   * @param model
   *          model where data is stored
   * @return parsed block diagram
   */
  private BlockDiagram getBlockDiagram(Node node, Model model) {
    logger.warn("BlockDiagrams are not implemented");
    return null;
  }

  /**
   * Parses CellDesigner node with collection of Alias Group and adds the groups
   * into this model.
   * 
   * @param model
   *          model where the data is stored
   * @param xmlNode
   *          xml node
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  public void parseGroups(Model model, Node xmlNode) throws InvalidXmlSchemaException {
    NodeList nodes = xmlNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:group")) {
          model.addElementGroup(getAliasGroup(node, model));
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:listOfGroups: " + node.getNodeName());
        }
      }
    }
  }

  /**
   * Parses xml node for alias group.
   * 
   * @param groupNode
   *          xml node
   * @param model
   *          model where the data is stored
   * @return parsed alias group
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  ElementGroup getAliasGroup(Node groupNode, Model model) throws InvalidXmlSchemaException {
    ElementGroup result = new ElementGroup();
    String id = XmlParser.getNodeAttr("id", groupNode);
    result.setIdGroup(id);
    String members = XmlParser.getNodeAttr("members", groupNode);

    String[] list = members.split(",");
    for (String string : list) {
      Element alias = model.getElementByElementId(string);
      if (alias == null) {
        throw new InvalidGroupException("Group \"" + id + "\" contains alias with id: \"" + string
            + "\", but such alias doesn't exist in the model.");
      }
      result.addElement(alias);
    }

    NodeList nodes = groupNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException("Unknown element of celldesigner:group: " + node.getNodeName());
      }
    }

    return result;
  }

  /**
   * Parses CellDesigner xml node for single layer.
   * 
   * @param layerNode
   *          xml node
   * @return Layer that corresponds to CellDesigner xml node.
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  Layer getLayer(Node layerNode) throws InvalidXmlSchemaException {
    Layer layer = new Layer();
    layer.setLayerId(XmlParser.getNodeAttr("id", layerNode));
    layer.setName(XmlParser.getNodeAttr("name", layerNode));
    layer.setLocked(XmlParser.getNodeAttr("locked", layerNode));
    layer.setVisible(XmlParser.getNodeAttr("visible", layerNode));
    NodeList list = layerNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:listOfTexts")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node textNode = residueList.item(j);
            if (textNode.getNodeType() == Node.ELEMENT_NODE) {
              if (textNode.getNodeName().equalsIgnoreCase("celldesigner:layerSpeciesAlias")) {
                layer.addLayerText(getLayerText(textNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfTexts " + textNode.getNodeName());
              }
            }
          }
        } else if (node.getNodeName().equals("celldesigner:listOfSquares")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node textNode = residueList.item(j);
            if (textNode.getNodeType() == Node.ELEMENT_NODE) {
              if (textNode.getNodeName().equalsIgnoreCase("celldesigner:layerCompartmentAlias")) {
                if (XmlParser.getNodeAttr("type", textNode).equalsIgnoreCase("SQUARE")) {
                  layer.addLayerRect(getLayerRect(textNode));
                } else if (XmlParser.getNodeAttr("type", textNode).equalsIgnoreCase("OVAL")) {
                  layer.addLayerOval(getLayerOval(textNode));
                } else {
                  throw new InvalidXmlSchemaException(
                      "Unknown celldesigner:layerCompartmentAlias type: " + XmlParser.getNodeAttr("type", textNode));
                }
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfSquares " + textNode.getNodeName());
              }
            }
          }
        } else if (node.getNodeName().equals("celldesigner:listOfFreeLines")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node textNode = residueList.item(j);
            if (textNode.getNodeType() == Node.ELEMENT_NODE) {
              if (textNode.getNodeName().equalsIgnoreCase("celldesigner:layerFreeLine")) {
                layer.addLayerLine(getLayerLine(textNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfFreeLines " + textNode.getNodeName());
              }
            }
          }
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:layer " + node.getNodeName());
        }
      }
    }

    return layer;
  }

  /**
   * Creates CellDesigner xml for a layer.
   * 
   * @param layer
   *          object to be transformed into xml
   * @return CellDesigner xml representation for the layer
   */
  String layerToXml(Layer layer) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:layer id =\"" + layer.getLayerId() + "\" ");
    result.append(" name =\"" + layer.getName() + "\" ");
    result.append(" locked =\"" + layer.isLocked() + "\" ");
    result.append(" visible =\"" + layer.isVisible() + "\">\n");

    result.append("<celldesigner:listOfTexts>\n");
    for (LayerText layerText : layer.getTexts()) {
      result.append(layerTextToXml(layerText));
    }
    result.append("</celldesigner:listOfTexts>\n");

    result.append("<celldesigner:listOfSquares>\n");
    for (LayerRect layerRect : layer.getRectangles()) {
      result.append(layerRectToXml(layerRect));
    }
    for (LayerOval layerOval : layer.getOvals()) {
      result.append(layerOvalToXml(layerOval));
    }
    result.append("</celldesigner:listOfSquares>\n");

    result.append("<celldesigner:listOfFreeLines>\n");
    for (PolylineData layerLine : layer.getLines()) {
      result.append(layerLineToXml(layerLine));
    }
    result.append("</celldesigner:listOfFreeLines>\n");

    result.append("</celldesigner:layer>\n");
    return result.toString();
  }

  /**
   * Parses CellDesigner xml node into LayerRect object.
   * 
   * @param textNode
   *          CellDesigner xml node for layer text object.
   * @return LayerRect instance representing the cml node
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  LayerRect getLayerRect(Node textNode) throws InvalidXmlSchemaException {
    LayerRect result = new LayerRect();
    NodeList nodes = textNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:paint")) {
          result.setColor(commonParser.getColor(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          result.setX(XmlParser.getNodeAttr("x", node));
          result.setY(XmlParser.getNodeAttr("y", node));
          result.setWidth(XmlParser.getNodeAttr("w", node));
          result.setHeight(XmlParser.getNodeAttr("h", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:ispaint")) {
          // ???
          continue;
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:layerCompartmentAlias: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Transforms LayerRect object into CellDesigner xml node.
   * 
   * @param layer
   *          object to be transformed into xml
   * @return CellDesigner xml node for LayerRect
   */
  String layerRectToXml(LayerRect layer) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:layerCompartmentAlias type=\"Square\">");
    result.append("<celldesigner:paint color=\"" + XmlParser.colorToString(layer.getColor()) + "\"/>");
    result.append("<celldesigner:bounds x=\"" + layer.getX() + "\" ");
    result.append(" y=\"" + layer.getY() + "\" ");
    result.append(" w=\"" + layer.getWidth() + "\" ");
    result.append(" h=\"" + layer.getHeight() + "\"/>");
    result.append("</celldesigner:layerCompartmentAlias>");
    return result.toString();
  }

  /**
   * Parses CellDesigner xml node into LayerLine object.
   * 
   * @param lineNode
   *          CellDesigner xml node for layer text object.
   * @return LayerLine instance representing the cml node
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  PolylineData getLayerLine(Node lineNode) throws InvalidXmlSchemaException {
    PolylineData ld = new PolylineData();

    if (XmlParser.getNodeAttr("isDotted", lineNode).equalsIgnoreCase("true")) {
      ld.getEndAtd().setArrowLineType(LineType.DOTTED);
    }
    if (XmlParser.getNodeAttr("isArrow", lineNode).equalsIgnoreCase("true")) {
      ld.getEndAtd().setArrowType(ArrowType.FULL);
    }
    NodeList nodes = lineNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          double sx = Double.parseDouble(XmlParser.getNodeAttr("sx", node));
          double sy = Double.parseDouble(XmlParser.getNodeAttr("sy", node));
          double ex = Double.parseDouble(XmlParser.getNodeAttr("ex", node));
          double ey = Double.parseDouble(XmlParser.getNodeAttr("ey", node));
          Point2D startPoint = new Point2D.Double(sx, sy);
          Point2D endPoint = new Point2D.Double(ex, ey);
          ld.addPoint(startPoint);
          ld.addPoint(endPoint);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:line")) {
          ld.setColor(XmlParser.stringToColor(XmlParser.getNodeAttr("color", node)));
          ld.setWidth(XmlParser.getNodeAttr("width", node));
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:layerFreeLine: " + node.getNodeName());
        }
      }
    }
    return ld;
  }

  /**
   * Transforms LayerLine object into CellDesigner xml node.
   * 
   * @param layer
   *          object to be transformed into xml
   * @return CellDesigner xml node for LayerLine
   */
  String layerLineToXml(PolylineData layer) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:layerFreeLine ");
    result.append(" isDotted=\"" + layer.getEndAtd().getArrowLineType().equals(LineType.DOTTED) + "\" ");
    result.append(" isArrow=\"" + layer.getEndAtd().getArrowType().equals(ArrowType.FULL) + "\" >");

    result.append("<celldesigner:bounds sx=\"" + layer.getBeginPoint().getX() + "\" ");
    result.append(" sy=\"" + layer.getBeginPoint().getY() + "\" ");
    result.append(" ex=\"" + layer.getEndPoint().getX() + "\" ");
    result.append(" ey=\"" + layer.getEndPoint().getY() + "\" />");
    result.append("<celldesigner:line ");
    result.append(" width=\"" + layer.getWidth() + "\" ");
    result.append(" color=\"" + XmlParser.colorToString(layer.getColor()) + "\"/>");
    result.append("</celldesigner:layerFreeLine>\n");
    return result.toString();
  }

  /**
   * Parses CellDesigner xml node into LayerOval object.
   * 
   * @param textNode
   *          CellDesigner xml node for layer text object.
   * @return LayerOval instance representing the cml node
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  LayerOval getLayerOval(Node textNode) throws InvalidXmlSchemaException {
    LayerOval result = new LayerOval();
    NodeList nodes = textNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:paint")) {
          result.setColor(commonParser.getColor(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          result.setX(XmlParser.getNodeAttr("x", node));
          result.setY(XmlParser.getNodeAttr("y", node));
          result.setWidth(XmlParser.getNodeAttr("w", node));
          result.setHeight(XmlParser.getNodeAttr("h", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:ispaint")) {
          // ???
          continue;
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:layerCompartmentAlias: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Transforms LayerOval object into CellDesigner xml node.
   * 
   * @param layer
   *          object to be transformed into xml
   * @return CellDesigner xml node for LayerOval
   */
  String layerOvalToXml(LayerOval layer) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:layerCompartmentAlias type=\"Oval\">");
    result.append("<celldesigner:paint color=\"" + XmlParser.colorToString(layer.getColor()) + "\"/>");
    result.append("<celldesigner:bounds x=\"" + layer.getX() + "\" ");
    result.append(" y=\"" + layer.getY() + "\" ");
    result.append(" w=\"" + layer.getWidth() + "\" ");
    result.append(" h=\"" + layer.getHeight() + "\"/>");
    result.append("</celldesigner:layerCompartmentAlias>");
    return result.toString();
  }

  /**
   * Parses CellDesigner xml node into LayerText object.
   * 
   * @param textNode
   *          CellDesigner xml node for layer text object.
   * @return LayerText instance representing the xml node
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  LayerText getLayerText(Node textNode) throws InvalidXmlSchemaException {
    LayerText result = new LayerText();
    NodeList nodes = textNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:layerNotes")) {
          String notes = XmlParser.getNodeValue(node).trim();
          Color backgroundColor = extractBackgroundColor(notes);
          if (backgroundColor != null) {
            result.setBackgroundColor(backgroundColor);
            notes = removeBackgroundColor(notes);
          }
          Color borderColor = extractBorderColor(notes);
          if (borderColor != null) {
            result.setBorderColor(borderColor);
            notes = removeBorderColor(notes);
          }
          result.setNotes(notes);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:paint")) {
          result.setColor(commonParser.getColor(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          result.setX(XmlParser.getNodeAttr("x", node));
          result.setY(XmlParser.getNodeAttr("y", node));
          result.setWidth(XmlParser.getNodeAttr("w", node));
          result.setHeight(XmlParser.getNodeAttr("h", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:font")) {
          result.setFontSize(XmlParser.getNodeAttr("size", node));
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:layerSpeciesAlias: " + node.getNodeName());
        }
      }
    }
    new RestAnnotationParser().processNotes(result.getNotes(), result);
    return result;
  }

  String removeBackgroundColor(String notes) {
    return removeColor(notes, "BackgroundColor");
  }

  String removeBorderColor(String notes) {
    return removeColor(notes, "BorderColor");
  }

  private String removeColor(String notes, String string) {
    String lines[] = notes.split("[\n\r]+");
    StringBuilder result = new StringBuilder("");
    for (String line : lines) {
      if (!line.startsWith(string + "=") && !line.startsWith(string + ":")) {
        result.append(line + "\n");
      }
    }
    return result.toString();
  }

  Color extractBackgroundColor(String notes) {
    return extractColor(notes, "BackgroundColor");
  }

  Color extractBorderColor(String notes) {
    return extractColor(notes, "BorderColor");
  }

  private Color extractColor(String notes, String string) {
    String lines[] = notes.split("[\n\r]+");
    for (String line : lines) {
      if (line.startsWith(string + "=") || line.startsWith(string + ":")) {
        String colorString = line.replace(string + "=", "").replace(string + ":", "");
        return new ColorParser().parse(colorString);
      }
    }
    return null;
  }

  /**
   * Transforms LayerText object into CellDesigner xml node.
   * 
   * @param layer
   *          object to be transformed into xml
   * @return CellDesigner xml node for LayerText
   */
  String layerTextToXml(LayerText layer) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:layerSpeciesAlias>");
    result.append("<celldesigner:layerNotes>\n");
    String notes = XmlParser.escapeXml(layer.getNotes());
    if (notes == null) {
      notes = "";
    }
    if (!layer.getBackgroundColor().equals(Color.LIGHT_GRAY)) {
      notes += "\nBackgroundColor:" + new ColorParser().colorToHtml(layer.getBackgroundColor());
    }
    if (!layer.getBorderColor().equals(Color.LIGHT_GRAY)) {
      notes += "\nBorderColor:" + new ColorParser().colorToHtml(layer.getBorderColor());
    }
    result.append(notes);
    result.append("\n</celldesigner:layerNotes>");
    result.append("<celldesigner:paint color=\"" + XmlParser.colorToString(layer.getColor()) + "\"/>");
    result.append("<celldesigner:bounds x=\"" + layer.getX() + "\" ");
    result.append(" y=\"" + layer.getY() + "\" ");
    result.append(" w=\"" + layer.getWidth() + "\" ");
    result.append(" h=\"" + layer.getHeight() + "\"/>");
    result.append("<celldesigner:font size=\"" + layer.getFontSize().intValue() + "\"/>");
    result.append("</celldesigner:layerSpeciesAlias>\n");
    return result.toString();
  }

}
