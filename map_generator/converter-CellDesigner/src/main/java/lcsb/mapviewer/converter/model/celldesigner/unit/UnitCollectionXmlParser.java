package lcsb.mapviewer.converter.model.celldesigner.unit;

import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;

public class UnitCollectionXmlParser {

  private UnitXmlParser unitParser = new UnitXmlParser();

  public Set<SbmlUnit> parseXmlUnitCollection(Node functionsNode) throws InvalidXmlSchemaException {
    Set<SbmlUnit> result = new HashSet<>();
    for (Node node : XmlParser.getNodes("unitDefinition", functionsNode.getChildNodes())) {
      result.add(unitParser.parseFunction(node));
    }
    return result;
  }

  public String toXml(Set<SbmlUnit> units) {
    StringBuilder builder = new StringBuilder();
    builder.append("<listOfUnitDefinitions>\n");
    for (SbmlUnit unit : units) {
      builder.append(unitParser.toXml(unit));
    }
    builder.append("</listOfUnitDefinitions>\n");
    return builder.toString();
  }

}
