package lcsb.mapviewer.converter.model.celldesigner.unit;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.kinetics.*;

public class UnitXmlParser {
  /**
   * Default class logger
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(UnitXmlParser.class);

  public SbmlUnit parseFunction(Node unitNode) throws InvalidXmlSchemaException {
    String unitId = XmlParser.getNodeAttr("id", unitNode);

    SbmlUnit result = new SbmlUnit(unitId);
    result.setName(XmlParser.getNodeAttr("name", unitNode));
    Node factors = XmlParser.getNode("listOfUnits", unitNode);
    if (factors == null) {
      throw new InvalidXmlSchemaException(
          "Unit Definition " + unitId + " doesn't contain information about units (listOfUnits node)");
    }
    for (SbmlUnitTypeFactor factor : parseUnitTypeFactors(factors)) {
      result.addUnitTypeFactor(factor);
    }

    return result;
  }

  private List<SbmlUnitTypeFactor> parseUnitTypeFactors(Node factors) throws InvalidXmlSchemaException {
    List<SbmlUnitTypeFactor> result = new ArrayList<>();
    List<Node> variables = XmlParser.getNodes("unit", factors.getChildNodes());
    for (Node node : variables) {
      result.add(parseFactor(node));
    }
    return result;
  }

  private SbmlUnitTypeFactor parseFactor(Node node) throws InvalidXmlSchemaException {
    String unitTypeString = XmlParser.getNodeAttr("kind", node);
    SbmlUnitType unitType;
    try {
      unitType = SbmlUnitType.valueOf(unitTypeString.toUpperCase());
    } catch (Exception e) {
      throw new InvalidXmlSchemaException("Unknown unit kind: " + unitTypeString);
    }
    int scale = 0;
    double multiplier = 1.0;
    int exponent = 1;

    String scaleString = XmlParser.getNodeAttr("scale", node);
    if (scaleString != null && !scaleString.isEmpty()) {
      scale = Integer.parseInt(scaleString);
    }
    String exponentString = XmlParser.getNodeAttr("exponent", node);
    if (exponentString != null && !exponentString.isEmpty()) {
      exponent = Integer.parseInt(exponentString);
    }
    String multiplierString = XmlParser.getNodeAttr("multiplier", node);
    if (multiplierString != null && !multiplierString.isEmpty()) {
      multiplier = Double.parseDouble(multiplierString);
    }
    return new SbmlUnitTypeFactor(unitType, exponent, scale, multiplier);
  }

  public String toXml(SbmlUnit unit) {
    StringBuilder result = new StringBuilder();
    result.append("<unitDefinition ");
    result.append("id=\"" + unit.getUnitId() + "\" ");
    result.append("name=\"" + unit.getName() + "\" ");
    result.append(">\n");
    result.append("<listOfUnits>\n");
    for (SbmlUnitTypeFactor factor : unit.getUnitTypeFactors()) {
      result.append(toXml(factor));
    }
    result.append("</listOfUnits>\n");
    result.append("</unitDefinition>");
    return result.toString();
  }

  private String toXml(SbmlUnitTypeFactor factor) {
    StringBuilder result = new StringBuilder();
    result.append("<unit ");
    result.append("kind=\"" + factor.getUnitType().toString().toLowerCase() + "\" ");
    result.append("scale=\"" + factor.getScale() + "\" ");
    result.append("exponent=\"" + factor.getExponent() + "\" ");
    result.append("multiplier=\"" + factor.getMultiplier() + "\" ");
    result.append("/>\n");
    return result.toString();
  }
}
