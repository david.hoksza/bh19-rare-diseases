package lcsb.mapviewer.converter.model.celldesigner.structure;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.GenericProtein;

/**
 * Class representing CellDesigner {@link GenericProtein}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerGenericProtein extends CellDesignerProtein<GenericProtein> {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private transient Logger logger = LogManager.getLogger(CellDesignerGenericProtein.class);

  /**
   * Default constructor.
   */
  public CellDesignerGenericProtein() {
    super();
  }

  /**
   * Constructor that creates a copy of species.
   * 
   * @param species
   *          original species
   */
  public CellDesignerGenericProtein(CellDesignerSpecies<?> species) {
    super(species);
  }

  /**
   * Default constructor.
   * 
   * @param id
   *          identifier of the protein
   */
  public CellDesignerGenericProtein(String id) {
    setElementId(id);
    setName(id);
  }

  @Override
  public CellDesignerGenericProtein copy() {
    if (this.getClass().equals(CellDesignerGenericProtein.class)) {
      return new CellDesignerGenericProtein(this);
    } else {
      throw new NotImplementedException("Copy method for " + this.getClass() + " class not implemented");
    }
  }

  @Override
  public GenericProtein createModelElement(String aliasId) {
    GenericProtein result = new GenericProtein(aliasId);
    super.setModelObjectFields(result);

    return result;
  }
}
