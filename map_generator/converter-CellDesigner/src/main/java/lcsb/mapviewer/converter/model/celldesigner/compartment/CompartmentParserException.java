package lcsb.mapviewer.converter.model.celldesigner.compartment;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerParserException;
import lcsb.mapviewer.model.map.compartment.Compartment;

/**
 * Exception thrown when problem with parsing {@link Compartment} is
 * encountered.
 * 
 * @author Piotr Gawron
 * 
 */
public class CompartmentParserException extends CellDesignerParserException {
  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(CompartmentParserException.class);
  /**
   * Identifier of compartment that was a reason for this exception.
   */
  private String compartmentId;

  /**
   * Default constructor.
   * 
   * @param message
   *          message thrown
   * @param compartmentId
   *          {@link #compartmentId}
   */
  public CompartmentParserException(String message, String compartmentId) {
    super(message);
    this.compartmentId = compartmentId;
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          message thrown
   * @param compartment
   *          {@link #compartmentId}
   */
  public CompartmentParserException(String message, Compartment compartment) {
    super(message);
    if (compartment != null) {
      setCompartmentId(compartment.getElementId());
    }
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          message thrown
   * @param compartment
   *          {@link #compartmentId}
   * @param e
   *          super exception
   */
  public CompartmentParserException(String message, Compartment compartment, Exception e) {
    super(message, e);
    if (compartment != null) {
      setCompartmentId(compartment.getElementId());
    }
  }

  /**
   * Default constructor.
   * 
   * @param compartment
   *          {@link #compartmentId}
   * @param e
   *          super exception
   */
  public CompartmentParserException(Compartment compartment, Throwable e) {
    super(e);
    if (compartment != null) {
      setCompartmentId(compartment.getElementId());
    }
  }

  /**
   * Default constructor.
   * 
   * @param compartmentId
   *          {@link #compartmentId}
   * @param e
   *          super exception
   */
  public CompartmentParserException(String compartmentId, Throwable e) {
    super(e);
    setCompartmentId(compartmentId);
  }

  /**
   * @return the compartmentId
   * @see #compartmentId
   */
  public String getCompartmentId() {
    return compartmentId;
  }

  /**
   * @param compartmentId
   *          the compartmentId to set
   * @see #compartmentId
   */
  public void setCompartmentId(String compartmentId) {
    this.compartmentId = compartmentId;
  }

  @Override
  public String getMessageContext() {
    if (compartmentId != null) {
      return "[Compartment: " + compartmentId + "]";
    }
    return null;
  }

}
