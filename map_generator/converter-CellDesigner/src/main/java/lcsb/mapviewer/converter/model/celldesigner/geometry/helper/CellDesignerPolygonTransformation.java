package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import java.awt.geom.Point2D;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

/**
 * Class with basic operations on polygons.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerPolygonTransformation {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(CellDesignerPolygonTransformation.class.getName());

  /**
   * Returns a middle point between two points (using Euclidean distance).
   * 
   * @param pointA
   *          first coordinate
   * @param pointB
   *          second coordinate
   * @return central point between pointA and pointB
   */
  private Point2D getMidPoint(Point2D pointA, Point2D pointB) {
    return new Point2D.Double((pointA.getX() + pointB.getX()) / 2, (pointA.getY() + pointB.getY()) / 2);
  }

  /**
   * Returns a point on polygon for the anchor given as a parameter.
   * 
   * @param points
   *          list of points which describes polygon. By default polygon can
   *          contain only 8 or 16 points (all CellDesigner elements can be drawn
   *          as such coordinates).
   * @param anchor
   *          direction in which we look for coordinates
   * @return coordinates on the polygon described by the anchor point
   */
  public Point2D getPointOnPolygonByAnchor(List<Point2D> points, CellDesignerAnchor anchor) {
    if (points.size() == CellDesignerAnchor.DIFFERENT_ANCHORS / 2) {
      return getPointOn8NodesPolygonByAnchor(points, anchor);
    } else if (points.size() == CellDesignerAnchor.DIFFERENT_ANCHORS) {
      return getPointOn16NodesPolygonByAnchor(points, anchor);
    } else {
      throw new InvalidArgumentException("Invalid number of points: " + points.size());
    }
  }

  /**
   * Creates a copy of a point given as a parameter.
   * 
   * @param point
   *          object to copy
   * @return copy of the point parameter
   */
  private Point2D copyPoint(Point2D point) {
    return new Point2D.Double(point.getX(), point.getY());
  }

  /**
   * This method returns a point on the border defined by the list of 8 points for
   * a given anchor.
   * 
   * @param points
   *          list of 8 points that define a polygon
   * @param anchor
   *          direction in which we are looking for a point
   * @return point on the border
   */
  private Point2D getPointOn8NodesPolygonByAnchor(List<Point2D> points, CellDesignerAnchor anchor) {
    if (anchor == null || anchor.getAngle() == null) {
      throw new InvalidArgumentException("Invalid anchor: " + anchor);
    } else {
      int position = (anchor.ordinal() + CellDesignerAnchor.DIFFERENT_ANCHORS / 2)
          % CellDesignerAnchor.DIFFERENT_ANCHORS;
      if (position % 2 == 0) {
        return copyPoint(points.get(position / 2));
      } else {
        return getMidPoint(points.get(position / 2),
            points.get((position / 2 + 1) % (CellDesignerAnchor.DIFFERENT_ANCHORS / 2)));
      }
    }
  }

  /**
   * This method returns a point on the border defined by the list of 16 points
   * for a given anchor.
   * 
   * @param points
   *          list of 16 points that define a polygon
   * @param anchor
   *          direction in which we are looking for a point
   * @return point on the border
   */
  private Point2D getPointOn16NodesPolygonByAnchor(List<Point2D> points, CellDesignerAnchor anchor) {
    if (anchor == null || anchor.getAngle() == null) {
      throw new InvalidArgumentException("Invalid anchor: " + anchor);
    } else {
      int position = (anchor.ordinal() + CellDesignerAnchor.DIFFERENT_ANCHORS / 2)
          % CellDesignerAnchor.DIFFERENT_ANCHORS;
      return copyPoint(points.get(position));
    }
  }
}
