package lcsb.mapviewer.converter.model.celldesigner.structure;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Drug;

/**
 * Class representing CellDesigner {@link Drug}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerDrug extends CellDesignerSpecies<Drug> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor that copies the data from species given in the argument.
   * 
   * @param species
   *          parent species from which we want to copy data
   */
  public CellDesignerDrug(CellDesignerSpecies<?> species) {
    super(species);
  }

  /**
   * Default constructor.
   */
  public CellDesignerDrug() {
  }

  @Override
  public CellDesignerDrug copy() {
    if (this.getClass() == CellDesignerDrug.class) {
      return new CellDesignerDrug(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public Drug createModelElement(String aliasId) {
    Drug result = new Drug(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

}
