package lcsb.mapviewer.converter.model.celldesigner.structure;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.ReceptorProtein;

/**
 * Class representing CellDesigner {@link ReceptorProtein}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerReceptorProtein extends CellDesignerProtein<ReceptorProtein> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public CellDesignerReceptorProtein() {
    super();
  }

  /**
   * Constructor that creates a copy of species.
   * 
   * @param species
   *          original species
   */
  public CellDesignerReceptorProtein(CellDesignerSpecies<?> species) {
    super(species);
  }

  @Override
  public CellDesignerReceptorProtein copy() {
    if (this.getClass().equals(CellDesignerReceptorProtein.class)) {
      return new CellDesignerReceptorProtein(this);
    } else {
      throw new NotImplementedException("Copy method for " + this.getClass() + " class not implemented");
    }
  }

  @Override
  public ReceptorProtein createModelElement(String aliasId) {
    ReceptorProtein result = new ReceptorProtein(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

}
