package lcsb.mapviewer.converter.model.celldesigner.alias;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CommonXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.StructuralState;

/**
 * Generic abstract interface for parsing CellDesigner xml nodes with species
 * definition.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          type of the object to parse
 */
public abstract class AbstractAliasXmlParser<T extends Element> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(AbstractAliasXmlParser.class.getName());

  /**
   * Set of common functions used in parsing cell designer xml.
   */
  private CommonXmlParser commonParser = new CommonXmlParser();

  /**
   * Parse object from the xml node.
   * 
   * @param node
   *          xml node to parse
   * 
   * @return parsed object
   * @throws InvalidXmlSchemaException
   *           thrown when xmlString is invalid
   */
  abstract T parseXmlAlias(Node node) throws InvalidXmlSchemaException;

  /**
   * Parse object from the xml string.
   * 
   * @param xmlString
   *          xml string
   * @return parsed object
   * @throws InvalidXmlSchemaException
   *           thrown when xmlString is invalid
   */
  public T parseXmlAlias(String xmlString) throws InvalidXmlSchemaException {
    Document doc = XmlParser.getXmlDocumentFromString(xmlString);
    NodeList root = doc.getChildNodes();
    return parseXmlAlias(root.item(0));

  }

  /**
   * Method that transform object into CellDesigner xml.
   * 
   * @param alias
   *          object to be transformed
   * @return CellDesigner xml representation of the alias
   */
  abstract String toXml(T alias);

  /**
   * @return the commonParser
   */
  CommonXmlParser getCommonParser() {
    return commonParser;
  }

  /**
   * @param commonParser
   *          the commonParser to set
   */
  void setCommonParser(CommonXmlParser commonParser) {
    this.commonParser = commonParser;
  }

  protected String createFontTag(T alias) {
    return "<celldesigner:font size=\"" + alias.getFontSize().intValue() + "\"/>";
  }

  protected String createStructuralStateTag(Species species) {
    StructuralState structuralState = null;
    if (species instanceof Protein) {
      structuralState = ((Protein) species).getStructuralState();
    }
    if (species instanceof Complex) {
      structuralState = ((Complex) species).getStructuralState();
    }
    if (structuralState != null) {
      CellDesignerAliasConverter converter = new CellDesignerAliasConverter(species, false);
      double angle = converter.getAngleForPoint(species, structuralState.getCenter());
      return "<celldesigner:structuralState angle=\"" + angle + "\"/>";
    }
    return "";
  }

}
