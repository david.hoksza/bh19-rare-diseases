package lcsb.mapviewer.converter.model.celldesigner.annotation;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.comparator.*;
import lcsb.mapviewer.common.exception.*;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This class is a converter of annotation provided by lcsb in raw text format
 * into set of MiriamData that can be used later on.
 * 
 * @author Piotr Gawron
 * 
 */
public class RestAnnotationParser {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(RestAnnotationParser.class);

  /**
   * Pattern used to find rdf node in string xml.
   */
  private Pattern rdfNodePattern = Pattern.compile("(?<=<rdf:RDF)([\\s\\S]*?)(?=</rdf:RDF>)");

  /**
   * Parser used for parsing annotations in rdf format.
   */
  private XmlAnnotationParser xmlAnnotationParser = new XmlAnnotationParser();

  /**
   * Class used for some simple operations on {@link BioEntity} elements.
   */
  private ElementUtils elementUtils = new ElementUtils();

  /**
   * This method parse the string with annotations provided by the lcsb team.
   * Parsing has been prepared based on the information provided by Kazuhiro
   * Fujita, kaf@sbi.jp.
   *
   * @param annotationString
   *          - string with data to parse
   * @return list of {@link MiriamData annotations}
   */
  public Set<MiriamData> getMiriamData(String annotationString) {
    Set<MiriamData> result = new HashSet<MiriamData>();

    // if the string is empty, then clean the state of the class and return
    if (annotationString == null) {
      return result;
    }

    String[] lines = annotationString.split("\n");
    for (int i = 0; i < lines.length; i++) {
      String line = lines[i];
      for (NoteField field : NoteField.values()) {
        if (field.getMiriamType() != null) {
          ArrayList<String> ids = getIds(line, field.getCommonName() + ":");
          if (ids != null) {
            Set<MiriamData> hgncData = idsToData(MiriamRelationType.BQ_BIOL_HAS_VERSION, field.getMiriamType(), ids);
            result.addAll(hgncData);
          }
        }
      }
    }
    return result;
  }

  /**
   * Creates note string with structural information about element.
   *
   * @param element
   *          element for which notes are created
   * @return note string with structural information about element
   */
  public String createAnnotationString(Element element) {
    return createAnnotationString(element, false);
  }

  /**
   * Creates note string with structural information about element.
   *
   * @param element
   *          element for which notes are created
   * @param forceFullInfo
   *          when true annotation string will contain information about empty
   *          fields
   * @return note string with structural information about element
   */
  public String createAnnotationString(Element element, boolean forceFullInfo) {
    Set<MiriamData> data = element.getMiriamData();
    StringBuilder sb = new StringBuilder();
    sb.append(createEntry(NoteField.SYMBOL, element.getSymbol(), forceFullInfo));
    sb.append(createEntry(NoteField.NAME, element.getFullName(), forceFullInfo));
    sb.append(createEntry(NoteField.PREVIOUS_SYMBOLS, element.getFormerSymbols(), forceFullInfo));
    sb.append(createEntry(NoteField.SYNONYMS, element.getSynonyms(), forceFullInfo));
    sb.append(createEntry(NoteField.HGNC, filterMiriam(data, MiriamType.HGNC), forceFullInfo));
    sb.append(createEntry(NoteField.ENTREZ, filterMiriam(data, MiriamType.ENTREZ), forceFullInfo));
    sb.append(createEntry(NoteField.REFSEQ, filterMiriam(data, MiriamType.REFSEQ), forceFullInfo));
    sb.append(createEntry(NoteField.REACTOME, filterMiriam(data, MiriamType.REACTOME), forceFullInfo));
    sb.append(createEntry(NoteField.PUBMED, filterMiriam(data, MiriamType.PUBMED), forceFullInfo));
    sb.append(createEntry(NoteField.KEGG_GENES, filterMiriam(data, MiriamType.KEGG_GENES), forceFullInfo));
    sb.append(createEntry(NoteField.PANTHER, filterMiriam(data, MiriamType.PANTHER), forceFullInfo));
    sb.append(createEntry(NoteField.DESCRIPTION, null, forceFullInfo));
    sb.append(createEntry(NoteField.ABBREVIATION, element.getAbbreviation(), forceFullInfo));
    sb.append(createEntry(NoteField.SEMANTIC_ZOOM_LEVEL_VISIBILITY, element.getVisibilityLevel(), forceFullInfo));
    sb.append(createEntry(NoteField.TRANSPARENCY_ZOOM_LEVEL_VISIBILITY, element.getTransparencyLevel(), forceFullInfo));

    sb.append(createEntry(NoteField.CHARGED_FORMULA, element.getFormula(), forceFullInfo));
    if (element instanceof Species) {
      sb.append(createEntry(NoteField.CHARGE, ((Species) element).getCharge(), forceFullInfo));
    }

    sb.append("\n");
    return sb.toString();
  }

  private List<MiriamData> filterMiriam(Collection<MiriamData> miriamDataSet, MiriamType type) {
    List<MiriamData> result = new ArrayList<>();
    for (MiriamData miriamData : miriamDataSet) {
      if (miriamData.getDataType().equals(type)) {
        result.add(miriamData);
      }
    }
    return result;
  }

  private String createEntry(NoteField type, Object value, boolean forceFullInfo) {
    if (value == null) {
      if (forceFullInfo) {
        return type.getCommonName() + ": \n";
      } else {
        return "";
      }
    } else if (value instanceof String) {
      String string = (String) value;
      if (!(string.trim().isEmpty()) || forceFullInfo) {
        return type.getCommonName() + ": " + XmlParser.escapeXml(string) + "\n";
      } else {
        return "";
      }
    } else if (value instanceof Integer) {
      return type.getCommonName() + ": " + value + "\n";
    } else if (value instanceof Collection) {
      Collection<?> collection = (Collection<?>) value;
      if (collection.size() > 0 || forceFullInfo) {
        String result = "";
        for (Object object : collection) {
          if (!result.equals("")) {
            result = result + ", ";
          }
          if (object instanceof MiriamData) {
            result += ((MiriamData) object).getResource();
          } else {
            result += XmlParser.escapeXml(object.toString());
          }

        }
        return type.getCommonName() + ": " + result + "\n";
      } else {
        return "";
      }
    } else {
      throw new InvalidArgumentException("Unknown class type: " + value.getClass());
    }
  }

  /**
   * Returns value for the type of the structured annotation. Structured
   * annotation is in format:
   *
   * <pre>
   * TYPE1:description1
   * TYPE2:descripton2
   * ...
   * </pre>
   *
   * @param annotationString
   *          whole annotation string
   * @param prefix
   *          prefix used for identifying line
   * @return value for the given type in structured annotation
   */
  private String getParamByPrefix(String annotationString, String prefix) {
    if (annotationString == null) {
      return null;
    }

    String[] lines = annotationString.split("\n");
    for (int i = 0; i < lines.length; i++) {
      String line = lines[i];
      if (line.startsWith(prefix)) {
        String result = StringEscapeUtils.unescapeHtml4(line.substring(prefix.length()).trim());
        if (result.equals("")) {
          return null;
        } else {
          return result;
        }
      }
    }
    return null;
  }

  /**
   * Returns list of symbols from the annotation string.
   *
   * @param annotationString
   *          annotation string
   * @return list of symbol
   */
  public List<String> getSynonyms(String annotationString) {
    List<String> result = new ArrayList<String>();
    String synonyms = getParamByPrefix(annotationString, NoteField.SYNONYMS.getCommonName() + ":");
    if (synonyms != null) {
      for (String string : synonyms.split(",")) {
        if (!string.trim().equals("")) {
          result.add(string.trim());
        }
      }
    }
    return result;
  }

  /**
   * Returns list of former symbols from the annotation string.
   *
   * @param annotationString
   *          annotation string
   * @return list of former symbol
   */
  public List<String> getFormerSymbols(String annotationString) {
    List<String> result = new ArrayList<String>();
    String formerSymbols = getParamByPrefix(annotationString, NoteField.PREVIOUS_SYMBOLS.getCommonName() + ":");
    if (formerSymbols != null) {
      for (String string : formerSymbols.split(",")) {
        if (!string.trim().equals("")) {
          result.add(string.trim());
        }
      }
    }
    return result;
  }

  /**
   * This method parse a string with id in a format:<br>
   * database_ID: idfield1, idfield2, ..., idfieldn<br>
   * into a vector of string ids.
   *
   * @param line
   *          - a line to be parsed
   * @param baseId
   *          - database string id
   * @return vector of string ids
   */
  protected ArrayList<String> getIds(String line, String baseId) {
    if (line.indexOf(baseId) >= 0) {
      ArrayList<String> result = new ArrayList<String>();
      String tmpLine = line.substring(baseId.length());
      String[] ids = tmpLine.split(",");
      for (String string : ids) {
        // only non-empty ids
        if (!string.trim().equals("")) {
          result.add(string.trim());
        }
      }
      return result;
    } else {
      return null;
    }
  }

  /**
   * This method transform the vector of ids into a set of MiriamData.
   *
   * @param type
   *          - relationtype of the annotaion ids
   * @param mType
   *          {@link MiriamType type} of the reference resource
   * @param ids
   *          - list of ids
   * @return set of miriam data
   */
  private Set<MiriamData> idsToData(MiriamRelationType type, MiriamType mType, ArrayList<String> ids) {
    Set<MiriamData> result = new HashSet<MiriamData>();
    for (String id : ids) {
      MiriamData md = new MiriamData(type, mType, id);
      result.add(md);
    }
    return result;
  }

  /**
   * Process element notes and assign structural information from it.
   *
   * @param node
   *          node with notes about element
   * @param element
   *          where the structural data should be put
   */
  public void processNotes(Node node, BioEntity element) {
    String notes = getNotes(node);
    processNotes(notes, element);
  }

  /**
   * Transforms xml node into notes.
   *
   * @param node
   *          xml node with notes
   * @return string with notes
   */
  public String getNotes(Node node) {
    String notes = "";

    if (!node.getNodeName().contains("notes")) {
      throw new InvalidArgumentException("Invalid notes node");
    }

    Node htmlNode = XmlParser.getNode("html", node.getChildNodes());
    if (htmlNode == null) {
      notes = XmlParser.nodeToString(node).trim();
    } else {
      Node bodyNode = XmlParser.getNode("body", htmlNode.getChildNodes());
      if (bodyNode != null) {
        notes = XmlParser.nodeToString(bodyNode).trim();
      }
    }

    if (notes.indexOf("</head>") >= 0) {
      notes = notes.substring(notes.indexOf("</head>") + "</head>".length());
    }
    notes = notes.replace("xmlns=\"http://www.w3.org/1999/xhtml\"", "");
    return notes.replaceAll("&amp;", "&");
  }

  /**
   * Assigns synonyms to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setSynonyms(BioEntity element, String annotationString) {
    List<String> synonyms = getSynonyms(annotationString);
    if (synonyms.size() == 0) {
      return;
    }
    if (element.getSynonyms() == null || element.getSynonyms().size() == 0) {
      element.setSynonyms(synonyms);
    } else {
      StringSetComparator stringSetComparator = new StringSetComparator();
      Set<String> set1 = new HashSet<String>();
      Set<String> set2 = new HashSet<String>();
      set1.addAll(element.getSynonyms());
      set2.addAll(synonyms);
      if (stringSetComparator.compare(set1, set2) != 0) {
        logger.warn(elementUtils.getElementTag(element) + "Synonyms list different than default [" + synonyms + "]["
            + element.getSynonyms() + "]. Ignoring.");
      }
    }
  }

  /**
   * Assigns list of symbols to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setFormerSymbolsToSpecies(Element element, String annotationString) {
    List<String> formerSymbols = getFormerSymbols(annotationString);
    if (formerSymbols.size() == 0) {
      return;
    }
    if (element.getFormerSymbols() == null || element.getFormerSymbols().size() == 0) {
      element.setFormerSymbols(formerSymbols);
    } else {
      StringListComparator stringListComparator = new StringListComparator();
      if (stringListComparator.compare(element.getFormerSymbols(), formerSymbols) != 0) {
        logger.warn(elementUtils.getElementTag(element) + " Former symbols list different than default ["
            + formerSymbols + "][" + element.getFormerSymbols() + "]. Ignoring.");
      }
    }
  }

  /**
   * Assigns full name to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setFullNameToSpecies(Element element, String annotationString) {
    StringComparator stringComparator = new StringComparator();
    String fullName = getFullName(annotationString);
    if (fullName == null) {
      return;
    }
    if (element.getFullName() == null) {
      element.setFullName(fullName);
    } else if (stringComparator.compare(element.getFullName(), fullName, true) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " New full name different than default [" + fullName + "]["
          + element.getFullName() + "]. Ignoring.");
    }
  }

  public String getFullName(String annotationString) {
    return getParamByPrefix(annotationString, NoteField.NAME.getCommonName() + ":");
  }

  /**
   * Assigns abbreviation to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setAbbreviation(BioEntity element, String annotationString) {
    StringComparator stringComparator = new StringComparator();
    String abbreviation = getAbbreviation(annotationString);
    if (abbreviation == null) {
      return;
    }
    if (element.getAbbreviation() == null) {
      element.setAbbreviation(abbreviation);
    } else if (stringComparator.compare(element.getAbbreviation(), abbreviation, true) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " Abbreviation different than default [" + abbreviation + "]["
          + element.getAbbreviation() + "]. Ignoring.");
    }
  }

  public String getAbbreviation(String annotationString) {
    return getParamByPrefix(annotationString, NoteField.ABBREVIATION.getCommonName() + ":");
  }

  /**
   * Assigns subsystem to the reaction from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setSubsystemToReaction(Reaction element, String annotationString) {
    StringComparator stringComparator = new StringComparator();
    String abbreviation = getParamByPrefix(annotationString, NoteField.SUBSYSTEM.getCommonName() + ":");
    if (abbreviation == null) {
      return;
    }
    if (element.getSubsystem() == null) {
      element.setSubsystem(abbreviation);
    } else if (stringComparator.compare(element.getSubsystem(), abbreviation, true) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " Subsystem different than default [" + abbreviation + "]["
          + element.getSubsystem() + "]. Ignoring.");
    }
  }

  /**
   * Assigns gene protein reaction to the reaction from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setGeneProteinReactionToReaction(Reaction element, String annotationString) {
    StringComparator stringComparator = new StringComparator();
    String abbreviation = getParamByPrefix(annotationString, NoteField.GENE_PROTEIN_REACTION.getCommonName() + ":");
    if (abbreviation == null) {
      return;
    }
    if (element.getGeneProteinReaction() == null) {
      element.setGeneProteinReaction(abbreviation);
    } else if (stringComparator.compare(element.getGeneProteinReaction(), abbreviation, true) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " GeneProteinReaction different than default [" + abbreviation
          + "][" + element.getGeneProteinReaction() + "]. Ignoring.");
    }
  }

  /**
   * Assigns formula to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setFormula(BioEntity element, String annotationString) {
    StringComparator stringComparator = new StringComparator();
    String formula = getFormula(annotationString);
    if (formula == null) {
      formula = getParamByPrefix(annotationString, NoteField.CHARGED_FORMULA.getCommonName() + ":");
      if (formula == null) {
        return;
      }
    }
    if (element.getFormula() == null) {
      element.setFormula(formula);
    } else if (stringComparator.compare(element.getFormula(), formula, true) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " Formula different than default [" + formula + "]["
          + element.getFormula() + "]. Ignoring.");
    }
  }

  public String getFormula(String annotationString) {
    return getParamByPrefix(annotationString, NoteField.FORMULA.getCommonName() + ":");
  }

  /**
   * Assigns mechanical confidence score to the reaction from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setMechanicalConfidenceScoreToReaction(Reaction element, String annotationString) {
    IntegerComparator integerComparator = new IntegerComparator();

    String formula = getParamByPrefix(annotationString, NoteField.MECHANICAL_CONFIDENCE_SCORE.getCommonName() + ":");
    if (formula == null) {
      return;
    }
    Integer charge = Integer.valueOf(formula);
    if (element.getMechanicalConfidenceScore() == null) {
      element.setMechanicalConfidenceScore(charge);
    } else if (integerComparator.compare(element.getMechanicalConfidenceScore(), charge) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " MechanicalConfidenceScore different than default [" + formula
          + "][" + element.getFormula() + "]. Ignoring.");
    }
  }

  /**
   * Assigns charge to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setCharge(Species element, String annotationString) {
    IntegerComparator integerComparator = new IntegerComparator();
    String value = getParamByPrefix(annotationString, NoteField.CHARGE.getCommonName() + ":");
    if (value == null || value.trim().equals("") || value.trim().equals("-")) {
      return;
    }
    try {
      Integer charge = Integer.valueOf(value);
      if (element.getCharge() == null || element.getCharge() == 0) {
        element.setCharge(charge);
      } else if (integerComparator.compare(element.getCharge(), charge) != 0) {
        logger.warn(elementUtils.getElementTag(element) + " Charge different than default [" + value + "]["
            + element.getCharge() + "]. Ignoring.");
      }
    } catch (NumberFormatException e) {
      logger.warn(elementUtils.getElementTag(element) + " Invalid charge (integer value expected): " + value);
    }
  }

  /**
   * Assigns lower bound to the reaction from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setLowerBoundToReaction(Reaction element, String annotationString) {
    DoubleComparator doubleComparator = new DoubleComparator();
    String formula = getParamByPrefix(annotationString, NoteField.LOWER_BOUND.getCommonName() + ":");
    if (formula == null || formula.trim().equals("")) {
      return;
    }
    Double value = Double.valueOf(formula);
    if (element.getLowerBound() == null) {
      element.setLowerBound(value);
    } else if (doubleComparator.compare(element.getLowerBound(), value) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " LowerBound different than default [" + formula + "]["
          + element.getLowerBound() + "]. Ignoring.");
    }
  }

  /**
   * Assigns upper bound to the reaction from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setUpperBoundToReaction(Reaction element, String annotationString) {
    DoubleComparator doubleComparator = new DoubleComparator();
    String formula = getParamByPrefix(annotationString, NoteField.UPPER_BOUND.getCommonName() + ":");
    if (formula == null || formula.trim().equals("")) {
      return;
    }
    Double value = Double.valueOf(formula);
    if (element.getUpperBound() == null) {
      element.setUpperBound(value);
    } else if (doubleComparator.compare(element.getUpperBound(), value) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " UpperBound different than default [" + formula + "]["
          + element.getUpperBound() + "]. Ignoring.");
    }
  }

  /**
   * Assigns symbol to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setSymbol(BioEntity element, String annotationString) {
    String symbol = getSymbol(annotationString);
    if (symbol == null) {
      return;
    }
    if (element.getSymbol() == null) {
      element.setSymbol(symbol);
    } else if (!element.getSymbol().equals(symbol)) {
      logger.warn(elementUtils.getElementTag(element) + " New symbol different than default [" + symbol + "]["
          + element.getSymbol() + "]. Ignoring.");
    }
  }

  public String getSymbol(String annotationString) {
    return getParamByPrefix(annotationString, NoteField.SYMBOL.getCommonName() + ":");
  }

  /**
   * Assigns semanticZoomingLevel to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setSemanticZoomLevelVisibility(BioEntity element, String annotationString) {
    String zoomLevelVisibility = getParamByPrefix(annotationString,
        NoteField.SEMANTIC_ZOOM_LEVEL_VISIBILITY.getCommonName() + ":");
    if (zoomLevelVisibility == null) {
      return;
    }
    if (element.getVisibilityLevel() == null || element.getVisibilityLevel().isEmpty()) {
      element.setVisibilityLevel(zoomLevelVisibility);
    } else if (!element.getVisibilityLevel().equals(zoomLevelVisibility)) {
      logger.warn(elementUtils.getElementTag(element) + " New "
          + NoteField.SEMANTIC_ZOOM_LEVEL_VISIBILITY.getCommonName() + " different than default [" + zoomLevelVisibility
          + "][" + element.getVisibilityLevel() + "]. Ignoring.");
    }
  }

  private void setTransparencyZoomLevelVisibility(Element element, String annotationString) {
    String transparencyZoomLevelVisibility = getParamByPrefix(annotationString,
        NoteField.TRANSPARENCY_ZOOM_LEVEL_VISIBILITY.getCommonName() + ":");
    if (transparencyZoomLevelVisibility == null) {
      transparencyZoomLevelVisibility = getParamByPrefix(annotationString,
          NoteField.TRANSPARENCY_ZOOM_LEVEL_VISIBILITY_OLD.getCommonName() + ":");
      if (transparencyZoomLevelVisibility == null) {
        return;
      } else {
        logger.warn("Deprecated parameter found: " + NoteField.TRANSPARENCY_ZOOM_LEVEL_VISIBILITY_OLD.getCommonName()
            + ". Use " + NoteField.TRANSPARENCY_ZOOM_LEVEL_VISIBILITY.getCommonName());
      }
    }
    if (element.getTransparencyLevel() == null || element.getTransparencyLevel().isEmpty()) {
      element.setTransparencyLevel(transparencyZoomLevelVisibility);
    } else if (!element.getTransparencyLevel().equals(transparencyZoomLevelVisibility)) {
      logger.warn(elementUtils.getElementTag(element) + " New "
          + NoteField.TRANSPARENCY_ZOOM_LEVEL_VISIBILITY.getCommonName() + " different than default ["
          + transparencyZoomLevelVisibility + "][" + element.getVisibilityLevel() + "]. Ignoring.");
    }
  }

  private void setZIndex(Drawable element, String annotationString) {
    String zIndex = getParamByPrefix(annotationString,
        NoteField.Z_INDEX.getCommonName() + ":");
    if (zIndex != null) {
      try {
        Integer z = Integer.valueOf(zIndex);
        if (element.getZ() == null) {
          element.setZ(z);
        } else if (!element.getZ().equals(z)) {
          logger.warn(elementUtils.getElementTag(element) + " New " + NoteField.Z_INDEX.getCommonName()
              + " different than default [" + zIndex + "][" + element.getZ() + "]. Ignoring.");
        }
      } catch (NumberFormatException e) {
        logger.warn("Invalid e index", e);
      }
    }
  }

  /**
   * Assigns notes to the element from notes string. This might look strange. The
   * idea is that sometimes we have notes from more then one source. And the data
   * from these sources should be merged. So, this method in fact merges notes in
   * the element and description extracted from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setNotes(BioEntity element, String annotationString) {
    String description = getDescription(annotationString);
    if (description == null) {
      return;
    }
    if (element.getNotes().trim().equals("")) {
      element.setNotes(description);
    } else if (element.getNotes().contains(description)) {
      return;
    } else {
      element.setNotes(element.getNotes().trim() + "\n" + description + "\n");
    }
  }

  public String getDescription(String annotationString) {
    return getParamByPrefix(annotationString, NoteField.DESCRIPTION.getCommonName() + ":");
  }

  /**
   * Process notes and assign structural information from it.
   *
   * @param notes
   *          notes about element
   * @param object
   *          where the structural data should be put
   */
  public void processNotes(String notes, Drawable object) {
    StringBuilder annotations = new StringBuilder();

    String[] string = notes.split("\n");
    StringBuilder newNotes = new StringBuilder("");
    for (String string2 : string) {
      boolean remove = false;
      for (NoteField field : NoteField.values()) {

        if (string2.startsWith(field.getCommonName() + ":") && field.getClazz().isAssignableFrom(object.getClass())) {
          remove = true;
        }
      }
      if (remove) {
        annotations.append(string2 + "\n");
      } else {
        newNotes.append(string2 + "\n");
      }
    }
    String ann = annotations.toString();
    setZIndex(object, ann);
    if (object instanceof LayerText) {
      ((LayerText) object).setNotes(newNotes.toString().trim());
    }
    if (object instanceof BioEntity) {
      BioEntity bioEntity = (BioEntity) object;
      bioEntity.setNotes(newNotes.toString().trim());

      setNotes(bioEntity, ann);
      setSymbol(bioEntity, ann);
      setSynonyms(bioEntity, ann);
      setSemanticZoomLevelVisibility(bioEntity, ann);
      setAbbreviation(bioEntity, ann);
      setFormula(bioEntity, ann);
      if (object instanceof Reaction) {
        Reaction reaction = (Reaction) object;
        setMechanicalConfidenceScoreToReaction(reaction, ann);
        setLowerBoundToReaction(reaction, ann);
        setUpperBoundToReaction(reaction, ann);
        setSubsystemToReaction(reaction, ann);
        setGeneProteinReactionToReaction(reaction, ann);
      } else if (object instanceof Element) {
        setTransparencyZoomLevelVisibility((Element) object, ann);
        setFullNameToSpecies((Element) object, ann);
        setFormerSymbolsToSpecies((Element) object, ann);
        if (object instanceof Species) {
          setCharge((Species) object, ann);
        }
      } else {
        throw new NotImplementedException("Don't know how to process class: " + object.getClass());
      }

      try {
        processRdfDescription(bioEntity);
      } catch (InvalidXmlSchemaException e) {
        String warning = elementUtils.getElementTag(object) + " Problem with processing notes. Invalid RDF node.";
        logger.warn(warning);
      }
    }
  }

  /**
   * Process rdf description from notes, removes it from the description and adds
   * apropriate information to miriam data set.
   *
   * @param element
   *          notes of this element will be processed
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  void processRdfDescription(BioEntity element) throws InvalidXmlSchemaException {
    String notes = element.getNotes();

    Matcher nodeMatcher = rdfNodePattern.matcher(notes);
    if (nodeMatcher.find()) {
      String rdfString = "<rdf:RDF" + nodeMatcher.group(1) + "</rdf:RDF>";
      element.addMiriamData(xmlAnnotationParser.parse(rdfString));

      notes = notes.substring(0, nodeMatcher.start() - "<rdf:RDF".length())
          + notes.substring(nodeMatcher.end() + "</rdf:RDF>".length());
      element.setNotes(notes);
    }
  }

  /**
   * Process notes and assign structural information from it.
   *
   * @param element
   *          object with notes to be processed
   */
  public void processNotes(Element element) {
    processNotes(element.getNotes(), element);
  }
}
