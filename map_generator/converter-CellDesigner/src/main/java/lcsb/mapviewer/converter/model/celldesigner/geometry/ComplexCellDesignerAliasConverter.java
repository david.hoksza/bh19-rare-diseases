package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.geom.*;

import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Class that provides CellDesigner specific graphical information for
 * ComplexAlias. It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<Complex> {

  /**
   * How big is the triangle trimmed part of the complex.
   */
  private static final int TRIMMED_CORNER_SIZE = 5;

  /**
   * Default constructor.
   *
   * @param sbgn
   *          Should the converter use sbgn standard
   */
  protected ComplexCellDesignerAliasConverter(boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(Complex alias, CellDesignerAnchor anchor) {
    if (invalidAnchorPosition(alias, anchor)) {
      return alias.getCenter();
    }
    return getRectangleTransformation().getPointOnRectangleByAnchor(alias.getX(), alias.getY(), alias.getWidth(),
        alias.getHeight(), anchor);
  }

  @Override
  public PathIterator getBoundPathIterator(Complex alias) {
    return getAliasPath(alias).getPathIterator(new AffineTransform());
  }

  /**
   * Returns the border of complex alias.
   * 
   * @param alias
   *          exact object for which we want to get a border
   * @return border of the alias
   */
  private GeneralPath getAliasPath(Element alias) {
    GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
    path.moveTo(alias.getX() + TRIMMED_CORNER_SIZE, alias.getY());
    path.lineTo(alias.getX() + alias.getWidth() - TRIMMED_CORNER_SIZE, alias.getY());
    path.lineTo(alias.getX() + alias.getWidth(), alias.getY() + TRIMMED_CORNER_SIZE);
    path.lineTo(alias.getX() + alias.getWidth(), alias.getY() + alias.getHeight() - TRIMMED_CORNER_SIZE);
    path.lineTo(alias.getX() + alias.getWidth() - TRIMMED_CORNER_SIZE, alias.getY() + alias.getHeight());
    path.lineTo(alias.getX() + TRIMMED_CORNER_SIZE, alias.getY() + alias.getHeight());
    path.lineTo(alias.getX(), alias.getY() + alias.getHeight() - TRIMMED_CORNER_SIZE);
    path.lineTo(alias.getX(), alias.getY() + TRIMMED_CORNER_SIZE);
    path.closePath();
    return path;
  }

}
