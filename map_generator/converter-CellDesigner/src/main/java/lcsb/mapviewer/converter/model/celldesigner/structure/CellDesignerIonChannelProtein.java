package lcsb.mapviewer.converter.model.celldesigner.structure;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.IonChannelProtein;

/**
 * Class representing CellDesigner {@link IonChannelProtein}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerIonChannelProtein extends CellDesignerProtein<IonChannelProtein> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public CellDesignerIonChannelProtein() {
    super();
  }

  /**
   * Constructor that creates a copy of species.
   * 
   * @param species
   *          original species
   */
  public CellDesignerIonChannelProtein(CellDesignerSpecies<?> species) {
    super(species);
  }

  @Override
  public CellDesignerIonChannelProtein copy() {
    if (this.getClass().equals(CellDesignerIonChannelProtein.class)) {
      return new CellDesignerIonChannelProtein(this);
    } else {
      throw new NotImplementedException("Copy method for " + this.getClass() + " class not implemented");
    }
  }

  @Override
  public IonChannelProtein createModelElement(String aliasId) {
    IonChannelProtein result = new IonChannelProtein(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

}
