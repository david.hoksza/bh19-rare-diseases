package lcsb.mapviewer.converter.model.celldesigner.species;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.*;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.species.*;

/**
 * Class used to parse CellDesigner xml nodes containing collections of
 * {@link CellDesignerSpecies}.
 * 
 * @author Piotr Gawron
 * 
 */
public class SpeciesCollectionXmlParser {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(SpeciesCollectionXmlParser.class.getName());

  /**
   * Parser used for {@link CellDesignerRna} objects.
   */
  private RnaXmlParser rnaParser;

  /**
   * Parser used for {@link CellDesignerGene} objects.
   */
  private GeneXmlParser geneParser;

  /**
   * Parser used for {@link CellDesignerProtein} objects.
   */
  private ProteinXmlParser proteinParser;

  /**
   * Parser used for {@link CellDesignerAntisenseRna} objects.
   */
  private AntisenseRnaXmlParser antisenseRnaParser;

  /**
   * Parser used for <a hred="http://sbml.org">SBML</a> structures.
   */
  private SpeciesSbmlParser sbmlSpeciesParser;

  /**
   * Parser used for extracting structure information from notes.
   */
  private RestAnnotationParser rap = new RestAnnotationParser();

  /**
   * Parser used for parsing annotation.
   */
  private XmlAnnotationParser xap = new XmlAnnotationParser();

  /**
   * Helper class used to decode/encode CellDesigner strings.
   */
  private AbstractElementXmlParser<CellDesignerElement<?>, Element> helpParser = new AbstractElementXmlParser<CellDesignerElement<?>, Element>() {
    @Override
    public Pair<String, CellDesignerElement<?>> parseXmlElement(Node node)
        throws InvalidXmlSchemaException {
      throw new NotImplementedException();
    }

    @Override
    public String toXml(Element element) throws InconsistentModelException {
      throw new NotImplementedException();
    }

    @Override
    public String encodeName(String name) {
      return super.encodeName(name);
    }
  };

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  private Collection<SbmlUnit> units;

  /**
   * Default constructor. Model is required because some CellDesigner nodes
   * require access to other parts of the model.
   * 
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public SpeciesCollectionXmlParser(CellDesignerElementCollection elements, Collection<SbmlUnit> units) {
    sbmlSpeciesParser = new SpeciesSbmlParser(elements, units);
    this.elements = elements;
    this.units = units;
    rnaParser = new RnaXmlParser(elements);
    geneParser = new GeneXmlParser(elements);
    proteinParser = new ProteinXmlParser(elements);
    antisenseRnaParser = new AntisenseRnaXmlParser(elements);
  }

  /**
   * Parses xml node with set of rnas.
   * 
   * @param rnaNode
   *          CellDesigner xml node
   * @return list of rnas (with celldesigner identifiers) taken from xml
   * 
   * @throws InvalidXmlSchemaException
   *           thrown when node contains problematic data
   */
  public List<Pair<String, ? extends CellDesignerSpecies<?>>> parseXmlRnaCollection(Node rnaNode)
      throws InvalidXmlSchemaException {
    List<Pair<String, ? extends CellDesignerSpecies<?>>> result = new ArrayList<>();
    NodeList nodes = rnaNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:RNA")) {
          Pair<String, CellDesignerRna> rna = rnaParser.parseXmlElement(node);
          result.add(rna);
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:listOfRNAs: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Transforms set of rnas into CellDesigner xml node.
   * 
   * @param collection
   *          set of rnas
   * @return CellDesigner xml node corrseponding to the input
   */
  public String rnaCollectionToXmlString(Collection<Rna> collection) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:listOfRNAs>");
    Set<String> ids = new HashSet<>();
    for (Rna species : collection) {
      String id = elements.getElementId(species);
      if (!ids.contains(id)) {
        ids.add(id);
        result.append(rnaParser.toXml(species));
      }
    }
    result.append("</celldesigner:listOfRNAs>\n");
    return result.toString();
  }

  /**
   * Parses xml node with set of genes.
   * 
   * @param geneNode
   *          CellDesigner xml node
   * @return list of genes taken from xml
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  public List<Pair<String, ? extends CellDesignerSpecies<?>>> parseXmlGeneCollection(Node geneNode)
      throws InvalidXmlSchemaException {
    List<Pair<String, ? extends CellDesignerSpecies<?>>> result = new ArrayList<>();
    NodeList nodes = geneNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:gene")) {
          Pair<String, CellDesignerGene> gene = geneParser.parseXmlElement(node);
          result.add(gene);
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:listOfGenes: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Transforms set of genes into CellDesigner xml node.
   * 
   * @param collection
   *          set of genes
   * @return CellDesigner xml node corrseponding to the input
   */
  public String geneCollectionToXmlString(Collection<Gene> collection) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:listOfGenes>");
    Set<String> ids = new HashSet<>();
    for (Gene species : collection) {
      String id = elements.getElementId(species);
      if (!ids.contains(id)) {
        ids.add(id);
        result.append(geneParser.toXml(species));
      }
    }
    result.append("</celldesigner:listOfGenes>\n");
    return result.toString();
  }

  /**
   * Parses xml node with set of protein.
   * 
   * @param proteinNode
   *          CellDesigner xml node
   * @return list of proteins (with celldesigner identifiers) taken from xml
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  public List<Pair<String, ? extends CellDesignerSpecies<?>>> parseXmlProteinCollection(Node proteinNode)
      throws InvalidXmlSchemaException {
    List<Pair<String, ? extends CellDesignerSpecies<?>>> result = new ArrayList<>();
    NodeList nodes = proteinNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:protein")) {
          Pair<String, CellDesignerProtein<?>> protein = proteinParser.parseXmlElement(node);
          result.add(protein);
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:listOfProteins: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Transforms set of proteins into CellDesigner xml node.
   * 
   * @param collection
   *          set of proteins
   * @return CellDesigner xml node corresponding to the input
   */
  public String proteinCollectionToXmlString(Collection<Protein> collection) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:listOfProteins>");
    Set<String> ids = new HashSet<>();
    for (Protein species : collection) {
      String id = elements.getElementId(species);
      if (!ids.contains(id)) {
        ids.add(id);
        result.append(proteinParser.toXml(species));
      }
    }
    result.append("</celldesigner:listOfProteins>\n");
    return result.toString();
  }

  /**
   * Parses xml node with set of antisense rnas.
   * 
   * @param rnaNode
   *          CellDesigner xml node
   * @return list of antisense rnas (with celldesigner identifiers) taken from xml
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  public List<Pair<String, ? extends CellDesignerSpecies<?>>> parseXmlAntisenseRnaCollection(Node rnaNode)
      throws InvalidXmlSchemaException {
    List<Pair<String, ? extends CellDesignerSpecies<?>>> result = new ArrayList<>();
    NodeList nodes = rnaNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:AntisenseRNA")) {
          Pair<String, CellDesignerAntisenseRna> antisenseRna = antisenseRnaParser.parseXmlElement(node);
          result.add(antisenseRna);
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:listOfAntisenseRNAs: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Transforms set of antisense rnas into CellDesigner xml node.
   * 
   * @param collection
   *          set of antisense rnas
   * @return CellDesigner xml node corrseponding to the input
   */
  public String antisenseRnaCollectionToXmlString(Collection<AntisenseRna> collection) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:listOfAntisenseRNAs>");
    Set<String> ids = new HashSet<>();
    for (AntisenseRna species : collection) {
      String id = elements.getElementId(species);
      if (!ids.contains(id)) {
        ids.add(id);
        result.append(antisenseRnaParser.toXml(species));
      }
    }
    result.append("</celldesigner:listOfAntisenseRNAs>\n");
    return result.toString();
  }

  /**
   * Parse collection of SBML species.
   * 
   * @param spieciesNode
   *          xml node (in SBML format) containing list of species
   * @return list of species (with celldesigner identifiers) from the SBML node
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  public List<Pair<String, ? extends CellDesignerSpecies<?>>> parseSbmlSpeciesCollection(Node spieciesNode)
      throws InvalidXmlSchemaException {
    List<Pair<String, ? extends CellDesignerSpecies<?>>> result = new ArrayList<>();
    NodeList nodes = spieciesNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("species")
            || node.getNodeName().equalsIgnoreCase("celldesigner:species")) {
          Pair<String, CellDesignerSpecies<?>> sp = sbmlSpeciesParser.parseXmlElement(node);
          result.add(sp);
        } else {
          throw new InvalidXmlSchemaException("Unknown element of listOfSpecies: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Transforms set of {@link CellDesignerSpecies} into SBML node.
   * 
   * @param collection
   *          set of species to transform
   * @return SBML node for set of species
   * @throws InconsistentModelException
   *           thrown when xml cannot be generated because structure of objects is
   *           invalid
   */
  public String speciesCollectionToSbmlString(Collection<Species> collection) throws InconsistentModelException {
    StringBuilder result = new StringBuilder();
    if (collection.size() > 0) {
      result.append("<listOfSpecies>");
      Set<String> addedIds = new HashSet<>();
      for (Species species : collection) {
        // not all elements should be added to the result string: elements that
        // are in complexes should be excluded
        boolean add = false;
        if (species.getComplex() == null) {
          add = true;
        }
        String sbmlId = elements.getElementId(species);
        if (add && !addedIds.contains(sbmlId)) {
          addedIds.add(sbmlId);
          result.append(sbmlSpeciesParser.toXml(species));
        }
      }
      result.append("</listOfSpecies>\n");
    }
    return result.toString();
  }

  /**
   * Parses CellDEsigner xml for complexes to retrieve included species (they
   * might not exist in other parts of the model).
   * 
   * @param spieciesNode
   *          CellDesigner xml node for included species (structure of elements in
   *          complexes)
   * @return list of species (with celldesigner identifiers) included in complexes
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  public List<Pair<String, ? extends CellDesignerSpecies<?>>> parseIncludedSpeciesCollection(Node spieciesNode)
      throws InvalidXmlSchemaException {
    List<Pair<String, ? extends CellDesignerSpecies<?>>> result = new ArrayList<>();
    // What should be done here?
    // What are the inclusions?
    // Probably this should be improved, this part of a CD model is really
    // messy.
    NodeList nodes = spieciesNode.getChildNodes();
    SpeciesSbmlParser parser = new SpeciesSbmlParser(elements, units);
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:species")) {
          String idIncluded = XmlParser.getNodeAttr("id", node);
          String nameIncluded = XmlParser.getNodeAttr("name", node);
          if (idIncluded == null || idIncluded.isEmpty()) {
            throw new InvalidXmlSchemaException("Included species does not contain id");
          } else {
            Node annNode = null;
            Node notesNode = null;
            for (int y = 0; y < node.getChildNodes().getLength(); y++) {
              Node child = node.getChildNodes().item(y);
              if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equalsIgnoreCase("celldesigner:annotation")) {
                  annNode = child;
                } else if (child.getNodeName().equalsIgnoreCase("celldesigner:notes")) {
                  notesNode = child;
                } else {
                  throw new InvalidXmlSchemaException(
                      "Unknown element of celldesigner:species: " + child.getNodeName());
                }
              }
            }

            if (annNode == null) {
              throw new InvalidXmlSchemaException("Included species does not contain annotation node");
            } else {
              Node spNode = XmlParser.getNode("celldesigner:speciesIdentity", annNode.getChildNodes());
              if (spNode != null) {
                Pair<String, CellDesignerSpecies<?>> row = parser.parseSpeciesIdentity(spNode);
                CellDesignerSpecies<?> sp = row.getRight();
                sp.setElementId(idIncluded);
                sp.setName(helpParser.decodeName(nameIncluded));
                sp.setNotes(rap.getNotes(notesNode));

                result.add(row);
              } else {
                throw new InvalidXmlSchemaException("No celldesigner:speciesIdentity node in included tag");
              }
            }

          }
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:listOfIncludedSpecies: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Creates CellDesigner included species node. This node contain information
   * about CellDesigner structure for complexes.
   * 
   * @param collection
   *          list of Species
   * @return CellDesigner included species xml node
   */
  public String speciesCollectionToXmlIncludedString(Collection<Species> collection) {
    StringBuilder sb = new StringBuilder("");
    sb.append("<celldesigner:listOfIncludedSpecies>\n");
    Set<String> processedSpecies = new HashSet<>();
    for (Element species : collection) {
      if (species instanceof Complex && !processedSpecies.contains(elements.getElementId(species))) {
        Complex cs = (Complex) species;
        processedSpecies.add(elements.getElementId(species));

        Set<String> processedChildren = new HashSet<>();

        for (Element element : cs.getElements()) {
          Species child = (Species) element;
          if (!processedChildren.contains(elements.getElementId(child))) {
            processedChildren.add(elements.getElementId(child));
            sb.append(
                "<celldesigner:species id=\"" + elements.getElementId(child) + "\" name=\""
                    + XmlParser.escapeXml(helpParser.encodeName(child.getName())) + "\">");
            sb.append("<celldesigner:notes>");
            sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title/></head><body>");
            RestAnnotationParser rap = new RestAnnotationParser();

            sb.append(rap.createAnnotationString(child));
            sb.append(child.getNotes());
            sb.append(xap.dataSetToXmlString(child.getMiriamData(), elements.getElementId(child)));
            sb.append("</body></html>");
            sb.append("</celldesigner:notes>\n");
            sb.append("<celldesigner:annotation>\n");
            sb.append("<celldesigner:complexSpecies>" + elements.getElementId(cs) + "</celldesigner:complexSpecies>\n");

            sb.append(sbmlSpeciesParser.speciesIdentityToXml(child));
            sb.append("</celldesigner:annotation>\n");
            sb.append("</celldesigner:species>\n");
          }

        }
      }
    }
    sb.append("</celldesigner:listOfIncludedSpecies>\n");
    return sb.toString();
  }
}
