package lcsb.mapviewer.common.geometry;

import static org.junit.Assert.*;

import java.awt.*;
import java.awt.geom.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.common.Configuration;

public class LineTransformationTest {
  static Logger logger = LogManager.getLogger(LineTransformationTest.class);
  static double EPSILON = Configuration.EPSILON;
  LineTransformation lineTransformation = new LineTransformation();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testIntersection() {
    Shape s = new RoundRectangle2D.Double(0.0, 0.0, 100.0, 100.0, 10.0, 10.0);
    Line2D line = new Line2D.Double(50, 50, 50, 200);
    Point2D p = lineTransformation.getIntersectionWithPathIterator(line, s.getPathIterator(new AffineTransform()));
    Point2D intPoint = new Point2D.Double(50, 100);
    double dist = p.distance(intPoint);
    assertEquals("Invalid intersection point: " + p, 0, dist, EPSILON);
  }

  @Test
  public void testIntersectionWithBezierCurve() {
    double x = 0;
    double y = 0;
    double w = 100;
    double h = 200;
    GeneralPath path = new GeneralPath();

    path.moveTo((float) (x + 0.25 * w), (float) (y + 0.25 * h));
    path.quadTo((float) (x + 0.05 * w), (float) (y + 0.25 * h), x, (float) (y + 0.5 * h));
    path.curveTo(x, (float) (y + 0.66 * h), (float) (x + 0.18 * w), (float) (y + 0.9 * h), (float) (x + 0.31 * w),
        (float) (y + 0.8 * h));
    path.curveTo((float) (x + 0.4 * w), (y + h), (float) (x + 0.7 * w), (y + h), (float) (x + 0.8 * w),
        (float) (y + 0.8 * h));
    path.curveTo((x + w), (float) (y + 0.8 * h), (x + w), (float) (y + 0.6 * h), (float) (x + 0.875 * w),
        (float) (y + 0.5 * h));
    path.curveTo((x + w), (float) (y + 0.3 * h), (float) (x + 0.8 * w), (float) (y + 0.1 * h), (float) (x + 0.625 * w),
        (float) (y + 0.2 * h));
    path.curveTo((float) (x + 0.5 * w), (float) (y + 0.05 * h), (float) (x + 0.3 * w), (float) (y + 0.05 * h),
        (float) (x + 0.25 * w), (float) (y + 0.25 * h));
    path.closePath();

    Line2D line = new Line2D.Double(50, 50, 50, 300);
    Point2D p = lineTransformation.getIntersectionWithPathIterator(line, path.getPathIterator(new AffineTransform()));
    assertNotNull(p);
  }

  @Test
  public void testIntersectionToInvalidPath() {
    Line2D line = new Line2D.Double(50, 50, 50, 200);
    Point2D p = lineTransformation.getIntersectionWithPathIterator(line, null);
    assertNull(p);

    Shape s = new Rectangle2D.Double(0.0, 0.0, 10.0, 0.0);
    p = lineTransformation.getIntersectionWithPathIterator(line, s.getPathIterator(new AffineTransform()));
    assertNull(p);

    p = lineTransformation.getIntersectionWithPathIterator(line, new PathIterator() {

      @Override
      public int getWindingRule() {
        // TODO Auto-generated method stub
        return 0;
      }

      @Override
      public boolean isDone() {
        return true;
      }

      @Override
      public void next() {
        // TODO Auto-generated method stub

      }

      @Override
      public int currentSegment(float[] coords) {
        // TODO Auto-generated method stub
        return 0;
      }

      @Override
      public int currentSegment(double[] coords) {
        // TODO Auto-generated method stub
        return 0;
      }
    });
    assertNull(p);
  }

  @Test
  public void testDistance() {
    assertNotNull(lineTransformation);
    Point2D p1 = new Point2D.Double(2, 2);
    Point2D p2 = new Point2D.Double(3, 3);
    Point2D p3 = new Point2D.Double(4, 4);

    double dist = lineTransformation.distBetweenPointAndLineSegment(p1, p2, p3);
    double d = Math.sqrt(2);
    assertEquals(dist, d, EPSILON);

    p3 = new Point2D.Double(3, 4);
    dist = lineTransformation.distBetweenPointAndLineSegment(p1, p2, p3);
    assertEquals(dist, 1, EPSILON);

    p3 = new Point2D.Double(3, 3);
    dist = lineTransformation.distBetweenPointAndLineSegment(p1, p2, p3);
    assertEquals(dist, 0, EPSILON);

    p3 = new Point2D.Double(2.5, 2.5);
    dist = lineTransformation.distBetweenPointAndLineSegment(p1, p2, p3);
    assertEquals(dist, 0, EPSILON);

    p3 = new Point2D.Double(2, 2);
    dist = lineTransformation.distBetweenPointAndLineSegment(p1, p2, p3);
    assertEquals(dist, 0, EPSILON);

    p3 = new Point2D.Double(2, 0);
    dist = lineTransformation.distBetweenPointAndLineSegment(p1, p2, p3);
    assertEquals(dist, 2, EPSILON);

    p3 = new Point2D.Double(3, 2);
    dist = lineTransformation.distBetweenPointAndLineSegment(p1, p2, p3);
    d = Math.sqrt(0.5);
    assertEquals(dist, d, EPSILON);

  }

  @Test
  public void testDistanceToEmptyLine() {
    Point2D p1 = new Point2D.Double(2, 2);
    Point2D p2 = new Point2D.Double(3, 3);
    double dist = lineTransformation.distBetweenPointAndLineSegment(p1, p1, p2);
    assertEquals(p1.distance(p2), dist, EPSILON);

    // the same with line
    Line2D line = new Line2D.Double(p1, p1);
    dist = lineTransformation.distBetweenPointAndLineSegment(line, p2);
    assertEquals(p1.distance(p2), dist, EPSILON);
  }

  @Test
  public void testClosestPointOnSegmentLineToPointOnEmptyLine() {
    Point2D p1 = new Point2D.Double(2, 2);
    Point2D p2 = new Point2D.Double(3, 3);
    Point2D p3 = lineTransformation.closestPointOnSegmentLineToPoint(p1, p1, p2);
    assertEquals(p3, p1);

    // the same with line
    Line2D line = new Line2D.Double(p1, p1);
    p3 = lineTransformation.closestPointOnSegmentLineToPoint(line, p2);
    assertEquals(p3, p1);

  }

  @Test
  public void testClosestPoint() {
    assertNotNull(lineTransformation);
    Point2D p1 = new Point2D.Double(2, 2);
    Point2D p2 = new Point2D.Double(3, 3);
    Point2D p3 = new Point2D.Double(4, 4);

    Point2D res = lineTransformation.closestPointOnSegmentLineToPoint(p1, p2, p3);
    double d = Math.sqrt(2);
    assertEquals(res.distance(p3), d, EPSILON);

    p3 = new Point2D.Double(3, 4);
    res = lineTransformation.closestPointOnSegmentLineToPoint(p1, p2, p3);
    assertEquals(res.distance(p3), 1, EPSILON);

    p3 = new Point2D.Double(3, 3);
    res = lineTransformation.closestPointOnSegmentLineToPoint(p1, p2, p3);
    assertEquals(res.distance(p3), 0, EPSILON);

    p3 = new Point2D.Double(2.5, 2.5);
    res = lineTransformation.closestPointOnSegmentLineToPoint(p1, p2, p3);
    assertEquals(res.distance(p3), 0, EPSILON);

    p3 = new Point2D.Double(2, 2);
    res = lineTransformation.closestPointOnSegmentLineToPoint(p1, p2, p3);
    assertEquals(res.distance(p3), 0, EPSILON);

    p3 = new Point2D.Double(2, 0);
    res = lineTransformation.closestPointOnSegmentLineToPoint(p1, p2, p3);
    assertEquals(res.distance(p3), 2, EPSILON);

    p3 = new Point2D.Double(3, 2);
    res = lineTransformation.closestPointOnSegmentLineToPoint(p1, p2, p3);
    d = Math.sqrt(0.5);
    assertEquals(res.distance(p3), d, EPSILON);

  }
}
