package lcsb.mapviewer.common.geometry;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class TextAlignmentTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (TextAlignment type : TextAlignment.values()) {
      assertNotNull(type);

      // for coverage tests
      TextAlignment.valueOf(type.toString());
    }
  }

}
