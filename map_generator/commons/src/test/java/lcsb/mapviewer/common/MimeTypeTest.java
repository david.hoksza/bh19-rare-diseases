package lcsb.mapviewer.common;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class MimeTypeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (MimeType type : MimeType.values()) {
      assertNotNull(type);
      assertNotNull(type.getTextRepresentation());
      assertFalse(type.getTextRepresentation().isEmpty());

      // for coverage tests
      MimeType.valueOf(type.toString());
    }
  }

}
