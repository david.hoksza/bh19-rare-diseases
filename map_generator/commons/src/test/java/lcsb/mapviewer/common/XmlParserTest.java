package lcsb.mapviewer.common;

import static org.junit.Assert.*;

import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.ElementImpl;
import org.junit.*;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;

public class XmlParserTest extends CommonTestFunctions {
  Logger logger = LogManager.getLogger(XmlParserTest.class);
  boolean threadSucceded;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testColorParsing() {
    String strColor = "ffcbcd09";

    Color c = XmlParser.stringToColor(strColor);
    String resultString = XmlParser.colorToString(c);
    assertTrue("Different string representation: " + strColor + " - " + resultString,
        strColor.equalsIgnoreCase(resultString));

    Color c2 = XmlParser.stringToColor(resultString);

    assertEquals(c.getRed(), c2.getRed());
    assertEquals(c.getGreen(), c2.getGreen());
    assertEquals(c.getBlue(), c2.getBlue());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidColorParsing() {
    String strColor = "hello world";

    XmlParser.stringToColor(strColor);
  }

  @Test
  public void testColorParsingWithAlpha() {
    String strColor = "fecbcd09";

    Color c = XmlParser.stringToColor(strColor);
    String resultString = XmlParser.colorToString(c);
    assertTrue("Different string representation: " + strColor + " - " + resultString,
        strColor.equalsIgnoreCase(resultString));

    Color c2 = XmlParser.stringToColor(resultString);

    assertEquals(c.getRed(), c2.getRed());
    assertEquals(c.getGreen(), c2.getGreen());
    assertEquals(c.getBlue(), c2.getBlue());
    assertEquals(c.getAlpha(), c2.getAlpha());
  }

  @Test
  public void testGetXmlDocumentFromString() throws Exception {
    Document validDoc = XmlParser.getXmlDocumentFromString("<node>test</node>");
    assertNotNull(validDoc);

  }

  @Test
  public void testNodeToString() throws Exception {
    String xml = "<test_node>test_x</test_node>";
    Document validDoc = XmlParser.getXmlDocumentFromString(xml);
    String str = XmlParser.nodeToString(validDoc);
    assertEquals(xml.trim(), str.trim());
  }

  @Test
  public void testEmptyNodeToString() throws Exception {
    String str = XmlParser.nodeToString(null);
    assertNull(str);
  }

  @Test
  public void testNodeToStringWithHeader() throws Exception {
    String xml = "<test_node>test_x</test_node>";
    Document validDoc = XmlParser.getXmlDocumentFromString(xml);
    String str = XmlParser.nodeToString(validDoc, true);
    assertTrue(str.contains(xml));
  }

  @Test
  public void testConcurrencyParse() throws Exception {
    StringBuilder builder = new StringBuilder("<doc>");
    for (int i = 0; i < 30000; i++)
      builder.append("<test_node>test_x</test_node>");
    builder.append("</doc>");
    final String xml = builder.toString();
    threadSucceded = false;
    Thread t1 = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          Document validDoc = XmlParser.getXmlDocumentFromString(xml);
          assertNotNull(validDoc);
          threadSucceded = true;
        } catch (InvalidXmlSchemaException e) {
          e.printStackTrace();
        }
      }
    });
    t1.start();
    assertNotNull(XmlParser.getXmlDocumentFromString(xml));
    t1.join();
    assertTrue("There was a problem with multithreading", threadSucceded);
  }

  @Test
  public void testInvalidNodeToString() throws Exception {
    final DocumentImpl xmlDoc = new DocumentImpl();

    Element root = xmlDoc.createElement("booking");

    class Tmp extends ElementImpl {
      /**
       * 
       */
      private static final long serialVersionUID = 1L;

      public Tmp() {
        this.ownerDocument = xmlDoc;
      }

      @Override
      public NamedNodeMap getAttributes() {
        return null;
      }

    }
    ;
    Element el = new Tmp();
    root.appendChild(el);
    assertNotNull(XmlParser.nodeToString(root, true));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testGetXmlDocumentFromInvalidString() throws Exception {
    XmlParser.getXmlDocumentFromString("<node>test<node>");
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testGetXmlDocumentFromInvalidInputStream() throws Exception {
    XmlParser.getXmlDocumentFromInputSource(new InputSource(), true);
  }

  @Test
  public void testEscapeXml() throws Exception {
    String str = XmlParser.escapeXml("<xml>node</xml>");
    assertFalse(str.contains("<"));
  }

  @Test
  public void testEscapeNullXml() throws Exception {
    String str = XmlParser.escapeXml(null);
    assertNull(str);
  }

  @Test
  public void testFileToString() throws Exception {
    String str = XmlParser.fileToString("testFiles/test.txt");
    assertTrue(str.contains("test"));
    assertTrue(str.contains("file"));
    assertTrue(str.contains("with some content"));
  }

  @Test
  public void testInputStreamToString() throws Exception {
    InputStream stream = new ByteArrayInputStream("stream string".getBytes(StandardCharsets.UTF_8));
    String str = XmlParser.inputStreamToString(stream);
    assertEquals("stream string", str);
  }

  @Test
  public void testGetNodeValue() throws Exception {
    Document document = XmlParser.getXmlDocumentFromString("<node>content</node>");
    Node node = XmlParser.getNode("node", document);
    String str = XmlParser.getNodeValue(node);
    assertEquals("content", str);
  }

  @Test
  public void testGetNodeValue2() throws Exception {
    String str = XmlParser.getNodeValue(null);
    assertEquals("", str);
  }

  @Test
  public void testGetNodeValue3() throws Exception {
    Document document = XmlParser.getXmlDocumentFromString("<node></node>");
    Node node = XmlParser.getNode("node", document);
    String str = XmlParser.getNodeValue(node);
    assertEquals("", str);
  }

  @Test
  public void testGetNodeValue4() throws Exception {
    Document document = XmlParser.getXmlDocumentFromString("<node><subnode/></node>");
    Node node = XmlParser.getNode("node", document);
    String str = XmlParser.getNodeValue(node);
    assertEquals("", str);
  }

  @Test(expected = IOException.class)
  public void testInputStreamToStringThrowsException() throws Exception {
    XmlParser.inputStreamToString(new InputStream() {
      @Override
      public int read() throws IOException {
        throw new IOException();
      }
    });
  }

  @Test(expected = IOException.class)
  public void testFileToStringThrowsException() throws Exception {
    XmlParser.fileToString("testFiles/unknown file.txt");
  }

  @Test
  public void testGetNodeAttr() throws Exception {
    Document document = XmlParser.getXmlDocumentFromString("<node attr=\"val\">content</node>");
    Node node = XmlParser.getNode("node", document);
    String str = XmlParser.getNodeAttr("attr", node);
    assertEquals("val", str);
    str = XmlParser.getNodeAttr("attr2", node);
    assertEquals("", str);
  }

  @Test
  public void testGetNodes() throws Exception {
    Document document = XmlParser
        .getXmlDocumentFromString("<node><subnode>content1</subnode><subnode>content2</subnode><other/></node>");
    Node node = XmlParser.getNode("node", document);
    List<Node> nodes = XmlParser.getNodes("subnode", node.getChildNodes());
    assertEquals(2, nodes.size());
  }

  @Test
  public void testGetNode() throws Exception {
    Document document = XmlParser
        .getXmlDocumentFromString("<node><subnode>content1</subnode><subnode>content2</subnode><other/></node>");
    Node node = XmlParser.getNode("node", document);
    Node child = XmlParser.getNode("other", node.getChildNodes());
    assertNotNull(child);
  }

  @Test
  public void testGetNode2() throws Exception {
    Document document = XmlParser.getXmlDocumentFromString(
        "<node attr=\"x\"><subnode>content1</subnode><subnode>content2</subnode><other/></node>");
    Node node = XmlParser.getNode("node", document);
    Node child = XmlParser.getNode("other2", node.getChildNodes());
    assertNull(child);
  }

  @Test
  public void testGetNode3() throws Exception {
    Document document = XmlParser.getXmlDocumentFromString("<node attr=\"x\">xxx</node>");
    Node node = XmlParser.getNode("node", document);
    Node child = XmlParser.getNode("other2", node.getChildNodes());
    assertNull(child);
  }

  @Test
  public void testLowercaseXmlNames() throws Exception {
    String inputXml = "<node><ASD>Test</ASD></node>";
    String outputXml = XmlParser.lowercaseXmlNames(inputXml);

    assertTrue(outputXml.indexOf("asd") >= 0);
    assertTrue(outputXml.indexOf("Test") >= 0);
  }
}
