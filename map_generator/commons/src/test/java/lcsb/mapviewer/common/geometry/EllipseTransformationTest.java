package lcsb.mapviewer.common.geometry;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.common.Configuration;

public class EllipseTransformationTest {
  Logger logger = LogManager.getLogger(EllipseTransformationTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testPointOnEllipse() {
    EllipseTransformation et = new EllipseTransformation();
    Point2D point = et.getPointOnEllipseByDegree(0, 0, 200, 200, 0);
    assertTrue(point.distance(0, 100) <= Configuration.EPSILON);
  }

  @Test
  public void testPointOnEllipse2() {
    EllipseTransformation et = new EllipseTransformation();
    Point2D point = et.getPointOnEllipseByDegree(0, 0, 200, 200, 90);
    assertTrue(point.distance(100, 0) <= Configuration.EPSILON);
  }

  @Test
  public void testPointOnEllipse3() {
    EllipseTransformation et = new EllipseTransformation();
    Point2D point = et.getPointOnEllipseByDegree(0, 0, 200, 200, 180);
    assertTrue(point.distance(200, 100) <= Configuration.EPSILON);
  }

  @Test
  public void testPointOnEllipse4() {
    EllipseTransformation et = new EllipseTransformation();
    Point2D point = et.getPointOnEllipseByDegree(0, 0, 200, 200, 270);
    assertTrue(point.distance(100, 200) <= Configuration.EPSILON);
  }

  @Test
  public void testPointOnEllipse5() {
    EllipseTransformation et = new EllipseTransformation();
    Point2D point = et.getPointOnEllipseByDegree(0, 0, 200, 200, 360);
    assertTrue(point.distance(0, 100) <= Configuration.EPSILON);
  }

  @Test
  public void testPointOnEllipse6() {
    EllipseTransformation et = new EllipseTransformation();
    Point2D point = et.getPointOnEllipseByDegree(0, 0, 200, 200, 300);
    assertNotNull(point);
  }

  @Test
  public void testPointOnEllipse7() {
    EllipseTransformation et = new EllipseTransformation();
    Point2D point = et.getPointOnEllipseByDegree(0, 0, 200, 200, -360);
    assertTrue(point.distance(0, 100) <= Configuration.EPSILON);
  }

  @Test
  public void testPointOnEllipse8() {
    EllipseTransformation et = new EllipseTransformation();
    Point2D point = et.getPointOnEllipseByDegree(new Ellipse2D.Double(0, 0, 200, 200), -360);
    assertTrue(point.distance(0, 100) <= Configuration.EPSILON);
  }

}
