package lcsb.mapviewer.common.exception;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class InvalidArgumentExceptionTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor1() {
    assertNotNull(new InvalidArgumentException());
  }

  @Test
  public void testConstructor2() {
    assertNotNull(new InvalidArgumentException("str"));
  }

  @Test
  public void testConstructor3() {
    assertNotNull(new InvalidArgumentException(new Exception()));
  }

  @Test
  public void testConstructor4() {
    assertNotNull(new InvalidArgumentException("dsr", new Exception()));
  }

}
