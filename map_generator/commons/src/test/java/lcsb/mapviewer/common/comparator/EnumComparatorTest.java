package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import lcsb.mapviewer.common.MimeType;

public class EnumComparatorTest {

  @Test
  public void testEquals() {
    EnumComparator<MimeType> comp = new EnumComparator<>();
    assertEquals(0, comp.compare(MimeType.CSS, MimeType.CSS));
  }

  @Test
  public void testDifferent() {
    EnumComparator<MimeType> comp = new EnumComparator<>();
    assertTrue(0 != comp.compare(MimeType.CSS, MimeType.JPG));
  }

}
