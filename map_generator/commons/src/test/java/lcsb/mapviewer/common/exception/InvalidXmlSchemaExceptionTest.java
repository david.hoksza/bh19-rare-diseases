package lcsb.mapviewer.common.exception;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class InvalidXmlSchemaExceptionTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor1() {
    assertNotNull(new InvalidXmlSchemaException());
  }

  @Test
  public void testConstructor2() {
    assertNotNull(new InvalidXmlSchemaException("str"));
  }

  @Test
  public void testConstructor3() {
    assertNotNull(new InvalidXmlSchemaException(new Exception()));
  }

  @Test
  public void testConstructor4() {
    assertNotNull(new InvalidXmlSchemaException("dsr", new Exception()));
  }

}
