package lcsb.mapviewer.common.geometry;

import java.awt.geom.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;

/**
 * Class with basic operators on lines.
 * 
 * @author Piotr Gawron
 * 
 */
public class LineTransformation {

  /**
   * Which value in the PathIterartor segment information array correspond to X
   * coordinate in the cubicTo type.
   */
  private static final int SEG_CUBICTO_END_Y_COORDINATE_INDEX = 5;
  /**
   * Which value in the PathIterartor segment information array correspond to Y
   * coordinate in the cubicTo type.
   */
  private static final int SEG_CUBICTO_END_X_COORDINATE_INDEX = 4;
  /**
   * Length of the PathIterartor segment information array.
   */
  private static final int PATH_ITERATOR_COORDINATES_STRUCT_SIZE = 6;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(LineTransformation.class.getName());

  /**
   * Returns a cross point between path and a line.
   * 
   * @param line
   *          line for which we are looking for the intersection
   * @param pi
   *          PathIterator for which we are looking for the intersection
   * @return point of intersection between path and line if the points doesn't
   *         exist then null is returned
   */
  public Point2D getIntersectionWithPathIterator(final Line2D line, final PathIterator pi) {
    if (pi == null) {
      return null;
    }
    double[] coordinates = new double[PATH_ITERATOR_COORDINATES_STRUCT_SIZE];
    Point2D first = null;
    Point2D last = null;
    Point2D actual = null;
    while (!pi.isDone()) {
      int type = pi.currentSegment(coordinates);
      last = actual;
      actual = new Point2D.Double(coordinates[0], coordinates[1]);
      switch (type) {
      case PathIterator.SEG_MOVETO:
        break;
      case PathIterator.SEG_LINETO:
        break;
      case PathIterator.SEG_QUADTO:
        break;
      case PathIterator.SEG_CUBICTO:

        // in case when there is an arc we define only end points of the arc
        // as a border
        actual = new Point2D.Double(coordinates[SEG_CUBICTO_END_X_COORDINATE_INDEX],
            coordinates[SEG_CUBICTO_END_Y_COORDINATE_INDEX]);
        break;
      case PathIterator.SEG_CLOSE:
        actual = first;
        break;
      default:
        break;
      }
      if (first == null) {
        first = actual;

        // check if the two following points are not the same (this could cause
        // NaN values)
      } else if (last.distance(actual) > Configuration.EPSILON) {
        double x1 = last.getX();
        double y1 = last.getY();
        double x2 = actual.getX();
        double y2 = actual.getY();
        double x3 = line.getX1();
        double y3 = line.getY1();
        double x4 = line.getX2();
        double y4 = line.getY2();
        if (Line2D.linesIntersect(x1, y1, x2, y2, x3, y3, x4, y4)) {
          double d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
          double xi = ((x3 - x4) * (x1 * y2 - y1 * x2) - (x1 - x2) * (x3 * y4 - y3 * x4)) / d;
          double yi = ((y3 - y4) * (x1 * y2 - y1 * x2) - (y1 - y2) * (x3 * y4 - y3 * x4)) / d;
          return new Point2D.Double(xi, yi);
        }
      }

      pi.next();
    }
    return null;
  }

  /**
   * Returns a distance between line segment and a point.
   * 
   * @param line
   *          line segment to which we are looking the shortest distance
   * @param point
   *          point from which are looking the shortest distance
   * @return distance between line and a point
   */
  public double distBetweenPointAndLineSegment(final Line2D line, final Point2D point) {
    return distBetweenPointAndLineSegment(line.getP1(), line.getP2(), point);
  }

  /**
   * Returns a distance between line segment (defined by v and w points) and a
   * point.
   * 
   * @param v
   *          start point of the line
   * @param w
   *          end point of the line
   * @param point
   *          point from which we want to find a distance
   * @return distance between point and line segment
   */
  public double distBetweenPointAndLineSegment(final Point2D v, final Point2D w, final Point2D point) {
    // Return minimum distance between line segment vw and point p
    double l2 = v.distanceSq(w); // i.e. |w-v|^2 - avoid a sqrt
    if (l2 == 0.0) {
      return point.distance(v); // v == w case
    }
    // Consider the line extending the segment, parameterized as v + t (w - v).
    // We find projection of point p onto the line.
    // It falls where t = [(p-v) . (w-v)] / |w-v|^2
    double t = ((point.getX() - v.getX()) * (w.getX() - v.getX()) + (point.getY() - v.getY()) * (w.getY() - v.getY()))
        / l2;
    if (t < 0.0) {
      return point.distance(v); // Beyond the 'v' end of the segment
    } else if (t > 1.0) {
      return point.distance(w); // Beyond the 'w' end of the segment
    }
    // Projection falls on the segment
    return point.distance(v.getX() + t * (w.getX() - v.getX()), v.getY() + t * (w.getY() - v.getY()));
  }

  /**
   * Returns a point that is on the line and is as close as possible to the point.
   * 
   * @param line
   *          line on which the point should be found
   * @param point
   *          point to which the result should be as close as possible
   * @return point on the line that is as close as possible to the parameter point
   */
  public Point2D closestPointOnSegmentLineToPoint(final Line2D line, final Point2D point) {
    Point2D result = closestPointOnSegmentLineToPoint(line.getP1(), line.getP2(), point);
    return result;
  }

  /**
   * Returns a point that is on the line (v-w) and is as close as possible to the
   * point.
   * 
   * @param v
   *          start of the line segment
   * @param w
   *          end of the line segment
   * @param p
   *          point to which the result should be as close as possible
   * @return point on the line (v-w) that is as close as possible to the parameter
   *         point
   */
  public Point2D closestPointOnSegmentLineToPoint(final Point2D v, final Point2D w, final Point2D p) {
    // Return minimum distance between line segment vw and point p
    double l2 = v.distanceSq(w); // i.e. |w-v|^2 - avoid a sqrt
    if (l2 == 0.0) {
      return v; // v == w case
    }
    // Consider the line extending the segment, parameterized as v + t (w - v).
    // We find projection of point p onto the line.
    // It falls where t = [(p-v) . (w-v)] / |w-v|^2
    double t = ((p.getX() - v.getX()) * (w.getX() - v.getX()) + (p.getY() - v.getY()) * (w.getY() - v.getY())) / l2;
    if (t < 0.0) {
      return v; // Beyond the 'v' end of the segment
    } else if (t > 1.0) {
      return w; // Beyond the 'w' end of the segment
    }
    // Projection falls on the segment
    return new Point2D.Double(v.getX() + t * (w.getX() - v.getX()), v.getY() + t * (w.getY() - v.getY()));
  }

}
