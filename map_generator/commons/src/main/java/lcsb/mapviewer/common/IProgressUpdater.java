package lcsb.mapviewer.common;

/**
 * Progress updater interface. It contains only one function that updates
 * information about process progress (from 0 to 100).
 * 
 * @author Piotr Gawron
 * 
 */
public interface IProgressUpdater {
  /**
   * Maximum progress value.
   */
  double MAX_PROGRESS = 100.0;
  /**
   * Defines the minimum progress value change that should be propagated.
   */
  double PROGRESS_BAR_UPDATE_RESOLUTION = 0.5;

  /**
   * Set progress status.
   * 
   * @param progress
   *          progress value (between 0 and 100)
   */
  void setProgress(double progress);
}
