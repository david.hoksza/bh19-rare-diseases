package lcsb.mapviewer.common.comparator;

import java.awt.Color;

/**
 * Comparator implementation for {@link Color} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class ColorComparator extends AbstractComparator<Color> {

  @Override
  public int compare(Color arg0, Color arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }

    } else if (arg1 == null) {
      return -1;
    }
    return ((Integer) arg0.getRGB()).compareTo(arg1.getRGB());
  }

}
