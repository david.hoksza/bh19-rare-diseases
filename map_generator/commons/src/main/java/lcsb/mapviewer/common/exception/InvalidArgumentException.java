package lcsb.mapviewer.common.exception;

/**
 * Exception to be thrown when one of the argument is invalid.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidArgumentException extends RuntimeException {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor - initializes instance variable to unknown.
   */

  public InvalidArgumentException() {
    super(); // call superclass constructor
  }

  /**
   * Public constructor with parent exception that was catched.
   * 
   * @param e
   *          parent exception
   */

  public InvalidArgumentException(final String e) {
    super(e);
  }

  /**
   * Public constructor with parent exception that was catched.
   * 
   * @param e
   *          parent exception
   */
  public InvalidArgumentException(final Exception e) {
    super(e);
  }

  /**
   * Public constructor with parent exception that was catched.
   * 
   * @param message
   *          exception message
   * @param e
   *          parent exception
   */
  public InvalidArgumentException(final String message, final Exception e) {
    super(message, e);
  }

}
