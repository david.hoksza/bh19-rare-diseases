package lcsb.mapviewer.common.comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Comparator used for {@link String} class. It's null safe (it allows to
 * compare strings that are null).
 * 
 * @author Piotr Gawron
 * 
 */
public class StringComparator extends AbstractComparator<String> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(StringComparator.class);

  @Override
  public int compare(String arg0, String arg1) {
    return compare(arg0, arg1, false);
  }

  /**
   * Allows to compare two strings ignoring whitespace difference.
   * 
   * @param arg0
   *          first string to compare
   * @param arg1
   *          second string to compare
   * @param ignoreWhiteSpaceDifference
   *          should the difference in whitespace be ignored
   * @return 0 when strings are identical, -1/1 when they are different
   */
  public int compare(String arg0, String arg1, boolean ignoreWhiteSpaceDifference) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }

    if (ignoreWhiteSpaceDifference) {
      String str1 = arg0.replaceAll("[\n\r\t\\ ]+", "\n");
      String str2 = arg1.replaceAll("[\n\r\t\\ ]+", "\n");
      return str1.trim().compareTo(str2.trim());
    }
    return arg0.trim().compareTo(arg1.trim());
  }

}
