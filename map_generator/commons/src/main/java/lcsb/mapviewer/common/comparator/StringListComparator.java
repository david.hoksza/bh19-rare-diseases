package lcsb.mapviewer.common.comparator;

import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Comparator used for list of strings.
 * 
 * @author Piotr Gawron
 * 
 */
public class StringListComparator implements Comparator<List<String>> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(StringListComparator.class);

  /**
   * String comparator used for comparing strings.
   */
  private StringComparator stringComparator = new StringComparator();
  /**
   * Integer comparator used for comparing integers.
   */
  private IntegerComparator integerComparator = new IntegerComparator();

  @Override
  public int compare(List<String> arg0, List<String> arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }

    if (arg0.size() != arg1.size()) {
      logger.debug("String lists have different size: " + arg0.size() + ", " + arg1.size());
      return integerComparator.compare(arg0.size(), arg1.size());
    }

    for (int i = 0; i < arg0.size(); i++) {
      if (stringComparator.compare(arg0.get(i), arg1.get(i)) != 0) {
        logger.debug("Strings in list different: \"" + arg0.get(i) + "\", \"" + arg1.get(i) + "\"");
        return stringComparator.compare(arg0.get(i), arg1.get(i));
      }
    }

    return 0;
  }

}
