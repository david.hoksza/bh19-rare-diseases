package lcsb.mapviewer.common.comparator;

import java.awt.geom.Line2D;
import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;

/**
 * Comparator used for {@link Line2D} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class LineComparator implements Comparator<Line2D> {

  private static Logger logger = LogManager.getLogger(LineComparator.class);

  private PointComparator pointComparator;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public LineComparator(double epsilon) {
    this.pointComparator = new PointComparator(epsilon);
  }

  /**
   * Default constructor.
   */
  public LineComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  public int compare(Line2D arg0, Line2D arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }

    } else if (arg1 == null) {
      return -1;
    }
    if (pointComparator.compare(arg0.getP1(), arg1.getP1()) != 0) {
      logger.debug("Line start different: [" + arg0.getP1() + ", " + arg0.getP2() + "] - [" + arg1.getP1() + ", "
          + arg1.getP2() + "]");
      return pointComparator.compare(arg0.getP1(), arg1.getP1());
    } else if (pointComparator.compare(arg0.getP2(), arg1.getP2()) != 0) {
      logger.debug("Line end different: [" + arg0.getP1() + ", " + arg0.getP2() + "] - [" + arg1.getP1() + ", "
          + arg1.getP2() + "]");
      return pointComparator.compare(arg0.getP2(), arg1.getP2());
    } else {
      return 0;
    }
  }

}
