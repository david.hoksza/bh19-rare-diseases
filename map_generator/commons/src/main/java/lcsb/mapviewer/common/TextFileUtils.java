package lcsb.mapviewer.common;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Class with util function to operate on text files.
 * 
 * @author Piotr Gawron
 *
 */
public final class TextFileUtils {

  /**
   * Name of the param that contains number of columns.
   */
  public static final String COLUMN_COUNT_PARAM = "__COLUMN_COUNT";

  /**
   * Default constructor that prevents instatiation.
   */
  private TextFileUtils() {

  }

  /**
   * Parses input stream to get parameters from header. {@link InputStream} should
   * be a text file. Header parameters are lines at the beginning of the file
   * starting with '#' character.
   * 
   * @param is
   *          input stream for a file to process
   * @return map with parameters parsed from input stream
   * @throws IOException
   *           thrown when there is a problem with accessing input stream
   */
  public static Map<String, String> getHeaderParametersFromFile(InputStream is) throws IOException {
    Map<String, String> result = new HashMap<>();
    BufferedReader in = new BufferedReader(new InputStreamReader(is));
    String line = null;
    while ((line = in.readLine()) != null) {
      if (line.startsWith("#")) {
        String tmp = line.substring(1);
        if (tmp.indexOf("=") > 0) {
          String key = tmp.substring(0, tmp.indexOf("=")).trim();
          String value = tmp.substring(tmp.indexOf("=") + 1).trim();
          result.put(key, value);
        }
      } else {
        String key = COLUMN_COUNT_PARAM;
        String value = line.split("\t").length + "";
        result.put(key, value);
        break;
      }
    }
    is.close();
    return result;
  }

}
