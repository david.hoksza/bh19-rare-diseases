package lcsb.mapviewer.converter;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import lcsb.mapviewer.converter.zip.LayoutZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFile;

public class ComplexZipConverterParamsTest {

  @Test
  public void testCaseInsensivity() {
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    ZipEntryFile entry = new LayoutZipEntryFile("TEST.xml", "test", null);
    params.entry(entry);
    assertNotNull(params.getEntry("TEST.xml"));
    assertNotNull(params.getEntry("test.xml"));
    assertNotNull(params.getEntry("TEST.XML"));
  }

}
