package lcsb.mapviewer.converter;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import com.google.common.io.Files;

import lcsb.mapviewer.converter.zip.ImageZipEntryFile;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class OverviewParserTest extends ConverterTestFunctions {
  private static final String TEST_FILES_VALID_OVERVIEW_ZIP = "testFiles/valid_overview.zip";
  private static final String TEST_FILES_VALID_OVERVIEW_CASE_SENSITIVE_ZIP = "testFiles/valid_overview_case_sensitive.zip";
  Logger logger = LogManager.getLogger(OverviewParserTest.class);
  OverviewParser parser = new OverviewParser();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParsingValidFile() throws Exception {
    Set<Model> models = createValidTestMapModel();
    List<ImageZipEntryFile> imageEntries = createImageEntries(TEST_FILES_VALID_OVERVIEW_ZIP);
    List<OverviewImage> result = parser.parseOverviewLinks(models, imageEntries, null,
        new ZipFile(TEST_FILES_VALID_OVERVIEW_ZIP));
    assertNotNull(result);
    assertEquals(1, result.size());

    OverviewImage img = result.get(0);

    assertEquals("test.png", img.getFilename());
    assertEquals((Integer) 639, img.getHeight());
    assertEquals((Integer) 963, img.getWidth());
    assertEquals(2, img.getLinks().size());

    OverviewLink link = img.getLinks().get(0);
    List<Point2D> polygon = link.getPolygonCoordinates();
    assertEquals(4, polygon.size());

    assertTrue(link instanceof OverviewModelLink);

    OverviewModelLink mLink = (OverviewModelLink) link;
    Model mainModel = models.iterator().next();
    assertEquals(mainModel.getModelData(), mLink.getLinkedModel());
    assertEquals((Integer) 10, mLink.getxCoord());
    assertEquals((Integer) 10, mLink.getyCoord());
    assertEquals((Integer) 3, mLink.getZoomLevel());
  }

  @Test
  public void testParsingValidCaseSensitiveFile() throws Exception {
    Set<Model> models = createValidTestMapModel();
    List<ImageZipEntryFile> imageEntries = createImageEntries(TEST_FILES_VALID_OVERVIEW_CASE_SENSITIVE_ZIP);
    for (ImageZipEntryFile imageZipEntryFile : imageEntries) {
      imageZipEntryFile.setFilename(imageZipEntryFile.getFilename().toLowerCase());
    }
    List<OverviewImage> result = parser.parseOverviewLinks(models, imageEntries, null,
        new ZipFile(TEST_FILES_VALID_OVERVIEW_CASE_SENSITIVE_ZIP));
    assertNotNull(result);
    assertEquals(1, result.size());

    OverviewImage img = result.get(0);

    assertEquals("test.png", img.getFilename());
    assertEquals((Integer) 639, img.getHeight());
    assertEquals((Integer) 963, img.getWidth());
    assertEquals(2, img.getLinks().size());

    OverviewLink link = img.getLinks().get(0);
    List<Point2D> polygon = link.getPolygonCoordinates();
    assertEquals(4, polygon.size());

    assertTrue(link instanceof OverviewModelLink);

    OverviewModelLink mLink = (OverviewModelLink) link;
    Model mainModel = models.iterator().next();
    assertEquals(mainModel.getModelData(), mLink.getLinkedModel());
    assertEquals((Integer) 10, mLink.getxCoord());
    assertEquals((Integer) 10, mLink.getyCoord());
    assertEquals((Integer) 3, mLink.getZoomLevel());
  }

  private List<ImageZipEntryFile> createImageEntries(String string) throws IOException {
    List<ImageZipEntryFile> result = new ArrayList<>();

    ZipFile zipFile = new ZipFile(string);
    try {
      Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        if (!entry.isDirectory()) {
          result.add(new ImageZipEntryFile(entry.getName()));
        }
      }
      return result;
    } finally {
      zipFile.close();
    }
  }

  @Test
  public void testParsingValidFile2() throws Exception {
    Set<Model> models = createValidTestMapModel();

    String tmpDir = Files.createTempDir().getAbsolutePath();

    List<ImageZipEntryFile> imageEntries = createImageEntries(TEST_FILES_VALID_OVERVIEW_ZIP);
    List<OverviewImage> result = parser.parseOverviewLinks(models, imageEntries, tmpDir,
        new ZipFile(TEST_FILES_VALID_OVERVIEW_ZIP));

    assertTrue(new File(tmpDir + "/test.png").exists());

    assertNotNull(result);
    assertEquals(1, result.size());
    OverviewImage img = result.get(0);
    assertEquals("test.png", img.getFilename());
  }

  @Test(expected = InvalidOverviewFile.class)
  public void testParsingInvalidFile1() throws Exception {
    List<ImageZipEntryFile> imageEntries = createImageEntries("testFiles/invalid_overview_1.zip");
    Set<Model> models = createValidTestMapModel();

    parser.parseOverviewLinks(models, imageEntries, null, new ZipFile("testFiles/invalid_overview_1.zip"));
  }

  @Test(expected = InvalidOverviewFile.class)
  public void testParsingInvalidFile2() throws Exception {
    List<ImageZipEntryFile> imageEntries = createImageEntries("testFiles/invalid_overview_2.zip");
    Set<Model> models = createValidTestMapModel();

    parser.parseOverviewLinks(models, imageEntries, null, new ZipFile("testFiles/invalid_overview_2.zip"));
  }

  @Test(expected = InvalidOverviewFile.class)
  public void testParsingInvalidFile3() throws Exception {
    List<ImageZipEntryFile> imageEntries = createImageEntries("testFiles/invalid_overview_3.zip");
    Set<Model> models = createValidTestMapModel();

    parser.parseOverviewLinks(models, imageEntries, null, new ZipFile("testFiles/invalid_overview_3.zip"));
  }

  private Set<Model> createValidTestMapModel() {
    Set<Model> result = new HashSet<>();
    Model model = new ModelFullIndexed(null);
    model.setName("main");
    result.add(model);
    return result;
  }

  /**
   * Test coordinates that overlap (exception is expected).
   * 
   * @throws Exception
   */
  @Test(expected = InvalidOverviewFile.class)
  public void testParseInvalidCoordinates() throws Exception {
    String invalidCoordinates = "test.png	10,10 100,10 100,100 10,10	main.xml	10,10	3\n" +
        "test.png	10,10 10,400 400,400 400,10	main.xml	10,10	4";
    Set<Model> models = createValidTestMapModel();

    List<OverviewImage> images = new ArrayList<OverviewImage>();
    OverviewImage oi = new OverviewImage();
    oi.setFilename("test.png");
    oi.setWidth(1000);
    oi.setHeight(1000);
    images.add(oi);

    parser.processCoordinates(models, images, invalidCoordinates);
  }

  @Test
  public void testParseValidCoordinates() throws Exception {
    String invalidCoordinates = "FILE	POLYGON	LINK_TARGET	MODEL_COORDINATES	MODEL_ZOOM_LEVEL	LINK_TYPE\n" +
        "test.png	10,10 100,10 100,100 10,10	main.xml	10,10	3	MODEL\n" +
        "test.png	200,200 200,400 400,400 400,200	main.xml	10,10	4	MODEL";
    Set<Model> models = createValidTestMapModel();

    List<OverviewImage> images = new ArrayList<OverviewImage>();
    OverviewImage oi = new OverviewImage();
    oi.setFilename("test.png");
    oi.setWidth(1000);
    oi.setHeight(1000);
    images.add(oi);

    parser.processCoordinates(models, images, invalidCoordinates);

    assertEquals(2, oi.getLinks().size());
  }

  @Test
  public void testParseValidComplexCoordinates() throws Exception {
    String invalidCoordinates = FileUtils.readFileToString(new File("testFiles/coordinates.txt"), "UTF-8");
    Set<Model> models = createValidTestMapModel();

    List<OverviewImage> images = new ArrayList<>();
    OverviewImage oi = new OverviewImage();
    oi.setFilename("test.png");
    oi.setWidth(1000);
    oi.setHeight(1000);
    images.add(oi);

    OverviewImage oi2 = new OverviewImage();
    oi2.setFilename("test2.png");
    oi2.setWidth(1000);
    oi2.setHeight(1000);
    images.add(oi2);

    parser.processCoordinates(models, images, invalidCoordinates);

    assertEquals(2, oi.getLinks().size());
    assertEquals(1, oi2.getLinks().size());
  }

}
