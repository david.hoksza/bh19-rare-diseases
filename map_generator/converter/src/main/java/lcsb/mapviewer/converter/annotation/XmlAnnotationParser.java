package lcsb.mapviewer.converter.annotation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.Author;

/**
 * This class allow to parse xml nodes responsible for species annotations in
 * CellDesigner.
 * 
 * @author Piotr Gawron
 * 
 */
public class XmlAnnotationParser {

  /**
   * Default logger.
   */
  private static Logger logger = LogManager.getLogger(XmlAnnotationParser.class.getName());

  private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  /**
   * Default constructor.
   */
  public XmlAnnotationParser() {
  }

  /**
   * This method parse the xml string passed as an argument. All information
   * obtained by the method are stored in local variables: miriamDataSet,
   * speciesId and can be accessed later on.
   * 
   * @param data
   *          - xml string to be parsed
   * 
   * @return collection of miriam objects that were obtained from the xml node
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  public Set<MiriamData> parse(String data) throws InvalidXmlSchemaException {
    // start from creating a DOM parser and parse the whole document
    Document doc = XmlParser.getXmlDocumentFromString(data);

    NodeList root = doc.getChildNodes();

    // process the whole schema
    return parseRdfNode(root);
  }

  /**
   * Retrieves set of Miriam annotation from xml rdf node.
   * 
   * @param root
   *          xml node list
   * @return set of miriam annotations.
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  private Set<MiriamData> parseRdfNode(NodeList root) throws InvalidXmlSchemaException {
    Node rdf = XmlParser.getNode("rdf:RDF", root);
    return parseRdfNode(rdf);
  }

  /**
   * Retrieves set of Miriam annotation from xml rdf node.
   * 
   * @param rdf
   *          xml node
   * @return set of miriam annotations.
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  public Set<MiriamData> parseRdfNode(Node rdf) throws InvalidXmlSchemaException {
    Set<MiriamData> miriamDataSet = new HashSet<>();
    if (rdf != null) {
      Node description = XmlParser.getNode("rdf:Description", rdf.getChildNodes());
      if (description != null) {
        NodeList list = description.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            String relationTypeString = node.getNodeName();
            MiriamRelationType relationType = MiriamRelationType.getTypeByStringRepresentation(relationTypeString);
            if (relationType != null) {
              miriamDataSet.addAll(parseMiriamNode(node));
            } else if (relationTypeString.equals("dc:creator")) {
            } else if (relationTypeString.equals("dcterms:created")) {
            } else if (relationTypeString.equals("dcterms:modified")) {
            } else {
              logger.warn("RDF relation type is not supported: " + relationTypeString);
            }
          }
        }
      }
    } else {
      throw new InvalidXmlSchemaException("rdf:Rdf node not found");
    }
    return miriamDataSet;
  }

  public List<Author> getAuthorsFromRdf(Node rdf) throws InvalidXmlSchemaException {
    List<Author> authors = new ArrayList<>();

    if (rdf != null) {
      Node description = XmlParser.getNode("rdf:Description", rdf.getChildNodes());
      if (description != null) {
        NodeList list = description.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            String relationTypeString = node.getNodeName();
            if (relationTypeString.equals("dc:creator")) {
              Node bag = XmlParser.getNode("rdf:Bag", node.getChildNodes());
              if (bag == null) {
                throw new InvalidXmlSchemaException("No rdf:Bag node found");
              }
              list = bag.getChildNodes();
              List<Node> nodes = XmlParser.getNodes("rdf:li", list);
              for (Node li : nodes) {
                Author author = parseAuthor(li);
                authors.add(author);
              }
            }
          }
        }
      }
    } else {
      throw new InvalidXmlSchemaException("rdf:Rdf node not found");
    }
    return authors;
  }

  public Calendar getCreateDateFromRdf(Node rdf) throws InvalidXmlSchemaException {
    if (rdf != null) {
      Node description = XmlParser.getNode("rdf:Description", rdf.getChildNodes());
      if (description != null) {
        NodeList list = description.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            String relationTypeString = node.getNodeName();
            if (relationTypeString.equals("dcterms:created")) {
              Node dateNode = XmlParser.getNode("dcterms:W3CDTF", node);
              if (dateNode != null) {
                return getDateFromNode(dateNode);
              }
            }
          }
        }
      }
    } else {
      throw new InvalidXmlSchemaException("rdf:Rdf node not found");
    }
    return null;
  }

  private Calendar getDateFromNode(Node dateNode) {
    try {
      Date date = dateFormat.parse(dateNode.getTextContent().replaceAll("[T]", " "));
      Calendar result = Calendar.getInstance();
      result.setTime(date);
      return result;
    } catch (ParseException | DOMException e) {
      logger.warn("Problem with parsing date: ", e);
    }
    return null;
  }

  private Author parseAuthor(Node li) {
    NodeList list = li.getChildNodes();
    String firstName = null;
    String lastName = null;
    String organization = null;
    String email = null;
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("vCard:EMAIL")) {
          email = node.getTextContent().trim();
        } else if (node.getNodeName().equals("vCard:ORG")) {
          organization = node.getTextContent().trim();
        } else if (node.getNodeName().equals("vCard:N")) {
          Node familyNode = XmlParser.getNode("vCard:Family", node);
          if (familyNode != null) {
            lastName = familyNode.getTextContent().trim();
          }
          Node givenNode = XmlParser.getNode("vCard:Given", node);
          if (givenNode != null) {
            firstName = givenNode.getTextContent().trim();
          }
        } else {
          logger.warn("Unknown data node in vcard: " + node.getNodeName());
        }
      }
    }
    Author author = new Author(firstName, lastName);
    author.setEmail(email);
    author.setOrganisation(organization);
    return author;
  }

  /**
   * This method converts a xml node into MiriamData object (annotation of a
   * species).
   * 
   * @param node
   *          - xml node that contains representation of a single annotation of a
   *          species
   * @return MiriamData object that represents annotation of a species
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  public Set<MiriamData> parseMiriamNode(Node node) throws InvalidXmlSchemaException {
    Set<MiriamData> result = new HashSet<>();
    NodeList list = node.getChildNodes();
    String relationTypeString = node.getNodeName();
    MiriamRelationType relationType = MiriamRelationType.getTypeByStringRepresentation(relationTypeString);
    Node bag = XmlParser.getNode("rdf:Bag", list);
    if (bag == null) {
      throw new InvalidXmlSchemaException("No rdf:Bag node found");
    }
    list = bag.getChildNodes();
    List<Node> nodes = XmlParser.getNodes("rdf:li", list);
    for (Node li : nodes) {
      String dataTypeUri = XmlParser.getNodeAttr("rdf:resource", li);
      if (dataTypeUri == null || dataTypeUri.isEmpty()) {
        throw new InvalidXmlSchemaException("rdf:li does not have a rdf:resource attribute");
      }

      try {
        MiriamData md = MiriamType.getMiriamByUri(dataTypeUri);
        if (relationType == null) {
          logger.warn("Unknown relation type: " + relationTypeString + ". For miriam uri: " + dataTypeUri + ".");
        } else {
          if (!MiriamType.PUBMED.equals(md.getDataType())) {
            md.setRelationType(relationType);
          }
          result.add(md);
        }
      } catch (InvalidArgumentException e) {
        logger.warn(e.getMessage());
      }
    }
    return result;
  }

  /**
   * This method converts a set of MiriamData into xml string that can be put to
   * CellDesigner schema.
   * 
   * @param data
   *          - a set of MiriamData to be converted.
   * @return xml string representation of the input data
   */
  public String dataSetToXmlString(Collection<MiriamData> data, String metaid) {
    return dataSetToXmlString(data, new ArrayList<>(), null, new ArrayList<>(), metaid);
  }

  /**
   * This method converts a set of MiriamData into xml string that can be put to
   * CellDesigner schema.
   * 
   * @param data
   *          - a set of MiriamData to be converted.
   * @return xml string representation of the input data
   */
  public String dataSetToXmlString(Collection<MiriamData> data, Collection<Author> authors, Calendar createDate,
      List<Calendar> modificationDates, String metaid) {
    StringBuilder result = new StringBuilder("");
    result.append(
        "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" "
            + "xmlns:dc=\"http://purl.org/dc/elements/1.1/\" "
            + "xmlns:dcterms=\"http://purl.org/dc/terms/\" " + "xmlns:vCard=\"http://www.w3.org/2001/vcard-rdf/3.0#\" "
            + "xmlns:bqbiol=\"http://biomodels.net/biology-qualifiers/\" "
            + "xmlns:bqmodel=\"http://biomodels.net/model-qualifiers/\">\n");
    result.append("<rdf:Description rdf:about=\"#" + metaid + "\">\n");
    for (MiriamData miriamData : data) {
      result.append(miriamDataToXmlString(miriamData));
    }
    for (Author author : authors) {
      result.append(authorToXmlString(author));
    }
    if (createDate != null) {
      result.append("<dcterms:created rdf:parseType=\"Resource\">");
      result.append(dateToXmlString(createDate));
      result.append("</dcterms:created>\n");
    }
    result.append("<dcterms:modified rdf:parseType=\"Resource\">");
    for (Calendar modificationDate : modificationDates) {
      result.append(dateToXmlString(modificationDate));
    }
    result.append("</dcterms:modified>\n");
    result.append("</rdf:Description>\n");
    result.append("</rdf:RDF>\n");
    return result.toString();
  }

  private Object dateToXmlString(Calendar createDate) {
    StringBuilder result = new StringBuilder("");
    result.append("<dcterms:W3CDTF>");
    result.append(dateFormat.format(createDate.getTime()).replaceAll(" ", "T") + "Z");
    result.append("</dcterms:W3CDTF>\n");
    return result.toString();
  }

  private String authorToXmlString(Author author) {
    StringBuilder result = new StringBuilder("");
    result.append("<dc:creator>\n");
    result.append("<rdf:Bag>\n");
    result.append("<rdf:li rdf:parseType=\"Resource\">\n");
    result.append("<vCard:N rdf:parseType=\"Resource\">\n");
    if (author.getLastName() != null) {
      result.append("<vCard:Family>" + XmlParser.escapeXml(author.getLastName()) + "</vCard:Family>\n");
    }
    if (author.getFirstName() != null) {
      result.append("<vCard:Given>" + XmlParser.escapeXml(author.getFirstName()) + "</vCard:Given>\n");
    }
    result.append("</vCard:N>\n");

    if (author.getEmail() != null && !author.getEmail().trim().isEmpty()) {
      result.append("<vCard:EMAIL>" + XmlParser.escapeXml(author.getEmail()) + "</vCard:EMAIL>\n");
    }
    if (author.getOrganisation() != null && !author.getOrganisation().trim().isEmpty()) {
      result.append("<vCard:ORG rdf:parseType=\"Resource\">\n");
      result.append("<vCard:Orgname>" + XmlParser.escapeXml(author.getOrganisation()) + "</vCard:Orgname>\n");
      result.append("</vCard:ORG>\n");
    }

    result.append("</rdf:li>\n");
    result.append("</rdf:Bag>\n");
    result.append("</dc:creator>\n");
    return result.toString();
  }

  /**
   * This method converts a single MiriamData into xml string that can be put to
   * CellDesigner schema.
   * 
   * @param data
   *          - a MiriamData to be converted.
   * @return xml string representation of the input data
   */
  public String miriamDataToXmlString(MiriamData data) {
    StringBuilder result = new StringBuilder("");
    result.append("<" + data.getRelationType().getStringRepresentation() + ">\n");
    result.append("<rdf:Bag>\n");
    result.append("<rdf:li rdf:resource=\"" + data.getDataType().getUris().get(0) + ":"
        + data.getResource().replaceAll(":", "%3A") + "\"/>\n");
    result.append("</rdf:Bag>\n");
    result.append("</" + data.getRelationType().getStringRepresentation() + ">\n");

    return result.toString();
  }

  public List<Calendar> getModificationDatesFromRdf(Node rdf) throws InvalidXmlSchemaException {
    List<Calendar> result = new ArrayList<>();

    if (rdf != null) {
      Node description = XmlParser.getNode("rdf:Description", rdf.getChildNodes());
      if (description != null) {
        NodeList list = description.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            String relationTypeString = node.getNodeName();
            if (relationTypeString.equals("dcterms:modified")) {
              for (Node dateNode : XmlParser.getNodes("dcterms:W3CDTF", node.getChildNodes())) {
                result.add(getDateFromNode(dateNode));
              }
            }
          }
        }
      } else {
        throw new InvalidXmlSchemaException("rdf:Description node not found");
      }
    } else {
      throw new InvalidXmlSchemaException("rdf:Rdf node not found");
    }

    return result;
  }

}
