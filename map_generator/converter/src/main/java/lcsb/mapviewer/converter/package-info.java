/**
 * Provides generic interface used for reading data and transforming it into
 * {@link lcsb.mapviewer.model.map.model.Model Model} structure.
 * 
 */
package lcsb.mapviewer.converter;
