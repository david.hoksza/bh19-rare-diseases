package lcsb.mapviewer.converter;

import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.zip.*;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.graphics.*;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.species.Element;

public class ProjectFactory {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ProjectFactory.class);

  private ComplexZipConverter converter;

  private GlyphParser glyphParser = new GlyphParser();

  public ProjectFactory(ComplexZipConverter converter) {
    this.converter = converter;
  }

  public Project create(ComplexZipConverterParams params) throws InvalidInputDataExecption, ConverterException {
    return create(params, new Project());
  }

  public Project create(ComplexZipConverterParams params, Project project)
      throws InvalidInputDataExecption, ConverterException {
    try {
      Model model = converter.createModel(params);

      Set<Model> models = new HashSet<>();
      models.add(model);
      models.addAll(model.getSubmodels());

      project.addModel(model);

      ZipFile zipFile = params.getZipFile();
      Enumeration<? extends ZipEntry> entries;

      entries = zipFile.entries();
      List<ImageZipEntryFile> imageEntries = new ArrayList<>();
      int overlayOrder = 1;
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        if (!entry.isDirectory()) {
          ZipEntryFile zef = params.getEntry(entry.getName());
          if (zef instanceof ImageZipEntryFile) {
            imageEntries.add((ImageZipEntryFile) zef);
          } else if (zef instanceof GlyphZipEntryFile) {
            project.addGlyph(glyphParser.parseGlyph((GlyphZipEntryFile) zef, zipFile));
          } else if (zef instanceof LayoutZipEntryFile) {
            project.addLayout(
                converter.layoutZipEntryFileToLayout(params, zipFile, entry, (LayoutZipEntryFile) zef, overlayOrder++));
          }
        }
      }
      if (imageEntries.size() > 0) {
        OverviewParser parser = new OverviewParser();
        project
            .addOverviewImages(parser.parseOverviewLinks(models, imageEntries, params.getVisualizationDir(), zipFile));
      }
      if (project.getGlyphs().size() > 0) {
        assignGlyphsToElements(project);
      }
      return project;
    } catch (IOException e) {
      throw new InvalidArgumentException(e);
    } catch (InvalidCoordinatesFile e) {
      throw new InvalidInputDataExecption("Invalid coordinates file. " + e.getMessage(), e);
    }
  }

  private void assignGlyphsToElements(Project project) throws InvalidGlyphFile {
    Set<ModelData> models = new HashSet<>();
    models.addAll(project.getModels());

    for (ModelData model : project.getModels()) {
      for (ModelSubmodelConnection connection : model.getSubmodels()) {
        models.add(connection.getSubmodel());
      }
    }
    for (ModelData model : models) {
      for (Element element : model.getElements()) {
        Glyph glyph = extractGlyph(project, element.getNotes());
        if (glyph != null) {
          element.setNotes(removeGlyph(element.getNotes()));
          element.setGlyph(glyph);
        }
      }
      for (Layer layer : model.getLayers()) {
        for (LayerText text : layer.getTexts()) {
          Glyph glyph = extractGlyph(project, text.getNotes());
          if (glyph != null) {
            text.setNotes(removeGlyph(text.getNotes()));
            text.setGlyph(glyph);
          }
        }
      }
    }
  }

  String removeGlyph(String notes) {
    String lines[] = notes.split("[\n\r]+");
    StringBuilder result = new StringBuilder("");
    for (String line : lines) {
      if (!line.startsWith("Glyph:")) {
        result.append(line + "\n");
      }
    }
    return result.toString();
  }

  Glyph extractGlyph(Project project, String notes) throws InvalidGlyphFile {
    String lines[] = notes.split("[\n\r]+");
    for (String line : lines) {
      if (line.startsWith("Glyph:")) {
        String glyphString = line.replace("Glyph:", "").trim().toLowerCase();
        for (Glyph glyph : project.getGlyphs()) {
          if (glyph.getFile().getOriginalFileName().toLowerCase().equalsIgnoreCase(glyphString)) {
            return glyph;
          }
        }
        throw new InvalidGlyphFile("Glyph file doesn't exist: " + glyphString);
      }
    }
    return null;
  }

}
