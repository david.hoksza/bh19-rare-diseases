package lcsb.mapviewer.converter;

/**
 * Exception that should be thrown when there is a problem with input data.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidInputDataExecption extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with exception message.
   * 
   * @param message
   *          error message
   */
  public InvalidInputDataExecption(String message) {
    super(message);
  }

  /**
   * Default constructor with super exception as a source.
   * 
   * @param e
   *          super exception
   */
  public InvalidInputDataExecption(Exception e) {
    super(e);
  }

  /**
   * Default constructor - initializes instance variable to unknown.
   */

  public InvalidInputDataExecption() {
    super(); // call superclass constructor
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          exception message
   * @param e
   *          super exception
   */
  public InvalidInputDataExecption(String message, Exception e) {
    super(message, e);
  }

  /**
   * Default constructor with exception message.
   * 
   * @param message
   *          error message
   * @param filename
   *          name of the file where data is invalid
   */
  public InvalidInputDataExecption(String message, String filename) {
    super("[File \"" + filename + "\"]: " + message.trim());
  }

}
