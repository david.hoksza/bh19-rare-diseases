package lcsb.mapviewer.converter;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;

/**
 * This util class populate with z-index data if necessary.
 * 
 * @author Piotr Gawron
 *
 */
public class ZIndexPopulator {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(ZIndexPopulator.class);

  private DoubleComparator doubleComparator = new DoubleComparator(Configuration.EPSILON);
  private StringComparator stringComparator = new StringComparator();

  /**
   * Populate all elements that don't have z-index with valid values. Elements
   * without z-index would have bigger z-values than elements with existing
   * z-values.
   * 
   * @param model
   */
  public void populateZIndex(Model model) {
    int maxZIndex = 0;

    List<Drawable> bioEntities = new ArrayList<>();
    for (Layer layer : model.getLayers()) {
      bioEntities.addAll(layer.getOvals());
      bioEntities.addAll(layer.getRectangles());
      bioEntities.addAll(layer.getTexts());
    }
    bioEntities.addAll(model.getBioEntities());
    bioEntities.sort(new Comparator<Drawable>() {
      @Override
      public int compare(Drawable o1, Drawable o2) {
        if (o1 instanceof Reaction) {
          if (o2 instanceof Reaction) {
            return stringComparator.compare(((Reaction) o1).getElementId(), ((Reaction) o2).getElementId());
          }
          return -1;
        }
        if (o2 instanceof Reaction) {
          return 1;
        }
        if (o1 instanceof Element && o2 instanceof Element) {
          Element e1 = (Element) o1;
          Element e2 = (Element) o2;

          if (isChildren(e1, e2)) {
            return -1;
          }
          if (isChildren(e2, e1)) {
            return 1;
          }
          int result = -doubleComparator.compare(o1.getSize(), o2.getSize());
          if (result == 0) {
            return stringComparator.compare(e1.getElementId(), e2.getElementId());
          }
        }

        return -doubleComparator.compare(o1.getSize(), o2.getSize());
      }

      private boolean isChildren(Element e1, Element e2) {
        if (e1 instanceof Complex && e2 instanceof Species) {
          Complex parent = (Complex) e1;
          Species child = (Species) e2;
          return parent.getAllSimpleChildren().contains(child);
        }
        if (e1 instanceof Compartment) {
          Compartment parent = (Compartment) e1;
          return parent.getAllSubElements().contains(e2);
        }
        return false;
      }
    });

    for (Drawable bioEntity : bioEntities) {
      if (bioEntity.getZ() == null) {
        bioEntity.setZ(++maxZIndex);
      }
    }

  }
}
