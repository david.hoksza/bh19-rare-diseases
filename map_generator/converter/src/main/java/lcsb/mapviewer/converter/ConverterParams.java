package lcsb.mapviewer.converter;

import java.io.*;

import org.apache.xerces.xni.parser.XMLInputSource;

/**
 * Class with params for {@link Converter} interface.
 * 
 * @author Piotr Gawron
 */
public class ConverterParams {
  /**
   * Input stream for the parser.
   */
  private InputStream inputStream;

  /**
   * Name of the file used as a data input. (not obligatory)
   */
  private String filename;
  /**
   * Should the map be autoresized after parsing or not. Autoresizing will equal
   * right with left margin and top with bottom margin.
   */
  private boolean sizeAutoAdjust = true;

  /**
   * Should the map be formated as SBGN.
   */
  private boolean sbgnFormat = false;

  /**
   * Set file as input source.
   * 
   * @param fileName
   *          path to a file that should be parsed.
   * @return object with all set parameters
   * @throws FileNotFoundException
   */
  public ConverterParams filename(String fileName) throws FileNotFoundException {
    inputStream = new FileInputStream(fileName);
    this.filename = fileName;
    return this;
  }

  /**
   * 
   * Set input stream as input source.
   * 
   * @param is
   *          input stream that should be parsed
   * @return object with all set parameters
   */
  public ConverterParams inputStream(InputStream is) {
    return this.inputStream(is, null);
  }

  public ConverterParams inputStream(InputStream is, String filename) {
    this.inputStream = is;
    if (filename != null) {
      this.filename = filename;
    }
    return this;
  }

  /**
   * @return the source
   * @see #source
   */
  public XMLInputSource getSource() {
    return new XMLInputSource(null, this.filename, null, this.inputStream, null);
  }

  /**
   * 
   * @param value
   *          the sizeAutoAdjust to set
   * @see #sizeAutoAdjust
   * @return object with all parameters
   */
  public ConverterParams sizeAutoAdjust(boolean value) {
    sizeAutoAdjust = value;
    return this;
  }

  /**
   * @return the inputStream
   * @see #inputStream
   */
  public InputStream getInputStream() {
    return this.inputStream;
  }

  /**
   * @return the sizeAutoAdjust
   * @see #sizeAutoAdjust
   */
  public boolean isSizeAutoAdjust() {
    return sizeAutoAdjust;
  }

  /**
   * @return the filename
   * @see #filename
   */
  public String getFilename() {
    return filename;
  }

  /**
   * @return the sbgnFormat
   * @see #sbgnFormat
   */
  public boolean isSbgnFormat() {
    return sbgnFormat;
  }

  /**
   * @param sbgnFormat
   *          the sbgnFormat to set
   * @return ConverterParams object with all parameters
   * @see #sbgnFormat
   */
  public ConverterParams sbgnFormat(boolean sbgnFormat) {
    this.sbgnFormat = sbgnFormat;
    return this;
  }

}
