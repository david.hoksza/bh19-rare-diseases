package lcsb.mapviewer.converter.zip;

import java.io.Serializable;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.model.SubmodelType;

/**
 * Structure used to describe a file in a zip archive with single entry about
 * {@link lcsb.mapviewer.model.map.model.Model model} or submodel.
 * 
 * @author Piotr Gawron
 * 
 */
public class ModelZipEntryFile extends ZipEntryFile implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Name that should be used for the file.
   */
  private String name;

  /**
   * Is the file root (top) model in the project.
   */
  private String root = "false";

  /**
   * Is the file a mapping file.
   */
  private String mapping = "false";

  /**
   * What kind of submodel it is.
   */
  private SubmodelType type = SubmodelType.UNKNOWN;

  /**
   * Type of the input file. For now we accept only CellDesigner files, but in
   * future it will change.
   */
  private InputFileType fileType = InputFileType.UNKNOWN;

  /**
   * Default constructor.
   */
  public ModelZipEntryFile() {

  }

  /**
   * Constructor that initializes all params.
   * 
   * @param filename
   *          {@link #filename}
   * @param name
   *          {@link #name}
   * @param root
   *          {@link #root}
   * @param mapping
   *          {@link #mapping}
   * @param type
   *          {@link #type}
   */
  public ModelZipEntryFile(String filename, String name, boolean root, boolean mapping, SubmodelType type) {
    setFilename(filename);
    if (name == null && !mapping) {
      throw new InvalidArgumentException("Name cannot be null for submap");
    }
    this.name = name;
    this.root = root + "";
    this.mapping = mapping + "";
    this.type = type;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the type
   * @see #type
   */
  public SubmodelType getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(SubmodelType type) {
    this.type = type;
  }

  /**
   * @return the root
   * @see #root
   */
  public String getRoot() {
    return root;
  }

  /**
   * @return the mapping
   * @see #mapping
   */
  public String getMapping() {
    return mapping;
  }

  /**
   * @param mapping
   *          the mapping to set
   * @see #mapping
   */
  public void setMapping(String mapping) {
    this.mapping = mapping;
  }

  /**
   * @return the fileType
   * @see #fileType
   */
  public InputFileType getFileType() {
    return fileType;
  }

  /**
   * @param fileType
   *          the fileType to set
   * @see #fileType
   */
  public void setFileType(InputFileType fileType) {
    this.fileType = fileType;
  }

  /**
   * Returns <code>true</code> if {@link #root} is positive, <code>false</code>
   * otherwise.
   *
   * @return <code>true</code> if {@link #root} is positive, <code>false</code>
   *         otherwise
   */
  public boolean isRoot() {
    return "true".equalsIgnoreCase(root);
  }

  /**
   * @param root
   *          the root to set
   * @see #root
   */
  public void setRoot(String root) {
    this.root = root;
  }

  /**
   * Sets {@link #root} vale.
   *
   * @param root
   *          new {@link #root} value
   */
  public void setRoot(boolean root) {
    if (root) {
      setRoot("true");
    } else {
      setRoot("false");
    }
  }

  /**
   * Returns <code>true</code> if {@link #mapping} is positive, <code>false</code>
   * otherwise.
   * 
   * @return <code>true</code> if {@link #mapping} is positive, <code>false</code>
   *         otherwise
   */
  public boolean isMappingFile() {
    return "true".equalsIgnoreCase(mapping);
  }

}
