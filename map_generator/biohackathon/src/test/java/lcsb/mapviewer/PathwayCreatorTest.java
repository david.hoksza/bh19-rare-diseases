package lcsb.mapviewer;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.project05.data.Pathway;

public class PathwayCreatorTest {

  Logger logger = LogManager.getLogger();

  @Test
  public void test() throws InvalidInputDataExecption {
    Pathway pathway = new PathwayCreator().createPathway("data/brugada_output_file.tsv", new HashSet<>());
    assertNotNull(pathway);
    InputStream stream = new ByteArrayInputStream(pathway.content.getBytes(StandardCharsets.UTF_8));
    Model model = new SbmlParser().createModel(new ConverterParams().inputStream(stream));
    assertTrue(model.getElements().size() > 0);
    assertTrue(model.getReactions().size() > 0);
  }

  @Test
  public void test2() throws InvalidInputDataExecption {
    GenericProtein protein = new GenericProtein("x");
    protein.setName("PKP2");
    Set<Element> existingElements = new HashSet<>();
    existingElements.add(protein);
    Pathway pathway = new PathwayCreator().createPathway("data/brugada_output_file.tsv", existingElements);
    assertNotNull(pathway);
    InputStream stream = new ByteArrayInputStream(pathway.content.getBytes(StandardCharsets.UTF_8));
    Model model = new SbmlParser().createModel(new ConverterParams().inputStream(stream));
    assertTrue(model.getElements().size() > 0);
    assertTrue(model.getReactions().size() > 0);

    for (Element element : model.getElements()) {
      assertFalse(element.getName().equals("PKP2"));
    }
  }

}
