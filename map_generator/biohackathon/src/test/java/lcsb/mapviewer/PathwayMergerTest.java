package lcsb.mapviewer;

import static org.junit.Assert.assertNotNull;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.project05.data.Pathway;
import lcsb.mapviewer.project05.data.PathwaySource;

public class PathwayMergerTest {

  Logger logger = LogManager.getLogger();

  @Test
  public void testMerge() throws Exception {
    PathwayMerger merger = new PathwayMerger();
    PathwayFetcher fetcher = new PathwayFetcher();
    List<Pathway> pathways = new ArrayList<>();
    pathways.add(fetcher.fetchPath("WP706", PathwaySource.WIKIPATHWAYS));
    pathways.add(fetcher.fetchPath("WP2118", PathwaySource.WIKIPATHWAYS));
    pathways.add(fetcher.fetchPath("WP382", PathwaySource.WIKIPATHWAYS));
    pathways.add(fetcher.fetchPath("WP2911", PathwaySource.WIKIPATHWAYS));
    pathways.add(fetcher.fetchPath("WP383", PathwaySource.WIKIPATHWAYS));
    Pathway result = merger.mergePathways(pathways);
    assertNotNull(result.content);
    PrintWriter writer = new PrintWriter("test.xml", "UTF-8");
    writer.println(result.content);
    writer.close();
  }

}
