package lcsb.mapviewer;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PathwayCreatorTest.class,
    PathwayFetcherTest.class, PathwayMergerTest.class })
public class AllTests {

}
