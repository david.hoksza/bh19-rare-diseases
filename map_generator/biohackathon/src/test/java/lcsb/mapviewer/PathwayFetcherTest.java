package lcsb.mapviewer;

import static org.junit.Assert.assertNotNull;

import java.io.*;
import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;

import lcsb.mapviewer.common.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.project05.data.Pathway;
import lcsb.mapviewer.project05.data.PathwaySource;
import lcsb.mapviewer.wikipathway.GpmlParser;

public class PathwayFetcherTest {

  Logger logger = LogManager.getLogger();

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  @Test
  public void testFetchMinerva() throws IOException {
    Pathway pathway = new PathwayFetcher().fetchPath(
        "https://pdmap.uni.lu/minerva/api/\tpd_map_autumn_19\t4895\tPostsynaptic terminal\t1240.750\t5976.583\t4263.417\t500.000",
        PathwaySource.MINERVA);
    assertNotNull(pathway.content);
  }

  @Test
  public void fetchAllWikipathwayPathways() throws IOException {
    PathwayFetcher fetcher = new PathwayFetcher();
    String content = fetcher.fetchFile("http://webservice.wikipathways.org/listPathways");
    for (String line : content.split("\n")) {
      if (line.contains("<ns2:id>WP")) {
        String id = line.substring(line.indexOf(">"), line.indexOf("<", line.indexOf(">")));
        logger.info("CHECK: " + id);
        Pathway pathway = new PathwayFetcher().fetchPath(id, PathwaySource.WIKIPATHWAYS);
        InputStream stream = new ByteArrayInputStream(pathway.content.getBytes(StandardCharsets.UTF_8));
        try {
          new GpmlParser().createModel(new ConverterParams().inputStream(stream));
        } catch (Exception e) {
          logger.warn("FAIL " + id, e);
        }
      }
    }
  }

}
