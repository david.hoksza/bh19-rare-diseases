package lcsb.mapviewer.project05.data;

public enum PathwaySource {
  WIKIPATHWAYS, MINERVA, TEXT_MINING
}
