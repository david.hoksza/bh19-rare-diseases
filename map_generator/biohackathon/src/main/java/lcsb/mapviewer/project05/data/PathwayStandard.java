package lcsb.mapviewer.project05.data;

public enum PathwayStandard {
  SBML,
  GPML;
}
