package lcsb.mapviewer.project05.data;

public class Pathway {
  public String content;
  public String id;
  public PathwaySource type;
  public PathwayStandard format;

  public Pathway(PathwaySource source, String id, String fetchWikipathway, PathwayStandard format) {
    this.id = id;
    this.content = fetchWikipathway;
    this.type = source;
    this.format = format;
  }
}
