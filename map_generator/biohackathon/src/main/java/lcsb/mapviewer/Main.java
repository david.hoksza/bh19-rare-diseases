package lcsb.mapviewer;

import java.io.ByteArrayInputStream;
import java.util.*;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.layout.ApplySimpleLayoutModelCommand;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.project05.data.Pathway;

public class Main {
  static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {

    Options options = new Options();
    Option enricherOption = new Option(null, "enrichr-file", true, "Enrichr output for wikipathway");
    Option minervaOption = new Option(null, "minerva-file", true, "Minerva pathways");
    Option outputOption = new Option(null, "output-file", true, "Output map in sbml format");
    Option textMiningOption = new Option(null, "text-mining-file", true, "Text mining data");
    enricherOption.setRequired(true);
    minervaOption.setRequired(true);
    outputOption.setRequired(true);

    options.addOption(enricherOption)
        .addOption(minervaOption)
        .addOption(outputOption)
        .addOption(textMiningOption);

    HelpFormatter formatter = new HelpFormatter();

    CommandLineParser parser = new DefaultParser();
    try {
      // parse the command line arguments
      CommandLine line = parser.parse(options, args);
      String wikipathways = line.getOptionValue("enrichr-file");
      String minervaPathways = line.getOptionValue("minerva-file");
      String network = line.getOptionValue("text-mining-file");
      String output = line.getOptionValue("output-file");

      PathwayMerger merger = new PathwayMerger();
      PathwayFetcher fetcher = new PathwayFetcher();
      PathwayCreator creator = new PathwayCreator();
      List<Pathway> pathways = new ArrayList<>();
      pathways.addAll(fetcher.fetchWikipathwayPathways(wikipathways));
      pathways.addAll(fetcher.fetchMinervaPathways(minervaPathways));
      Pathway tmp = merger.mergePathways(pathways);

      ByteArrayInputStream is = new ByteArrayInputStream(tmp.content.getBytes("UTF-8"));
      Model modelWithoutTextMining = new SbmlParser().createModel(new ConverterParams().inputStream(is));

      Model resultMap = modelWithoutTextMining;
      if (network != null) {
        pathways.add(findPositionForTextMining(pathways),
            creator.createPathway(network, modelWithoutTextMining.getElements()));

        Pathway result = merger.mergePathways(pathways);

        is = new ByteArrayInputStream(result.content.getBytes("UTF-8"));
        Model modelWithTextMining = new SbmlParser().createModel(new ConverterParams().inputStream(is));

        Collection<BioEntity> reactions = creator.addMissingReactions(modelWithTextMining,
            creator.getElementNames(modelWithoutTextMining.getElements()), network);

        new ApplySimpleLayoutModelCommand(modelWithTextMining, reactions).execute();
        resultMap = modelWithTextMining;
      }
      new SbmlParser().model2File(resultMap, output);
      logger.info("Size of the model: " + resultMap.getWidth() + "; " + resultMap.getHeight());
    } catch (ParseException exp) {
      System.err.println("Parsing with input parameters: " + exp.getMessage());
      formatter.printHelp("map_generator", options);
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(-1);
    }
  }

  private static int findPositionForTextMining(List<Pathway> pathways) {
    int count = pathways.size() + 1;
    int rowSize = (int) Math.sqrt(count);
    return rowSize * (rowSize / 2) + rowSize / 2;
  }

}
