package lcsb.mapviewer;

import java.io.*;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.layout.ApplySimpleLayoutModelCommand;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.project05.data.*;

public class PathwayCreator {

  Logger logger = LogManager.getLogger();
  private int counter = 0;

  public Pathway createPathway(String networkFileName, Set<Element> existingElements) {
    try {
      Map<String, Element> elements = parseElements(networkFileName, existingElements);
      List<Reaction> reactions = createReactions(networkFileName, elements);

      Model result = new ModelFullIndexed(null);
      result.addElements(elements.values());
      result.addReactions(reactions);
      
      new ApplySimpleLayoutModelCommand(result).execute();

      return new Pathway(PathwaySource.TEXT_MINING, null, new SbmlParser().model2String(result), PathwayStandard.SBML);
    } catch (Exception e) {
      throw new RuntimeException("Problem with pathway", e);
    }
  }

  private List<Reaction> createReactions(String networkFileName, Map<String, Element> elements) throws IOException {
    List<Reaction> result = new ArrayList<>();
    try (BufferedReader br = new BufferedReader(new FileReader(networkFileName))) {
      String line;
      int sourceHgncSymbolCol = -1;
      int targetHgncSymbolCol = -1;
      int isDirected = -1;
      int references = -1;
      while ((line = br.readLine()) != null) {
        String cols[] = line.split("\t", -1);
        if (line.indexOf("source_hgnc_symbol") >= 0) {
          for (int i = 0; i < cols.length; i++) {
            if (cols[i].equalsIgnoreCase("source_hgnc_symbol")) {
              sourceHgncSymbolCol = i;
            } else if (cols[i].equalsIgnoreCase("target_hgnc_symbol")) {
              targetHgncSymbolCol = i;
            } else if (cols[i].equalsIgnoreCase("is_directed")) {
              isDirected = i;
            } else if (cols[i].equalsIgnoreCase("references")) {
              references = i;
            }
          }
          continue;
        }
        if (cols[references].equalsIgnoreCase("NA")) { // skip reaction without references
          continue;
        }
        if (cols[isDirected].equalsIgnoreCase("0")) { // skip not directed
          continue;
        }

        Reaction reaction = new StateTransitionReaction("re" + counter++);
        if (elements.get(cols[sourceHgncSymbolCol]) != null && elements.get(cols[targetHgncSymbolCol]) != null) {
          reaction.addReactant(new Reactant(elements.get(cols[sourceHgncSymbolCol])));
          reaction.addProduct(new Product(elements.get(cols[targetHgncSymbolCol])));
          String tmp[] = cols[references].split(",", -1);
          for (String string : tmp) {
            if (!string.equals("NA") && !string.isEmpty()) {
              reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, string));
            }
          }
          if (cols[isDirected].equals("0")) {
            reaction.setReversible(true);
          }
          result.add(reaction);
        }
      }
    }

    return result;
  }

  private Map<String, Element> parseElements(String elementsFileName, Set<Element> originalExistingElements)
      throws IOException {
    Set<String> existingElementNames = this.getElementNames(originalExistingElements);
    Map<String, Element> result = new HashMap<>();
    try (BufferedReader br = new BufferedReader(new FileReader(elementsFileName))) {
      String line;
      int targetEntrezIdCol = -1;
      int targetHgncSymbolCol = -1;
      int sourceEntrezIdCol = -1;
      int sourceHgncSymbolCol = -1;
      int isDirected = -1;
      int references = -1;
      while ((line = br.readLine()) != null) {
        String cols[] = line.split("\t", -1);
        if (line.indexOf("source_entrezgene_id") >= 0) {
          for (int i = 0; i < cols.length; i++) {
            if (cols[i].equalsIgnoreCase("source_entrezgene_id")) {
              sourceEntrezIdCol = i;
            } else if (cols[i].equalsIgnoreCase("source_hgnc_symbol")) {
              sourceHgncSymbolCol = i;
            } else if (cols[i].equalsIgnoreCase("target_entrezgene_id")) {
              targetEntrezIdCol = i;
            } else if (cols[i].equalsIgnoreCase("target_hgnc_symbol")) {
              targetHgncSymbolCol = i;
            } else if (cols[i].equalsIgnoreCase("is_directed")) {
              isDirected = i;
            } else if (cols[i].equalsIgnoreCase("references")) {
              references = i;
            }
          }
          continue;
        }
        if (cols[references].equalsIgnoreCase("NA")) { // skip reaction without references
          continue;
        }
        if (cols[isDirected].equalsIgnoreCase("0")) { // skip not directed
          continue;
        }

        GenericProtein protein = new GenericProtein("pr" + (counter++));
        protein.setName(cols[sourceHgncSymbolCol]);
        protein.addMiriamData(new MiriamData(MiriamType.ENTREZ, cols[sourceEntrezIdCol]));
        protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, cols[sourceHgncSymbolCol]));
        if (result.get(protein.getName()) == null) {
          boolean exists = existingElementNames.contains(protein.getName());
          if (!exists) {
            result.put(protein.getName(), protein);
          }
        }
        protein = new GenericProtein("pr" + (counter++));
        protein.setName(cols[targetHgncSymbolCol]);
        protein.addMiriamData(new MiriamData(MiriamType.ENTREZ, cols[targetEntrezIdCol]));
        protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, cols[targetHgncSymbolCol]));
        if (result.get(protein.getName()) == null) {
          boolean exists = existingElementNames.contains(protein.getName());
          if (!exists) {
            result.put(protein.getName(), protein);
          }
        }
      }
    }

    return result;
  }

  public Set<String> getElementNames(Set<Element> elements) {
    Set<String> result = new HashSet<>();
    for (Element element : elements) {
      if (element instanceof Species) {
        if (((Species) element).getComplex() == null) {
          result.add(element.getName());
        }
      }

    }
    return result;
  }

  Map<String, Element> elementsByName;

  public Collection<BioEntity> addMissingReactions(Model model, Set<String> elementNames, String networkFileName)
      throws IOException {
    List<BioEntity> result = new ArrayList<>();
    elementsByName = new HashMap<>();
    try (BufferedReader br = new BufferedReader(new FileReader(networkFileName))) {
      String line;
      int sourceHgncSymbolCol = -1;
      int targetHgncSymbolCol = -1;
      int isDirected = -1;
      int references = -1;
      while ((line = br.readLine()) != null) {
        String cols[] = line.split("\t", -1);
        if (line.indexOf("source_hgnc_symbol") >= 0) {
          for (int i = 0; i < cols.length; i++) {
            if (cols[i].equalsIgnoreCase("source_hgnc_symbol")) {
              sourceHgncSymbolCol = i;
            } else if (cols[i].equalsIgnoreCase("target_hgnc_symbol")) {
              targetHgncSymbolCol = i;
            } else if (cols[i].equalsIgnoreCase("is_directed")) {
              isDirected = i;
            } else if (cols[i].equalsIgnoreCase("references")) {
              references = i;
            }
          }
          continue;
        }
        if (cols[references].equalsIgnoreCase("NA")) { // skip reaction without references
          continue;
        }
        if (cols[isDirected].equalsIgnoreCase("0")) { // skip not directed
          continue;
        }

        Reaction reaction = new StateTransitionReaction("re_added" + counter++);
        if (elementNames.contains(cols[sourceHgncSymbolCol]) || elementNames.contains(cols[targetHgncSymbolCol])) {
          reaction.addReactant(new Reactant(findElementByName(model, cols[sourceHgncSymbolCol])));
          reaction.addProduct(new Product(findElementByName(model, cols[targetHgncSymbolCol])));
          String tmp[] = cols[references].split(",", -1);
          for (String string : tmp) {
            if (!string.equals("NA") && !string.isEmpty()) {
              reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, string));
            }
          }
          if (cols[isDirected].equals("0")) {
            reaction.setReversible(true);
          }
          model.addReaction(reaction);
          result.add(reaction);
        }
      }
    }
    return result;
  }

  private Element findElementByName(Model model, String name) {
    Element result = elementsByName.get(name);
    if (result == null) {
      for (Element element : model.getElements()) {
        if (Objects.equals(element.getName(), name)) {
          if (((Species) element).getComplex() == null) {
            if (result == null || element.getCompartment() == null) {
              result = element;
            }
          }
        }
      }
    }
    if (result==null) {
      logger.error(name);
    }
    elementsByName.put(name, result);
    return result;
  }

}
