package lcsb.mapviewer;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;

public class AddElementPrefixCommand extends ModelCommand {

  String prefix;

  public AddElementPrefixCommand(Model model, String prefix) {
    super(model);
    this.prefix = prefix;
  }

  @Override
  protected void undoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void redoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void executeImplementation() {

    Model model = getModel();

    for (Element alias : model.getElements()) {
      alias.setElementId(prefix + alias.getElementId());
    }
    for (Reaction reaction : model.getReactions()) {
      reaction.setIdReaction(prefix + reaction.getElementId());
    }
  }

}
