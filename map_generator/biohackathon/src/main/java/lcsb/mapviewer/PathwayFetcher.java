package lcsb.mapviewer;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.MoveCommand;
import lcsb.mapviewer.converter.*;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.project05.data.*;
import lcsb.mapviewer.wikipathway.GpmlParser;

public class PathwayFetcher {
  Logger logger = LogManager.getLogger();

  public Pathway fetchPath(String id, PathwaySource source) throws IOException {
    switch (source) {
    case WIKIPATHWAYS:
      return new Pathway(source, id, fetchWikipathway(id), PathwayStandard.GPML);
    case MINERVA:
      return new Pathway(source, id, fetchMinerva(id), PathwayStandard.SBML);
    default:
      throw new RuntimeException("Unknown database type: " + source);
    }
  }

  private String fetchMinerva(String id) throws IOException {
    try {
      String tmp[] = id.split("\t");
      String host = tmp[0];
      String projectId = tmp[1];
      Integer modelId = Integer.valueOf(tmp[2]);

      double height = Double.valueOf(tmp[4]);
      double width = Double.valueOf(tmp[5]);
      double x = Double.valueOf(tmp[6]);
      double y = Double.valueOf(tmp[7]);

      String url = host + "projects/" + projectId + "/models/" + modelId
          + ":downloadModel?handlerClass=lcsb.mapviewer.converter.model.sbml.SbmlParser&"
          + "polygonString=" +
          x + "," + y + ";" +
          (x + width) + "," + y + ";" +
          (x + width) + "," + (y + height) + ";" +
          x + "," + (y + height) + ";";

      String content = fetchFile(url);

      InputStream is = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));

      Model model = new SbmlParser().createModel(new ConverterParams().inputStream(is));
      for (Element element : model.getElements()) {
        if (element.getX() < x) {
          element.setX(x);
        }
        if (element.getY() < y) {
          element.setY(y);
        }

        if (element.getX() + element.getWidth() > x + width) {
          element.setWidth(x + width - element.getX());
        }
        if (element.getY() + element.getHeight() > y + height) {
          element.setHeight(y + height - element.getY());
        }
      }

      new MoveCommand(model, -x, -y).execute();
      model.setWidth(width);
      model.setHeight(height);
      return new SbmlParser().model2String(model);
    } catch (InvalidInputDataExecption | CommandExecutionException | ConverterException e) {
      throw new RuntimeException("Problem with pathway", e);
    }
  }

  private String fetchWikipathway(String id) throws IOException {
    String url = "https://www.wikipathways.org//wpi/wpi.php?action=downloadFile&type=gpml&pwTitle=Pathway:" + id;
    String result = fetchFile(url);
    try {
      InputStream is = new ByteArrayInputStream(result.getBytes("UTF-8"));
      new GpmlParser().createModel(new ConverterParams().inputStream(is));
    } catch (Exception e) {
      logger.error("Problem with wikipathway: " + id);
      return null;
    }
    return result;
  }

  String fetchFile(String url) throws MalformedURLException, IOException, UnsupportedEncodingException {
    logger.info("Fetching file: " + url);
    URL website = new URL(url);
    ByteArrayOutputStream fos = new ByteArrayOutputStream();
    IOUtils.copy(website.openStream(), fos);
    String result = new String(fos.toByteArray(), "UTF-8");
    logger.info("File fetched");
    return result;
  }

  public List<Pathway> fetchWikipathwayPathways(String filename) throws IOException {
    List<Pathway> result = new ArrayList<>();
    try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
      String line;
      int termColumn = -1;
      int pathwayTypeColumn = -1;
      while ((line = br.readLine()) != null) {
        String cols[] = line.split("\t");
        if (line.indexOf("Term") >= 0) {
          for (int i = 0; i < cols.length; i++) {
            if (cols[i].equals("Term")) {
              termColumn = i;
            } else if (cols[i].equals("pathway_db")) {
              pathwayTypeColumn = i;
            }
          }
          continue;
        }
        if (cols[pathwayTypeColumn].toLowerCase().contains("wikipathways")) {
          String tmp[] = cols[termColumn].split(" ");
          String pathwayName = tmp[tmp.length - 1];

          Pathway p = fetchPath(pathwayName, PathwaySource.WIKIPATHWAYS);
          if (p.content != null) {
            result.add(p);
          }
        } else {
          logger.error("Unknown pathway type: " + cols[pathwayTypeColumn]);
        }

      }
    }
    return result;
  }

  public List<Pathway> fetchMinervaPathways(String filename) throws IOException {
    List<Pathway> result = new ArrayList<>();
    try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
      String line;
      while ((line = br.readLine()) != null) {
        String cols[] = line.split("\t");
        if (cols[0].equals("minerva_instance")) {
          continue;
        }

        result.add(fetchPath(line, PathwaySource.MINERVA));
      }
    }
    return result;
  }
}
