package lcsb.mapviewer;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.thoughtworks.xstream.converters.ConversionException;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.MoveCommand;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.*;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.project05.data.Pathway;
import lcsb.mapviewer.project05.data.PathwayStandard;
import lcsb.mapviewer.wikipathway.GpmlParser;

public class PathwayMerger {

  static int MARGIN = 200;

  Logger logger = LogManager.getLogger();

  Pathway mergePathways(List<Pathway> pathways) throws ConverterException {
    int count = pathways.size();

    double offsetX = 0;
    double offsetY = 0;

    Model result = new ModelFullIndexed(null);

    int rowSize = (int) Math.sqrt(count);

    double mapWidth = 0.0;
    double mapHeight = 0.0;

    int counter = 0;
    double rowHeight = 0;
    for (Pathway pathway : pathways) {
      try {

        Model model = getModel(pathway);

        new MoveCommand(model, offsetX, offsetY).execute();
        new AddElementPrefixCommand(model, "path_" + counter + "_").execute();

        result.addElements(model.getElements());
        result.addReactions(new ArrayList<>(model.getReactions()));

        mapHeight = Math.max(mapHeight, offsetY + model.getHeight() + MARGIN);
        mapWidth = Math.max(mapWidth, offsetX + model.getWidth() + MARGIN);

        counter++;
        rowHeight = Math.max(rowHeight, model.getHeight() + MARGIN);
        if (counter % rowSize == 0) {
          offsetX = 0;
          offsetY += rowHeight;
          rowHeight = 0;
        } else {
          offsetX += model.getWidth() + MARGIN;
        }
      } catch (CommandExecutionException e) {
        throw new ConversionException("Problem with pathway");
      }
    }

    result.setWidth(mapWidth);
    result.setHeight(mapHeight);
    
    return new Pathway(null, null, new SbmlParser().model2String(result), PathwayStandard.SBML);
  }

  private Model getModel(Pathway pathway) {
    InputStream is = new ByteArrayInputStream(pathway.content.getBytes(StandardCharsets.UTF_8));
    Model model = null;
    try {
      if (pathway.format == PathwayStandard.GPML) {
        model = new GpmlParser().createModel(new ConverterParams().inputStream(is));
      } else if (pathway.format == PathwayStandard.SBML) {
        model = new SbmlParser().createModel(new ConverterParams().inputStream(is));
      } else {
        throw new InvalidArgumentException("Invalid format: " + pathway.format);
      }
    } catch (InvalidInputDataExecption | ConverterException e) {
      new RuntimeException("Problem with pathway");
    }
    return model;
  }
}
