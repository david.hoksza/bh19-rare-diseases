minerva_instance	project_id	model_id	name	bounds.height	bounds.width	bounds.x	bounds.y
https://pdmap.uni.lu/minerva/api/	pd_map_autumn_19	4895	Autophagy	1647.75	3794	13356	4422.25
https://pdmap.uni.lu/minerva/api/	pd_map_autumn_19	4895	Postsynaptic terminal	1240.75	5976.58333333334	4263.41666666666	500
https://pdmap.uni.lu/minerva/api/	pd_map_autumn_19	4895	Dopamine secretion and recycling	4048	1982	20352.6666666667	4638.75
https://pdmap.uni.lu/minerva/api/	pd_map_autumn_19	4895	Signalling in striatal neurons	5280	3282	22349	3688.25
https://pdmap.uni.lu/minerva/api/	pd_map_autumn_19	4895	Autophagy	2359.75	2322.33333333334	14797.6666666667	6090
https://pdmap.uni.lu/minerva/api/	pd_map_autumn_19	4895	Neuroinflammation	3380	10236	3065.66666666666	9940.75
https://pdmap.uni.lu/minerva/api/	pd_map_autumn_19	4903	Autophagy	1136	1852	3166	1136
