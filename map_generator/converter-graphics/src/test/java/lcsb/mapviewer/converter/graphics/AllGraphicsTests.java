package lcsb.mapviewer.converter.graphics;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.converter.graphics.bioEntity.AllBioEntityTests;
import lcsb.mapviewer.converter.graphics.geometry.AllGeometryTests;
import lcsb.mapviewer.converter.graphics.placefinder.AllPlaceFinderTest;

@RunWith(Suite.class)
@SuiteClasses({
    AbstractImageGeneratorTest.class,
    AllBioEntityTests.class,
    AllGeometryTests.class,
    AllPlaceFinderTest.class,
    ConverterTest.class,
    ImageGeneratorsTest.class,
    MapGeneratorTest.class,
    NormalImageGeneratorTest.class,
    PdfImageGeneratorTest.class,
    SvgImageGeneratorTest.class

})
public class AllGraphicsTests {

}
