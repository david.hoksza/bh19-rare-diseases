package lcsb.mapviewer.converter.graphics.bioEntity.element.compartment;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.atLeastOnce;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class SquareCompartmentConverterTest extends GraphicsTestFunctions {

  Logger logger = LogManager.getLogger();

  ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE);
  SquareCompartmentConverter converter = new SquareCompartmentConverter(colorExtractor);

  @Test
  public void testDrawSolidAndColorUsage() throws Exception {
    int size = 600;
    Model model = new ModelFullIndexed(null);

    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());

    SquareCompartment pathway = new SquareCompartment("id");
    pathway.setX(10);
    pathway.setY(10);
    pathway.setWidth(100);
    pathway.setHeight(200);
    pathway.setFillColor(Color.BLUE);
    pathway.setBorderColor(Color.YELLOW);
    pathway.setFontColor(Color.GREEN);
    pathway.setName("xyz");
    pathway.setTransparencyLevel("100");
    model.addElement(pathway);

    converter.draw(pathway, graphics, new ConverterParams().nested(true));

    ArgumentCaptor<Color> argument = ArgumentCaptor.forClass(Color.class);
    Mockito.verify(graphics, atLeastOnce()).setColor(argument.capture());
    List<Color> values = removeAlpha(argument.getAllValues());

    assertTrue("Fill colour wasn't used", values.contains(Color.BLUE));
    assertTrue("Border colour wasn't used", values.contains(Color.YELLOW));
    assertTrue("Font colour wasn't used", values.contains(Color.GREEN));

  }

}
