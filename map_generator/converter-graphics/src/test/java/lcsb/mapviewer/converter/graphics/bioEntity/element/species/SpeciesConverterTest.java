package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.converter.graphics.geometry.ArrowTransformation;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.GenericColorSchema;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.*;

public class SpeciesConverterTest extends GraphicsTestFunctions {
  Logger logger = LogManager.getLogger(SpeciesConverterTest.class);

  ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawAliasWithLayouts() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    ProteinConverter rc = new ProteinConverter(colorExtractor);

    GenericProtein alias = createProtein();
    rc.draw(alias, graphics, new ConverterParams());

    int val = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    GenericProtein alias2 = createProtein();

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    ColorSchema schema = new GenericColorSchema();
    schema.setColor(Color.RED);
    List<ColorSchema> schemas = new ArrayList<>();
    schemas.add(schema);

    rc.draw(alias2, graphics, new ConverterParams(), schemas);

    int val2 = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    assertTrue(val != val2);
  }

  @Test
  public void testDrawAfterDrawingReactionWithLayouts() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    ProteinConverter rc = new ProteinConverter(colorExtractor);

    GenericProtein alias = createProtein();
    rc.draw(alias, graphics, new ConverterParams());

    int val = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    GenericProtein alias2 = createProtein();

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    ColorSchema schema = new GenericColorSchema();
    schema.setColor(Color.RED);
    List<ColorSchema> schemas = new ArrayList<>();
    schemas.add(schema);

    rc.draw(alias2, graphics, new ConverterParams(), schemas);

    int val2 = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    rc.draw(alias2, graphics, new ConverterParams(), new ArrayList<>());

    int val3 = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    assertTrue(val != val2);
    assertEquals(val, val3);
  }

  @Test
  public void testDrawReactionWithFewLayouts() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    ProteinConverter rc = new ProteinConverter(colorExtractor);

    GenericProtein alias = createProtein();
    rc.draw(alias, graphics, new ConverterParams());

    int val = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    GenericProtein alias2 = createProtein();

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    ColorSchema schema = new GenericColorSchema();
    schema.setColor(Color.RED);
    List<ColorSchema> schemas = new ArrayList<>();
    schemas.add(schema);
    schemas.add(null);
    schema = new GenericColorSchema();
    schema.setColor(Color.BLUE);
    schemas.add(schema);

    rc.draw(alias2, graphics, new ConverterParams(), schemas);

    int val2 = bi.getRGB((int) (alias.getX() + alias.getWidth() / 4), (int) alias.getCenterY());
    int val3 = bi.getRGB((int) (alias.getCenterX()), (int) alias.getCenterY());
    int val4 = bi.getRGB((int) (alias.getX() + 3 * alias.getWidth() / 4), (int) alias.getCenterY());

    assertTrue(val != val2);
    assertEquals(val, val3);
    assertTrue(val != val4);
  }

  @Test
  public void testDrawResidue() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    ModificationResidue residue = new Residue();
    residue.setPosition(new Point2D.Double(10, 10));

    ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
    converter.drawModification(residue, graphics, false);
    verify(converter, times(1)).drawResidue(any(), any(), anyBoolean());
  }

  @Test
  public void testDrawText() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    graphics.setColor(Color.YELLOW);

    GenericProtein protein = new GenericProtein("i");
    protein.setFontColor(Color.PINK);

    ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
    converter.drawText(protein, graphics, new ConverterParams());
    
    assertEquals(Color.YELLOW, graphics.getColor());

    ArgumentCaptor<Color> argument = ArgumentCaptor.forClass(Color.class);
    Mockito.verify(graphics, atLeastOnce()).setColor(argument.capture());
    List<Color> values = removeAlpha(argument.getAllValues());

    assertTrue("Font colour wasn't used", values.contains(Color.PINK));
  }

  @Test
  public void testDrawBindingRegion() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    Protein protein = createProtein();
    BindingRegion bindingRegion = new BindingRegion();
    bindingRegion.setPosition(new Point2D.Double(10, 10));
    bindingRegion.setWidth(100.0);
    bindingRegion.setHeight(10.0);
    protein.addBindingRegion(bindingRegion);

    ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
    converter.drawModification(bindingRegion, graphics, false);
    verify(converter, times(1)).drawBindingRegion(any(), any());
  }

  @Test
  public void testDrawModificationSite() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    ModificationSite modificationSite = new ModificationSite();
    modificationSite.setPosition(new Point2D.Double(10, 10));

    ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
    converter.drawModification(modificationSite, graphics, false);
    verify(converter, times(1)).drawModificationSite(any(), any(), anyBoolean());
  }

  @Test
  public void testDrawProteinBindingRegion() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    ProteinBindingDomain proteinBindingRegion = new ProteinBindingDomain();
    proteinBindingRegion.setPosition(new Point2D.Double(10, 10));
    proteinBindingRegion.setWidth(100);
    proteinBindingRegion.setHeight(20);

    ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
    converter.drawModification(proteinBindingRegion, graphics, false);
    verify(converter, times(1)).drawProteinBindingDomain(any(), any());
  }

  @Test
  public void testDrawCodingRegion() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    CodingRegion codingRegion = new CodingRegion();
    codingRegion.setPosition(new Point2D.Double(10, 10));
    codingRegion.setWidth(100);
    codingRegion.setHeight(20);

    ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
    converter.drawModification(codingRegion, graphics, false);
    verify(converter, times(1)).drawCodingRegion(any(), any());
  }

  @Test
  public void testDrawRegulatoryRegion() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    RegulatoryRegion regulatoryRegion = new RegulatoryRegion();
    regulatoryRegion.setPosition(new Point2D.Double(10, 10));
    regulatoryRegion.setWidth(100);
    regulatoryRegion.setHeight(20);

    ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
    converter.drawModification(regulatoryRegion, graphics, false);
    verify(converter, times(1)).drawRegulatoryRegion(any(), any());
  }

  @Test
  public void testDrawTranscriptionSite() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    TranscriptionSite transcriptionSite = new TranscriptionSite();
    transcriptionSite.setPosition(new Point2D.Double(10, 10));
    transcriptionSite.setWidth(100);
    transcriptionSite.setHeight(20);
    transcriptionSite.setActive(true);
    transcriptionSite.setDirection("LEFT");

    ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));

    converter.drawModification(transcriptionSite, graphics, false);
    verify(converter, times(1)).drawTranscriptionSite(any(), any());
  }

  @Test
  public void testDrawTranscriptionSiteSize() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    TranscriptionSite transcriptionSite = new TranscriptionSite();
    transcriptionSite.setPosition(new Point2D.Double(10, 10));
    transcriptionSite.setWidth(100);
    transcriptionSite.setHeight(20);
    transcriptionSite.setDirection("LEFT");

    ArrowTransformation arrowTransformation = Mockito.spy(new ArrowTransformation());

    ProteinConverter converter = new ProteinConverter(colorExtractor);
    converter.setArrowTransformation(arrowTransformation);

    converter.drawModification(transcriptionSite, graphics, false);
    verify(arrowTransformation, times(1)).drawLine(
        argThat(
            polylineData -> polylineData.getEndAtd().getLen() == SpeciesConverter.TRANSCRIPTION_SITE_ARROW_LENGTH),
        any());
  }

  @Test
  public void testDrawTranscriptionSiteDescription() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());

    TranscriptionSite transcriptionSite = new TranscriptionSite();
    transcriptionSite.setPosition(new Point2D.Double(10, 10));
    transcriptionSite.setWidth(100);
    transcriptionSite.setHeight(20);
    transcriptionSite.setActive(true);
    transcriptionSite.setDirection("LEFT");
    transcriptionSite.setName("x");

    ProteinConverter converter = new ProteinConverter(colorExtractor);

    converter.drawModification(transcriptionSite, graphics, false);
    verify(graphics, times(1)).drawString(anyString(), anyInt(), anyInt());
  }

  @Test
  public void testDrawAliasWithGlyph() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    ProteinConverter rc = new ProteinConverter(colorExtractor);

    GenericProtein alias = createProtein();
    Glyph glyph = new Glyph();
    UploadedFileEntry file = new UploadedFileEntry();
    file.setOriginalFileName("test");
    byte[] fileContent = Files.readAllBytes(new File("testFiles/glyph.png").toPath());

    file.setFileContent(fileContent);
    glyph.setFile(file);
    alias.setGlyph(glyph);
    rc.draw(alias, graphics, new ConverterParams());

    verify(graphics, times(1)).drawImage(any(Image.class), anyInt(), anyInt(), anyInt(), anyInt(), anyInt(), anyInt(),
        anyInt(), anyInt(), nullable(ImageObserver.class));
  }

}
