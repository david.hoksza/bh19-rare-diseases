package lcsb.mapviewer.converter.graphics;

import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.mockito.Mockito;

import lcsb.mapviewer.common.UnitTestFailedWatcher;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.species.*;

public abstract class GraphicsTestFunctions {

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  int elementCounter = 1;

  protected Graphics2D createGraphicsMock() {
    Graphics2D graphics = Mockito.mock(Graphics2D.class);
    FontMetrics fontMetrics = Mockito.mock(FontMetrics.class);
    when(fontMetrics.getStringBounds(nullable(String.class), nullable(Graphics.class)))
        .thenReturn(new Rectangle2D.Double());
    when(graphics.getFontMetrics()).thenReturn(fontMetrics);
    return graphics;
  }

  protected Reaction createReaction(GenericProtein modifierElement, GenericProtein reactantElement,
      GenericProtein productElement) {
    Reaction reaction = new Reaction("r" + elementCounter++);

    Point2D center = new Point2D.Double((reactantElement.getCenterX() + productElement.getCenterX()) / 2,
        (reactantElement.getCenterY() + productElement.getCenterY()) / 2);

    Modifier modifier = new Catalysis(modifierElement);
    modifier.setLine(new PolylineData(modifierElement.getCenter(), center));
    modifier.getLine().setWidth(1.0);

    Reactant reactant = new Reactant(reactantElement);
    reactant.setLine(new PolylineData(reactantElement.getCenter(), center));
    reactant.getLine().setWidth(1.0);
    Product product = new Product(productElement);
    product.setLine(new PolylineData(productElement.getCenter(), center));
    product.getLine().setWidth(1.0);
    reaction.addModifier(modifier);
    reaction.addProduct(product);
    reaction.addReactant(reactant);
    reaction.setLine(new PolylineData(center, center));
    reaction.setZ(elementCounter);
    return reaction;
  }

  protected GenericProtein createProtein() {
    GenericProtein result = new GenericProtein("s" + elementCounter++);
    result.setZ(elementCounter);
    result.setX(10);
    result.setY(10);
    result.setWidth(10);
    result.setHeight(10);
    return result;
  }

  protected Complex createComplex() {
    Complex complex = new Complex("c" + elementCounter++);
    complex.setName("a");
    complex.setX(300);
    complex.setY(90);
    complex.setWidth(100);
    complex.setHeight(50);
    complex.setZ(elementCounter);
    return complex;
  }

  protected List<Color> removeAlpha(List<Color> allValues) {
    List<Color> result = new ArrayList<>();
    for (Color color : allValues) {
      result.add(new Color(color.getRGB()));
    }
    return result;
  }

  protected SimpleMolecule createSimpleMolecule() {
    SimpleMolecule protein = new SimpleMolecule ("sm" + elementCounter++);
    protein.setX(10);
    protein.setY(20);
    protein.setWidth(100);
    protein.setHeight(80);
    protein.setFillColor(Color.WHITE);

    return protein;
  }

}
