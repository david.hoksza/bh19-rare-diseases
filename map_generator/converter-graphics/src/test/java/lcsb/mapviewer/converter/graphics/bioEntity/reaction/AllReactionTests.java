package lcsb.mapviewer.converter.graphics.bioEntity.reaction;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ReactionConverterTest.class })
public class AllReactionTests {

}
