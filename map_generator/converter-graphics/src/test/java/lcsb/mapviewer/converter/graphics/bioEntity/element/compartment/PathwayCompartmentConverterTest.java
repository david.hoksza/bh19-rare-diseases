package lcsb.mapviewer.converter.graphics.bioEntity.element.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.atLeastOnce;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class PathwayCompartmentConverterTest extends GraphicsTestFunctions {

  ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE);
  PathwayCompartmentConverter converter = new PathwayCompartmentConverter(colorExtractor);

  @Test
  public void testDrawSolid() throws Exception {
    int size = 600;
    Model model = new ModelFullIndexed(null);

    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());

    PathwayCompartment pathway = new PathwayCompartment("id");
    pathway.setX(10);
    pathway.setY(10);
    pathway.setWidth(100);
    pathway.setHeight(200);
    pathway.setFillColor(Color.BLUE);
    pathway.setBorderColor(Color.ORANGE);
    pathway.setFontColor(Color.RED);
    pathway.setTransparencyLevel("12");
    model.addElement(pathway);

    converter.draw(pathway, graphics, new ConverterParams().nested(true));

    ArgumentCaptor<Color> argument = ArgumentCaptor.forClass(Color.class);
    Mockito.verify(graphics, atLeastOnce()).setColor(argument.capture());
    List<Color> values = removeAlpha(argument.getAllValues());

    assertTrue("Fill colour wasn't used", values.contains(Color.BLUE));
    assertTrue("Border colour wasn't used", values.contains(Color.ORANGE));
    assertTrue("Font colour wasn't used", values.contains(Color.RED));
  }

  @Test
  public void testDrawTransparent() throws Exception {
    int size = 600;
    Model model = new ModelFullIndexed(null);

    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());

    PathwayCompartment pathway = new PathwayCompartment("id");
    pathway.setX(10);
    pathway.setY(10);
    pathway.setWidth(100);
    pathway.setHeight(200);
    pathway.setFillColor(Color.BLUE);
    model.addElement(pathway);

    converter.draw(pathway, graphics, new ConverterParams().nested(true));

    ArgumentCaptor<Color> argument = ArgumentCaptor.forClass(Color.class);
    Mockito.verify(graphics, Mockito.atLeastOnce()).setColor(argument.capture());

    // alpha will be different
    String blueHtml = new ColorParser().colorToHtml(Color.BLUE);
    String usedColorHtml = new ColorParser().colorToHtml(argument.getAllValues().get(0));
    assertEquals(blueHtml, usedColorHtml);
  }

}
