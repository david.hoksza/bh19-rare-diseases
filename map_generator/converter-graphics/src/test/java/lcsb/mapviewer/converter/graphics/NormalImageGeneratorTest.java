package lcsb.mapviewer.converter.graphics;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;

public class NormalImageGeneratorTest extends GraphicsTestFunctions {
  static Logger logger = LogManager.getLogger(NormalImageGenerator.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  private Model createCompartmentModel() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(526);
    model.setHeight(346);

    LayerText compAlias = new LayerText();
    compAlias.setX(256.0);
    compAlias.setY(79.0);
    compAlias.setWidth(233.0);
    compAlias.setHeight(188.0);
    compAlias.setNotes("asd as");
    compAlias.setColor(Color.BLACK);
    Layer layer = new Layer();
    layer.addLayerText(compAlias);
    layer.setVisible(true);
    model.addLayer(layer);

    layer.addLayerLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(100, 100)));

    Complex alias = new Complex("1");
    alias.setName("a");
    alias.setX(300);
    alias.setY(90);
    alias.setWidth(100);
    alias.setHeight(50);
    model.addElement(alias);

    return model;
  }

  @Test
  public void testArtifitialInHierarchicalView() throws Exception {
    Model model = createCompartmentModel();
    Layer layer = model.getLayers().iterator().next();

    layer.addLayerText(new LayerText(new Rectangle2D.Double(10, 10, 500, 500), "X asd"));
    layer.addLayerText(new LayerText(new Rectangle2D.Double(20, 20, 200, 200), "Y qwe ret"));
    layer.addLayerText(new LayerText(new Rectangle2D.Double(100, 100, 199, 220), "Z dgf fsd aaewq ret"));

    new CreateHierarchyCommand(model, 1, 8).execute();
    model.getCompartments().get(1).setTransparencyLevel("2");
    model.getCompartments().get(0).setTransparencyLevel("2");
    model.getCompartments().get(3).setTransparencyLevel("2");
    new PngImageGenerator(new Params().level(2).scale(4).width(600).height(600).model(model).nested(true));
  }

}
