package lcsb.mapviewer.converter.graphics;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.awt.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class AbstractImageGeneratorTest extends GraphicsTestFunctions {
  Logger logger = LogManager.getLogger(AbstractImageGeneratorTest.class);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawSimpleMap() throws Exception {
    try {
      Graphics2D graphics = createGraphicsMock();

      Model model = createSimpleModel();

      AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
      gen.draw();

      // 3 times for proteins and 4 times for reaction
      verify(graphics, times(7)).draw(any());
    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testDrawSimpleMapWithNesting() throws Exception {
    try {
      Graphics2D graphics = createGraphicsMock();

      Model model = createSimpleModel();

      AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
      gen.setParams(new AbstractImageGenerator.Params().model(model).nested(true));
      gen.draw();

      // 3 times for proteins and 4 times for reaction
      verify(graphics, times(7)).draw(any());
    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testDrawMapWithInvisibleLayerNesting() throws Exception {
    try {
      Graphics2D graphics = createGraphicsMock();

      Model model = createEmptyModel();
      Layer layer = new Layer();
      layer.setVisible(false);
      LayerRect rect = new LayerRect();
      rect.setX(10.0);
      rect.setX(10.0);
      rect.setWidth(20.);
      rect.setHeight(20.);
      layer.addLayerRect(rect);
      model.addLayer(layer);

      AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
      gen.setParams(new AbstractImageGenerator.Params().model(model).nested(true));
      gen.draw();

      // 3 times for proteins and 4 times for reaction
      verify(graphics, times(0)).draw(any());
    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testDrawSimpleMapWithWhenNestingHidesElement() throws Exception {
    try {
      Graphics2D graphics = createGraphicsMock();

      Model model = createSimpleModel();
      model.getElementByElementId("s1").setVisibilityLevel(2);

      AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
      gen.setParams(new AbstractImageGenerator.Params().model(model).nested(true).level(0));
      gen.draw();

      // 2 times for proteins and 3 times for reaction
      verify(graphics, times(5)).draw(any());
    } catch (Exception e) {
      throw e;
    }
  }

  private Model createSimpleModel() {
    Model model = createEmptyModel();

    GenericProtein protein1 = createProtein();
    model.addElement(protein1);

    GenericProtein protein2 = createProtein();
    protein2.setX(30);
    model.addElement(protein2);

    GenericProtein protein3 = createProtein();
    protein3.setX(40);
    model.addElement(protein3);

    Reaction reaction = createReaction(protein1, protein2, protein3);

    model.addReaction(reaction);

    return model;
  }

  private Model createEmptyModel() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(100);
    model.setHeight(100);
    return model;
  }

  private AbstractImageGenerator createAbstractImageGeneratorMock(Graphics2D graphics, Model model) throws Exception {
    AbstractImageGenerator result = Mockito.mock(AbstractImageGenerator.class, Mockito.CALLS_REAL_METHODS);
    result.setGraphics(graphics);
    result.setParams(new AbstractImageGenerator.Params().model(model).level(0));
    return result;

  }

}
