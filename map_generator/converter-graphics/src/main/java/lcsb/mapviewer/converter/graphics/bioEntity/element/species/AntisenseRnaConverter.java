package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.*;
import java.awt.geom.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * This class defines methods used for drawing {@link AntisenseRna} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */

public class AntisenseRnaConverter extends SpeciesConverter<AntisenseRna> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(AntisenseRnaConverter.class.getName());

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public AntisenseRnaConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final AntisenseRna antisenseRna, final Graphics2D graphics, final ConverterParams params) {
    Color oldColor = graphics.getColor();
    GeneralPath path = getAntisenseRnaPath(antisenseRna);
    graphics.setColor(antisenseRna.getFillColor());
    graphics.fill(path);
    Stroke stroke = graphics.getStroke();
    graphics.setColor(antisenseRna.getBorderColor());
    graphics.setStroke(getBorderLine(antisenseRna));
    graphics.draw(path);
    graphics.setStroke(stroke);

    for (ModificationResidue mr : antisenseRna.getRegions()) {
      drawModification(mr, graphics, false);
    }

    drawText(antisenseRna, graphics, params);
    graphics.setColor(oldColor);
  }

  /**
   * Returns {@link AntisenseRna} border as a {@link GeneralPath} object.
   * 
   * @param antisenseRna
   *          {@link AntisenseRna} for which we want to get border
   * @return border of the {@link AntisenseRna}
   */
  private GeneralPath getAntisenseRnaPath(final AntisenseRna antisenseRna) {
    // CHECKSTYLE:OFF
    GeneralPath path = new GeneralPath(Path2D.WIND_EVEN_ODD, 4);
    path.moveTo(antisenseRna.getX(), antisenseRna.getY());
    path.lineTo(antisenseRna.getX() + antisenseRna.getWidth() * 3 / 4, antisenseRna.getY());
    path.lineTo(antisenseRna.getX() + antisenseRna.getWidth(), antisenseRna.getY() + antisenseRna.getHeight());
    path.lineTo(antisenseRna.getX() + antisenseRna.getWidth() / 4, antisenseRna.getY() + antisenseRna.getHeight());
    // CHECKSTYLE:ON
    path.closePath();
    return path;
  }

  @Override
  public PathIterator getBoundPathIterator(final AntisenseRna alias) {
    return getAntisenseRnaPath(alias).getPathIterator(new AffineTransform());
  }

}
