package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.PathIterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class defines methods used for drawing {@link SimpleMolecule} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class SimpleMoleculeConverter extends SpeciesConverter<SimpleMolecule> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SimpleMoleculeConverter.class.getName());

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public SimpleMoleculeConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final SimpleMolecule simpleMolecule, final Graphics2D graphics,
      final ConverterParams params) {
    int homodir;
    if (params.isSbgnFormat()) {
      // If the SBGN display mode is set, multimer is shown as two stacked glyphs
      if (simpleMolecule.getHomodimer() > 1) {
        homodir = 2;
      } else {
        homodir = 1;
      }
    } else {
      homodir = simpleMolecule.getHomodimer();
    }

    simpleMolecule.setWidth(simpleMolecule.getWidth() - SpeciesConverter.HOMODIMER_OFFSET * (homodir - 1));
    simpleMolecule.setHeight(simpleMolecule.getHeight() - SpeciesConverter.HOMODIMER_OFFSET * (homodir - 1));

    simpleMolecule.setX(simpleMolecule.getX() + SpeciesConverter.HOMODIMER_OFFSET * (homodir));
    simpleMolecule.setY(simpleMolecule.getY() + SpeciesConverter.HOMODIMER_OFFSET * (homodir));

    for (int i = 0; i < homodir; i++) {
      simpleMolecule.setX(simpleMolecule.getX() - SpeciesConverter.HOMODIMER_OFFSET);
      simpleMolecule.setY(simpleMolecule.getY() - SpeciesConverter.HOMODIMER_OFFSET);
      Shape shape = new Ellipse2D.Double(simpleMolecule.getX(), simpleMolecule.getY(), simpleMolecule.getWidth(),
          simpleMolecule.getHeight());
      Color oldColor = graphics.getColor();
      graphics.setColor(simpleMolecule.getFillColor());
      graphics.fill(shape);
      graphics.setColor(simpleMolecule.getBorderColor());
      Stroke stroke = graphics.getStroke();
      graphics.setStroke(getBorderLine(simpleMolecule));
      graphics.draw(shape);
      graphics.setStroke(stroke);
      graphics.setColor(oldColor);

      // SBGN view - multimers are displayed with a unit of information containing
      // cardinality
      if (params.isSbgnFormat() && (i == homodir - 1)) {
        String unitOfInformationText = null;
        if (simpleMolecule.getStatePrefix() != null && simpleMolecule.getStateLabel() != null) {
          unitOfInformationText = simpleMolecule.getStatePrefix() + ":" + simpleMolecule.getStateLabel();
        }
        if (homodir == 2 && (unitOfInformationText == null || !unitOfInformationText.contains("N:"))) {
          if (unitOfInformationText != null) {
            unitOfInformationText += "; ";
          } else {
            unitOfInformationText = "";
          }
          unitOfInformationText += "N:" + simpleMolecule.getHomodimer();
        }

        drawUnitOfInformation(unitOfInformationText, simpleMolecule, graphics);
      }
    }
    simpleMolecule.setWidth(simpleMolecule.getWidth() + SpeciesConverter.HOMODIMER_OFFSET * (homodir - 1));
    simpleMolecule.setHeight(simpleMolecule.getHeight() + SpeciesConverter.HOMODIMER_OFFSET * (homodir - 1));
    drawText(simpleMolecule, graphics, params);
  }

  @Override
  public PathIterator getBoundPathIterator(final SimpleMolecule simpleMolecule) {
    throw new InvalidStateException("This class doesn't provide boundPath");
  }

}
