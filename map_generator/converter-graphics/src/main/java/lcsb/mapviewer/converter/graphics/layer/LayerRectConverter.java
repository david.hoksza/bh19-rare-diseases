package lcsb.mapviewer.converter.graphics.layer;

import java.awt.*;
import java.awt.geom.Rectangle2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.map.layout.graphics.LayerRect;

/**
 * This class allows to draw layer on Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class LayerRectConverter {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(LayerRectConverter.class.getName());

  /**
   * Draws rectangle on the Graphics2D.
   * 
   * @param rect
   *          object to be drawn
   * @param graphics
   *          where we want to draw the object
   */
  public void draw(final LayerRect rect, final Graphics2D graphics) {
    Color tmpColor = graphics.getColor();
    graphics.setColor(rect.getColor());
    Rectangle2D rectangle = new Rectangle2D.Double(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
    graphics.draw(rectangle);
    graphics.setColor(tmpColor);

  }

}