package lcsb.mapviewer.converter.graphics.layer;

import java.awt.*;
import java.awt.geom.Rectangle2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.map.layout.graphics.LayerText;

/**
 * This class allows to draw {@link LayerText} on Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class LayerTextConverter {

  /**
   * Color used for text frames.
   */
  private static final Color FRAME_COLOR = Color.LIGHT_GRAY;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(LayerTextConverter.class.getName());
  /**
   * Should the border around text be visible.
   */
  private boolean visibleTextBorder = false;

  /**
   * Default constructor.
   * 
   * @param visibleTextBorder
   *          should the text have border
   */
  public LayerTextConverter(final boolean visibleTextBorder) {
    this.visibleTextBorder = visibleTextBorder;
  }

  public LayerTextConverter() {
  }

  /**
   * Draw text on the Graphics2D.
   * 
   * @param text
   *          object to be drawn
   * @param graphics
   *          where we want to draw the object
   */
  public void draw(final LayerText text, final Graphics2D graphics) {
    if (visibleTextBorder) {
      Color tmpColor = graphics.getColor();
      graphics.setColor(FRAME_COLOR);
      Rectangle2D rect = new Rectangle2D.Double(text.getX(), text.getY(), text.getWidth(), text.getHeight());
      graphics.draw(rect);
      graphics.setColor(tmpColor);
    }
    int x = text.getX().intValue() + 2;
    int y = text.getY().intValue();
    y += graphics.getFontMetrics().getHeight();
    graphics.drawString(text.getNotes(), x, y);

  }

}