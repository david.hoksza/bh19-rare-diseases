package lcsb.mapviewer.converter.graphics;

import java.io.*;

import org.apache.commons.io.output.ByteArrayOutputStream;

import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * This class implements PDF generator for the image object. This class uses
 * itext library to create all converters.
 * 
 * @author Piotr Gawron
 * 
 */
public class PdfImageGenerator extends AbstractImageGenerator {
  /**
   * Stream where pdf data will be generated. When request to save pdf appears
   * this stream is used to generate file.
   */
  private ByteArrayOutputStream inMemoryOutputStream;

  /**
   * Object representing pdf document.
   */
  private Document pdfDocument;

  /**
   * Default constructor. Create an image that is described by params. For more
   * information see
   * {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params
   * params}.
   *
   * @param params
   *          parameters used for the image creation.
   * @throws DrawingException
   *           thrown when there was a problem with drawing a map
   */
  public PdfImageGenerator(final Params params) throws DrawingException {
    super(params);
  }

  @Override
  protected void createImageObject(final double width, final double height) {
    try {
      inMemoryOutputStream = new ByteArrayOutputStream();
      pdfDocument = new Document(new Rectangle((int) width, (int) height));
      PdfWriter writer = PdfWriter.getInstance(pdfDocument, inMemoryOutputStream);
      pdfDocument.open();
      PdfContentByte cb = writer.getDirectContent();
      PdfGraphics2D g2d = new PdfGraphics2D(cb, (float) width, (float) height, new PdfFontMapper());

      setGraphics(g2d);
    } catch (DocumentException e) {
      throw new InvalidStateException("Problem with initializing pdf generator", e);
    }

  }

  @Override
  protected void closeImageObject() {
    getGraphics().dispose();
    pdfDocument.close();
  }

  @Override
  public void saveToFileImplementation(final String fileName) throws IOException {
    FileOutputStream fos = new FileOutputStream(fileName);
    saveToOutputStreamImplementation(fos);
    fos.close();
  }

  @Override
  public void saveToOutputStreamImplementation(OutputStream os) throws IOException {
    inMemoryOutputStream.writeTo(os);
  }

  @Override
  public void savePartToFileImplementation(int x, int y, int width, int height, String fileName) throws IOException {
    throw new NotImplementedException("Partial save is not implemented in PNG image generator");
  }

  @Override
  public void savePartToOutputStreamImplementation(int x, int y, int width, int height, OutputStream os)
      throws IOException {
    throw new NotImplementedException("Partial save is not implemented in PNG image generator");
  }

  @Override
  public String getFormatName() {
    return "PDF";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.PDF;
  }

  @Override
  public String getFileExtension() {
    return "pdf";
  }

}
