package lcsb.mapviewer.converter.graphics;

import java.awt.Color;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

/**
 * This class is a util class containing information about all currently
 * available implementations of {@link AbstractImageGenerator} class.
 * 
 * @author Piotr Gawron
 *
 */
public class ImageGenerators {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(ImageGenerators.class);
  /**
   * List of {@link AbstractImageGenerator} classes available in the system.
   */
  private List<Pair<String, Class<? extends AbstractImageGenerator>>> availableGenerators;

  /**
   * List of all possible objects extending {@link AbstractImageGenerator}
   * interface that are available in the system.
   */
  private List<AbstractImageGenerator> generatorInstances = null;

  /**
   * Default constructor.
   */
  public ImageGenerators() {
    try {
      availableGenerators = new ArrayList<Pair<String, Class<? extends AbstractImageGenerator>>>();
      Model model = new ModelFullIndexed(null);
      AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().model(model).width(1)
          .minColor(Color.WHITE).maxColor(Color.WHITE).height(1);
      generatorInstances = new ArrayList<>();
      generatorInstances.add(new PngImageGenerator(params));
      generatorInstances.add(new PdfImageGenerator(params));
      generatorInstances.add(new SvgImageGenerator(params));
      // generatorInstances.add(new JpgImageGenerator(params));
      for (AbstractImageGenerator abstractImageGenerator : generatorInstances) {
        availableGenerators
            .add(new Pair<String, Class<? extends AbstractImageGenerator>>(abstractImageGenerator.getFormatName(),
                abstractImageGenerator.getClass()));
      }

    } catch (DrawingException e) {
      throw new InvalidStateException("Problem with ImageGenarater instance creation...", e);
    }
  }

  /**
   * Returns {@link #availableGenerators}.
   * 
   * @return {@link #availableGenerators}
   */
  public List<Pair<String, Class<? extends AbstractImageGenerator>>> getAvailableImageGenerators() {
    return availableGenerators;
  }

  /**
   * Generates image and saves it to a file.
   * 
   * @param generatorClass
   *          which {@link AbstractImageGenerator} should be used to generate an
   *          image
   * @param params
   *          parameters of the image generation process (scaling, size, nesting,
   *          etc)
   * @param filename
   *          name of the file where result should be saved
   * @return {@link MimeType} of the generated object
   * @throws IOException
   *           thrown when there is a problem with output file
   * @throws DrawingException
   *           thrown when there is a problem drawing map
   */
  public MimeType generate(Class<? extends AbstractImageGenerator> generatorClass, AbstractImageGenerator.Params params,
      String filename)
      throws IOException, DrawingException {
    try {
      AbstractImageGenerator generator = generatorClass.getConstructor(AbstractImageGenerator.Params.class)
          .newInstance(params);
      generator.saveToFile(filename);
      return generator.getMimeType();
    } catch (InstantiationException e) {
      throw new InvalidStateException("Problem with ImageGenarater instance creation...", e);
    } catch (IllegalAccessException e) {
      throw new InvalidStateException("Problem with ImageGenarater instance creation...", e);
    } catch (IllegalArgumentException e) {
      throw new InvalidStateException("Problem with ImageGenarater instance creation...", e);
    } catch (InvocationTargetException e) {
      throw new InvalidStateException("Problem with ImageGenarater instance creation...", e);
    } catch (NoSuchMethodException e) {
      throw new InvalidStateException("Problem with ImageGenarater instance creation...", e);
    } catch (SecurityException e) {
      throw new InvalidStateException("Problem with ImageGenarater instance creation...", e);
    }
  }

  /**
   * Generates image and saves it to a file.
   * 
   * @param generatorClass
   *          name of the {@link AbstractImageGenerator} implementation that
   *          should be used to generate an image
   * @param params
   *          parameters of the image generation process (scaling, size, nesting,
   *          etc)
   * @param filename
   *          name of the file where result should be saved
   * @return {@link MimeType} of the generated object
   * @throws IOException
   *           thrown when there is a problem with output file
   * @throws DrawingException
   *           thrown when there is a problem drawing map
   */
  public MimeType generate(String generatorClass, Params params, String filename) throws IOException, DrawingException {
    for (Pair<String, Class<? extends AbstractImageGenerator>> element : availableGenerators) {
      if (element.getRight().getCanonicalName().equals(generatorClass)) {
        return generate(element.getRight(), params, filename);
      }
    }
    throw new InvalidArgumentException("Unknown class type: " + generatorClass);

  }

  /**
   * Returns file extension that should be used for files generated by
   * implementation of {@link AbstractImageGenerator} class.
   * 
   * @param generatorClass
   *          name of the class that extends {@link AbstractImageGenerator}
   * @return file extension that should be used for files generated by
   *         implementation of {@link AbstractImageGenerator} class
   */
  public String getExtension(String generatorClass) {
    for (Pair<String, Class<? extends AbstractImageGenerator>> element : availableGenerators) {
      if (element.getRight().getCanonicalName().equals(generatorClass)) {
        return getExtension(element.getRight());
      }
    }
    throw new InvalidArgumentException("Unknown class type: " + generatorClass);
  }

  public boolean isValidClassName(String generatorClass) {
    for (Pair<String, Class<? extends AbstractImageGenerator>> element : availableGenerators) {
      if (element.getRight().getCanonicalName().equals(generatorClass)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Returns file extension that should be used for files generated by
   * implementation of {@link AbstractImageGenerator} class.
   * 
   * @param generatorClass
   *          class that extends {@link AbstractImageGenerator}
   * @return file extension that should be used for files generated by
   *         implementation of {@link AbstractImageGenerator} class
   */
  public String getExtension(Class<? extends AbstractImageGenerator> generatorClass) {
    for (AbstractImageGenerator imageGenerator : generatorInstances) {
      if (generatorClass.isAssignableFrom(imageGenerator.getClass())) {
        return imageGenerator.getFileExtension();
      }
    }
    throw new InvalidArgumentException("Unknown class type: " + generatorClass);
  }
}
