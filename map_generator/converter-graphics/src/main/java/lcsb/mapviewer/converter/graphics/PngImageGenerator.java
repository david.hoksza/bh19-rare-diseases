package lcsb.mapviewer.converter.graphics;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.MimeType;

/**
 * This implementation of {@link AbstractImageGenerator} allows to generate PNG
 * images.
 * 
 * @author Piotr Gawron
 * 
 */
public class PngImageGenerator extends NormalImageGenerator {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(PngImageGenerator.class);

  /**
   * Default constructor. Create an image that is described by params. For more
   * information see
   * {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params
   * params}.
   * 
   * @param params
   *          parameters used for the image creation.
   * @throws DrawingException
   *           thrown when there was a problem with drawing a map
   */
  public PngImageGenerator(Params params) throws DrawingException {
    super(params);
  }

  @Override
  protected void closeImageObject() {
  }

  @Override
  public void saveToFileImplementation(String fileName) throws IOException {
    FileOutputStream fos = new FileOutputStream(new File(fileName));
    saveToOutputStreamImplementation(fos);
    fos.close();
  }

  @Override
  public void saveToOutputStreamImplementation(OutputStream os) throws IOException {
    ImageIO.write(getBi(), "PNG", os);
  }

  @Override
  public void savePartToFileImplementation(int x, int y, int width, int height, String fileName) throws IOException {
    FileOutputStream fos = new FileOutputStream(new File(fileName));
    savePartToOutputStreamImplementation(x, y, width, height, fos);
    fos.close();
  }

  @Override
  public void savePartToOutputStreamImplementation(int x, int y, int width, int height, OutputStream os)
      throws IOException {
    BufferedImage tmpBI = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Graphics2D tmpGraphics = tmpBI.createGraphics();
    tmpGraphics.drawImage(getBi(), 0, 0, width, height, x, y, x + width, y + width, null);
    ImageIO.write(tmpBI, "PNG", os);
  }

  @Override
  public String getFormatName() {
    return "PNG image";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.PNG;
  }

  @Override
  public String getFileExtension() {
    return "png";
  }

}
