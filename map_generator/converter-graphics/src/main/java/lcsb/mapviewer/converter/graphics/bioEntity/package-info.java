/**
 * Provides classes that draws {@link lcsb.mapviewer.model.map.BioEntity
 * BioEntites} on the Graphics2D object.
 */
package lcsb.mapviewer.converter.graphics.bioEntity;
