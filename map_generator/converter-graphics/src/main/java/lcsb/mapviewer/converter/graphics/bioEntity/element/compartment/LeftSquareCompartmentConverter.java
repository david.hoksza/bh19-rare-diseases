package lcsb.mapviewer.converter.graphics.bioEntity.element.compartment;

import java.awt.Shape;
import java.awt.geom.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.map.compartment.LeftSquareCompartment;
import lcsb.mapviewer.model.map.layout.ColorSchema;

/**
 * Class responsible for drawing LeftSquareCompartment on the Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class LeftSquareCompartmentConverter extends CompartmentConverter<LeftSquareCompartment> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(LeftSquareCompartmentConverter.class.getName());

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing elements
   * 
   */
  public LeftSquareCompartmentConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected Shape getOuterShape(LeftSquareCompartment compartment) {
    return new Line2D.Double(compartment.getWidth(), compartment.getHeight(), compartment.getWidth(), 0);
  }

  @Override
  protected Shape getInnerShape(LeftSquareCompartment compartment) {
    return new Line2D.Double(
        compartment.getWidth() - compartment.getThickness(), 0, compartment.getWidth() - compartment.getThickness(),
        compartment.getHeight());
  }

  @Override
  protected Shape getBorderShape(LeftSquareCompartment compartment) {
    return new Area(new Rectangle2D.Double(0.0, 0.0, compartment.getWidth(), compartment.getHeight()));
  }

}
