package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.*;
import java.awt.geom.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.bioEntity.element.ElementConverter;
import lcsb.mapviewer.converter.graphics.geometry.ArrowTransformation;
import lcsb.mapviewer.model.graphics.*;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.*;

/**
 * This class defines basics used for drawing {@link Species} (node in the graph
 * representation) on the {@link Graphics2D} object.
 * 
 * @param <T>
 *          type of {@link Species} class that can be drawn with this converter
 * @author Piotr Gawron
 * 
 */
public abstract class SpeciesConverter<T extends Species> extends ElementConverter<T> {

  /**
   * Length of the arrow for transcription site visualization.
   */
  public static final int TRANSCRIPTION_SITE_ARROW_LENGTH = 8;
  /**
   * Angle of the arrow for transcription site visualization.
   */
  public static final double TRANSCRIPTION_SITE_ARROW_ANGLE = 3 * Math.PI / 4;
  /**
   * What is the distance between homodimer aliases when homodimer>1.
   */
  public static final int HOMODIMER_OFFSET = 6;

  /**
   * Default diameter of the modification residues.
   */
  public static final int DEFAULT_MODIFICATION_DIAMETER = 15;
  /**
   * How far from the original shape should the activity border be drawn.
   */
  protected static final int ACTIVITY_BORDER_DISTANCE = 5;
  /**
   * Default species font size.
   */
  protected static final int DEFAULT_SPECIES_FONT_SIZE = 12;

  /**
   * Height of the rectangle that contains unit of information.
   */
  private static final int UNIT_OF_INFORMATION_HEIGHT = 20;

  /**
   * Size of the margin in the unit of information description.
   */
  private static final int TEXT_MARGIN_FOR_UNIT_OF_INFORMATION_DESC = 20;

  /**
   * Minimum width of the unit of information rectangle.
   */
  private static final int MIN_UNIT_OF_INFORMATION_WIDTH = 40;
  /**
   * Default font size for the modifier description.
   */
  protected static final int DEFAULT_SPECIES_MODIFIER_FONT_SIZE = 10;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SpeciesConverter.class.getName());
  /**
   * Graphical helper object with line transformation functions.
   */
  private LineTransformation lineTransformation = new LineTransformation();
  private ArrowTransformation arrowTransformation = new ArrowTransformation();

  /**
   * Default font used to draw unit of information of the species.
   */
  private Font unitOfInformationFont = null;
  /**
   * Object that helps to convert {@link ColorSchema} values into colors.
   */
  private ColorExtractor colorExtractor;

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  protected SpeciesConverter(ColorExtractor colorExtractor) {
    this.colorExtractor = colorExtractor;
  };

  /**
   * Returns default shape of the {@link Species}.
   * 
   * @param species
   *          {@link Species} for which we are looking for a border
   * @return {@link Shape} object defining given {@link Species}
   */
  protected Shape getDefaultAliasShape(final Species species) {
    Shape shape;
    shape = new Rectangle(species.getX().intValue(), species.getY().intValue(), species.getWidth().intValue(),
        species.getHeight().intValue());
    return shape;
  }

  /**
   * Returns font that should be used for drawing description of the
   * {@link Species}.
   * 
   * @param species
   *          {@link Species} for which we are looking for a font
   * @param params
   *          specific drawing parameters (like scale)
   * @return {@link Font} that should be used for drawing {@link Species}
   *         description
   */
  protected Font getFont(final Species species, ConverterParams params) {
    double fontSize = species.getFontSize();
    return new Font(Font.SANS_SERIF, 0, (int) (fontSize * params.getScale()));
  }

  /**
   * Returns text describing {@link Species}.
   * 
   * @param species
   *          object under investigation
   * @return description of the {@link Species}
   */
  protected String getText(final T species) {
    String name = species.getName();
    if (name.equals("")) {
      name = " ";
    }

    return name;
  }

  /**
   * Return width of the text.
   *
   * @param text
   *          width of this text will be computed
   * @param graphics
   *          the width will be computed assuming using this graphics
   * @return width of the text
   */
  protected double getTextWidth(final String text, final Graphics2D graphics) {
    if (text == null) {
      return 0;
    }
    if (text.equals("")) {
      return 0;
    }

    double result = 0;
    String[] lines = text.split("\n");
    for (String string : lines) {
      result = Math.max(result, graphics.getFontMetrics().stringWidth(string));
    }

    return result;
  }

  /**
   * Returns text height.
   *
   * @param text
   *          height of this text will be computed
   * @param graphics
   *          the height will be computed assuming using this graphics
   * @return height of the text
   */
  protected double getTextHeight(final String text, final Graphics2D graphics) {
    if (text == null) {
      return 0;
    }
    if (text.equals("")) {
      return 0;
    }

    double result = 0;
    int lines = text.split("\n").length;
    result = graphics.getFontMetrics().getHeight() * lines;
    return result;
  }

  /**
   * This method draws a string on graphics using current font. The coordinates of
   * the text is given as a point. Both parameters centered described if text
   * should be automatically centered horizontally and vertically.
   *
   * @param point
   *          where the text should be drawn
   * @param text
   *          text to draw
   * @param graphics
   *          where we want to draw the object
   * @param horizontalCentered
   *          should the text be horizontally centered
   * @param verticalCentered
   *          should the text be vertically centered
   */
  protected void drawText(final Point2D point, final String text, final Graphics2D graphics,
      final boolean horizontalCentered, final boolean verticalCentered) {
    double height = getTextHeight(text, graphics);
    double x = point.getX();
    double y = point.getY();
    if (verticalCentered) {
      y -= height / 2 - graphics.getFontMetrics().getAscent();
    }
    String[] lines = text.split("\n");

    double lineHeight = graphics.getFontMetrics().getHeight();
    for (String string : lines) {
      double currX = x;
      if (horizontalCentered) {
        currX -= getTextWidth(string, graphics) / 2;
      }
      graphics.drawString(string, (int) currX, (int) y);
      y += lineHeight;
    }
  }

  /**
   * Returns line style used for drawing alias border.
   *
   * @param species
   *          {@link Species} to be drawn
   * @return style of the line used to draw {@link Species}
   */
  protected Stroke getBorderLine(final Species species) {
    if (!species.isHypothetical()) {
      if (species instanceof Complex) {
        return LineType.SOLID_BOLD.getStroke();
      } else {
        return LineType.SOLID.getStroke();
      }
    } else {
      if (species instanceof Complex) {
        return LineType.DASHED_BOLD.getStroke();
      } else {
        return LineType.DASHED.getStroke();
      }
    }
  }

  /**
   * Returns border of the {@link Species} as {@link PathIterator}.
   *
   * @param species
   *          {@link Species} for which we are looking for a border
   * @return {@link PathIterator} object defining given {@link Species}
   */
  protected abstract PathIterator getBoundPathIterator(T species);

  /**
   * @return the lineTransformation
   */
  protected LineTransformation getLineTransformation() {
    return lineTransformation;
  }

  /**
   * @param lineTransformation
   *          the lineTransformation to set
   */
  protected void setLineTransformation(LineTransformation lineTransformation) {
    this.lineTransformation = lineTransformation;
  }

  /**
   * @return the unitOfInformationFont
   */
  protected Font getUnitOfInformationFont() {
    return unitOfInformationFont;
  }

  /**
   * @param unitOfInformationFont
   *          the unitOfInformationFont to set
   */
  protected void setUnitOfInformationFont(Font unitOfInformationFont) {
    this.unitOfInformationFont = unitOfInformationFont;
  }

  /**
   * Draws structural state description of the alias (ellipse in the top part of
   * the alias).
   *
   * @param state
   *          state description text
   * @param species
   *          state description should be drawn on this {@link Species}
   * @param graphics
   *          where the drawing should be performed
   */
  protected void drawStructuralState(StructuralState state, final Graphics2D graphics) {
    if (state == null) {
      return;
    }

    double width = state.getWidth();
    double height = state.getHeight();

    Ellipse2D ellipse = new Ellipse2D.Double(state.getPosition().getX(), state.getPosition().getY(), width, height);
    Color c = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(ellipse);
    graphics.setColor(c);
    graphics.draw(ellipse);
    if (!state.getValue().equals("")) {
      Font tmpFont = graphics.getFont();
      Font font = new Font(Font.SANS_SERIF, 0, state.getFontSize().intValue());
      graphics.setFont(font);

      width = graphics.getFontMetrics().stringWidth(state.getValue());
      height = graphics.getFontMetrics().getAscent() - graphics.getFontMetrics().getDescent();

      double x = state.getPosition().getX() + (state.getWidth() - width) / 2;
      double y = state.getPosition().getY() + (state.getHeight() + height) / 2;

      graphics.drawString(state.getValue(), (int) x, (int) y);
      graphics.setFont(tmpFont);
    }

  }

  /**
   * Draws unit of information for the {@link Species} (rectangle in the top part
   * of the alias).
   *
   * @param text
   *          unit of information text
   * @param species
   *          unit of information should be drawn on this {@link Species}
   * @param graphics
   *          where the drawing should be performed
   */
  protected void drawUnitOfInformation(String text, T species, final Graphics2D graphics) {
    if (text == null) {
      return;
    }

    Point2D p = new Point2D.Double(species.getCenterX(), species.getY());

    double width = MIN_UNIT_OF_INFORMATION_WIDTH;
    if (!text.trim().equals("")) {
      width = Math.max(MIN_UNIT_OF_INFORMATION_WIDTH,
          graphics.getFontMetrics().stringWidth(text) + TEXT_MARGIN_FOR_UNIT_OF_INFORMATION_DESC);
    }
    width = Math.min(width, species.getWidth());
    double height = UNIT_OF_INFORMATION_HEIGHT;

    Rectangle2D rectangle = new Rectangle2D.Double(p.getX() - width / 2, p.getY() - height / 2, width, height);
    Color c = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(rectangle);
    graphics.setColor(c);
    graphics.draw(rectangle);
    if (!text.trim().equals("")) {
      Font font = graphics.getFont();
      graphics.setFont(getUnitOfInformationFont());

      width = graphics.getFontMetrics().stringWidth(text);
      height = graphics.getFontMetrics().getAscent() - graphics.getFontMetrics().getDescent();

      graphics.drawString(text, (int) (p.getX() - width / 2), (int) (p.getY() + height / 2));
      graphics.setFont(font);
    }
  }

  @Override
  public final void draw(T species, Graphics2D graphics, ConverterParams params,
      List<ColorSchema> visualizedLayoutsColorSchemas) throws DrawingException {
    if (species.getGlyph() != null) {
      drawGlyph(species, graphics);
    } else {
      drawImpl(species, graphics, params);
    }

    Color oldColor = graphics.getColor();
    int count = 0;
    double width = species.getWidth() / visualizedLayoutsColorSchemas.size();
    for (ColorSchema schema : visualizedLayoutsColorSchemas) {
      if (schema != null) {
        double startX = (double) count / (double) visualizedLayoutsColorSchemas.size();
        graphics.setColor(Color.BLACK);

        int x = (int) (startX * species.getWidth() + species.getX());
        graphics.drawRect(x, species.getY().intValue(), (int) width, species.getHeight().intValue());

        Color color = colorExtractor.getNormalizedColor(schema);
        Color bgAlphaColor = new Color(color.getRed(), color.getGreen(), color.getBlue(), LAYOUT_ALPHA);
        graphics.setColor(bgAlphaColor);
        graphics.fillRect(x, species.getY().intValue(), (int) width, species.getHeight().intValue());
      }
      count++;
    }
    graphics.setColor(oldColor);
  }

  @Override
  public void drawText(final T species, final Graphics2D graphics, final ConverterParams params) {
    String text = getText(species);
    Font oldFont = graphics.getFont();
    Color oldColor = graphics.getColor();
    Font font = getFont(species, params);
    graphics.setColor(species.getFontColor());
    graphics.setFont(font);

    Point2D point = species.getCenter();
    if (species instanceof Complex) {
      if (((Complex) species).getElements().size() > 0) {
        if (isTransparent(species, params)) {
          point.setLocation(point.getX(), species.getY() + species.getHeight() - graphics.getFontMetrics().getAscent());
        }
      }
    }
    drawText(point, text, graphics, true, true);
    graphics.setFont(oldFont);
    graphics.setColor(oldColor);
  }

  /**
   * This method draws modification of the alias. If drawEmptyModification is set
   * to false then modification is not drawn if empty. If drawDescription is set
   * then also description (position) of the modification is drawn on the canvas.
   * 
   * @param protein
   *          object that is 'parent' of the residue
   * @param mr
   *          modification to be drawn
   * @param graphics
   *          - where the modification should be drawn
   * @param drawEmptyModification
   *          flag that indicates if we should draw empty modification
   */
  protected void drawModification(final ModificationResidue mr, final Graphics2D graphics,
      boolean drawEmptyModification) {
    if (mr instanceof Residue) {
      drawResidue((Residue) mr, graphics, drawEmptyModification);
    } else if (mr instanceof ModificationSite) {
      drawModificationSite((ModificationSite) mr, graphics, drawEmptyModification);
    } else if (mr instanceof BindingRegion) {
      drawBindingRegion((BindingRegion) mr, graphics);
    } else if (mr instanceof ProteinBindingDomain) {
      drawProteinBindingDomain((ProteinBindingDomain) mr, graphics);
    } else if (mr instanceof CodingRegion) {
      drawCodingRegion((CodingRegion) mr, graphics);
    } else if (mr instanceof RegulatoryRegion) {
      drawRegulatoryRegion((RegulatoryRegion) mr, graphics);
    } else if (mr instanceof TranscriptionSite) {
      drawTranscriptionSite((TranscriptionSite) mr, graphics);
    } else {
      throw new InvalidArgumentException("Unknown class type: " + mr.getClass());
    }
  }

  protected void drawResidue(final Residue residue, final Graphics2D graphics, boolean drawEmptyModification) {
    if (!drawEmptyModification && residue.getState() == null) {
      return;
    }
    double diameter = DEFAULT_MODIFICATION_DIAMETER;
    Point2D p = residue.getPosition();
    Ellipse2D ellipse = new Ellipse2D.Double(p.getX() - diameter / 2, p.getY() - diameter / 2, diameter, diameter);
    Color c = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(ellipse);
    graphics.setColor(c);
    graphics.draw(ellipse);

    ModificationState state = residue.getState();
    if (state != null) {
      String str = state.getAbbreviation();
      Font tmpFont = graphics.getFont();
      graphics.setFont(getStructuralFont());
      double x = p.getX() - graphics.getFontMetrics().stringWidth(str) / 2;
      double y = p.getY() + graphics.getFontMetrics().getAscent() / 2;
      graphics.drawString(str, (int) x, (int) y);
      graphics.setFont(tmpFont);
    }
  }

  private Font getStructuralFont() {
    return new Font(Font.SANS_SERIF, 0, DEFAULT_SPECIES_MODIFIER_FONT_SIZE);
  }

  /**
   * This method draws {@link ModificationSite} of the {@link Species}. If
   * drawEmptyModification is set to false then modification is not drawn if
   * empty.
   * 
   * @param modificationSite
   *          modification to be drawn
   * @param graphics
   *          - where the modification should be drawn
   * @param drawEmptyModification
   *          flag that indicates if we should draw empty modification
   */
  protected void drawModificationSite(final ModificationSite modificationSite, final Graphics2D graphics,
      boolean drawEmptyModification) {
    if (!drawEmptyModification && modificationSite.getState() == null) {
      return;
    }
    double diameter = DEFAULT_MODIFICATION_DIAMETER;

    double y = modificationSite.getPosition().getY();

    Point2D p = new Point2D.Double(modificationSite.getPosition().getX(), y - DEFAULT_MODIFICATION_DIAMETER);

    Ellipse2D ellipse = new Ellipse2D.Double(p.getX() - diameter / 2, p.getY() - diameter / 2, diameter, diameter);
    Color c = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(ellipse);
    graphics.setColor(c);
    graphics.draw(ellipse);
    graphics.drawLine((int) p.getX(), (int) (p.getY() + diameter / 2), (int) p.getX(), (int) y);

    ModificationState state = modificationSite.getState();
    if (state != null) {
      String str = state.getAbbreviation();
      Font tmpFont = graphics.getFont();
      graphics.setFont(getStructuralFont());
      double textX = p.getX() - graphics.getFontMetrics().stringWidth(str) / 2;
      double textY = p.getY() + graphics.getFontMetrics().getAscent() / 2;
      graphics.drawString(str, (int) textX, (int) textY);
      graphics.setFont(tmpFont);
    }
  }

  protected void drawBindingRegion(final BindingRegion bindingRegion, final Graphics2D graphics) {
    drawRegionModification(bindingRegion, graphics);
  }

  protected void drawProteinBindingDomain(final ProteinBindingDomain proteinBindingDomain, final Graphics2D graphics) {
    drawRegionModification(proteinBindingDomain, graphics);
  }

  protected void drawCodingRegion(final CodingRegion codingRegion, final Graphics2D graphics) {
    drawRegionModification(codingRegion, graphics);
  }

  private void drawRegionModification(AbstractRegionModification modification, Graphics2D graphics) {
    Point2D rectangleCenter = modification.getPosition();
    double width = modification.getWidth();
    double height = modification.getHeight();
    Rectangle2D rectangle = new Rectangle2D.Double(rectangleCenter.getX() - width / 2,
        rectangleCenter.getY() - height / 2, width, height);
    Color c = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(rectangle);
    graphics.setColor(c);
    graphics.draw(rectangle);

    String str = modification.getName();
    if (str != null && !str.trim().equals("")) {
      Font tmpFont = graphics.getFont();
      graphics.setFont(getStructuralFont());
      double textX = rectangleCenter.getX() - graphics.getFontMetrics().stringWidth(str) / 2;
      double textY = rectangleCenter.getY() + graphics.getFontMetrics().getAscent() / 2;
      graphics.drawString(str, (int) textX, (int) textY);
      graphics.setFont(tmpFont);
    }

  }

  protected void drawRegulatoryRegion(final RegulatoryRegion regulatoryRegion, final Graphics2D graphics) {
    drawRegionModification(regulatoryRegion, graphics);
  }

  protected void drawTranscriptionSite(final TranscriptionSite transcriptionSite, final Graphics2D graphics) {

    Color color = graphics.getColor();
    if (transcriptionSite.getActive()) {
      color = Color.RED;
    } else {
      color = Color.BLACK;
    }
    double y = transcriptionSite.getPosition().getY();
    double x = transcriptionSite.getPosition().getX();
    PolylineData line = new PolylineData();
    if (transcriptionSite.getDirection().equals("RIGHT")) {
      x += transcriptionSite.getWidth();
    }
    line.addPoint(new Point2D.Double(x, y));

    y -= DEFAULT_MODIFICATION_DIAMETER;
    line.addPoint(new Point2D.Double(x, y));

    if (transcriptionSite.getDirection().equals("RIGHT")) {
      x -= transcriptionSite.getWidth();
    } else {
      x += transcriptionSite.getWidth();
    }
    line.addPoint(new Point2D.Double(x, y));

    line.setColor(color);
    line.getEndAtd().setArrowType(ArrowType.FULL);
    line.getEndAtd().setLen(TRANSCRIPTION_SITE_ARROW_LENGTH);
    line.getEndAtd().setAngle(TRANSCRIPTION_SITE_ARROW_ANGLE);

    arrowTransformation.drawLine(line, graphics);

    y = transcriptionSite.getPosition().getY() - DEFAULT_MODIFICATION_DIAMETER - DEFAULT_SPECIES_FONT_SIZE / 2;
    if (transcriptionSite.getName() != null && !transcriptionSite.getName().isEmpty()) {
      Point2D centerTextPoint = new Point2D.Double(
          transcriptionSite.getPosition().getX() + transcriptionSite.getWidth() / 2, y);
      drawText(centerTextPoint, transcriptionSite.getName(), graphics, true, false);
    }
  }

  public ArrowTransformation getArrowTransformation() {
    return arrowTransformation;
  }

  public void setArrowTransformation(ArrowTransformation arrowTransformation) {
    this.arrowTransformation = arrowTransformation;
  }

}
