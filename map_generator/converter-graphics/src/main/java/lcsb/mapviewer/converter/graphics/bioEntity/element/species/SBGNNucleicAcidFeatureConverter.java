package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.*;
import java.awt.geom.*;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class defines methods used for drawing {@link Species} of nucleic acid
 * feature in the SBGN way on the {@link Graphics2D} object.
 * 
 * @author Michał Kuźma
 *
 */

public class SBGNNucleicAcidFeatureConverter extends SpeciesConverter<Species> {

  /**
   * How big should be the arc in rectangle for nucleic acid feature
   * representation.
   */
  private static final int RECTANGLE_CORNER_ARC_SIZE = 5;

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public SBGNNucleicAcidFeatureConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  /**
   * Returns shape of SBGN Nucleic acid feature.
   * 
   * @param species
   *          {@link Species} for which the shape should be returned
   * @return shape of the SBGN Nucleic acid feature for given alias
   */
  private Shape getShape(final Species species) {
    GeneralPath bottomRoundedRectangle = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
    double x = species.getX(), y = species.getY(), width = species.getWidth(), height = species.getHeight();

    bottomRoundedRectangle.moveTo(x, y);
    bottomRoundedRectangle.lineTo(x, y + height - RECTANGLE_CORNER_ARC_SIZE);
    bottomRoundedRectangle.curveTo(x, y + height, x + RECTANGLE_CORNER_ARC_SIZE, y + height,
        x + RECTANGLE_CORNER_ARC_SIZE, y + height);
    bottomRoundedRectangle.lineTo(x + width - RECTANGLE_CORNER_ARC_SIZE, y + height);
    bottomRoundedRectangle
        .curveTo(x + width, y + height, x + width, y + height - RECTANGLE_CORNER_ARC_SIZE, x + width,
            y + height - RECTANGLE_CORNER_ARC_SIZE);
    bottomRoundedRectangle.lineTo(x + width, y);
    bottomRoundedRectangle.closePath();
    return bottomRoundedRectangle;
  }

  @Override
  protected void drawImpl(Species species, Graphics2D graphics, ConverterParams params) {
    // Unit of information text - multimer cardinality
    String unitOfInformationText = null;
    if (species.getStatePrefix() != null && species.getStateLabel() != null) {
      if (species.getStatePrefix().equals("free input")) {
        unitOfInformationText = species.getStateLabel();
      } else {
        unitOfInformationText = species.getStatePrefix() + ":" + species.getStateLabel();
      }
    }

    int homodir = species.getHomodimer();

    species.setWidth(species.getWidth() - SpeciesConverter.HOMODIMER_OFFSET * (species.getHomodimer() - 1));
    species.setHeight(species.getHeight() - SpeciesConverter.HOMODIMER_OFFSET * (species.getHomodimer() - 1));

    species.setX(species.getX() + SpeciesConverter.HOMODIMER_OFFSET * homodir);
    species.setY(species.getY() + SpeciesConverter.HOMODIMER_OFFSET * homodir);

    int glyphCount;
    if (homodir > 1) {
      glyphCount = 2;
    } else {
      glyphCount = 1;
    }
    for (int i = 0; i < glyphCount; i++) {
      species.setX(species.getX() - SpeciesConverter.HOMODIMER_OFFSET);
      species.setY(species.getY() - SpeciesConverter.HOMODIMER_OFFSET);

      Shape shape = getShape(species);

      Color c = graphics.getColor();
      graphics.setColor(species.getFillColor());
      graphics.fill(shape);
      graphics.setColor(c);

      Stroke stroke = graphics.getStroke();
      graphics.setStroke(getBorderLine(species));
      graphics.draw(shape);
      graphics.setStroke(stroke);

      if (i == 1) {
        if (homodir > 1 && (unitOfInformationText == null || !unitOfInformationText.contains("N:"))) {
          if (unitOfInformationText == null) {
            unitOfInformationText = "";
          } else {
            unitOfInformationText = unitOfInformationText.concat("; ");
          }
          unitOfInformationText = unitOfInformationText.concat("N:").concat(Integer.toString(homodir));
        }
      }

    }

    drawUnitOfInformation(unitOfInformationText, species, graphics);
    drawText(species, graphics, params);

    species.setWidth(species.getWidth() + SpeciesConverter.HOMODIMER_OFFSET * (homodir - 1));
    species.setHeight(species.getHeight() + SpeciesConverter.HOMODIMER_OFFSET * (homodir - 1));
  }

  @Override
  protected PathIterator getBoundPathIterator(Species species) {
    return getShape(species).getPathIterator(new AffineTransform());
  }

}
