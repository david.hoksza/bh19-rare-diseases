#!/usr/bin/env bash

# ------------------------- PARAMETERS TO SET -------------------------
# Load parameters either from parameters.sh, or from a file provided as the first parameter of the script
if [[ $1 == "" ]]; then
    source parameters.sh
else
    source $1
fi
# ------------------------- PARAMETERS TO SET -------------------------

ASSOCIATIONS_DIR=associations/
ASSOCIATIONS_DATA_DIR=$ASSOCIATIONS_DIR/data/
EXTEND_DIR=${ASSOCIATIONS_DIR}/extend_network/
EXTEND_CONFIG=${EXTEND_DIR}/config.txt
RES_DIR=results
ENRICHMENT_DIR=enrichment/
ENRICHMENT_CONFIG=${ENRICHMENT_DIR}/config.txt
ENRICHMENT_CONFIG_TMP=${ENRICHMENT_CONFIG}.tmp
MAP_GENERATOR_DIR=map_generator/
PYTHON_BIN=python3
PIP_BIN=pip3

DISGENET_ASSOCIATION_SCORE_THRESHOLD_STR=${DISGENET_ASSOCIATION_SCORE_THRESHOLD//./_}
OPENTARGETS_ASSOCIATION_SCORE_THRESHOLD_STR=${OPENTARGETS_ASSOCIATION_SCORE_THRESHOLD//./_}

ORPHANET_IDS_UNDERSCORE=${ORPHANET_IDS//,/_}

mkdir $RES_DIR
RES_DIR=$RES_DIR/${ORPHANET_IDS_UNDERSCORE}
mkdir $RES_DIR

# First we copy the parameters into the result directory
cp parameters.sh ${RES_DIR}

check_exit_code(){
    if [ $? -ne 0 ]; then
        tput setaf 1; echo "The last command failed. Can't continue..."
        exit
    fi
}

log() {
    echo `date +"%T"`: $1
}


out_paths=""
if [ ${USE_DISGENET} = 1 ]; then
    log "Querying DisGeNET..."
    disgenet_out_path=${RES_DIR}/01-disgenet-n_${DISGENET_CNT_THRESHOLD}-s_${DISGENET_ASSOCIATION_SCORE_THRESHOLD_STR}.json
    $PYTHON_BIN $ASSOCIATIONS_DIR/disgenet.py -o ${ORPHANET_IDS} -n ${DISGENET_CNT_THRESHOLD} -s ${DISGENET_ASSOCIATION_SCORE_THRESHOLD} > ${disgenet_out_path}
    check_exit_code
    log "DisGeNET gene and variant associations stored in ${disgenet_out_path}"
    out_paths="${out_paths},${disgenet_out_path}"
fi


if [ ${USE_OPENTARGETS} = 1 ]; then
    # Get associations from OpenTargets
    log "Querying OpenTargets..."
    opentargets_out_path=${RES_DIR}/01-opentargets-n_${DISGENET_CNT_THRESHOLD}-s_${OPENTARGETS_ASSOCIATION_SCORE_THRESHOLD_STR}.json
    $PYTHON_BIN $ASSOCIATIONS_DIR/opentargets.py -o ${ORPHANET_IDS} -n ${OPENTARGETS_CNT_THRESHOLD} -s ${OPENTARGETS_ASSOCIATION_SCORE_THRESHOLD} > ${opentargets_out_path}
    check_exit_code
    log "Opentargets gene and variant associations stored in ${opentargets_out_path}"
    out_paths="${out_paths},${opentargets_out_path}"
fi



# Merge with ClinVar
log "Merging with ClinVar..."
genes_variants_out_path=${RES_DIR}/02-genes_variants.log
out_paths=${out_paths#,}
$PYTHON_BIN $ASSOCIATIONS_DIR/merge_with_clinvar.py -v $out_paths -c ${ASSOCIATIONS_DATA_DIR}/OrphaHPO_clinvar_variants_summary.tsv -oid ${ORPHANET_IDS} > ${genes_variants_out_path}
check_exit_code
log "Integration with ClinVar stored in ${genes_variants_out_path}"

genes_line=`cat ${genes_variants_out_path} | grep "genes in total"`
genes_out_path=${genes_variants_out_path/02-genes_variants/03-genes}
#echo ${genes_line#*:} | sed 's/\,/\n/g' > ${genes_out_path}
echo ${genes_line#*:} | tr ',' '\n' > ${genes_out_path}
log "Genes stored in ${genes_out_path}"

log "Extending genes list..."
text_mining_out_path=${genes_out_path/03-genes/03-text-mining}
Rscript --vanilla ${EXTEND_DIR}/get_extended_network.R ${genes_out_path} ${EXTEND_CONFIG}
check_exit_code
mv output.txt ${text_mining_out_path}
log "Genes list extended"

minerva_genes_out_path=${RES_DIR}/04-minerva-genes.txt
$PYTHON_BIN $ASSOCIATIONS_DIR/minerva_genes.py -f ${genes_out_path} > ${minerva_genes_out_path}
check_exit_code
log "Genes stored in ${minerva_genes_out_path}"

var_line=`cat ${genes_variants_out_path} | grep "variants in total"`
variants_out_path=${genes_variants_out_path/02-genes_variants/03-variants}
#echo ${var_line#*:} | sed 's/\,/\n/g' > ${variants_out_path}
echo ${var_line#*:} | tr ',' '\n' > ${variants_out_path}
log "Variants stored in ${variants_out_path}"

if [ ${USE_VEP} = 1 ]; then
    # Get associations from OpenTargets
    log "Filtering by allele frequency via Ensemble VEP service..."
    variants_vep_out_path=${variants_out_path/-variants/-varaints_vep}
    $PYTHON_BIN $ASSOCIATIONS_DIR/vepmining/vepAllInOne.py -f ${variants_out_path} -t ${VEP_THRESHOLD}> ${variants_vep_out_path}
    check_exit_code
    log "Fitlered variants stored in ${variants_vep_out_path}"
    variants_out_path=${variants_vep_out_path}
fi


log "Getting detailed variants information..."
minerva_variants_out_path=${RES_DIR}/04-minerva-variants.txt
$PYTHON_BIN $ASSOCIATIONS_DIR/minerva_variants.py -f ${variants_out_path} > ${minerva_variants_out_path}
log "Detailed variants information obtained"
check_exit_code

if [ ${STOP_AFTER_STAGE} = 1 ]; then
    echo "Exiting after stage ${STOP_AFTER_STAGE}"
    exit 0
fi
# ------------------------------ 2. Obtain pathways ------------------------------
log "Retrieving enriched pathways..."

tr '\r' '\n' < ${ENRICHMENT_CONFIG} > ${ENRICHMENT_CONFIG_TMP}

#update paremters file
#the .bak is because MAC uses BSD version of sed which requires the backup for in-place replacements
sed -E -i.bak "s/(max_areas_per_map)(.*)/\1,${ENRICH_MAX_AREAS_PER_MAP}/g" ${ENRICHMENT_CONFIG_TMP}
sed -E -i.bak "s/(max_areas_per_pathway_db)(.*)/\1,${MAX_AREAS_PER_PATHWAY_DB}/g" ${ENRICHMENT_CONFIG_TMP}
rm ${ENRICHMENT_CONFIG_TMP}.bak

Rscript --vanilla ${ENRICHMENT_DIR}/enrich_maps.R ${genes_out_path} ${ENRICHMENT_CONFIG_TMP}
check_exit_code

enriched_maps_out_path=$RES_DIR/05-enriched_disease_maps.txt
enriched_paths_out_path=$RES_DIR/05-enriched_pathways.txt
mv enriched_disease_maps.txt ${enriched_maps_out_path}
mv enriched_pathways.txt ${enriched_paths_out_path}

rm ${ENRICHMENT_CONFIG_TMP}

log "Enriched pathways retrieved"


if [ ${STOP_AFTER_STAGE} = 2 ]; then
    echo "Exiting after stage ${STOP_AFTER_STAGE}"
    exit 0
fi
# ------------------------------ 2. Assemble pathways and overlays into a map ------------------------------
if [ ${BUILD_MAP_GENERATOR} = 1 ]; then
    echo "Bulding the map generator ..."
    cd ${MAP_GENERATOR_DIR}
    mvn -DskipTests=true clean install -pl biohackathon -am
    cd ..
fi
log "Map generator built"
log "Assembling the map from pathways ..."
map_out_path=${RES_DIR}/06-minerva_map.xml
java -Xmx4g -jar ${MAP_GENERATOR_DIR}/biohackathon/target/biohackathon-1.0-jar-with-dependencies.jar --enrichr-file ${enriched_paths_out_path} --minerva-file  ${enriched_maps_out_path} --text-mining-file  ${text_mining_out_path} --output-file ${map_out_path}
#java -Xmx4g -jar ${MAP_GENERATOR_DIR}/biohackathon/target/biohackathon-1.0-jar-with-dependencies.jar --enrichr-file ${enriched_paths_out_path} --minerva-file  ${enriched_maps_out_path} --output-file ${map_out_path}
check_exit_code

log "Pathways assembled into ${map_out_path}"

log "Implanting UniProt annotations..."
map_out_path_uniprot=${map_out_path/.xml/_unp.xml}
$PYTHON_BIN ${MAP_GENERATOR_DIR}/utils/implant_annotations.py -m ${map_out_path} -v ${minerva_variants_out_path} > ${map_out_path_uniprot}
check_exit_code
log "UniProt annotations implanted"


log "Trimming long strings..."
map_out_path_trimmed=${map_out_path_uniprot/.xml/_trim.xml}
$PYTHON_BIN ${MAP_GENERATOR_DIR}/utils/trim_long_strings.py -m ${map_out_path_uniprot} > ${map_out_path_trimmed}
check_exit_code
log "Long string trimmed"

log "Combining the map with overlays"

tmp_dir=${RES_DIR}/tmp/
tmp_dir_layouts=${tmp_dir}/layouts/

mkdir ${tmp_dir}
cp ${map_out_path_trimmed} ${tmp_dir}

mkdir ${tmp_dir_layouts}
cp ${minerva_genes_out_path} ${tmp_dir_layouts}
cp ${minerva_variants_out_path} ${tmp_dir_layouts}

map_zip_out_path=${map_out_path_trimmed/.xml/.zip}
rm ${map_zip_out_path}
cd ${tmp_dir}
zip -r tmp.zip .
cd -
mv ${tmp_dir}/tmp.zip ${map_zip_out_path}

rm -rf ${tmp_dir}
log "Final map with overlays stored in ${map_zip_out_path}"