import common
import json
import argparse
from typing import List, Dict
import requests


def get_genes_and_evidence(orphanet_ids:List[str], association_score: float) -> Dict:

    genes = {}

    for oid in orphanet_ids:
        res = requests.get(
            "https://api.opentargets.io/v3/platform/public/association/filter",
            params={'disease': 'Orphanet_{}'.format(oid),
                    'scorevalue_min': association_score,
                    'size': '1000'}
        )
        if res.status_code == requests.codes.ok:
            content = json.loads(res.text)
            for data in content['data']:
                genes[data['target']['gene_info']['symbol']] = {
                    'gene_name': data['target']['gene_info']['symbol'],
                    'evidence_id': data['id'],
                    'association_score': data['association_score']['overall'],
                    'association_score_evidence': data['association_score']['datatypes'],
                    'variants': []
                }

    return genes

def get_variants(genes: Dict[str, Dict]):
    for gene_name in genes:
        s_ge = genes[gene_name]['evidence_id'].split('-')
        target = s_ge[0]
        disease = s_ge[1]

        res = requests.get(
            "https://platform-api-qc.opentargets.io/v3/platform/public/evidence/filter",
            params={'target': target,
                    'disease': disease,
                    'data_type': 'genetic_association',
                    'size': 10000
                    }
        )
        if res.status_code == requests.codes.ok:
            content = json.loads(res.text)
            if 'data' in content:
                for content_item in content['data']:
                    if 'variant' in content_item:
                        if 'dbsnp' in content_item['variant']['id']:
                            rsId = content_item['variant']['id'].split("/")[-1]
                            genes[gene_name]['variants'].append(rsId)

        # Not sure whether we can't get duplicities from the API so we better uniquify
        genes[gene_name]['variants'] = list(set(genes[gene_name]['variants']))


if __name__ == '__main__':

    common.init_logging()


    parser = argparse.ArgumentParser()

    parser.add_argument("-oids", "--orphanet_ids",
                        required=True,
                        help="Inpu Orphanet numbers")
    parser.add_argument("-n", "--top-n",
                        required=False,
                        default=50,
                        type=int,
                        help="Retrieve the top N genes sorted by association score (0 for all)")
    parser.add_argument("-s", "--threshold-score",
                        required=False,
                        default=0,
                        type=float,
                        help="Only consider genes with association score higher than given threshold")

    args = parser.parse_args()

    genes_evidence = get_genes_and_evidence(args.orphanet_ids.split(","), args.threshold_score)
    genes_sorted = {}
    cnt = args.top_n
    i = 0
    for k, v in sorted(genes_evidence.items(), key=lambda x: x[1]['association_score'], reverse=True):
        if i >= cnt:
            break
        i += 1
        genes_sorted[k] = v

    get_variants(genes_sorted)

    print(json.dumps({"name": "opentargets", "genes": genes_sorted}, indent=2))






