import common
import logging
import json
import argparse
from typing import List, Dict, Set
import requests
import copy


def get_dbsnp(ids: List[str]) -> List[Dict]:

    snps = []

    logging.info("Connecting to myvariant API")
    res = requests.post(
        # "https://api.ncbi.nlm.nih.gov/variation/v0/beta/refsnp/{}".format(id)
        "https://myvariant.info/v1/variant",
        data={
            "ids": ",".join(ids)
        },
        params={
            "fields": "hg19.start, vcf.ref, dbnsfp.aa.ref, vcf.alt, dbnsfp.aa.alt, dbsnp.gene.symbol, "
                      "dbsnp.vartype, chrom, dbsnp.rsid, dbnsfp.aa.pos, dbnsfp.uniprot.acc"
        }
    )
    logging.info("Retrieved data from myvariant API")
    if res.status_code == requests.codes.ok:
        content = json.loads(res.text)
        cnt = 0
        for res in content:

            if "dbsnp" not in res:
                continue

            if "gene" not in res["dbsnp"]:
                # This can happen for variants which are not mapped onto a gene in dbSNP but for example OpenTargets can
                # obtain this information from a difference source. Example would be rs9388451 which is not associated
                # with a gene in dbSNP but it is in GWAS Catalog (or at least this was the situation as of 2019-12-02)
                continue

            genes = res["dbsnp"]["gene"]
            if not isinstance(genes, list):
                genes = [genes]

            cnt += len(genes)

            if "dbnsfp" not in res \
                    or "uniprot" not in res["dbnsfp"] \
                    or "aa" not in res["dbnsfp"] :
                continue
            uniprot_ids = set()
            if isinstance(res["dbnsfp"]["uniprot"], list):
                for uniprot in res["dbnsfp"]["uniprot"]:
                    if isinstance(uniprot, list):
                        uniprot = uniprot[0]
                    uniprot_ids.add(uniprot["acc"])
            else:
                uniprot = res["dbnsfp"]["uniprot"]
                if isinstance(uniprot, list):
                    uniprot = uniprot[0]
                uniprot_ids.add(uniprot["acc"])
            #TODO: this should be properly handled
            # if len(uniprot_ids) > 1:
            #     continue
            # else:
            #     uniprot_acc = uniprot_ids.pop()
            uniprot_acc = uniprot_ids.pop()

            aa = res["dbnsfp"]["aa"]
            aas = aa if isinstance(aa, list) else [aa] #we can hav multiple aa mappings if there is a splice site

            for aa in aas:

                if "ref" not in aa:
                    continue

                for gene in genes:

                    position = str(res["hg19"]["start"])
                    original_dna = res["vcf"]["ref"]
                    original_aa = aa["ref"]
                    alternative_dna = res["vcf"]["alt"]
                    alternative_aa = aa["alt"]

                    name = gene["symbol"]
                    description = res["dbsnp"]["vartype"]
                    color = "#ff0000"
                    contig = res["chrom"]
                    allele_frequency = "0.8" #there is probably a bug in myvariant since it shows wrong allele in dbsnp and then the frequency might be wrong as well
                    variant_identifier = res["dbsnp"]["rsid"]

                    # Sometimes, you get something like this:
                    # "uniprot": [
                    #         {
                    #           "acc": "O14746",
                    #           "entry": "TERT_HUMAN"
                    #         },
                    #         {}
                    #       ],


                    pos = aa["pos"]

                    if isinstance(pos, list):
                        #TODO - taking the first one is just temporary solution because this can be off from the position of Uniprot record which is in MINERVA
                        aa_pos = pos[0]
                    elif isinstance(pos, int):
                        aa_pos = pos
                    else:
                        assert False
                    amino_acid_change = "{}:XXX:g.{}{}>{}:p.{}{}{}".format(name,
                                                                           position,
                                                                           original_dna,
                                                                           alternative_dna,
                                                                           original_aa,
                                                                           aa_pos,
                                                                           alternative_aa)

                    snps.append({
                        "position": position,
                        "original_dna": original_dna,
                        "alternative_dna": alternative_dna,
                        "gene_name": name,
                        "description": description,
                        "color": color,
                        "contig": contig,
                        "allele_frequency": allele_frequency,
                        "variant_identifier": variant_identifier,
                        "amino_acid_change": amino_acid_change,
                        "identifier_uniprot": uniprot_acc
                    })

        logging.info("Skipped {} variants out of {}".format(cnt - len(snps), cnt))

    return snps

def remove_snps_with_multiple_uniprot_ids(db_snps: List[Dict]) -> List[Dict]:
    snp_cnt:Dict[str, Set] = {}
    for snp in db_snps:
        id = snp["variant_identifier"]
        if id not in snp_cnt:
            snp_cnt[id] = set()
        snp_cnt[id].add(snp["identifier_uniprot"])

    id_to_keep = [id for id in snp_cnt if len(snp_cnt[id]) == 1]
    new_db_snps: List[Dict] = []
    for snp in db_snps:
        if snp["variant_identifier"] in id_to_keep:
            new_db_snps.append(snp)

    logging.info("Removed {} variants out of {} because of multiple uniprot ids per gene.".format(len(db_snps) - len(new_db_snps), len(db_snps)))

    return new_db_snps

def get_minerva_format(db_snps: List[Dict]) -> str:

    out = ""

    if len(db_snps) == 0:
        return out


    out += "#NAME=DISEASE_ASSOCIATED_VARIANTS\n"
    out += "#TYPE=GENETIC_VARIANT\n"
    out += "#GENOME_TYPE=UCSC\n"
    out += "#GENOME_VERSION=hg19\n"
    keys = list(db_snps[0].keys())
    keys.remove('variant_identifier')
    out += "\t".join(keys) + "\n"
    for snp in db_snps:
        snp_cp = copy.deepcopy(snp)
        del snp_cp['variant_identifier']
        out += "\t".join(snp_cp.values()) + "\n"

    return out

if __name__ == '__main__':

    common.init_logging()


    parser = argparse.ArgumentParser()

    parser.add_argument("-f", "--file",
                        required=True,
                        help="A file with comma-separated list of snp ids")

    args = parser.parse_args()

    with open(args.file) as f:
        dbsnp_ids = f.read()
        db_snps = get_dbsnp(dbsnp_ids.split("\n"))
        print(get_minerva_format(remove_snps_with_multiple_uniprot_ids(db_snps)))
