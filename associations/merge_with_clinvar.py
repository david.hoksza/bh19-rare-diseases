import common
import logging
import json
import argparse
from typing import List, Dict, Set, Tuple
import requests
import pandas as pd


def parse_gene_variants(fnames: List[str]) -> List[Dict]:
    gvs:List[Dict] = []
    for fname in fnames:
        with open(fname) as f:
            gvs.append(json.loads(f.read()))
    return gvs


def df_contains_list(df, column_id, ids):
    df_nn = df[df[column_id].notnull()]
    # df_nn[column_id] = ',' + df[column_id].astype(str) + ','
    frames = []
    for id in ids:
        frames.append(df_nn[df_nn[column_id].str.contains(',' + id + ',')])
        # frames.append(df_nn[df_nn[column_id] == id])
    return pd.concat(frames)


def filter_out_conflicting(gene_variants: Dict[str, Dict]):
    rv = {}
    for gene in gene_variants:
        rv[gene] = {"variants": []}
        vars = gene_variants[gene]['variants']
        for dbsnp in vars:
            assert len(vars[dbsnp]) > 0
            if len(vars[dbsnp]) == 1 and 'benign' not in vars[dbsnp][0].lower():
                rv[gene]['variants'].append(dbsnp)
            else:
                if len(vars[dbsnp]) > 1:
                    benign_cnt = 0
                    for significance in vars[dbsnp]:
                        if 'benign' in significance.lower():
                            benign_cnt += 1
                    if benign_cnt < len(vars[dbsnp]):
                        #if at least one of the evidence is not benign, include the variant
                        rv[gene]['variants'].append(dbsnp)
    return rv


def get_clinvar_variants(clinvar_fname:str, orpha_ids:List[str]):
    # df = pd.read_csv(clinvar_fname, compression='zip', sep='\t')
    df = pd.read_csv(clinvar_fname, sep='\t')
    df["OrphanetID"] = ',' + df["OrphanetID"].astype(str) + ','
    # print(df.head())
    orpha_full_ids = ["Orphanet:ORPHA{}".format(id) for id in orpha_ids]
    df_relevant = df_contains_list(df, "OrphanetID", orpha_full_ids)
    gene_variants = {}
    for index, row in df_relevant.iterrows():
        gene_symbols = row['GeneSymbol'] #we can have multiple gene symbols per snp (e.g DPAGT1;HMBS)
        for gene_symbol in gene_symbols.split(";"):
            dbsnp = row['RS...dbSNP.']
            significance = row['ClinicalSignificance']
            if dbsnp == -1:
                continue
            dbsnp = 'rs{}'.format(dbsnp)

            if gene_symbol not in gene_variants:
                gene_variants[gene_symbol] = {'variants': {}}
            if dbsnp not in gene_variants[gene_symbol]['variants']:
                gene_variants[gene_symbol]['variants'][dbsnp] = []
            gene_variants[gene_symbol]['variants'][dbsnp].append(significance)
    return filter_out_conflicting(gene_variants)


def contrast_clinvar(gene_variants:List[Dict], gene_variants_clinvar:Dict):

    all_g_v = gene_variants + [gene_variants_clinvar]

    genes: List[Set[str]] = []

    print("-------------Gene-level summary-------------")
    for i in range(len(all_g_v)):
        genes.append(set(all_g_v[i]["genes"].keys()))
        print('{} {} genes: {}'.format(len(genes[i]), all_g_v[i]["name"], sorted(genes[i])))

    print()

    for i in range(len(all_g_v)):
        name1 = all_g_v[i]["name"]
        genes1 = genes[i]
        for j in range(0, len(all_g_v)):
            if i == j:
                continue
            name2 = all_g_v[j]["name"]
            genes2 = genes[j]

            g1_g2 = sorted(genes1.difference(genes2))
            g2_g1 = sorted(genes2.difference(genes1))

            print('{} genes in {} and not in {}: {}'.format(len(g1_g2), name1, name2, str(g1_g2)))
            print('{} genes in {} and not in {}: {}'.format(len(g2_g1), name2, name1, str(g2_g1)))

    print()
    print("-------------Variant-level summary-------------")
    variants:List[Set[Tuple[str]]] = [] #for each dataset a set of gene-dbsnpid pairs
    for i in range(len(all_g_v)):
        variants.append(set())
        for gene in all_g_v[i]["genes"]:
            for variant in all_g_v[i]["genes"][gene]['variants']:
                variants[i].add((gene, variant))
        print('{} {} variants: {}'.format(len(variants[i]), all_g_v[i]["name"], sorted(variants[i])))

    for i in range(len(all_g_v)):
        name1 = all_g_v[i]["name"]
        vars1 = variants[i]
        for j in range(0, len(all_g_v)):
            if i == j:
                continue
            name2 = all_g_v[j]["name"]
            vars2 = variants[j]

            v1_v2 = sorted(vars1.difference(vars2))
            v2_v1 = sorted(vars2.difference(vars1))

            print('{} variants in {} and not in {}: {}'.format(len(v1_v2), name1, name2, str(v1_v2)))
            print('{} variants in {} and not in {}: {}'.format(len(v2_v1), name2, name1, str(v2_v1)))

    print()
    print("-------------Merged data-------------")
    genes_all = set.union(*genes)
    print('{} genes in total: {}'.format(len(genes_all), ",".join(genes_all)))
    dbsnp_all = set()
    for dataset_variants in variants:
        dbsnp_all = dbsnp_all.union([v[1] for v in dataset_variants])
    print('{} variants in total: {}'.format(len(dbsnp_all), ", ".join(dbsnp_all)))

if __name__ == '__main__':

    common.init_logging()


    parser = argparse.ArgumentParser()

    parser.add_argument("-v", "--variant-files",
                        required=True,
                        help="Comma-separated list of JSON input files with gene-variant mapping")
    parser.add_argument("-c", "--clinvar",
                        required=True,
                        help="ClinVar input file")
    parser.add_argument("-oids", "--orphanet_ids",
                        required=True,
                        help="List of OMIM IDs, Orphanet IDs and others")

    args = parser.parse_args()

    gene_variants: List[Dict] = parse_gene_variants(args.variant_files.split(","))
    gene_variants_clinvar = get_clinvar_variants(args.clinvar, args.orphanet_ids.split(","))
    contrast_clinvar(gene_variants, {"name": "clinvar", "genes": gene_variants_clinvar})