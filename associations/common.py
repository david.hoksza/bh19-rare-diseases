#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Helper tools and functions common to every module.

"""

import sys
import logging
import gzip
import os
import numpy as np
import fnmatch
from six import iteritems
import argparse

__author__ = "David Hoksza"
__email__ = "david.hoksza@gmail.com"
__license__ = 'X11'

def extract_target_from_fn(fn):

    target = fn[:fn.rfind('.')]
    target = target[max(target.rfind('/'), target.rfind('\\')) + 1:]

    return  target


def gen_dict_extract(key, var):
    if hasattr(var,'iteritems'):
        for k, v in var.iteritems():
            if k == key:
                yield v
            if isinstance(v, dict):
                for result in gen_dict_extract(key, v):
                    yield result
            elif isinstance(v, list):
                for d in v:
                    for result in gen_dict_extract(key, d):
                        yield result

def find_files_recursively(directory, pattern):
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, pattern):
            matches.append(os.path.join(root, filename))
    return matches


def input(message):
    if sys.version_info >= (3,):
        response = input(message)
    else:
        response = raw_input(message)


def open_file(file_name, mode="r", encoding="utf-8"):
    access_type = mode
    if sys.version_info >= (3,): access_type = mode + "t"
    if file_name.endswith("gz"):
        return gzip.open(file_name, access_type, encoding=encoding)
    else:
        return open(file_name, access_type, encoding=encoding)


def init_logging():
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s] %(module)s - %(message)s',
        datefmt='%H:%M:%S')


def to_float(x):
    try:
        a = float(x)
        if np.isinf(a): a = float('nan')
    except ValueError:
        return float('nan')
    else:
        return a


def delete_files(file_names):
    for fn in file_names:
        os.remove(fn)


def find_key_in_dictionary(key, dictionary):
    for k, v in iteritems(dictionary):
        if k == key:
            yield v
        elif isinstance(v, dict):
            for result in find_key_in_dictionary(key, v):
                yield result
        elif isinstance(v, list):
            for d in v:
                for result in find_key_in_dictionary(key, d):
                    yield result


def find_val_in_dictionary(value, dictionary):
    import re

    if isinstance(dictionary, dict):
        for k, v in iteritems(dictionary):
            if isinstance(v, str) and re.match(value, v):
                yield (k, v)
            elif isinstance(v, dict):
                for result in find_val_in_dictionary(value, v):
                    yield result
            elif isinstance(v, list):
                for d in v:
                    if isinstance(d, str) and re.match(value, d):
                        yield (k, d)
                    else:
                        for result in find_val_in_dictionary(value, d):
                            yield result
    elif isinstance(dictionary, list):
        for d in dictionary:
            for result in find_val_in_dictionary(value, d):
                yield result


def substr_in_list(str, l):
    return any(str in s for s in l)

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def sanitize_string(s: str):
    return "".join(x for x in s if x.isalnum() or x in "_-")


