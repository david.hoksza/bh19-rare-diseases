import common
import logging
import json
import argparse
from typing import List, Dict
import requests

def get_umls_ids(orphanet_ids:List[str]) -> List[str]:

    umls_ids = []
    for orphanet_id in orphanet_ids:
        res = requests.get(
            "https://www.ebi.ac.uk/ols/api/ontologies/ordo/terms", params={'iri': 'http://www.orpha.net/ORDO/Orphanet_{}'.format(orphanet_id)})
        if res.status_code == requests.codes.ok:
            content = json.loads(res.text)
            annotations = content['_embedded']['terms'][0]['annotation']['hasDbXref']
            umls_ids += [ann.replace("UMLS:", "") for ann in annotations if 'UMLS:' in ann]

    return umls_ids


def get_genes(umls_ids:List[str], score:float) -> Dict[str, Dict]:

    genes = {}

    logging.info("Connecting to disgenet gda endpoint")
    res = requests.get(
        "http://www.disgenet.org/api/gda/disease/{}".format(",".join(umls_ids)),
        params={"min_score": score}
    )
    logging.info("Retrieved data from disgenet gda endpoint")
    if res.status_code == requests.codes.ok:
        content = json.loads(res.text)
        for rec in content:
            genes[rec['gene_symbol']] = {"association_score": rec['score'], "variants": []}

    return genes

def get_variants(umls_ids:List[str], genes: Dict[str, Dict]):

    logging.info("Connecting to disgenet vda endpoint")
    res = requests.get(
        "http://www.disgenet.org/api/vda/disease/{}".format(",".join(umls_ids)),
        params={"gene": ",".join(genes.keys())}
    )
    logging.info("Retrieved data from disgenet vda endpoint")

    if res.status_code == requests.codes.ok:
        content = json.loads(res.text)
        for rec in content:
            gene = rec['gene_symbol']
            for g in gene.split(';'):
                if g not in genes:
                    continue
                genes[g]['variants'].append(rec['variantid'])

if __name__ == '__main__':

    common.init_logging()

    parser = argparse.ArgumentParser()

    parser.add_argument("-oids", "--orphanet_ids",
                        required=True,
                        help="Input Orphanet numbers")
    parser.add_argument("-n", "--top-n",
                        required=False,
                        default=50,
                        type=int,
                        help="Retrieve the top N genes sorted by association score (0 for all)")
    parser.add_argument("-s", "--threshold-score",
                       required=False,
                       default=0,
                       type=float,
                       help="Only consider genes with association score higher than given threshold")

    args = parser.parse_args()

    umls_ids = get_umls_ids(args.orphanet_ids.split(","))
    genes = get_genes(umls_ids, args.threshold_score)
    get_variants(umls_ids, genes)
    genes_sorted = {}
    cnt = args.top_n
    i = 0
    for k, v in sorted(genes.items(), key=lambda x: x[1]['association_score'], reverse=True):
        if i >= cnt:
            break
        i += 1
        genes_sorted[k] = v

    print(json.dumps({"name": "disgenet", "genes": genes_sorted}, indent=2))






